#!/usr/bin/perl
# $Id: ary-perl.code,v 1.5 2005/04/04 14:56:38 bfulgham Exp $
# http://www.bagley.org/~doug/shootout/

# this program is modified from:
#   http://cm.bell-labs.com/cm/cs/who/bwk/interps/pap.html
# Timing Trials, or, the Trials of Timing: Experiments with Scripting
# and User-Interface Languages</a> by Brian W. Kernighan and
# Christopher J. Van Wyk.

# Modified: 2005-06-18 Cosimo Streppone

use integer;

my $n = @ARGV[0] || 1;
my(@X, @Y, $i, $k);
my $last = $n - 1;

# Initialize @X list in a single step
@X = (1 .. $n);

# Execute 1000 times
for(0 .. 999) {
    # Use of `$_' aliasing is faster than using a lexical var
    # Also, there is no need to reverse (0 .. $last) list
    $Y[$_] += $X[$_] for 0 .. $last;
}

print $Y[0], ' ', $Y[$last], "\n";

