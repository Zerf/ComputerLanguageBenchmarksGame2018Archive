{ The Great Computer Language Shootout
  http://shootout.alioth.debian.org

  contributed by Ales Katona
  modified by Vincent Snijders
}

program mandelbrot;

const
  Limit=4.0;

var n, x, y, i, bits,bit: Longint;
    Zr, Zi, Ti, Tr, Cr, Ci: Double;

begin
  Val(ParamStr(1), n, y);
  writeln('P4');
  writeln(n,' ',n);
  for y := 0 to n-1 do
  begin
    bits := 255;  bit := 128;
    Ci := 2.0 * y / n - 1.0;
    for x := 0 to n-1 do
    begin
      Cr := 2.0 * x / n - 1.5;

      Zr := 0;  Zi := 0; Tr := 0; Ti := 0;
      i := 0;
      while (i<50) and (Tr + Ti< limit) do begin
        inc(i);
        Zi := 2*Zr*Zi + Ci;
        Zr := Tr - Ti + Cr;
        Ti := Zi * Zi;
        Tr := Zr * Zr;
      end;
      
      if (i<50) or (Tr + Ti>=limit) then
        bits := bits xor bit;

      if bit > 1 then
        bit := bit shr 1
      else
      begin
        write(chr(bits));
        bits := 255;  bit := 128;
      end;
    end;
    if bit < 128 then write(chr(bits xor((bit shl 1)-1)));
  end;
end.
