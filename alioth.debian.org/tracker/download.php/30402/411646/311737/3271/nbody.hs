{-# OPTIONS_GHC -O2 -fexcess-precision -funbox-strict-fields #-}
{-# LANGUAGE BangPatterns #-}
-- The Computer Language Benchmarks Game
-- http://shootout.alioth.debian.org/
--
-- Contributed by David Hotham.
--
-- To be compiled with:
--
--  -funfolding-use-threshold=16
import System(getArgs)
import Text.Printf(printf)
import Data.List(foldl', foldl1')

data Vector3 = Vec !Double !Double !Double

(.+.) :: Vector3 -> Vector3 -> Vector3
(Vec x y z) .+. (Vec u v w) = Vec (x+u) (y+v) (z+w)

(.-.) :: Vector3 -> Vector3 -> Vector3
(Vec x y z) .-. (Vec u v w) = Vec (x-u) (y-v) (z-w)

(*.) :: Double -> Vector3 -> Vector3
k *. (Vec x y z) = Vec (k*x) (k*y) (k*z)

magnitude2 :: Vector3 -> Double
magnitude2 (Vec x y z) = x*x + y*y + z*z

data Body = Body !Vector3 !Vector3 !Double

jupPos, satPos, uraPos, nepPos :: Vector3
jupPos = Vec (4.84143144246472090e+00)
             (-1.16032004402742839e+00)
             (-1.03622044471123109e-01)
satPos = Vec (8.34336671824457987e+00)
             (4.12479856412430479e+00)
             (-4.03523417114321381e-01)
uraPos = Vec (1.28943695621391310e+01)
             (-1.51111514016986312e+01)
             (-2.23307578892655734e-01)
nepPos = Vec (1.53796971148509165e+01)
             (-2.59193146099879641e+01)
             (1.79258772950371181e-01)

days_per_year :: Double
days_per_year = 365.24

jupVel, satVel, uraVel, nepVel :: Vector3
jupVel = Vec (1.66007664274403694e-03*days_per_year)
             (7.69901118419740425e-03*days_per_year)
             (-6.90460016972063023e-05*days_per_year)
satVel = Vec (-2.76742510726862411e-03*days_per_year)
             (4.99852801234917238e-03*days_per_year)
             (2.30417297573763929e-05*days_per_year)
uraVel = Vec (2.96460137564761618e-03*days_per_year)
             (2.37847173959480950e-03*days_per_year)
             (-2.96589568540237556e-05*days_per_year)
nepVel = Vec (2.68067772490389322e-03*days_per_year)
             (1.62824170038242295e-03*days_per_year)
             (-9.51592254519715870e-05*days_per_year)

jupMass, satMass, uraMass, nepMass, solar_mass :: Double
solar_mass = 4 * pi ** 2;
jupMass = (9.54791938424326609e-04 * solar_mass)
satMass = (2.85885980666130812e-04 * solar_mass)
uraMass = (4.36624404335156298e-05 * solar_mass)
nepMass = (5.15138902046611451e-05 * solar_mass)

sun, jupiter, saturn, uranus, neptune :: Body
sun = Body (Vec 0 0 0) (Vec 0 0 0) solar_mass
jupiter = Body jupPos jupVel jupMass
saturn = Body satPos satVel satMass
uranus = Body uraPos uraVel uraMass
neptune = Body nepPos nepVel nepMass

kineticEnergy :: Body -> Double
kineticEnergy (Body _ vel m) = 0.5 * m * (magnitude2 vel)

potentialEnergy :: Body -> Body -> Double
potentialEnergy (Body p1 _ m1) (Body p2 _ m2) = -m1 * m2 / dist
  where dist = sqrt . magnitude2 $ p1 .-. p2

momentum :: Body -> Vector3
momentum (Body _ vel m) = m *. vel

offsetMomemtum :: [Body] -> [Body]
offsetMomemtum [] = []
offsetMomemtum bodies@(b:bs) = (Body p (v .+. compensation) m) : bs
  where totalMomentum = foldl1' (.+.) $ map momentum bodies
        (Body p v m) = b
        compensation = (-1/m) *. totalMomentum

initialState :: [Body]
initialState = offsetMomemtum [sun, jupiter, saturn, uranus, neptune]

sum' :: (Num a) => [a] -> a
sum' = foldl' (+) 0

energy :: [Body] -> Double
energy [] = 0
energy (b:bs) = sum' [ke, pe, energy bs]
          where ke = kineticEnergy b
                pe = sum' $ map (potentialEnergy b) bs

accelPairwise :: Double -> Body -> Body -> (Vector3, Vector3)
accelPairwise dt (Body p1 _ m1) (Body p2 _ m2) = (dv1, dv2)
  where diff = p1 .-. p2
        !dist2 = magnitude2 diff
        !mag = dt / (dist2 * (sqrt dist2))
        dv1 = ((-m2 * mag) *. diff)
        dv2 = ((m1 * mag) *. diff)

moveAlong :: Double -> Body -> Body
moveAlong dt (Body p1 v1 m1) = Body (p1 .+. (dt *. v1)) v1 m1

accelAll :: Double -> [Body] -> [Body]
accelAll _ [] = []
accelAll dt (b:bs) = newB : accelAll dt newBs
  where dvs = map (accelPairwise dt b) bs
        newB = foldl' (\(Body p v m) (dv, _) -> Body p (v .+. dv) m) b dvs
        newBs = zipWith (\(Body p v m) (_, dv) -> Body p (v .+. dv) m) bs dvs

strictList :: [a] -> [a]
strictList xs = foldr seq xs xs

delta_t :: Double
delta_t = 0.01

advance :: [Body] -> [Body]
advance = strictList . map (moveAlong delta_t) . accelAll delta_t

iterate' :: (a -> a) -> a -> [a]
iterate' f a = a : (iterate' f $! f a)

main :: IO ()
main = do n <- getArgs >>= readIO . head
          printf "%.9f\n" $ energy initialState
          printf "%.9f\n" $ energy $ iterate' advance initialState !! n
