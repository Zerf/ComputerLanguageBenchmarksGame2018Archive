# The Great Computer Language Shootout
#   http://shootout.alioth.debian.org/
#
#   contributed by Pilho Kim
#   and modified from pidigits.py
#
#   Using the Gmpy module
#   http://code.google.com/p/gmpy/downloads/list

import gmpy as _g
import sys
from itertools import *

def gen_x():
    return imap(lambda k: (_g.mpz(k), 4*_g.mpz(k) + 2, _g.mpz(0), 2*_g.mpz(k) + 1), count(1))

def compose((aq, ar, as_, at), (bq, br, bs, bt)):
    return (aq * bq,
            aq * br + ar * bt,
            as_ * bq + at * bs,
            as_ * br + at * bt)

def extract((q, r, s, t), j):
    return (q*j + r) // (s*j + t)

def pi_digits():
    z = (_g.mpz(1), _g.mpz(0), _g.mpz(0), _g.mpz(1))
    x = gen_x()
    while 1:
        y = extract(z, _g.mpz(3))
        while y != extract(z, _g.mpz(4)):
            z = compose(z, x.next())
            y = extract(z, _g.mpz(3))
        z = compose((_g.mpz(10), -10*y, _g.mpz(0), _g.mpz(1)), z)
        yield y

def main():
    n = int(sys.argv[1])
    digits = pi_digits()
    width = 10
    for i in xrange(width, n+1, width):
        print "%s\t:%d" % ("".join(imap(str, islice(digits, width))), i)
    if n % width > 0:
        print "%s\t:%d" % ("".join(imap(str, islice(digits, n % width))).ljust(width), n)


main()
