"* The Computer Language Benchmarks Game
    http://shootout.alioth.debian.org/
    adapted from a program by Paolo Bonzini
    contributed by Isaac Gouy 
    adapted further by Carlo Teixeira *"!


Object subclass: #Tests
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Shootout'!

Tests class
	instanceVariableNames: ''!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Tests class methodsFor: 'platform'!

arg
   ^CEnvironment commandLine last asNumber!

stdin
   ^Stdin!

stdinSpecial
   ^ExternalReadStream on:
      (ExternalConnection ioAccessor: (UnixDiskFileAccessor new handle: 0))!

stdout
"   ^Stdout"
^Transcript.!

stdoutSpecial
   ^ExternalWriteStream on:
      (ExternalConnection ioAccessor: (UnixDiskFileAccessor new handle: 1))! !

!Tests class methodsFor: 'benchmarking'!

threadring
   | done |
   (self threadRing: (done := Semaphore new)) takeToken: self arg.
   done wait.
   ^''!

threadRing: aSemaphore
   | first last |
   503 to: 1 by: -1 do: [:i|
      first := Thread named: i next: first done: aSemaphore.
      last isNil ifTrue: [ last:=first. ].
   ].
   last nextThread: first.
   ^first!

threadring: arg
   | done |
   (self threadRing: (done := Semaphore new)) takeToken: arg.
   done wait.
   ^''! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


Object subclass: #Thread
	instanceVariableNames: 'name nextThread token semaphore done '
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BenchmarksGame'!

Thread class
	instanceVariableNames: ''!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Thread class methodsFor: 'instance creation'!

named: anInteger next: aThread done: aSemaphore
   ^self new name: anInteger; nextThread: aThread; done: aSemaphore; fork !

new
   ^self basicNew semaphore: Semaphore new ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Thread methodsFor: 'accessing'!

done: aSemaphore
   done := aSemaphore !

fork
   [ self run ] forkAt: Processor userBackgroundPriority.!

name: anInteger
   name := anInteger !

nextThread: aThread
   nextThread := aThread !

run
	
	[semaphore wait.
	0==token] whileFalse: [nextThread takeToken: token - 1].
	name printOn: Tests stdout.
	Tests stdout cr.
	done signal!

semaphore: aSemaphore
   semaphore := aSemaphore !

takeToken: x
   token := x.
   semaphore signal ! !
