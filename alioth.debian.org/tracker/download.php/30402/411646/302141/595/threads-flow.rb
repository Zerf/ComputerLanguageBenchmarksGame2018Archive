#!/usr/bin/ruby
# The Great Computer Language Shootout
#   http://shootout.alioth.debian.org/
# 
# Usage: threads-flow [iterations] [threads]
#
# contributed by - Joe Nall

require "thread"

iterations = Integer(ARGV.shift || 150)
thread_count=Integer(ARGV.shift || 3000)

tail_q = in_q = Queue.new
out_q = nil
thread_count.times do |i|
    out_q = in_q
    in_q = Queue.new
    Thread.new(in_q,out_q) do |input,output|
         while TRUE
             output.push(input.pop + 1)
         end
    end
end

sum=0
iterations.times do
    in_q.push(0)
    sum += tail_q.pop()
end
   
print sum, "\n"
