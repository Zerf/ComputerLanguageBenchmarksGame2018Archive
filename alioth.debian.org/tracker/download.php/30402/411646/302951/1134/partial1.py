# The Computer Language Shootout
# http://shootout.alioth.debian.org/
# Readable version by bearophile, Jan 27 2006

from math import sin, cos
from sys import argv

def main_function(n, cos=cos, sin=sin):
    # The cos=cos, sin=sin speeds up the lookup a little
    tot = 0.0
    twoThirds = 2.0/3.0
    for k in range(n): tot += twoThirds ** k
    print "%.9f\t(2/3)^k" % tot

    tot = 0.0
    for k in range(1, n+1): tot += k ** -0.5
    print "%.9f\tk^-0.5" % tot

    tot = 0.0
    for k in range(1, n+1): tot += 1.0 / (k*(k+1.0))
    print "%.9f\t1/k(k+1)" % tot

    tot = 0.0
    for k in range(1, n+1):
        sk = sin(k)
        tot += 1.0 / (k*k*k * sk*sk)
    print "%.9f\tFlint Hills" % tot

    tot = 0.0
    for k in range(1, n+1):
        ck = cos(k)
        tot += 1.0 / (k*k*k * ck*ck)
    print "%.9f\tCookson Hills" % tot

    tot = 0.0
    for k in range(1, n+1): tot += 1.0/k
    print "%.9f\tHarmonic" % tot

    tot = 0.0
    for k in range(1, n+1): tot += 1.0/(k*k)
    print "%.9f\tRiemann Zeta" % tot

    tot = 0.0
    for k in range(1, n, 2): tot += 1.0/k
    for k in range(2, n+1, 2): tot -= 1.0/k
    print "%.9f\tAlternating Harmonic" % tot

    tot = 0.0
    for k in range(1, 2*n, 4): tot += 1.0/k
    for k in range(3, 2*n+1, 4): tot -= 1.0/k
    print "%.9f\tGregory" % tot

import psyco; psyco.bind(main_function)
main_function( int(argv[1]) )