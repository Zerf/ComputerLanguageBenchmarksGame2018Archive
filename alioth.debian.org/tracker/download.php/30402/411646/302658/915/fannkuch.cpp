/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   converted to C++ from D by Rafal Rusin
   compile: g++ -o fannkuch fannkuch.cpp
*/

#include <iostream>
#include <string>
#include <memory>
using namespace std;

template <typename T>
class auto_array {
    private:
    T * array;
    public:
    auto_array(T *array): array(array) {}
    inline T &operator[](int i) const {
	return array[i];
    }
    ~auto_array() {
	delete[] array;
    }
};

int fannkuch(int n);

int main(int argc, char *argv[])
{
    int n = argc > 1 ? atoi(argv[1]) : 1;
    cout << "Pfannkuchen(" << n << ") = " << fannkuch(n);
    return 0;
}

int fannkuch(int n)
{
    auto_array<int> perm(new int[n]), perm1(new int[n]), count(new int[n]);
    int gi, j, k, temp, flips, m = n - 1, r = n, maxFlipsCount = 0;
    int check = 0;

    for (int i = 0; i < n; i++) perm1[i] = i;
    for(;;)
    {
       if (check < 30)
       {
            for (int i = 0; i < n; i++) cout << perm1[i]+1;
	    cout << endl;
            check++;
        }

        while(r != 1) { count[r-1] = r; r--; }
        if(!(perm1[0] == 0 || perm1[m] == m))
        {
            for (int i = 0; i < n; i++) perm[i] = perm1[i];

            gi = perm[0];
            flips = 0;
            while(gi)
            {
                temp = perm[gi];
                perm[gi] = gi;
                gi = temp;
                for(j = 1, k = gi - 1; j < k; j++, k--)
                {
                    temp = perm[j];
                    perm[j] = perm[k];
                    perm[k] = temp;
                }
                flips++;
            }

            if(flips > maxFlipsCount) maxFlipsCount = flips;
        }
        for(;;)
        {
            if(r == n) return(maxFlipsCount);
            temp = perm1[0];
            for(gi = 0; gi < r;)
            {
                j = gi + 1;
                perm1[gi] = perm1[j];
                gi = j;
            }
            perm1[r] = temp;

            count[r]--;
            if(count[r] > 0) break;
            r++;
        }
    }
}
