;; The Great Computer Language Shootout
;;   http://shootout.alioth.debian.org/
;;   contributed by James McIlree
;;   Modified by Juho Snellman 2005-11-19
;;     * About 50% speedup on both SBCL and CMUCL
;;     * Use SIMPLE-BASE-STRINGs instead of (SIMPLE-ARRAY CHARACTER) for
;;       lower memory use on Unicode SBCL
;;     * Declare the type of SEQUENCE in ACCUMULATE-FREQUENCIES
;;   Modified by Robert Brown 2006-10-09
;;     * Program runs twice as fast.
;;     * Use an SBCL feature to specify equality and hash functions
;;       when creating hash tables for sequence fragments.

(defun read-data (stream)
  (let ((data (make-array 0
                          :element-type 'base-char
                          :adjustable t
                          :fill-pointer 0)))
    (do ((line (read-line stream nil 'eof) (read-line stream nil 'eof)))
        ((or (eq line 'eof) (string= ">THREE" line :start2 0 :end2 6))))
    (do ((line (read-line stream nil 'eof) (read-line stream nil 'eof)))
        ((or (eq line 'eof) (char= (schar line 0) #\>)))
      (if (not (char= (schar line 0) #\;))
          (dotimes (char-index (length line))
            (vector-push-extend
             (char-upcase (the base-char (schar line char-index))) data))))
    (coerce data 'simple-base-string)))

(defun make-fragment-hash-table (sequence fragment-length)
  (declare (type (integer 0 20) fragment-length)
           (type (simple-base-string) sequence))
  (labels ((fragment-equal (fragment1 fragment2)
             (declare (type fixnum fragment1 fragment2))
             (loop repeat fragment-length
                   for i of-type fixnum upfrom fragment1
                   for j of-type fixnum upfrom fragment2
                   do (when (not (char= (aref sequence i) (aref sequence j)))
                        (return-from fragment-equal nil)))
             t)
           (hash-fragment (fragment)
             (macrolet ((set-result (form)
                          `(setf result (ldb (byte 32 0) ,form))))
               (let ((result 0))
                 (declare (type (unsigned-byte 32) result))
                 (loop repeat fragment-length
                       for i of-type fixnum upfrom fragment
                       do (set-result (+ result (char-code (aref sequence i))))
                          (set-result (+ result (ash result 10)))
                          (set-result (logxor result (ash result -6))))
                 (set-result (+ result (ash result 3)))
                 (set-result (logxor result (ash result -11)))
                 (set-result (logxor result (ash result 15)))
                 (logand result most-positive-fixnum)))))
    (sb-int:define-hash-table-test 'fragment #'fragment-equal #'hash-fragment)
    (make-hash-table :test 'fragment)))

(defun calculate-frequencies (sequence fragment-length)
  (declare (type (simple-base-string) sequence)
           (type (integer 0 20) fragment-length))
  (let ((hash (make-fragment-hash-table sequence fragment-length)))
    (dotimes (fragment (- (length sequence) fragment-length))
      (setf (gethash fragment hash)
            (1+ (the (integer 0 1000000) (gethash fragment hash 0)))))
    hash))

(defun write-frequencies (sequence length)
  (declare (type (simple-base-string) sequence))
  (labels ((compare-fragments (fragment1 fragment2)
             (string< (subseq sequence fragment1 (+ fragment1 length))
                      (subseq sequence fragment2 (+ fragment2 length))))
           (compare-fragment-counts (fc1 fc2)
             (let ((count1 (cdr fc1))
                   (count2 (cdr fc2)))
               (cond ((> count1 count2) t)
                     ((< count1 count2) nil)
                     (t (compare-fragments (car fc1) (car fc2)))))))
    (let ((frequencies (calculate-frequencies sequence length)))
      (multiple-value-bind (sum fragment-counts)
          (loop for fragment being the hash-key of frequencies
                using (hash-value count)
                sum count into total-count
                collect (cons fragment count) into fragment-counts
                finally (return (values total-count fragment-counts)))
        (let ((sorted (sort fragment-counts #'compare-fragment-counts)))
          (dolist (fragment-count sorted)
            (let ((fragment (car fragment-count))
                  (count (cdr fragment-count)))
              (format t "~A ~,3F~%"
                      (subseq sequence fragment (+ fragment length))
                      (* (/ count sum) 100.0))))))))
  (terpri))

(defun write-frequency (sequence entry)
  (declare (type (simple-base-string) sequence))
  (let* ((simple-entry (coerce entry 'simple-base-string))
         (fragment (search simple-entry sequence))
         (length (length simple-entry)))
    (format t "~A~C~A~%"
            (gethash fragment (calculate-frequencies sequence length) 0)
            #\Tab
            entry)))

(defun main ()
  (let ((sequence (read-data *standard-input*)))
    (declare (type (simple-base-string) sequence))
    (write-frequencies sequence 1)
    (write-frequencies sequence 2)
    (dolist (entry '("GGT" "GGTA" "GGTATT" "GGTATTTTAATT"
                     "GGTATTTTAATTTATAGT"))
      (write-frequency sequence entry))))
