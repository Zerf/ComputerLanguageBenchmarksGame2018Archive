;;; The Computer Language Shootout
;;; http://shootout.alioth.debian.org/
;;;
;;; Contributed by: Shyamal Prasad
;;; Modifed by: Wade Humeniuk

(asdf:operate 'asdf:load-op 'cl-ppcre)

(defparameter *regex-list*
  (list
   "agggtaaa|tttaccct"
   "[cgt]gggtaaa|tttaccc[acg]"
   "a[act]ggtaaa|tttacc[agt]t"
   "ag[act]gtaaa|tttac[agt]ct"
   "agg[act]taaa|ttta[agt]cct"
   "aggg[acg]aaa|ttt[cgt]ccct"
   "agggt[cgt]aa|tt[acg]accct"
   "agggta[cgt]a|t[acg]taccct"
   "agggtaa[cgt]|[acg]ttaccct"))

(defparameter *scanner-list*
  (mapcar #'cl-ppcre:create-scanner 
	  (list
	   "agggtaaa|tttaccct"
	   "[cgt]gggtaaa|tttaccc[acg]"
	   "a[act]ggtaaa|tttacc[agt]t"
	   "ag[act]gtaaa|tttac[agt]ct"
	   "agg[act]taaa|ttta[agt]cct"
	   "aggg[acg]aaa|ttt[cgt]ccct"
	   "agggt[cgt]aa|tt[acg]accct"
	   "agggta[cgt]a|t[acg]taccct"
	   "agggtaa[cgt]|[acg]ttaccct")))

(defparameter *alternatives*
  (mapcar (lambda (pair)
	    (list (cl-ppcre:create-scanner (first pair))
		  (second pair)))
	  '(("B" "(c|g|t)")  ("D" "(a|g|t)")
	    ("H" "(a|c|t)")  ("K" "(g|t)")
	    ("M" "(a|c)")    ("N" "(a|c|g|t)")
	    ("R" "(a|g)")    ("S" "(c|t)")
	    ("V" "(a|c|g)")  ("W" "(a|t)")
	    ("Y" "(c|t)"))))


;; Read in the entire file as the benchmark specifieds
(defun get-input-chars (stream)
  (declare (optimize safety (speed 0)))
  (with-output-to-string (output)
    (loop while
	  (multiple-value-bind (line missing)
	      (read-line stream nil)
	    (when line (write-string line output))
	    (unless missing (write-char #\Newline output))
	    line))))



(defun main (&optional (stream *standard-input*))
  (declare (optimize safety (speed 1)))
  (let*
      ;; Benchmark definition requires using a regex to
      ;; remove headers/newlines from the file
      ((text (get-input-chars stream))
       (sequence (cl-ppcre:regex-replace-all ">[^\\n]*\\n|\\n" text "")))

    (declare (string text sequence))

    ;; Count and print the number of subsequences
    (loop for regex in *regex-list*
	  for scanner in *scanner-list* do
	  (format t "~a ~a~%" regex
		  (let ((sum 0))
		    (declare (fixnum sum))
		    (cl-ppcre:do-matches (s e scanner sequence)
		      (declare (fixnum s e))
		      (incf sum))
		    sum)))

    ;; Print lengths
    (format t "~%~a~%" (length text))
    (format t "~a~%" (length sequence))

    ;; do the alternative substitution and create the new text string
    ;; that the benchmark definition requires
    (loop for (regex new) in *alternatives* do
	  (setf sequence
		(cl-ppcre:regex-replace-all regex sequence new)))
    (format t "~a~%" (length sequence))))