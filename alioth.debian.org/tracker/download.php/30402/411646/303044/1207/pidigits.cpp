/*
 * -- The Computer Language Shootout
 *  -- http://shootout.alioth.debian.org/
 *  --
 *  -- contributed by Qingyan Liu (hmisty<at>gmail.com)
 *  --
 *  -- Prerequisite: GNU MP Library (gmp-4.1.4)
 */
#include <cstdlib>
#include <iostream>
#include <string>
#include <gmp.h>

using namespace std;

class BigInt {
	public:
		static const int BASE = 10; // 10-based

		BigInt() { mpz_init(big);	}
		BigInt(const string s) { mpz_init_set_str(big, s.c_str(), BASE); }
		BigInt(const signed long int si) { mpz_init_set_si(big, si); }

		friend BigInt operator+(const BigInt lhs, const BigInt rhs) {
			BigInt result;
			mpz_add(result.big, lhs.big, rhs.big);
			return result;
		}

		friend BigInt operator-(const BigInt lhs, const BigInt rhs) {
			BigInt result;
			mpz_sub(result.big, lhs.big, rhs.big);
			return result;
		}
		
		friend BigInt operator*(const BigInt lhs, const BigInt rhs) {
			BigInt result;
			mpz_mul(result.big, lhs.big, rhs.big);
			return result;
		}

		friend BigInt operator/(const BigInt lhs, const BigInt rhs) {
			BigInt result;
			// lhs.big = q * rhs.big + r
			mpz_tdiv_q(result.big, lhs.big, rhs.big);
			return result;
		}
		
		friend BigInt operator%(const BigInt lhs, const BigInt rhs) {
			BigInt result;
			mpz_tdiv_r(result.big, lhs.big, rhs.big);
			return result;
		}

		friend bool operator==(const BigInt lhs, const BigInt rhs) {
			return mpz_cmp(lhs.big, rhs.big) == 0;
		}
		
		friend ostream& operator<<(ostream& os, const BigInt rhs) {
			char *str = mpz_get_str(NULL, BASE, rhs.big);
			string s(str);
			free(str);
			os << s;
			return os;
		}
		
	private:
		mpz_t big;
};

template <class T>
class Transformation {
	public:
		Transformation(int q, int r, int s, int t):q(q), r(r), s(s), t(t), k(0) {}
		Transformation(T q, T r, T s, T t):q(q), r(r), s(s), t(t), k(0) {}
		~Transformation() {}

		Transformation<T> next() {
			k = k + T(1);
			q = k;
			r = k * T(4) + T(2);
			s = T(0);
			t = k * T(2) + T(1);
			return *this;
		}

		T extract(int j) {
			int bigj = j;
			T numerator = q * bigj + r;
			T denominator = s * bigj + t;
			return numerator / denominator;
		}

		Transformation<T> qrst(T q, T r, T s, T t) {
			this->q = q;
			this->r = r;
			this->s = s;
			this->t = t;
			this->k = T(0);
			return *this;
		}

		Transformation<T> compose(const Transformation<T> a) {
			Transformation<T> tr(
				q * a.q,
				q * a.r + r * a.t,
				s * a.q + t * a.s,
				s * a.r + t * a.t
			);
			return tr;
		}

	private:
		T q, r, s, t;
		T k;
};

template <class T>
class PiDigitSpigot {
	public:
		PiDigitSpigot():z(1,0,0,1), x(0,0,0,0), inverse(0,0,0,0) {}
		~PiDigitSpigot() {}

		T next() {
			T y = digit();
			if (is_safe(y)) {
				z = produce(y);
				return y;
			} else {
				z = consume(x.next());
				return next();
			}
		}

		T digit() {
			return z.extract(3);
		}

		bool is_safe(T digit) {
			return (digit == z.extract(4));
		}

		Transformation<T> produce(T i) {
			return ((inverse.qrst(T(10), i * T(-10), T(0), T(1))).compose(z));
		}

		Transformation<T> consume(const Transformation<T> a) {
			return z.compose(a);
		}

	private:
		Transformation<T> z, x, inverse;
};	

int main(int argc, char ** argv) {
	enum { FOO = 27 };
	int n = argc == 1 ? FOO : atoi(argv[1]);

	PiDigitSpigot<BigInt> digits;

	for (int i = 1; i <= n; i++) {
		cout << digits.next();
		if (i % 10 == 0)
			cout << "\t:" << i << endl;
	}
	if (n % 10 != 0) {
		for (int j = n % 10; j <= 10; j++)
			cout << ' ';
		cout << "\t:" << n << endl;
	}
}

