#!/usr/bin/env perl
# $$Id: random_pureperl.pl,v 1.2 2006/01/18 12:21:52 kforner Exp $
#  The Computer Language Shootout
#    http://shootout.alioth.debian.org/
# BENCHMARK: random
#   contributed by Karl FORNER

my ($IM,$IA,$IC,$LAST) = (139968,3877,29573,42);

sub gen_random { 
  my ($n,$max) = @_;
  $LAST = ($LAST * $IA + $IC) % $IM while ($n-- );
  return $max * $LAST / $IM;
}

printf "%.9f\n", gen_random($ARGV[0] || 1, 100.0);
