#!/usr/bin/perl

# This version uses the FastPermute module available on CPAN
# to run through the permutations for better performance.
# Contributed by Ben Sharp.

use Algorithm::FastPermute ('permute');

my (@p, $maxFlip);
push @p, $_ for 1..$ARGV[0];

permute {
    {
        my @q = @p;
	my $flip;

        while ($q[0] > 1) {
            $flip++;
            unshift @q, reverse splice @q, 0, $q[0];
        }
        $maxFlip = $flip if $flip > $maxFlip;
    }
} @p;

print "Pfannkuchen($ARGV[0]) = $maxFlip\n";
