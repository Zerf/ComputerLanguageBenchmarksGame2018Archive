// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org/
// Java version by Stefan Krause
// Adapted from the Java version by bearophile

import std.stdio, std.conv;

void main(char[][] args) {
    const double LIMITSQUARED = 4.0;
    const int ITERATIONS = 50;

    int size = args.length > 1 ? toInt(args[1]) : 10;
    real fac = 2.0 / size; // can't this be a double?
    int offset = size % 8;
    int shift = (offset == 0 ? 0 : (8 - offset));

    writefln("P4\n%d %d", size, size);

    for (int y = 0; y < size; y++) {
        ubyte bits = 0;
        double Ci = y * fac - 1.0;

        for (int x = 0; x < size; x++) {
            int i = ITERATIONS;
            double Zr = 0, Zi = 0, ZrN = 0, ZiN = 0;
            double Cr = x * fac - 1.5;

            do {
                Zi = 2.0 * Zr * Zi + Ci;
                Zr = ZrN - ZiN + Cr;
                ZiN = Zi * Zi;
                ZrN = Zr * Zr;
            } while (!(ZiN + ZrN > LIMITSQUARED) && --i);

            bits <<= 1;
            if (!i)
                bits++;

            if (x % 8 == 7) {
                putchar(bits);
                bits = 0;
            }
        }

        if (shift) {
            bits <<= shift;
            putchar(bits);
        }
    }
}