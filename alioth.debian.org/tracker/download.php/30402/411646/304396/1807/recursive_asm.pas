program recursive;

{$I-}
{$asmmode intel}

function ack(x,y:longint):longint;assembler;nostackframe;

asm
   or eax,eax
   jz @xzero
   or edx,edx
   jz @yzero
   dec edx
   push eax
   call ack
   mov edx,eax
   pop eax
   dec eax
{
   call ack
   ret
}
   jmp ack
@xzero:
   lea eax,[edx+1]
   ret
@yzero:
   dec eax
   mov edx,1
   call ack
end;

function fib(n:cardinal):cardinal;assembler;nostackframe;

asm
  cmp eax,2
  jge @norm
  mov eax,1
  ret
@norm:
  dec eax
  push eax
  call fib
  mov edx,eax
  pop eax
  push edx
  dec eax
  call fib
  pop edx
  add eax,edx
end;

const c1:double=1;
      c2:double=2;

procedure do_fibfp;assembler;nostackframe;

{FPC's calling convention pass floating point parameters on the FPU stack.
 Therefore, for speed we cannot use the Pascal parameter mechanism.}

asm
  comisd xmm0,c2
  jae @norm
  movsd xmm0,c1
  ret
@norm:
  subsd   xmm0,c1
  sub esp,8
  movsd [esp],xmm0
  call do_fibfp
  movsd xmm1,xmm0
  movsd xmm0,[esp]
  movsd [esp],xmm1
  subsd xmm0,c1
  call do_fibfp
  movsd xmm1,[esp]
  addsd xmm0,xmm1
  add esp,8
end;

function fibfp(n:double):double;

begin
  asm
    movsd xmm0,n
    call do_fibfp
    movsd @result,xmm0
  end;
end;

function tak(x,y,z:longint):longint;assembler;nostackframe;

asm
  cmp edx,eax
  jl @norm
  mov eax,ecx
  ret
@norm:
  sub esp,12         {Reserve some local storage.}
  mov [esp+0+0],eax
  mov [esp+0+4],edx
  mov [esp+0+8],ecx
  dec eax
  call tak
  push eax
  mov eax,[esp+4+4]  {Add 4 to offsets to compensate for push.}
  mov edx,[esp+4+8]
  mov ecx,[esp+4+0]
  dec eax
  call tak
  push eax
  mov eax,[esp+8+8]  {Add 8 to offsets to compensate for push.}
  mov edx,[esp+8+0]
  mov ecx,[esp+8+4]
  dec eax
  call tak
  mov ecx,eax
  pop edx
  pop eax
  call tak
  add esp,12
end;

procedure do_takfp;assembler;nostackframe;

{FPC's calling convention pass floating point parameters on the FPU stack.
 Therefore, for speed we cannot use the Pascal parameter mechanism.}

asm
  comisd xmm1,xmm0
  jb @norm
  movsd xmm0,xmm2
  ret
@norm:
  sub esp,40             {Reserve some local storage.}
  movsd [esp+0],xmm0
  movsd [esp+8],xmm1
  movsd [esp+16],xmm2
  subsd xmm0,c1
  call do_takfp
  movsd [esp+24],xmm0
  movsd xmm0,[esp+8]   {Add 4 to offsets to compensate for push.}
  movsd xmm1,[esp+16]
  movsd xmm2,[esp+0]
  subsd xmm0,c1
  call do_takfp
  movsd [esp+32],xmm0
  movsd xmm0,[esp+16] {Add 8 to offsets to compensate for push.}
  movsd xmm1,[esp+0]
  movsd xmm2,[esp+8]
  subsd xmm0,c1
  call do_takfp
  movsd xmm2,xmm0
  movsd xmm1,[esp+32]
  movsd xmm0,[esp+24]
  add esp,40
  call tak
end;

function takfp(x,y,z:double):double;

begin
  asm
    movsd xmm0,x
    movsd xmm1,y
    movsd xmm2,z
    call do_takfp
    movsd @result,xmm0
  end;
end;

var n:longint;

begin
   if ParamCount = 1 then begin
      Val(ParamStr(1), n);
      n := n - 1;
   end
      else n := 2;

   writeLn('Ack(3,', n + 1, '): ', Ack(3, n+1));
   writeLn('Fib(', (28.0 + n):1:1, '): ', FibFP(28.0 + n):1:1);
   writeLn('Tak(', 3 * n,',', 2 * n, ',', n, '): ', Tak(3*n, 2*n, n));
   writeLn('Fib(3): ', Fib(3));
   writeLn('Tak(3.0,2.0,1.0): ', TakFP(3.0,2.0,1.0):1:1);
end.
