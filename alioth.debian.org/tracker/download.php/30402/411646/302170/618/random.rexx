#!/usr/bin/regina -a

/* ------------------------------------------------------------------ */
/* The Great Computer Language Shootout                               */
/* http://shootout.alioth.debian.org/                                 */
/*                                                                    */
/* % regina -v                                                        */
/* REXX-Regina_3.3(MT) 5.00 25 Apr 2004                               */
/* % uname -orvmp                                                     */
/* 2.6.5-1.358 #1 Wed Oct 13 17:49:34 EST 2004 i686 i686 GNU/Linux    */
/*                                                                    */
/* Contributed by Anthony Borla                                       */
/* ------------------------------------------------------------------ */

trace 'OFF' ; numeric digits 11
IA = 3877 ; IC = 29573 ; IM = 139968 ; LAST = 42

N = ARG(1) ; if DATATYPE(N) \= 'NUM' | N < 1 then ; N = 1

do while N > 1
  call gen_random 100.0
  N = N - 1
end

say FORMAT(gen_random(100.0), , 9)

exit 0

/* ----------------------------- */

gen_random :
  LAST = (LAST * IA + IC) // IM
  return ARG(1) * LAST / IM
