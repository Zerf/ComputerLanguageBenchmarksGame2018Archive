/*	The Computer Language Benchmarks Game
	http://shootout.alioth.debian.org/

	copy & paste from java version by The Anh Tran
*/

using System;
using System.Threading;
using System.Text;

public class chame
{
    internal enum Color
    {
        blue,
        red,
        yellow,
        invalid
    }
    
    private static Color ColorCompliment (Color c1, Color c2)
    {
        switch (c1)
        {
            case Color.blue:
                switch (c2)
                {
                    case Color.blue:
                        return Color.blue;
                    case Color.red:
                        return Color.yellow;
                    case Color.yellow:
                        return Color.red;
                }
                break;
            case Color.red:
                switch (c2)
                {
                    case Color.blue:
                        return Color.yellow;
                    case Color.red:
                        return Color.red;
                    case Color.yellow:
                        return Color.blue;
                }
                break;
            case Color.yellow:
                switch (c2)
                {
                    case Color.blue:
                        return Color.red;
                    case Color.red:
                        return Color.blue;
                    case Color.yellow:
                        return Color.yellow;
                }
                break;
        }
        return Color.invalid;
    }
    
    private static String[] NUMBERS =
    {
        "zero", "one", "two", "three", "four", "five",
        "six", "seven", "eight", "nine"
    };
    
    private static String SpellNumber (int n)
    {
        StringBuilder sb = new StringBuilder ();
        string nStr = n.ToString();
        
        foreach (char c in nStr)
        {
            sb.Append (" ");
            sb.Append ( NUMBERS[c - '0'] );
        }
        
        return sb.ToString();
    }

    
    internal class MeetingPlace
    {
        public int meetings_left;
        private Creature waiting_creature;

        
        public MeetingPlace (int meetings)
        {
            meetings_left = meetings;
        }
        
        public Creature meet_Other (Creature current_creature)
        {
            lock (this)
            {
                if (meetings_left > 0)
                {
                    if (waiting_creature == null)  // first arrive
                    {
                        waiting_creature = current_creature;
                        return null;
                    }
                    else // second arrive
                    {
                        // store ref to first creature
                        Creature first_creature = waiting_creature;
                        waiting_creature = null;

                        meetings_left--;
                        return first_creature;
                    }
                }
                return null;
            }            
        }
    }
    
    internal class Creature
    {
        private MeetingPlace place;
        
        public int count;// = 0;
        public int sameCount;// = 0;
        private bool met;// = false;
        
        private Color color;
        private long id;
        
        public Creature (MeetingPlace place, Color c)
        {
            this.place = place;
            color = c;
        }
        
        public void run ()
        {
            id = GetHashCode();

            while (place.meetings_left > 0)
            {
                met = false;
                Creature other = place.meet_Other(this);
                
                if (other != null)
                {
                    this.goto_meet(other);
                }
                else
                {
                    while (met == false)
                        Thread.Sleep(0);
                }
            }
        }
        
        private void goto_meet(Creature other)
        {
            Color newcolor = ColorCompliment (this.color, other.color);
            this.color = other.color = newcolor;
            
            this.count++;
            other.count++;
            
            if (this.id == other.id)
            {
                this.sameCount++;
                other.sameCount++;
            }
            
            this.met = other.met = true;
        }
    }
    
    private static void runGame (int n, params Color[] colors)
    {
        MeetingPlace place = new MeetingPlace (n);
        
        Creature[] creatures = new Creature[colors.Length];
        Thread[] cr_threads = new Thread[colors.Length];

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < colors.Length; i++)
        {
            sb.Append(String.Format(" {0}", colors[i]));

            creatures[i] = new Creature(place, colors[i]);
            
            cr_threads[i] = new Thread(creatures[i].run);
            cr_threads[i].Start();
        }
        
        sb.AppendLine();
        
        foreach (Thread t in cr_threads)
            t.Join();
        
        int total = 0;
        foreach (Creature creature in creatures)
        {
            sb.AppendLine(String.Format ("{0}{1}", creature.count, SpellNumber (creature.sameCount)));
            total += creature.count;
        }
        
        sb.AppendLine(String.Format ("{0}\n", SpellNumber (total)));
        Console.Out.Write(sb);
    }
    
    public static void Main (String[] args)
    {
        
        int n = 600;
        if (args.Length > 0)
            n = Int32.Parse (args[0]);
        
        
        printColors();
        Console.Out.WriteLine ();
        
        runGame (n, Color.blue, Color.red, Color.yellow);
        runGame (n, Color.blue, Color.red, Color.yellow, Color.red, Color.yellow,
                Color.blue, Color.red, Color.yellow, Color.red, Color.blue);
    }
    
    
    private static void printColors ()
    {
        Color[] c = { Color.blue, Color.red, Color.yellow };
        
        foreach (Color c1 in c)
        {
            foreach (Color c2 in c)
                Console.Out.WriteLine("{0} + {1} -> {2}", c1, c2, ColorCompliment (c1, c2));
        }
    }
}
