! The Computer Language Shootout Benchmarks
! http://shootout.alioth.debian.org/
!
! contributed by Steve Decker
! using portions of the MPFUN90 library by David H. Bailey, Yozo Hida, 
! Karthik Jeyabalan, Xiaoye S. Li, and Brandon Thompson.
! http://crd.lbl.gov/~dhbailey/mpdist/
! 
! compilation:
!    g95 -O3 -funroll-loops pidigits.f90
!    ifort -O -ip pidigits.f90

! MPFUN90 library
module mpfuna
  implicit none

  integer, parameter :: mpkdp = kind(0.d0)
  
  real(mpkdp), parameter :: mpbbx = 4096.d0, mpbdx = mpbbx ** 2,  &
                            mpbx2 = mpbdx ** 2, mprbx = 1.d0 / mpbbx,  &
                            mprdx = mprbx ** 2, mprx2 = mprdx ** 2
  integer,     parameter :: mpnbt = 24, mpnpr = 32

  integer                :: mpldb = 6, mpier = 99, mpird = 1
end module mpfuna

module mpfunc
  use mpfuna

contains

  subroutine mpadd (a, b, c, mpnw)

    double precision d(mpnw+5), db
    dimension a(mpnw+2), b(mpnw+2), c(mpnw+4)

    ia = sign (1., a(1))
    ib = sign (1., b(1))
    na = min (int (abs (a(1))), mpnw)
    nb = min (int (abs (b(1))), mpnw)

    if (na .eq. 0) then
       c(1) = sign (nb, ib)
       c(2:nb+2) = b(2:nb+2)
       return
    else if (nb .eq. 0) then
       c(1) = sign (na, ia)
       c(2:na+2) = a(2:na+2)
       return
    end if

    db = merge(1.d0, -1.d0, ia == ib)
    ixa = a(2)
    ixb = b(2)
    ish = ixa - ixb
    
    if (ish .ge. 0) then
       m1 = min (na, ish)
       m2 = min (na, nb + ish)
       m3 = na
       m4 = min (max (na, ish), mpnw + 1)
       m5 = min (max (na, nb + ish), mpnw + 1)

       d(1:2) = 0.d0
       d(3:m1+2) = a(3:m1+2)
       d(m1+3:m2+2) = a(m1+3:m2+2) + db * b(m1+3-ish:m2+2-ish)
       d(m2+3:m3+2) = a(m2+3:m3+2)
       d(m3+3:m4+2) = 0.d0
       d(m4+3:m5+2) = db * b(m4+3-ish:m5+2-ish)
       
       nd = m5
       ixd = ixa
       d(nd+3:nd+4) = 0.d0
    else
       nsh = - ish
       m1 = min (nb, nsh)
       m2 = min (nb, na + nsh)
       m3 = nb
       m4 = min (max (nb, nsh), mpnw + 1)
       m5 = min (max (nb, na + nsh), mpnw + 1)

       d(1:2) = 0.d0
       d(3:m1+2) = db * b(3:m1+2)
       d(m1+3:m2+2) = a(m1+3-nsh:m2+2-nsh) + db * b(m1+3:m2+2)
       d(m2+3:m3+2) = db * b(m2+3:m3+2)
       d(m3+3:m4+2) = 0.d0
       d(m4+3:m5+2) = a(m4+3-nsh:m5+2-nsh)
       
       nd = m5
       ixd = ixb
       d(nd+3:nd+4) = 0.d0
    end if

    d(1) = sign (nd, ia)
    d(2) = ixd
    call mpnorm (d, c, mpnw) 
  end subroutine mpadd

  subroutine mpcpr (a, b, ic, mpnw)

    dimension a(mpnw+4), b(mpnw+4)

    ia = merge(0., sign (1., a(1)), a(1) == 0.)
    ib = merge(0., sign (1., b(1)), b(1) == 0.)

    if (ia .ne. ib) then
       ic = sign (1, ia - ib)
    else
       ma = a(2)
       mb = b(2)
       if (ma .ne. mb) then
          ic = ia * sign (1, ma - mb)
       else
          na = min (int (abs (a(1))), mpnw)
          nb = min (int (abs (b(1))), mpnw)
    
          do i = 3, min (na, nb) + 2
             if (a(i) .ne. b(i)) then
                ic = ia * sign (1., a(i) - b(i))
                return
             end if
          end do

          ic = merge(0, ia * sign (1, na - nb), na == nb)
       end if
    end if
  end subroutine mpcpr

  subroutine mpdiv (a, b, c, mpnw)

    double precision d, rb, ss, t0, t1, t2
    dimension a(mpnw+2), b(mpnw+2), c(mpnw+4), d(mpnw+4)

    ia = sign (1., a(1))
    ib = sign (1., b(1))
    na = min (int (abs (a(1))), mpnw)
    nb = min (int (abs (b(1))), mpnw)

    if (na .eq. 0) then
       c(1:2) = 0.
       return
    end if

    if (nb .eq. 1 .and. b(3) .eq. 1.) then
       c(1) = sign (na, ia * ib)
       c(2) = a(2) - b(2)
       c(3:na+2) = a(3:na+2)
       return
    end if

    t0 = mpbdx * b(3)
    if (nb .ge. 2) t0 = t0 + b(4)
    if (nb .ge. 3) t0 = t0 + mprdx * b(5)
    if (nb .ge. 4) t0 = t0 + mprx2 * b(6)
    rb = 1.d0 / t0
    md = min (na + nb, mpnw)
    d(1) = 0.d0

    d(2:na+1) = a(3:na+2)
    d(na+2:md+4) = 0.d0

    do j = 2, na + 1
       t1 = mpbx2 * d(j-1) + mpbdx * d(j) + d(j+1) + mprdx * d(j+2)
       t0 = int (rb * t1)
       j3 = j - 3
       i2 = min (nb, mpnw + 2 - j3) + 2
       ij = i2 + j3
       
       do i = 3, i2
          i3 = i + j3
          d(i3) = d(i3) - t0 * b(i)
       end do

       if (mod (j - 1, mpnpr) .eq. 0) then
          do i = j + 1, ij
             t1 = d(i)
             t2 = int (mprdx * t1)
             d(i-1) = d(i-1) + t2
             d(i) = t1 - mpbdx * t2
          end do
       end if
       d(j) = d(j) + mpbdx * d(j-1)
       d(j-1) = t0
    end do

    do j = na + 2, mpnw + 3
       t1 = mpbx2 * d(j-1) + mpbdx * d(j) + d(j+1)
       if (j .le. mpnw + 2) t1 = t1 + mprdx * d(j+2)
       t0 = int (rb * t1)
       j3 = j - 3
       i2 = min (nb, mpnw + 2 - j3) + 2
       ij = i2 + j3
       ss = 0.d0
       
       do i = 3, i2
          i3 = i + j3
          d(i3) = d(i3) - t0 * b(i)
          ss = ss + abs (d(i3))
       end do

       if (mod (j - 1, mpnpr) .eq. 0) then
          do i = j + 1, ij
             t1 = d(i)
             t2 = int (mprdx * t1)
             d(i-1) = d(i-1) + t2
             d(i) = t1 - mpbdx * t2
          end do
       end if
       d(j) = d(j) + mpbdx * d(j-1)
       d(j-1) = t0
       if (ss .eq. 0.d0) goto 170
       if (ij .le. mpnw + 1) d(ij+3) = 0.d0
    end do

    j = mpnw + 3
    
170 d(j) = 0.d0
    is = merge(1, 2, d(1) == 0.d0)
    nc = min (j - 1, mpnw)
    d(nc+3:nc+4) = 0.d0
    
    d(j+1:3:-1) = d(j+1-is:3-is:-1)
    
    d(1) = sign (nc, ia * ib)
    d(2) = a(2) - b(2) + is - 2
    
    call mpnorm (d, c, mpnw) 
  end subroutine mpdiv

  subroutine mpdmc (a, n, b)

    double precision a, aa
    dimension b(8)

    if (a .eq. 0.d0) then
       b(1:2) = 0.
    else
       n1 = n / mpnbt
       n2 = n - mpnbt * n1
       aa = abs (a) * 2.d0 ** n2

       if (aa .ge. mpbdx) then
          do k = 1, 100
             aa = mprdx * aa
             if (aa .lt. mpbdx) then
                n1 = n1 + k
                exit
             end if
          end do
       else if (aa .lt. 1.d0) then
          do k = 1, 100
             aa = mpbdx * aa
             if (aa .ge. 1.d0) then
                n1 = n1 - k
                exit
             end if
          end do
       end if

       b(2) = n1
       b(3) = aint (aa)
       aa = mpbdx * (aa - b(3))
       b(4) = aint (aa)
       aa = mpbdx * (aa - b(4))
       b(5) = aint (aa)
       aa = mpbdx * (aa - b(5))
       b(6) = aint (aa)
       b(7:8) = 0.
       
       do i = 6, 3, -1
          if (b(i) .ne. 0.) exit
       end do

       aa = i - 2
       b(1) = sign (aa, a)
    end if
  end subroutine mpdmc

  subroutine mpeq (a, b, mpnw)

    dimension a(mpnw+3), b(mpnw+3)

    ia = sign (1., a(1))
    na = min (int (abs (a(1))), mpnw)
    if (na .eq. 0)  then
       b(1:2) = 0.
    else
       b(1) = sign (na, ia)
       b(2:na+3) = a(2:na+3)
    end if
  end subroutine mpeq

  subroutine mpinfr (a, b, c, mpnw)

    dimension a(mpnw+2), b(mpnw+2), c(mpnw+2)

    ia = sign (1., a(1))
    na = min (int (abs (a(1))), mpnw)
    ma = a(2)
    if (na .eq. 0) then
       b(1:2) = 0.
       c(1:2) = 0.
    else
       nb = min (max (ma + 1, 0), na)
       if (nb .eq. 0) then
          b(1:2) = 0.
       else
          b(1) = sign (nb, ia)
          b(2) = ma
          b(3:nb+2) = a(3:nb+2)
          b(nb+3:nb+4) = 0.
       end if
       
       nc = na - nb
       if (nc .le. 0) then
          c(1:2) = 0.
       else
          c(1) = sign (nc, ia)
          c(2) = ma - nb
          c(3:nc+2) = a(3+nb:nc+2+nb)
          c(nc+3:nc+4) = 0.
       end if
       
       call mproun (b, mpnw) 
       call mproun (c, mpnw) 
    end if
  end subroutine mpinfr

  subroutine mpmdc (a, b, n)

    double precision aa, b
    dimension a(*)

    if (a(1) .eq. 0.)  then
       b = 0.d0
       n = 0
    else
       na = abs (a(1))
       aa = a(3)
       if (na .ge. 2) aa = aa + mprdx * a(4)
       if (na .ge. 3) aa = aa + mprx2 * a(5)
       if (na .ge. 4) aa = aa + mprdx * mprx2 * a(6)
       
       n = mpnbt * a(2)
       b = sign (aa, dble (a(1)))
    end if
  end subroutine mpmdc

  subroutine mpmul (a, b, c, mpnw)

    double precision d, t1, t2
    dimension a(mpnw+2), b(mpnw+2), c(mpnw+4), d(mpnw+4)
    
    ia = sign (1., a(1))
    ib = sign (1., b(1))
    na = min (int (abs (a(1))), mpnw)
    nb = min (int (abs (b(1))), mpnw)
    if (na .eq. 0 .or. nb .eq. 0) then
       c(1:2) = 0.
    else if (na .eq. 1 .and. a(3) .eq. 1.) then
       c(1) = sign (nb, ia * ib)
       c(2) = a(2) + b(2)
       c(3:nb+2) = b(3:nb+2)
    else if (nb .eq. 1 .and. b(3) .eq. 1.) then
       c(1) = sign (na, ia * ib)
       c(2) = a(2) + b(2)
       c(3:na+2) = a(3:na+2)
    else
       nc = min (na + nb, mpnw)
       d2 = a(2) + b(2)
       d(1:nc+4) = 0.d0
       
       do j = 3, na + 2
          t1 = a(j)
          j3 = j - 3
          n2 = min (nb + 2, mpnw + 4 - j3)
          d(3+j3:n2+j3) = d(3+j3:n2+j3) + t1 * b(3:n2)
          
          if (mod (j - 2, mpnpr) .eq. 0) then
             i1 = max (3, j - mpnpr)
             i2 = n2 + j3
             do i = i1, i2
                t1 = d(i)
                t2 = int (mprdx * t1)
                d(i-1) = d(i-1) + t2
                d(i) = t1 - mpbdx * t2
             end do
          end if
       end do
       
       if (d(2) .ne. 0.d0) then
          d2 = d2 + 1.
          d(nc+4:3:-1) = d(nc+3:2:-1)
       end if
       d(1) = sign (nc, ia * ib)
       d(2) = d2
       
       call mpnorm (d, c, mpnw)
    end if
  end subroutine mpmul

  subroutine mpmuld (a, b, n, c, mpnw)

    double precision b, bb, d
    dimension a(mpnw+2), c(mpnw+4), f(8), d(mpnw+4)
    
    ia = sign (1., a(1))
    na = min (int (abs (a(1))), mpnw)
    ib = sign (1.d0, b)
    if (na .eq. 0 .or. b .eq. 0.d0) then
       c(1:2) = 0.
    else
       n1 = n / mpnbt
       n2 = n - mpnbt * n1
       bb = abs (b) * 2.d0 ** n2
       
       if (bb .ge. mpbdx) then
          do k = 1, 100
             bb = mprdx * bb
             if (bb .lt. mpbdx) then
                n1 = n1 + k
                exit
             end if
          end do
       else if (bb .lt. 1.d0) then
          do k = 1, 100
             bb = mpbdx * bb
             if (bb .ge. 1.d0) then
                n1 = n1 - k
                exit
             end if
          end do
       end if
       
       if (bb .ne. aint (bb)) then
          bb = sign (bb, b)
          call mpdmc (bb, n1 * mpnbt, f)
          call mpmul (f, a, c, mpnw) 
       else
          d(1) = sign (na, ia * ib)
          d(2) = a(2) + n1
          d(3:na+2) = bb * a(3:na+2)
          d(na+3:na+4) = 0.d0
          call mpnorm (d, c, mpnw) 
       end if
    end if
  end subroutine mpmuld

  subroutine mpnorm (d, a, mpnw)

    double precision d, t1, t2, t3
    dimension d(mpnw+4), a(mpnw+4)
    
    ia = sign (1.d0, d(1))
    na = min (int (abs (d(1))), mpnw)
    if (na .eq. 0)  then
       a(1:2) = 0.
       return
    end if
    n4 = na + 4
    a2 = d(2)
    d(2) = 0.d0
    

    do
       t1 = 0.d0
       
       do i = n4, 3, -1
          t3 = t1 + d(i)
          t2 = mprdx * (t3)
          t1 = int (t2)
          if (t2 .lt. 0.d0 .and. t1 .ne. t2) t1 = t1 - 1.d0
          d(i) = t3 - t1 * mpbdx
       end do
       
       d(2) = d(2) + t1
       
       if (d(2) .lt. 0.d0) then
          ia = - ia
          d(3) = d(3) + mpbdx * d(2)
          d(2) = 0.d0
          d(3:n4) = -d(3:n4)
          cycle
       else if (d(2) .gt. 0.d0) then
          a(n4:3:-1) = d(n4-1:2:-1)
          na = min (na + 1, mpnw)
          a2 = a2 + 1.
       else
          a(3:n4) = d(3:n4)
       end if
       exit
    end do
    
    a(1) = sign (na, ia)
    a(2) = a2
    
    call mproun (a, mpnw) 
  end subroutine mpnorm

  subroutine mproun (a, mpnw)

    parameter (amx = 2.e6)
    dimension a(mpnw+4)
    
    a2 = a(2)
    a(2) = 0.
    ia = sign (1., a(1))
    na = min (int (abs (a(1))), mpnw)
    n4 = na + 4
    if (a(3) .eq. 0.) then

       do i = 4, n4
          if (a(i) .ne. 0.) exit
       enddo
    
       if (i == n4 .and. a(n4) == 0.) then
          a(1:2) = 0.
          return
       end if
       
       k = i - 3

       do i = 3, n4 - k
          a(i) = a(i+k)
       end do
       
       a2 = a2 - k
       na = na - max (k - 2, 0)
       if (k .eq. 2) a(na+3) = 0.
    end if

    if (na .eq. mpnw .and. mpird .ge. 1) then
       if (mpird .eq. 1 .and. a(na+3) .ge. 0.5d0 * mpbdx .or. mpird .eq. 2  &
            .and. a(na+3) .ge. 1.) a(na+2) = a(na+2) + 1.

       do i = na + 2, 3, -1
          if (a(i) .lt. mpbdx) exit
          a(i) = a(i) - mpbdx
          a(i-1) = a(i-1) + 1.
       end do

       if (i == 3 .and. a(3) >= mpbdx) then
          a(3) = a(2)
          na = 1
          a2 = a2 + 1.
       end if
    end if
    
    if (a(na+2) .eq. 0.) then
       
       do i = na + 2, 3, -1
          if (a(i) .ne. 0.) exit
       end do
       
       if (i == 3 .and. a(3) == 0.) then
          a(1:2) = 0.
          return
       end if
       
       na = i - 2
    end if

    if (a(3) .eq. 0.) then
       a(1:2) = 0.
    else
       a(1) = sign (na, ia)
       a(2) = a2
       a(na+3:na+4) = 0.
    end if
  end subroutine mproun
end module mpfunc

module mpdefmod
  use mpfuna, only: mpldb, mpier
  implicit none

  integer mpipl, mpiou, mpiep, mpwds
  parameter (mpipl = 12800, mpiou = 56, mpiep = 10 - mpipl)
  parameter (mpwds = mpipl / 7.224719896d0 + 2.d0)

  integer, private :: kdb, mp4, mp24, mp41
  parameter (kdb = kind(0.d0), mp4 = mpwds + 4, mp24 = 2 * mp4, mp41 = mp4 + 1)

  type mp_integer
     sequence
     real mpi(mp4)
  end type mp_integer
  type mp_real
     sequence
     real mpr(mp4)
  end type mp_real

  integer, public:: mpnwx, mpoud, new_mpipl, new_mpwds

contains

  subroutine mpinit(n_mpipl)
    integer, optional:: n_mpipl
    
    if (present (n_mpipl)) then
       if (n_mpipl > mpipl) then
          write (mpldb, *) 'mpinit: argument too large'
          stop
       endif
       new_mpipl = n_mpipl
       new_mpwds = n_mpipl / 7.224719896d0 + 2.d0
       mpnwx = new_mpwds
    else
       new_mpipl = mpipl
       new_mpwds = mpwds
       mpnwx = new_mpwds
    endif

    mpier = 0
    mpoud = mpiou
  end subroutine mpinit
end module mpdefmod

module mpintmod
  use mpfunc, only: mpeq, mpmdc, mpdmc, mpadd, mpinfr, mpmuld, mpdiv, mpcpr
  use mpdefmod
  implicit none

  integer, parameter, private :: kdb = kind(0.d0), mp4 = mpwds + 4,  &
                                 mp24 = 2 * mp4, mp41 = mp4 + 1

  private :: mp_eqjj, mp_eqij, mp_eqji, mp_addjj, mp_mulij, mp_mulji,  &
             mp_divjj, mp_eqtij

  interface assignment (=)
     module procedure mp_eqjj
     module procedure mp_eqij
     module procedure mp_eqji
  end interface

  interface operator (+)
     module procedure mp_addjj
  end interface

  interface operator (*)
     module procedure mp_mulij
     module procedure mp_mulji
  end interface

  interface operator (/)
     module procedure mp_divjj
  end interface

  interface operator (.eq.)
     module procedure mp_eqtij
  end interface

contains

  subroutine mp_eqjj (ja, jb)
    type(mp_integer), intent(out) :: ja
    type(mp_integer), intent(in)  :: jb
    integer :: mpnw
    mpnw = mpnwx
    call mpeq (jb%mpi, ja%mpi, mpnw) 
  end subroutine mp_eqjj

  subroutine mp_eqij (ia, jb)
    integer,          intent(out) :: ia
    type(mp_integer), intent(in)  :: jb
    real(kdb) :: db
    integer :: ib
    call mpmdc (jb%mpi, db, ib)
    ia = db * 2.d0 ** ib
  end subroutine mp_eqij

  subroutine mp_eqji (ja, ib)
    type(mp_integer), intent(out) :: ja
    integer,          intent(in)  :: ib
    real(kdb) :: db
    db = ib
    call mpdmc (db, 0, ja%mpi)
  end subroutine mp_eqji

  function mp_addjj (ja, jb)
    type(mp_integer), intent(in) :: ja, jb
    type(mp_integer) :: mp_addjj
    type(mp_real) :: q1, q2
    integer :: mpnw
    mpnw = mpnwx
    call mpadd (ja%mpi, jb%mpi, q1%mpr, mpnw) 
    call mpinfr (q1%mpr, mp_addjj%mpi, q2%mpr, mpnw) 
  end function mp_addjj

  function mp_mulij (ia, jb)
    integer,          intent(in) :: ia
    type(mp_integer), intent(in) :: jb
    type(mp_integer) :: mp_mulij
    type(mp_real) :: q1, q2
    real(kdb) :: da
    integer :: mpnw
    mpnw = mpnwx
    da = ia
    call mpmuld (jb%mpi, da, 0, q1%mpr, mpnw) 
    call mpinfr (q1%mpr, mp_mulij%mpi, q2%mpr, mpnw) 
  end function mp_mulij

  function mp_mulji (ja, ib)
    type(mp_integer), intent(in) :: ja
    integer,          intent(in) :: ib
    type(mp_integer) :: mp_mulji
    type(mp_real) :: q1, q2
    real(kdb) :: db
    integer :: mpnw
    mpnw = mpnwx
    db = ib
    call mpmuld (ja%mpi, db, 0, q1%mpr, mpnw) 
    call mpinfr (q1%mpr, mp_mulji%mpi, q2%mpr, mpnw) 
  end function mp_mulji

  function mp_divjj (ja, jb)
    type(mp_integer), intent (in) :: ja, jb
    type(mp_integer) :: mp_divjj
    type(mp_real) :: q1, q2
    integer :: mpnw
    mpnw = mpnwx
    call mpdiv (ja%mpi, jb%mpi, q1%mpr, mpnw) 
    call mpinfr (q1%mpr, mp_divjj%mpi, q2%mpr, mpnw) 
  end function mp_divjj

  function mp_eqtij (ia, jb)
    integer,          intent(in) :: ia
    type(mp_integer), intent(in) :: jb
    logical :: mp_eqtij
    type(mp_real) :: q1
    real(kdb) :: da
    integer :: mpnw, ic
    mpnw = mpnwx
    da = ia
    call mpdmc (da, 0, q1%mpr)
    call mpcpr (q1%mpr, jb%mpi, ic, mpnw) 
    mp_eqtij = ic == 0
  end function mp_eqtij
end module mpintmod

! pidigits code
module pi_mod
  use mpintmod
  implicit none

  interface comp
     module procedure comp1
     module procedure comp2
  end interface

contains

  function lfts(k)
    integer, intent(in) :: k
    integer, dimension(2,2) :: lfts

    lfts = reshape( (/ k, 0, 4*k + 2, 2*k + 1 /), (/ 2, 2 /) )
  end function lfts

  function comp1(a, b)
    integer,          dimension(2,2), intent(in) :: a
    type(mp_integer), dimension(2,2), intent(in) :: b
    type(mp_integer), dimension(2,2) :: comp1

    comp1(1,1) = a(1,1)*b(1,1) + a(1,2)*b(2,1)
    comp1(2,1) = a(2,1)*b(1,1) + a(2,2)*b(2,1)
    comp1(1,2) = a(1,1)*b(1,2) + a(1,2)*b(2,2)
    comp1(2,2) = a(2,1)*b(1,2) + a(2,2)*b(2,2)
  end function comp1

  function comp2(a, b)
    type(mp_integer), dimension(2,2), intent(in) :: a
    integer,          dimension(2,2), intent(in) :: b
    type(mp_integer), dimension(2,2) :: comp2
    
    comp2(1,1) = a(1,1)*b(1,1) + a(1,2)*b(2,1)
    comp2(2,1) = a(2,1)*b(1,1) + a(2,2)*b(2,1)
    comp2(1,2) = a(1,1)*b(1,2) + a(1,2)*b(2,2)
    comp2(2,2) = a(2,1)*b(1,2) + a(2,2)*b(2,2)
  end function comp2
  
  function prod(z, n)
    type(mp_integer), dimension(2,2), intent(in) :: z
    integer,                          intent(in) :: n
    type(mp_integer), dimension(2,2) :: prod

    prod = comp(reshape( (/ 10, 0, -10*n, 1 /), (/ 2, 2 /) ), z)
  end function prod
  
  logical function safe(z, n)
    type(mp_integer), dimension(2,2), intent(in) :: z
    integer,                          intent(in) :: n

    safe = n == (z(1,1) * 4 + z(1,2)) / (z(2,1) * 4 + z(2,2))
  end function safe

  integer function next(z)
    type(mp_integer), dimension(2,2), intent(in) :: z
    
    next = (z(1,1) * 3 + z(1,2)) / (z(2,1) * 3 + z(2,2))
  end function next
end module pi_mod

program pidigits
  use pi_mod
  implicit none

  character, parameter :: tab = achar(9)

  type(mp_integer), dimension(2,2) :: z
  integer           :: num, y, i = 1, j = 1, k = 1
  character(len=17) :: outLine = repeat(" ", 10) // tab // " :    "
  character(len=4)  :: argv

  call get_command_argument(1, argv)
  read(argv, *) num

  call mpinit(max(3*floor(num**1.21),16))
  z(1,1) = 1; z(2,1) = 0; z(1,2) = 0; z(2,2) = 1

  do
     y = next(z)
     if (safe(z, y)) then
        write(unit=outLine(k:k), fmt="(i1)") y
        if (k < 10 .and. i < num) then
           k = k + 1
        else
           k = 1
           write(unit=outLine(14:17), fmt="(i0)") i
           write(*, "(a)") trim(outLine)
           outLine(1:10) = repeat(" ", 10)
        end if

        z = prod(z, y)
        i = i + 1
        if (i > num) exit
     else
        z = comp(z, lfts(j))
        j = j + 1
     end if
  end do
end program pidigits
