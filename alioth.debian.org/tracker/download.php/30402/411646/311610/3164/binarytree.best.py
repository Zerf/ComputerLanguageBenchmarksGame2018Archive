# The Computer Language Shootout Benchmarks
# http://shootout.alioth.debian.org/
#
# contributed by Antoine Pitrou
# modified by Dominique Wahli and Larry Hastings

from sys import argv


def make_tree(item, depth):
    item2 = 2 * item
    if depth < 2:
        if depth:
          return (item, (item2 - 1, None, None), (item2, None, None))
        return (item, None, None)
    depth -= 1
    return (item, make_tree(item2 - 1, depth), make_tree(item2, depth))

def check_tree((item, left, right)):
    if left:
        return item + check_tree(left) - check_tree(right)
    return item


def main():
    min_depth = 4
    max_depth = max(min_depth + 2, int(argv[1]))
    stretch_depth = max_depth + 1

    print "stretch tree of depth %d\t check: %d" % (stretch_depth, check_tree(make_tree(0, stretch_depth)))

    long_lived_tree = make_tree(0, max_depth - 1)

    for depth in xrange(min_depth, stretch_depth, 2):
        iterations = 2**(max_depth - depth + min_depth)

        check = 0
        for i in xrange(1, iterations + 1):
            check += check_tree(make_tree(i, depth)) + check_tree(make_tree(-i, depth))

        print "%d\t trees of depth %d\t check: %d" % (iterations * 2, depth, check)

    print "long lived tree of depth %d\t check: %d" % (max_depth, check_tree(long_lived_tree))

if __name__ == '__main__':
    main()
