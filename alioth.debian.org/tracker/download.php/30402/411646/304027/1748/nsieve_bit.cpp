#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <cstdlib>

using namespace std;

inline
unsigned long
nsieve ( const size_t m ){
	static vector<unsigned long> 	flags;
	unsigned long 								count(0);
	const size_t 									lookupbit[]=
							{	0x00000001,0x00000002,0x00000004,0x00000008,
								0x00000010,0x00000020,0x00000040,0x00000080,
								0x00000100,0x00000200,0x00000400,0x00000800,
								0x00001000,0x00002000,0x00004000,0x00008000,
								0x00010000,0x00020000,0x00040000,0x00080000,
								0x00100000,0x00200000,0x00400000,0x00800000,
								0x01000000,0x02000000,0x04000000,0x08000000,
								0x10000000,0x20000000,0x40000000,0x80000000,
							 } ; 
							 
	flags.resize( (m>>5)+1 );
	flags[0]=0xfffffffc;
	fill( flags.begin()+1, flags.end() , 0xffffffff );
	  	
	for (size_t i=2; i<m; ++i )
		if ( flags[i>>5] & 1<<(i&0x1F) ){
       	for(size_t k=i*2; k<m; k+=i){        		        		
       		flags[k>>5] &=  ~lookupbit[k&0x1F];
       	}
       	++count;
   	}	  	
     	
	return count;
}


int main(int argc, char *argv[])
{
  int n = (argc == 2) ? atoi(argv[1]) : 2;
  unsigned m;
	
	cout<<setw(8);
	
	m = (1<<n)*10000;	
	cout<<endl<<"Primes up to "<<m<<" "<<nsieve(m);
	  
	m = (1<<n-1)*10000;
	cout<<endl<<"Primes up to "<<m<<" "<<nsieve(m);
	
	m = (1<<n-2)*10000;
	cout<<endl<<"Primes up to "<<m<<" "<<nsieve(m);
}
