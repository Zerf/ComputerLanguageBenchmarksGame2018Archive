/*	The Computer Language Benchmarks Game	
	http://shootout.alioth.debian.org/
	convert from c++ version by The Anh Tran
*/

using System;
using System.Threading;
using System.Text;

public class chame
{
	internal enum Color
	{
		blue,
		red,
		yellow,
		invalid
	}

	private static Color ColorCompliment(Color c1, Color c2)
	{
		switch (c1)
		{
			case Color.blue:
				switch (c2)
				{
					case Color.blue:
						return Color.blue;
					case Color.red:
						return Color.yellow;
					case Color.yellow:
						return Color.red;
				}
				break;
			case Color.red:
				switch (c2)
				{
					case Color.blue:
						return Color.yellow;
					case Color.red:
						return Color.red;
					case Color.yellow:
						return Color.blue;
				}
				break;
			case Color.yellow:
				switch (c2)
				{
					case Color.blue:
						return Color.red;
					case Color.red:
						return Color.blue;
					case Color.yellow:
						return Color.yellow;
				}
				break;
		}
		return Color.invalid;
	}

	private static String[] NUMBERS =
	{
		"zero", "one", "two", "three", "four", "five",
		"six", "seven", "eight", "nine"
	};

	private static String SpellNumber(int n)
	{
		StringBuilder sb = new StringBuilder();
		string nStr = n.ToString();

		foreach (char c in nStr)
			sb.AppendFormat(" {0}", NUMBERS[c - '0']);

		return sb.ToString();
	}


	private sealed class MeetingPlace
	{
		internal volatile int meetings_left;
		private volatile Creature waiting_creature;
		private int spin_lock;

		public MeetingPlace(int meetings)
		{
			meetings_left = meetings;
		}

		private void Lock()
		{
			int spin = 0;
			while (Interlocked.CompareExchange(ref spin_lock, 1, 0) != 0)
				Thread.SpinWait(++spin);
		}
		private void Unlock()
		{
			Interlocked.CompareExchange(ref spin_lock, 0, 1);
		}

		public Creature Meet(Creature current_creature)
		{
			Lock();
			try
			{
				if (meetings_left > 0)
				{
					if (waiting_creature == null)  // first arrive
					{
						waiting_creature = current_creature;
						return null;
					}
					else // second arrive
					{
						// store ref to first creature
						Creature first_creature = waiting_creature;
						waiting_creature = null;

						meetings_left--;
						return first_creature;
					}
				}
				return null;
			}
			finally
			{
				Unlock();
			}
		}
	}

	private sealed class Creature
	{
		private MeetingPlace place;

		internal int count;     // = 0;
		internal int sameCount; // = 0;
		private volatile bool met;// = false;

		private Color color;
		private long id;
        private AutoResetEvent wait;

		public Creature(MeetingPlace place, Color c)
		{
			this.place = place;
			color = c;
            wait = new AutoResetEvent(false);
		}

		public void run()
		{
			id = GetHashCode();

			while (place.meetings_left > 0)
			{
				met = false;
				Creature other = place.Meet(this);

				if (other != null)
					this.Meet(other);
				else
				{
					while (this.met == false)
						wait.WaitOne();
				}
			}
		}

		private void Meet(Creature other)
		{
			Color newcolor = ColorCompliment(this.color, other.color);
			this.color = other.color = newcolor;

			this.count++;
			other.count++;

			if (this.id == other.id)
			{
				this.sameCount++;
				other.sameCount++;
			}

			this.met = other.met = true;
            other.wait.Set();
		}
	}

	private static void RunGame(int n, params Color[] colors)
	{
		MeetingPlace place = new MeetingPlace(n);

		Creature[] creatures = new Creature[colors.Length];
		Thread[] cr_threads = new Thread[colors.Length];

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < colors.Length; i++)
		{
			sb.AppendFormat(" {0}", colors[i]);

			creatures[i] = new Creature(place, colors[i]);

			cr_threads[i] = new Thread(creatures[i].run);
			cr_threads[i].Start();
		}
		sb.AppendLine();

		foreach (Thread t in cr_threads)
			t.Join();

		int total = 0;
		foreach (Creature creature in creatures)
		{
            total += creature.count;
            sb.AppendFormat("{0}{1}\n", creature.count, SpellNumber(creature.sameCount));
		}

		sb.AppendFormat("{0}\n\n", SpellNumber(total));
		Console.Out.Write(sb);
	}

	public static void Main(String[] args)
	{
		int n = 600;
		if (args.Length > 0)
			n = Int32.Parse(args[0]);

		PrintColors();
		Console.Out.WriteLine();

		RunGame(n, Color.blue, Color.red, Color.yellow);
		RunGame(n, Color.blue, Color.red, Color.yellow, Color.red, Color.yellow,
				Color.blue, Color.red, Color.yellow, Color.red, Color.blue);
	}


	private static void PrintColors()
	{
		Color[] c = { Color.blue, Color.red, Color.yellow };

		foreach (Color c1 in c)
		{
			foreach (Color c2 in c)
				Console.Out.WriteLine("{0} + {1} -> {2}", c1, c2, ColorCompliment(c1, c2));
		}
	}
}

