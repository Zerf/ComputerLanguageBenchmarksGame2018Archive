/* The Computer Language Shootout
 * http://shootout.alioth.debian.org/
 * Contributed by Joern Inge Vestgaarden
 * Modified by Jorge Peixoto de Morais Neto
 * Compile with -ffast-math
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <error.h>

#define WIDTH 60
#define MIN(a,b) ((a) <= (b) ? (a) : (b))
#define NELEMENTS(x) (sizeof (x) / sizeof ((x)[0]))

typedef struct {
    float p;
    char c;
} aminoacid_t;

static inline float myrandom (double max) { 
    unsigned long const IM = 139968;
    unsigned long const IA = 3877;
    unsigned long const IC = 29573;
    static unsigned long last = 42; 
    last = (last * IA + IC) % IM; 
    return max * last / IM; 
} 

static void accumulate_probabilities (aminoacid_t *genelist, size_t len) {
    float cp = 0.0;
    size_t i;
    for (i = 0; i < len; i++) {
        cp += genelist[i].p;
        genelist[i].p = cp;
    }
}

/* This function prints the characters of the string s. When it */
/* reaches the end of the string, it goes back to the beginning */
/* It stops when the total number of characters printed is count. */
/* count may not be a multiple of the length of the string */
/* Between each group of consecutive WIDTH characters, */
/* it prints a newline */
/* This function assumes that WIDTH <= strlen (s) + 1 */
static void repeat_fasta (char const *s, size_t count) {
    size_t len = strlen(s);
    size_t pos = 0; 
    while (count) { 
 	size_t line = MIN(WIDTH, count); 
 	size_t available = len - pos; 
 	if (line <= available) {     
	    /* We can print the whole line at once*/
 	    fwrite_unlocked (s+pos,1,line,stdout); 
 	    pos += line; 
 	} else {
 	    fwrite_unlocked (s+pos,1,available,stdout);
	    fwrite_unlocked (s,1,line-available,stdout);
 	    pos = line - available;
 	} 
	putc_unlocked ('\n', stdout);
 	count -= line;
    } 
}

/* This function takes a pointer to the first element of an array  */
/* Each element of the array is a struct with a character and */
/* a float number p between 0 and 1. */
/* The function generates a random float number r and */
/* finds the first number of the array such that p >= r. */
/* This is a weighted random selection. */
/* The function then prints this character. */
/* This is done count times. */
/* Between each group of consecutive WIDTH characters,  */
/* the function prints a newline */
static void random_fasta (aminoacid_t const *genelist, size_t count) {
     char buf[WIDTH + 1]; 
     while (count) { 
 	size_t line = MIN(WIDTH, count); 
 	count -= line; 
 	size_t pos = 0; 
 	while (pos < line) { 
 	    aminoacid_t const *a = genelist; 
 	    float r = myrandom(1.0); 
 	    while (a->p < r) 
 		++a; /* Linear search */ 
 	    buf[pos++] = a->c; 
 	} 
 	buf[pos] = '\n'; 
 	fwrite_unlocked (buf, 1, line+1, stdout); 
     } 
}

int main (int argc, char **argv) {
    char const *alu ="\
GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG\
GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA\
CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT\
ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA\
GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG\
AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC\
AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA";

    aminoacid_t iub[] = {
	{ 0.27, 'a' },
	{ 0.12, 'c' },
	{ 0.12, 'g' },
	{ 0.27, 't' },
	{ 0.02, 'B' },
	{ 0.02, 'D' },
	{ 0.02, 'H' },
	{ 0.02, 'K' },
	{ 0.02, 'M' },
	{ 0.02, 'N' },
	{ 0.02, 'R' },
	{ 0.02, 'S' },
	{ 0.02, 'V' },
	{ 0.02, 'W' },
	{ 0.02, 'Y' }};

    aminoacid_t homosapiens[] = {
	{ 0.3029549426680, 'a' },
	{ 0.1979883004921, 'c' },
	{ 0.1975473066391, 'g' },
	{ 0.3015094502008, 't' }};

    size_t count = 1000;
    if (argc > 1) { 
 	char *tail; 
 	count = strtoul (argv[1], &tail, 0); 
 	if (tail == argv[1])  
 	    error (1, 0, "Could not convert \"%s\" to an unsigned long integer", argv[1]); 
    } 

    accumulate_probabilities (iub, NELEMENTS(iub)); 
    accumulate_probabilities (homosapiens, NELEMENTS(homosapiens));

    puts (">ONE Homo sapiens alu");
    repeat_fasta (alu, count * 2);
    puts (">TWO IUB ambiguity codes");
    random_fasta (iub, count * 3);
    puts (">THREE Homo sapiens frequency");
    random_fasta (homosapiens, count * 5);
    return 0;
}
