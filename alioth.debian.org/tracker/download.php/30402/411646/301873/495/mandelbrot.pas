program mandelbrot;

var n, x, y, i, bits,bit: Longint;
    Zr, Zi, Cr, Ci, tmp: Double;
begin
  Val(ParamStr(1), n, y);
  writeln('P4');
  writeln(n,' ',n);
  for y := 0 to n-1 do
  begin
    bits := 255;  bit := 128;
    Ci := 2.0 * y / n - 1.0;
    for x := 0 to n-1 do
    begin
      Cr := 2.0 * x / n - 1.5;

      Zr := Cr;  Zi := Ci;
      for i := 1 to 50 do
      begin
        if Zr*Zr + Zi*Zi > 4.0 then begin
          bits := bits xor bit;
          break;
        end;
        tmp := Zr*Zr - Zi*Zi + Cr;
        Zi := 2*Zr*Zi + Ci;
        Zr := tmp;
      end;

      if bit > 1 then bit := bit shr 1 else
      begin
        write(chr(bits));
        bits := 255;  bit := 128;
      end;
    end;
    if bit < 128 then write(chr(bits xor((bit shl 1)-1)));
  end;
end. 
