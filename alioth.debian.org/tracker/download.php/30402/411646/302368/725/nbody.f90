! The Great Computer Language Shootout
!   http://shootout.alioth.debian.org/
!
!   contributed by Steve Decker, based on the Fortran version by Simon Geard
!
! compilation:
!   g95 -O3 -funroll-loops -fomit-frame-pointer nbody.f90
!   gfortran -O3 -funroll-loops -fomit-frame-pointer nbody.f90
!   ifort -O3 -ip nbody.f90

program nbody
  implicit none

  integer,  parameter :: dp = selected_real_kind(10)
  real(dp), parameter :: TStep = 0.01_dp, Pi = 3.141592653589793_dp,  &
                         SolarMass = 4 * Pi**2, DaysPerYear = 365.24_dp,  &
                         SolarMassR = 1. / SolarMass

  integer          :: num, i
  character(len=8) :: argv

  type body
     real(dp), dimension(3) :: pos, vel
     real(dp) :: mass
  end type body
  
  type(body), parameter :: Jupiter = body( (/ 4.84143144246472090d0,  &
       -1.16032004402742839d0, -1.03622044471123109d-01 /),  &
       (/ 1.66007664274403694d-03 * DaysPerYear,  &
       7.69901118419740425d-03 * DaysPerYear,  &
       -6.90460016972063023d-05 * DaysPerYear /),  &
       9.54791938424326609d-04 * SolarMass)

  type(body), parameter :: Saturn = body( (/ 8.34336671824457987d+00,  &
       4.12479856412430479d+00, -4.03523417114321381d-01 /),  &
       (/ -2.76742510726862411d-03 * DaysPerYear,  &
       4.99852801234917238d-03 * DaysPerYear,  &
       2.30417297573763929d-05 * DaysPerYear /),  &
       2.85885980666130812d-04 * SolarMass)
  
  type(body), parameter :: Uranus = body( (/ 1.28943695621391310d+01,  &
       -1.51111514016986312d+01, -2.23307578892655734d-01 /),  &
       (/ 2.96460137564761618d-03 * DaysPerYear,  &
       2.37847173959480950d-03 * DaysPerYear,  &
       -2.96589568540237556d-05 * DaysPerYear /),  &
       4.36624404335156298d-05 * SolarMass)

  type(body), parameter :: Neptune = body( (/ 1.53796971148509165d+01,  &
       -2.59193146099879641d+01, 1.79258772950371181d-01 /),  &
       (/ 2.68067772490389322d-03 * DaysPerYear,  &
       1.62824170038242295d-03 * DaysPerYear,  &
       -9.51592254519715870d-05 * DaysPerYear /),  &
       5.15138902046611451d-05 * SolarMass)

  type(body), parameter :: Sun = body( (/ 0.0d0, 0.0d0, 0.0d0 /),  &
       (/ 0.0d0, 0.0d0, 0.0d0 /), SolarMass)
  
  type(body), dimension(5) :: bodies =  &
       (/ Sun, Jupiter, Saturn, Uranus, Neptune /)
  
  call get_command_argument(1, argv)
  read(argv, *) num

  call offset_momentum(1)
  write(*, "(f12.9)") energy(bodies)
  do i = 1, num
     call advance()
  end do
  write(*, "(f12.9)") energy(bodies)
  
contains
  
  subroutine offset_momentum(k)
    integer, intent(in) :: k
    
    real(dp), dimension(3) :: moment
    
    do i = 1, 3
       moment(i) = sum(bodies%vel(i) * bodies%mass)
    end do
    bodies(k)%vel = -moment * SolarMassR
  end subroutine offset_momentum
  
  subroutine advance()
    real(dp), dimension(3) :: dx
    real(dp)               :: mag
    integer                :: i, j
    
    do i = 1, size(bodies)
       do j = i+1, size(bodies)
          dx = bodies(i)%pos - bodies(j)%pos
          
          mag = TStep / (sqrt(dx(1)**2 + dx(2)**2 + dx(3)**2)**3)
          
          bodies(i)%vel = bodies(i)%vel - dx * bodies(j)%mass * mag
          
          bodies(j)%vel = bodies(j)%vel + dx * bodies(i)%mass * mag
       end do
    end do
 
    bodies%pos(1) = bodies%pos(1) + TStep * bodies%vel(1)
    bodies%pos(2) = bodies%pos(2) + TStep * bodies%vel(2)
    bodies%pos(3) = bodies%pos(3) + TStep * bodies%vel(3)    
  end subroutine advance
  
  real(dp) function energy(bodies)
    type(body), dimension(:), intent(in) :: bodies
    
    real(dp), dimension(3) :: dx
    integer                :: j
    
    energy = 0.0d0
    do i = 1, size(bodies)
       energy = energy + 0.5d0 * bodies(i)%mass * sum(bodies(i)%vel**2)
       
       do j = i+1, size(bodies)
          dx = bodies(i)%pos - bodies(j)%pos
          energy = energy - (bodies(i)%mass * bodies(j)%mass) / sqrt(sum(dx**2))
       end do
    end do
  end function energy
end program nbody
