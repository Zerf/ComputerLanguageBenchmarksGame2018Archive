#!/usr/bin/python
#
# Contributed by Ian Osgood
#
import sys

comps = {'A':"T", 'C':"G", 'G':"C", 'T':"A",
         'B':"V", 'D':"H", 'H':"D", 'K':"M",
         'M':"K", 'R':"Y", 'V':"B", 'Y':"R",
         'W':'W', 'N':'N', 'S':'S'}

def complement(segment):
    return ''.join([comps[c.upper()] for c in segment])

def flush(seq):
    seq.reverse()
    width = 60
    n = len(seq)
    for i in xrange(0, n, width):
        print complement(seq[i:i+width])

sequence = None
for line in sys.stdin:
    if line[0] == '>':
	if sequence: flush(sequence)
        sequence = []
        print line,
    else:
	sequence += list(line.strip())
flush(sequence)
