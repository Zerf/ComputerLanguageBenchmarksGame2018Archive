{ The Great Computer Language Shootout
  http://shootout.alioth.debian.org

  contributed by Joost van der Sluis
  modified by Ciucu Mihai
}
program nsieve;

{$mode objfpc}

var n, useless : integer;

procedure primes(n : integer); inline;
var flags : array of boolean;
    size,i,j,count,sroot,k : integer;
begin
  size:=10000 shl n;
  SetLength(flags, size+10);
  fillchar(flags[0],size+2,true);
  count:=0; sroot:=trunc(sqrt(size));
  for i := 2 to size do
    if flags[i] then
    begin
      inc(count);
      j:=2*i; k:=i;
      if i>2 then
             begin
             if i<sroot then j:=i*i
                        else j:=size+1;
             inc(k,i);
             end;
      while j<=size do
      begin
      if flags[j] then flags[j] := false;
      inc(j,k);
      end;
    end;
  writeln('Primes up to', size:9, count:9);
end;

begin
  val(ParamStr(1), n, useless);
  primes(n);
  primes(n-1);
  primes(n-2);
end.
