#!/Users/aks/j602/bin/jconsole
NB. nsieve-1
NB.
NB. The Computer Language Benchmarks Game
NB.   http://shootout.alioth.debian.org/
NB.
NB.   contributed by Alan K. Stebbens <aks@stebbens.org>
NB.   modified by
NB.
NB.
NB. This script implements the nsieve benchmark
NB. as defined at:
NB.  http://shootout.alioth.debian.org/gp4/benchmark.php?test=partialsums&lang=all
NB. 
NB.
NB. The "correct" output file for this benchmark test is defined
NB. at http://shootout.alioth.debian.org/gp4/iofile.php?test=nsievebits&lang=all&file=output
NB.
NB. Primes up to    40000     4203
NB. Primes up to    20000     2262
NB. Primes up to    10000     1229

echo=: 0 0&$ @ (1!:2&2)

n     =: >((''&-:@>@{.) { ])2;~".>{.2}.ARGV NB. get any argument and default it

NB. primes is a language primitive of J

p=:,._1 p: ,i=:,.10000*2^(n-i.]l=:>:n)

echo (l#,:'Primes up to	'),.(":i),.(l#,:'	'),.":p

exit ''
