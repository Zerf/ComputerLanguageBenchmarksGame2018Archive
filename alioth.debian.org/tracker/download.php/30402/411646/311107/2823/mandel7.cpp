/*	The Computer Language Benchmarks Game
	http://shootout.alioth.debian.org/

	contributed by Paolo Bonzini
	further optimized by Jason Garrett-Glaser
	OpenMP by The Anh Tran
*/

#include <stdio.h>
#include <stdlib.h>

// need "-fopenmp" flag when compile
#include <omp.h>

typedef double	v2d	__attribute__ ((vector_size(16))); // vector of two doubles

__attribute__((nothrow))
static void 
mandelbrot(char* data, int* nbyte_each_line, int N, int width_bytes)
{
	v2d v10	= { 1.0, 1.0 };
	v2d v15	= { 1.5, 1.5 };
	v2d four	= { 4.0, 4.0 };

	v2d inverse_n = {2.0 / N, 2.0 / N};

	// schedule(dynamic) because many mbrot elements located near the center point
	// coordinate [width/2 height/2] -> center lines take more computation time
	#pragma omp parallel for default(shared) schedule(dynamic, N >> 5)
	for (int y = 0; y < N; ++y)
	{
		int bit_num = 0;
		int byte_count = 0;
		int byte_acc = 0;
		char* pdata = data + y * width_bytes;

		v2d Civ = {y, y};
		Civ = Civ * inverse_n - v10;

		for (int x = 0; x < N; x += 2)
		{
			v2d Crv = {x +1, x};
			Crv = Crv * inverse_n - v15;

			// avoid (2 multiplies and 2 adds) in 1st iteration: zero*zero + zero*zero + Civ
			v2d Zrv	= Crv;
			// avoid (1 sub and 1 add): zero - zero + Crv
			v2d Ziv	= Civ;
			v2d Trv	= Crv * Crv;
			v2d Tiv	= Civ * Civ;

			// assume that element [x, y] and element [x+1, y] belong to mandelbrot set
			int result = 3;

			// skip zero*zero => start from 1
			int i = 1;
			while ( (result != 0) && (i++ < 50) )
			{
				Ziv = (Zrv*Ziv) + (Zrv*Ziv) + Civ;
				Zrv = Trv - Tiv + Crv;
			
				Trv = Zrv * Zrv;
				Tiv = Ziv * Ziv;

				// delta = (Trv + Tiv) <= four ? 0xff : 0x00		 (i.e. |Z| <= 4.0)
				v2d delta = (v2d)__builtin_ia32_cmplepd( (Trv + Tiv), four );

				// Z1 is [x, y]	Z2 is [x+1, y]
				// mask = 3 <=> |Z2| <= 4	|Z1| <= 4
				// mask = 2 <=> |Z2| > 4	|Z1| <= 4
				// mask = 1 <=> |Z2| <= 4	|Z1| > 4
				// mask = 0 <=> |Z2| > 4 	|Z1| > 4
				int mask = __builtin_ia32_movmskpd(delta);

				// mask-out not-mandelbrot element
				result &= mask;
			}	
		
			byte_acc <<= 2;
			byte_acc |= result;

			bit_num += 2;
			if ( __builtin_expect((bit_num == 8), false) )
			{
				pdata[ byte_count++ ] = (char)byte_acc;
				bit_num = byte_acc = 0;
			}
		} // end foreach( column )

		if ( __builtin_expect((bit_num != 0), false) )
		{
			byte_acc <<= (8 - (N & 7));
			pdata[ byte_count++ ] = (char)byte_acc;
		}

		nbyte_each_line[y] = byte_count;
	} // end parallel region
}

int main (int argc, char **argv)
{
	int N = 200;

	if  (argc == 2)
		N = atoi( argv[1] );
	printf("P4\n%d %d\n", N, N);

	int width_bytes = N/8 +1;

	char *data = (char*)malloc( width_bytes * N * sizeof(char) );
	int* nbyte_each_line = (int*)calloc( N, sizeof(int) );

	mandelbrot(data, nbyte_each_line, N, width_bytes);

	char* pdata = data;
	for (int y = 0; y < N; y++)
	{
		fwrite( pdata, nbyte_each_line[y], 1, stdout);
		pdata += width_bytes;
	}

	free(data);
	free(nbyte_each_line);

	return 0;
}

