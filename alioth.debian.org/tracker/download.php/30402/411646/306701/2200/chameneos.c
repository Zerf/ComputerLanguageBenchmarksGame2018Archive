/* The Computer Language Benchmarks Game
   http://shootout.alioth.debian.org/

   Written by Jorge Peixoto de Morais Neto (JorgePeixotoMorais@gmail.com)
   based on code by Josh Goldfoot
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <err.h>

static sem_t mutex;
static sem_t second_creature;

static unsigned long meetingsleft;

typedef enum { Blue, Red, Yellow, Faded } color_t;
typedef struct creature_t {
    unsigned long *meetingsp;
    color_t color;
} creature_t;

static color_t complementarycolor (color_t c1, color_t c2) {
    switch (c1) {
    case Blue:
	return c2 == Red ? Yellow : Red;
    case Red:
	return c2 == Blue ? Yellow : Blue;
    case Yellow:
	return c2 == Blue ? Red : Blue;
    default:
	return Faded;
    }
}

static color_t new_color (color_t color) {
    static  int mp_state = 0; // meeting place state
    static color_t color1, color2;
    color_t othercolor;
    sem_wait (&mutex);
    switch (mp_state) {
    case 0:
	color1 = color;
	mp_state = 1;
	sem_post (&mutex);
	sem_wait (&second_creature);
	othercolor = color2;
	sem_post (&mutex);
    return complementarycolor (color, othercolor);
    case 1:
	mp_state = (--meetingsleft ? 0 : -1);
	color2 = color;
	othercolor = color1;
	sem_post (&second_creature);
	return complementarycolor (color, othercolor);
    case -1:
    default:
	sem_post (&mutex);
	return Faded;
    }
}

static void *run_creature (void *voidpme) {
    creature_t *me = voidpme;
    unsigned long meetings = 0;
    do {
	meetings++;
	me->color = new_color (me->color);
    } while (me->color != Faded);
    *me->meetingsp = meetings - 1;
    static int zero = 0;
    return &zero;
}

static unsigned long meetings_of_four_creatures (void) {
    sem_init (&mutex, 0, 1);
    sem_init (&second_creature, 0, 0);

    unsigned long reports[4];
    creature_t creatures[4] = {{&reports[0], Blue},
			      {&reports[1], Red},
			      {&reports[2], Yellow},
			      {&reports[3], Blue}};
    pthread_t pids[4];
    int i;
    for (i = 0; i < 4; i++)
	pthread_create (&pids[i], NULL, run_creature, &creatures[i]);

    for (i = 0; i < 4; i++)
	pthread_join (pids[i], NULL);

    unsigned long sum = reports[0];
    for (i = 1; i < 4; i++)
	sum += reports[i];
    return sum;
}

int main (int argc, char const **argv) {
    if (argc == 1)
	meetingsleft = 1e6;
    else {
	char *tail;
	meetingsleft = strtoul (argv[1], &tail, 0);
	if (tail == argv[1]) 
	    errx (1, "Could not convert \"%s\" to an unsigned long integer", argv[1]);
    }
    unsigned long sum = meetings_of_four_creatures ();
    printf ("%lu\n", sum);
    return 0;
}
