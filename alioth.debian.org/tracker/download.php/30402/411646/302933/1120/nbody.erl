%  The Great Computer Language Shootout
%   http://shootout.alioth.debian.org/
%
%   contributed by Hans Svensson
%
%   erl -noshell -noinput -run nbody main 5000000

-module(nbody).

-export([main/1]).

-define(PI,3.141592653589793).
-define(SOLARMASS,(4*?PI*?PI)).
-define(DAYSPERYEAR,365.24).

-define(X,1).
-define(Y,2).
-define(Z,3).
-define(VX,4).
-define(VY,5).
-define(VZ,6).
-define(M,7).

main([Arg]) ->
    N = list_to_integer(Arg),
    main(N),
    erlang:halt(0);

main(N) ->
    Bodies_ = offset_momentum(bodies()),
    io:format("~.9f\n",[energy(Bodies_)]),
    io:format("~.9f\n",[energy(advance(N,Bodies_,0.01))]).

advance(0,Bodies,_) ->
    Bodies;
advance(N,Bodies,Dt) ->
    Bodies_ = adv_loop(Bodies,Dt,[]),
    Bodies__ = adv_loop3(Bodies_,Dt),
    advance(N-1,Bodies__,Dt).

adv_loop([],_,Bodies)->
    lists:reverse(Bodies);
adv_loop([Body|Bodies],Dt,Bodies2) ->
    {Body_,Bodies_} = adv_loop2(Body,Bodies,[],Dt),
    adv_loop(Bodies_,Dt,[Body_|Bodies2]).

adv_loop2(Body,[],Bodies2,_)->
    {Body,lists:reverse(Bodies2)};
adv_loop2(Body,[Body2|Bodies],Bodies2,Dt) ->
    BX = element(?X,Body), BY = element(?Y,Body), BZ = element(?Z,Body),
    B2X = element(?X,Body2), B2Y = element(?Y,Body2), B2Z = element(?Z,Body2),
    Dx = BX - B2X,
    Dy = BY - B2Y,
    Dz = BZ - B2Z,
    Dist = math:sqrt(Dx*Dx+Dy*Dy+Dz*Dz),
    Mag = Dt / (Dist * Dist * Dist),
    MB = element(?M,Body), MMB =  MB * Mag,
    MB2 = element(?M,Body2), MMB2 =  MB2 * Mag,
    Body_ = {BX,BY,BZ,
	     element(?VX,Body) - (Dx * MMB2),
	     element(?VY,Body) - (Dy * MMB2),
	     element(?VZ,Body) - (Dz * MMB2),
	     MB},
    Body2_ = {B2X,B2Y,B2Z,
	     element(?VX,Body2) + (Dx * MMB),
	     element(?VY,Body2) + (Dy * MMB),
	     element(?VZ,Body2) + (Dz * MMB),
	     MB2},
    adv_loop2(Body_,Bodies,[Body2_ | Bodies2],Dt).
    
adv_loop3([],_)->
    [];
adv_loop3([Body|Bodies],Dt) ->
    Body_ = {element(?X,Body) + (Dt * element(?VX,Body)),
	     element(?Y,Body) + (Dt * element(?VY,Body)),
	     element(?Z,Body) + (Dt * element(?VZ,Body)),
	     element(?VX,Body),element(?VY,Body),element(?VZ,Body),
	     element(?M,Body)},
    [Body_ | adv_loop3(Bodies,Dt)].

energy(Bodies) ->
    nrgy(Bodies,0.0).

nrgy([],E)->
    E;
nrgy([Body|Bodies],E) ->
    E_ = E + 0.5 * (element(?M,Body) *
		    ((element(?VX,Body)*element(?VX,Body)) +
		     (element(?VY,Body)*element(?VY,Body)) +
		     (element(?VZ,Body)*element(?VZ,Body)))),
    nrgy(Bodies,nrgy_loop(Body,Bodies,E_)).

nrgy_loop(_,[],E)->
    E;
nrgy_loop(Body,[Body2|Bodies2],E)->
    Dx = element(?X,Body) - element(?X,Body2),
    Dy = element(?Y,Body) - element(?Y,Body2),
    Dz = element(?Z,Body) - element(?Z,Body2),
    Dist = math:sqrt(Dx*Dx+Dy*Dy+Dz*Dz),
    E_ = E - (element(?M,Body)*element(?M,Body2)) / Dist,
    nrgy_loop(Body,Bodies2,E_).

offset_momentum([Body|Bodies]) ->
    {Px,Py,Pz} = off_loop([Body|Bodies],0.0,0.0,0.0),
    [{element(?X,Body),element(?Y,Body),element(?Z,Body),
      -(Px/?SOLARMASS),-(Py/?SOLARMASS),-(Pz/?SOLARMASS),
      element(?M,Body)} | Bodies].

off_loop([],Px,Py,Pz) ->
    {Px,Py,Pz};
off_loop([Body|Bodies],Px,Py,Pz) ->
    off_loop(Bodies,
	     Px + element(?VX,Body)*element(?M,Body),
	     Py + element(?VY,Body)*element(?M,Body),
	     Pz + element(?VZ,Body)*element(?M,Body)).

bodies() ->
    [{ % sun 
       0, 0, 0, 0, 0, 0, ?SOLARMASS
      },
     { %jupiter 
       4.84143144246472090e+00,
       -1.16032004402742839e+00,
       -1.03622044471123109e-01,
       1.66007664274403694e-03 * ?DAYSPERYEAR,
       7.69901118419740425e-03 * ?DAYSPERYEAR,
       -6.90460016972063023e-05 * ?DAYSPERYEAR,
       9.54791938424326609e-04 * ?SOLARMASS
      },
     { % saturn 
       8.34336671824457987e+00,
       4.12479856412430479e+00,
       -4.03523417114321381e-01,
       -2.76742510726862411e-03 * ?DAYSPERYEAR,
       4.99852801234917238e-03 * ?DAYSPERYEAR,
       2.30417297573763929e-05 * ?DAYSPERYEAR,
       2.85885980666130812e-04 * ?SOLARMASS
      },
     { % uranus
       1.28943695621391310e+01,
       -1.51111514016986312e+01,
       -2.23307578892655734e-01,
       2.96460137564761618e-03 * ?DAYSPERYEAR,
       2.37847173959480950e-03 * ?DAYSPERYEAR,
       -2.96589568540237556e-05 * ?DAYSPERYEAR,
       4.36624404335156298e-05 * ?SOLARMASS
      },
     { % neptune
       1.53796971148509165e+01,
       -2.59193146099879641e+01,
       1.79258772950371181e-01,
       2.68067772490389322e-03 * ?DAYSPERYEAR,
       1.62824170038242295e-03 * ?DAYSPERYEAR,
       -9.51592254519715870e-05 * ?DAYSPERYEAR,
       5.15138902046611451e-05 * ?SOLARMASS
      }].
