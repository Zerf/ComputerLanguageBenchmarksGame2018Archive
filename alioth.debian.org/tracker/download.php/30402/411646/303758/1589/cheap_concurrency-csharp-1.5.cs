/*
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * Contributed by: Alexandre Alapetite http://alexandre.alapetite.net/
 * Date: 2006-08-13. Updated 2006-08-15
 * Language: C# 1.5 (Microsoft .NET 1.1, Mono mcs)
 */

using System;
using System.Collections;
using System.Threading;

namespace cheap_concurrency
{
	public sealed class MyMessage
	{
		private const int numberOfThreads = 500;
		internal static int numberOfMessagesToSend;

		public static void Main(string[] args)
		{
			numberOfMessagesToSend = int.Parse(args[0]);

			MessageThread chain = null;
			for (int i = numberOfThreads; i > 0; i--)
			{
				chain = new MessageThread(chain);
				new Thread(new ThreadStart(chain.Run)).Start();
			}

			for (int i = 0; i < numberOfMessagesToSend; i++)
				chain.Enqueue(0);
		}
	}

	internal sealed class MessageThread
	{
		private MessageThread nextThread;
		private Queue list = new Queue();
		private int numberOfMessagesToSend;

		internal MessageThread(MessageThread nextThread)
		{
			this.nextThread = nextThread;
			this.numberOfMessagesToSend = MyMessage.numberOfMessagesToSend;
		}

		internal void Run()
		{
			if (nextThread == null)
			{
				int sum = 0;
				while (numberOfMessagesToSend > 0)
				{
					lock (list.SyncRoot)
						while (list.Count > 0)
						{
							sum += (int)list.Dequeue();
							numberOfMessagesToSend--;
						}
					Thread.Sleep(0);
				}
				Console.WriteLine(sum);
			}
			else
			{
				while (numberOfMessagesToSend > 0)
				{
					lock (list.SyncRoot)
						while (list.Count > 0)
						{
							nextThread.Enqueue((int)list.Dequeue());
							numberOfMessagesToSend--;
						}
					Thread.Sleep(0);
				}
			}
		}

		internal void Enqueue(int mess)
		{
			lock (list.SyncRoot)
				list.Enqueue(mess + 1);
		}
	}
}
