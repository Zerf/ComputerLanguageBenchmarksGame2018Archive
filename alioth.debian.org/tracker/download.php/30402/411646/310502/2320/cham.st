'From VisualWorks® NonCommercial, 7.4.1 of May 30, 2006 on January 11, 2008 at 4:33:18 am'!


Smalltalk defineClass: #Mall
	superclass: #{Core.Object}
	indexedType: #none
	private: false
	instanceVariableNames: 'mustWait firstCall guard secondCreature firstCreature maxRendezvous bouncer '
	classInstanceVariableNames: ''
	imports: ''
	category: 'chameleon'!

Smalltalk.Mall defineSharedVariable: #Units
	private: false
	constant: false
	category: 'As yet unclassified'
	initializer: nil!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Mall class methodsFor: 'as yet unclassified'!

closeMall: aMall forCreatures: creatures usingGuard: guard
	creatures size timesRepeat: [ guard wait ].
!

createCreaturesWith: aCollectionOfColours
	| aName |
	aName:=0.
	^ aCollectionOfColours collect: 
		[ : aColour | 
		aName := aName + 1.
		Creature 
			withName: aName
			colour: aColour ].
!

generateReportFor: creatures 
	| sum readOut |
	readOut := WriteStream on: String new.
	creatures do: 
		[ : aCreature | 

			 aCreature creaturesMet printOn: readOut.
					readOut space.
aCreature selfMet printOn: readOut.
			readOut cr.
			].
	sum := creatures 
		inject: 0
		into: [ : accum : each | accum + each creaturesMet ].
		readOut space.
	sum printString do: 
		[ : el | 
		readOut
			nextPutAll: (self units at: el digitValue + 1) ;
			space ].
	^ readOut!

generateReportForColours: colours
	| readOut |
	readOut := WriteStream on: String new.
colours do: 
		[ : colour | 
			colour printOn: readOut.
			readOut space ].
	^ readOut!

initialize
"self initialize"
Units := #(
		'zero'
		'one'
		'two'
		'three'
		'four'
		'five'
		'six'
		'seven'
		'eight'
		'nine'
	).!

new
	^super new initialize.!

openMall: aMall forCreatures: creatures usingGuard: sema 
	creatures do: 
		[ : aCreature | 
		[ aCreature visitMall: aMall.
		sema signal ]  fork].!

openMallWith: aCollectionOfColours forNumberOfMeets: aNumber 
	| mall creatures guard readOut |
	readOut := WriteStream on: String new.
	guard := Semaphore new.
	mall := Mall new.
	mall maxRendezvous: aNumber.
readOut nextPutAll: (self generateReportForColours: aCollectionOfColours) contents; cr .
		
	creatures := self createCreaturesWith: aCollectionOfColours.
	self openMall: mall forCreatures:  creatures usingGuard: guard.
	self waitForClosingOfMall: mall withCreatures:  creatures usingGuard: guard.
	
readOut nextPutAll:  (self generateReportFor: creatures) contents ;
		cr.
^readOut.!

performTest
self runBenchMark: Tests arg on: Tests stdout.
^''!

runBenchMark
|firstTestColours secondTestColours|

Transcript show: ChameneosColour generateReportOfColours contents; cr.

firstTestColours :=Array with: ChameneosColour  createBlue with: ChameneosColour createRed  with: ChameneosColour createYellow.

secondTestColours :=(OrderedCollection new) 
add: ChameneosColour  createBlue; add: ChameneosColour createRed ; add: ChameneosColour createYellow; add:ChameneosColour createRed ; add:ChameneosColour createYellow; add:ChameneosColour createBlue; add:ChameneosColour createRed; add:ChameneosColour createYellow; add:ChameneosColour createRed; add:ChameneosColour createBlue; yourself.

Mall openMallWith: firstTestColours  forNumberOfMeets: 6000.
Transcript cr.
Mall openMallWith: secondTestColours  forNumberOfMeets: 6000.
!

runBenchMark: number
|firstTestColours secondTestColours|

Transcript show: ChameneosColour generateReportOfColours contents; cr.

firstTestColours :=Array with: ChameneosColour  createBlue with: ChameneosColour createRed  with: ChameneosColour createYellow.

secondTestColours :=(OrderedCollection new) 
add: ChameneosColour  createBlue; add: ChameneosColour createRed ; add: ChameneosColour createYellow; add:ChameneosColour createRed ; add:ChameneosColour createYellow; add:ChameneosColour createBlue; add:ChameneosColour createRed; add:ChameneosColour createYellow; add:ChameneosColour createRed; add:ChameneosColour createBlue; yourself.

Mall openMallWith: firstTestColours  forNumberOfMeets: number.
Transcript cr.
Mall openMallWith: secondTestColours  forNumberOfMeets: number.
!

runBenchMark: number on: anOutputStream
|firstTestColours secondTestColours|

anOutputStream nextPutAll: (ChameneosColour generateReportOfColours contents); cr.

firstTestColours :=Array with: ChameneosColour  createBlue with: ChameneosColour createRed  with: ChameneosColour createYellow.

secondTestColours :=(OrderedCollection new) 
add: ChameneosColour  createBlue; add: ChameneosColour createRed ; add: ChameneosColour createYellow; add:ChameneosColour createRed ; add:ChameneosColour createYellow; add:ChameneosColour createBlue; add:ChameneosColour createRed; add:ChameneosColour createYellow; add:ChameneosColour createRed; add:ChameneosColour createBlue; yourself.

anOutputStream nextPutAll: (Mall openMallWith: firstTestColours  forNumberOfMeets: number) contents.
anOutputStream cr.
anOutputStream nextPutAll: (Mall openMallWith: secondTestColours  forNumberOfMeets: number) contents.
anOutputStream flush.
!

units

^Units.!

waitForClosingOfMall: aMall withCreatures: creatures usingGuard: guard
	creatures size timesRepeat: [ guard wait ].
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Mall methodsFor: 'as yet unclassified'!

allowedMoreMeetings
	^ maxRendezvous > 0!

completeRendezvousOf: aChameneos 
	mustWait := false.
	self rendezvousOccurred.
	 ^secondCreature!

initialize
	mustWait := false.
	firstCall := true.
	bouncer:= Semaphore new.
	guard := Semaphore forMutualExclusion.
	firstCreature := nil.
	secondCreature := nil!

meetSomeoneWith: aChameneos 
 	secondCreature := aChameneos.
	firstCall := true.
	mustWait := true.
	^firstCreature.!

mustWait 
[guard critical: [mustWait and: [ self allowedMoreMeetings ]]]  whileTrue: [ self signal. bouncer wait]!

rendezvousOccurred
	maxRendezvous := maxRendezvous - 1!

signal
bouncer signal. Processor yield.
"bouncer copy do: [:each| bouncer signal. Processor yield.]"!

visitWith: aChameneos 
|partner|
		self mustWait.
		firstCall 
			ifTrue: 
				[ self waitForSomeoneElseWith: aChameneos.
				partner:= guard critical: [self completeRendezvousOf: aChameneos].
				bouncer signal.]
			ifFalse: 
				[ partner:=guard critical: [self meetSomeoneWith: aChameneos].
				bouncer signal].
		^guard critical: [
 maxRendezvous >= 0 
			ifTrue: [ partner ]
			ifFalse: [ nil ].
].!

waitForSomeoneElseWith: aChameneos 
guard critical: [firstCreature := aChameneos.
	firstCall := false.
].
[guard critical: [ firstCall not and: [ self allowedMoreMeetings ] ]] whileTrue: [self signal. bouncer wait]! !

!Mall methodsFor: 'accessing'!

bouncer
	^bouncer!

bouncer: anObject
	bouncer := anObject!

guard
	^ guard!

guard: anObject 
	guard := anObject!

maxRendezvous
	^ maxRendezvous!

maxRendezvous: max
	 maxRendezvous:=max.! !

#{Mall} initialize!

Smalltalk defineClass: #Creature
	superclass: #{Core.Object}
	indexedType: #none
	private: false
	instanceVariableNames: 'creatureName colour selfMet creaturesMet '
	classInstanceVariableNames: ''
	imports: ''
	category: 'chameleon'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Creature class methodsFor: 'as yet unclassified'!

withName: aName colour: aColour
|creature|
creature:=Creature new initialize.
creature name: aName.
creature colour: aColour .
^creature.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Creature methodsFor: 'as yet unclassified'!

if: vis isMeThen: block
(vis isNil not and: [self name=vis name]) ifTrue:[block value].!

initialize
selfMet:=0.
creaturesMet :=0.!

visitMall: mall 

		[	| partner | 
		partner := mall visitWith: self.
		partner ifNotNil: 
			[ self colour: (self colour complementaryColourFor: partner colour).
			self == partner
		ifTrue: [ selfMet := selfMet + 1 ].
			creaturesMet := creaturesMet + 1.
			 ] . 
Processor yield.
		partner isNil. ] whileFalse! !

!Creature methodsFor: 'accessing'!

colour
	^ colour!

colour: anObject
	colour := anObject!

creaturesMet
	^ creaturesMet!

creaturesMet: anObject
	creaturesMet := anObject!

name
	^ creatureName !

name: anObject
	creatureName := anObject!

selfMet
	^ selfMet!

selfMet: anObject
	^ selfMet := anObject! !

Smalltalk defineClass: #ChameneosColour
	superclass: #{Core.Object}
	indexedType: #none
	private: false
	instanceVariableNames: 'color '
	classInstanceVariableNames: ''
	imports: ''
	category: 'chameleon'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!ChameneosColour class methodsFor: 'as yet unclassified'!

createBlue
	"comment stating purpose of message"

	| |
^(super new) color: ColorValue blue.!

createRed
	"comment stating purpose of message"

	| |
^(super new) color: ColorValue red.!

createYellow
	"comment stating purpose of message"

	| |
^(super new) color: ColorValue yellow.!

generateReportOfColours
	| readOut colours red blue yellow |
	red:=ChameneosColour createRed.
     yellow:=ChameneosColour createYellow.
     blue:=ChameneosColour createBlue.
colours:=Array with: blue with: red with: yellow.

	readOut := WriteStream on: String new.

colours do:[:aColour| colours do:[:anotherColour| aColour printOn: readOut.
		readOut nextPutAll: ' + '.
		   anotherColour printOn: readOut.
		 readOut nextPutAll: ' -> '.
		(aColour complementaryColourFor: anotherColour) printOn: readOut.
		readOut cr]].
^readOut.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!ChameneosColour methodsFor: 'as yet unclassified'!

color

^color!

color: aColor

color:=aColor .!

complementaryColourFor: aChameneosColour
	"determine the complementary colour defined as..."
(self hasSameColorAs: aChameneosColour) ifTrue:[^self].
self isBlue ifTrue: [aChameneosColour  isRed ifTrue: [^self class createYellow] ifFalse: [^self class createRed.]].
self isRed ifTrue: [aChameneosColour  isBlue ifTrue: [^self class createYellow] ifFalse: [^self class createBlue.]].
aChameneosColour  isBlue ifTrue: [^self class createRed] ifFalse: [^self class createBlue.].
!

hasSameColorAs: aChameneos

^self color=aChameneos color.!

isBlue

^self color=ColorValue blue.!

isRed

^self color=ColorValue red.!

isYellow

^self color=ColorValue yellow.!

printOn: aStream

aStream nextPutAll: (self color class constantNameFor: self color).! !
