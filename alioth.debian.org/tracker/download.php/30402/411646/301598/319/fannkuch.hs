-- contributed by Jeff Newbern
-- compile with: ghc -O3 fannkuch.hs -o fannkuch

-- This is an attempt to improve over generate-and-test by instead searching the space
-- of permutations backward from the final state while tracking the depth of each
-- intermediate solution.  We use a special "Any" value to signify that any unassigned
-- value can occupy a slot, and we search only prior states that could actually lead to
-- this state through a flip of assigned values, or a flip requiring the assignment
-- of an unassigned value to slot 1.

-- This code uses lists to implement the permutation data, etc., which imposes O(n)
-- algorithms for indexing and deletion.  Replacing the lists with IOUArrays would
-- allow O(1) access, but make the code messier.  As it is, this code is 6-8 times
-- faster than the generate-and-test approach on the inputs I tried.

import System(getArgs)
import Data.List(delete)

data Val = Num Int | Any deriving Eq
data State = ST { depth :: Int, permutation :: [Val], unassigned :: [Int], assigned :: [Int] }

-- generate a list of the deepest initial states that can reach a goal state
-- through flips and assignments.
traceBackFrom :: State -> [State]
traceBackFrom s@(ST _ vs us as) =
  let flipped            = filter (isSame vs) as
      assignedAndFlipped = filter (isAny vs)  us
  in if (null flipped) && (null assignedAndFlipped)
       then [s]
       else (concatMap traceBackFrom (map (unflip s) flipped)) ++ 
            (concatMap traceBackFrom (map (unflipAndAssign s) assignedAndFlipped))
  where isAny  vs n = vs!!(n-1) == Any
        isSame vs n = vs!!(n-1) == (Num n)
        unflip (ST d vs us as) n = let (start,rest) = splitAt n vs
                                       start' = (Num n):(tail (reverse start))
                                   in ST (d+1) (start' ++ rest) us as
        unflipAndAssign (ST d vs us as) n = let (start,rest) = splitAt n vs
                                                start' = (Num n):(tail (reverse start))
                                            in ST (d+1) (start' ++ rest) (delete n us) (n:as)

main = do n <- getArgs >>= readIO.head
          let goalState = ST 0 ((Num 1):(replicate (n-1) Any)) [2..n] []
          let deepestInitialStates = traceBackFrom goalState
          let m = maximum (map depth deepestInitialStates)
          putStrLn $ "Pfannkuchen(" ++ (show n) ++ ") = " ++ (show m)
