program harmonic;

{ submitted by Ian Osgood }
{$mode objfpc}

uses SysUtils;

var i: longint; sum, d: double;
begin
  for i := 1 to StrToInt(paramstr(1)) do
  begin
    d := d + 1.0;
    sum := sum + 1.0/d;
  end;
  writeln(sum:0:9);
end.

