#!/usr/bin/swipl -q -O -t halt -g go -f

%%% The Great Computer Language Shootout
%%% http://shootout.alioth.debian.org/
%%% $Id: $
%%%
%%% Contributed by: Sergei Matusevich 2005
%%% vim: syntax=prolog ts=2

flip( [1 | _], Res, Res ) :- !.

flip( [N | List], Count, Res ) :-
  append( Head, Tail, [N | List] ),
  length( Head, N ),
  reverse( Head, RevHead ),
  append( RevHead, Tail, NewList ),
  NewCount is Count + 1,
  !, flip( NewList, NewCount, Res ).

pfannkuchen( Len, _ ) :-
  numlist( 1, Len, L0 ),
  recorda( max_flips, 0 ),
  recorda( perm_count, 0 ),
  permutation( L0, L ),
  print_perm( L ),
  flip( L, 0, Count ),
  recorded( max_flips, Max, Ref ),
  Count > Max,
  recorda( max_flips, Count ),
  erase( Ref ), fail.

pfannkuchen( _, Res ) :-
  recorded( max_flips, Res ).

print_perm( _ ) :-
  recorded( perm_count, Count ),
  Count >= 30, !.

print_perm( L ) :-
  recorded( perm_count, Count ),
  Count < 30,
  Count2 is Count + 1,
  recorda( perm_count, Count2 ),
  print_list( L ), !.

print_list( [] ) :- nl.

print_list( [H | T] ) :-
  write( H ),
  print_list( T ).

go :-
  current_prolog_flag( argv, Argv ),
  append( _, [ --, Arg | _ ], Argv ),
  atom_number( Arg, Param ),
  pfannkuchen( Param, Res ),
  format( 'Pfannkuchen(~d) = ~d\n', [Param, Res] ).

