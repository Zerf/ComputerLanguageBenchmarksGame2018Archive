﻿/*
  The Computer Language Benchmarks Game
  http://shootout.alioth.debian.org/
  Modifications to Josh Goldfoot version by Byron Foster by Miguel de Icaza
  which was modified originally from the Nice entry by Isaac Gouy
  fixes by Uffe Seerup
*/
using System;
using System.Text;
using System.Text.RegularExpressions;

public class regexdna
{

    public static void Main(String[] args)
    {
        StringBuilder sb = new StringBuilder();
        int initialLength = 0;
        for (String line; (line = Console.ReadLine()) != null;  )
        {
            initialLength += line.Length + 1;
            if (line.StartsWith(">")) continue;
            sb.Append(line);
        }

        String sequence = sb.ToString();
        int codeLength = sequence.Length;

        String[] variants =
      {
        "agggtaaa|tttaccct" ,"[cgt]gggtaaa|tttaccc[acg]", "a[act]ggtaaa|tttacc[agt]t",
        "ag[act]gtaaa|tttac[agt]ct", "agg[act]taaa|ttta[agt]cct", "aggg[acg]aaa|ttt[cgt]ccct",
        "agggt[cgt]aa|tt[acg]accct", "agggta[cgt]a|t[acg]taccct", "agggtaa[cgt]|[acg]ttaccct"
      };


        int count;
        foreach (string v in variants)
        {
            count = 0;
            Regex r = new Regex(v, RegexOptions.Compiled);

            for (Match m = r.Match(sequence); m.Success; m = m.NextMatch()) count++;
            Console.WriteLine("{0} {1}", v, count);
        }

        sb = new StringBuilder(4096);
        foreach (char c in sequence)
        {
            switch (c)
            {
                case 'B': sb.Append("(c|g|t)"); break;
                case 'D': sb.Append("(a|g|t)"); break;
                case 'H': sb.Append("(a|c|t)"); break;
                case 'K': sb.Append("(g|t)"); break;
                case 'M': sb.Append("(a|c)"); break;
                case 'N': sb.Append("(a|c|g|t)"); break;
                case 'R': sb.Append("(a|g)"); break;
                case 'S': sb.Append("(c|g)"); break;
                case 'V': sb.Append("(a|c|g)"); break;
                case 'W': sb.Append("(a|t)"); break;
                case 'Y': sb.Append("(c|t)"); break;
                default: sb.Append(c); break;
            }
        }

        Console.WriteLine("\n{0}\n{1}\n{2}",
           initialLength, codeLength, sb.Length);

    }
}
