-----------------------------------------------------------
-- BinaryTrees
--
-- Ada 95 (GNAT)
--
-- Contributed by Jim Rogers
-----------------------------------------------------------

with Tree_Node_Pckg; use Tree_Node_Pckg;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Ada.Command_Line; use Ada.Command_Line;
with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;

procedure Binarytrees is
   Min_Depth : constant Positive := 4;
   Check_Factor : constant Positive := 64;
   N : Natural := 1;
   Stretch_Tree : Tree_Node;
   Long_Lived_Tree : Tree_Node;
   Short_Lived_Tree_1 : Tree_Node;
   Short_Lived_Tree_2 : Tree_Node;
   Max_Depth : Positive;
   Stretch_Depth : Positive;
   Check : Integer;
   Depth : Natural;
   Iterations : Positive;
begin
   if Argument_Count > 0 then
      N := Positive'Value(Argument(1));
   end if;
   Max_Depth := Positive'Max(Min_Depth + 1, N);
   Stretch_Depth := Max_Depth + 1;
   Stretch_Tree := Top_Down_Tree(0, Stretch_Depth);
   if Stretch_Tree /= Null_Tree_Node then
      Clear(Stretch_Tree);
   end if;
   Long_Lived_Tree := Top_Down_Tree(-1, Min_Depth);
   Depth := Min_Depth;
   while Depth <= Max_Depth loop
      Iterations := 2**(Max_Depth - Depth + Min_Depth);
      Check := 0;
      for I in 1..Iterations loop
         Short_Lived_Tree_1 := Top_Down_Tree(I, Depth);
         Short_Lived_Tree_2 := Bottom_Up_Tree(I, Depth);
         Check := Check + Node_Item(Short_Lived_Tree_1) / Check_Factor;
         Check := Check + Node_Item(Short_Lived_Tree_2) / Check_Factor;
         Clear(Short_Lived_Tree_1);
         Clear(Short_Lived_Tree_2);
      end loop;
      Put(Item => Iterations * 2, Width => 0);
      Put(Ht & " trees of depth ");
      Put(Item => Depth, Width => 0);
      Put(Ht & " check: ");
      Put(Item => Check, Width => 0);
      New_Line;
      Depth := Depth + 2;
   end loop;
   if Long_Lived_Tree /= Null_Tree_Node and
         Stretch_Tree = Null_Tree_Node then
      Put_Line("OK");
   end if;
end BinaryTrees;


-----------------------------------------------------------
-- BinaryTrees
--
-- Ada 95 (GNAT)
--
-- Contributed by Jim Rogers
-----------------------------------------------------------

package Tree_Node_Pckg is 
   type Tree_Node is private;
   Null_Tree_Node : constant Tree_Node;
   function Top_Down_Tree(Item : Integer; Depth : Natural) return Tree_Node;
   function Bottom_Up_Tree(Item : Integer; Depth : Natural) return Tree_Node;
   function Node_Item(Item : Tree_Node) return Integer;
   procedure Clear(Item : in out Tree_Node);
private
   type Node;
   type Tree_Node is access Node;
   type Node is record
      Left : Tree_Node := null;
      Right : Tree_Node := null;
      Item : Integer := 0;
   end record;
   Null_Tree_Node : constant Tree_Node := null;
end Tree_Node_Pckg;

-----------------------------------------------------------
-- BinaryTrees
--
-- Ada 95 (GNAT)
--
-- Contributed by Jim Rogers
-----------------------------------------------------------

with Ada.Unchecked_Deallocation;

package body Tree_Node_Pckg is
   procedure Free is new Ada.Unchecked_Deallocation(Node, Tree_Node);
   procedure Clear(Item : in out Tree_Node) is
   begin
      if Item /= null then
         Clear(Item.Left);
         Clear(Item.Right);
         Free(Item);
      end if;
   end Clear;
   procedure To(Item : in out Tree_Node; Depth : Natural) is
      Temp : Natural := Depth;
   begin
      if Depth > 0 then
         if Item = null then
            Item := new Node;
         end if;
         Temp := Depth - 1;
         Item.Left := new Node'(null, null, Item.Item);
         Item.Right := new Node'(null, null, Item.Item);
         To(Item.Left, Temp);
         To(Item.Right, Temp);
      end if;
   end To;
   function Top_Down_Tree(Item : Integer; Depth : Natural) return Tree_Node is
      Temp : Tree_Node := new Node'(null, null, Item);
   begin
      To(Temp, Depth);
      return Temp;
   end Top_Down_Tree;
   function Bottom_Up_Tree(Item : Integer; Depth : Natural) return Tree_Node is
   begin
      if Depth > 0 then
         return new Node'(Bottom_Up_Tree(Item, Depth - 1),
            Bottom_Up_Tree(Item, Depth - 1),
            Item);
      else
         return new Node'(null, null, Item);
      end if;
   end Bottom_Up_Tree;
   function Node_Item(Item : Tree_Node) return Integer is
   begin
      if Item /= null then
         if Item.Left = null then
            return Item.Item;
         else
            return Item.Item + Node_Item(Item.Left) - Node_Item(Item.Right);
         end if;
      else
         return 0;
      end if;
   end Node_Item;
end Tree_Node_Pckg;




