/*
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * Written by Jon Harrop, 2005
 * Compile: g++ -O2 -o nsieve_bits_g++ nsieve_bits.cpp
 */

#include <iostream>
#include <vector>
#include <cstring>

int nsieve(int n) {
  std::vector<bool> t(n+1);
  int count = 0;

  for (int i=2; i<=n; ++i)
    if (! t.at(i)) {
      ++count;
      for (int j=i*2; j<=n; j+=i)
	t.at(j) = true;
    }

  return count;
}

void test(int n) {
  int m = (1 << n) * 10000;
  std::cout << "Primes up to " << m << " " << nsieve(m) << std::endl;
}

int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "usage: nsieve <n>\n";
    return 1;
  }
  int n = atoi(argv[1]);
  test(n);
  if (n >= 1) test(n - 1);
  if (n >= 2) test(n - 2);
  exit(0);
}
