/*
 * The Computer Lannguage Shootout
 * http://shootout.alioth.debian.org/
 * Contributed by Erik Søe Sørensen
 *
 * "fannkuch"	for C gcc (C99)
 */

#include <stdlib.h>
#include <stdio.h>

static int* count;
static int* perm;
static int r;
static int N;

static int succ() {
    while (r>1) {r--; count[r]=r+1;}
    do {
	if (r>=N) return 1; /* Done. */

	/* Rotate lower r elements downwards: */
	int tmp = perm[0];
	for (int i=0; i<r; i++)
	    perm[i] = perm[i+1];
	perm[r] = tmp;

	/* Register the rotation
	 * - repeat with higher r if the rotation was completed: */
    } while (--count[r++] == 0);
    r--;
    return 0;
}


void printperm(int p[]) {
    for (int i=0; i<N; i++)
	printf("%d", p[i]);
    printf("\n");
}

int testperm(int a[]) {
    int flips = 0;

    int k;
    while ((k=a[0]) > 1) {
	int i=0, j=k-1;
	while (i<j) {
	    int tmp = a[i]; a[i] = a[j]; a[j] = tmp;
	    i++; j--;
	}
	++flips;
    }
    return flips;
}

int main(int argc, const char** argv) {
    int n = (argc>1)? atoi(argv[1]) : 0;

    N = r = n;
    perm  = calloc(n, sizeof(*perm));
    count = calloc(n, sizeof(*count));

    /* Init permutation: */
    for (int i=0; i<N; i++) perm[i]=i+1;

    int maxFlips = 0;
    for (int i=0 ;; i++) {
	if (i<30) printperm(perm);
	if (succ()) break;

	if (perm[0]==1 || perm[n-1]==n) continue; /* Shortcut */

	/* Make a copy: */
	int tmp[N];
	for (int j=0; j<N; j++) tmp[j]=perm[j];

	/* Try it out: */
	int flips = testperm(tmp);
	if (flips > maxFlips) maxFlips = flips;
    }

    printf("Pfannkuchen(%d) = %d\n", n, maxFlips);
}
