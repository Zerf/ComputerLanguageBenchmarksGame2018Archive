% The Computer Language Benchmarks Game
% http://shootout.alioth.debian.org/
% Contributed by: Sergei Matusevich 2007
% Parallelized by Fredrik Svahn 2008

-module(regexdna).

-export([main/1]).

-define( VARIANTS,
  [ "agggtaaa|tttaccct",
    "[cgt]gggtaaa|tttaccc[acg]",
    "a[act]ggtaaa|tttacc[agt]t",
    "ag[act]gtaaa|tttac[agt]ct",
    "agg[act]taaa|ttta[agt]cct",
    "aggg[acg]aaa|ttt[cgt]ccct",
    "agggt[cgt]aa|tt[acg]accct",
    "agggta[cgt]a|t[acg]taccct",
    "agggtaa[cgt]|[acg]ttaccct" ] ).

read_lines(File, SzTotal, [Seg|Segz]) ->
  case io:get_line(File, '') of
    eof -> {SzTotal, [Seg|Segz]};
    Str ->
      Len = size(Str),
      Eol = Len - 1,
      read_lines( File, SzTotal + Len,
        case Str of
          <<">",_/binary>>         ->     [[],Seg|Segz];
          <<Trim:Eol/binary,"\n">> -> [[Trim|Seg]|Segz];
          _                        ->  [[Str|Seg]|Segz]
        end )
  end.

main(_) ->
  io:setopts(standard_io, [binary]),
  {SzTotal, [S3Raw,S2Raw,S1Raw|_]} = read_lines(standard_io, 0, [[]]),
  S1 = lists:reverse(S1Raw),
  S2 = lists:reverse(S2Raw),
  S3 = lists:reverse(S3Raw),

  L2  = iolist_size(S2),
  L13 = iolist_size(S1) + iolist_size(S3),
	
  Server = self(),
  S23 = iolist_to_binary([S2,S3]),
  Pids = [ spawn( fun()-> Server ! { self(), Re, 
                                     case re:run(S23, Re, [global]) of
                                       { match, Res } -> length(Res);
                                       _ -> 0
                                     end }
                  end ) || Re <- ?VARIANTS],

  L2Subst = length( lists:flatmap(
    fun(Ch) ->
      case Ch of
        $B -> "(c|g|t)";
        $D -> "(a|g|t)";
        $H -> "(a|c|t)";
        $K -> "(g|t)";
        $M -> "(a|c)";
        $N -> "(a|c|g|t)";
        $R -> "(a|g)";
        $S -> "(c|g)";
        $V -> "(a|c|g)";
        $W -> "(a|t)";
        $Y -> "(c|t)";
        XX -> [XX]
      end
    end, binary_to_list(iolist_to_binary(S2)) ) ),

  [receive {Pid, Re, M} -> io:format("~s ~w~n", [Re, M]) end || Pid <- Pids],
  io:format("~n~w~n~w~n~w~n", [SzTotal, L13 + L2, L13 + L2Subst]),
  halt(0).

