//	The Computer Language Shootout
//	http://shootout.alioth.debian.org/
//	contributed by Paul Kitchin
//	compile with -lpthread

#include <iostream>
#include <sstream>
#include <pthread.h>

unsigned int const number_of_threads = 500;
unsigned int const thread_stack_size = 100 * 1024;
unsigned int number_of_messages_per_thread;
unsigned int message_total;

unsigned char volatile signals[number_of_threads];
unsigned int volatile messages[number_of_threads];

void * thread_function(void * argument)
{
	unsigned int thread_id = reinterpret_cast< unsigned int >(argument);
	unsigned char volatile & thread_signal(signals[thread_id]);
	unsigned int volatile & thread_message(messages[thread_id]);
	unsigned int messages_received = 0;
	if (thread_id + 1 != number_of_threads)
	{
		unsigned char volatile & next_thread_signal(signals[thread_id + 1]);
		unsigned int volatile & next_thread_message(messages[thread_id + 1]);
		while (messages_received != number_of_messages_per_thread)
		{
			if (thread_signal)
			{
				while (next_thread_signal)
				{
					sched_yield();
				}
				next_thread_message = thread_message + 1;
				thread_message = 0;
				next_thread_signal = 1;
				thread_signal = 0;
				++messages_received;
			}
			else
			{
				sched_yield();
			}
		}
	}
	else
	{
		while (messages_received != number_of_messages_per_thread)
		{
			if (thread_signal)
			{
				message_total += thread_message + 1;
				thread_message = 0;
				thread_signal = 0;
				++messages_received;
			}
			else
			{
				sched_yield();
			}
		}
	}
	return 0;
}

int main(int argc, char * * argv)
{
	if (argc != 2)
	{
		std::cout << "Usage: " << argv[0] << " <n>\n";
		return 1;
	}
	{
		std::istringstream convertor(argv[1]);
		if (!(convertor >> number_of_messages_per_thread) || !convertor.eof())
		{
			std::cout << "Usage: " << argv[0] << " <n>\n";
			std::cout << "\tn must be an integer\n";
			return 1;
		}
	}
	pthread_attr_t thread_attributes;
	if (pthread_attr_init(&thread_attributes) != 0 || pthread_attr_setstacksize(&thread_attributes, thread_stack_size) != 0)
	{
		std::cout << "pthread attribute error\n";
		return 1;
	}
	pthread_t threads[number_of_threads];
	for (unsigned int thread_number = 0; thread_number < number_of_threads; ++thread_number)
	{
		if (pthread_create(&threads[thread_number], &thread_attributes, thread_function, reinterpret_cast< void * >(thread_number)) != 0)
		{
			std::cout << thread_number << "pthread thread creation error\n";
			return 1;
		}
	}
	for (unsigned int message_number = 0; message_number < number_of_messages_per_thread; ++message_number)
	{
		while (signals[0])
		{
			sched_yield();
		}
		messages[0] = 0;
		signals[0] = 1;
	}
	for (unsigned int thread_number = 0; thread_number < number_of_threads; ++thread_number)
	{
		void * return_value;
		if (pthread_join(threads[thread_number], &return_value) != 0)
		{
			std::cout << "pthread thread completion error\n";
			return 1;
		}
	}
	std::cout << message_total << '\n';
}
