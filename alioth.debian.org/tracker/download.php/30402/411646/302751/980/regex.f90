! The Computer Language Shootout Benchmarks
! http://shootout.alioth.debian.org/
!
! contributed by Steve Decker
! main program based on the Python version by Dominique Wahli
! compilation:
!    g95 -O1 -funroll-loops -fomit-frame-pointer regex.f90
!    ifort -O3 regex.f90

module string_mod
  implicit none
  
  interface indx
     module procedure indexC
     module procedure indexArr
  end interface
  
contains

  pure function tolower(line)
    character(len=*), intent(in) :: line
    character(len=len(line)) :: tolower
    
    character(len=122), parameter :: Table = repeat(" ", 61) //  &
         ">  abcdefghijklmnopqrstuvwxyz" // repeat(" ", 6) //  &
         "abcdefghijklmnopqrstuvwxyz"

    integer :: i
    
    do i = 1, len(line)
       tolower(i:i) = Table(iachar(line(i:i)):iachar(line(i:i)))
    end do
  end function tolower

  pure integer function indexC(string, find)
    character, dimension(:), intent(in) :: string
    character,               intent(in) :: find

    integer :: i
    
    indexC = 0
    do i = 1, size(string)
       if (string(i) == find) then
          indexC = i
          exit
       end if
    end do
  end function indexC

  ! The Boyer-Moore method
  pure integer function indexArr(y, x)
    character, dimension(:), intent(in) :: y, x
    
    integer,   dimension(0:127)   :: bmbc
    integer,   dimension(size(x)) :: bmgs
    character, dimension(size(x)) :: string
    integer :: ysize, xsize, i, j, k

    ysize = size(y)
    xsize = size(x)

    bmbc = xsize
    do i = 1, xsize-1
       bmbc(iachar(x(i))) = xsize - i
    end do

    do i = 1, xsize
       string(:i) = x(xsize-i+1:xsize)
       do j = 1, xsize-1
          k = i + j - 1
          if (xsize-k > 0) then
             if (string(1) == x(xsize-k)) cycle
          end if
          if (all(string(2+max(0,k-xsize):i) == x(max(1,xsize-k+1):xsize-j))) exit
       end do
       bmgs(xsize-i+1) = j
    end do

    indexArr = 0
    j = 1
    do
       do i = xsize, 1, -1
          if (x(i) /= y(i+j-1)) exit
       end do
       if (i == 0) then
          indexArr = j
          exit
       end if
       j = j + max(bmgs(i), bmbc(iachar(y(i+j-1))) - xsize + i)
       if (j > ysize - xsize + 1) exit
    end do
  end function indexArr
end module string_mod

module regex_subs
  use string_mod
  implicit none
  
  integer,   parameter :: MaxCharLen = 8
  character, parameter :: NewLine = achar(10)
  
contains

  pure subroutine sub(pattern, replacement, string, length)
    character(len=*),        intent(in)    :: pattern, replacement
    character, dimension(:), intent(inout) :: string
    integer,                 intent(inout) :: length

    character(len=*),        parameter :: Separator = "|", Star = ".*"
    
    character, dimension(:), allocatable :: rep, p1a, p2a
    character, dimension(0) :: null
    integer :: head, tail, pos, ind1, ind2
    logical :: done
    character(len=MaxCharLen) :: p1, p2

    head = 1
    done = .false.
    p1 = repeat(" ", MaxCharLen)
    p2 = repeat(" ", MaxCharLen)
    do
       tail = index(pattern(head:), Separator) - 1
       if (tail == -1) then
          tail = len(pattern)
          done = .true.
       end if
       pos = index(pattern(head:tail), Star) - 1
       if (pos == -1) then
          if (pattern(head:tail) == "(\n)") then
             call find_replace_all(string, length, NewLine, null)
          else
             allocate(rep(len_trim(replacement)))
             rep = transfer(trim(replacement), rep)
             call find_replace_all(string, length, pattern(head:head), rep)
             deallocate(rep)
          end if
       else
          call process_star(p1, head, pos)
          call process_star(p2, pos+len(Star)+1, tail)
          allocate(p1a(len_trim(p1)), p2a(len_trim(p2)))
          p1a = transfer(trim(p1), p1a)
          p2a = transfer(trim(p2), p2a)
          do
             ind1 = indx(string(:length), p1a)
             if (ind1 == 0) exit
             ind2 = indx(string(ind1:length), p2a) + ind1
             if (ind2 == ind1) exit
             string(ind1:length) = string(ind2:ind2+length-ind1)
             length = length - (ind2-ind1)
          end do
       end if
       if (done) exit
       head = tail + 2
    end do
    
  contains
    
    pure subroutine process_star(p, a, b)
      character(len=*), intent(inout) :: p
      integer,          intent(in)    :: a, b
      
      integer :: i, j
      
      j = 1
      do i = a, b
         if (pattern(i:i) /= "(" .and. pattern(i:i) /= ")") then
            p(j:j) = pattern(i:i)
            j = j + 1
         end if
      end do
      if (p == "\n") p = NewLine
    end subroutine process_star
  end subroutine sub
  
  pure integer function num_matches(pattern, string, length)
    character(len=*),        intent(in) :: pattern
    character, dimension(:), intent(in) :: string
    integer,                 intent(in) :: length
    
    character(len=*), parameter :: Separator = "|"

    character, dimension(:), allocatable :: findArr
    integer :: head, tail, lb, rb, i, ind1, ind2
    logical :: done
    character(len=MaxCharLen) :: finds
    
    num_matches = 0
    head = 1
    done = .false.
    do
       tail = index(pattern(head:), Separator) - 1
       if (tail == -1) then
          tail = len(pattern)
          done = .true.
       end if
       lb = index(pattern(head:tail), "[")
       rb = index(pattern(head:tail), "]")
       finds(1:lb-1) = pattern(head:head+lb-2)
       finds(lb+1:) = pattern(head+rb:tail)
       if (lb == 0) rb = 2
       do i = 1, rb-lb-1
          if (lb /= 0) finds(lb:lb) = pattern(head+lb+i-1:head+lb+i-1)
          allocate(findArr(len_trim(finds)))
          findArr = transfer(finds(1:len_trim(finds)), findArr)
          ind1 = 1
          do
             ind2 = indx(string(ind1:length), findArr)
             if (ind2 == 0) exit
             num_matches = num_matches + 1
             ind1 = ind1 + ind2
          end do
          deallocate(findArr)
       end do
       if (done) exit
       head = tail + 2
    end do
  end function num_matches

  pure subroutine find_replace_all(string, length, find, rep)
    character, dimension(:), intent(inout) :: string
    integer,                 intent(inout) :: length
    character,               intent(in)    :: find
    character, dimension(:), intent(in)    :: rep

    character, dimension(size(string)) :: orig
    integer :: lenrep, sprev, oprev, ocur, scur

    lenrep = size(rep)
    sprev = 0
    oprev = 1
    orig = string
    do
       ocur = indx(orig(oprev:length), find) + oprev - 1
       if (ocur == oprev - 1) exit
       scur = sprev + ocur - oprev
       string(sprev+1:scur) = orig(oprev:ocur-1)
       sprev = scur + lenrep
       string(scur+1:sprev) = rep
       oprev = ocur + 1
    end do
    ocur = length + 1
    scur = sprev + ocur - oprev
    string(sprev+1:scur) = orig(oprev:ocur-1)
    length = scur
  end subroutine find_replace_all
end module regex_subs

program regex
  use regex_subs
  implicit none

  integer, parameter :: NumVariants = 9, NumSubsts = 11, InitBufSize = 53248, LineLen = 60
  character(len=25), dimension(NumVariants), parameter :: Variants = (/  &
       "agggtaaa|tttaccct        ", "[cgt]gggtaaa|tttaccc[acg]",  &
       "a[act]ggtaaa|tttacc[agt]t", "ag[act]gtaaa|tttac[agt]ct",  &
       "agg[act]taaa|ttta[agt]cct", "aggg[acg]aaa|ttt[cgt]ccct",  &
       "agggt[cgt]aa|tt[acg]accct", "agggta[cgt]a|t[acg]taccct",  &
       "agggtaa[cgt]|[acg]ttaccct" /)
  character(len=9), dimension(2,NumSubsts), parameter :: Subst = reshape( (/  &
       "b        ", "(c|g|t)  ", "d        ", "(a|g|t)  ", "h        ",  &
       "(a|c|t)  ", "k        ", "(g|t)    ", "m        ", "(a|c)    ",  &
       "n        ", "(a|c|g|t)", "r        ", "(a|g)    ", "s        ",  &
       "(c|g)    ", "v        ", "(a|c|g)  ", "w        ", "(a|t)    ",  &
       "y        ", "(c|t)    " /), (/ 2, NumSubsts /) )

  character, dimension(:), allocatable :: seq, temp
  integer                :: stat, i = 0, j = 0, ilen, clen
  character(len=LineLen) :: line

  allocate(seq(InitBufSize))
  seq = " "
  do
     i = j + 1
     read (*, "(a)", iostat=stat) line
     if (stat /= 0) exit
     j = i + len_trim(line)
     if (j > size(seq)) then
        allocate(temp(size(seq)))
        temp = seq
        deallocate(seq)
        allocate(seq(2*size(temp)))
        seq(:size(temp)) = temp
        seq(size(temp)+1:) = " "
        deallocate(temp)
     end if
     seq(i:j) = transfer(trim(tolower(line))//NewLine, seq)
  end do
  allocate(temp(nint(1.35*j)))
  temp(:j) = seq(:j)
  temp(j+1:) = " "
  deallocate(seq)
  allocate(seq(size(temp)))
  seq = temp
  deallocate(temp)
  ilen = j

  call sub("(>.*\n)|(\n)", "", seq, j)
  clen = j

  do i = 1, NumVariants
     write (*, "(a,1x,i0)") trim(Variants(i)), num_matches(trim(Variants(i)), seq, j)
  end do

  do i = 1, NumSubsts
     call sub(Subst(1,i), Subst(2,i), seq, j)
  end do

  write (*, "(a)") ""
  write (*, "(i0)") ilen
  write (*, "(i0)") clen
  write (*, "(i0)") j
end program regex
