-----------------------------------------------------------------------
-- The Great Computer Language Shootout
-- http://shootout.alioth.debian.org/
--
-- contributed bh Jim Rogers
-----------------------------------------------------------------------
with Ada.Text_Io;
use Ada.Text_Io;
with Ada.Integer_Text_Io;
use Ada.Integer_Text_Io;
with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;
with Gnat.Regpat;
use Gnat.Regpat;

procedure Regexdna is

   type Sequence_Lines is array (Positive range <>) of Unbounded_String;
   function Length (
         Item : in     Sequence_Lines)
     return Natural is
      Sum : Natural := 0;
   begin
      for I in Item'range loop
         Sum := Sum + Length(Item(I));
      end loop;
      return Sum;
   end Length;

      function Replace(Item : in Unbounded_String;
            Index : Positive;
            Substitution : Unbounded_String) return Unbounded_String is
         Temp : Unbounded_String;
      begin
         if Index = 1 then
            Temp := Substitution & Slice(Item, 2, Length(Item));
         else
            Temp := Slice(Item, 1, Index - 1) &
               Substitution & Slice(Item, Index + 1, Length(Item));
         end if;
         return Temp;
      end Replace;

      pragma Inline (Replace);

   Variants : constant
   array (Natural range <>) of Unbounded_String := (
      To_Unbounded_String("agggtaaa|tttaccct"),
      To_Unbounded_String("[cgt]gggtaaa|tttaccc[acg]"),
      To_Unbounded_String("a[act]ggtaaa|tttacc[agt]t"),
      To_Unbounded_String("ag[act]gtaaa|tttac[agt]ct"),
      To_Unbounded_String("agg[act]taaa|ttta[agt]cct"),
      To_Unbounded_String("aggg[acg]aaa|ttt[cgt]ccct"),
      To_Unbounded_String("agggt[cgt]aa|tt[acg]accct"),
      To_Unbounded_String("agggta[cgt]a|t[acg]taccct"),
      To_Unbounded_String("agggtaa[cgt]|[acg]ttaccct"));
   Count : Natural;

   type Iub is
      record
         Code         : Unbounded_String;
         Alternatives : Unbounded_String;
   end record;
   subtype Codes_Index is Natural range 0..10;
   type Codes_Array is array (Codes_Index) of Iub;
   Codes : constant Codes_Array := (
      (To_Unbounded_String ("B"), To_Unbounded_String ("(c|g|t)")), 
      (To_Unbounded_String ("D"), To_Unbounded_String ("(a|g|t)")), 
      (To_Unbounded_String ("H"), To_Unbounded_String ("(a|c|t)")), 
      (To_Unbounded_String ("K"), To_Unbounded_String ("(g|t)")), 
      (To_Unbounded_String ("M"), To_Unbounded_String ("(a|c)")), 
      (To_Unbounded_String ("N"), To_Unbounded_String ("(a|c|g|t)")), 
      (To_Unbounded_String ("R"), To_Unbounded_String ("(a|g)")), 
      (To_Unbounded_String ("S"), To_Unbounded_String ("(c|g)")), 
      (To_Unbounded_String ("V"), To_Unbounded_String ("(a|c|g)")), 
      (To_Unbounded_String ("W"), To_Unbounded_String ("(a|t)")), 
      (To_Unbounded_String ("Y"), To_Unbounded_String ("(c|t)")));
      
   type Matcher_Array is array(Codes_Index) of Pattern_Matcher(1024);
   Compiled_Codes : Matcher_Array;

   Sequence       : Sequence_Lines (1 .. 1_000_000) := (others => Null_Unbounded_String);
   Continuous     : Unbounded_String;
   Initial_Length : Natural;
   Line_Length    : Natural;
   Line           : String (1 .. 80);
   Num_Lines      : Natural := 0;
   R              : Pattern_Matcher (1024);
   Code_Length    : Natural;
   Matches        : Match_Array (0 .. 79);
begin
   -- Read FASTA Sequence
   while not End_Of_File loop
      Ada.Text_Io.Get_Line(
         Item => Line,
         Last => Line_Length);
      Num_Lines := Num_Lines + 1;
      Append(
         Source   => Sequence (Num_Lines),
         New_Item => Line (1 .. Line_Length));
   end loop;
   Initial_Length := Length(Sequence(1..Num_Lines)) + Num_Lines;

   -- remove FASTA descriptions
   Compile(
      Matcher    => R,
      Expression => ">.*",
      Flags      => Case_Insensitive);
   for I in 1..Num_Lines loop
      if Match(R, To_String(Sequence(I))) then
         Sequence(I) := Null_Unbounded_String;
      end if;
      Append(
         Source   => Continuous,
         New_Item => Sequence (I));
   end loop;
   Code_Length := Length(Sequence);

   -- regex match
   for I in Variants'range loop
      Count := 0;
      Compile(
         Matcher    => R,
         Expression => To_String (Variants (I)),
         Flags      => Case_Insensitive);
      for J in 1..Length(Continuous) - 8 loop
         if Match(R, Slice(Continuous, J, J + 8)) then
            Count := Count + 1;
         end if;
      end loop;
      Count := Count / 2;
      Put(To_String(Variants(I) & " "));
      Put(
         Item  => Count,
         Width => 1);
      New_Line;
   end loop;

   -- regex substitution
   for I in Codes_Index loop
      Compile(Matcher => Compiled_Codes(I),
         Expression => To_String(Codes(I).Code),
         Flags => No_Flags);
   end loop;
   
   Continuous := Null_Unbounded_String;
   
   for J in 1..Num_Lines loop
      for I in Codes_Index'range loop
         loop
            Match(
               Self    => Compiled_Codes(I),
               Data    => To_String (Sequence(J)),
               Matches => Matches);
            exit when Matches(0) = No_Match;
            for K in reverse Matches'range loop
               if Matches(K).First >= 1 then
                  Sequence(J) := Replace(
                     Item => Sequence(J),
                     Index  => Matches(K).First,
                     Substitution  => Codes(I).Alternatives);
               end if;
            end loop;
         end loop;
      end loop;
      Append(Source => Continuous, New_Item => Sequence(J));
   end loop;

   New_Line;
   Put(
      Item  => Initial_Length,
      Width => 1);
   New_Line;
   Put(
      Item  => Code_Length,
      Width => 1);
   New_Line;
   Put(
      Item  => Length (Continuous),
      Width => 1);
end Regexdna;

