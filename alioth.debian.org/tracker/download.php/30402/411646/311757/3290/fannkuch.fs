﻿(*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 * F# Version contributed by Alex Peake
 * Single Threaded Version
 *)


#light
open System

let swap n m (arr : int[]) =
    let temp = arr.[n]
    arr.[n] <- arr.[m]
    arr.[m] <- temp
    arr

let flip n (arr : int[]) =
    for i in 0..n / 2 do
        swap i (n - i) arr |> ignore
    arr

let fannkuch N =
    let  a = [| for i in 1..N -> i |]
    let  p = [| for i in 0..N -> i |]
   
    for e in a do printf "%d" e
    do printfn "" 

    let mutable print = 30
    let mutable i = 1
    let mutable j = 0
    let mutable tmp = 0
    let mutable maxFlips = 0
    let mutable numberFlips = 0
    let flipper = Array.create N 0

    while(i < N) do
        p.[i] <- p.[i] - 1
        j <- i % 2 * p.[i]
        tmp <- a.[j]
        a.[j] <- a.[i]
        a.[i] <- tmp
        i <- 1
        while (p.[i] = 0) do 
            p.[i] <- i
            i <- i + 1
        
        for i in 0..N-1 do
            flipper.[i] <- a.[i]
        
        numberFlips <- 0
        while (flipper.[0] <> 1) do
            flip (flipper.[0] - 1) flipper |> ignore
            numberFlips <- numberFlips + 1
        if numberFlips > maxFlips then maxFlips <- numberFlips
        
        if (print > 0) then
            for e in a do printf "%d" e
            do printfn ""
            print <- print - 1
    
    maxFlips

[<EntryPoint>]
let main(args) =
    let x = if args.Length > 0 then int args.[0] else 7
    printfn "Pfannkuchen(%d) = %d" x (fannkuch x)
    0




