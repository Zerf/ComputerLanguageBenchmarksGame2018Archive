REBOL [
  Name: sum-files
  Author: leaflord
  Date: 17-Apr-2009
]

nums: load to-file system/script/args
forall nums [ sum: sum + first nums ]
print sum