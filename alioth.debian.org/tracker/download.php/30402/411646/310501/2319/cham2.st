'From VisualWorks® NonCommercial, 7.4.1 of May 30, 2006 on January 11, 2008 at 4:19:09 am'!


Smalltalk defineClass: #Monitor
	superclass: #{Core.Object}
	indexedType: #none
	private: false
	instanceVariableNames: 'mutex ownerProcess nestingLevel defaultQueue queueDict queuesMutex '
	classInstanceVariableNames: ''
	imports: ''
	category: 'Kernel-Processes'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Monitor class methodsFor: 'as yet unclassified'!

new
^ super new initialize.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Monitor methodsFor: 'accessing'!

cleanup
	self checkOwnerProcess.
	self critical: [self privateCleanup].! !

!Monitor methodsFor: 'initialize-release'!

initialize
	mutex := Semaphore forMutualExclusion.
	queuesMutex := Semaphore forMutualExclusion.
	nestingLevel := 0.! !

!Monitor methodsFor: 'signaling-default'!

signal
	"One process waiting for the default event is woken up."

	^ self signal: nil!

signalAll
	"All processes waiting for the default event are woken up."

	^ self signalAll: nil! !

!Monitor methodsFor: 'signaling-specific'!

signal: aSymbolOrNil
	"One process waiting for the given event is woken up. If there is no process waiting 
	for this specific event, a process waiting for the default event gets resumed."

	| queue |
	self checkOwnerProcess.
	queue := self queueFor: aSymbolOrNil.
	queue isEmpty ifTrue: [queue := self defaultQueue].
	self signalQueue: queue.!

signalAll: aSymbolOrNil
	"All process waiting for the given event or the default event are woken up."

	| queue |
	self checkOwnerProcess.
	queue := self queueFor: aSymbolOrNil.
	self signalAllInQueue: self defaultQueue.
	queue ~~ self defaultQueue ifTrue: [self signalAllInQueue: queue].!

signalReallyAll
	"All processes waiting for any events (default or specific) are woken up."

	self checkOwnerProcess.
	self signalAll.
	self queueDict valuesDo: [:queue |
		self signalAllInQueue: queue].! !

!Monitor methodsFor: 'synchronization'!

critical: aBlock
	"Critical section.
	Executes aBlock as a critical section. At any time, only one process can be executing code 
	in a critical section.
	NOTE: All the following synchronization operations are only valid inside the critical section 
	of the monitor!!"

	| result |
	[self enter.
	result := aBlock value] ensure: [self exit].
	^ result.! !

!Monitor methodsFor: 'waiting-basic'!

wait
	"Unconditional waiting for the default event.
	The current process gets blocked and leaves the monitor, which means that the monitor
	allows another process to execute critical code. When the default event is signaled, the
	original process is resumed."

	^ self waitMaxMilliseconds: nil!

waitUntil: aBlock
	"Conditional waiting for the default event.
	See Monitor>>waitWhile: aBlock."

	^ self waitUntil: aBlock for: nil!

waitWhile: aBlock
	"Conditional waiting for the default event.
	The current process gets blocked and leaves the monitor only if the argument block
	evaluates to true. This means that another process can enter the monitor. When the 
	default event is signaled, the original process is resumed, which means that the condition
	(argument block) is checked again. Only if it evaluates to false, does execution proceed.
	Otherwise, the process gets blocked and leaves the monitor again..."

	^ self waitWhile: aBlock for: nil! !

!Monitor methodsFor: 'waiting-specific'!

waitFor: aSymbolOrNil
	"Unconditional waiting for the non-default event represented by the argument symbol.
	Same as Monitor>>wait, but the process gets only reactivated by the specific event and 
	not the default event."

	^ self waitFor: aSymbolOrNil maxMilliseconds: nil!

waitUntil: aBlock for: aSymbolOrNil
	"Confitional waiting for the non-default event represented by the argument symbol.
	See Monitor>>waitWhile:for: aBlock."

	^ self waitUntil: aBlock for: aSymbolOrNil maxMilliseconds: nil!

waitWhile: aBlock for: aSymbolOrNil
	"Confitional waiting for the non-default event represented by the argument symbol.
	Same as Monitor>>waitWhile:for:, but the process gets only reactivated by the specific 
	event and not the default event."

	^ self waitWhile: aBlock for: aSymbolOrNil maxMilliseconds: nil! !

!Monitor methodsFor: 'waiting-timeout'!

waitFor: aSymbolOrNil maxMilliseconds: anIntegerOrNil
	"Same as Monitor>>waitFor:, but the process gets automatically woken up when the 
	specified time has passed."

	self checkOwnerProcess.
	self waitInQueue: (self queueFor: aSymbolOrNil) maxMilliseconds: anIntegerOrNil.!

waitFor: aSymbolOrNil maxSeconds: aNumber
	"Same as Monitor>>waitFor:, but the process gets automatically woken up when the 
	specified time has passed."

	^ self waitFor: aSymbolOrNil maxMilliseconds: (aNumber * 1000) asInteger!

waitMaxMilliseconds: anIntegerOrNil
	"Same as Monitor>>wait, but the process gets automatically woken up when the 
	specified time has passed."

	^ self waitFor: nil maxMilliseconds: anIntegerOrNil!

waitMaxSeconds: aNumber
	"Same as Monitor>>wait, but the process gets automatically woken up when the 
	specified time has passed."

	^ self waitMaxMilliseconds: (aNumber * 1000) asInteger!

waitUntil: aBlock for: aSymbolOrNil maxMilliseconds: anIntegerOrNil
	"Same as Monitor>>waitUntil:for:, but the process gets automatically woken up when the 
	specified time has passed."

	^ self waitWhile: [aBlock value not] for: aSymbolOrNil maxMilliseconds: anIntegerOrNil!

waitUntil: aBlock for: aSymbolOrNil maxSeconds: aNumber
	"Same as Monitor>>waitUntil:for:, but the process gets automatically woken up when the 
	specified time has passed."

	^ self waitUntil: aBlock for: aSymbolOrNil maxMilliseconds: (aNumber * 1000) asInteger!

waitUntil: aBlock maxMilliseconds: anIntegerOrNil
	"Same as Monitor>>waitUntil:, but the process gets automatically woken up when the 
	specified time has passed."

	^ self waitUntil: aBlock for: nil maxMilliseconds: anIntegerOrNil!

waitUntil: aBlock maxSeconds: aNumber
	"Same as Monitor>>waitUntil:, but the process gets automatically woken up when the 
	specified time has passed."

	^ self waitUntil: aBlock maxMilliseconds: (aNumber * 1000) asInteger!

waitWhile: aBlock for: aSymbolOrNil maxMilliseconds: anIntegerOrNil
	"Same as Monitor>>waitWhile:for:, but the process gets automatically woken up when the 
	specified time has passed."

	self checkOwnerProcess.
	self waitWhile: aBlock inQueue: (self queueFor: aSymbolOrNil) maxMilliseconds: anIntegerOrNil.!

waitWhile: aBlock for: aSymbolOrNil maxSeconds: aNumber
	"Same as Monitor>>waitWhile:for:, but the process gets automatically woken up when the 
	specified time has passed."

	^ self waitWhile: aBlock for: aSymbolOrNil maxMilliseconds: (aNumber * 1000) asInteger!

waitWhile: aBlock maxMilliseconds: anIntegerOrNil
	"Same as Monitor>>waitWhile:, but the process gets automatically woken up when the 
	specified time has passed."

	^ self waitWhile: aBlock for: nil maxMilliseconds: anIntegerOrNil!

waitWhile: aBlock maxSeconds: aNumber
	"Same as Monitor>>waitWhile:, but the process gets automatically woken up when the 
	specified time has passed."

	^ self waitWhile: aBlock maxMilliseconds: (aNumber * 1000) asInteger! !

!Monitor methodsFor: 'private'!

checkOwnerProcess
	self isOwnerProcess
		ifFalse: [self error: 'Monitor access violation'].!

defaultQueue
	defaultQueue ifNil: [defaultQueue := OrderedCollection new].
	^ defaultQueue!

enter
	self isOwnerProcess ifTrue: [
		nestingLevel := nestingLevel + 1.
	] ifFalse: [
		mutex wait.
		ownerProcess := Processor activeProcess.
		nestingLevel := 1.
	].!

exit
	nestingLevel := nestingLevel - 1.
	nestingLevel < 1 ifTrue: [
		ownerProcess := nil.
		mutex signal
	].!

exitAndWaitInQueue: anOrderedCollection maxMilliseconds: anIntegerOrNil
	| lock delay |
	queuesMutex 
		critical: [lock := anOrderedCollection addLast: Semaphore new].
	self exit.
	anIntegerOrNil isNil ifTrue: [
		lock wait
	] ifFalse: [
"		delay := MonitorDelay signalLock: lock afterMSecs: anIntegerOrNil inMonitor: self queue: anOrderedCollection."
self halt.
		lock wait.
		delay unschedule.
	].
	self enter.!

isOwnerProcess
	^ Processor activeProcess == ownerProcess!

privateCleanup
	queuesMutex critical: [
		defaultQueue isEmpty ifTrue: [defaultQueue := nil].
		queueDict ifNotNil: [
			queueDict copy keysAndValuesDo: [:id :queue | 
				queue isEmpty ifTrue: [queueDict removeKey: id]].
			queueDict isEmpty ifTrue: [queueDict := nil].
		].
	].!

queueDict
	queueDict ifNil: [queueDict := IdentityDictionary new].
	^ queueDict.!

queueFor: aSymbol
	aSymbol ifNil: [^ self defaultQueue].
	^ self queueDict 
		at: aSymbol 
		ifAbsent: [self queueDict at: aSymbol put: OrderedCollection new].!

signalAllInQueue: anOrderedCollection
	queuesMutex critical: [
		anOrderedCollection do: [:lock | lock signal].
		anOrderedCollection removeAllSuchThat: [:each | true].
	].!

signalLock: aSemaphore inQueue: anOrderedCollection
	queuesMutex critical: [
		aSemaphore signal.
		anOrderedCollection remove: aSemaphore ifAbsent: [].
	].!

signalQueue: anOrderedCollection
	queuesMutex critical: [
		anOrderedCollection isEmpty ifTrue: [^ self].
		anOrderedCollection removeFirst signal.
	].!

waitInQueue: anOrderedCollection maxMilliseconds: anIntegerOrNil
	self exitAndWaitInQueue: anOrderedCollection maxMilliseconds: anIntegerOrNil.!

waitWhile: aBlock inQueue: anOrderedCollection maxMilliseconds: anIntegerOrNil
	[aBlock value] whileTrue: [self exitAndWaitInQueue: anOrderedCollection maxMilliseconds: anIntegerOrNil].! !

Smalltalk defineClass: #Mall
	superclass: #{Core.Object}
	indexedType: #none
	private: false
	instanceVariableNames: 'mustWait firstCall guard secondCreature firstCreature maxRendezvous bouncer '
	classInstanceVariableNames: ''
	imports: ''
	category: 'chameleon'!

Smalltalk.Mall defineSharedVariable: #Units
	private: false
	constant: false
	category: 'As yet unclassified'
	initializer: nil!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Mall class methodsFor: 'as yet unclassified'!

closeMall: aMall forCreatures: creatures usingGuard: guard
	creatures size timesRepeat: [ guard wait ].
!

createCreaturesWith: aCollectionOfColours
	| aName |
	aName:=0.
	^ aCollectionOfColours collect: 
		[ : aColour | 
		aName := aName + 1.
		Creature 
			withName: aName
			colour: aColour ].
!

generateReportFor: creatures 
	| sum readOut |
	readOut := WriteStream on: String new.
	creatures do: 
		[ : aCreature | 

			 aCreature creaturesMet printOn: readOut.
					readOut space.
aCreature selfMet printOn: readOut.
			readOut cr.
			].
	sum := creatures 
		inject: 0
		into: [ : accum : each | accum + each creaturesMet ].
		readOut space.
	sum printString do: 
		[ : el | 
		readOut
			nextPutAll: (self units at: el digitValue + 1) ;
			space ].
	^ readOut!

generateReportForColours: colours
	| readOut |
	readOut := WriteStream on: String new.
colours do: 
		[ : colour | 
			colour printOn: readOut.
			readOut space ].
	^ readOut!

initialize
"self initialize"
Units := #(
		'zero'
		'one'
		'two'
		'three'
		'four'
		'five'
		'six'
		'seven'
		'eight'
		'nine'
	).!

new
	^super new initialize.!

openMall: aMall forCreatures: creatures usingGuard: sema 
	creatures do: 
		[ : aCreature | 
		[ aCreature visitMall: aMall.
		sema signal ]  fork].!

openMallWith: aCollectionOfColours forNumberOfMeets: aNumber 
	| mall creatures guard outputStream|
	outputStream:= WriteStream on: String new.
	guard := Semaphore new.
	mall := Mall new.
	mall maxRendezvous: aNumber.
	outputStream nextPutAll: (self generateReportForColours: aCollectionOfColours) contents.
	outputStream cr.
		
	creatures := self createCreaturesWith: aCollectionOfColours.
	self openMall: mall forCreatures:  creatures usingGuard: guard.
	self waitForClosingOfMall: mall withCreatures:  creatures usingGuard: guard.
      outputStream nextPutAll: (self generateReportFor: creatures) contents.

^outputStream contents.!

performTest
 | n |
   n := Tests arg.
self runBenchMark: n outputTo: Tests stdout.
^''.!

runBenchMark: number
|firstTestColours secondTestColours|

Transcript show: ChameneosColour generateReportOfColours contents; cr.

firstTestColours :=Array with: ChameneosColour  createBlue with: ChameneosColour createRed  with: ChameneosColour createYellow.

secondTestColours :=(OrderedCollection new) 
add: ChameneosColour  createBlue; add: ChameneosColour createRed ; add: ChameneosColour createYellow; add:ChameneosColour createRed ; add:ChameneosColour createYellow; add:ChameneosColour createBlue; add:ChameneosColour createRed; add:ChameneosColour createYellow; add:ChameneosColour createRed; add:ChameneosColour createBlue; yourself.

Mall openMallWith: firstTestColours  forNumberOfMeets: number.
Transcript cr.
Mall openMallWith: secondTestColours  forNumberOfMeets: number.
!

runBenchMark: number outputTo: aStream
|firstTestColours secondTestColours|

aStream nextPutAll: ChameneosColour generateReportOfColours contents; cr.

firstTestColours :=Array with: ChameneosColour  createBlue with: ChameneosColour createRed  with: ChameneosColour createYellow.

secondTestColours :=(OrderedCollection new) 
add: ChameneosColour  createBlue; add: ChameneosColour createRed ; add: ChameneosColour createYellow; add:ChameneosColour createRed ; add:ChameneosColour createYellow; add:ChameneosColour createBlue; add:ChameneosColour createRed; add:ChameneosColour createYellow; add:ChameneosColour createRed; add:ChameneosColour createBlue; yourself.

aStream nextPutAll: (Mall openMallWith: firstTestColours  forNumberOfMeets: number).
aStream cr.
aStream nextPutAll: (Mall openMallWith: secondTestColours  forNumberOfMeets: number).
aStream flush.!

units

^Units.!

waitForClosingOfMall: aMall withCreatures: creatures usingGuard: guard
	creatures size timesRepeat: [ guard wait ].
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Mall methodsFor: 'as yet unclassified'!

allowedMoreMeetings
	^ maxRendezvous > 0!

completeRendezvousOf: aChameneos 
	mustWait := false.
	self rendezvousOccurred.
!

initialize
	mustWait := false.
	firstCall := true.
	bouncer:= Semaphore new.
"	guard := Semaphore forMutualExclusion."
	guard := Monitor new.
	firstCreature := nil.
	secondCreature := nil!

meetSomeoneWith: aChameneos 
	secondCreature := aChameneos.
	firstCall := true.
	mustWait := true.
!

mustWait 
	self guard waitWhile: [ mustWait and: [ self allowedMoreMeetings ] ]!

rendezvousOccurred
	maxRendezvous := maxRendezvous - 1!

visitWith: aChameneos 

	^ self guard critical: 
		[ 	| partner |
		self mustWait.
		firstCall 
			ifTrue: 
				[ 
				self waitForSomeoneElseWith: aChameneos.
				self completeRendezvousOf: aChameneos.
				partner := secondCreature.
				guard signalAll]
			ifFalse: 
				[ 
				self meetSomeoneWith: aChameneos.
				partner := 	firstCreature. 
				guard signalAll ].
		 maxRendezvous >= 0 
			ifTrue: [ partner ]
			ifFalse: [ nil ] ]!

waitForSomeoneElseWith: aChameneos 
	firstCreature := aChameneos.
	firstCall := false.
	self guard waitWhile: [ firstCall not and: [ self allowedMoreMeetings ] ]! !

!Mall methodsFor: 'accessing'!

bouncer
	^bouncer!

bouncer: anObject
	bouncer := anObject!

guard
	^ guard!

guard: anObject 
	guard := anObject!

maxRendezvous
	^ maxRendezvous!

maxRendezvous: max
	 maxRendezvous:=max.! !

#{Mall} initialize!

Smalltalk defineClass: #Creature
	superclass: #{Core.Object}
	indexedType: #none
	private: false
	instanceVariableNames: 'creatureName colour selfMet creaturesMet '
	classInstanceVariableNames: ''
	imports: ''
	category: 'chameleon'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Creature class methodsFor: 'as yet unclassified'!

withName: aName colour: aColour
|creature|
creature:=Creature new initialize.
creature name: aName.
creature colour: aColour .
^creature.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Creature methodsFor: 'as yet unclassified'!

if: vis isMeThen: block
(vis isNil not and: [self name=vis name]) ifTrue:[block value].!

initialize
selfMet:=0.
creaturesMet :=0.!

visitMall: mall 

		[	| partner | 
		partner := mall visitWith: self.
		partner ifNotNil: 
			[ self colour: (self colour complementaryColourFor: partner colour).
			self == partner
		ifTrue: [ selfMet := selfMet + 1 ].
			creaturesMet := creaturesMet + 1.
			 ] . 
Processor yield.
		partner isNil. ] whileFalse! !

!Creature methodsFor: 'accessing'!

colour
	^ colour!

colour: anObject
	colour := anObject!

creaturesMet
	^ creaturesMet!

creaturesMet: anObject
	creaturesMet := anObject!

name
	^ creatureName !

name: anObject
	creatureName := anObject!

selfMet
	^ selfMet!

selfMet: anObject
	^ selfMet := anObject! !

Smalltalk defineClass: #ChameneosColour
	superclass: #{Core.Object}
	indexedType: #none
	private: false
	instanceVariableNames: 'color '
	classInstanceVariableNames: ''
	imports: ''
	category: 'chameleon'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!ChameneosColour class methodsFor: 'as yet unclassified'!

createBlue
	"comment stating purpose of message"

	| |
^(super new) color: ColorValue blue.!

createRed
	"comment stating purpose of message"

	| |
^(super new) color: ColorValue red.!

createYellow
	"comment stating purpose of message"

	| |
^(super new) color: ColorValue yellow.!

generateReportOfColours
	| readOut colours red blue yellow |
	red:=ChameneosColour createRed.
     yellow:=ChameneosColour createYellow.
     blue:=ChameneosColour createBlue.
colours:=Array with: blue with: red with: yellow.

	readOut := WriteStream on: String new.

colours do:[:aColour| colours do:[:anotherColour| aColour printOn: readOut.
		readOut nextPutAll: ' + '.
		   anotherColour printOn: readOut.
		 readOut nextPutAll: ' -> '.
		(aColour complementaryColourFor: anotherColour) printOn: readOut.
		readOut cr]].
^readOut.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!ChameneosColour methodsFor: 'as yet unclassified'!

color

^color!

color: aColor

color:=aColor .!

complementaryColourFor: aChameneosColour
	"determine the complementary colour defined as..."
(self hasSameColorAs: aChameneosColour) ifTrue:[^self].
self isBlue ifTrue: [aChameneosColour  isRed ifTrue: [^self class createYellow] ifFalse: [^self class createRed.]].
self isRed ifTrue: [aChameneosColour  isBlue ifTrue: [^self class createYellow] ifFalse: [^self class createBlue.]].
aChameneosColour  isBlue ifTrue: [^self class createRed] ifFalse: [^self class createBlue.].
!

hasSameColorAs: aChameneos

^self color=aChameneos color.!

isBlue

^self color=ColorValue blue.!

isRed

^self color=ColorValue red.!

isYellow

^self color=ColorValue yellow.!

printOn: aStream

aStream nextPutAll: (self color class constantNameFor: self color).! !
