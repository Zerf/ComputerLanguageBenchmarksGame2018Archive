/* The Computer Language Benchmarks Game
 *  http://shootout.alioth.debian.org/
 *  
 *  contributed by Francesco Massei
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <sched.h>

typedef struct node_s {
    struct node_s *l, *r;
    long i;
} node_st;

static node_st *new_node(node_st *l, node_st *r, long i)
{
    node_st *ret;
    ret = malloc(sizeof(*ret));
    ret->l = l;
    ret->r = r;
    ret->i = i;
    return ret;
}

static long item_check(node_st *node)
{
    if (node->l==NULL) return node->i;
    return node->i+item_check(node->l)-item_check(node->r);
}

static node_st *b_u_tree(long i, int d)
{
    if (d>0) return new_node(b_u_tree(2*i-1, d-1), b_u_tree(2*i, d-1), i);
    return new_node(NULL, NULL, i);
}

static void delete_node(node_st *node)
{
    if (node->l) {
        delete_node(node->l);
        delete_node(node->r);
    }
    free(node);
}

static long check_tree_of_depth(long iter, long depth)
{
    long i, check;
    node_st *tmp;

    for (check = 0, i = 1; i<=iter; ++i) {
        tmp = b_u_tree(i, depth);
        check += item_check(tmp);
        delete_node(tmp);

        tmp = b_u_tree(-i, depth);
        check += item_check(tmp);
        delete_node(tmp);
    }
    return check;
}

static int GetThreadCount()
{
	cpu_set_t cs;
    int count, i;

	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);
	for (count=i=0; i<8; i++) {
        if (CPU_ISSET(i, &cs)) count++;
	}
	return count;
}

int main(const int argc, const char *argv[])
{
    node_st *stretch, *longlived;
    long i, c;
    int n, depth, mindepth, maxdepth, stretchdepth;

    n = (argc>1) ? atoi(argv[1]) : 10;
    if (n < 1) {
        fprintf(stderr, "Wrong argument.\n");
        exit(EXIT_FAILURE);
    }

    mindepth = 4;
    maxdepth = mindepth + 2 > n ? mindepth + 2 : n;
    stretchdepth = maxdepth + 1;

    stretch = b_u_tree(0, stretchdepth);
    printf("stretch tree of depth %u\t check: %li\n", stretchdepth, 
                                                        item_check(stretch));
    delete_node(stretch);

    longlived = b_u_tree(0, maxdepth);

    #pragma omp parallel for default(shared) num_threads(GetThreadCount()) schedule(dynamic, 1)
   for (depth = mindepth; depth <= maxdepth; depth += 2) {
        i = 1 << (maxdepth - depth + mindepth);
        c = check_tree_of_depth(i, depth);
        printf("%ld\t trees of depth %d\t check: %ld\n", i*2, depth, c);
    }

    printf("long lived tree of depth %d\t check: %ld\n", maxdepth,
                                                        item_check(longlived));

    delete_node(longlived);

    return 0;
}

