/* binarytrees.cpp
 *
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Jon Harrop
 * Modified by Alex Mizrahi and Andreas Schäfer
 */

#include <iostream>
#include <stdlib.h>
#include <boost/pool/object_pool.hpp>

struct Node;

struct Node {
    Node *l, *r;
    int i;
    Node(int i2) : l(0), r(0), i(i2) {}
    Node(Node *l2, int i2, Node *r2) : l(l2), r(r2), i(i2) {}
    int check() const {
        if (l)
            return l->check() + i - r->check();
        else return i;
    }
};

Node *make(int i, int d, boost::object_pool<Node> *store) {
    if (d == 0) return store->construct(i);
    return store->construct(make(2*i-1, d-1, store), i, make(2*i, d-1, store));
}


int main(int argc, char *argv[]) {
    int min_depth = 4;
    int max_depth = std::max(min_depth+2,
                             (argc == 2 ? atoi(argv[1]) : 10));
    int stretch_depth = max_depth+1;

    {
        boost::object_pool<Node> store;
        Node *c = make(0, stretch_depth, &store);
        std::cout << "stretch tree of depth " << stretch_depth << "\t "
                  << "check: " << c->check() << std::endl;
    }

    boost::object_pool<Node> long_lived_store;
    Node *long_lived_tree=make(0, max_depth, &long_lived_store);

    for (int d=min_depth; d<=max_depth; d+=2) {
        int iterations = 1 << (max_depth - d + min_depth), c=0;

        for (int i=1; i<=iterations; ++i) {
            boost::object_pool<Node> store;
            Node *a = make(i, d, &store), *b = make(-i, d, &store);
            c += a->check() + b->check();
        }
        std::cout << (2*iterations) << "\t trees of depth " << d << "\t "
                  << "check: " << c << std::endl;
    }

    std::cout << "long lived tree of depth " << max_depth << "\t "
              << "check: " << (long_lived_tree->check()) << "\n";

    return 0;
}
