// The Computer Language Benchmarks Game
//   http://shootout.alioth.debian.org/
// Alternative version by bearophile
// Version for normal compilation

#include "stdio.h"

#define len 1 << 12

int main() {
    int tot = 0, nread = 0, current = 0, sign = 1;
    unsigned char buffer[len];

    while ((nread = fread(&buffer, 1, len, stdin)) > 0) {
        unsigned char* c = &buffer[0];
        unsigned char* end = &buffer[0] + nread;

        while (__builtin_expect(c < end, 1)) {
            if (__builtin_expect(*c >= '0' && *c <= '9', 1))
                current = current * 10 + *c++ - '0';
            else if (__builtin_expect(*c == '-', 0)) {
                sign = -1;
                c++;
            } else {
                tot += current * sign;
                current = 0;
                sign = 1;
                c++;
            }
        }
    }

    printf("%d\n", tot);
    return 0;
}
