#! /home/users/tomc/bin/rebase -sq
rebol[
	summary: [rebol random tom conlin 2005-11-29]
	version 0.2
]

decimal-pad: func [d[number!] p[integer!]/local r s t][
	d: to string! d
	either s: find/tail d "."
		[   either p >= length? s
			[insert/dup tail s "0" p - length? s]
			[	t: skip s p
				r: either #"5" > first t[0][10]
				while[not zero? r: to integer! r / 10][
					t: back t if #"." == first t[t: back t]
					r: -47 + first t
					change t r // 10
					if all[9 < r any[head? t #"-" == first t]][
						insert d "0" s: next s
					]
				]
				clear skip s p
			]
		]
		[insert tail d "." insert/dup tail d "0" p]
	d
]
;;do %decimal-pad.r
prandom: context[
	prev: 42
	set 'advance-prandom func[N [integer!]][
		loop N[prev: prev * 3877 + 29573 // 139968]
		prev / 1399.68
	]
	set 'reset-prandom does[prev: 42.0]
]
N: either N: system/script/args[N][1]
print decimal-pad advance-prandom to integer! N 9
