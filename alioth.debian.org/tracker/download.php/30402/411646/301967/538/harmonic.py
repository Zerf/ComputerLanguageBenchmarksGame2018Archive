#!/usr/bin/python
# http://shootout.alioth.debian.org/
#
# Contributed by Peter Crabtree

import sys, psyco

psyco.full()

def main():
    total = float()
    j = float(1)
    i = 1
    end = int(sys.argv[1])
    while i < end:
        total += 1 / j
        j += 1
        i += 1
    print "%.9f" % total
main()
