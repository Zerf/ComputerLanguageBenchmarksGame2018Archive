%% The Computer Language Benchmark Games
%% http://shootout.alioth.debian.org/
%% Contributed by Fredrik Svahn based on Per Gustafsson's mandelbrot program

-module(mandelbrot).
-export([main/1]).
-define(LIM_SQR, 4.0).
-define(ITER, 50).
-define(F(X),is_float(X)).
-define(SR, -1.5).
-define(SI, -1).

main([Arg]) ->
    N = list_to_integer(Arg),
    put(port, open_port({fd,0,1}, [out])),
    port_command(get(port),["P4\n", Arg, " ", Arg, "\n"]),
    calc_bits(1, 1, N, []),
    halt(0).

%Loop over all bits to calculate and print
calc_bits(N, N, N, Bits) -> 
    print_bytes(Bits), done;
calc_bits(N, Y, N, Bits) -> 
    print_bytes(Bits), calc_bits(1, Y + 1, N, []);
calc_bits(X, Y, N, Bits) ->
    calc_bits(X + 1, Y, N, [m(?ITER, ?SR+(N-X-1)*2/N, ?SI+(Y-1)*2/N) | Bits]).

%Mandelbrot algorithm
m(Iter, CR,CI) -> m(Iter - 1, CR, CI, CR, CI).

m(Iter, R, I, CR, CI) when ?F(R), ?F(I), ?F(CR), ?F(CI), Iter > 0 -> 
    case (R*R+I*I) of 
	Lim when Lim =< ?LIM_SQR -> m(Iter-1, R*R-I*I+CR, 2*R*I+CI, CR, CI);
	_ -> 0   
    end;
m(_,R,I,_,_) when ?F(R), ?F(I) ->
    case (R*R+I*I) of 
	Lim when Lim =< ?LIM_SQR -> 1;
	_ -> 0
    end.

%Print bytes one at a time. 
print_bytes(Bytes)-> 
    print_bytes(get(port), Bytes).
print_bytes(_, []) -> done;
print_bytes(Port, [B1, B2, B3, B4, B5, B6, B7, B8 | RemBytes]) ->
    port_command(Port, <<B1:1,B2:1,B3:1,B4:1,B5:1,B6:1,B7:1,B8:1>>),
    print_bytes(Port, RemBytes);
print_bytes(Port, RemBytes) -> 
    print_bytes(Port, RemBytes++[0]).
