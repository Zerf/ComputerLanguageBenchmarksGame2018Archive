﻿// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org/
//
// Contributed by Valentin Kraevskiy

#light
    
let id = 1N, 0N, 0N, 1N

let comp (q, r, s, t) (u, v, x) =
    (q * u, q * v + r * x, s * u, s * v + t * x)

let div x y = 
    let rec next w n =        
        if w > x then n else next (w + y) (n + 1N)
    next y 0N
let extr (q, r, s, t) x = div (q * x + r) (s * x + t)   
    
let next z = extr z 3N
let safe z = (=) (extr z 4N)
let prod (u, v, w, x) n =
    let neg = -10N * n
    (10N * u + neg * w, 10N * v + neg * x, w, x)

let digits = 
    let z = ref id    
    let lfts = 
        let n = ref 0N
        fun () -> n := !n + 1N
                  !n, 4N * !n + 2N, 2N * !n + 1N
    let rec digits () =
            let y = next !z 
            if safe !z y 
                then z := (prod !z y)
                     y
                else z := (comp !z <| lfts ())
                     digits ()
    digits

let rec loop n s total = 
    if total = 0 
        then for _ in 1 .. n do printf " "
             printf ("\t:%i\n") (s + 10 - n)
        else if n = 0 
            then printf "\t:%i\n" <| s + 10
                 loop 10 (s + 10) total
            else printf "%i" <| int (digits ()) 
                 loop (n - 1) s (total - 1)

loop 10 0 <| try int Sys.argv.[1] with _ -> 27 

 