// The Great Computer Language Shootout
//   http://shootout.alioth.debian.org/
// Contributed by Sebastien Loisel
// Converted to D by bearophile

import std.conv, std.math, std.gc;

double eval_A(int i, int j) {
    return 1.0 / ((i + j) * (i + j + 1) / 2 + i + 1);
}

void eval_A_times_u(int n, double[] u, double[] au) {
    for (int i; i < n; i++) {
        au[i] = 0;
        for (int j; j < n; j++)
            au[i] += eval_A(i, j) * u[j];
    }
}

void eval_At_times_u(int n, double[] u, double[] au) {
    for (int i; i < n; i++) {
        au[i] = 0;
        for (int j; j < n; j++)
            au[i] += eval_A(j, i) * u[j];
    }
}

void eval_AtA_times_u(int n, double[] u, double[] atAu) {
    double[] w = (cast(double*)malloc(double.sizeof * n))[0 .. n];
    eval_A_times_u(n, u, w);
    eval_At_times_u(n, w, atAu);
}

void main(string[] args) {
    int n = args.length > 1 ? toInt(args[1]) : 100;

    double[] u = (cast(double*)malloc(double.sizeof * n))[0 .. n];
    double[] v = (cast(double*)malloc(double.sizeof * n))[0 .. n];
    double vBv, vv;
    u[] = 1;

    for (int i; i < 10; i++) {
        eval_AtA_times_u(n, u, v);
        eval_AtA_times_u(n, v, u);
    }

    vBv = vv = 0;
    for (int i; i < n; i++) {
        vBv += u[i] * v[i];
        vv += v[i] * v[i];
    }

    printf("%0.9f\n", sqrt(vBv / vv));
    return 0;
}