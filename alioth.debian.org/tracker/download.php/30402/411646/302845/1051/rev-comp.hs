import Data.Char(toUpper)
import Data.Array
import System.IO

{-

Extremely simple version.

Using PackedStrings would be faster since this is heavly string-management bound which
is damn convenient, but also damn slow, in Haskell using the standard String type.

But this is an idiomatic approach.

By Sebastian Sylvan

-}

complement i = complArr ! i'
             where i' = toUpper i

complArr = array ('A','Z') (self ++ complAssoc)
           where self = az `zip` az
                 az = ['A'..'Z']
complAssoc = [
              ('A','T'),('C','G'),('G','C'),('T','A'),('U','A'),('M','K'),('R','Y'),('W','W'),
              ('S','S'),('Y','R'),('K','M'),('V','B'),('D','H'),('D','H'),('B','V'),('N','N')
             ] 

process header@('>':xs) = putStrLn header
process x = putStrLn (map complement x)
        
loop h = 
    do eof <- hIsEOF h
       case eof of
        True -> return ()
        False -> do xs <- hGetLine h
                    process xs
                    loop h

main = loop stdin