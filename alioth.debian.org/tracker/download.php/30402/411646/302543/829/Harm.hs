-- The Great Computer Language Shootout
-- http://shootout.alioth.debian.org/
-- sum harmonic series
-- compile with:  ghc -O2 -o Harm Harm.hs
-- contributed by Greg Buchholz
-- enhanced by Einar Karttunen
-- further enhanced by Mirko Rahn (n additions less)

import System(getArgs)
import Numeric(showFFloat)

main :: IO ()
main = getArgs >>= \ (~n:_) ->
       putStrLn $ showFFloat (Just 9) (loop (read n) 0) []

loop :: Double -> Double -> Double
loop 0 s = s
loop d s = loop (d-1) (s+(1/d))
