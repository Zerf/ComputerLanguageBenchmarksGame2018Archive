program fannkuch;
uses Math;

var maxFlips : Longint;

procedure exch(var a,b : Longint); inline;
var tmp : Longint;
begin
  tmp := a; a := b; b := tmp;
end;

procedure flop(p : array of Longint);
var flips,k,i : Longint;
begin
  flips := 0;
  while p[0] > 1 do
  begin
    k := p[0]-1;
    for i := 0 to (k-1) div 2 do
      exch(p[i], p[k-i]);
    flips := flips + 1;
  end;
  maxFlips := max(flips, maxFlips);
end;

var n,i,x : Longint;
    a,s : array of Longint;
begin
  Val(ParamStr(1), n, i);
  SetLength(a,n);
  SetLength(s,n);
  for i := 0 to n-1 do a[i] := i+1;
  for i := 0 to n-1 do s[i] := 0;
  maxFlips := 0;
  x := 1;
  repeat
    if s[x] < x then
    begin
      if odd(x) then exch(a[x],a[s[x]]) else exch(a[x],a[0]);
      flop(a);
      s[x] := s[x] + 1;
      x := 1;
    end
    else
    begin
      s[x] := 0;
      x := x + 1;
    end;
  until x = n;
  writeln('Pfannkuchen(',n,') = ',maxFlips);
end. 
