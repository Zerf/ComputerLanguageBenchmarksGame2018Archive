/* ------------------------------------------------------------------ */
/* The Great Computer Language Shootout                               */
/* http://shootout.alioth.debian.org/                                 */
/*                                                                    */
/* Contributed by Anthony Borla                                       */
/* ------------------------------------------------------------------ */

#include <cctype>
#include <list>
#include <queue>
#include <string>
#include <algorithm>
#include <iterator>
#include <iostream>

using namespace std;

typedef queue<string, list<string> > Header;

void dumpSegment(Header& header, string& segment, ostream& out = cout);
char complement(char element);

/* ----------------------------- */

int main()
{
  bool okToDump = false; string line, segment; Header header;

  while (getline(cin, line, '\n'))
  {
    if (line[0] == '>')
    {
      header.push(line); if (!okToDump) { okToDump = true; continue; }
      dumpSegment(header, segment); segment.resize(0);
    }
    else
    {
      segment += line;
    }
  }

  dumpSegment(header, segment);  

  return 0;
}

/* ----------------------------- */

void dumpSegment(Header& header, string& segment, ostream& out)
{
  const int LINELENGTH = 60;

  out << header.front() << endl; header.pop();

  reverse(segment.begin(), segment.end());
  transform(segment.begin(), segment.end(), segment.begin(), complement);

  string::iterator begin = segment.begin(), i = segment.begin();

  while (i != segment.end())
  {
    if (distance(begin, i) == LINELENGTH)
    {
       copy(begin, i, ostream_iterator<char>(out)); out << endl;
       begin = i; 
    }
    ++i;
  }

  copy(begin, i, ostream_iterator<char>(out)); out << endl;
}

/* ----------------------------- */

char complement(char element)
{
  static char charMap[] =
  {
    'T', 'V', 'G', 'H', '\0', '\0', 'C', 'D', '\0', '\0', 'M', '\0', 'K',
    'N', '\0', '\0', '\0', 'Y', 'S', 'A', 'A', 'B', 'W', '\0', 'R', '\0'
  };

  return charMap[toupper(element) - 'A'];
}
