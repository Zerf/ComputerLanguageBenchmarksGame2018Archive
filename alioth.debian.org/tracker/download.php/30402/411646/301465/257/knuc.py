#!/usr/bin/python
# http://shootout.alioth.debian.org/
#
# submitted by Ian Osgood

import sys

def hash_seq(seq, length):
	n,t = len(seq)+1-length, {}
	for i in xrange(n):
		s = seq[i:i+length]
		try:
			t[s] += 1
		except KeyError:
			t[s] = 1
	return n,t

def sort_seq(seq, length):
	n,t = hash_seq(seq, length)
	l = zip(t.values(), t.keys())
	l.sort()
	l.reverse()
	print '\n'.join(["%s %.3f" % (s,100.0*count/n) for count,s in l])
	print

def find_seq(seq, s):
	n,t = hash_seq(seq, len(s))
	print "%d\t%s" % (s in t and t[s] or 0, s)

def main():
	seq = []
	add_line = seq.append
	for line in sys.stdin:
		if line[0:6] == ">THREE":
			break
	for line in sys.stdin:
		if line[0] in ">;":
			break
		add_line(line[:-1])
	seq = ''.join(seq).upper()
	sort_seq(seq, 1)
	sort_seq(seq, 2)
	find_seq(seq, "GGT")
	find_seq(seq, "GGTA")
	find_seq(seq, "GGTATT")
	find_seq(seq, "GGTATTTTAATT")
	find_seq(seq, "GGTATTTTAATTTATAGT")

main()
