!Tests class methodsFor: 'benchmark scripts'!
sumcol2
   | s sum |
   s := self stdinSpecial.
   sum := 0.
   [s atEnd] whileFalse: [
      sum := sum + s nextLine asInteger].
   self stdout print: sum; nl.
   ^''! !


Tests sumcol2!
