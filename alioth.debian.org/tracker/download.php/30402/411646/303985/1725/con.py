import sys

def create_coroutine(n):
	if n > 1:
		coroutine = create_coroutine(n-1)
		while 1:
			yield coroutine.next()+1
	else:
		while 1:
			yield 1

def main():
	coroutine = create_coroutine( 500 )
	count = 0
	for i in xrange( int( sys.argv[1] ) ):
		count += coroutine.next()
	print count

main()