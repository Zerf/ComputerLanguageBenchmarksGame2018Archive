(* nsieve.ml -- Sieve of Eratosthenes
 *
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Christophe TROESTLER
 * Modified by Vladimir Silyaev
 * Modified by Stefan Kral
 *)

let nsieve m =
  let a = String.make m 'T' in
  let count = ref 0 in
  for i = 2 to m-1 do
    if a.[i] = 'T' then (
      incr count;
      let j = ref(i lsl 1) in 
      while !j < m do a.[!j] <- 'F'; j := !j+i done;
    )
  done;
  Printf.printf "Primes up to %8u %8u\n" m !count

let () =
  (* Use [Array.get] so it raises an exception even if compiled with -unsafe *)
  let n = try int_of_string (Array.get Sys.argv 1) with _ -> 2 in
  for i = 0 to 2 do nsieve(10000 lsl (n-i)) done

