# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
# Written by Dima Dorfman, 2004
# modified by Heinrich Acker

def nsieve(k):
    m = (1 << k) * 10000
    a = [True] * m
    n = 0
    for i, x in izip(count(2), islice(a, 2, None)):
        if x:
            a[i + i::i] = (False,) * (m // i - 1 - (not m % i))
            n += 1
    print 'Primes up to %8d %8d' % (m, n)

from itertools import count, islice, izip
import sys

n = int(sys.argv[1])
nsieve(n)
nsieve(n-1)
nsieve(n-2)
