;;  The Great Computer Language Shootout
;;  http://shootout.alioth.debian.org/
;; 
;;  contributed by Simon Brooke <simon@jasmine.org.uk>
;;  binary tree in lisp

;;  I have not written LISP for a living for twenty years;
;;  consequently this is almost certainly not an optimal solution.
;;  The performance of this program is hugely sensitive to GC and to
;;  available memory. See commentary at end of file

(defconstant *mindepth* 4)

(defun binarytree (n)
  (let*
      ((dflt (+ *mindepth* 2))
       (maxdepth
        (cond
         ((> dflt n) dflt)
         (t n)))
       (stretchdepth (+ maxdepth 1)))
    (format t "stretch tree of depth ~D~32T  check: ~D"
            stretchdepth
            (itemcheck (treenode 0 stretchdepth)))
    (terpri)
    (let ((longlife (treenode 0 maxdepth)))
      (dotimes (i (+ (- maxdepth *mindepth*) 2) nil)
        (cond ((zerop (mod i 2))
               (dodepth (+ *mindepth* i) maxdepth))))
      (format t "long lived tree of depth ~D~32T  check: ~D"
              maxdepth
              (itemcheck longlife))
      (terpri))))


(defun dodepth (depth maxdepth)
  (let ((check 0)
        (iterations (expt 2 (+ (- maxdepth depth) *mindepth*))))
    (dotimes (i iterations nil)
      ;; stuff
      (let
          ((j (+ i 1)))
        (setq check (+ check
        (itemcheck (treenode j depth))
        (itemcheck (treenode (- 0 j) depth))))))
    (format t "~D~9T trees of depth ~D~32T  check: ~D" (* iterations 2) depth check)
    (terpri)
    ))

;; old skool lisp. I could have defstructed a three-place cell, but am
;; guessing that the GC is optimised to collect conses. Consecuently a
;; node will be a list comprising (item leftsubtree . rightsubtree)
(defun treenode (item depth)
  (cond
   ((= depth 0)( cons 0 nil))
   (t (cons
       item
       (cons
        (treenode (- (* 2 item) 1) (- depth 1))
        (treenode (* 2 item) (- depth 1)))))))


;; once again pure old skool lisp - recurse down the tree. Assume a LISP
;; compiler can optimise the hell out of recursion!
(defun itemcheck (tree)
  (cond ((null (cdr tree))(car tree))
        (t (- (+ (car tree) (itemcheck (cadr tree)))
              (itemcheck (cddr tree))))))

;; Shamelessly stolen from Friedrich Dominicus' ackerman implementation
(defun main ()
  (let ((n
         (parse-integer
          (or (car (last #+sbcl sb-ext:*posix-argv*
                         #+cmu  extensions:*command-line-strings*
                         #+gcl  si::*command-args*)) "1"))))
    (binarytree n)))


;; (extensions:gc-on)
;; (time (binarytree 16))
;; on my machine (dual processor Athlon 1600, 2Gb core) this run (GC
;; switched on) is 7.6 seconds; Java 8.511 seconds; mono 7.35 seconds
;; Both Java and LISP were given 1Gb to play with; I don't know mono
;; well enough to optimise it.

;; (extensions:gc-off)
;; (extensions:gc)
;; (time (binarytree 16))
;; on my machine this run (GC switched off) this run takes 5.5 seconds
