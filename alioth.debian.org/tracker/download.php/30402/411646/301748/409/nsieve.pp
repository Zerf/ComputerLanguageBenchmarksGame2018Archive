program nsieve;
uses SysUtils;

var n : integer;

//procedure primes(n : integer);
procedure primes(n : integer); inline;
var flags : array of boolean;
    size,i,j,count : longint;
begin
  size := 10000 shl n;
  SetLength(flags, size+1);
//for i := 2 to size do flags[i] := true;
  fillchar(flags[0],length(flags),ord(true));
  count := 0;
  for i := 2 to size do
    if flags[i] then
    begin
      count := count + 1;
      j := i + i;
      while j <= size do begin
//      flags[j] := false;
        if flags[j] then flags[j] := false;
        j := j + i;
      end;
    end;
  writeln('Primes up to', size:9, count:9);
end;

begin
  n := StrToInt(paramstr(1));
  primes(n);
  primes(n-1);
  primes(n-2);
end.
