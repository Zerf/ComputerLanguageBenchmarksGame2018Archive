{ The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Ian Osgood
}
{$mode objfpc}

program harmonic;

var i, n, z: Integer; sum, d: double;
begin
  Val(ParamStr(1), n, z);
  for i := 1 to n do
  begin
    d := d + 1.0;
    sum := sum + 1.0/d;
  end;
  writeln(sum:0:9);
end.
