#include <jni.h>
#include <math.h>
#include "MathLib.h"
#include <stdio.h>

JNIEXPORT jdouble JNICALL Java_MathLib_sin (JNIEnv *e, jclass o, jdouble a)
{
    return(sin(a));
}

JNIEXPORT jdouble JNICALL Java_MathLib_cos (JNIEnv *e, jclass o, jdouble a)
{
    return(cos(a));
}
JNIEXPORT jdouble JNICALL Java_MathLib_sqrt (JNIEnv *e, jclass o, jdouble a)
{
    return(sqrt(a));
}

JNIEXPORT jdouble JNICALL Java_MathLib_pow (JNIEnv *e, jclass o, jdouble a, jdouble b)
{
    return(pow(a, b));
}
