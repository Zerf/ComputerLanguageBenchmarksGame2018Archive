/* The Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Josh Goldfoot
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char **argv)
{
    long n, k, a;
    double sum;
    double twoThirds = 2.0 / 3.0;
    
    n = (argc > 1) ? atoi(argv[1]) : 25000;
    for (sum = 0, k = 0; k < n; k++) sum += pow(twoThirds, k);
    printf("%0.9f\t(2/3)^k\n", sum);
    for (sum = 0, k = 1; k <= n; k++) sum += pow(k,-0.5);
    printf("%0.9f\tk^-0.5\n", sum);
    for (sum = 0, k = 1; k <= n; k++) sum += 1.0 /(k*(k+1.0 ));
    printf("%0.9f\t1/k(k+1)\n", sum);
    for (sum = 0, k = 1; k <= n; k++) sum += 1.0/(pow(k, 3.0) * pow(sin(k),2.0));
    printf("%0.9f\tFlint Hills\n", sum);
    for (sum = 0, k = 1; k <= n; k++) sum += 1.0/(pow(k, 3.0) * pow(cos(k),2.0));
    printf("%0.9f\tCookson Hills\n", sum);
    for (sum = 0, k = 1; k <= n; k++) sum += 1.0 / k;
    printf("%0.9f\tHarmonic\n", sum);
    for (sum = 0, k = 1; k <= n; k++) sum += 1.0 / pow(k, 2.0);
    printf("%0.9f\tRiemann Zeta\n", sum);
    for (sum = 0, a = -1, k = 1; k <= n; k++) sum += (a = -a) / (double) k;
    printf("%0.9f\tAlternating Harmonic\n", sum);
    for (sum = 0, a = -1, k = 1; k <= n; k++) sum += (a = -a)/(2.0 * k - 1);
    printf("%0.9f\tGregory\n", sum);
}
