/* The Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Josh Goldfoot
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char **argv)
{
    long n, k, a;
    double sum, sum0, sum1, sum2, sum3, sum4, sum5, sum6, sum7, harm, k3;
    double twoThirds = 2.0 / 3.0;
    
    n = (argc > 1) ? atoi(argv[1]) : 25000;
    sum0 = sum1 = sum2 = sum3 = sum4 = sum5 = sum6 = sum7 = 0;
    sum = 1;
    for (a = 1, k = 1; k <= n; k++) {
        sum += pow(twoThirds, k);
        k3 = pow(k, 3.0);
        harm = 1.0 / k;
        sum0 += pow(k,-0.5);
        sum1 += 1.0 /(k*(k+1.0 ));
        sum2 += 1.0/(k3 * pow(sin(k),2.0));
        sum3 += 1.0/(k3 * pow(cos(k),2.0));
        sum4 += harm;
        sum5 += 1.0 / pow(k, 2.0);
        sum6 += a * harm;
        sum7 += a/(2.0 * k - 1);
        a *= -1;
    }
    sum -= pow(twoThirds, k);
    printf("%0.9f\t(2/3)^k\n", sum);
    printf("%0.9f\tk^-0.5\n", sum0);
    printf("%0.9f\t1/k(k+1)\n", sum1);
    printf("%0.9f\tFlint Hills\n", sum2);
    printf("%0.9f\tCookson Hills\n", sum3);
    printf("%0.9f\tHarmonic\n", sum4);
    printf("%0.9f\tRiemann Zeta\n", sum5);
    printf("%0.9f\tAlternating Harmonic\n", sum6);
    printf("%0.9f\tGregory\n", sum7);
}
