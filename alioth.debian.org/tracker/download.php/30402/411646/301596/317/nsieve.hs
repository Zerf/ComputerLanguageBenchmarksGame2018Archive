-- written by Einar Karttunen
-- further optimized by Jeff Newbern

import Data.Array.IO
import Data.Array.Base
import Data.Bits (shiftL)
import System (getArgs)
import Control.Monad(when)

buildSieve :: Int -> IO (IOUArray Int Bool)
buildSieve hi = do arr <- newArray(2,hi) True
                   mapM_ (\i -> unsafeWrite arr i False) [4,6..hi]  --inline n=2 case
                   sieve arr 3 hi
                   return arr
  -- here we assume n is odd and n >= 3
  where sieve arr n hi | n > hi    = do return ()
                       | otherwise = do el <- unsafeRead arr n
                                        when el (elimMults arr n hi)
                                        sieve arr (n+2) hi
        elimMults arr n hi = mapM_ (\i -> unsafeWrite arr i False) [(3*n),(5*n)..hi]

countTrue :: IOUArray Int Bool -> (Int,Int) -> IO Int
countTrue arr (lo,hi) = helper arr lo hi 0
  where helper arr idx end acc | idx > end = do return acc
                               | otherwise = do el <- unsafeRead arr idx
                                                if el then helper arr (idx+1) end $! (acc + 1)
                                                      else helper arr (idx+1) end acc

writeCount m c | m > 0     = putStrLn ("Primes up to "++fmt 8 m++" "++fmt 8 c)
               | otherwise = return ()
  where fmt width i = let is = show i in (take (width - length is) (repeat ' ')) ++ is

main = do n <- getArgs >>= readIO.head
          let b1 = (1 `shiftL` (n-2)) * 10000
          let b2 = (1 `shiftL` (n-1)) * 10000
          let b3 = (1 `shiftL` n) * 10000
          arr <- buildSieve b3
          c1 <- countTrue arr (2,b1)
          c2 <- countTrue arr ((b1+1),b2)
          c3 <- countTrue arr ((b2+1),b3)
          writeCount b3 (c1+c2+c3) >> writeCount b2 (c1+c2) >> writeCount b1 c1
