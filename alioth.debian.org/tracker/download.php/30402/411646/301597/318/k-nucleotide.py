#!/usr/bin/python -OO
# The Great Computer Language Shootout
# http://shootout.alioth.debian.org/
#
# Contributed by Kevin Carson

from sys import stdin


class k_nucleotide :
    def __init__(self, sequence) :
        self.seq = sequence
        self.seq_length = len(self.seq)
        self.frequencies = {}


    def k_frequency(self, frame) :
        n = self.seq_length + 1 - frame
        
        for i in xrange(n) :
            k_nucleo = self.seq[i:i + frame]

            try :
                self.frequencies[k_nucleo] += 1
            except KeyError :
                self.frequencies[k_nucleo] = 1


    def generate_frequencies(self, length) :
        self.frequencies.clear()
        self.k_frequency(length)


    def freq_code_cmp(self, fc1, fc2) :
        if fc1[1] > fc2[1] :
            return -1
        elif fc1[1] == fc2[1] :
            if fc1[0] > fc2[0] :
                return 1
            elif fc1[0] == fc2[0] :
                return 0
            else :
                return -1
        else :
            return 1


    def write_frequencies(self, nucleotide_length) :
        self.generate_frequencies(nucleotide_length)

        fcp = self.frequencies.items()
        fcp.sort( self.freq_code_cmp )

        sum = 0
        for tuple in fcp :
            sum += tuple[1]

        for tuple in fcp :
            print "%s %.3f" % (tuple[0], (tuple[1] * 100.0) / sum)

        print ""


    def write_count(self, fragment) :
        self.generate_frequencies(len(fragment))

        try :
            count = self.frequencies[fragment]
        except KeyError :
            count = 0

        print "%d\t%s" % (count, fragment)


for line in stdin :
    if line[0:6] == ">THREE" :
        break

sequence = ''
for line in stdin :
    if (line[0] == ">") or (line[0] == ";") :
        break;
    sequence += line[:-1].upper()


kn = k_nucleotide(sequence)

kn.write_frequencies(1)
kn.write_frequencies(2)

kn.write_count("GGT")
kn.write_count("GGTA")
kn.write_count("GGTATT")
kn.write_count("GGTATTTTAATT")
kn.write_count("GGTATTTTAATTTATAGT")
