--[[
Each program should create and keep alive 503 threads, explicity or implicitly
linked in a ring, and pass a token between one thread and the next thread at
least N times.

Each program should
  create 503 linked threads (named 1 to 503)
  thread 503 should be linked to thread 1, forming an unbroken ring
  pass a token to thread 1
  pass the token from thread to thread N times
  print the name of the last thread (1 to 503) to take the token
--]]

require"coroutine"

-- first and only argument is number of token passes
local N = assert(tonumber(arg[1]))

-- fixed size pool
local poolsize = 503

-- cache these to avoid global environment lookups
local resume = coroutine.resume
local yield = coroutine.yield

local threads = {}

function body(token)
  while true do
    token = yield(token+1)
  end
end

for id = 1,poolsize do
  threads[id] = coroutine.create(body)
end

local id = 0
local ok
local token = 0
repeat
  id = (id%poolsize)+1
  ok, token = resume(threads[id], token)
  -- skip check for whether body raised exception
  --assert(ok)
until token == N

print(id)

