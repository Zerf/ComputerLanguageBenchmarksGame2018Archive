/* The Computer Language Benchmarks Game
   http://shootout.alioth.debian.org/

   contributed by Jeffrey Beu 5th March 2008
*/

#include <sys/mman.h>
#include <sys/stat.h>
#include <stdio.h>

inline int cstring2int(unsigned char **string_arg)
{
   unsigned char *string = *string_arg;
   int integer = 0, neg = 1;
   
   if (*string == '-')
   {  neg = 0;
      ++string;
   }

   while(1)
   {  unsigned char tmp = *string++ - '0';

      if (tmp < 10)
         integer = integer*10 + tmp;
      else
         break;
   }
   
   *string_arg = string;
   return neg ? integer : -integer;	
}

unsigned char* mmap_stdin(unsigned int *file_size)
{
   const int stdin_fd = 0;
   struct stat file_stat;
   fstat(stdin_fd,&file_stat);
   *file_size = file_stat.st_size;
   return (unsigned char*)mmap(0,*file_size,PROT_READ,MAP_PRIVATE,stdin_fd,0);
}

int main(void)
{
   unsigned int end,sum = 0;
   unsigned char *buffer = mmap_stdin(&end);
   unsigned char *buffer_end = buffer + end;

   while (buffer != buffer_end)
   {
      sum += cstring2int(&buffer);
   }

   printf("%d\n",sum);
   /* assume OS will ummap memory */
   return 0;
}
