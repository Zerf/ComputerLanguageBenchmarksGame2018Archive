/* The Computer Language Shootout
 * http://shootout.alioth.debian.org
 * 
 * reverse-complement benchmark
 * contributed by Laurent DEBACKER */

using System;
using System.Collections.Generic;
using System.IO;

public class revcomp
{
	private static char[] complements;
	
	static revcomp()
	{
		complements = new char[128];
		complements[(int)'a'] = complements[(int)'A'] = 'T';
		complements[(int)'c'] = complements[(int)'C'] = 'G';
		complements[(int)'g'] = complements[(int)'G'] = 'C';
		complements[(int)'t'] = complements[(int)'T'] = 'A';
		complements[(int)'u'] = complements[(int)'U'] = 'A';
		complements[(int)'m'] = complements[(int)'M'] = 'K';
		complements[(int)'r'] = complements[(int)'R'] = 'Y';
		complements[(int)'w'] = complements[(int)'W'] = 'W';
		complements[(int)'s'] = complements[(int)'S'] = 'S';
		complements[(int)'y'] = complements[(int)'Y'] = 'R';
		complements[(int)'k'] = complements[(int)'K'] = 'M';
		complements[(int)'v'] = complements[(int)'V'] = 'B';
		complements[(int)'h'] = complements[(int)'H'] = 'D';
		complements[(int)'d'] = complements[(int)'D'] = 'H';
		complements[(int)'b'] = complements[(int)'B'] = 'V';
		complements[(int)'n'] = complements[(int)'N'] = 'N';
	}
	
	public static void Main(string[] Arguments)
	{
		const int lineWidth = 60;
		TextReader input = Console.In;
		TextWriter output = Console.Out;
		
		List<char[]> older = new List<char[]>();
		
		char[] buffer = new char[1024];
		int position = buffer.Length;
		
		for(;;)
		{
			string line = input.ReadLine();
			
			if(line == null || line[0] == '>')
			{
				if(position != buffer.Length)
				{
					char[] cur = buffer;
					int left;
					while((left = cur.Length - position) >= lineWidth)
					{
						output.WriteLine(cur, position, lineWidth);
						position += lineWidth;
					}
					
					if(older.Count > 0)
					{
						for(int i = older.Count - 1 ; i >= 0 ; --i)
						{
							if(left > 0) output.Write(cur, position, left);
							cur = older[i];
							
							left = lineWidth - left;
							output.WriteLine(cur, 0, left);
							position = left;
							
							while((left = cur.Length - position) >= lineWidth)
							{
								output.WriteLine(cur, position, lineWidth);
								position += lineWidth;
							}
						}
						older.Clear();
					}
					
					if(left > 0) output.WriteLine(cur, position, left);
					position = buffer.Length;
				}
				
				if(line == null) break;
				output.WriteLine(line);
			}
			else if(line[0] != ';')
			{
				int len = line.Length, i = -1;
				
				if(position < len)
				{
					while(--position >= 0) buffer[position] = complements[(int)line[++i]];
					older.Add(buffer);
					position = buffer.Length << 1;
					buffer = new char[position];
				}
				
				while(++i < len) buffer[--position] = complements[(int)line[i]];
			}
		}
	}
}
