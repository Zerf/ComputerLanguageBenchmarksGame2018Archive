/*
 * g++ version, this is test for __gnu_cxx::hash_map;
 * I've made MyString for this test with minimal implementation
 * in order to avoid excessive copying of std::string;
 * hash function is from this page: http://www.azillionmonkeys.com/qed/hash.html;
 * algorithm is exactly same as D program
 */
#include <algorithm>
#include <map>
#include <ext/hash_map>
#include <utility>
#include <cstdio>
#include <cctype>
#include <cstring>
#include <iostream>
#include <ostream>
#include <iomanip>

#include "stdint.h"

#undef get16bits
#if ((defined(__GNUC__) || defined(__GNUG__)) && defined(__i386__)) \
  || defined(__WATCOMC__) || defined(_MSC_VER) || defined (__BORLANDC__) \
  || defined (__TURBOC__)
#define get16bits(d) (*((const uint16_t *) (d)))
#endif

#if !defined (get16bits)
#define get16bits(d) ((((const uint8_t *)(d))[1] << UINT32_C(8))\
                      +((const uint8_t *)(d))[0])
#endif


using namespace std;
using __gnu_cxx::hash_map;


class MyString{
public:
MyString():data_(0),parent_(0){}
MyString(const MyString& in):data_(in.data_),parent_(in.parent_)
{
  if(data_)++in.data_->refcount;
  if(parent_)++parent_->refcount;
}
MyString& operator=(const MyString& in)
{
  if(&in == this)return *this;
  dispose();
  data_=in.data_;
  parent_=in.parent_;
  if(in.data_)++in.data_->refcount;
  if(parent_)++parent_->refcount;
  return *this;
}
MyString(char* beg):data_(new data),parent_(0)
{
  data_->size=strlen(beg);
  data_->ptr=beg;
  data_->freeptr=false;
  data_->refcount=1;
  data_->capacity=strlen(beg);
}
MyString(char* beg,char* end):data_(new data),parent_(0)
{
  data_->size=end-beg;
  data_->ptr=beg;
  data_->freeptr=false;
  data_->refcount=1;
  data_->capacity=end-beg;
}
template <class iter>
MyString(iter beg,iter end):data_(new data),parent_(0)
{
  initHelper();
  unsigned index = 0;
  for(;beg!=end;++beg,++index)
  {
    if(++data_->size>data_->capacity)
    {
      data_->ptr =(char*)realloc(data_->ptr,data_->capacity*=2);
    }
    data_->ptr[index]=*beg;
  }
}
MyString(char* b, char* e, MyString& s)
: data_(new data)
{
  data_->size=e-b;
  data_->ptr=b;
  data_->freeptr=false;
  data_->refcount=1;
  parent_=s.data_;
  ++parent_->refcount;
}


~MyString()
{
  dispose();
}
unsigned size()const
{
  if(!data_)return 0;
  return data_->size;
}
char* begin()const
{
  if(data_)
  return data_->ptr;
  else return 0;
}
char* end()const
{
  if(data_)
  return data_->ptr+data_->size;
  else return 0;
}

bool operator==(const MyString& rhs)const
{
  if(!data_)if(!rhs.data_)return true;
            else return false;
  if(!rhs.data_)return false;
  if(data_->size!=rhs.data_->size)return false;
  return strncmp(data_->ptr,rhs.data_->ptr,data_->size)==0;
}

friend ostream& operator<<(ostream& os, const MyString& s)
{
  return os.write(s.begin(),s.size());
}
friend struct hash;
struct hash{
  size_t operator()(const MyString& p)const
  {
    if(!p.data_)return 0;
    return h(p.begin(),p.size());
  }

static uint32_t h(const char * data, int len) {
uint32_t hash = len, tmp;
int rem;

    if (len <= 0 || data == NULL) return 0;

    rem = len & 3;
    len >>= 2;

    /* Main loop */
    for (;len > 0; len--) {
        hash  += get16bits (data);
        tmp    = (get16bits (data+2) << 11) ^ hash;
        hash   = (hash << 16) ^ tmp;
        data  += 2*sizeof (uint16_t);
        hash  += hash >> 11;
    }

    /* Handle end cases */
    switch (rem) {
        case 3: hash += get16bits (data);
                hash ^= hash << 16;
                hash ^= data[sizeof (uint16_t)] << 18;
                hash += hash >> 11;
                break;
        case 2: hash += get16bits (data);
                hash ^= hash << 11;
                hash += hash >> 17;
                break;
        case 1: hash += *data;
                hash ^= hash << 10;
                hash += hash >> 1;
    }

    /* Force "avalanching" of final 127 bits */
    hash ^= hash << 3;
    hash += hash >> 5;
    hash ^= hash << 2;
    hash += hash >> 15;
    hash ^= hash << 10;

    return hash;
}
};
private:
struct data{
char* ptr;
unsigned size,refcount,capacity;
bool freeptr;
} *data_, *parent_;
void dispose()
{
  dispose_helper(data_);
  dispose_helper(parent_);
}
static void dispose_helper(data* d)
{
  if(d)
  {
    --d->refcount;
    if(!d->refcount)
    {
      if(d->freeptr)free(d->ptr);
      delete d;
    }
  }
}
void initHelper()
{
  data_->size = 0;
  data_->ptr=(char*)malloc(8);
  data_->refcount=1;
  data_->capacity=8;
  data_->freeptr=true;
}

};
char* toUpper(char* in)
{
  char* tmp = in;
  for(;*in;++in)
  {
    *in=toupper(*in);
  }
  return tmp;  
}

struct Ob{
char* buf;
size_t size,capacity;
Ob():buf((char*)malloc(4096)),size(0),capacity(4096){}
~Ob(){free(buf);}
void append(const char* in)
{
     for(;*in;++in)
     {
       if(++size>capacity)buf=(char*)realloc(buf,capacity*=2);
       buf[size-1]=*in;
     }  
}
char* begin()const{return buf;}
char* end()const{return buf+size;}
private:
Ob(const Ob&);
Ob& operator=(const Ob&);
};

// end of optimisation things

class KNucleotide{
private:
  struct Cmp{
    bool operator()(unsigned lhs,unsigned rhs)const
    {
         return rhs<lhs;
    }
  };
  MyString sequence;
  typedef hash_map<MyString,unsigned,MyString::hash> hm_t;
  typedef map<unsigned,MyString,Cmp> m_t;
  hm_t frequencies;
  unsigned k;
public:
  KNucleotide(const MyString& in):sequence(in),k(0)
  {
  }
  void WriteFrequencies(int nucleotideLength)
  {
     GenerateFrequencies(nucleotideLength);

     int sum = 0;
     for(hm_t::iterator i = frequencies.begin();i!=frequencies.end();++i)
     {
       if(i->first.size() == nucleotideLength) sum += i->second;
     }

     m_t freqsrt;
     for_each(frequencies.begin(),frequencies.end(),Insert(freqsrt));
     
     int last = 0;
     for(m_t::iterator i = freqsrt.begin();i!=freqsrt.end();++i)
     {
        if(i->first != last)
        {
          for(m_t::iterator j = freqsrt.begin();j!=freqsrt.end();++j)
          {
              if(j->second.size() == nucleotideLength)
              {
                double ratio = sum ? (double)j->first / (double)sum : 0;
                if(j->first == i->first) 
                  cout<<j->second<<' '<<setprecision(3)<<setiosflags(ios::fixed)<<setw(6)<<ratio * 100.0<<'\n';
              }
          }
          last = i->first;
        }
     }
    cout<<'\n';
  }
  void WriteCount(const MyString& nucleotideFragment)
  {
      GenerateFrequencies(nucleotideFragment.size());
      hm_t::iterator i = frequencies.find(nucleotideFragment);
      int count = i!=frequencies.end() ? i->second : 0;
      cout<<count<<'\t'<<nucleotideFragment<<'\n';
  }
private:
  void GenerateFrequencies(int length)
  {
      k = length;
      for(int frame = 0; frame < k; frame++) KFrequency(frame);
  }

  void KFrequency(int readingFrame)
  {
      int n = sequence.size() - k + 1;
      for(int i = readingFrame; i < n; i += k)
      {
          frequencies[MyString(sequence.begin()+i,sequence.begin()+i+k,sequence)]++;
      }
  }
  struct Insert{
  m_t& mp;
  Insert(m_t &m):mp(m){}
  void operator()(const pair<MyString,unsigned>& in)
  {
       mp.insert(make_pair(in.second,in.first));
  }
  };
};


int main(int argc, char* argv[])
{
    bool flag=false;
    char buf[4096];
    Ob ob;
    while(cin.getline(buf,4096))
    {
      if(*buf)
      {
         if(!flag)
         {
            flag = strlen(buf) >=6 && !strncmp(buf,">THREE",6);
            continue;
         }
         else
         {
            char c = *buf;
            if(c == '>') break;
            else if(c != ';') ob.append(toUpper(buf));
         }
      }
    }
    KNucleotide kn (MyString(ob.begin(),ob.end()));
    kn.WriteFrequencies(1);
    kn.WriteFrequencies(2);

    kn.WriteCount("GGT");
    kn.WriteCount("GGTA");
    kn.WriteCount("GGTATT");
    kn.WriteCount("GGTATTTTAATT");
    kn.WriteCount("GGTATTTTAATTTATAGT");

    return 0;

}
