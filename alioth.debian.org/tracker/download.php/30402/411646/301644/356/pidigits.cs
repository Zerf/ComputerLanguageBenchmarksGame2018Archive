/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Alp Toker <alp@atoker.com>
*/

using System;
using UBigInteger = Mono.Math.BigInteger;
using Sign = Mono.Math.BigInteger.Sign;

public class pidigits
{
	const int L = 10;

	public static void Main (string[] args)
	{
		if (args.Length != 1)
			return;

		int n = Int32.Parse (args[0]);
		int j = 0;

		PiDigitSpigot digits = new PiDigitSpigot ();

		while (n > 0) {
			if (n >= L) {
				for (int i = 0 ; i != L ; i++)
					Console.Write (digits.Next ());
				j += L;
			} else {
				for (int i = 0 ; i != n ; i++)
					Console.Write (digits.Next ());
				for (int i = n ; i != L ; i++)
					Console.Write (" ");
				j += n;
			}

			Console.Write ("\t:");
			Console.WriteLine (j);
			n -= L;
		}
	}
}

class PiDigitSpigot
{
	Transformation z = new Transformation (1, 0, 0, 1);
	Transformation x = new Transformation (0, 0, 0, 0);
	Transformation inverse = new Transformation (0, 0, 0, 0);

	public int Next ()
	{
		int y = Digit;

		if (IsSafe (y))
		{
			z = Produce (y);
			return y;
		}
		else {
			z = Consume (x.Next ());
			return Next ();
		}
	}

	public int Digit
	{
		get {
			return z.Extract (3);
		}
	}

	public bool IsSafe (int digit)
	{
		return digit == z.Extract (4);
	}

	public Transformation Produce (int i)
	{
		return (inverse.qrst (10,-10*i,0,1)).Compose (z);
	}

	public Transformation Consume (Transformation a)
	{
		return z.Compose (a);
	}
}

class Transformation
{
	BigInteger q, r, s, t;
	int k = 0;

	public Transformation (int q, int r, int s, int t)
	{
		this.q = new BigInteger (q);
		this.r = new BigInteger (r);
		this.s = new BigInteger (s);
		this.t = new BigInteger (t);
	}

	public Transformation (BigInteger q, BigInteger r, BigInteger s, BigInteger t)
	{
		this.q = q;
		this.r = r;
		this.s = s;
		this.t = t;
	}

	public Transformation Next ()
	{
		k++;
		q = new BigInteger (k);
		r = new BigInteger (4*k + 2);
		s = new BigInteger (0);
		t = new BigInteger (2*k + 1);

		return this;
	}

	public int Extract (int j)
	{
		BigInteger J = new BigInteger (j);
		return ((q * J + r) / (s * J + t)).IntValue ();
	}

	public Transformation qrst (int q, int r, int s, int t)
	{
		this.q = new BigInteger (q);
		this.r = new BigInteger (r);
		this.s = new BigInteger (s);
		this.t = new BigInteger (t);
		k = 0;

		return this;
	}

	public Transformation Compose (Transformation a)
	{
		return new Transformation (q*a.q, q*a.r + r*a.t, s*a.q + t*a.s, s*a.r + t*a.t);
	}
}

class BigInteger
{
	protected UBigInteger bi;
	protected Sign sign;

	protected BigInteger () {}

	public BigInteger (int value)
	{
		if (value == 0)
			sign = Sign.Zero;
		else if (value < 0)
			sign = Sign.Negative;
		else
			sign = Sign.Positive;

		bi = new UBigInteger ((int)sign*value);
	}

	public int IntValue ()
	{
		return (int)sign * (int)bi.GetBytes ()[0];
	}

	public override string ToString ()
	{
		return (sign == Sign.Negative ? "-" : "") + bi.ToString ();
	}

	public static BigInteger operator * (BigInteger bi1, BigInteger bi2)
	{
		BigInteger ret = new BigInteger ();
		ret.bi = bi1.bi * bi2.bi;
		ret.sign = (Sign) ((int)bi1.sign * (int)bi2.sign);
		return ret;
	}

	public static BigInteger operator / (BigInteger bi1, BigInteger bi2)
	{
		BigInteger ret = new BigInteger ();
		ret.bi = bi1.bi / bi2.bi;
		ret.sign = (Sign) ((int)bi1.sign * (int)bi2.sign);
		return ret;
	}

	public static BigInteger operator + (BigInteger bi1, BigInteger bi2)
	{
		BigInteger ret = new BigInteger ();
		if (bi1.sign == Sign.Zero)
			return bi2;

		if (bi2.sign == Sign.Zero)
			return bi1;

		if (bi1.sign == bi2.sign) {
			ret.bi = bi1.bi + bi2.bi;
			ret.sign = bi1.sign;
			return ret;
		}

		if (bi1.bi == bi2.bi) {
			ret.bi = 0;
			ret.sign = Sign.Zero;
			return ret;
		}

		if (bi1.bi < bi2.bi) {
			ret.bi = bi2.bi - bi1.bi;
			ret.sign = (Sign)((int)Sign.Positive * (int)bi2.sign);
			return ret;
		} else {
			ret.bi = bi1.bi - bi2.bi;
			ret.sign = (Sign)((int)Sign.Negative * (int)bi2.sign);
			return ret;
		}
	}
}

