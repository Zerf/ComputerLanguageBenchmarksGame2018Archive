def fannkuch(n)
   count = (1..n).to_a
   maxFlipsCount, m, r, check = 0, n-1, n, 0

   perm1 = (0..n-1).to_a
   while true
      if check < 30
         puts "#{perm1}"
         check += 1
      end

      while r != 1
         count[r-1] = r
         r -= 1
      end

      if perm1[0] != 0 and perm1[m] != m
         perm = perm1.dup
         flipsCount = 0
         k = perm[0]
         while k != 0
            perm[0...k+1] = perm[0...k+1].reverse!
            flipsCount += 1
            k = perm[0]
         end
         maxFlipsCount = flipsCount if flipsCount > maxFlipsCount
      end
      while true
         if r==n : return maxFlipsCount end
         perm1.insert r,perm1.shift
         count[r] -= 1
         break if count[r] > 0
         r += 1
      end
   end
end

N = (ARGV[0] || 1).to_i
puts "Pfannkuchen(#{N}) = #{fannkuch(N)}"