{ The Great Computer Language Shootout
  http://shootout.alioth.debian.org

  contributed by Joost van der Sluis
}

program nsieveBits;

{$mode objfpc}

const bpc = SizeOf(cardinal) * 8;

procedure primes(n : integer);
var size,count,prime,i : integer;
    flags : array of cardinal;

  function IsSet(i : longword) : boolean; inline;
  var offset : integer; mask : cardinal;
  begin
    offset := i div bpc;
    mask   := 1 shl (i mod bpc);
    IsSet := (flags[offset] and mask) <> 0;
  end;

  procedure Clear(i : longword); inline;
  var offset : integer; mask : cardinal;
  begin
    offset := i div bpc;
    mask   := 1 shl (i mod bpc);
    if ((flags[offset] and mask) <> 0) then flags[offset] := flags[offset] and not mask;
  end;

begin
  size := 10000 shl n;
  SetLength(flags, size div bpc + 1);
  filldword(flags[0],length(flags),high(cardinal));

  count := 0;
  for prime := 2 to size do
    if IsSet(prime) then
    begin
      count := count + 1;
      i := prime + prime;
      while i <= size do
      begin
        Clear(i);
        i := i + prime;
      end;
    end;
  writeln('Primes up to', size:9, count:9);
end;

var n, z : integer;
begin
  Val(ParamStr(1), n, z);
  primes(n);
  primes(n-1);
  primes(n-2);
end.