;;; -*- mode: lisp -*-
;;; $Id: takfp-sbcl.code,v 1.7 2005/02/26 08:53:42 bfulgham Exp $
;;; http://shootout.alioth.debian.org/
;;; Contributed by Brent Fulgham

(declaim (optimize (speed 3) (debug 0) (safety 0) (space 0) (compilation-speed 0)))

(defun tak (x y z)
  (declare (single-float x y z))
  (if (not (< y x))
      z
      (tak (tak (- x 1) y z) (tak (- y 1) z x) (tak (- z 1) x y))))

(defun main ()
  (let ((n (* 1.0 (parse-integer
		   (or (car (last #+sbcl sb-ext:*posix-argv*
				  #+cmu  extensions:*command-line-strings*
				  #+gcl  si::*command-args*)) "1")))))
    (format t "~d~%" (tak (* n 3.0) (* n 2.0) (* n 1.0) ))))

;;; vim: ts=4 ft=lisp
