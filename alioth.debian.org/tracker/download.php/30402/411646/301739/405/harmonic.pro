/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by isykes 20050604
   modified by …

compile with: gplc --global-size 175000 --min-size harmonic.pro  
	for n<=10000000	*/

:-initialization(run).

	run:-	get_N(Nth_val),
		harmonic(Nth_val,0),!.
	run.

		get_N(Nth_val):- argument_list([Input]),
				number_atom(Nth_val,Input),
				integer(Nth_val),
				Nth_val > 0,!.
		get_N(_):- write('Problem with argument!\n'),fail.

		harmonic(0,Sigma):- write(Sigma),nl.
		harmonic(Old_val,Old_Sigma):-
			New_Sigma is Old_Sigma + (1 / Old_val),
			New_val is dec(Old_val),!,	
			harmonic(New_val,New_Sigma).
