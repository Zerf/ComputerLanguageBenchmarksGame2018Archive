// The Computer Lannguage Shootout
// http://shootout.alioth.debian.org/
// Original C version by Heiner Marxen
// Low-level version adapted from C by bearophile

import std.conv, std.c.stdlib;

static long fannkuch(int n) {
    int r, i, k, didpr;
    int* perm, perm1, count;
    long flips, flipsMax;
    int n1 = n - 1;

    if (n < 1)
        return 0;

    perm  = cast(int*)calloc(n, int.sizeof);
    perm1 = cast(int*)calloc(n, int.sizeof);
    count = cast(int*)calloc(n, int.sizeof);

    for (i = 0; i < n; ++i)
        perm1[i] = i; // initial (trivial) permu

    r = n;
    didpr = 0;
    flipsMax = 0;
    for (;;) {
        if (didpr < 30) {
            for (i = 0; i < n; ++i)
                printf("%d", cast(int)(1 + perm1[i]));
            printf("\n");
            ++didpr;
        }

        for (; r!=1 ; --r)
            count[r-1] = r;

        if (!(perm1[0] == 0 || perm1[n1] == n1)) {
            flips = 0;
            for (i = 1; i < n ; ++i) // perm = perm1
                perm[i] = perm1[i];

            k = perm1[0]; // cache perm[0] in k

            do { // k != 0 ==> k > 0
                int j;
                for (i = 1, j = k-1; i < j; ++i, --j) {
                    int tmp;
                    tmp = perm[i];
                    perm[i] = perm[j];
                    perm[j] = tmp;
                }
                ++flips;

                // Now exchange k (caching perm[0]) and perm[k]
                j = perm[k];
                perm[k] = k;
                k = j;
            } while (k);

            if (flipsMax < flips)
                flipsMax = flips;
        }

        for (;;) {
            if (r == n)
                return flipsMax;

            // rotate down perm[0..r] by one
            {
                int perm0 = perm1[0];
                i = 0;
                while (i < r) {
                    k = i + 1;
                    perm1[i] = perm1[k];
                    i = k;
                }
                perm1[r] = perm0;
            }
            if ((count[r] -= 1) > 0)
                break;
            ++r;
        }
    }
}

void main(char[][] args) {
    int n = args.length > 1 ? toInt(args[1]) : 1;
    printf("Pfannkuchen(%d) = %ld\n", n, fannkuch(n));
}