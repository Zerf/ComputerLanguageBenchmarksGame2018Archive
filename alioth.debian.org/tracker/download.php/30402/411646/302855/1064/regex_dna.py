# The Great Computer Language Shootout
# http://shootout.alioth.debian.org/
# Contributed by Dominique Wahli
# Modified by bearophile, Jan 5 2006

import re, sys

seq = sys.stdin.read()
ilen = len(seq)

seq = re.sub('(>.*\n)|(\n)', '', seq)
clen = len(seq)

var = """agggtaaa|tttaccct
         [cgt]gggtaaa|tttaccc[acg]
         a[act]ggtaaa|tttacc[agt]t
         ag[act]gtaaa|tttac[agt]ct
         agg[act]taaa|ttta[agt]cct
         aggg[acg]aaa|ttt[cgt]ccct
         agggt[cgt]aa|tt[acg]accct
         agggta[cgt]a|t[acg]taccct
         agggtaa[cgt]|[acg]ttaccct"""

for f in var.split():
    print f, len(re.findall(f, seq))

sbs = "B(c|g|t) D(a|g|t) H(a|c|t) K(g|t) M(a|c) N(a|c|g|t) R(a|g) S(c|g) V(a|c|g) W(a|t) Y(c|t)"

for el in sbs.split():
    seq = re.sub(el[0], el[1:], seq)

print "\n", ilen, "\n", clen, "\n", len(seq)