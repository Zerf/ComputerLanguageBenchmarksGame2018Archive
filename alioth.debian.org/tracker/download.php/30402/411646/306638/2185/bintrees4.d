// The Great Computer Language Shootout
// http://shootout.alioth.debian.org/
// By bearophile, optimized for speed, it uses a free list

import std.stdio, std.conv, std.gc;


class TreeNode {
    // Free list management ---------------
    static TreeNode freelist;        // start of free list

    static TreeNode allocate(int item, TreeNode left=null, TreeNode right=null) {
        TreeNode t;
        if (freelist) {
            t = freelist;
            freelist = t.next;
            t.item = item;
            t.left = left;
            t.right = right;
        } else
            t = new TreeNode(item, left, right);
        return t;
    }

    static void deallocate(TreeNode t) {
        t.next = freelist;
        freelist = t;
    }

    TreeNode next; // for use by TreeNode FreeList
    // End free list management ---------------

    int item;
    TreeNode left, right;

    this(int item, TreeNode left=null, TreeNode right=null) {
        this.item = item;
        this.left = left;
        this.right = right;
    }

    int check() {
        return left is null ? item : item + left.check - right.check;
    }
}


TreeNode makeTree(int item, int depth) {
    if (depth > 0)
        return TreeNode.allocate(item, makeTree(2*item-1, depth-1), makeTree(2*item, depth-1));
    else
        return TreeNode.allocate(item);
}

void unmakeTree(TreeNode t) {
    if (t.left !is null) {
        unmakeTree(t.left);
        unmakeTree(t.right);
    }
    TreeNode.deallocate(t);
}


void main(string[] args) {
    std.gc.disable();

    const minDepth = 4;
    int n = (args.length > 1) ? toInt(args[1]) : 2;
    int maxDepth = (minDepth + 2) > n ? minDepth + 2 : n;

    auto t = makeTree(0, maxDepth + 1);
    writefln("stretch tree of depth ", maxDepth + 1, "\t check: ", t.check);
    unmakeTree(t);

    auto longLivedTree = makeTree(0, maxDepth);

    for (int depth = minDepth; depth <= maxDepth; depth += 2) {
        int iterations = 1 << (maxDepth - depth + minDepth);
        int check = 0;

        for (int i = 1; i <= iterations; i++){
            auto t1 = makeTree(i, depth);
            check += t1.check;
            unmakeTree(t1);

            auto t2 = makeTree(-i, depth);
            check += t2.check;
            unmakeTree(t2);
        }

        writefln(iterations * 2, "\t trees of depth ", depth, "\t check: ", check);
    }

    writefln("long lived tree of depth ", maxDepth, "\t check: ", longLivedTree.check);
}