// The Computer Language Shootout Benchmarks
// http://shootout.alioth.debian.org/
// contributed by Benoit Hudson based on a contribution of Kevin Carson
// Adapted to D by bearophile

import std.gc: malloc;
import std.conv: toInt;

struct treeNode {
    treeNode* left, right;
    int item;
}

static treeNode* freelist;

treeNode* newTreeNode(treeNode* left, treeNode* right, int item) {
    treeNode* t;

    if (freelist) {
        t = freelist;
        freelist = freelist.left;
    } else
        t = cast(treeNode*)malloc(treeNode.sizeof);

    t.left = left;
    t.right = right;
    t.item = item;
    return t;
}

int itemCheck(treeNode* tree) {
    if (tree.left is null)
        return tree.item;
    else
        return tree.item + itemCheck(tree.left) - itemCheck(tree.right);
}

treeNode* bottomUpTree(int item, uint depth) {
    if (depth > 0)
        return newTreeNode(bottomUpTree(2*item-1, depth-1), bottomUpTree(2*item, depth-1), item);
    else
        return newTreeNode(null, null, item);
}

void deleteTree(treeNode* tree) {
    if (tree.left !is null) {
        deleteTree(tree.left);
        deleteTree(tree.right);
    }

    tree.left = freelist;
    freelist = tree;
}

void main(string[] args) {
    uint N, depth, minDepth, maxDepth, stretchDepth;
    treeNode* stretchTree, longLivedTree, tempTree;

    N = toInt(args[1]);
    minDepth = 4;

    if ((minDepth + 2) > N)
        maxDepth = minDepth + 2;
    else
        maxDepth = N;

    stretchDepth = maxDepth + 1;

    stretchTree = bottomUpTree(0, stretchDepth);
    printf("stretch tree of depth %u\t check: %li\n", stretchDepth, itemCheck(stretchTree));

    deleteTree(stretchTree);

    longLivedTree = bottomUpTree(0, maxDepth);

    for (depth = minDepth; depth <= maxDepth; depth += 2) {
        int i, iterations, check;
        iterations = 1 << (maxDepth - depth + minDepth);
        check = 0;

        for (i = 1; i <= iterations; i++) {
            tempTree = bottomUpTree(i, depth);
            check += itemCheck(tempTree);
            deleteTree(tempTree);

            tempTree = bottomUpTree(-i, depth);
            check += itemCheck(tempTree);
            deleteTree(tempTree);
        }

        printf("%li\t trees of depth %u\t check: %li\n", iterations*2, depth, check);
    }

    printf("int lived tree of depth %u\t check: %li\n", maxDepth, itemCheck(longLivedTree) );
}