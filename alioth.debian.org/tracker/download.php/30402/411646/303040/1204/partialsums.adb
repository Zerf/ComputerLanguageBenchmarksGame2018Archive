-----------------------------------------------------------------------
-- The Computer Language Shootout
-- http://shootout.alioth.debian.org
-- contributed by Jim Rogers
-----------------------------------------------------------------------
with Ada.Numerics.Generic_Elementary_Functions;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Command_Line; use Ada.Command_Line;


procedure Partialsums is 
   type Real is digits 12;
   package Real_Funcs is new Ada.Numerics.Generic_Elementary_Functions(Real);
   use Real_Funcs;
   package Real_Io is new Ada.Text_Io.Float_Io(Real);
   use Real_Io;
   
   N   : Long_Integer;
   Sum : array(1..9) of Real := (Others => 0.0);
   A   : Real := -1.0;
   K_Sq : Real;
   K_Cu  : Real;
   K_Sin : Real;
   K_Cos : Real;
   Kr    : Real;
begin
   N := Long_Integer'Value(Argument(1));
   
   for K in 1..N loop
      Kr := Real(K);
      K_Sq := Kr * Kr;
      K_Cu := Kr * K_Sq;
      K_Sin := Sin(Kr);
      K_Cos := Cos(Kr);
      Sum(1) := Sum(1) + (2.0/3.0)**(Kr - 1.0);
      Sum(2) := Sum(2) + 1.0 / Sqrt(Kr);
      Sum(3) := Sum(3) + 1.0 / (Kr * (Kr + 1.0));
      Sum(4) := Sum(4) + 1.0 / (K_Cu * K_Sin * K_Sin);
      Sum(5) := Sum(5) + 1.0 / (K_Cu * K_Cos * K_Cos);
      Sum(6) := Sum(6) + 1.0 / Kr;
      Sum(7) := Sum(7) + 1.0 / K_Sq;
      A := -A;
      Sum(8) := Sum(8) + A / Kr;
      Sum(9) := Sum(9) + A / (2.0 * Kr - 1.0);
   end loop;
   Real_Io.Put(Item => Sum(1), Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "(2/3)^k");
 
   Real_Io.Put(Item => Sum(2), Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "k^-0.5");

   Real_Io.Put(Item => Sum(3), Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "1/k(k+1)");
   
   Real_Io.Put(Item => Sum(4), Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "Flint Hills");

   Real_Io.Put(Item => Sum(5), Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "Cookson Hills");

   Real_Io.Put(Item => Sum(6), Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "Harmonic");

   Real_Io.Put(Item => Sum(7), Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "Riemann Zeta");

   Real_Io.Put(Item => Sum(8), Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "Alternating Harmonic");

   Real_Io.Put(Item => Sum(9), Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "Gregory");
end Partialsums;
