-module(revcomp).
%% The Computer Language Shootout
%% http://shootout.alioth.debian.org/
%%
%% contributed by Vlad Balin
-compile( [ native, { hipe, o3 } ] ).
-compile( [ inline, { inline_size, 100 } ] ).

-export([main/1]).

main([_Args]) ->
    io:setopts( [ binary ] ),
    loop(),
    halt().

loop() -> loop( [] ).

%% Buffer = iolist()
loop( Buffer ) ->
    case io:get_line('') of
        eof -> flush( Buffer, << >> );
        << ">", _/bytes >> = Head ->
            flush( Buffer, Head ),
            loop( [] );
        Line -> loop( rev_comp_line( Line, Buffer ) )
    end.

%% Buffer, Suffix ) -> iolist().
%% Buffer = Suffix = iolist().
%% Format and write Buffer with sequence followed by Suffix text
flush( Buffer, Suffix ) ->
    Text = format( list_to_binary( Buffer ), Suffix ),
    io:put_chars( Text ).

%% format( Buffer, Suffix ) -> iolist().
%% Buffer = bytes(), Suffix = iolist().
%% Split Buffer into 60-char lines, append Suffix to the end of buffer.
format( << Line:60/bytes, Rest/bytes >>, Suffix ) -> [ Line, 10 | format( Rest, Suffix ) ];
format( << >>, Suffix ) -> Suffix;
format( Line, Suffix ) -> [ Line, 10, Suffix ].

%% rev_comp( Line, Buffer ) -> Buffer.
%% Line = bytes().
%% Buffer = string().
rev_comp_line( << 10 >>, Buffer ) -> Buffer;
rev_comp_line( << H, Rest/bytes >>, Buffer ) -> rev_comp_line( Rest, [ rev_comp( H ) | Buffer ] ).

%% rev_comp( char() ) -> char().
rev_comp( $A ) -> $T;
rev_comp( $C ) -> $G;
rev_comp( $G ) -> $C;
rev_comp( $T ) -> $A;
rev_comp( $U ) -> $A;
rev_comp( $M ) -> $K;
rev_comp( $R ) -> $Y;
rev_comp( $Y ) -> $R;
rev_comp( $K ) -> $M;
rev_comp( $V ) -> $B;
rev_comp( $H ) -> $D;
rev_comp( $D ) -> $H;
rev_comp( $B ) -> $V;
rev_comp( $a ) -> $T;
rev_comp( $c ) -> $G;
rev_comp( $g ) -> $C;
rev_comp( $t ) -> $A;
rev_comp( $u ) -> $A;
rev_comp( $m ) -> $K;
rev_comp( $r ) -> $Y;
rev_comp( $y ) -> $R;
rev_comp( $k ) -> $M;
rev_comp( $v ) -> $B;
rev_comp( $h ) -> $D;
rev_comp( $d ) -> $H;
rev_comp( $b ) -> $V;
rev_comp( S ) -> S.