#-- The Great Computer Language Shootout
#-- http://shootout.alioth.debian.org/
#-- contributed by Mike Pall, Tobias Polzin

import sys

N = int(sys.argv[1])
first = None
second = None
meetings = 0

#-- Create a very social creature.
def creature(me):
    global N, first, second, meetings
    met = 0
    while 1: 
        #-- Meet another creature.

        #-- Wait until meeting place clears.
        while second:
            yield None

        other = first
        if other:
            #-- Hey, I found a new friend!
            second = me
        else:
            # -- Sniff, nobody here (yet).
            if N <= 0:
                #-- Uh oh, the mall is closed.
                meetings += met
                yield None

                # The mall was closed, so everyone is faded.
                raise StopIteration
            N -= 1
            first = me
            while not second:
                yield None #-- Wait for another creature.
            other = second

            first = None
            second = None

        # perform meeting
        met += 1
        if me != other:   me = 6 - me - other

#-- Trivial round-robin scheduler.
def schedule(threads):
    global meetings
    try:
        while 1:
            for thread in threads:
                thread.next()
    except StopIteration:
        return meetings

def main():
    RED, BLUE, YELLOW = xrange(1,4)

    #-- A bunch of colorful creatures.
    threads = [
        creature(BLUE),
        creature(RED),
        creature(YELLOW),
        creature(BLUE) ]

    print schedule(threads)

main()
