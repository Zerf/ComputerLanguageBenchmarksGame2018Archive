-- n-body in Haskell
-- The Great Computer Language Shootout
-- http://shootout.alioth.debian.org/
-- Contributed by Greg Buchholz with help from
-- Josef Svenningsson and Daniel Fischer
-- compile : ghc -O2 -o nbody nbody.hs
-- run : nbody 1000 +RTS -H300M -K100M
-- has a space leak that needs fixing

{-# OPTIONS_GHC -fglasgow-exts #-}

import System(getArgs)
import Numeric

data Vec = V !Double !Double !Double deriving Show
instance Num Vec where
    (V a b c) + (V x y z) = (V (a+x) (b+y) (c+z))
    (V a b c) - (V x y z) = (V (a-x) (b-y) (c-z))
    fromInteger 0 = V 0.0 0.0 0.0 -- for sum function
    signum _ = error "Not implemented."
    abs _ = error "Not implemented."
    _ * _ = error "Not implemented."
instance Eq Vec where (V a b c) == (V x y z) = (a==x) && (b==y) && (c==z)

dot (V a b c) (V x y z) = a*x + b*y + c*z
scale (V a b c) n = V (n*a) (n*b) (n*c)
mag (V x y z) =  sqrt (x*x + y*y + z*z)

data Planet = Planet Vec Vec Double deriving Show --Position Velocity Mass
dist (Planet p1 _ _) (Planet p2 _ _) = mag $ p1 - p2
mass (Planet _ _ m) = m
vel  (Planet _ v _) = v
pos  (Planet p _ _) = p

main = do
        [arg] <- getArgs
        let iter = read arg :: Int
        let n = 5 :: Int
        let bodies = offset_momentum n [sun, jupiter, saturn, neptune, uranus]
        let begin = energy n bodies
        putStrLn $ showFFloat (Just 9) begin ""
        let final = (iterate (advance 0.01) bodies)
        let end = energy n (final !! iter)
        putStrLn $ showFFloat (Just 9) end ""

days_per_year = 365.24
solar_mass = 4.0 * pi * pi

update :: (a -> [a] -> a) -> ([a] -> [a]) -> [a] -> [a]
update f newlist []     = []
update f newlist (a:as) = a' : update f (newlist . (a:)) as
  where a' = f a (newlist as)

advance dt ps = update newplanet id ps
  where newplanet p ps = Planet (pos p + delta_x) new_v (mass p)
          where delta_v = sum (map (\q ->
                  (pos p - pos q) `scale` ((mass q)*dt/(dist p q)^3)) ps)
                new_v   = (vel p) - delta_v
                delta_x = new_v `scale` dt

energy:: Int -> [Planet] -> Double
energy n ps = kinetic - potential
  where
    kinetic   = 0.5 * (sum (map (\q->(mass q)*((vel q) `dot` (vel q))) ps))
    potential = sum [(mass (ps!!i))*(mass (ps!!j))/(dist (ps!!i) (ps!!j))
                      | i<-[0..n-1], j<-[i+1..n-1]]

offset_momentum n ((Planet p v m):ps) = (Planet p new_v m):ps
  where new_v = (sum (map (\n->(vel n) `scale` (mass n)) ps))
                `scale` ((-1.0)/solar_mass)

jupiter = (Planet
 (V 4.84143144246472090e+00 (-1.16032004402742839e+00) (-1.03622044471123109e-01))
 (V ( 1.66007664274403694e-03 * days_per_year)
    ( 7.69901118419740425e-03 * days_per_year)
    ((-6.90460016972063023e-05) * days_per_year))
 (9.54791938424326609e-04 * solar_mass))

saturn = (Planet
 (V 8.34336671824457987e+00 4.12479856412430479e+00 (-4.03523417114321381e-01))
 (V (-2.76742510726862411e-03 * days_per_year)
    (4.99852801234917238e-03 * days_per_year)
    (2.30417297573763929e-05 * days_per_year))
 (2.85885980666130812e-04 * solar_mass))

uranus = (Planet
 (V 1.28943695621391310e+01 (-1.51111514016986312e+01) (-2.23307578892655734e-01))
 (V (2.96460137564761618e-03 * days_per_year)
    (2.37847173959480950e-03 * days_per_year)
    (-2.96589568540237556e-05 * days_per_year))
 (4.36624404335156298e-05 * solar_mass))

neptune = (Planet
 (V 1.53796971148509165e+01 (-2.59193146099879641e+01) 1.79258772950371181e-01)
 (V (2.68067772490389322e-03 * days_per_year)
    (1.62824170038242295e-03 * days_per_year)
    (-9.51592254519715870e-05 * days_per_year))
 (5.15138902046611451e-05 * solar_mass))

sun = (Planet (V 0.0 0.0 0.0) (V 0.0 0.0 0.0) solar_mass)
