%% The Computer Language Benchmarks Game
%% http://shootout.alioth.debian.org/
%% Contributed by Fredrik Svahn based on Per Gustafsson's mandelbrot program

-compile( [ native, { hipe, o3 } ] ).

-module(mandelbrot).
-export([main/1]).
-define(LIM_SQR, 4.0).
-define(ITER, 50).
-define(SR, -1.5).
-define(SI, -1).

main([Arg]) ->
    N = list_to_integer(Arg),
    io:put_chars(["P4\n", Arg, " ", Arg, "\n"]),

    Row = fun(Y)-> spawn(fun()-> row(N-2, ?SI+Y*2/N, 2/N, []) end) end,
    Pids = lists:map(Row, lists:seq(0,N-1)),

    %Pass token around to make sure printouts are in the right order
    hd(Pids) ! tl(Pids) ++ [ self() ],
    receive _Token -> halt(0) end.

row(-1, _, _, Bits) ->
    receive Pids ->
	    put_chars(Bits, []),
	    hd(Pids) ! tl(Pids)
    end;

row(X, Y2, N2, Bits) ->
    row(X-1, Y2, N2, [ m(?ITER, ?SR+X*N2, Y2) | Bits]).

%Mandelbrot algorithm
m(Iter, CR,CI) -> m(Iter - 1, CR, CI, CR, CI).

m(Iter, R, I, CR, CI) when Iter > 0 ->
    case (R*R+I*I) of
	Lim when Lim =< ?LIM_SQR -> m(Iter-1, R*R-I*I+CR, 2*R*I+CI, CR, CI);
	_ -> 0
    end;
m(_,R,I,_,_) ->
    case (R*R+I*I) of
	Lim when Lim =< ?LIM_SQR -> 1;
	_ -> 0
    end.

put_chars([], Bytes) -> io:put_chars(lists:reverse(Bytes));
put_chars([B1, B2, B3, B4, B5, B6, B7, B8 | RemBytes], Bytes) ->
    put_chars(RemBytes, [<<B1:1,B2:1,B3:1,B4:1,B5:1,B6:1,B7:1,B8:1>> | Bytes]);
put_chars(RemBytes, Bytes) ->
    put_chars(RemBytes++[0], Bytes).
