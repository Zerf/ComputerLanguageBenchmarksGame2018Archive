﻿// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org/
//
// Contributed by Valentin Kraevskiy

#light

let iter = 50
let size = try int Sys.argv.[1] with _ -> 200
let map x shift = float (2 * x) / (float size) - shift

printf "P4\n%i %i\n" size size
let stream = System.Console.OpenStandardOutput ()

let rec belongs (zr, zi) (cr, ci) = function
    | 0 -> true
    | _ when zr * zr + zi * zi > 4.0 -> false
    | n -> belongs (zr * zr - zi * zi + cr, ci + 2.0 * zi * zr) 
            (cr, ci) (n - 1)
   
let print, flush = 
    let left = ref 8
    let bits = ref 0uy
    let flush = 
        fun () -> stream.WriteByte !bits      
                  bits := 0uy
                  left := 8
    fun flag ->         
        bits := !bits <<< 1 ||| flag    
        decr left
        if !left = 0 then flush ()
    ,
    fun () -> if !left < 8 then flush ()
      
for y in 0 .. size - 1 do    
    for x in 0 .. size - 1 do
        let c = map x 1.5, map y 1.0
        print <| if belongs c c iter then 1uy else 0uy
    flush ()            