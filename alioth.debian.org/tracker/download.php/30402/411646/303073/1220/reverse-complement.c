/* The Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Josh Goldfoot
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

void reverse_in_place(char* start, char* end )
{
   static char comp[26] = {'T', 'V', 'G', 'H', 'E', 'F', 'C', 'D', 'I', 'J', 
      'M', 'L', 'K', 'N', 'O', 'P', 'Q', 'Y', 'S', 'A', 'U', 'B', 'W',
      'X', 'R', 'Z'};
   char tmp;
   char* seqstart = start;
   char* seqend = end;
      
   while (seqstart < seqend) {
      while (*seqstart < 'A')
         seqstart++;
      while (*seqend < 'A')
         seqend--;
      tmp = comp[toupper(*seqstart) - 'A'];
      *(seqstart++) = comp[toupper(*seqend) - 'A'];
      *(seqend--) = tmp;
   }
}

int main(int argc, char *argv[])
{
   long buflen, bytesread, seqstart;
   char *buffer, *x, *tmp;
   int linelen;
   char line [64];
   buflen = 127150;
   buffer = malloc(buflen);
   x = buffer;
   seqstart = bytesread = 0;

   while (fgets (x, 64, stdin)) {
      linelen = strlen(x);
      bytesread = bytesread + linelen;
      if (x[0] == '>') {
         if (seqstart) {
            strcpy(line, x);
            reverse_in_place(&(buffer[seqstart]), &(buffer[bytesread - linelen - 1]));
            x[0] = 0;
            printf("%s", buffer);
            buffer[0] = 0;
            strcpy(buffer, line);
            x = buffer;
            bytesread = linelen;
         }
         seqstart = bytesread;
      }
      if (buflen - bytesread < 80) {
         buflen *= 10;
         tmp = realloc (buffer, buflen + 1);
         if (! tmp) return -1;
         buffer = tmp;
         x = &(buffer[bytesread - linelen]);
      }
      x = x + linelen;
   }
   reverse_in_place(&(buffer[seqstart]), x);
   printf("%s", buffer);
   free(buffer);
   return 0;
}
