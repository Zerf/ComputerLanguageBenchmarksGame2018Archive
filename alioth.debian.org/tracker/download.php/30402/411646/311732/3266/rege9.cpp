// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org/
//
// Contributed by The Anh Tran

#include <omp.h>
#include <memory.h>
#include <cassert>
#include <sched.h>

#include <iostream>
#include <vector>
#include <iterator>

#include <boost/format.hpp>
#include <boost/scoped_array.hpp>
#include <boost/xpressive/xpressive.hpp>

using namespace boost::xpressive;



// This struct encapsules a line of data
// data line read from input has length = 60
// But we reserve 256 bytes to avoid resizing when regex_replace is called
struct DataLine
{
	static const int MAX_LEN = 256;

	char data[MAX_LEN];
	size_t data_len;
};

struct InputAsDataLines
{
	boost::scoped_array<DataLine>	data;
	size_t							nline;
};


// This struct stores markers [begin - end) of each DNA segment
// Input data has 3 segments
typedef	std::pair<size_t, size_t>	Segment;
typedef std::vector< Segment >		Segment_List;

struct InputAsCharArray
{
	boost::scoped_array<char>	data;
	Segment_List				nsegment;
};


// read all redirected data from stdin
void ReadInput_StripHeader(	size_t &file_size,	size_t &strip_size,
						   InputAsCharArray &char_arr,
						   InputAsDataLines &line_arr)
{
	// get input size
	file_size = ftell(stdin);
	fseek(stdin, 0, SEEK_END);
	file_size = ftell(stdin) - file_size;
	fseek(stdin, 0, SEEK_SET);

	// load content into memory
	char* p_input_buffer = reinterpret_cast<char*>(calloc(1, file_size +1));
	{
		size_t sz = fread(p_input_buffer, 1, file_size, stdin);
		assert(sz == file_size);
	}

	// Alloc flat array storage space
	char_arr.data.reset(new char[file_size]);
	char* p_flat_array = char_arr.data.get();

	{
		// Predict ndataline
		size_t nlines = file_size / 60 + 8;
		// Alloc dataline storage space
		line_arr.data.reset(new DataLine[nlines]);
	}
	DataLine* p_lines_array = line_arr.data.get();


	// Rule: Strip pattern: ">.*\n | \n"
	cregex const strip_ptn( as_xpr('>') >> *(~_n) >> _n | _n );
	cmatch match_result;


	const char* p_src_beg = p_input_buffer;
	const char* p_src_end = p_input_buffer + file_size;

	// dna segment markers
	size_t dna_seg_begin =0, dna_seg_end =0;

	// reset counter
	strip_size = 0;
	line_arr.nline = 0;

	// Regex_Search on input source
	while ( boost::xpressive::regex_search((const char*)p_src_beg, (const char*)p_src_end, match_result, strip_ptn) )
	{
		size_t bytes_to_copy = match_result[0].first - p_src_beg;

		if (bytes_to_copy > 0)	// normal data line
		{
			// copy to DataLine. 60 chars from [src_beg to '\n')
			memcpy(p_lines_array[line_arr.nline].data, p_src_beg, bytes_to_copy);
			p_lines_array[line_arr.nline].data_len = bytes_to_copy;
			++line_arr.nline;
			
			// copy to simple - flat array
			memcpy(p_flat_array + dna_seg_end, p_src_beg, bytes_to_copy);
			dna_seg_end += bytes_to_copy;
			
			strip_size += bytes_to_copy;
		}
		else		// header data line
		{
			if (dna_seg_end > 0)
			{
				char_arr.nsegment.push_back (Segment(dna_seg_begin, dna_seg_end));
				dna_seg_begin = dna_seg_end;
			}
		}

		// update source pointer
		p_src_beg = match_result[0].second;
	}

	// add last DNA segment to list
	if (dna_seg_begin < dna_seg_end)
		char_arr.nsegment.push_back (Segment(dna_seg_begin, dna_seg_end));

	free(p_input_buffer);
}


void CountPatternsMatch(InputAsCharArray &flat_array, std::string &result)
{
	static char const* patterns[] = 
	{
		"agggtaaa|tttaccct",
		"[cgt]gggtaaa|tttaccc[acg]",
		"a[act]ggtaaa|tttacc[agt]t",
		"ag[act]gtaaa|tttac[agt]ct",
		"agg[act]taaa|ttta[agt]cct",
		"aggg[acg]aaa|ttt[cgt]ccct",
		"agggt[cgt]aa|tt[acg]accct",
		"agggta[cgt]a|t[acg]taccct",
		"agggtaa[cgt]|[acg]ttaccct"
	};
	static const size_t npatttern = sizeof(patterns) / sizeof(patterns[0]);
	
	// element X in this array, counts how many matched so far, of pattern X
	static size_t matched_count[npatttern] = {0};
	// element X in this array, stores how many dna segments, which has been processed with pattern X
	static size_t segment_processed[npatttern] = {0};
	size_t const nsegments = flat_array.nsegment.size ();

	for (size_t ipt = 0; ipt < npatttern; ++ipt)
	{
		// runtime compile regex
		cregex const &search_ptn(cregex::compile(patterns[ipt], regex_constants::nosubs|regex_constants::optimize));

		size_t job = 0;
		// Parallel search, work is divided by dna segments
		while ((job = __sync_fetch_and_add(&segment_processed[ipt], 1)) < nsegments)
		{
			char const *pbegin	= flat_array.data.get() + flat_array.nsegment[job].first;
			char const *pend	= flat_array.data.get() + flat_array.nsegment[job].second;

			cregex_iterator ite(pbegin, pend, search_ptn), ite_end;
			size_t count = std::distance (ite, ite_end);
			
			#pragma omp atomic
			matched_count[ipt] += count;
		}
	}

	// we want the last thread, reaching this code block, to print result
	static size_t thread_passed = 0;
	if (__sync_add_and_fetch(&thread_passed, 1) == static_cast<size_t>(omp_get_num_threads()))
	{
		boost::format fmt("%1% %2%\n");
		for (size_t i = 0; i < npatttern; ++i)
		{
			fmt % patterns[i] % matched_count[i];
			result += fmt.str();
		}
		thread_passed = 0;
	}
}


struct IUB
{
	char const*	iu_replace;
	int			replace_length;
};

static 
IUB const iub[] = 
{
	{"(c|g|t)",		7},	// B
	{"(a|g|t)",		7},	// D
	{"(a|c|t)",		7},	// H
	{"(g|t)",		5},	// K
	{"(a|c)",		5},	// M
	{"(a|c|g|t)",	9},	// N
	{"(a|g)",		5},	// R
	{"(c|t)",		5},	// S
	{"(a|c|g)",		7},	// V
	{"(a|t)",		5},	// W
	{"(c|t)",		5}	// Y
};
size_t const n_search_ptn = sizeof(iub)/sizeof(iub[0]);

int GetReplacementIndex(char c)
{
	int r;
	switch (c)
	{
	case 'B':	r = 0;	break;
	case 'D':	r = 1;	break;
	case 'H':	r = 2;	break;
	case 'K':	r = 3;	break;
	case 'M':	r = 4;	break;
	case 'N':	r = 5;	break;
	case 'R':	r = 6;	break;
	case 'S':	r = 7;	break;
	case 'V':	r = 8;	break;
	case 'W':	r = 9;	break;
	case 'Y':	r = 10;	break;
	default:	r = -1;	break;
	}

	assert(0 <= r);
	return r;
}

void Replace_Patterns(InputAsDataLines const &lines_array, size_t &replace_len)
{
	static size_t line_processed = 0;
	char tmpbuf[DataLine::MAX_LEN];

	cregex const &search_ptn(cregex::compile("[BDHKMNRSVWY]", regex_constants::optimize));
	cmatch search_matched;

	// Fetch line hasn't been processed
	// This is parallel replace, work is divided by lines
	size_t job;
	while ((job = __sync_fetch_and_add(&line_processed, 1)) <  lines_array.nline)
	{
		DataLine &dataline = lines_array.data[job];
		size_t char_copied = 0;

		const char* psrc_beg = dataline.data;
		const char* psrc_end = dataline.data + dataline.data_len;

		bool changed = false;
		while ( regex_search(psrc_beg, psrc_end, search_matched, search_ptn) )
		{
			// copy prefix [string[0] .. first char matched)
			size_t prefix = search_matched[0].first - psrc_beg;
			memcpy(tmpbuf + char_copied, psrc_beg, prefix);
			char_copied += prefix;

			// copy [replace_pattern]
			int ptn_idx = GetReplacementIndex(*(search_matched[0].first));
			memcpy(tmpbuf + char_copied, iub[ptn_idx].iu_replace, iub[ptn_idx].replace_length);
			char_copied += iub[ptn_idx].replace_length;

			// set pointer to next search position
			psrc_beg = search_matched[0].second;
			changed = true;
		}

		if (changed)
		{
			// copy [last char matched - end line)
			{
				size_t suffix = psrc_end - psrc_beg;
				memcpy(tmpbuf + char_copied, psrc_beg, suffix);
				char_copied += suffix;
			}

			// replace existing data line
			memcpy(dataline.data, tmpbuf, char_copied);
			dataline.data_len = char_copied;
		}

		// update replaced sequence length
		#pragma omp atomic
		replace_len += dataline.data_len;
	}
}



// Detect single - multi thread benchmark
static
int GetThreadCount()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	for (int i = 0; i < 16; i++)
	{
		if (CPU_ISSET(i, &cs))
		count++;
	}
	return count;
}


int main()
{
	size_t initial_length = 0;
	size_t striped_length = 0;
	size_t replace_length = 0;
	
	InputAsDataLines data_as_lines;
	InputAsCharArray data_as_flat_array;
	
	ReadInput_StripHeader (initial_length, striped_length, 
		data_as_flat_array, data_as_lines);

	std::string match_result;
	#pragma omp parallel default(shared) num_threads(GetThreadCount())
	{
		CountPatternsMatch(data_as_flat_array, match_result);
		Replace_Patterns (data_as_lines, replace_length);
	}


	std::cout << match_result << std::endl
		<< initial_length	<< std::endl
		<< striped_length	<< std::endl
		<< replace_length	<< std::endl;
}

