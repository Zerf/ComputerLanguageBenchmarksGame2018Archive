// The Great Computer Language Shootout
// http://shootout.alioth.debian.org/
//
// Compile: g++ -O2 -fomit-frame-pointer -fopenmp -msse2 -march=k8 -mfpmath=sse -o spectralnorm spectralnorm.cpp
//
// Original C contributed by Sebastien Loisel
// Conversion to C++ by Jon Harrop
// OpenMP parallelize by The Anh Tran

#include <cmath>
#include <cstdlib>
#include <cstdio>

// Use OpenMP. Supported from GCC >= v4.2.xx
// need "-fopenmp" flag when compile
#include <omp.h>

double eval_A(int i, int j) 
{
	// 1.0 / (i + j) * (i + j +1) / 2 + i + 1;
	// n * (n+1) is even number. Therefore, just (>> 1) for (/2)
	int d = (	( (i + j) * (i + 1 +j) ) >> 1	) 	+ i + 1;

	return 1.0 / d; 
}



void eval_A_times_u (const double u[], int inRange, double Au[], int outRange1, int outRange2)
{
	for (int i =outRange1; i < outRange2; i++)
	{
		double sum = 0.0;
		for (int j =0; j < inRange; j++)
			sum += eval_A(i, j) * u[j];
		Au[i] = sum;
	}
}


void eval_At_times_u(const double u[], int inRange, double Au[], int outRange1, int outRange2)
{
	for (int i =outRange1; i < outRange2; i++)
	{
		double sum = 0.0;
		for (int j =0; j < inRange; j++)
			sum += eval_A(j, i) * u[j];
		Au[i] = sum;
	}
}


// This function is executed by N threads.
// Each thread will fill its chunk only [range1 .. range2) 
void eval_AtA_times_u(const double u[], double AtAu[], double v[], int inRange, int range1, int range2)
{
	eval_A_times_u( u, inRange, v, range1, range2 ); 

	// synchronize at completion of array "v" before going to fill AtAu. 
	// Because each element of AtAu reading all elements of "v"
	#pragma omp barrier

	eval_At_times_u( v, inRange, AtAu, range1, range2 ); 

	// syn on array AtAu
	#pragma omp barrier
}


double spectral_game(int N)
{
	//__attribute__((aligned(16))) 
	double u[N];
	double tmp[N];
	double v[N];

	double vBv	= 0.0;
	double vv	= 0.0;

	// fill 1.0	
	// only 44kB, fit in L2 cache, not parallel worthy
	//#pragma omp for schedule(static)
	for (int i = 0; i < N; i++)
		u[ i ] = 1.0;

	#pragma omp parallel default(shared)
	{
		// this block will be executed by NUM_THREADS
		// variable declared in this block is private for each thread
		int threadid	= omp_get_thread_num();
		int threadcount	= omp_get_num_threads();
		int chunk		= N / threadcount;

		// calculate each thread's working range [range1 .. range2)	--> static schedule here
		int myRange1 = threadid * chunk;
		int myRange2 = ( threadid < (threadcount -1) ) ? (myRange1 + chunk) : N;

		for (int ite = 0; ite < 10; ite++) 
		{
			eval_AtA_times_u(u, v, tmp, N, myRange1, myRange2);
			eval_AtA_times_u(v, u, tmp, N, myRange1, myRange2);
		}

		// multi thread adding
		#pragma omp for schedule(static) reduction( + : vBv, vv ) nowait
		for (int i =0; i < N; i++) 
		{
			vv += v[ i ] * v[ i ];
			vBv += u[ i ] * v[ i ];
		}
	} // end parallel region

	return sqrt( vBv / vv );
}


int main(int argc, char *argv[])
{
	int N = ((argc == 2) ? atoi(argv[1]) : 2000);
	
	// main calculation
	double result = spectral_game(N);

	printf("%.9f\n", result);
	return 0;
}

