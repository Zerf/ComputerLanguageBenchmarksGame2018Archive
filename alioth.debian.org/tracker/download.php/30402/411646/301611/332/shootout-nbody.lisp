(defconstant +days-per-year+ 365.24d0)
(defconstant +solar-mass+ (the double-float (* 4d0 pi pi)))

(defstruct body
  (pos (make-array 3 :element-type 'double-float) :type (simple-array double-float (3)))
  (vel (make-array 3 :element-type 'double-float) :type (simple-array double-float (3)))
  (mass 0.0d0 :type double-float))

(defun make-body1 (x y z vx vy vz m)
  (let ((b (make-body))
	(pos (make-array 3 :element-type 'double-float 
			 :initial-contents (list x y z)))
	(vel (make-array 3 :element-type 'double-float 
			 :initial-contents (list vx vy vz))))
    (setf (body-pos b) pos)
    (setf (body-vel b) vel)
    (setf (body-mass b) m)
    b))

(defparameter *jupiter*
  (make-body1 4.84143144246472090d0
	     -1.16032004402742839d0
	     -1.03622044471123109d-1
	     (* 1.66007664274403694d-3 +days-per-year+)
	     (* 7.69901118419740425d-3 +days-per-year+)
	     (* -6.90460016972063023d-5  +days-per-year+)
	     (* 9.54791938424326609d-4 +solar-mass+)))

(defparameter *saturn*
  (make-body1 8.34336671824457987d0
    4.12479856412430479d0
    -4.03523417114321381d-1
    (* -2.76742510726862411d-3 +days-per-year+)
    (* 4.99852801234917238d-3 +days-per-year+)
    (* 2.30417297573763929d-5 +days-per-year+)
    (* 2.85885980666130812d-4 +solar-mass+)))

(defparameter *uranus*
  (make-body1     1.28943695621391310d1
    -1.51111514016986312d1
    -2.23307578892655734d-1
    (* 2.96460137564761618d-03 +days-per-year+)
    (* 2.37847173959480950d-03 +days-per-year+)
    (* -2.96589568540237556d-05 +days-per-year+)
    (* 4.36624404335156298d-05 +solar-mass+)))

(defparameter *neptune*
  (make-body1  1.53796971148509165d+01
    -2.59193146099879641d+01
    1.79258772950371181d-01
    (* 2.68067772490389322d-03 +days-per-year+)
    (* 1.62824170038242295d-03 +days-per-year+)
    (* -9.51592254519715870d-05 +days-per-year+)
    (* 5.15138902046611451d-05 +solar-mass+)))

(defparameter *sun*
  (make-body1 0.0d0 0.0d0 0.0d0 0.0d0 0.0d0 0.0d0 +solar-mass+))

(defun applyforces (x y dt)
  (declare (optimize speed))
  (declare (type body x y)
	   (type double-float dt))
  (let* ((dx (- (aref (body-pos x) 0) (aref (body-pos y) 0)))
	 (dy (- (aref (body-pos x) 1) (aref (body-pos y) 1)))
	 (dz (- (aref (body-pos x) 2) (aref (body-pos y) 2)))
	 (distance (sqrt (the (double-float 0.0d0) (+ (* dx dx) (* dy dy) (* dz dz)))))
	 (mag (/ dt distance distance distance)))
    (setf (aref (body-vel x) 0) (- (aref (body-vel x) 0) (* dx (body-mass y) mag)))
    (setf (aref (body-vel x) 1) (- (aref (body-vel x) 1) (* dy (body-mass y) mag)))
    (setf (aref (body-vel x) 2) (- (aref (body-vel x) 2) (* dz (body-mass y) mag)))
    (setf (aref (body-vel y) 0) (+ (aref (body-vel y) 0) (* dx (body-mass x) mag)))
    (setf (aref (body-vel y) 1) (+ (aref (body-vel y) 1) (* dy (body-mass x) mag)))
    (setf (aref (body-vel y) 2) (+ (aref (body-vel y) 2) (* dz (body-mass x) mag))))
  nil)

(defun move (b dt)
  (declare (type body b)
	   (type double-float dt))
  (map-into (body-pos b) (lambda (p v) (+ p (* v dt))) (body-pos b) (body-vel b)))

(defun advance (system dt)
  (loop for i from 0 below (length system) do
       (loop for j from (1+ i) below (length system) do
	    (applyforces (aref system i) (aref system j) dt)))
  (loop for i across system do
       (move i dt)))

(defun energy (system)
  (let ((e 0.0d0))
    (loop for i from 0 below (array-dimension system 0) do
	  (let ((b (aref system i)))
	    (setf e (+ e (* 0.5d0 (body-mass b) (loop for v across (body-vel b) sum (* v v)))))
	    (loop for j from (1+ i) below (array-dimension system 0) do
		  (let* ((b2 (aref system j))
			 (dist (sqrt (loop for b across (body-pos b) and
					   b2 across (body-pos b2) sum (* (- b b2) (- b b2))))))
		    (setf e (- e (/ (* (body-mass b) (body-mass b2)) dist)))))))
    e))

(defun offset-momentum (system)
  (let ((px 0.0d0)
	(py 0.0d0)
	(pz 0.0d0))
    (loop for p across system do
	  (setf px (+ px (* (aref (body-vel p) 0) (body-mass p))))
	  (setf py (+ py (* (aref (body-vel p) 1) (body-mass p))))
	  (setf pz (+ pz (* (aref (body-vel p) 2) (body-mass p)))))
    (setf (aref (body-vel (aref system 0)) 0) (/ (- px) +solar-mass+))
    (setf (aref (body-vel (aref system 0)) 1) (/ (- py) +solar-mass+))
    (setf (aref (body-vel (aref system 0)) 2) (/ (- pz) +solar-mass+))))


(defun nbody (n)
  (let ((system (make-array 5 :element-type 'body :initial-contents
			    (list *sun* *jupiter* *saturn* *uranus* *neptune*))))
    (offset-momentum system)
    (format t "~,9F~%" (energy system))
    (loop for i from 1 upto n do
	  (advance system 0.01d0))
    (format t "~,9F~%" (energy system))))

(defun main ()
  (let ((n (parse-integer (or (car (last #+sbcl sb-ext:*posix-argv*
                                         #+cmu  extensions:*command-line-strings*
					 #+gcl  si::*command-args*)) "1"))))
    (nbody n)))