#!/usr/bin/tclsh

proc main {} {
    set sum 0
    catch {
        while {1} { incr sum [gets stdin] }
    }
    puts $sum
}

main


