rem The Computer Language Shootout
rem http://shootout.alioth.debian.org/
rem contributed by Josh Goldfoot
rem based on the D version by Dave Fladebo

dim sum(8) as double
dim as double k, k2, k3, ks, kc, tt
n = val(command$) + 1
if n = 0 then n = 25001
alt = 1.0
tt = 2 / 3

for k = 1 to n-1
   k2 = k * k
   k3 = k2 * k
   ks = sin(k)
   kc = cos(k)
   
   sum(0) += tt ^ (k - 1)
   sum(1) += 1 / sqr(k)
   sum(2) += 1 / (k * (k + 1))
   sum(3) += 1 / (k3 * ks * ks)
   sum(4) += 1 / (k3 * kc * kc)
   sum(5) += 1 / k
   sum(6) += 1 / k2
   sum(7) += alt / k
   sum(8) += alt / (2 * k - 1)
   alt *= -1
next k

REM this is necessary because "print using" rounds poorly
dim factor as longint
factor = 1000000000
for i = 0 to 8
   sum(i) = clngint(sum(i) * factor) / factor
next i

fmt$ = "#.#########"

print using fmt$; sum(0); 
print chr(9); "(2/3)^k"
a$ = str(sum(1))
print left(a$, instr(a$,".")+9);
print chr(9); "k^-0.5"
print using fmt$; sum(2);
print chr(9); "1/k(k+1)"
print using fmt$; sum(3);
print chr(9); "Flint Hills"
print using fmt$; sum(4);
print chr(9); "Cookson Hills"
print using fmt$; sum(5);
print chr(9); "Harmonic"
print using fmt$; sum(6);
print chr(9); "Riemann Zeta"
print using fmt$; sum(7);
print chr(9); "Alternating Harmonic"
print using fmt$; sum(8);
print chr(9); "Gregory"


