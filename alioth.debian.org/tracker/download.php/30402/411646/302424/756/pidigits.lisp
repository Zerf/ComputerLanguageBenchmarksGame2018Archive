;;    The Great Computer Language Shootout
;;    http://shootout.alioth.debian.org/
;;
;;    contributed by Christopher Neufeld  <shootout0000@cneufeld.ca>
;;    modified by Juho Snellman 2005-10-25
;;      * Replaced (FLOOR (/ ... ...)) with (FLOOR ... ...). Avoids
;;        creation of useless and very expensive temporary rationals.
;;      * Fixed output format (too many spaces)

;;; A streaming pidigits implementation in ANSI Lisp for the debian
;;; shootout.  Aimed at sbcl, but tested on clisp and gcl as well.

(defparameter *digits-per-line* 10)
(defparameter *stop-digits* 300)

(defun compute-pi (next safe prod consume z next-state)
  (do ((digits-out 0))
      ((>= digits-out *stop-digits*))

    (let ((y (funcall next z)))
      (if (funcall safe z y)
          (progn
            (format t "~D" y)
            (incf digits-out)
            (if (zerop (mod digits-out *digits-per-line*))
                (format t "	:~D~%" digits-out))
            (setf z (funcall prod z y)))
          (let ((state (funcall next-state)))
            (setf z (funcall consume z state)))))))

(defun comp (a1 a2)
  (let ((retval (make-array 4)))
    (setf (aref retval 0) (+ (* (aref a1 0) (aref a2 0))
                             (* (aref a1 1) (aref a2 2))))
    (setf (aref retval 1) (+ (* (aref a1 0) (aref a2 1))
                             (* (aref a1 1) (aref a2 3))))
    (setf (aref retval 2) (+ (* (aref a1 2) (aref a2 0))
                             (* (aref a1 3) (aref a2 2))))
    (setf (aref retval 3) (+ (* (aref a1 2) (aref a2 1))
                             (* (aref a1 3) (aref a2 3))))
    retval))

(defun extr (state x)
  (declare (type (array integer 1) state))
  (declare (type integer x))
  (declare (optimize (speed 3) (safety 0) (space 0)))
  (floor (+ (* (aref state 0) x) (aref state 1))
         (+ (* (aref state 2) x) (aref state 3))))

(defparameter init (make-array 4 :element-type 'integer :initial-contents #(1 0 0 1)))
(defparameter *curstate* (make-array 4 :element-type 'integer :initial-contents #(0 2 0 1)))

(defun next-state ()
  (incf (aref *curstate* 0))
  (incf (aref *curstate* 1) 4)
  (incf (aref *curstate* 3) 2)
  *curstate*)

(defun safe (z n)
  (= n (extr z 4)))

(defun next (z)
  (extr z 3))

(defun prod (z n)
  (let ((v1 (make-array 4)))
    (setf (aref v1 0) 10)
    (setf (aref v1 1) (* n -10))
    (setf (aref v1 2) 0)
    (setf (aref v1 3) 1)
    (comp v1 z)))

(defun consume (z z-prime)
  (comp z z-prime))

(defun main ()
  (let ((n (parse-integer (or (car (last #+sbcl sb-ext:*posix-argv*
                                         #+clisp ext:*args*
					 #+cmu extensions:*command-line-strings*
					 #+gcl  si::*command-args*)) "300"))))
    (if n
        (setf *stop-digits* n)))
  (compute-pi #'next #'safe #'prod #'consume init #'next-state))
