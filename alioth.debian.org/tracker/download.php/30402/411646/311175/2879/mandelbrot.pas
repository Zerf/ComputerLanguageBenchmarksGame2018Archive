{ The Computer Language Benchmarks Game
  http://shootout.alioth.debian.org

  contributed by Ales Katona
  modified by Vincent Snijders
  modified by Jeppe Johansen
}

program mandelbrot;

uses sysutils;

var n, yg: longint;
    
	 fThreads: longint;
	 fBuffer: PByte;
	 fLen: plongint;

function run(param: pointer): longint;
var
  Cy, Step: double;
  lx, x, y, bits,bit: Longint;
  function CalculatePoint(const Cx, Cy: double): boolean; nostackframe; inline;
  const
    Limit = 4;
  var
    i: longint;
    Zr, Zi, Ti, Tr: Double;

  begin
    Zr := 0;  Zi := 0; Tr := 0; Ti := 0;
    for i := 1 to 50 do begin
      Zi := 2*Zr*Zi + Cy;
      Zr := Tr - Ti + Cx;
      Ti := Zi * Zi;
      Tr := Zr * Zr;
      if (Tr + Ti>=limit) then exit(true);
    end;

    CalculatePoint := false;
  end;

begin
  run := 0;
  Step := 2/n;
  while true do
  begin
    y := interlockedIncrement(yg)-1;
	 if y >= n then
		break;
    Cy := y * Step - 1;
    bits := 255;  bit := 128;
	 lx := 0;
    for x := 0 to n-1 do
    begin
      if CalculatePoint(x * Step  - 1.5, Cy) then
        bits := bits xor bit;

      if bit > 1 then
        bit := bit shr 1
      else
      begin
		  fBuffer[y*(n+1)+lx] := bits;
		  inc(lx);
        bits := 255;  bit := 128;
      end;
    end;
    if bit < 128 then
	 begin
		fBuffer[y*(n+1)+lx] := bits xor((bit shl 1)-1);
		inc(lx);
	 end;
	 flen[y] := lx;
  end;
  
  interlockeddecrement(fThreads);
  EndThread(0);
end;

var c: longword;
	 i, i2: longint;
begin
  Val(ParamStr(1), n);
  
  yg := 0;
  fBuffer := GetMem((n+1)*n);
  fLen := GetMem(n*4);
  
  writeln('P4');
  writeln(n,' ',n);
  
  fThreads := n shr 5; //Start a number of calculation threads to take advantage of SMP systems
  for i := 0 to fThreads-1 do
    BeginThread(nil, 0, @run, nil,0,c);

  while fThreads > 0 do sleep(0); //There exists no good crossplatform synchronization primitives
  
  for i := 0 to n-1 do //Write the output to a file
    for i2 := 0 to fLen[i]-1 do
	   write(chr(fBuffer[i*(n+1)+i2]));
  
  freemem(fBuffer);
  freemem(flen);
end.
