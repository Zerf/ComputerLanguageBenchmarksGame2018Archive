rem The Computer Language Shootout
rem http://shootout.alioth.debian.org/
rem contributed by Josh Goldfoot
rem based on the C version by Greg Buchholz

option explicit

sub main()
    dim as integer w, h, x, y, stdout, i, iter, bit_num
    dim as ubyte byte_acc
    dim as double limit, zr, zi, cr, ci, tr, ti
    iter = 50
    limit = 2.0
    limit = limit * limit

    w = val(command$)
    if w < 1 then w = 300
    h = w
    stdout = freefile
    open cons for binary as #stdout
    print "P4"
    print str(w);h

    for y = 0 to h-1
        for x = 0 to w-1
            zr = 0.0 : zi = 0.0
            cr = (2*x/w - 1.5) : ci=(2*y/h - 1)

            for i = 0 to iter-1
                tr = zr*zr - zi*zi + cr
                ti = 2*zr*zi + ci
                zr = tr : zi = ti
                if zr*zr+zi*zi > limit then exit for
            next i

            if zr*zr+zi*zi > limit then
                byte_acc = (byte_acc shl 1)
            else
                byte_acc = (byte_acc shl 1) + 1
            end if

            bit_num += 1

            if bit_num = 8 then
                put #stdout, ,byte_acc
                byte_acc = 0
                bit_num = 0
            elseif x = w-1 then
                byte_acc = byte_acc shl (8 - w mod 8)
                put #stdout, ,byte_acc
                byte_acc = 0
                bit_num = 0
            end if

        next x
    next y
end sub

main()

