%%
%% The Computer Language Benchmarks Game
%% http://shootout.alioth.debian.org/
%%
%% Based on the Python version by Dominique Wahli,
%% and the Erlang version by Sergei Matusevich.
%% 
%% contributed by Michael Pitidis
%%

%% This version uses the experimental PCRE module for Erlang,
%% http://www.erlang.org/eeps/eep-0011.html
%%
%% Tested with Erlang R13A (erts-5.7) (HiPE CVS).
%%
%% The variant counts are computed in parallel, but a simple
%% sequential version is provided for reference as well.
%% Still, the major bottleneck is Erlang's IO from stdin (reading
%% the file with file:read_file/1 is about twice as fast for me),
%% and the first replacement, which cannot be parallelized(?).

-module(regexdna).

-compile([native, {hipe, [o3]}, inline, {inline_size, 100}]).

%-compile(export_all).
-export([main/1]).

-define(VARIANTS, [
  "agggtaaa|tttaccct",
  "[cgt]gggtaaa|tttaccc[acg]",
  "a[act]ggtaaa|tttacc[agt]t",
  "ag[act]gtaaa|tttac[agt]ct",
  "agg[act]taaa|ttta[agt]cct",
  "aggg[acg]aaa|ttt[cgt]ccct",
  "agggt[cgt]aa|tt[acg]accct",
  "agggta[cgt]a|t[acg]taccct",
  "agggtaa[cgt]|[acg]ttaccct"
] ).

-define(SUBSTS, [
  {"K", "(g|t)"},
  {"M", "(a|c)"},
  {"R", "(a|g)"},
  {"S", "(c|g)"},
  {"W", "(a|t)"},
  {"Y", "(c|t)"},
  {"B", "(c|g|t)"},
  {"D", "(a|g|t)"},
  {"H", "(a|c|t)"},
  {"V", "(a|c|g)"},
  {"N", "(a|c|g|t)"}
] ).

main([_Arg]) ->
  io:setopts([binary]),
  case _Arg of
    "seq" -> run_sequential();
    _ -> run_parallel()
  end,
  halt().

run_sequential() ->
  S1 = read_input(<< >>),
  S2 = strip(S1),
  S3 = substitute_variants(S2),
  [ print(count_variants(S2, V)) || V <- ?VARIANTS ],
  print(empty),
  [ print(bsize(S)) || S <- [S1, S2, S3] ].

%% Simple concurrent approach, run everything in parallel,
%% collect the results, sort and print them.
run_parallel() ->
  register(collector, self()),
  S1 = read_input(<< >>),
  S2 = strip(S1),
  Jobs = [ { fun count_variants/2, [S2, V] } || V <- ?VARIANTS ]
      ++ [ { fun() -> empty end, [] },
           { fun bsize/1, [S1] },
           { fun bsize/1, [S2]},
           { fun(S) -> bsize(substitute_variants(S)) end, [S2] } ],
  N = schedule(Jobs),
  [ print(R) || R <- collect(N) ].

%% Substitution helpers.
strip(S) -> re:replace(S, "(>.*\n)|\n", "", [global]).

substitute_variants(S) -> lists:foldl(fun substitute_variant/2, S, ?SUBSTS).

substitute_variant({What, With}, Seq) -> re:replace(Seq, What, With, [global]).

count_variants(Seq, Var) ->
  Len = case re:run(Seq, Var, [global]) of
    {match, Matches} -> length(Matches);
    nomatch -> 0
  end,
  {Var, Len}.

%% Parallel helpers.
schedule(Jobs) -> lists:foldl(fun new_job/2, 0, Jobs).

new_job({Fun, Args}, N) ->
  spawn(fun() -> collector ! {N, apply(Fun, Args)} end),
  N + 1.

collect(N) -> collect(N, []).
collect(0, Results) -> [ R || {_, R} <- lists:keysort(1, Results) ];
collect(N, Results) -> receive {K, R} -> collect(N-1, [{K, R} | Results]) end.

%% General helpers.
print( empty ) -> io:nl();
print({ Var, Count }) -> io:format("~s ~p~n", [Var, Count]);
print( Len ) -> io:format("~p~n", [Len]).

read_input(Buf) ->
  case io:get_line('') of
    eof -> Buf;
    % Appending to binaries in this manner should be efficient, according to
    % http://erlang.org/doc/efficiency_guide/binaryhandling.html#4
    Line -> read_input(<< Buf/binary, Line/binary >>)
  end.

bsize(B) when is_list(B)   -> size(list_to_binary(B));
bsize(B) when is_binary(B) -> size(B).
