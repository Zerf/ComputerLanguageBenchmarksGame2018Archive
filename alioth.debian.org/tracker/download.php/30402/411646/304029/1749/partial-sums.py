#!/usr/bin/env python

from math import sin, cos, sqrt
import sys
import time

n = 25001
if len(sys.argv) > 1:
   n = int(sys.argv[1])

twoThirds = 2.0/3.0
iterations = range(1,n)

sums0 = sum( twoThirds**(x-1) for x in iterations )
sums1 = sum( 1.0/sqrt(x) for x in iterations )
sums2 = sum( 1.0/(x * (x+1)) for x in iterations )
sums3 = sum( 1.0/(x * x * x * sin(x) * sin(x)) for x in iterations )
sums4 = sum( 1.0/(x * x * x * cos(x) * cos(x)) for x in iterations )
sums5 = sum( 1.0/x for x in iterations )
sums6 = sum( 1.0/(x*x) for x in iterations )
sums7 = sum( (-1.0,1.0)[x%2]/x  for x in iterations )
sums8 = sum( (-1.0,1.0)[x%2]/(x * 2 - 1)  for x in iterations )

print "%0.9f     (2/3)^k-1" % sums0
print "%0.9f     k^-0.5" % sums1
print "%0.9f     1/k(k+1)" % sums2
print "%0.9f     Flint Hills" % sums3
print "%0.9f     Cookson Hills" % sums4
print "%0.9f     Harmonic" % sums5
print "%0.9f     Riemann Zeta" % sums6
print "%0.9f     Alternating Harmonic" % sums7
print "%0.9f     Gregory" % sums8
