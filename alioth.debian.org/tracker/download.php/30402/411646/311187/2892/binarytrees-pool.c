/*
 * The Computer Language Shootout Benchmarks
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Eckehard Berns
 * Based on code by Kevin Carson
 */

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

typedef struct node {
	struct node *left, *right;
	long item;
} node;

typedef struct node_pool {
	long len, pos;
	node *nodes;
} nodepool;

static node *
new_node(nodepool *pool, node *left, node *right, long item)
{
	node *ret;

	ret = &pool->nodes[pool->pos++];
	ret->left = left;
	ret->right = right;
	ret->item = item;

	return ret;
}

static long
item_check(node *tree)
{
	if (tree->left == NULL)
		return tree->item;
	else
		return tree->item + item_check(tree->left) -
		    item_check(tree->right);
}

static node *
bottom_up_tree(nodepool *pool, long item, int depth)
{
	if (depth > 0)
		return new_node(pool,
		    bottom_up_tree(pool, 2 * item - 1, depth - 1),
		    bottom_up_tree(pool, 2 * item, depth - 1), item);
	else
		return new_node(pool, NULL, NULL, item);
}

struct worker_args {
	long iter, check;
	int depth;
	pthread_t id;
	struct worker_args *next;
};

static void *
check_tree_of_depth(void *_args)
{
	nodepool pool;
	struct worker_args *args = _args;
	long i, iter, check, depth;
	node *tmp;

	iter = args->iter;
	depth = args->depth;

	pool.len = (1 << (depth + 1)) - 1;
	pool.pos = 0;
	pool.nodes = malloc(sizeof(node) * pool.len);

	check = 0;
	for (i = 1; i <= iter; i++) {
		tmp = bottom_up_tree(&pool, i, depth);
		check += item_check(tmp);
		pool.pos = 0;

		tmp = bottom_up_tree(&pool, -i, depth);
		check += item_check(tmp);
		pool.pos = 0;
	}

	free(pool.nodes);

	args->check = check;
	return NULL;
}

int
main(int ac, char **av)
{
	nodepool pool;
	node *stretch, *longlived;
	struct worker_args *args, *targs, *hargs;
	int n, depth, mindepth, maxdepth, stretchdepth;

	n = ac > 1 ? atoi(av[1]) : 10;
	if (n < 1) {
		fprintf(stderr, "Wrong argument.\n");
		exit(1);
	}

	mindepth = 4;
	maxdepth = mindepth + 2 > n ? mindepth + 2 : n;
	stretchdepth = maxdepth + 1;

	pool.len = (1 << (stretchdepth + 1)) - 1;
	pool.pos = 0;
	pool.nodes = malloc(sizeof(node) * pool.len);
	stretch = bottom_up_tree(&pool, 0, stretchdepth);
	printf("stretch tree of depth %u\t check: %li\n", stretchdepth,
	    item_check(stretch));
	pool.pos = 0;
	free(pool.nodes);
	pool.len = (1 << (maxdepth + 1)) - 1;
	pool.nodes = malloc(sizeof(node) * pool.len);

	longlived = bottom_up_tree(&pool, 0, maxdepth);

	hargs = NULL;
	targs = NULL;
	for (depth = mindepth; depth <= maxdepth; depth += 2) {

		args = malloc(sizeof(struct worker_args));
		args->iter = 1 << (maxdepth - depth + mindepth);
		args->depth = depth;
		args->next = NULL;
		if (targs == NULL) {
			hargs = args;
			targs = args;
		} else {
			targs->next = args;
			targs = args;
		}
		pthread_create(&args->id, NULL, check_tree_of_depth, args);
	}

	while (hargs != NULL) {
		args = hargs;
		pthread_join(args->id, NULL);
		printf("%ld\t trees of depth %d\t check: %ld\n",
		    args->iter * 2, args->depth, args->check);
		hargs = args->next;
		free(args);
	}

	printf("long lived tree of depth %d\t check: %ld\n", maxdepth,
	    item_check(longlived));

	/* not in original C version: */
	free(pool.nodes);

	return 0;
}

