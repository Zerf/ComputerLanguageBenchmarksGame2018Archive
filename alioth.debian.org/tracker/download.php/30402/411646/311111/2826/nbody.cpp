/*
* The Great Computer Language Shootout
* http://shootout.alioth.debian.org/
*
* C version contributed by Christoph Bauer
* Slightly improved by Mark Hinds
* OpenMP by The Anh Tran
*/


#include <math.h>
#include <stdio.h>
#include <stdlib.h>

// need "-fopenmp" flag
#include <omp.h>

#define pi M_PI
#define solar_mass (4 * pi * pi)
#define days_per_year 365.24
#define ADV_TIME	0.01

typedef struct _tag_planet_
{
	double x, y, z;
	double vx, vy, vz;
	double mass;
} planet;

#define NBODIES 5
planet bodies[NBODIES] = 
{
	{                               /* sun */
		0, 0, 0, 0, 0, 0, solar_mass
	},
	{                               /* jupiter */
		4.84143144246472090e+00,
		-1.16032004402742839e+00,
		-1.03622044471123109e-01,
		1.66007664274403694e-03 * days_per_year,
		7.69901118419740425e-03 * days_per_year,
		-6.90460016972063023e-05 * days_per_year,
		9.54791938424326609e-04 * solar_mass
	},

	{                               /* saturn */
		8.34336671824457987e+00,
		4.12479856412430479e+00,
		-4.03523417114321381e-01,
		-2.76742510726862411e-03 * days_per_year,
		4.99852801234917238e-03 * days_per_year,
		2.30417297573763929e-05 * days_per_year,
		2.85885980666130812e-04 * solar_mass
	},

	{                               /* uranus */
		1.28943695621391310e+01,
		-1.51111514016986312e+01,
		-2.23307578892655734e-01,
		2.96460137564761618e-03 * days_per_year,
		2.37847173959480950e-03 * days_per_year,
		-2.96589568540237556e-05 * days_per_year,
		4.36624404335156298e-05 * solar_mass
	},

	{                               /* neptune */
		1.53796971148509165e+01,
		-2.59193146099879641e+01,
		1.79258772950371181e-01,
		2.68067772490389322e-03 * days_per_year,
		1.62824170038242295e-03 * days_per_year,
		-9.51592254519715870e-05 * days_per_year,
		5.15138902046611451e-05 * solar_mass
	}
};

typedef struct _tag_delta_
{
	double	dx;
	double	dy;
	double	dz;
} Delta;

typedef struct _tag_job_
{
	int		line;
	int		column;
	Delta*	dt_upper;
	Delta*	dt_lower;
} Job;

void advance(int nbodies, planet* bodies, int n)
{
	// square matrix[N][N], contains interactive multiplier between 2 planets
	Delta* delta = (Delta*)calloc(nbodies * nbodies, sizeof(Delta));

	/* this 'jobs' array labels the interactive-information of upper triangle matrix
	So works can be divided easily between N threads. Without this, works is unbalance

	Pln	0	1	2	3	4
	0	_	j0	j1	j2	j3
	1	j0	_	j4	j5	j6
	2	j1		_	j7	j8
	3	j2			_	j9
	4	j3				_

	*/
	int jobs_count = ((nbodies * (nbodies-1)) /2);
	Job* jobs = (Job*)calloc( jobs_count, sizeof(Job) );

	// labeling the interaction between planet X and planet Y
	int index = 0;
	for (int ln = 0; ln < (nbodies -1); ln++)
	{
		for ( int cl = ln +1; cl < nbodies; cl++)
		{
			jobs[index].line = ln;
			jobs[index].column = cl;
			
			// each job calculates a multiplier in the Delta matrix
			// so, just store pointer to its [upper + lower] location
			jobs[index].dt_upper = delta + (ln * nbodies + cl);
			jobs[index].dt_lower = delta + (cl * nbodies + ln);

			index++;
		}
	}

	
	#pragma omp parallel default(shared)
	for (int loop = 1; loop <= n; loop++)
	{
		// calculate the multiplier of 2 planets interaction
		#pragma omp for schedule(static) nowait
		for (int idx = 0; idx < jobs_count; idx++) 
		{
			const planet *pln1 = bodies + jobs[idx].line;
			const planet *pln2 = bodies + jobs[idx].column;
		
			double dx = pln1->x - pln2->x;
			double dy = pln1->y - pln2->y;
			double dz = pln1->z - pln2->z;

			double distance2 = dx * dx + dy * dy + dz * dz;
			double distance = sqrt(distance2);

			double mag = ADV_TIME / (distance * distance2);

			dx *= mag;
			dy *= mag;
			dz *= mag;

			jobs[idx].dt_lower->dx = dx;
			jobs[idx].dt_lower->dy = dy;
			jobs[idx].dt_lower->dz = dz;

			*(jobs[idx].dt_upper) = *(jobs[idx].dt_lower);
		}

		// Got interaction multiplier, update each planet velocity & coordinate
		#pragma omp for schedule(static)
		for (int ln = 0; ln < nbodies; ln++)
		{
			Delta* pdt = delta + (ln * nbodies);

			for ( int cl = 0; cl < nbodies; cl++, pdt++ )
			{
				if (ln < cl)
				{
					bodies[ln].vx -= pdt->dx * bodies[cl].mass;
					bodies[ln].vy -= pdt->dy * bodies[cl].mass;
					bodies[ln].vz -= pdt->dz * bodies[cl].mass;
				}
				else
				{
					if (cl < ln)
					{
						bodies[ln].vx += pdt->dx * bodies[cl].mass;
						bodies[ln].vy += pdt->dy * bodies[cl].mass;
						bodies[ln].vz += pdt->dz * bodies[cl].mass;
					}
					else
					{
						continue;
					}
				}
			}

			bodies[ln].x += ADV_TIME * bodies[ln].vx;
			bodies[ln].y += ADV_TIME * bodies[ln].vy;
			bodies[ln].z += ADV_TIME * bodies[ln].vz;
		}
	} // end advancing loop

	free(jobs);
	free(delta);
}

double energy(int nbodies, planet * bodies)
{
	double e = 0.0;

	#pragma omp parallel for default(shared) schedule(static) reduction(+ : e)
	for (int i = 0; i < nbodies; i++) 
	{
		planet * b = &(bodies[i]);
		e += (0.5 * b->mass) * (b->vx * b->vx + b->vy * b->vy + b->vz * b->vz);

		for (int j = i + 1; j < nbodies; j++) 
		{
			planet * b2 = &(bodies[j]);
			
			double dx = b->x - b2->x;
			double dy = b->y - b2->y;
			double dz = b->z - b2->z;
			
			double distance = sqrt(dx * dx + dy * dy + dz * dz);
			
			e -= (b->mass * b2->mass) / distance;
		}
	}

	return e;
}

void offset_momentum(int nbodies, planet * bodies)
{
	double px = 0.0, py = 0.0, pz = 0.0;

	#pragma omp parallel for default(shared) schedule(static) reduction(+ : px, py, pz)
	for (int i = 0; i < nbodies; i++) 
	{
		px += bodies[i].vx * bodies[i].mass;
		py += bodies[i].vy * bodies[i].mass;
		pz += bodies[i].vz * bodies[i].mass;
	}
	
	bodies[0].vx = - px / solar_mass;
	bodies[0].vy = - py / solar_mass;
	bodies[0].vz = - pz / solar_mass;
}


int main(int argc, char ** argv)
{
	int n = (argc == 2)? atoi(argv[1]) : 2000;

	offset_momentum(NBODIES, bodies);
	printf ("%.9f\n", energy(NBODIES, bodies));
	
	advance(NBODIES, bodies, n);
	printf ("%.9f\n", energy(NBODIES, bodies));

	return 0;
}


