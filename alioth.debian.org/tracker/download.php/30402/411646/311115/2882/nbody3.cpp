/*
* The Computer Language Benchmarks Game
* http://shootout.alioth.debian.org/
*
* C version contributed by Christoph Bauer
* Slightly improved by Mark Hinds
* OpenMP by The Anh Tran
*/


#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sched.h>
#include <memory.h>
#include <omp.h>

#define ALG 16


// Reimplement a 'fast' barrier. Spin instead of mutex_lock
int spin[2] = {0};
int num_threads = 0;

size_t	junk = 0;

template<int N>
static inline 
void BarrierSpin()
{
	#pragma omp atomic
	--spin[N];

	while ( (0 < spin[N]) && (spin[N] < num_threads) )
	{
		//sched_yield();	// debug
		// just do something silly while waiting other threads
		++junk;
	}

	spin[N] = num_threads;
}



const double	SOLAR_MASS	= (4 * M_PI * M_PI);
const double	DAYS_PER_YEAR	= 365.24;
const double	ADV_STEP	= 0.01;
const int	NBODIES		= 5;

__attribute__((aligned(ALG)))
typedef struct _tag_planet_
{
	double x, y, z;
	double vx, vy, vz;
	double mass;
} Planet;

Planet bodies[] = 
{
	{                               /* sun */
		0, 0, 0, 0, 0, 0, SOLAR_MASS
	},
	
	{                               /* jupiter */
		4.84143144246472090e+00,
		-1.16032004402742839e+00,
		-1.03622044471123109e-01,
		1.66007664274403694e-03 * DAYS_PER_YEAR,
		7.69901118419740425e-03 * DAYS_PER_YEAR,
		-6.90460016972063023e-05 * DAYS_PER_YEAR,
		9.54791938424326609e-04 * SOLAR_MASS
	},

	{                               /* saturn */
		8.34336671824457987e+00,
		4.12479856412430479e+00,
		-4.03523417114321381e-01,
		-2.76742510726862411e-03 * DAYS_PER_YEAR,
		4.99852801234917238e-03 * DAYS_PER_YEAR,
		2.30417297573763929e-05 * DAYS_PER_YEAR,
		2.85885980666130812e-04 * SOLAR_MASS
	},

	{                               /* uranus */
		1.28943695621391310e+01,
		-1.51111514016986312e+01,
		-2.23307578892655734e-01,
		2.96460137564761618e-03 * DAYS_PER_YEAR,
		2.37847173959480950e-03 * DAYS_PER_YEAR,
		-2.96589568540237556e-05 * DAYS_PER_YEAR,
		4.36624404335156298e-05 * SOLAR_MASS
	},

	{                               /* neptune */
		1.53796971148509165e+01,
		-2.59193146099879641e+01,
		1.79258772950371181e-01,
		2.68067772490389322e-03 * DAYS_PER_YEAR,
		1.62824170038242295e-03 * DAYS_PER_YEAR,
		-9.51592254519715870e-05 * DAYS_PER_YEAR,
		5.15138902046611451e-05 * SOLAR_MASS
	}
};


__attribute__((aligned(ALG)))
typedef struct _tag_inter_multi_
{
	double	dx;
	double	dy;
	double	dz;
} InteractionMultiplier;

// square matrix[N][N], contains interaction multiplier between 2 planets
InteractionMultiplier multiplier[NBODIES][NBODIES];


__attribute__((aligned(ALG)))
typedef struct _tag_delta_
{
	int		line;
	int		column;

	InteractionMultiplier*	upper_matrix;
	InteractionMultiplier*	lower_matrix;
} Delta;

/* this 'jobs' array labels the interactive-information of upper triangle matrix
So works can be divided easily between N threads. Without this, works is very unbalance

Plnt0	1	2	3	4
0	_	j0	j1	j2	j3
1	j0	_	j4	j5	j6
2	j1	j4	_	j7	j8
3	j2	j5	j7	_	j9
4	j3	j6	j8	j9	_

*/

const int jobs_count = ((NBODIES * (NBODIES -1)) /2); // (5*4/2) = 10
Delta jobs[jobs_count];


static void advance_multi_thread(int n)
{
	// labeling the interaction between planet X and planet Y
	int index = 0;
	for (int ln = 0; ln < (NBODIES -1); ln++)
	{
		for ( int cl = ln +1; cl < NBODIES; cl++)
		{
			jobs[index].line = ln;
			jobs[index].column = cl;
			
			// each job calculates a multiplier in the Delta matrix
			// so, just store pointer to its [upper + lower] location
			jobs[index].upper_matrix = &(multiplier[ln][cl]);
			jobs[index].lower_matrix = &(multiplier[cl][ln]);

			index++;
		}
	}
	
	spin[0] = spin[1] = num_threads;
	#pragma omp parallel default(shared) num_threads(num_threads)
	{
		for (int loop = 1; loop <= n; loop++)
		{
			// calculate the multiplier of 2 planets interaction
			#pragma omp for schedule(static) nowait
			for (int idx = 0; idx < jobs_count; idx++)
			{
				const Planet &pln1 = bodies[ jobs[idx].line ];
				const Planet &pln2 = bodies[ jobs[idx].column ];
			
				double dx = pln1.x - pln2.x;
				double dy = pln1.y - pln2.y;
				double dz = pln1.z - pln2.z;

				double distance2 = dx * dx + dy * dy + dz * dz;
				double distance = sqrt(distance2);

				double mag = ADV_STEP / (distance * distance2);

				dx *= mag;
				dy *= mag;
				dz *= mag;

				jobs[idx].lower_matrix->dx = dx;
				jobs[idx].lower_matrix->dy = dy;
				jobs[idx].lower_matrix->dz = dz;

				jobs[idx].upper_matrix->dx = -dx;
				jobs[idx].upper_matrix->dy = -dy;
				jobs[idx].upper_matrix->dz = -dz;
			}

			// must sync ~ 50M times -> omp_barrier() cost is too high
			BarrierSpin<0>();

			// Got interaction multiplier, update each planet velocity & coordinate
			#pragma omp for schedule(static) nowait
			for (int ln = 0; ln < NBODIES; ln++)
			{
				for ( int cl = 0; cl < NBODIES; cl++)
				{
					bodies[ln].vx += multiplier[ln][cl].dx * bodies[cl].mass;
					bodies[ln].vy += multiplier[ln][cl].dy * bodies[cl].mass;
					bodies[ln].vz += multiplier[ln][cl].dz * bodies[cl].mass;
				}

				bodies[ln].x += ADV_STEP * bodies[ln].vx;
				bodies[ln].y += ADV_STEP * bodies[ln].vy;
				bodies[ln].z += ADV_STEP * bodies[ln].vz;
			}

			BarrierSpin<1>();
		} // end advancing loop
	} // end parallel region
}

static 
double energy()
{
	double e = 0.0;

	for (int i = 0; i < NBODIES; i++) 
	{
		Planet *b = bodies + i;
		e += (0.5 * b->mass) * (b->vx * b->vx + b->vy * b->vy + b->vz * b->vz);

		for (int j = i + 1; j < NBODIES; j++)
		{
			Planet *b2 = bodies + j;

			double dx = b->x - b2->x;
			double dy = b->y - b2->y;
			double dz = b->z - b2->z;
			
			double distance = sqrt(dx * dx + dy * dy + dz * dz);
			
			e -= (b->mass * b2->mass) / distance;
		}
	}

	return e;
}

static 
void offset_momentum()
{
	double px = 0.0, py = 0.0, pz = 0.0;

	for (int i = 0; i < NBODIES; i++) 
	{
		Planet *b = bodies +i;

		px += b->vx * b->mass;
		py += b->vy * b->mass;
		pz += b->vz * b->mass;
	}

	bodies[0].vx = - px / SOLAR_MASS;
	bodies[0].vy = - py / SOLAR_MASS;
	bodies[0].vz = - pz / SOLAR_MASS;
}


// Old singlethread
void advance_single_thread(int N)
{
	for (int loop = 1; loop <= N; loop++)
	{
		// calculate multiplier then update velocity
		for (int i = 0; i < (NBODIES -1); i++) 
		{
			Planet *b = bodies + i;
			for (int j = i + 1; j < NBODIES; j++)
			{
				Planet *b2 = bodies + j;

				double dx = b->x - b2->x;
				double dy = b->y - b2->y;
				double dz = b->z - b2->z;

				double distance2 = dx * dx + dy * dy + dz * dz;
				double distance = sqrt(distance2);
				double mag = ADV_STEP / (distance * distance2);

				dx *= mag;
				dy *= mag;
				dz *= mag;

				b->vx -= dx * b2->mass;
				b->vy -= dy * b2->mass;
				b->vz -= dz * b2->mass;

				b2->vx += dx * b->mass;
				b2->vy += dy * b->mass;
				b2->vz += dz * b->mass;
			}
		}

		// update coordinate
		for (int i = 0; i < NBODIES; i++) 
		{
			Planet* b = bodies + i;

			b->x += ADV_STEP * b->vx;
			b->y += ADV_STEP * b->vy;
			b->z += ADV_STEP * b->vz;
		}
	}
}

int GetThread()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	for (int i = 0; i < 8; i++)
	{
		if (CPU_ISSET(i, &cs))
			count++;
	}
	return count;
}

int main(int argc, char ** argv)
{
	memset(jobs, 0, sizeof(jobs));
	memset(multiplier, 0, sizeof(multiplier));

	int n = (argc == 2)? atoi(argv[1]) : 1000;

	offset_momentum();
	printf ("%.9f\n", energy());

	num_threads = GetThread();
	if (num_threads > 1)
		advance_multi_thread(n);
	else
		advance_single_thread(n);

	printf ("%.9f\n", energy());

	return 0;
}

