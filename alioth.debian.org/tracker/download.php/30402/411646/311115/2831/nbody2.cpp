/*
* The Computer Language Benchmarks Game
* http://shootout.alioth.debian.org/
*
* C version contributed by Christoph Bauer
* Slightly improved by Mark Hinds
* OpenMP by The Anh Tran
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sched.h>

// "-fopenmp" flag
#include <omp.h>



// Reimplement a 'fast' barrier. Spin instead of mutex_lock
volatile int spin[2] = {0};
int num_threads = 0;

template<int N>
static inline void 
BarrierSpin()
{
	#pragma omp atomic
	--spin[N];

	while ( (0 < spin[N]) && (spin[N] < num_threads) )
	{
		if (N != 0)	// _DEBUG
			sched_yield();
	}

	spin[N] = num_threads;
}



const double	solar_mass	= (4 * M_PI * M_PI);
const double	days_per_year	= 365.24;

const double	adv_step		= 0.01;
const int		NBODIES		= 5;

__attribute__((aligned(16)))
typedef struct _tag_planet_
{
	double x, y, z;
	double vx, vy, vz;
	double mass;
} Planet;

Planet bodies[NBODIES] = 
{
	{                               /* sun */
		0, 0, 0, 0, 0, 0, solar_mass
	},
	
	{                               /* jupiter */
		4.84143144246472090e+00,
		-1.16032004402742839e+00,
		-1.03622044471123109e-01,
		1.66007664274403694e-03 * days_per_year,
		7.69901118419740425e-03 * days_per_year,
		-6.90460016972063023e-05 * days_per_year,
		9.54791938424326609e-04 * solar_mass
	},

	{                               /* saturn */
		8.34336671824457987e+00,
		4.12479856412430479e+00,
		-4.03523417114321381e-01,
		-2.76742510726862411e-03 * days_per_year,
		4.99852801234917238e-03 * days_per_year,
		2.30417297573763929e-05 * days_per_year,
		2.85885980666130812e-04 * solar_mass
	},

	{                               /* uranus */
		1.28943695621391310e+01,
		-1.51111514016986312e+01,
		-2.23307578892655734e-01,
		2.96460137564761618e-03 * days_per_year,
		2.37847173959480950e-03 * days_per_year,
		-2.96589568540237556e-05 * days_per_year,
		4.36624404335156298e-05 * solar_mass
	},

	{                               /* neptune */
		1.53796971148509165e+01,
		-2.59193146099879641e+01,
		1.79258772950371181e-01,
		2.68067772490389322e-03 * days_per_year,
		1.62824170038242295e-03 * days_per_year,
		-9.51592254519715870e-05 * days_per_year,
		5.15138902046611451e-05 * solar_mass
	}
};



__attribute__((aligned(16)))
typedef struct _tag_multi_
{
	double	dx;
	double	dy;
	double	dz;
} Multiplier;

// square matrix[N][N], contains interactive multiplier between 2 planets
Multiplier multiplier[NBODIES][NBODIES];



__attribute__((aligned(16)))
typedef struct _tag_job_
{
	Planet*		line;
	Planet*		column;
	Multiplier*	dt_upper;
	Multiplier*	dt_lower;
} Job;

/* this 'jobs' array labels the interactive-information of upper triangle of Multiplier matrix
So works can be divided easily between N threads. Without this, works is very unbalance

Pln	0	1	2	3	4
0	_	j0	j1	j2	j3
1	j0	_	j4	j5	j6
2	j1	j4	_	j7	j8
3	j2	j5	j7	_	j9
4	j3	j6	j8	j9	_

*/

const int jobs_count = ((NBODIES * (NBODIES-1)) /2);
Job jobs[jobs_count];


static void advance_multi_thread(int n)
{
	// labeling the interaction between planet X and planet Y
	int index = 0;
	for (int ln = 0; ln < (NBODIES -1); ln++)
	{
		for ( int cl = ln +1; cl < NBODIES; cl++)
		{
			jobs[index].line	= &(bodies[ ln ]);
			jobs[index].column	= &(bodies[ cl ]);
			
			// each job calculates a multiplier in the Delta matrix
			// so, just store pointer to its [upper + lower] location
			jobs[index].dt_upper = &(multiplier[ln][cl]);
			jobs[index].dt_lower = &(multiplier[cl][ln]);

			index++;
		}
	}

	
	#pragma omp parallel default(shared)
	{
		#pragma omp single
		spin[0] = spin[1] = num_threads = omp_get_num_threads();

		for (int loop = 1; loop <= n; loop++)
		{
			// calculate the multiplier of 2 planets interaction
			#pragma omp for schedule(static) nowait
			for (int idx = 0; idx < jobs_count; idx++)
			{
				const Planet *pln1 = jobs[idx].line;
				const Planet *pln2 = jobs[idx].column;
			
				double dx = pln1->x - pln2->x;
				double dy = pln1->y - pln2->y;
				double dz = pln1->z - pln2->z;

				double distance2 = dx * dx + dy * dy + dz * dz;
				double distance = sqrt(distance2);

				double mag = adv_step / (distance * distance2);

				dx *= mag;
				dy *= mag;
				dz *= mag;

				jobs[idx].dt_lower->dx = dx;
				jobs[idx].dt_lower->dy = dy;
				jobs[idx].dt_lower->dz = dz;

				*(jobs[idx].dt_upper) = *(jobs[idx].dt_lower);
			}

			// must sync ~ 50M times -> omp_barrier() cost is too high
			BarrierSpin<0>();

			// Got interaction multiplier, update each planet velocity & coordinate
			#pragma omp for schedule(static) nowait
			for (int ln = 0; ln < NBODIES; ln++)
			{
				for ( int cl = 0; cl < NBODIES; cl++)
				{
					if (ln < cl)
					{
						bodies[ln].vx -= multiplier[ln][cl].dx * bodies[cl].mass;
						bodies[ln].vy -= multiplier[ln][cl].dy * bodies[cl].mass;
						bodies[ln].vz -= multiplier[ln][cl].dz * bodies[cl].mass;
					}
					else
					{
						if (cl < ln)
						{
							bodies[ln].vx += multiplier[ln][cl].dx * bodies[cl].mass;
							bodies[ln].vy += multiplier[ln][cl].dy * bodies[cl].mass;
							bodies[ln].vz += multiplier[ln][cl].dz * bodies[cl].mass;
						}
					}
				}

				bodies[ln].x += adv_step * bodies[ln].vx;
				bodies[ln].y += adv_step * bodies[ln].vy;
				bodies[ln].z += adv_step * bodies[ln].vz;
			}

			// omp_barrier() cost is too high
			BarrierSpin<1>();
		} // end advancing loop
	} // end parallel region
}

static double 
energy()
{
	double e = 0.0;

	#pragma omp parallel for default(shared) schedule(static) reduction(+ : e)
	for (int i = 0; i < NBODIES; i++) 
	{
		Planet * b = &(bodies[i]);
		e += (0.5 * b->mass) * (b->vx * b->vx + b->vy * b->vy + b->vz * b->vz);

		for (int j = i + 1; j < NBODIES; j++) 
		{
			Planet * b2 = &(bodies[j]);
			
			double dx = b->x - b2->x;
			double dy = b->y - b2->y;
			double dz = b->z - b2->z;
			
			double distance = sqrt(dx * dx + dy * dy + dz * dz);
			
			e -= (b->mass * b2->mass) / distance;
		}
	}

	return e;
}

static void 
offset_momentum()
{
	double px = 0.0, py = 0.0, pz = 0.0;

	#pragma omp parallel for default(shared) schedule(static) reduction(+ : px, py, pz)
	for (int i = 0; i < NBODIES; i++) 
	{
		px += bodies[i].vx * bodies[i].mass;
		py += bodies[i].vy * bodies[i].mass;
		pz += bodies[i].vz * bodies[i].mass;
	}
	
	bodies[0].vx = - px / solar_mass;
	bodies[0].vy = - py / solar_mass;
	bodies[0].vz = - pz / solar_mass;
}

static void advance_single_thread(int N);
static bool IsMultiThread();

int main(int argc, char ** argv)
{
	int n = (argc == 2)? atoi(argv[1]) : 50000;

	offset_momentum();
	printf ("%.9f\n", energy());
	
	if (IsMultiThread())
		advance_multi_thread(n);
	else
		advance_single_thread(n);

	printf ("%.9f\n", energy());

	return 0;
}

// Copy & paste from C entry
void advance_single_thread(int N)
{
	for (int loop = 1; loop <= N; loop++)
	{
		for (int i = 0; i < (NBODIES -1); i++) 
		{
			Planet *b = &(bodies[i]);
		
			for (int j = i + 1; j < NBODIES; j++) 
			{
				Planet *b2 = &(bodies[j]);

				double dx = b->x - b2->x;
				double dy = b->y - b2->y;
				double dz = b->z - b2->z;

				double distance2 = dx * dx + dy * dy + dz * dz;
				double distance = sqrt(distance2);
				double mag = adv_step / (distance * distance2);

				dx *= mag;
				dy *= mag;
				dz *= mag;
			
				b->vx -= dx * b2->mass;
				b->vy -= dy * b2->mass;
				b->vz -= dz * b2->mass;
			
				b2->vx += dx * b->mass;
				b2->vy += dy * b->mass;
				b2->vz += dz * b->mass;
			}
		}

		for (int i = 0; i < NBODIES; i++) 
		{
			Planet *b = &(bodies[i]);
		
			b->x += adv_step * b->vx;
			b->y += adv_step * b->vy;
			b->z += adv_step * b->vz;
		}
	}
}

bool IsMultiThread()
{
	if (omp_get_num_procs() > 1)
	{
		cpu_set_t cs;
		CPU_ZERO(&cs);
		sched_getaffinity(0, sizeof(cs), &cs);

		int count = 0;
		for (int i = 0; i < 4; i++)
		{
			if (CPU_ISSET(i, &cs))
				count++;
		}
		return count > 1;
	}

	return false;
}

