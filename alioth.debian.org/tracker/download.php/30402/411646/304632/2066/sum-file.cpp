#include <iostream>

int main( )
{
    std::ios_base::sync_with_stdio(false);
    int n, s = 0;
    
    while (std::cin >> n) {
        s += n;
    }

    std::cout << s;
}
