-----------------------------------------------------------------------
-- The Computer Language Shootout
-- http://shootout.alioth.debian.org
-- contributed by Jim Rogers, modified by Gautier de Montmollin
-----------------------------------------------------------------------
with Ada.Numerics.Generic_Elementary_Functions;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Command_Line; use Ada.Command_Line;


procedure Partialsums is
   type Real is digits 12;
   package Real_Funcs is new Ada.Numerics.Generic_Elementary_Functions(Real);
   use Real_Funcs;
   package Real_Io is new Ada.Text_Io.Float_Io(Real);
   use Real_Io;

   Two_Thirds : constant Real := 2.0/3.0;

   N   : Integer;
   A1, A2, A3, A4, A5, A6, A7, A8, A9 : Real := 0.0;
   Alt : Real := -1.0;

begin

   N := Integer'Value(Argument(1));

   for K in 1..N loop
      declare
         RK : Real := Real(K);
         iK : Real := 1.0 / RK;
         SK : Real := Sin(RK);
         CK : Real := Cos(RK);
      begin
         Alt := -Alt;
         A1 := A1 + Two_Thirds**(K - 1);
         A2 := A2 + Sqrt(iK);
         A3 := A3 + iK / (RK + 1.0);
         A4 := A4 + (iK * iK * iK) / (SK**2);
         A5 := A5 + (iK * iK * iK) / (CK**2);
         A6 := A6 + iK;
         A7 := A7 + iK * iK;
         A8 := A8 + Alt * iK;
         A9 := A9 + Alt / (2.0 * RK - 1.0);
      end;
   end loop;

   Real_Io.Put(Item => A1, Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "(2/3)^k");

   Real_Io.Put(Item => A2, Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "k^-0.5");

   Real_Io.Put(Item => A3, Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "1/k(k+1)");

   Real_Io.Put(Item => A4, Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "Flint Hills");

   Real_Io.Put(Item => A5, Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "Cookson Hills");

   Real_Io.Put(Item => A6, Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "Harmonic");

   Real_Io.Put(Item => A7, Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "Riemann Zeta");

   Real_Io.Put(Item => A8, Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "Alternating Harmonic");

   Real_Io.Put(Item => A9, Fore => 1, Aft => 9, Exp => 0);
   Put_Line(Ascii.Ht & "Gregory");
end Partialsums;
