"""
The Computer Language Benchmarks Game
http://shootout.alioth.debian.org/
Contributed by Arnar Birgisson

This uses the Stackless Python variant, which uses
non-preemptive, cooperative microthreads (tasklets)
"""

import sys
import stackless

N = int(sys.argv[1])
THREADS = 503

def tsk(no, in_,out):
    while True:
        x = in_.receive()
        if x == 0:
            print no
            break
        out.send(x-1)

channels = [stackless.channel() for i in xrange(THREADS)]

for i in xrange(THREADS):
    stackless.tasklet(tsk)(i+1, channels[i], channels[(i+1) % THREADS])

channels[0].send(N)

stackless.run()