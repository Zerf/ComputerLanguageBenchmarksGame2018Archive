{ The Great Computer Language Shootout
  http://shootout.alioth.debian.org

  contributed by Ian Osgood
}

program spellcheck;
uses Classes;

var dict : TStringList;
    f : Text;
    word : string;
    i : longint;
begin
  dict := TStringList.Create;
  dict.Sorted := true;

  Assign(f,'Usr.Dict.Words');
  Reset(f);
  while not Eof(f) do begin
    readln(f,word);
    dict.Add(word);
  end;

  while not Eof do begin
    readln(word);
    if not dict.Find(word,i) then writeln(word);
  end;
end.
