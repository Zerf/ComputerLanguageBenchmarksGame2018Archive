#!/usr/bin/python
# http://shootout.alioth.debian.org/
#
# Contributed by Greg Buchholz

import sys, psyco

psyco.full()

def main():
    s = float(0)
    for i in range(int(sys.argv[1])):
        s += 1./(i+1.)
    print "%.9f" % s

main()
