/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Joshua Seagoe
*/

#include <algorithm>
#include <vector>
#include <cstdio> /* for printf */ 
#include <cstdlib> /* for atoi */ 

/* generate a series of increasing values starting with i + 1 */ 
template <typename T> struct IncrementGenF
{ T i; const T &operator()(void) { return ++i; } };

template <typename T>
unsigned fannkuch(const T &n)
{
  if (n < 2)
    return 0;

  std::vector<T> a(n), b(n);

  /* start with 1..N in nondecreasing order */ 
  IncrementGenF<T> gen = { 0 };
  std::generate(a.begin(), a.end(), gen);

  /* next_permutation goes in lexicographical order */ 
  /* skip everything that starts with a 1           */ 
  std::swap(a[0], a[1]);

  unsigned maximum = 0;
  do
  {
    /* the maximum will only occur when the highest  */ 
    /* value can be flipped to the front, which will */ 
    /* never happen if it starts out at the end      */ 
    if (a[n-1] == n)
      continue;

    std::copy(a.begin(), a.end(), b.begin());

    unsigned flips;
    for (flips = 0; b[0] != 1; ++flips)
      std::reverse(b.begin(), b.begin() + b[0]);

    if (flips > maximum)
      maximum = flips;

  } while (std::next_permutation(a.begin(), a.end()));

  return maximum;
}

int main(int argc, char **argv)
{
  const int n = argc > 1 ? atoi(argv[1]) : 9;
  printf("Pfannkuchen(%d) = %d\n", n, fannkuch(n));
}
