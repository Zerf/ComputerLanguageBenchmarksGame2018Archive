(* The Computer Language Shootout
   http://shootout.alioth.debian.org/
   contributed by Scott Cruzen
*)

val n = valOf (Int.fromString (hd (CommandLine.arguments())))

fun for (i, c, f) = if c i then (for (f i,c,f)) else ()

fun print_fmt xr s = 
   print(concat [Real.fmt (StringCvt.FIX (SOME 9)) (!xr), "\t", s,"\n"]);

val _ =
   let
      val a = ref ~1.0
      val sum = ref 0.0
   in
      for (0, fn k => k <= n,
         fn k => (sum := !sum + Real.Math.pow(2.0 / 3.0, Real.fromInt k) ; k + 1));
      print_fmt sum "(2/3)^k";

      sum := 0.0;
      for (1, fn k => k <= n,
         fn k => (sum := !sum + 1.0 / Real.Math.sqrt(Real.fromInt k) ; k + 1));
      print_fmt sum "k^-0.5";

      sum := 0.0;
      for (1, fn k => k <= n,
         fn k => (let val k = Real.fromInt k in
            sum := !sum + 1.0 / (k * (k+1.0)) end; k + 1));
      print_fmt sum "1/k(k+1)";

      sum := 0.0;
      for (1, fn k => k <= n, 
         fn k => (let val k = Real.fromInt k val sk = Real.Math.sin k in
            sum := !sum + 1.0 / (k * k * k * sk * sk) end; k+1));
      print_fmt sum "Flint Hills";

      sum := 0.0;
      for (1, fn k => k <= n, 
         fn k => (let val k = Real.fromInt k val sk = Real.Math.cos k in
            sum := !sum + 1.0 / (k * k * k * sk * sk) end; k+1));
      print_fmt sum "Cookson Hills";

      sum := 0.0;
      for (1, fn k => k <= n, fn k => (sum := !sum + 1.0 / Real.fromInt k; k+1));
      print_fmt sum "Harmonic";

      sum := 0.0;
      for (1, fn k => k <= n, fn k => (let val k = Real.fromInt k in
         sum := !sum + 1.0 / (k * k) end; k+1));
      print_fmt sum "Riemann Zeta";

      sum := 0.0;
      for (1, fn k => k <= n, fn k => (a := ~ (!a); sum := !sum + !a / Real.fromInt k; k+1));
      print_fmt sum "Alternating Harmonic";

      sum := 0.0;
      a := ~1.0;
      for (1, fn k => k <= n, fn k => (a := ~ (!a); sum := !sum + !a / (2.0 * Real.fromInt k - 1.0); k+1));
      print_fmt sum "Gregory"
   end
