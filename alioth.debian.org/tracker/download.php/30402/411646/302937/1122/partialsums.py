# The Computer Language Shootout
# http://shootout.alioth.debian.org/

# contributed by Josh Goldfoot
# loosely based on the Haskell implementation by Chris Kuklewicz

from math import sin, cos
from sys import argv

def main(n):
    twoThirds = 2.0 / 3.0
    print "%.9f\t%s" % (sum(map(lambda k: twoThirds ** k, xrange(n))),"(2/3)^k")
    for (name, fun) in [("k^-0.5",lambda k: k ** -0.5), ("1/k(k+1)",lambda k: 1.0/(k*(k+1.0))),
                        ("Flint Hills",lambda k: 1.0/(k ** 3.0 * sin(k) ** 2.0)),
                        ("Cookson Hills",lambda k: 1.0/(k ** 3.0 * cos(k) ** 2.0)),
                        ("Harmonic",lambda k: 1.0/k),("Riemann Zeta",lambda k: 1.0/k ** 2.0)]:
        print "%.9f\t%s" % (sum(map(fun, xrange(1,n+1))), name)
    for (name, fun) in [("Alternating Harmonic",lambda k: 1.0/k),("Gregory",lambda k: 1.0/(2.0*k -1.0))]:
        print "%.9f\t%s" % (sum(map(fun, xrange(1,n+1,2))) - sum(map(fun, xrange(2,n+1,2))), name)

main((len(argv) > 1) and int(argv[1]) or 25000)
