// The Computer Language Shootout
// http://shootout.alioth.debian.org/
// Precedent C entry modified by bearophile for speed and size
// Compile with:  -O3 -s -std=c99 -fomit-frame-pointer

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char boolean;


static void nsieve(int m) {
    unsigned int count = 0, i, j;
    boolean * a = (boolean *) malloc(m * sizeof(boolean));
    memset(a, 1, m);

    for (i = 2; i < m; ++i)
        if (a[i]) {
            ++count;
            for (j = i << 1; j < m; j += i)
                if (a[j]) a[j] = 0;
    }

    free(a);
    printf ("Primes up to %8u %8u\n", m, count);
}

int main(int argc, char * argv[]) {
    int m = atoi(argv[1]);
    for (int i = 0; i < 3; i++)
        nsieve(10000 << (m-i));
    return 0;
}
