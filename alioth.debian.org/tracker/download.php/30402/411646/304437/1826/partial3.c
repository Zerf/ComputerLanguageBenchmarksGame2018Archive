/*
   The Computer Language Shootout
   http://shootout.alioth.debian.org/
   Contributed by David Smith 
	Removed a cos() and pow() and a bunch of divides. 

   Compile with:
      gcc -O3 -fomit-frame-pointer -ffast-math -o partial3 partial3.c * -lm
*/



#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define dk ((double)k)

int
main (int argc, char *argv[])
{
	unsigned long int k, n = atoi(argv[1]);
   double a1 = 0, a2 = 0, a3 = 0, a4 = 0, a5 = 0, a6 = 0, a7 = 0, 
          a8 = 0, a9 = 0, q = 1, alt = -1;
   double ki, ki3, sk2;

   for (k=0; k<n; ++k) {
      a1 += q;
      q *= 2. / 3.;
	}
   for (k=1; k<=n; ++k) {
      alt = -alt;
      ki = 1.0 / dk;
      a2 += sqrt(ki);
      a3 += ki / (1.0 + dk);
      a6 += ki;
      a8 += ki * alt;
      a7 += ki * ki;
      a9 += alt / (2.0 * dk - 1.0);
	}
   for (k=1; k<=n; ++k) {
      sk2 = pow(sin(dk),2);
      ki3 = pow(dk,-3);
      a4 += ki3 / sk2;
      a5 += ki3 / (1.0 - sk2);
   }

   printf("%.9lf\t(2/3)^k\n", a1);
   printf("%.9lf\tk^-0.5\n", a2);
	printf("%.9lf\t1/k(k+1)\n", a3);
   printf("%.9lf\tFlint Hills\n", a4);
   printf("%.9lf\tCookson Hills\n", a5);
   printf("%.9lf\tHarmonic\n", a6);
   printf("%.9lf\tRiemann Zeta\n", a7);
   printf("%.9lf\talternating Harmonic\n", a8);
   printf("%.9lf\tGregory\n", a9);

	return 0;
}

