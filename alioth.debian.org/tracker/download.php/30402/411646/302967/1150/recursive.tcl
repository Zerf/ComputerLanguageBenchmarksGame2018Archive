# The Computer Language Shootout
# http://shootout.alioth.debian.org/
# contributed by Andrew McParland, based on code from the Doug Bagley benchmarks

interp recursionlimit {} 20000

# Note reversal of arguments
proc ack {n m} {
    if {$m} {
        if {$n} {
            return [ack [ack [incr n -1] $m] [incr m -1]]
        } else {
            return [ack 1 [incr m -1]]
        }
    } else {
        return [incr n]
    }
}

proc fib {n} {
    if {$n < 2} {
        return 1
    } else {
        return [expr { [fib [expr {$n-2}]] + [fib [expr {$n-1}]] } ]
    }
}

proc tak {x y z} {
    if {$y >= $x} {
        return $z
    } else {
        return [tak [tak [expr {$x-1}] $y $z] [tak [expr {$y-1}] $z $x] [tak [expr {$z-1}] $x $y]]
    }
}

set n [expr [lindex $argv 0] - 1]

puts [format "Ack(3,%d): %d" [expr $n+1] [ack [expr $n+1] 3]]
puts [format "Fib(%.1f): %.1f" [expr $n+28.0] [fib [expr $n+28.0]]]
set n3 [expr $n*3]; set n2 [expr $n*2]
puts [format "Tak(%d,%d,%d): %d" $n3 $n2 $n [tak $n3 $n2 $n]]
puts [format "Fib(3): %d" [fib 3]]
puts [format "Tak(3.0,2.0,1.0): %.1f" [tak 3.0 2.0 1.0]]
