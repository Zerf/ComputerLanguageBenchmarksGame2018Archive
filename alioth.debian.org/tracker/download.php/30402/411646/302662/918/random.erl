-module(random).
-export([main/1]).

main([Arg]) ->
   N = list_to_integer(Arg),
   io:fwrite("~.9f\n", [rand(N, 42, 0.0, 100.0)]),
   halt(0).

-define(IM, 139968).
-define(IA, 3877).
-define(IC, 29573).
-define(IMF,float(?IM)).

rand(0, _, Rand, _) -> Rand;
rand(N, Seed, _Rand, Max) when is_float(Max) ->
   NewSeed = (Seed * ?IA + ?IC) rem ?IM,
   NewRand = Max * NewSeed / ?IMF,
   rand(N-1, NewSeed, NewRand, Max).

