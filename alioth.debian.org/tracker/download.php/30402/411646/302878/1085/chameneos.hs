{-# OPTIONS_GHC -O2 -funbox-strict-fields #-}
{- The Computer Language Shootout
   
   contributed by Aaron Denney
   modified by Chris Kuklewicz, 11 Jan 2006

   compile with "ghc --make -O2 -funbox-strict-fields chameneos.hs -o chameneos.ghc_run"
   run with "./chameneos.ghc_run %A" where %A is the number of meetings

   This is a symmetric solution that does not use a manager thread.

   http://shootout.alioth.debian.org/
   http://shootout.alioth.debian.org/benchmark.php?test=chameneos&lang=all
-}
import Control.Concurrent
import Control.Monad
import System (getArgs)

data Color = C !Int deriving (Eq)
red = C 0; yellow = C 1; blue = C 2; faded = C 3

complement :: Color -> Color -> Color
complement (C a) (C b) | a == b    = C a
                       | otherwise = C (3 - a - b)

data Meeting = M !(MVar Int) !(MVar (Color, MVar Color))

new_meeting maxMeetings = liftM2 M (newMVar maxMeetings) newEmptyMVar

wait_other (M meets waiting) color wake_up = do
  remainingMeets <- takeMVar meets -- used as lock
  let sleep_on = do putMVar waiting (color, wake_up)
                    putMVar meets remainingMeets
                    takeMVar wake_up
      wake_waiter (other_c,other_wake_up) = do putMVar other_wake_up color
                                               putMVar meets (remainingMeets - 1)
                                               return other_c
  case remainingMeets of
    0 -> putMVar meets 0 >> return faded
    _ -> tryTakeMVar waiting >>= maybe sleep_on wake_waiter

spawnCreature meeting_place startingColor = do
  metVar <- newEmptyMVar
  wake_up  <- newEmptyMVar
  let creature = putMVar metVar =<< inner_creature startingColor (0::Int)
        where inner_creature color have_met = do
                color `seq` have_met `seq` return ()
                other <- wait_other meeting_place color wake_up
                if other == faded
                  then return have_met
                  else inner_creature (complement color other) (have_met + 1)
  forkIO $ creature -- One thread per creature
  return metVar

main = do 
  args <- getArgs
  let meetings = if null args then (1000000::Int) else (read . head) args
  meeting_place <- new_meeting meetings
  metVars <- mapM (spawnCreature meeting_place) [blue, red, yellow, blue]
  vals <- mapM takeMVar metVars -- Main thread waits for completion
  print $ sum vals
