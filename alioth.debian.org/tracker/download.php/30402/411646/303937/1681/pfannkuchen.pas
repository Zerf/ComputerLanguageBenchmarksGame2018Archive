(* The Computer Language Shootout
   http://shootout.alioth.debian.org/
   
   contributed by Florian Klaempfl
   modified by Micha Nelissen
   modified by Vincent Snijders *)

program pfannkuchen;

type
    TIntegerArray = Array[0..99] of longint;
    
var
   perm, perm1, count: TIntegerArray;
   r, n, answer : longint;

procedure swap(var a, b: longint); inline;
var
  tmp: longint;
begin
  tmp := a;
  a := b;
  b := tmp;
end;

function NextPermutation: boolean;
var
  perm0: longint;
  i, r0: longint;
begin
  r0 := r; // use local variable
  NextPermutation := true;
  while true do begin
     if r0 = n then
     begin
        NextPermutation := false;
        break;
     end;
     perm0 := perm1[0];
     for i := 1 to r0 do
        perm1[i-1] := perm1[i];
     perm1[r0] := perm0;
     dec(count[r0]);
     if count[r0] > 0 then
       break;
     inc(r0);
  end;
  r := r0;
end;

function fannkuch: longint;
var
   print30, m, i, flips: longint;
   pi, pj : PLongint;
begin
   print30 := 0;
   fannkuch := 0;
   m := n - 1;
   for i := 0 to m do
       perm1[i] := i;
   r := n;
   repeat
      if print30 < 30 then
      begin
         for i := 0 to m do
            write(perm1[i] + 1);
         writeln;
         inc(print30);
      end;
      while r <> 1 do
      begin
         count[r-1] := r;
         dec(r);
      end;
      if not ((perm1[0]=0) or (perm1[m]=m)) then
      begin
         move(perm1[0], perm[0], sizeof(longint)*n);
         flips := 0;
         repeat
           pi := @perm[0];
           pj := @perm[perm[0]];
           while pi<pj do
             begin
               swap(pi^,pj^);
               inc(pi);
               dec(pj);
             end;
           inc(flips);
         until perm[0] = 0;
         if flips > fannkuch then
            fannkuch := flips;
      end;
   until not NextPermutation;
end;

begin
   if paramCount() = 1 then
     Val(ParamStr(1), n)
   else
     n := 7;
   answer := fannkuch;
   writeln('Pfannkuchen(',n,') = ', answer);
end.

