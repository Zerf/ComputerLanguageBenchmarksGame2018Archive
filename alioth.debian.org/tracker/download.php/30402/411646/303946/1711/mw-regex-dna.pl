# The Computer Language Shootout
# http://shootout.alioth.debian.org/
# contributed by Danny Sauer
# completely rewritten and
# cleaned up for speed and fun by
# Mirco Wahab (wahab@chemie.uni-halle.de)
use strict;
use warnings;                       ###########################################
use re 'eval';                      # FWIW, I posted the code because it is   #
                                    # fairly small and fast enough to give    #
my $content = do{ local$/;<STDIN> };# a fair picture of Perls RE capabilities.#
my $l_file  = length $content;      # The only magic found below is the count #
my $dispose = qr/^>.*$|\n/m;        # of the /motifs/ by 'code assertion',    #
$content   =~ s/$dispose//g;        # (which comes handy here). Now there is  #
my $l_code = length $content;       # very vew real code left (beat this) :-) #
                                    ###########################################
my (@seq, $regex, %h) = qw(
    agggtaaa|tttaccct      [cgt]gggtaaa|tttaccc[acg]  a[act]ggtaaa|tttacc[agt]t
ag[act]gtaaa|tttac[agt]ct  agg[act]taaa|ttta[agt]cct  aggg[acg]aaa|ttt[cgt]ccct
agggt[cgt]aa|tt[acg]accct  agggta[cgt]a|t[acg]taccct  agggtaa[cgt]|[acg]ttaccct);
chop ($regex = join '', map{ $h{$_}=0; "(?:($_)(?{++\$h{\'$_\'}}))|" } @seq);

1 while $content =~ /$regex/ogi;
printf "$_ $h{$_}\n" for @seq;

my %iubcode = (  B => '(c|g|t)',  D => '(a|g|t)'  ,  H => '(a|c|t)',
  K => '(g|t)',  M => '(a|c)'  ,  N => '(a|c|g|t)',  R => '(a|g)'  ,
  S => '(c|g)',  V => '(a|c|g)',  W => '(a|t)'    ,  Y => '(c|t)'   );
my $findiub = '(['.(join '',keys %iubcode).'])';

1 while $content =~ s/$findiub/$iubcode{$1}/og;
printf "\n%d\n%d\n%d\n", $l_file, $l_code, length($content);

