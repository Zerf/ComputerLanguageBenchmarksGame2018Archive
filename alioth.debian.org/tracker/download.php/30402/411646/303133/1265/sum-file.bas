rem The Computer Language Shootout
rem http://shootout.alioth.debian.org/
rem contributed by Josh Goldfoot

file_handle = freefile()
open cons as file_handle

total = 0
loop1:
   input #file_handle, a$
   if a$ <> "" then
      total = total + valint(a$)
      goto loop1
   end if
   
print str(total)

