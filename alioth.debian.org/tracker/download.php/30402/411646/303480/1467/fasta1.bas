
'The Computer Language Shootout
'http://shootout.alioth.debian.org/
'contributed by Antoni Gual  5/2006

option explicit
type aminoacids
   c as integer
   p as double
end type

const IUB_LEN = 15
dim iub(14) as aminoacids={(asc("a"),0.27),_
                           (asc("c"),0.12),_
                           (asc("g"),0.12),_
                           (asc("t"),0.27),_
                           (asc("B"),0.02),_
                           (asc("D"),0.02),_
                           (asc("H"),0.02),_
                           (asc("K"),0.02),_
                           (asc("M"),0.02),_
                           (asc("N"),0.02),_
                           (asc("R"),0.02),_
                           (asc("S"),0.02),_
                           (asc("V"),0.02),_
                           (asc("W"),0.02),_
                           (asc("Y"),0.02)}

const HOMOSAPIENS_LEN = 4
dim homosapiens(3) as aminoacids={(asc("a"),0.3029549426680),_
                                  (asc("c"),0.1979883004921),_
                                  (asc("g"),0.1975473066391),_
                                  (asc("t"),0.3015094502008)}

dim alu as zstring *300 => "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG"_
                           "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA"_
                           "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT"_
                           "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA"_
                           "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG"_
                           "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC"_
                           "AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA"

#define linelength 60

function gen_random(max as double) as double
   const IM = 139968
   const IA =  3877
   const IC =  29573
   const iim=  1/im
   static last as long = 42
   last = (last * IA + IC) mod IM
   return max * last *iIM
end function

sub makeCumulative(genelist() as aminoacids, count as integer)
   dim cp as double,i
   for i = 0 to count-1
      cp += genelist(i).p
      genelist(i).p = cp
   next i
end sub

function selectRandom(genelist() as aminoacids, count as integer) 
   dim r as double,lo,hi,i
   r = gen_random(1)
   if r < genelist(0).p then return genelist(0).c
   lo = 0
   hi = count - 1
   while hi > lo + 1
      i = (hi + lo) \ 2
      if r < genelist(i).p then hi = i else lo = i
   wend
   return genelist(hi).c
end function

sub makeRandomFasta(id as string, desc as string, genelist() as aminoacids, count as integer, n as long)

   dim i,todo,m,tline as zstring * linelength+1
   print ">";id;" ";desc
   for todo =n to 1 step -linelength
     m=iif(todo<linelength,todo,linelength)
     for i = 0 to m-1: tline[i]=selectRandom(genelist(), count):next
     tline[i]=0   
     print tline
   next
end sub

sub makeRepeatFasta(id as string, desc as string, s as zstring ptr, n as integer)
  dim sp,lst,i,ll
  lst=len(*s)
  sp=1
  print ">";id;" ";desc
  for i=n to 1 step -linelength
     ll=iif(i<linelength,i,linelength)  
     if sp<lst-ll then
       print mid(*s,sp,ll)
       sp+=ll
     else     
       print mid(*s,sp);
       sp=ll-(lst-sp) 
       print left(*s,sp-1)
     end if
   next    
end sub

dim n
n = val(command$)
if n < 1 then n = 1000
makeCumulative(iub(), IUB_LEN)
makeCumulative(homosapiens(), HOMOSAPIENS_LEN)
makeRepeatFasta("ONE", "Homo sapiens alu", @alu, n*2)
makeRandomFasta("TWO", "IUB ambiguity codes", iub(), IUB_LEN, n*3)
makeRandomFasta("THREE", "Homo sapiens frequency", homosapiens(), HOMOSAPIENS_LEN, n*5)
