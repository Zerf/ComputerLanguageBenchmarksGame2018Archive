{-# OPTIONS -fglasgow-exts -fbang-patterns #-}
--
-- The Computer Language Shootout
-- http://shootout.alioth.debian.org/
--
-- Contributed by Vasile Gaburici
--
-- Real line-oriented I/O
--

import Data.List
import qualified Data.ByteString.Char8 as C8
import System.IO.Error as Err

maybeGetLine :: IO (Maybe C8.ByteString)
maybeGetLine =  Err.catch (do line <- C8.getLine
                              return $ Just line) (\e -> return Nothing)

addLine :: Int -> IO ()
addLine the_sum = do maybe_line <- maybeGetLine
                     case maybe_line of
                       Nothing   -> putStrLn $ show the_sum
                       Just line -> do
                         let Just(num, _) = C8.readInt line
                         addLine $! num + the_sum

main = addLine 0
