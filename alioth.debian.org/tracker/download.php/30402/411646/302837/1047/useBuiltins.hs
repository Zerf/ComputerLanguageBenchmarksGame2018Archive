{-  The Computer Language Shootout
    http://shootout.alioth.debian.org/
    compile with:  ghc -O2 -o fannkuch fannkuch.hs
    contributed by Josh Goldfoot, "fixing" the version by Greg Buchholz
      permutations function translated from the C version by Heiner Marxen 
    Joel Koerwer changed the code to use the builtin function, maximum, 
      greatly improving performance. Also cleaned up a bit.
-}

import System (getArgs)

main = do   [n] <- getArgs
            let p = permutations [1 .. read n]
            mapM putStrLn $ map (>>= show) $ take 30 p
            putStr $ "Pfannkuchen(" ++ n ++ ") = "
            print $ findmax p

findmax :: [[Int]] -> Int
findmax = maximum . map (flop 0)

flop :: Int -> [Int] -> Int
flop acc (1:xs) = acc
flop acc list@(x:xs) = flop (acc+1) mangle
    where   mangle = (reverse front) ++ back
            (front,back) = splitAt x list

permutations :: [Int] -> [[Int]]
permutations arry =
    arry : (permuteloop n arry [1..n] 1)
    where n = length arry

permuteloop :: Int -> [a] -> [Int] -> Int -> [[a]]
permuteloop n arry count r
        | r == n = []
        | count' !! r > 0 = arry' : (permuteloop n arry' (reload r count') 1)
        | otherwise = permuteloop n arry' count' (r+1)
        where 
            count' = (take r count) ++ ((count !! r) - 1):(drop (r+1) count)
            arry' = rotate r arry


rotate :: Int -> [a] -> [a]
rotate r (x:xs) =
   begin ++ x:end
   where (begin, end) = splitAt r xs

reload :: Int -> [Int] -> [Int]
reload r count
   | r == 1 = count
   | otherwise = [1..r] ++ (drop r count)
