;; The Great Computer Language Shootout
;; Harmonic benchmark
;;
;; Implemented by Will M. Farr 

(module harmonic
   (main main))

(define (int->double::double i::int)
   (pragma::double "((double) ($1))" i))

(define (sum-harmonic::double n::int)
   (do ((i 1 (+fx i 1))
	(sum 0.0 (+fl sum (/fl 1.0 (int->double i)))))
       ((>fx i n) sum)))

(define (main argv)
   (let ((n (string->number (cadr argv))))
      (display (sum-harmonic n))
      (newline)))