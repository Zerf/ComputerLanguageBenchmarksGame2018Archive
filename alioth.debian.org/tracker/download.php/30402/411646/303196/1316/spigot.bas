' The Computer Language Shootout
' http://shootout.alioth.debian.org/
' contributed by Simon Nash (yetifoot)

' A spigot algorithm for the Digits of \pi, Stanley Rabinowitz
' and Stan Wagon, Amer.Math.Monthly, March 1995, 195-203

Sub PrintNumber(v As Integer)
  Static c As Integer
    If c <> 0 Then 
      Print str(v);
      If c mod 10 = 0 Then Print chr(9) & ":" & c & chr(10);
    End If
    c += 1
End Sub

Dim As Integer i, j, k, q, x, nines, predigit, N, _LEN, r

  N = CInt(Command$)
  _LEN = 10 * N \ 3
  
  Dim As Long a(_LEN)
  
  For j = 0 To _LEN - 1
    a(j) = 2
  Next j
  nines = 0
  predigit = 0
  For j = 0 To N - 1
    q = 0
    For i = _LEN To 1 Step -1
      x = 10 * a(i - 1) + q * i
      a(i - 1) = x mod (2 * i - 1)
      q = x \ (2 * i - 1)
    Next i
    a(0) = q mod 10
    q = q \ 10
    If q = 9 Then
      nines += 1 
    ElseIf q = 10 Then
      PrintNumber(predigit + 1)
      predigit = 0
      While nines <> 0
        PrintNumber(0)
        nines -= 1
      Wend
    Else
      PrintNumber(predigit)
      predigit = q
      While nines <> 0
        PrintNumber(9)
        nines -= 1
      Wend
    End If
  Next j
  PrintNumber(predigit)
  r = N mod 10
  If r <> 0 Then Print Space(10 - r) & chr(9) & ":" & N & chr(10);
