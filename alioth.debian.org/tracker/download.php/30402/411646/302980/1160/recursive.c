/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Lester Vecsey */

#include <stdio.h>
#include <stdlib.h>

int Ack(int x, int y) { 

	return !x ? y + 1 : (!y ? Ack(x-1,1) : Ack(x-1, Ack(x,y-1)));

	}

#define Fib_core(f,n) (n<2 ? 1 : f(n-2) + f(n-1))
#define Tak_core(f,x,y,z) (y<x ? f(f(x-1.0,y,z), f(y-1.0,z,x), f(z-1.0,x,y)) : z)

int Fib(int n) { return Fib_core(Fib,n); }
double Fib_double(double n) { return Fib_core(Fib_double,n); }

int Tak(int x, int y, int z) { return Tak_core(Tak,x,y,z); }
double Tak_double(double x, double y, double z) { return Tak_core(Tak_double,x,y,z); }

int main(int argc, char *argv[]) {

	int n = argc>1 ? strtol(argv[1],NULL,10) : 3;

	printf("Ack(%d,%d): %d\nFib(%.1f): %.1f\nTak(%d,%d,%d): %d\nFib(%d): %d\nTak:(%.1f,%.1f,%.1f): %.1f\n",
		n, n, Ack(n,n), (float) n*10, Fib_double(n*10), n*2, n+1, n-1, Tak(n*2,n+1,n-1), n, Fib(n), 
		(float)n, (float)n-1, (float)n-2, Tak_double(n,n-1,n-2));

	return 0;

	}

