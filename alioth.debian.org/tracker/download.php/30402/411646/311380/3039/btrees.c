/*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Diogo Sousa (aka orium)
 */

/* Compile with: gcc -Wall -pipe -O3 -lpthread -fomit-frame-pointer	\
 * -march=native -fopenmp btrees.c -o btrees
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sched.h>
#include <omp.h>

#define max(x,y) (((x) > (y)) ? (x) : (y))
#define min(x,y) (((x) < (y)) ? (x) : (y))

struct computed
{
	int trees;
	int depth;
	int check;
};

struct tree_node
{
	int item;
	
	struct tree_node *left;	
	struct tree_node *right;
};

struct args
{
	int max_depth;
	struct tree_node **long_lived_tree;
	struct computed *computed;
	int stretch_depth;
};

inline struct tree_node *
make_tree_node(int item, struct tree_node *left, struct tree_node *right)
{
	struct tree_node *node;
	
	node=malloc(sizeof(*node));
	
	node->item=item;
	node->left=left;
	node->right=right;
	
	return node;
}

struct tree_node *
make_tree(int item, int depth)
{
	if (!depth)
		return make_tree_node(item,NULL,NULL);
	else
		return make_tree_node(item,
				      make_tree((item<<1)-1,depth-1),
				      make_tree(item<<1,depth-1));
}

int
check_tree_and_free(struct tree_node *node)
{
	if (node->left == NULL)
	{
		int r=node->item;
		free(node);
		return r;
	}
	else
	{	
		struct tree_node my_node=*node;
		free(node);
		return my_node.item+check_tree_and_free(my_node.left)
			-check_tree_and_free(my_node.right);
	}
}

/* Shootout bench uses affinity to emulate single core processor.
 * This function search for appropriate number of threads to spawn
 *
 * Took from spectral-norm, c++ implementation #7
 */
int
get_thread_count()
{
        cpu_set_t cs;
        int count=0;
        int i;
	
        CPU_ZERO(&cs);
        sched_getaffinity(0,sizeof(cs),&cs);
	
        for (i = 0; i < 8; i++)
                if (CPU_ISSET(i,&cs))
                        count++;
	
        return count;
}

void
build_long_lived_tree(struct args *args)
{
	*args->long_lived_tree=make_tree(0,args->max_depth);	
}

void
compute_stretch_tree(struct args *args)
{
	args->computed[0].trees=-1;
	args->computed[0].depth=args->stretch_depth;
	args->computed[0].check
		=check_tree_and_free(make_tree(0,args->stretch_depth));
}

int
computed_trees_checks(int depth, int end)
{
	int i;
	int check=0;
	
	for (i=1; i <= end; i++)
		check+=check_tree_and_free(make_tree(i,depth))
			+check_tree_and_free(make_tree(-i,depth));
	
	return check;
}

void
print_computed_data(struct computed *computed, size_t size)
{
	int i;
	
	printf("stretch tree of depth %d\t check: %d\n",computed[0].depth,
	       computed[0].check);
	
	for (i=1; i < size; i++)
	{
		printf("%d\t trees of depth %d\t check: %d\n",
		       computed[i].trees,computed[i].depth,computed[i].check);
	}
}

int
main(int argc, char **argv)
{
	struct tree_node *long_lived_tree;
	int min_depth=4;
	int max_depth=max(min_depth+1, (argc >= 2) ? atoi(argv[1]) : 20);
	int stretch_depth=max_depth+1;
	int i;
	struct computed computed[(stretch_depth-min_depth)/2
				 +(stretch_depth-min_depth)%2+1];
	struct args args;
	
	args.max_depth=max_depth;
	args.long_lived_tree=&long_lived_tree;
	args.computed=computed;
	args.stretch_depth=stretch_depth;
	
	build_long_lived_tree(&args);
	compute_stretch_tree(&args);

#pragma omp parallel for default(shared) num_threads(get_thread_count()) schedule(dynamic,1) private(i)
	for (i=min_depth; i < stretch_depth; i+=2)
	{
		int its=1<<(max_depth-(i-min_depth));
		int c=((i-min_depth)>>1)+1;

		computed[c].check=computed_trees_checks(i,its);
		computed[c].trees=its<<1;
		computed[c].depth=i;
	}
	
	print_computed_data(computed,sizeof(computed)/sizeof(*computed));
	printf("long lived tree of depth %d\t check: %d\n",max_depth,
	       check_tree_and_free(long_lived_tree));
	
	return 0;
}
