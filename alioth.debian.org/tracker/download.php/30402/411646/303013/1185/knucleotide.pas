(* The Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Josh Goldfoot
*)

program knucleotide;

(* simple_hash available from CVS *)
uses simple_hash, SysUtils, Strings;

type
   sorter      = record
		    sequence : PChar;
		    num	     : longint;
		 end;	     
   sorterArray = array of sorter;

function hash_table_size (fl : dword; buflen : dword): dword;
var
   maxsize1, maxsize2, r : dword;
begin
   maxsize1 := buflen - fl;
   maxsize2 := 4;
   while (fl > 1) and (maxsize2 < maxsize1) do
   begin
      fl := fl - 1;
      maxsize2 := maxsize2 * 4;
   end;
   if maxsize1 < maxsize2 then
      r := maxsize1
   else
      r := maxsize2;
   hash_table_size := r;
end; { hash_table_size }

function generate_frequencies(fl: integer; buffer: PChar; buflen : longint): ht_pht;
var
   ht	  : ht_pht;
   reader : PChar;
   i	  : longint;
   nulled : char;
   found  : ht_pnode;
begin
   if fl <= buflen then
   begin
      ht := ht_create (hash_table_size (fl, buflen));
      for i := 0 to buflen - fl do
      begin
         reader := @buffer[i];
         nulled := reader[fl];
         reader[fl] := #0;
         found := ht_find_new (ht, reader);
         reader[fl] := nulled;
         found^.val := found^.val + 1;
      end;
      generate_frequencies := ht;
   end else 
      generate_frequencies := nil;
end; { generate_frequencies }

procedure sortArray(var s : sorterArray; size:longint);
var
   i,j : longint;
   tmp : sorter;
begin
   for i := 0 to size-2 do
      for j := i+1 to size-1 do
         if s[i].num < s[j].num then
	 begin
	    tmp.num := s[i].num;
	    tmp.sequence := s[i].sequence;
	    s[i].num := s[j].num;
	    s[i].sequence := s[j].sequence;
	    s[j].num := tmp.num;
	    s[j].sequence := tmp.sequence;
	 end;
end; { sortArray }

procedure write_frequencies(fl : integer; buffer : PChar; buflen : longint);
var
   ht	   : ht_pht;
   i, size : longint;
   total   : real;
   nd	   : ht_pnode;
   s	   : sorterArray;
begin
   ht := generate_frequencies(fl, buffer, buflen);
   total := 0;
   size := 0;
   nd := ht_first(ht);
   while (nd <> nil) do
   begin
      total := total + nd^.val;
      size := size + 1;
      nd := ht_next(ht);
   end;
   SetLength(s, size);

   nd := ht_first(ht);
   size := 0;
   while (nd <> nil) do
   begin
      s[size].sequence := nd^.key;
      strupper(s[size].sequence);
      s[size].num := nd^.val;
      size := size + 1;
      nd := ht_next(ht);
   end;

   sortArray(s, size);
   for i := 0 to size - 1 do
      writeln(s[i].sequence,' ', (100 * s[i].num / total):3:3);
    writeln;
   (* ht_destroy(ht); *)
end; { write_frequencies }

procedure write_count(searchFor : PChar; buffer : PChar; buflen : longint);
var
   ht : ht_pht;
   nd : ht_pnode;
begin
   ht := generate_frequencies (strlen (searchFor), buffer, buflen);
   nd := ht_find(ht, searchFor);
   if (nd <> nil) then
      write(nd^.val)
   else
      write(0);
   strupper(searchFor);
   writeln(#9, searchFor);
   (*ht_destroy(ht);*)
end; { write_count }

procedure main;
var
   c		  : char;
   buffer, x	  : PChar;
   buflen, seqlen : longint;
   line		  : String;
begin
   seqlen := 0;
   repeat
      readln(line)
   until (line[1] = '>') and (line[2] = 'T') and (line[3] = 'H');
   buflen := 13000;
   buffer := stralloc(buflen + 1);
   x := buffer;
   repeat
      readln(x);
      c := x[0];
      if (c <> '>') and (c <> ';') then
      begin
	 seqlen := seqlen + 60;
	 if (seqlen + 80 > buflen) then
	 begin
	    buflen := buflen * 10;
	    reallocmem(buffer, buflen + 1);
	    x := @(buffer[seqlen])
	 end
	 else
	    x := @(x[60]);
	 x[0] := #0;
      end;
   until eof;
   
   seqlen := strlen(buffer);
   
   write_frequencies(1, buffer, seqlen);
   write_frequencies(2, buffer, seqlen);
   write_count('ggt', buffer, seqlen);
   write_count('ggta', buffer, seqlen);
   write_count('ggtatt', buffer, seqlen);
   write_count('ggtattttaatt', buffer, seqlen);
   write_count('ggtattttaatttatagt', buffer, seqlen);
   strdispose(buffer);
end; { main }
   
   
begin
   main;
end.
