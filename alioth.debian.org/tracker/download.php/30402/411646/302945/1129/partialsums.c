/*
** The Great Computer Language Shootout
** http://shootout.alioth.debian.org/
** contributed by Mike Pall
**
** compile with: gcc -O3 -fomit-frame-pointer -o partialsums partialsums.c -lm
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char **argv)
{
  int k, n = atoi(argv[1]);
  double sum;

/*
* Yes, I tried using a double as a primary or secondary loop variable.
* But the x86 ABI requires a cleared x87 FPU stack before every call
* (e.g. to pow()) which nullifies any performance gains.
*/
#define kd ((double)k)

  sum = 0.0;
  for (k = 0; k <= n; k++) sum += pow(2.0/3.0, kd);
  printf("%.9f\t(2/3)^k\n", sum);

  sum = 0.0;
  for (k = 1 ; k <= n; k++) sum += pow(kd, -0.5);
  printf("%.9f\tk^-0.5\n", sum);

  sum = 0.0;
  for (k = 1; k <= n; k++) sum += 1.0/(kd*(kd+1.0));
  printf("%.9f\t1/k(k+1)\n", sum);

  sum = 0.0;
  for (k = 1; k <= n; k++) {
    double sk = sin(kd);
    sum += 1.0/(kd*kd*kd*sk*sk);
  }
  printf("%.9f\tFlint Hills\n", sum);

  sum = 0.0;
  for (k = 1; k <= n; k++) {
    double ck = cos(kd);
    sum += 1.0/(kd*kd*kd*ck*ck);
  }
  printf("%.9f\tCookson Hills\n", sum);

  sum = 0.0;
  for (k = 1; k <= n; k++) sum += 1.0/kd;
  printf("%.9f\tHarmonic\n", sum);

  sum = 0.0;
  for (k = 1; k <= n; k++) sum += 1.0/(kd*kd);
  printf("%.9f\tRiemann Zeta\n", sum);

  sum = 0.0;
  for (k = 1; k <= n-1; k += 2) sum += 1.0/kd;
  for (k = 2; k <= n; k += 2) sum -= 1.0/kd;
  printf("%.9f\tAlternating Harmonic\n", sum);

  sum = 0.0;
  for (k = 1; k <= 2*n-1; k += 4) sum += 1.0/kd;
  for (k = 3; k <= 2*n; k += 4) sum -= 1.0/kd;
  printf("%.9f\tGregory\n", sum);

  return 0;
}

