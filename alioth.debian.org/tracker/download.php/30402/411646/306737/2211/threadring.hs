-- The Great Computer Language Shootout
-- http://shootout.alioth.debian.org/
-- contributed by Jed Brown with improvements by Spencer Janssen

import Control.Concurrent
import Control.Monad
import System.Environment

ringSize = 503

newThread l i = do
  r <- newEmptyMVar
  forkIO (thread i l r)
  return r

thread :: Int -> MVar Int -> MVar Int -> IO ()
thread i l r = do
  m <- takeMVar l 
  when (m == 1) (print i)
  putMVar r $! max 0 (m - 1)
  when (m > 0) (thread i l r)

main = do
    a <- newMVar . read . head =<< getArgs
    z <- foldM newThread a [2..ringSize]
    thread 1 z a
