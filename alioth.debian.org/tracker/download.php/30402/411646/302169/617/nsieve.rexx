#!/usr/local/bin/regina -a

/* ------------------------------------------------------------------ */
/* The Great Computer Language Shootout                               */
/* http://shootout.alioth.debian.org/                                 */
/*                                                                    */
/* % regina -v                                                        */
/* REXX-Regina_3.3(MT) 5.00 25 Apr 2004                               */
/* % uname -orvmp                                                     */
/* 2.6.5-1.358 #1 Wed Oct 13 17:49:34 EST 2004 i686 i686 GNU/Linux    */
/*                                                                    */
/* Contributed by Anthony Borla                                       */
/* ------------------------------------------------------------------ */

trace 'OFF' ; numeric digits 11

N = ARG(1) ; if DATATYPE(N) \= 'NUM' | N < 1 then ; N = 1

call nsieveResults 2 ** N * 10000
call nsieveResults 2 ** (N - 1) * 10000
call nsieveResults 2 ** (N - 2) * 10000

exit 0

/* ----------------------------- */

nsieveResults : procedure
  N = ARG(1)
  say "Primes up to" N nsieve(N)
  return

/* ----------------------------- */

nsieve : procedure
  M = ARG(1) ; count = -1

  do i = 2 to M
    if SYMBOL('A.i') \= 'VAR' then do
      do j = i + i by i while j < M ; A.j = 1 ; end 
      count = count + 1
    end
  end

  return count
