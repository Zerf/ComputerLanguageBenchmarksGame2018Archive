// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org/
//
// Contributed by Shyamal Prasad
// Modified by Paul Kitchin
// OpenMP by The Anh Tran


#include <iostream>
#include <vector>
#include <sys/mman.h>

// need -fopenmp flag
#include <omp.h>
// doesn't need any lib to link, header files are enough
#include <boost/xpressive/xpressive.hpp>

using namespace boost::xpressive;

// This struct encapsules a line of data
struct Chunk
{
	static const int MAX_LEN = 256;

	char data[MAX_LEN];
	size_t data_len;
	
	// pointer to prev/next line, for linear browsing operation
	Chunk* prev;
	Chunk* next;

	Chunk()
	{
		data_len = 0;
		prev = next = 0;
	}
};

typedef std::vector<Chunk*> Sequence;

// This struct represents a linear pointer, for searching patterns
// If inc/dev step surpasses a data line, it'll wrap to prev/next line
struct linear_iterator : public std::iterator<std::bidirectional_iterator_tag, char>
{
	const char	*pdata, *pbegin, *pend;
	const Chunk*	dataline;

	linear_iterator() : pdata(0), pbegin(0), pend(0), dataline(0)
	{	}

	linear_iterator(const Sequence& seq)
	{
		dataline = seq[0];
		if (dataline != 0)
		{
			pbegin = dataline->data;
			pend = pbegin + dataline->data_len;
			pdata = pbegin;
		}
		else
			pbegin = pend = pdata = 0;
	}

	inline bool operator == (const linear_iterator& rhs) const
	{
		return (pdata == rhs.pdata);
	}
	inline bool operator != (const linear_iterator& rhs) const
	{
		return !(*this == rhs);
	}
	inline linear_iterator& operator ++()
	{
		if (__builtin_expect(++pdata >= pend, false))
		{
			if (dataline->next != 0)
			{
				dataline = dataline->next;
				pbegin = dataline->data;
				pend = pbegin + dataline->data_len;
				pdata = pbegin;
			}
			else
				pbegin = pend = pdata = 0;
		}
		return (*this);
	}
	inline linear_iterator& operator --()
	{
		if (__builtin_expect(pdata <= pbegin, false))
		{
			if (dataline->prev != 0)
			{
				if (pdata != 0)
					dataline = dataline->prev;
				pbegin = dataline->data;
				pend = pbegin + dataline->data_len;
				pdata = pend -1;
			}
			else
				pbegin = pend = pdata = 0;
		}
		else
			--pdata;
		return (*this);
	}
	inline char operator*() const
	{
		return *pdata;
	}
};


// read all redirected data from stdin
Sequence* ReadInput(size_t &size, size_t &szstrip, Chunk* &memblk)
{
	size = ftell(stdin);
	fseek(stdin, 0, SEEK_END);
	size = ftell(stdin) - size;
	fseek(stdin, 0, SEEK_SET);

	// map input file to process space
	char* pdata = (char*)mmap(0, size, PROT_READ, MAP_PRIVATE|MAP_POPULATE, fileno(stdin), 0);

	// an array to store data line markers
	Sequence* seq = new Sequence();

	// predict ndataline = size / 60
	size_t alloc_sz = size / 60 + 8;
	seq->reserve (alloc_sz);

	// Pre-alloc whole storage space: maximum of nline * sizeof(Chunk) OR size * 1.5
	alloc_sz = std::max( alloc_sz * sizeof(Chunk), size + (size >> 1) );
	memblk = (Chunk*)malloc( alloc_sz );

	// Game rule: ">[^\n]*\n | \n"
	cregex strip( ( s1 = (as_xpr('>') >> *(~_n) >> _n) ) | ( s2 = _n ) );
	cmatch mtrs;

	Chunk* index = memblk;
	Chunk* c1 = new (index) Chunk();
	++index;

	const char* psrc = pdata;
	const char* pend = pdata + size;
	szstrip = 0;

	while ( regex_search((const char*)psrc, (const char*)pend, mtrs, strip) )	{
		if (mtrs[2].matched)
		{			c1->data_len = mtrs[0].first - psrc;
			memcpy(c1->data, psrc, c1->data_len);
			szstrip += c1->data_len;
			seq->push_back (c1);

			Chunk* c2 = new (index) Chunk();			++index;

			c2->prev = c1;			c1->next = c2;
			c1 = c2;
		}

		psrc = mtrs[0].second;	}

	if (c1->prev != 0)		c1->prev->next = 0;	//delete c1;

	munmap(pdata, size);	return seq;
}
void PrintPatternsMatch(const Sequence & seq)
{
	const char* patterns[] = {
		"agggtaaa|tttaccct",
		"[cgt]gggtaaa|tttaccc[acg]",
		"a[act]ggtaaa|tttacc[agt]t",
		"ag[act]gtaaa|tttac[agt]ct",
		"agg[act]taaa|ttta[agt]cct",
		"aggg[acg]aaa|ttt[cgt]ccct",
		"agggt[cgt]aa|tt[acg]accct",
		"agggta[cgt]a|t[acg]taccct",
		"agggtaa[cgt]|[acg]ttaccct"
	};
	const int npt = sizeof(patterns) / sizeof(patterns[0]);
	size_t count_match[npt] = {0};

	// Parallel search, work is divided by search patterns
	#pragma omp parallel for default(shared) schedule(dynamic, 1)
	for (int i = 0; i < npt; ++i)
	{
		linear_iterator seq_ite(seq), seq_end;
		typedef basic_regex<linear_iterator> reg_li;
		reg_li const &search_ptn( reg_li::compile(patterns[i], regex_constants::nosubs|regex_constants::optimize) );

		typedef regex_iterator<linear_iterator> match_iterator;
		match_iterator mi(seq_ite, seq_end, search_ptn);
		match_iterator me;

		size_t count = 0;
		while (mi != me)
		{
			++count;
			++mi;
		}
		
		count_match[i] = count;
	}

	for (int i = 0; i < npt; ++i)
		std::cout << patterns[i] << ' ' << count_match[i] << std::endl;
}

size_t Replace_8mers(Sequence &seq)
{
	const cregex search_reg[] = 
	{	
		as_xpr('B'),	as_xpr('D'),	as_xpr('H'),	as_xpr('K'),	
		as_xpr('M'),	as_xpr('N'),	as_xpr('R'),	as_xpr('S'),	
		as_xpr('V'),	as_xpr('W'),	as_xpr('Y')
	};
	const char* replace_ptn[] =
	{
		"(c|g|t)",	"(a|g|t)",	"(a|c|t)",	"(g|t)",	
		"(a|c)",	"(a|c|g|t)","(a|g)",	"(c|t)",	
		"(a|c|g)",	"(a|t)",	"(c|t)"
	};
	const size_t n_search_ptn = sizeof(replace_ptn)/sizeof(replace_ptn[0]);
	const size_t replace_ptn_len[] = 
	{
		7,	7,	7,	5,	
		5,	9,	5,	5,	
		7,	5,	5
	};

	size_t new_seq_size = 0;	int nline = static_cast<int>(seq.size());
	// scan data line for each regex pattern
	#pragma omp parallel default(shared) reduction(+:new_seq_size)
	for (size_t ptn_idx = 0; ptn_idx < n_search_ptn; ++ptn_idx)	{
		cregex const &search_ptn (search_reg[ptn_idx]);
		cmatch mt;		char tmpbuf[Chunk::MAX_LEN];		// Parallel replace, work is divided by lines		#pragma omp for schedule(dynamic, nline >> 10) nowait
		for (int ln = 0; ln < nline; ++ln)		{			Chunk* c = seq[ln];			size_t deslen = 0;
			const char* psrc = c->data;			const char* psrc_end = c->data + c->data_len;
			bool changed = false;			while ( regex_search(psrc, psrc_end, mt, search_ptn) )			{				// copy [prefix - first char matched)				size_t prefix = mt[0].first - psrc;				memcpy(tmpbuf + deslen, psrc, prefix);				deslen += prefix;
				// copy [replace_pattern]				memcpy(tmpbuf + deslen, replace_ptn[ptn_idx], replace_ptn_len[ptn_idx]);				deslen += replace_ptn_len[ptn_idx];
				// set pointer to next search position				psrc = mt[0].second;				changed = true;			}
			if (changed)			{				// copy [last char matched - end line)				size_t suffix = psrc_end - psrc;				memcpy(tmpbuf + deslen, psrc, suffix);				deslen += suffix;
				// replace existing data line				memcpy(c->data, tmpbuf, deslen);				c->data_len = deslen;				c->data[deslen] = 0;
			}
			// if this is the last replace pattern, update replaced sequence length
			if ( ptn_idx == (n_search_ptn-1) )
				new_seq_size += c->data_len;		}	}
	// update new sequence size
	return new_seq_size;}

int main()
{
	Chunk* memblk = 0;
	size_t initial_length = 0, strip_length = 0;

	Sequence* seq = ReadInput (initial_length, strip_length, memblk);
	PrintPatternsMatch(*seq);
	std::size_t replace_8m_length = Replace_8mers (*seq);

	std::cout << std::endl
		<< initial_length		<< std::endl
		<< strip_length		<< std::endl
		<< replace_8m_length	<< std::endl;

	free(memblk);
	return 0;
}

