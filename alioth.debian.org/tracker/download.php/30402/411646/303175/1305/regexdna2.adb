-----------------------------------------------------------------------
-- The Great American Computer Language Shootout
-- http://shootout.alioth.debian.org/
--
-- Contributed by Jim Rogers
--
-- This version uses the GNAT Spitbol Pattern matching libraries
-- rather than the more commonly used Unix-style regex libraries
-----------------------------------------------------------------------
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Gnat.Spitbol.Patterns; use Gnat.Spitbol.Patterns;
use Gnat.Spitbol;

procedure Regexdna2 is
   Variant_Labels : constant array (Positive range 1..9) of Vstring := (
      To_Unbounded_String("agggtaaa|tttaccct"),
      To_Unbounded_String("[cgt]gggtaaa|tttaccc[acg]"),
      To_Unbounded_String("a[act]ggtaaa|tttacc[agt]t"),
      To_Unbounded_String("ag[act]gtaaa|tttac[agt]ct"),
      To_Unbounded_String("agg[act]taaa|ttta[agt]cct"),
      To_Unbounded_String("aggg[acg]aaa|ttt[cgt]ccct"),
      To_Unbounded_String("agggt[cgt]aa|tt[acg]accct"),
      To_Unbounded_String("agggta[cgt]a|t[acg]taccct"),
      To_Unbounded_String("agggtaa[cgt]|[acg]ttaccct"));
      
   Variant_Patterns : constant array(Positive range 1..9) of Pattern := (
      "agggtaaa" or "tttaccct",
      (Any("cgt") & "gggtaaa") or ("tttaccc" & Any("acg")),
      ("a" & Any("act") & "ggtaaa") or ("tttacc" & Any("agt") & "t"),
      ("ag" & Any("act") & "gtaaa") or ("tttac" & Any("agt") & "ct"),
      ("agg" & Any("act") & "taaa") or ("ttta" & Any("agt") & "cct"),
      ("aggg" & Any("acg") & "aaa") or ("ttt" & Any("cgt") & "ccct"),
      ("agggt" & Any("cgt") & "aa") or ("tt" & Any("acg") & "accct"),
      ("agggta" & Any("cgt") & "a") or ("t" & Any("acg") & "taccct"),
      ("agggtaa" & Any("cgt")) or (Any("acg") & "ttaccct"));
      
   type Iub is
      record
         Code         : Unbounded_String;
         Alternatives : Unbounded_String;
   end record;
   subtype Codes_Index is Natural range 0..10;
   type Codes_Array is array (Codes_Index) of Iub;
   Codes : constant Codes_Array := (
      (To_Unbounded_String ("B"), To_Unbounded_String ("(c|g|t)")), 
      (To_Unbounded_String ("D"), To_Unbounded_String ("(a|g|t)")), 
      (To_Unbounded_String ("H"), To_Unbounded_String ("(a|c|t)")), 
      (To_Unbounded_String ("K"), To_Unbounded_String ("(g|t)")), 
      (To_Unbounded_String ("M"), To_Unbounded_String ("(a|c)")), 
      (To_Unbounded_String ("N"), To_Unbounded_String ("(a|c|g|t)")), 
      (To_Unbounded_String ("R"), To_Unbounded_String ("(a|g)")), 
      (To_Unbounded_String ("S"), To_Unbounded_String ("(c|g)")), 
      (To_Unbounded_String ("V"), To_Unbounded_String ("(a|c|g)")), 
      (To_Unbounded_String ("W"), To_Unbounded_String ("(a|t)")), 
      (To_Unbounded_String ("Y"), To_Unbounded_String ("(c|t)")));
      
   function Get_Alternative(Code : Unbounded_String) return VString is
      Temp : Vstring := Null_Unbounded_String;
   begin
      for I in Codes'range loop
         if Codes(I).Code = Code then
            Temp := Codes(I).Alternatives;
            exit;
         end if;
      end loop;
      return Temp;
   end Get_Alternative;
   
   function Replace(Substitution : in Vstring;
         Index : Positive;
         Source : Vstring) return Vstring is
      Temp : Vstring;
   begin
      if Index = 1 then
         Temp := Substitution & Slice(Source, 2, Length(Source));
      else
         Temp := Slice(Source, 1, Index - 1) &
            Substitution & Slice(Source, Index + 1, Length(Source));
      end if;
      return Temp;
   end Replace;  
   
   type Dna_Lines is array(Positive range <>) of Vstring;
   function Length(Item : in Dna_Lines) return Natural is
      Sum : Natural := 0;
   begin
      for I in Item'range loop
         Sum := Sum + Length(Item(I));
      end loop;
      return Sum;
   end Length;
   
   Iub_Found : Vstring_var;
   Iub_Index : aliased Natural;
   Iub_Pattern : constant Pattern := ("B" or "D" or "H" or "K" 
      or "M" or "N" or "R" or "S" or "V" or "W" or "Y")  * 
      Iub_Found & Setcur(Iub_Index'access);
   
   Initial_Length : Natural := 0;
   Code_Length : Natural;
   Line : String(1..80);
   Var_Line : Vstring_Var;
   Line_Length : Natural;
   Sequence : Vstring_Var;
   Fasta_Description : constant Pattern := Pos(0) & ">" & Rest;
   Descrip_Pattern : constant Pattern := (Fasta_Description);
   Count : Natural := 0;
   Num_Lines : Natural;
   Match_Found : Boolean;
begin
   
   -- Read FASTA Sequence
   -- Record length and remove the unwanted elements
   
   while not End_Of_File loop
      Get_Line(Item => Line, Last => Line_Length);
      Var_Line := To_Unbounded_String(Line(1..Line_Length));
      Initial_Length := Initial_Length + Length(Var_Line) + 1;
      Match(Subject => Var_Line, 
         Pat => Descrip_Pattern, Replace => "");
      Append(Source => Sequence, New_Item => Var_Line);
   end loop;
   Code_Length := Length(Sequence);
   
   -- regex match
   for I in Variant_Labels'range loop
      Count := 0;
      for J in 1..Length(Sequence) - 8 loop
         if Match(Subject => Substr(Str   => Sequence,
                  Start => J, Len   =>8 ),
               Pat   => Variant_Patterns(I)) then
            Count := Count + 1;
         end if;
      end loop;
      Put(To_String(Variant_Labels(I)) & " ");
      Put(Item => Count, Width => 1);
      New_Line;
   end loop;
   New_Line;
   
   -- regex substitution

   Num_Lines := Length(Sequence) / 80;
   if Length(Sequence) mod 80 > 1 then
      Num_Lines := Num_Lines + 1;
   end if;
   declare
      Sequence_Lines : Dna_Lines(1..Num_Lines);
      Low, Sub_Len : Natural;
   begin
      -- Distribute Sequence to Sequence_Lines
      Low := 1;
      Sub_Len := 80;
      for I in Sequence_Lines'range loop
         Sequence_Lines(I) := Substr(Str => Sequence ,
            Start => Low, Len => Sub_Len );
         Low := Low + Sub_Len;
         if Low + Sub_Len > Length(Sequence) then
            Sub_Len := Length(Sequence) - Low + 1;
         end if;
      end loop;
      
      -- Perform the regex substitution
      for I in Sequence_Lines'range loop
         
         loop
            Match_Found := Match(Subject => Sequence_Lines(I),
               Pat => Iub_Pattern);
            exit when not Match_Found;
            Sequence_Lines(I) := Replace(Substitution => Get_Alternative(Iub_Found),
               Index => Iub_Index, Source => Sequence_Lines(I));    
         end loop;
      end loop;
      Put(Item => Initial_Length, Width => 1);
      New_Line;
      Put(Item => Code_Length, Width => 1);
      New_Line;
      Put(Item => Length(Sequence_Lines), Width => 1);
      New_Line;
   end;
   
   
end Regexdna2;
