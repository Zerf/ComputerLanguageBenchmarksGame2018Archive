#!/usr/bin/env python

# Contributed by Giovanni Bajo
# Use continuations as provided by the greenlet library
# SVN download: http://codespeak.net/svn/user/arigo/greenlet/

import sys
import greenlet

main = greenlet.getcurrent()

def thread(next):
    data = main.switch(None)
    switch = next.switch
    for i in xrange(N):
        data = switch(data+1)

# Create 500 linked greenlet. The last one links back to main.
g = main
for i in range(500):
    g = greenlet.greenlet(lambda g=g: thread(g))
    g.switch()

# Push 0 through the queue and sum results
N = int(sys.argv[1])
print sum(g.switch(0) for i in xrange(N))
