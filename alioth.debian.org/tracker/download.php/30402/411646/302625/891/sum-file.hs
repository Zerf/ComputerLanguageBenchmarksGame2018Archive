-- The Great Computer Language Shootout
-- http://shootout.alioth.debian.org/
-- contributed by Josh Goldfoot

-- sum-file in Haskell

module Main where

main = do
   incoming <- getContents
   putStrLn $ show $ sum $ map read $ lines incoming

