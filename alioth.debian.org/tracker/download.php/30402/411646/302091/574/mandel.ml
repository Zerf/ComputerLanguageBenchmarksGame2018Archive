let niter = 50
let limit = 2.

let limit2 = limit *. limit

type complex = { mutable r:float; mutable i:float }

let add_bit0 ~tmp:z c byte =
  z.r <- 0.;
  z.i <- 0.;
  let cr = c.r and ci = c.i in
  try
    for i = 1 to niter do
      let zi = 2. *. z.r *. z.i +. ci in
      z.r <- z.r *. z.r -. z.i *. z.i +. cr;
      z.i <- zi;
      if z.r *. z.r +. zi *. zi > limit2 then raise Exit;
    done;
    (byte lsl 1) lor 0x01
  with Exit -> (byte lsl 1) lor 0x00

let () =
  let w = int_of_string(Array.get Sys.argv 1) in
  let h = w in
  let fw = float w
  and fh = float h
  and cplmt8 = 8 - w mod 8 in
  Printf.printf "P4\n%i %i\n" w h;
  let byte = ref 0
  and bit = ref 0 in
  let c = { r = 0.0; i = 0.0 } in
  let tmp = { r = 0.0; i = 0.0 } in
  for y = 0 to h - 1 do
    c.i <- 2. *. float y /. fh -. 1.;
    for x = 0 to w - 1 do
      c.r <- 2. *. float x /. fw -. 1.5;
      byte := add_bit0 ~tmp c !byte;
      incr bit;
      if !bit = 8 then (
        output_byte stdout !byte;
        byte := 0;
        bit := 0;
      )
    done;
    if !bit <> 0 then (
      output_byte stdout (!byte lsl cplmt8);
      byte := 0;
      bit := 0;
    )
  done
