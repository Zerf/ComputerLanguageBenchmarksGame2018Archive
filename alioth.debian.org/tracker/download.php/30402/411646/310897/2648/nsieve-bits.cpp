/*
** The Great Computer Language Shootout
** http://shootout.alioth.debian.org/
** contributed by Mike Pall
** modified by Chris Cox
*/

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <vector>

typedef unsigned char bits;
#define BBITS		(sizeof(bits) * 8)
#define BSIZE(x)	(((x) / (3*8)) + sizeof(bits))
#define BMASK(x)	(bits(1) << (((x)/3) % BBITS))
#define BTEST(p, x)	((p)[(x) / (3*BBITS)] & BMASK(x))
#define BFLIP(p, x)	(p)[(x) / (3*BBITS)] ^= BMASK(x)
#define BCLEAR(p, x) (p)[(x) / (3*BBITS)] &= ~BMASK(x)
const unsigned int kPatternLength = 2;
const unsigned int kPattern[] = { 2, 4 };

int main(int argc, char **argv)
{
	const unsigned int sz = 10000 << (argc > 1 ? atoi(argv[1]) : 11);
	std::vector<bits> primes( BSIZE(sz) );
	unsigned int m;
	for (m = 0; m <= 2; m++) {
		const unsigned int n = sz >> m;
		memset(&primes[0], 0xff, BSIZE(n));
		const unsigned int end = int(sqrt(n));	// one factor of N must be <= sqrt(N)
		unsigned int count = 2;				// 2 and 3 are trivial cases, not stored
		unsigned int i, ii;
		for (i = 5, ii = 0; i <= end; i += kPattern[ii%kPatternLength], ii++)
			if (BTEST(primes, i)) {
				count++;
				unsigned int j, jj;
				for (j = i*5, jj = 0; j <= (n - i*12); j += 12*i, jj += 4) {
					BCLEAR(primes, j);
					BCLEAR(primes, j+i*2);
					BCLEAR(primes, j+i*6);
					BCLEAR(primes, j+i*8);
				}
				for (; j <= n; j += i * kPattern[jj%kPatternLength], jj++)
					BCLEAR(primes, j);
			}

		for (; i < (n - 12); i += 12, ii += 4) {
			bool test0 = 0 != BTEST(primes, i);
			bits test1 = 0 != BTEST(primes, (i+kPattern[(ii)%kPatternLength]));
			bits test2 = 0 != BTEST(primes, 6+i);
			bits test3 = 0 != BTEST(primes, 6+(i+kPattern[(ii)%kPatternLength]));
			count += test0 + test1 + test2 + test3;
		}
		for (; i <= n; i += kPattern[ii%kPatternLength], ii++)
			if (BTEST(primes, i))
				count++;
		printf("Primes up to %8d %8d\n", n, count);
	}
	return 0;
}
