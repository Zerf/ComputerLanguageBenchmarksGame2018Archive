;;; -*- mode: lisp -*-
;;; http://shootout.alioth.debian.org/
;;; by Patrick Frankenberger

(defparameter *buffer* (make-array 4096 :element-type 'base-char))
(defparameter *word* (make-array 0 :element-type 'base-char :adjustable t :fill-pointer t))
(defparameter *ht* (make-hash-table :size 60000 :test 'equal))

(defun add-word ()
  (declare (type (array base-char) *word*))
  (when (/= (length *word*) 0)
    (let ((key (string-downcase *word*)))
      (setf (gethash key *ht*) (1+ (the fixnum (gethash key *ht* 0))))
      (setf (fill-pointer *word*) 0))))

(defun readit ()
  (declare (type (simple-array base-char) *buffer*)
	   (type (array base-char) *word*))
  (do ((num-read (read-sequence *buffer* *standard-input*)
		(read-sequence *buffer* *standard-input*)))
      (nil)
    (loop for i fixnum below num-read do
	  (if (alpha-char-p (aref *buffer* i)) 
	      (vector-push-extend (aref *buffer* i) *word*)
	      (add-word)))
    (when (/= num-read 4096) (return))))

(defun order (l r)
  (declare (type (cons simple-base-string fixnum) l r))
  (if (/= (cdr l) (cdr r))
      (> (cdr l) (cdr r))
      (string> (car l) (car r))))

(defun printit ()
  (let ((res (make-array (hash-table-count *ht*) :element-type '(cons simple-base-string fixnum) :fill-pointer t)))
    (setf (fill-pointer res) 0)
    (maphash #'(lambda (k v) (vector-push (cons k v) res)) *ht*)
    (setf res (sort res #'order))
    (loop for a across res do
	  (format t "~7d ~A~%" (cdr a) (car a)))))

(defun main ()
  (readit)
  (printit))
