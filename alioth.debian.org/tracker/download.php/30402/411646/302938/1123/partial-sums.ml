(* The Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Johannes Weißl (OCaml version of Isaac Gouy's clean version)
*)

let rec twoThirds k n sum =
  if k < n then
    twoThirds(k +. 1.0) n (sum +. ((2.0 /. 3.0) ** k))
  else
    sum

let rec k05 k n sum =
  if k <= n then
    k05 (k +. 1.0) n (sum +. (k ** (-0.5)))
  else
    sum

let rec kk1 k n sum =
  if k <= n then
    kk1 (k +. 1.0) n (sum +. 1.0 /. (k *. (k +. 1.0)))
  else
    sum

let rec flintHills k n sum =
  if k <= n then
    flintHills (k +. 1.0) n (sum +. 1.0 /. ((k ** 3.0) *. ((sin k) ** 2.0)))
  else
    sum

let rec cooksonHills k n sum =
  if k <= n then
    cooksonHills (k +. 1.0) n (sum +. 1.0 /. ((k ** 3.0) *. ((cos k) ** 2.0)))
  else
    sum

let rec harmonic k n sum =
  if k <= n then
    harmonic (k +. 1.0) n (sum +. 1.0 /. k)
  else
    sum

let rec riemannZeta k n sum =
  if k <= n then
    riemannZeta (k +. 1.0) n (sum +. 1.0 /. (k ** 2.0))
  else
    sum

let rec altHarmonic k n a sum =
  if k <= n then
    altHarmonic (k +. 1.0) n (-.a) (sum +. a /. k)
  else
    sum

let rec gregory k n a sum =
  if k <= n then
    gregory (k +. 1.0) n (-.a) (sum +. a /. (2.0 *. k -. 1.0))
  else
    sum
;;

if Array.length Sys.argv > 1 then
  let n = float_of_string Sys.argv.(1) in
  Printf.printf "%.9f\t(2/3)^k\n" (twoThirds 0.0 n 0.0) ;
  Printf.printf "%.9f\tk^-0.5\n" (k05 1.0 n 0.0) ;
  Printf.printf "%.9f\t1/k(k+1)\n" (kk1 1.0 n 0.0) ;
  Printf.printf "%.9f\tFlint Hills\n" (flintHills 1.0 n 0.0) ;
  Printf.printf "%.9f\tCookson Hills\n" (cooksonHills 1.0 n 0.0) ;
  Printf.printf "%.9f\tHarmonic\n" (harmonic 1.0 n 0.0) ;
  Printf.printf "%.9f\tRiemann Zeta\n" (riemannZeta 1.0 n 0.0) ;
  Printf.printf "%.9f\tAlternating Harmonic\n" (altHarmonic 1.0 n 1.0 0.0) ;
  Printf.printf "%.9f\tGregory\n" (gregory 1.0 n 1.0 0.0)
