(* The Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Johannes Weißl (OCaml version of Isaac Gouy's clean version)
*)

let rec loop k n alt a1 a2 a3 a4 a5 a6 a7 a8 a9 =
  if k <= n then
    loop (k +. 1.0) n (-.alt)
      (a1 +. (2.0 /. 3.0) ** (k -. 1.0))
      (a2 +. k ** (-0.5))
      (a3 +. 1.0 /. (k *. (k +. 1.0)))
      (a4 +. 1.0 /. ((k *. k) *. k *. ((sin k) *. (sin k))))
      (a5 +. 1.0 /. ((k *. k) *. k *. ((cos k) *. (cos k))))
      (a6 +. 1.0 /. k)
      (a7 +. 1.0 /. (k *. k))
      (a8 +. alt /. k)
      (a9 +. alt /. (2.0 *. k -. 1.0))
  else
    [a1; a2; a3; a4; a5; a6; a7; a8; a9]

let _ =
  if Array.length Sys.argv > 1 then
    let n = float_of_string Sys.argv.(1) in
    let names = ["(2/3)^k"; "k^-0.5"; "1/k(k+1)"; "Flint Hills";
                 "Cookson Hills"; "Harmonic"; "Riemann Zeta";
                 "Alternating Harmonic"; "Gregory"] in
    let sums = loop 1.0 n 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 in
    List.iter2 (fun x y -> Printf.printf "%.9f\t%s\n" x y) sums names
