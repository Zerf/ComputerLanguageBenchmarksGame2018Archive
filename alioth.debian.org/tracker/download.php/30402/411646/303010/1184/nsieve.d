// The Great Computer Language Shootout
// http://shootout.alioth.debian.org/
// Written by Dima Dorfman, 2004
// Converted to D by Dave Fladebo
// Modified by bearophile, Feb 8 2006
// Compile with: dmd -O -inline -release nsieve.d

import std.stdio, std.string;

int main(char[][] args) {
    int m = atoi(args[1]);

    for(int i = 0; i < 3; i++)
        NSieve(10000 << (m-i));

    return 0;
}

void NSieve(int m) {
    bool[] isPrime;
    isPrime.length = m + 1;
    isPrime[] = true;
    int count = 0;

    for(int i = 2; i < isPrime.length; i++) {
        if(isPrime[i]) {
            count++;
            for(int k = i << 1; k < isPrime.length; k += i)
                if (isPrime[k])
                    isPrime[k] = false;
        }
    }

    writefln("Primes up to %8d %8d", m, count);
}
