/* ------------------------------------------------------------------ */
/* The Great Computer Language Shootout                               */
/* http://shootout.alioth.debian.org/                                 */
/*                                                                    */
/* Contributed by Anthony Borla                                       */
/* ------------------------------------------------------------------ */

#include <cctype>
#include <deque>
#include <queue>
#include <string>
#include <algorithm>
#include <iterator>
#include <iostream>

using namespace std;

const int LINELENGTH = 60;
const char ENDLINE = '\n', SEGMARKER = '>';

typedef queue<string, deque<string> > Header;
typedef string Segment;

typedef ostreambuf_iterator<char> StreamIterOut;

inline Segment& transformSegment(Segment& segment);
void dumpSegment(Header& header, Segment& segment, ostream& out = cout);
inline char complement(char element);

/* ----------------------------- */

int main()
{
  ios_base::sync_with_stdio(false);

  bool okToDump = false; string line; Segment segment; Header header;
  
  while (getline(cin, line, ENDLINE))
  {
    if (line[0] == SEGMARKER)
    {
      header.push(line); if (!okToDump) { okToDump = true; continue; }
      dumpSegment(header, transformSegment(segment));  
    }
    else
    {
      segment += line;
    }
  }

  dumpSegment(header, transformSegment(segment));  

  return 0;
}

/* ----------------------------- */

Segment& transformSegment(Segment& segment)
{
  reverse(segment.begin(), segment.end());
  transform(segment.begin(), segment.end(), segment.begin(), complement);
  return segment;
}

/* ----------------------------- */

void dumpSegment(Header& header, Segment& segment, ostream& out)
{
  out << header.front() << ENDLINE; header.pop();

  Segment::iterator begin = segment.begin(), i = segment.begin();

  while (i != segment.end())
  {
    if (distance(begin, i) == LINELENGTH)
    {
       copy(begin, i, StreamIterOut(out)); out << ENDLINE;
       begin = i; 
    }
    ++i;
  }

  copy(begin, i, StreamIterOut(out)); out << endl;

  segment.resize(0);
}

/* ----------------------------- */

char complement(char element)
{
  static char charMap[] =
  {
    'T', 'V', 'G', 'H', '\0', '\0', 'C', 'D', '\0', '\0', 'M', '\0', 'K',
    'N', '\0', '\0', '\0', 'Y', 'S', 'A', 'A', 'B', 'W', '\0', 'R', '\0'
  };

  return charMap[toupper(element) - 'A'];
}
