/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Lester Vecsey */


#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

	double Cr, Ci, Zr_sq=0, Zi_sq=0, ZrZi=0, Zr=0, Zi=0, limit_sq = 4.0;
	int expecting_len, max, bl, res, i=0, x=0, y=0, pos=0, acc=1, iter = 50;

	char *pbm_data, pbm_header[20], *p, *pend, *res_str;

	res = strtol( res_str = (argc > 1) ? argv[1] : "200", NULL, 10);

	if ( (p = pbm_data = (char*) malloc( max = res * (bl = ((res>>3) + ((res&7) > 0))))) == NULL) return -1;

	if (sprintf(pbm_header, "P4\n%d %d\n", res, res) != (expecting_len = strlen(res_str) * 2 + 5) ) return -1;

	if (fwrite(pbm_header, 1, expecting_len, stdout) != expecting_len) return -1;

	for ( pend = p + max; p < pend; x%=res, Zr=Zi=ZrZi=i=0) {
						
		Cr = (2*((double)x)/res - 1.5); Ci=(2*((double)y)/res - 1);

		for(acc<<=1; !(acc&1) && i++ < iter; acc |= Zr_sq+Zi_sq > limit_sq, ZrZi=Zr*Zi) {

			Zr = (Zr_sq=Zr*Zr) - (Zi_sq=Zi*Zi) + Cr;
			Zi = 2*ZrZi + Ci;

			}
			
		if (++x==res) { y++; if (acc<256) acc <<= (8 - res%8); }

		if (acc>255) { *p++ = (acc ^= 255) & 255; acc = 1; }
			
		}
	
	return -1 * fwrite(pbm_data, bl, res, stdout) != res;

	}

