/*
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * modified by Mehmet D. AKIN
 * threaded and refactored by Cedric Hurst
 *
 */

import java.io.IOException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

class fasta {
	public static ExecutorService threadPool = Executors.newFixedThreadPool(3);
    
    public static final int IM = 139968;
    public static final int IA = 3877;
    public static final int IC = 29573;

    public static final int LINE_LENGTH = 60;

    public static class frequency {
        public byte c;
        public double p;

        public frequency(char c, double p) {
            this.c = (byte)c;
            this.p = p;
        }
    }
    
	String id, desc;
    frequency[] a;
    int n, index, last;
    ByteArrayOutputStream writer;
	
	public fasta(String id, String desc, int n)
    {
    	this.id = id;
    	this.desc = desc;
    	this.n = n;
    	this.index = 0;
    	this.last = 42;
    	this.writer = new ByteArrayOutputStream(BUFFER_SIZE);
    }
    
    public fasta(String id, String desc, frequency[] a, int n)
    {
    	this(id, desc, n);
    	this.a = a;
    }
    
    class RandomFastaThread extends Thread {
    	public void run() {
    		index = 0;
	        int m = 0;
	        String descStr = ">" + id + " " + desc + '\n';
	        writer.write(descStr.getBytes(), 0, descStr.getBytes().length);
	        while (n > 0) {
	            if (n < LINE_LENGTH) m = n;  else m = LINE_LENGTH;
	            if(BUFFER_SIZE - index < m){
	                writer.write(bbuffer, 0, index);
	                index = 0;
	            }
	            for (int i = 0; i < m; i++) {
	                bbuffer[index++] = selectRandom(a);
	            }
	            bbuffer[index++] = '\n';
	            n -= LINE_LENGTH;
	        }
	        if(index != 0) writer.write(bbuffer, 0, index);
    	}
    }
	
    class RepeatFastaThread extends Thread {
    	public void run() {
    		index = 0;
            int m = 0;
            int k = 0;
            int kn = ALUB.length;
            String descStr = ">" + id + " " + desc + '\n';
            writer.write(descStr.getBytes(), 0, descStr.getBytes().length);
            while (n > 0) {
                if (n < LINE_LENGTH) m = n; else m = LINE_LENGTH;
                if(BUFFER_SIZE - index < m){
                    writer.write(bbuffer, 0, index);
                    index = 0;
                }
                for (int i = 0; i < m; i++) {
                    if (k == kn) k = 0;
                    bbuffer[index++] = ALUB[k];
                    k++;
                }
                bbuffer[index++] = '\n';
                n -= LINE_LENGTH;
            }
            if(index != 0) writer.write(bbuffer, 0, index);
    	}
    }
    
    // pseudo-random number generator
    public final double random(double max) {
        last = (last * IA + IC) % IM;
        return max * last / IM;
    }

    // Weighted selection from alphabet
    public static String ALU =
              "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG"
            + "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA"
            + "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT"
            + "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA"
            + "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG"
            + "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC"
            + "AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA";
    public static byte[] ALUB = ALU.getBytes();

    public static final frequency[] IUB = new frequency[] {
            new frequency('a', 0.27),
            new frequency('c', 0.12),
            new frequency('g', 0.12),
            new frequency('t', 0.27),

            new frequency('B', 0.02),
            new frequency('D', 0.02),
            new frequency('H', 0.02),
            new frequency('K', 0.02),
            new frequency('M', 0.02),
            new frequency('N', 0.02),
            new frequency('R', 0.02),
            new frequency('S', 0.02),
            new frequency('V', 0.02),
            new frequency('W', 0.02),
            new frequency('Y', 0.02)
    };

    public static final frequency[] HomoSapiens = new frequency[] {
        new frequency('a', 0.3029549426680d),
        new frequency('c', 0.1979883004921d),
        new frequency('g', 0.1975473066391d),
        new frequency('t', 0.3015094502008d)
    };

    public static void makeCumulative(frequency[] a) {
        double cp = 0.0;
        for (int i = 0; i < a.length; i++) {
            cp += a[i].p;
            a[i].p = cp;
        }
    }

    // naive
    public final byte selectRandom(frequency[] a) {
        int len = a.length;
        double r = random(1.0);
        for (int i = 0; i < len; i++)
            if (r < a[i].p)
                return a[i].c;
        return a[len - 1].c;
    }

    static int BUFFER_SIZE = 1024;
    static byte[] bbuffer = new byte[BUFFER_SIZE];
    
    final fasta makeRandom() throws IOException
    {
    	threadPool.execute(new RandomFastaThread());
    	return this;
    }

    final fasta makeRepeat() throws IOException
    {
        threadPool.execute(new RepeatFastaThread());
        return this;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
    	long start = System.currentTimeMillis();
    	
        makeCumulative(HomoSapiens);
        makeCumulative(IUB);
        int n = 2500000;
        if (args.length > 0)
            n = Integer.parseInt(args[0]);
        OutputStream out = System.out;
        fasta[] fastas = new fasta[3];
        
        fastas[0] = new fasta("ONE", "Homo sapiens alu", n * 2).makeRepeat();
        fastas[1] = new fasta("TWO", "IUB ambiguity codes", IUB, n * 3).makeRandom();
        fastas[2] = new fasta("THREE", "Homo sapiens frequency", HomoSapiens, n * 5).makeRandom();
        
        threadPool.shutdown();
        
        while(!threadPool.isTerminated())
        	Thread.sleep(50);
        
        for (fasta f : fastas) {
        	System.out.println(f.writer.toString());
        	System.out.println();
        }
        
        float fullExecutionTime = (System.currentTimeMillis()-start)/1000;
		System.out.println("\n\nFull Execution time: " + fullExecutionTime + " secs.");
        
		out.close();
    }
}
