/* The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 *
 * contributed by Isaac Gouy
 * modified by Uffe Seerup
 * Add multithread by The Anh Tran
 */

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Threading;

sealed class regexdna
{
    private static ArrayList input_as_segment, input_as_line;
    
    private static string search_result;
    private static int replace_len;

    private static int nthreads = Environment.ProcessorCount;
    private static Barrier barrier = new Barrier(nthreads);

    static void Main()
    {
        string input = ReadInput();
        int init_len = input.Length;

        int strip_len = StripHeader(input);
        input = null;

        // spawn threads
        Thread[] threads = new Thread[nthreads];
        for(int i = 0; i < threads.Length; ++i)
        {
            threads[i] = new Thread(worker_function);
            threads[i].Start();
        }

        foreach (Thread t in threads)
            t.Join();
        
        input_as_segment = input_as_line = null;

        Console.WriteLine("{0}\n{1}\n{2}\n{3}", 
            search_result, init_len, strip_len, replace_len);
    }

    private static string ReadInput()
    {
        string input = Console.In.ReadToEnd();
        return input;
    }

    private static int StripHeader(string input)
    {
        Regex r = new Regex(">.*\n|\n", RegexOptions.Compiled);
        Match match = r.Match(input);

        input_as_segment = new ArrayList();
        input_as_line = new ArrayList(input.Length / 60 + 8);

        StringBuilder segment = null;
        char[] line = new char[256];
        int copy_index = 0;
        int strip_len = 0;

        while (match.Success)
        {
            if (match.Length > 1) // header line
            {
                if (segment != null)
                    input_as_segment.Add(segment.ToString());
                segment = new StringBuilder();
            }

            int copy_len = match.Index - copy_index;
            if (copy_len > 0)
            {
                input.CopyTo(copy_index, line, 0, copy_len);
                segment.Append(line, 0, copy_len);

                String tmp = new String(line, 0, copy_len);
                input_as_line.Add(tmp);

                strip_len += copy_len;
            }

            copy_index = match.Index + match.Length;
            match = match.NextMatch();
        }

        if (segment != null)
            input_as_segment.Add(segment.ToString());

        return strip_len;
    }

    private static void worker_function()
    {
        CountMatch();
        Replace();
    }

    // C# doesn't allow static var inside function scope :|
    // => have to put static var here for multithread reading
    private static string[] search_patterns = 
    {
         "agggtaaa|tttaccct",
         "[cgt]gggtaaa|tttaccc[acg]",
         "a[act]ggtaaa|tttacc[agt]t",
         "ag[act]gtaaa|tttac[agt]ct",
         "agg[act]taaa|ttta[agt]cct",
         "aggg[acg]aaa|ttt[cgt]ccct",
         "agggt[cgt]aa|tt[acg]accct",
         "agggta[cgt]a|t[acg]taccct",
         "agggtaa[cgt]|[acg]ttaccct"
    };
    private static int[] pattern_processed = new int[search_patterns.Length];
    private static int[] pattern_results = new int[search_patterns.Length];
    private static int thread_passed = 0;

    private static void CountMatch()
    {
        int total_segment = input_as_segment.Count;

        for (int ptn_idx = 0; ptn_idx < search_patterns.Length; ++ptn_idx)
        {
            Regex rx = new Regex(search_patterns[ptn_idx], RegexOptions.Compiled);
            
            int fetch_segment;
            while ((fetch_segment = Interlocked.Increment(ref pattern_processed[ptn_idx]) -1) < total_segment)
            {
                String segment = (String)input_as_segment[fetch_segment];
                for (Match m = rx.Match(segment); m.Success; m = m.NextMatch()) 
                    Interlocked.Increment(ref pattern_results[ptn_idx]);
            }
        }

        // Allow only the last thread reaching this block to print result
        Interlocked.Increment(ref thread_passed);
        if (thread_passed == nthreads)
        {
            StringBuilder s = new StringBuilder();

            for (int ptn_idx = 0; ptn_idx < search_patterns.Length; ++ptn_idx)
                s.AppendFormat("{0} {1}\n", search_patterns[ptn_idx], pattern_results[ptn_idx]);
            
            search_result = s.ToString();
            input_as_segment = null;
        }
    }


    private static string[] ptn_search = 
    {
        "B", "D", "H", "K",
        "M", "N", "R", "S",
        "V", "W", "Y"
    };
    private static string[] ptn_replace = 
    {
        "(c|g|t)", "(a|g|t)", "(a|c|t)", "(g|t)",
        "(a|c)", "(a|c|g|t)", "(a|g)", "(c|g)",
        "(a|c|g)", "(a|t)", "(c|t)"
    };
    private static int[] ptn_processed = new int[ptn_search.Length];


    private static void Replace()
    {
        int total_lines = input_as_line.Count;

        for (int ptn_idx = 0; ptn_idx < ptn_search.Length; ++ptn_idx)
        {
            Regex rx = new Regex(ptn_search[ptn_idx], RegexOptions.Compiled);
            int line_fetch;

            while ((line_fetch = Interlocked.Increment(ref ptn_processed[ptn_idx]) - 1) < total_lines)
            {
                String line = (String)input_as_line[line_fetch];
                
                if (rx.IsMatch(line))
                    input_as_line[line_fetch] = rx.Replace(line, ptn_replace[ptn_idx]);

                // if this is the last pattern, update replace_len
                if (ptn_idx == ptn_search.Length - 1)
                {
                    String ln = (String)input_as_line[line_fetch];
                    Interlocked.Add(ref replace_len, ln.Length);
                }
            }

            // threads have to wait at the end of each replace pattern
            // to avoid concurrent modifying the same data line (at different ptn_idx values)
            barrier.Wait();
        }
    }
}

internal sealed class Barrier
{
    private long barrier;
    private long nthreads;
    private ManualResetEvent wait_event;
    
    public Barrier(int nthreads)
    {
        barrier = this.nthreads = nthreads;
        wait_event = new ManualResetEvent(false);
    }

    public void Wait()
    {
        Interlocked.Decrement(ref barrier);

        long b = Interlocked.Read(ref barrier);
        if ((0 < b) && (b < nthreads))
            wait_event.WaitOne();
        else
        {
            barrier = nthreads;
            ManualResetEvent h = wait_event;
            wait_event = new ManualResetEvent(false);
            h.Set();
            h.Close();
        }
    }
};
