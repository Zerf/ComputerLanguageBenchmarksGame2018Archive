/*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 * Contributed by Premysl Hruby
 * convert to C++ by dualamd
 */

//-------------------------------------------------------------------------------
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
typedef unsigned int uint;
const uint NUM_THREADS	= 503;
const uint STACK_SIZE		= 16*1024;
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
int	token  = -1;

pthread_mutex_t	mutex [NUM_THREADS];
char			stacks [NUM_THREADS] [STACK_SIZE];
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
__attribute__((nothrow, noreturn)) 
void* thread_func( void *num )
{
	int thisnode	= (int)num;
	int nextnode	= ( thisnode + 1 ) % NUM_THREADS;

	while (true) 
	{
		pthread_mutex_lock( &(mutex[ thisnode ]) );

		__builtin_prefetch(&token, 1, 3); // going to write to token, use very frequently
		__builtin_prefetch(&(mutex[ nextnode ]), 1, 1); // going to change nextnode mutex, not frequently

		if ( token > 0 ) 
		{
			token--;

			__builtin_prefetch( &(stacks[nextnode]), 0, 1);// going to read nextnode stack, not frequently
			pthread_mutex_unlock( &(mutex[ nextnode ]) );
		}
		else 
		{
			 printf( "%d\n", thisnode +1 );
			 exit(0);
		}
	}

	//return 0;
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
int main(int argc, char** args)
{
	if (argc != 2)
		token = 1000; // test case
	else
		token = atoi(args[1]);


	pthread_attr_t stack_attr;
	pthread_attr_init(&stack_attr);

	pthread_t cthread;

	for (uint i = 0; i < NUM_THREADS; i++) 
	{
		// init mutex objects
		pthread_mutex_init( &(mutex[ i ]), 0);
		pthread_mutex_lock( &(mutex[ i ]) );

		// manual set stack space & stack size for each thread
		pthread_attr_setstack( &stack_attr, &(stacks[i]), STACK_SIZE );

		// create thread using specified stackspace
		pthread_create( &cthread, &stack_attr, &thread_func, (void*)i );
	}

	// start game
	pthread_mutex_unlock( &(mutex[0]) );

	// wait for result
	pthread_join( cthread, 0 );

	return 1;
}
//-------------------------------------------------------------------------------

