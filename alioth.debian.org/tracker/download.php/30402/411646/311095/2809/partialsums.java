import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadFactory;
import static java.lang.Math.*;

class partialsums {

    private static final double TWO_THIRDS = 2.0 / 3.0;

    public static void main(String[] args) throws Exception {
        final int n = Integer.parseInt(args[0]);

        /**
         * Use the cores...
         */
        ExecutorService threads = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors(), 
                new ThreadFactory() {
            public Thread newThread(Runnable r) {
                System.out.println(r == null ? "null" : r.getClass().getName());
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            }
        });
        
        FutureTask<Double> a1 = new FutureTask<Double>(new Callable<Double>() {
            public Double call() {
                
                double a1 = 0.0;
                final int N = n -1;
                double y = 1.0;
                for(int k=0;k<N;++k) {
                    a1 += y;
                    y*=TWO_THIRDS;                    
                }
                
                return a1;
                
            }
        });
        threads.submit(a1);
        
        FutureTask<Double> a2 = new FutureTask<Double>(new Callable<Double>() {
            public Double call() {
                
                double a2 = 0.0;
                for(int k=1;k<=n;++k) {
                    a2 += 1.0 / sqrt(k);
                }
                
                return a2;
            }
        });
        threads.submit(a2);
        
        FutureTask<Double> a3 = new FutureTask<Double>(new Callable<Double>() {
            public Double call() {
                
                double a3 = 0.0;
                for(int k=1;k<=n;++k) {
                    double K = k;
                    a3 += 1.0 / (K * (K + 1.0));
                }
                
                return a3;
            }
        });
        threads.submit(a3);

        FutureTask<Double> flintHills = new FutureTask<Double>(
                new Callable<Double>() {
            public Double call() {
                
                double a4 = 0.0;
                for(int k=1;k<=n;++k) {
                    double K = k;
                    double sk = sin(K);
                    a4 += 1.0 / (K*K*K * sk * sk);
                }
                
                return a4;
            }
        });
        threads.submit(flintHills);

        FutureTask<Double> cooksonHills = new FutureTask<Double>(
                new Callable<Double>() {
            public Double call() {
                
                double a5 = 0.0;
                for(int k=1;k<=n;++k) {
                    double K = k;
                    double ck = cos(k);
                    a5 += 1.0 / (K*K*K * ck * ck);
                }
                
                return a5;
            }
        });
        threads.submit(cooksonHills);

        FutureTask<Double> harmonic = new FutureTask<Double>(
                new Callable<Double>() {
            public Double call() {
                
                double a6 = 0.0;
                for(int k=1;k<=n;++k) {
                    a6 += 1.0 / k;
                }
                
                return a6;
            }
        });
        threads.submit(harmonic);

        FutureTask<Double> riemannZeta = new FutureTask<Double>(
                new Callable<Double>() {
            public Double call() {
                
                double a7 = 0.0;
                for(int k=1;k<=n;++k) {
                    double K = k;
                    a7 += 1.0 / (K*K);
                }
                
                return a7;
            }
        });
        threads.submit(riemannZeta);
        
        FutureTask<Double> alternatingHarmonic = new FutureTask<Double>(
                new Callable<Double>() {
            public Double call() {
                
                final double [] ALT = {1.0, -1.0};
                double a8 = 0.0;
                int idx = 0;
                for(int k=1;k<=n;++k) {
                    a8 += ALT[idx] / k;
                    idx ^= 1;
                }
                
                return a8;
            }
        });
        threads.submit(alternatingHarmonic);

        FutureTask<Double> gregory = new FutureTask<Double>(
                new Callable<Double>() {
            public Double call() {
                
                final double [] ALT = {1.0, -1.0};
                double a9 = 0.0;
                int idx = 0;
                for(int k=1;k<=n;++k) {
                    a9 += ALT[idx] / (2.0 * k - 1.0);
                    idx ^= 1;
                }
                
                return a9;
            }
        });
        threads.submit(gregory);        

        System.out.printf("%.9f\t(2/3)^k\n", a1.get());
        System.out.printf("%.9f\tk^-0.5\n", a2.get());
        System.out.printf("%.9f\t1/k(k+1)\n", a3.get());
        System.out.printf("%.9f\tFlint Hills\n", flintHills.get());
        System.out.printf("%.9f\tCookson Hills\n", cooksonHills.get());
        System.out.printf("%.9f\tHarmonic\n", harmonic.get());
        System.out.printf("%.9f\tRiemann Zeta\n", riemannZeta.get());
        System.out.printf("%.9f\tAlternating Harmonic\n", alternatingHarmonic.get());
        System.out.printf("%.9f\tGregory\n", gregory.get());
 
    }
}
