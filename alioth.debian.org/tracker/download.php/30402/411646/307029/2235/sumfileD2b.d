// The Great Computer Language Shootout
//   http://shootout.alioth.debian.org/
//   http://www.bagley.org/~doug/shootout/
// Converted to D by Dave Fladebo
// Modified by bearophile
// Compile: dmd -O -inline -release sumcol.d

import std.c.stdio: printf;
import std.stdio: fgets, stdin;
import std.c.stdlib: atoi;

void main() {
    int sum;
    char[128] line;

    while(fgets(line.ptr, line.length, stdin))
        sum += atoi(line.ptr);

    printf("%d\n", sum);
}