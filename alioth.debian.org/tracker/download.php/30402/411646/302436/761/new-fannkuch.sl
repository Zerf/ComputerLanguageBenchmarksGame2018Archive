#!/usr/bin/env slsh
% The Computer Language Shootout Benchmarks
% http://shootout.alioth.debian.org/
%
% contributed by John E. Davis
private define gen_perms ();
private define gen_perms (n)
{
   if (n == 1)
     return _reshape ([1], [1,1]);

   variable m = n-1;
   variable p1 = gen_perms (m);
   variable dims;
   dims = array_shape (p1);
   variable nr = dims[0];
   variable p = Char_Type[n*nr, n];
   variable ii = [0:nr-1];

   p[ii,0] = n;
   p[ii,[1:m]] = p1;
   _for (1,m,1)
     {
	variable i = ();
	ii += nr;
	p[ii,[0:i-1]] = p1[*,[0:i-1]];
	p[ii,i] = n;
	p[ii,[i+1:m]] = p1[*,[i:m-1]];
     }
   return p;
}

private define process_perm (perm, n);
private variable Mask;
private define process_perm (perm, match)
{
   variable depth = 0, depth1;
   _for (0, length(match)-1, 1)
     {
	variable i = ();
	i = match[i];
	variable perm1 = @perm;
	array_reverse (perm1, 0, i);
	variable match1 = where (perm1 == Mask);
	depth1 = 1;
	if (length (match1))
	  depth1 = 1 + process_perm (perm1, match1);
	if (depth1 > depth)
	  depth = depth1;
     }
   return depth;
}

private define fannkuch (n)
{
   variable perms = gen_perms (n);
   variable i = where ((perms[*,0] == 1) and (perms[*,-1] == n));
   perms = perms[i,*];
   Mask = typecast ([1:n], Char_Type);
   Mask[0] = 0;
   variable depths = 0;
   _for (0, length(i)-1, 1)
     {
	variable j = ();
	variable perms1 = perms[j,*];
	variable match = where (perms1 == Mask);
	!if (length (match))
	  continue;
	variable d = process_perm (perms1, match);
	if (d > depths)
	  depths = d;
     }
   return depths;
}

public define slsh_main ()
{
   variable n = 1;
   if (__argc > 1) n = integer (__argv[1]);
   () = fprintf (stdout, "Pfannkuchen(%d) = %ld\n", n, fannkuch (n));
}

