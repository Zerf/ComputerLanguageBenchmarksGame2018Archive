# The Great Computer Language Shootout
# http://shootout.alioth.debian.org/
# nsieve benchmark for Psyco
# Optimized for Psyco from the Free Pascal version by bearophile, Jan 1 2006

import sys, psyco

def nSieve(n):
    flags = [1] * (n + 1)
    n1 = n + 1
    count = 0
    for i in xrange(2, n1):
        if flags[i]:
            k = i << 1
            while k < n1:
                if flags[k]: flags[k] = 0
                k += i
            count += 1
    print "Primes up to %8u %8u" % (m, count)

psyco.bind(nSieve)
m = int( (sys.argv+[2])[1] )
for i in 0, 1, 2:
    nSieve(10000 << (m-i))