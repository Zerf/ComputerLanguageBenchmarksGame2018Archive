'From VisualWorks® NonCommercial, 7.4.1 of May 30, 2006 on January 14, 2008 at 12:10:47 am'!


Smalltalk defineClass: #ChameneosColour
	superclass: #{Core.Object}
	indexedType: #none
	private: false
	instanceVariableNames: 'color '
	classInstanceVariableNames: 'red yellow blue '
	imports: ''
	category: 'chameleon'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!ChameneosColour class methodsFor: 'as yet unclassified'!

createBlue
	"comment stating purpose of message"

	| |
^(super new) color: #blue.!

createRed
	"comment stating purpose of message"

	| |
^(super new) color: #red.!

createYellow
	"comment stating purpose of message"

	| |
^(super new) color: #yellow.!

generateReportOfColours
	| readOut colours |
	colours:=Array with: blue with: red with: yellow.
	readOut := WriteStream on: String new.

	colours do:[:aColour| colours do:[:anotherColour| aColour printOn: readOut.
		readOut nextPutAll: ' + '.
		   anotherColour printOn: readOut.
		 readOut nextPutAll: ' -> '.
		(aColour complementaryColourFor: anotherColour) printOn: readOut.
		readOut cr]].
^readOut.!

initialize
"self initialize"
red:=self createRed.
blue:=self createBlue.
yellow:=self createYellow.! !

!ChameneosColour class methodsFor: 'accessing'!

blue
	^blue!

red
	^red!

yellow
	^yellow! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!ChameneosColour methodsFor: 'as yet unclassified'!

color
^color!

color: aColor
color:=aColor .!

complementaryColourFor: aChameneosColour
	"determine the complementary colour defined as..."
(self==aChameneosColour) ifTrue:[^self].
self isBlue ifTrue: [aChameneosColour isRed ifTrue: [^self class yellow] ifFalse: [^self class red.]].
self isRed ifTrue: [aChameneosColour isBlue ifTrue: [^self class yellow] ifFalse: [^self class blue.]].
aChameneosColour isBlue ifTrue: [^self class red] ifFalse: [^self class blue.].!

isBlue

^self==self class blue.!

isRed

^self==self class red.!

isYellow

^self==self class yellow.!

printOn: aStream
aStream nextPutAll: self color.! !

#{ChameneosColour} initialize!

Smalltalk defineClass: #Creature
	superclass: #{Core.Object}
	indexedType: #none
	private: false
	instanceVariableNames: 'creatureName colour selfMet creaturesMet '
	classInstanceVariableNames: ''
	imports: ''
	category: 'chameleon'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Creature class methodsFor: 'as yet unclassified'!

withName: aName colour: aColour
|creature|
creature:=self new initialize.
creature name: aName.
creature colour: aColour .
^creature.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Creature methodsFor: 'as yet unclassified'!

initialize
selfMet:=0.
creaturesMet :=0.!

visitMall: mall 
		[	| partner | 
		partner := mall visitWith: self.
		partner ifNotNil: 
			[ colour:= (colour complementaryColourFor: partner colour).
			self == partner
				ifTrue: [ selfMet := selfMet + 1 ].
				creaturesMet := creaturesMet + 1.
			 ] . 
	Processor yield.
		partner isNil. ] whileFalse! !

!Creature methodsFor: 'accessing'!

colour
	^ colour!

colour: anObject
	colour := anObject!

creaturesMet
	^ creaturesMet!

name
	^ creatureName !

name: anObject
	creatureName := anObject!

selfMet
	^ selfMet! !

Smalltalk defineClass: #Mall
	superclass: #{Core.Object}
	indexedType: #none
	private: false
	instanceVariableNames: 'mustWait firstCall guard secondCreature firstCreature maxRendezvous bouncer '
	classInstanceVariableNames: ''
	imports: ''
	category: 'chameleon'!

Smalltalk.Mall defineSharedVariable: #Units
	private: false
	constant: false
	category: 'As yet unclassified'
	initializer: nil!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Mall class methodsFor: 'as yet unclassified'!

createCreaturesWith: aCollectionOfColours
	| aName |
	aName:=0.
	^ aCollectionOfColours collect: 
		[ : aColour | 
		aName := aName + 1.
		Creature 
			withName: aName
			colour: aColour ].
!

generateReportFor: creatures 
	| sum readOut |
	readOut := WriteStream on: String new.
	creatures do: 
		[ : aCreature | 

			 aCreature creaturesMet printOn: readOut.
					readOut space.
aCreature selfMet printOn: readOut.
			readOut cr.
			].
	sum := creatures 
		inject: 0
		into: [ : accum : each | accum + each creaturesMet ].
		readOut space.
	sum printString do: 
		[ : el | 
		readOut
			nextPutAll: (self units at: el digitValue + 1) ;
			space ].
	^ readOut!

generateReportForColours: colours
	| readOut |
	readOut := WriteStream on: String new.
colours do: 
		[ : colour | 
			colour printOn: readOut.
			readOut space ].
	^ readOut!

initialize
"self initialize"
Units := #(
		'zero'
		'one'
		'two'
		'three'
		'four'
		'five'
		'six'
		'seven'
		'eight'
		'nine'
	).!

new
	^super new initialize.!

openMall: aMall forCreatures: creatures usingGuard: sema 
	creatures do: 
		[ : aCreature | 
		[ aCreature visitMall: aMall.
		sema signal ]  fork].!

openMallWith: aCollectionOfColours forNumberOfMeets: aNumber 
	| mall creatures guard readOut |
	readOut := WriteStream on: String new.
	guard := Semaphore new.
	mall := Mall new.
	mall maxRendezvous: aNumber.
readOut nextPutAll: (self generateReportForColours: aCollectionOfColours) contents; cr .
		
	creatures := self createCreaturesWith: aCollectionOfColours.
	self openMall: mall forCreatures:  creatures usingGuard: guard.
	self waitForClosingOfMall: mall withCreatures:  creatures usingGuard: guard.
	
readOut nextPutAll:  (self generateReportFor: creatures) contents ;
		cr.
^readOut.!

performTest
self runBenchMark: Tests arg on: Tests stdout.
^''!

runBenchMark: number on: anOutputStream
|firstTestColours secondTestColours|

anOutputStream nextPutAll: (ChameneosColour generateReportOfColours contents); cr.

firstTestColours :=Array with: ChameneosColour  createBlue with: ChameneosColour createRed  with: ChameneosColour createYellow.

secondTestColours :=(OrderedCollection new) 
add: ChameneosColour  createBlue; add: ChameneosColour createRed ; add: ChameneosColour createYellow; add:ChameneosColour createRed ; add:ChameneosColour createYellow; add:ChameneosColour createBlue; add:ChameneosColour createRed; add:ChameneosColour createYellow; add:ChameneosColour createRed; add:ChameneosColour createBlue; yourself.

anOutputStream nextPutAll: (Mall openMallWith: firstTestColours  forNumberOfMeets: number) contents.
anOutputStream cr.
anOutputStream nextPutAll: (Mall openMallWith: secondTestColours  forNumberOfMeets: number) contents.
anOutputStream flush.
!

units

^Units.!

waitForClosingOfMall: aMall withCreatures: creatures usingGuard: guard
	creatures size timesRepeat: [ guard wait ].
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Mall methodsFor: 'as yet unclassified'!

allowedMoreMeetings
	^ maxRendezvous > 0!

completeRendezvousOf: aChameneos 
	mustWait := false.
	self rendezvousOccurred.
	 ^secondCreature!

initialize
	mustWait := false.
	firstCall := true.
	bouncer:= Semaphore new.
	guard := Semaphore forMutualExclusion.
	firstCreature := nil.
	secondCreature := nil!

meetSomeoneWith: aChameneos 
 	secondCreature := aChameneos.
	firstCall := true.
	mustWait := true.
	^firstCreature.!

mustWait 
[guard critical: [mustWait and: [ self allowedMoreMeetings ]]]  whileTrue: [ self signal. bouncer wait]!

rendezvousOccurred
	maxRendezvous := maxRendezvous - 1!

signal
bouncer signal. Processor yield.
!

visitWith: aChameneos 
|partner|
		self mustWait.
		partner:=(guard critical: [firstCall])
			ifTrue: 
				[ self waitForSomeoneElseWith: aChameneos.
				guard critical: [self completeRendezvousOf: aChameneos]]
			ifFalse: 
				[guard critical: [self meetSomeoneWith: aChameneos]].
bouncer signal.
^guard critical: [maxRendezvous >= 0
			ifTrue: [ partner ]
			ifFalse: [ nil ].
].!

waitForSomeoneElseWith: aChameneos 
guard critical: [firstCreature := aChameneos.
	firstCall := false.].
[guard critical: [ firstCall not and: [ self allowedMoreMeetings ] ]] whileTrue: [self signal. bouncer wait]! !

!Mall methodsFor: 'accessing'!

maxRendezvous: max
	 maxRendezvous:=max.! !

#{Mall} initialize!
