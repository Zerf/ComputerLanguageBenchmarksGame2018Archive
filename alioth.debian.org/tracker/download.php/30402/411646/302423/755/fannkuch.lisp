;;; The Great Computer Language Shootout
;;; http://shootout.alioth.debian.org/
;;;
;;; transcribed from Christophe Troestler's OCaml version by Juho Snellman

(defun fannkuch (n)
  (declare (fixnum n))
  (let* ((p (make-array n
                        :element-type 'fixnum
                        :initial-contents (loop for i below n collect i)))
         (p2 (make-array n :element-type 'fixnum))
         (count (make-array n
                            :element-type 'fixnum
                            :initial-contents (loop for i from 1 to n
                                                    collect i)))
         (m (1- n)))
    (labels ((count-flips (p)
               (loop for k = (aref p 0)
                     until (zerop k)
                     do (dotimes (i (ceiling k 2))
                          (rotatef (aref p i)
                                   (aref p (- k i))))
                     count t))
             (aux (r acc)
               (declare (fixnum acc))
               (cond ((= r n)
                      acc)
                     (t
                      (let ((p/0 (aref p 0)))
                        (dotimes (i r)
                          (setf (aref p i)
                                (aref p (1+ i))))
                        (setf (aref p r) p/0))
                      (decf (aref count r))
                      (cond ((plusp (aref count r))
                            (dotimes (i r)
                              (setf (aref count i) (1+ i)))
                             (cond ((and (/= (aref p 0) 0)
                                         (/= (aref p m) m))
                                    (replace p2 p)
                                    (aux 1 (max (count-flips p2) acc)))
                                   (t
                                    (aux 1 acc))))
                            (t
                             (aux (1+ r) acc)))))))
      (aux 1 0))))

(defun main ()
  (let* ((args #+sbcl sb-ext:*posix-argv*
               #+cmu extensions:*command-line-strings*
               #+gcl si::*command-args*)
         (n (parse-integer (car (last args)))))
    (format t "Pfannkuchen(~d) = ~d~%" n (fannkuch n))))
