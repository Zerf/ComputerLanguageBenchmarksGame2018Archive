
/**
 * @author Graham Miller
 */
public class message {
    public static final int numberOfThreads = 500;

    public static int numberOfMessagesToSend;

	public static void main(String args[]) {
        numberOfMessagesToSend = Integer.parseInt(args[0]);

        RingBufferThread chain = null;
		for (int i = 0; i < numberOfThreads; i++) {
            chain = new RingBufferThread(chain, numberOfMessagesToSend*(numberOfThreads));
            chain.start();
       }

        for (int i = 0; i < numberOfMessagesToSend; i++) {
        	chain.enqueue(0);
        }
        chain.signalDoneSendingMessages();
    }

	public static class RingBufferThread extends Thread {
		private static final int RING_BUFFER_START = 0;
		private static final int RING_BUFFER_END = 50;
		private static final int RING_BUFFER_CAPACITY = RING_BUFFER_END-RING_BUFFER_START-1;

		private final int [] ringBuffer = new int[RING_BUFFER_CAPACITY+1];
		
		RingBufferThread nextThread;
		private volatile int loadIndex = 0;
		private volatile int consumeIndex = 0;
		private volatile boolean done = false;
		private final int finalSum;
		
		RingBufferThread(RingBufferThread nextThread, int finalSum) {
			this.nextThread = nextThread;
			this.finalSum = finalSum;
		}

		public void run() {
			if (nextThread != null) {
				while (!done || size() > 0) {
					nextThread.enqueue(dequeue());
				}
				nextThread.signalDoneSendingMessages();
			} else {
				int sum = 0;
				while (sum < finalSum) {
					int message = dequeue();
					sum += message;
				}
				System.out.println(sum);
				System.exit(0);
			}
		}


		/**
		 * @param message
		 */
		public final void enqueue(int message) {
			int stackLoadIndex;
			int stackConsumeIndex;
			do {
				stackLoadIndex = loadIndex;
				stackConsumeIndex = consumeIndex;
				// after this test becomes false, and the loop exits
				// an increase in consumeIndex cannot make it true again
				// that is once we have some free space, we will always
				// have free space until the thread calling this method
				// adds an element.
			} while (size(stackLoadIndex, stackConsumeIndex) == RING_BUFFER_CAPACITY && trueYield());
			
			// at this point, we know that there is space
			// for at least one element in the ring buffer
			// so we can always increment/wrap loadIndex
			ringBuffer[stackLoadIndex++] = message;
			if (stackLoadIndex >= RING_BUFFER_END) {
				stackLoadIndex = RING_BUFFER_START;
			}
			loadIndex = stackLoadIndex;
		}

		public final int dequeue() {
//			 ONE_THREAD_ONLY
//			if (dequeueThread == null){
//				dequeueThread = Thread.currentThread();
//			} else {
//				assert(dequeueThread == Thread.currentThread());
//			}
			int stackLoadIndex;
			int stackConsumeIndex;

			do {
				stackLoadIndex = loadIndex;
				stackConsumeIndex = consumeIndex;
				// after this test becomes false, and the loop exits
				// an increase in loadIndex cannot make it true again
				// that is once we have at least one element, we will always
				// have at least one element until the thread calling this
				// method removes one.
			} while (size(stackLoadIndex, stackConsumeIndex) <= 0 && trueYield());

			// at this point, we know that there is at least one
			// element in the ring buffer, so it is always safe
			// to increment/wrap consumeIndex
			int message = 1 + ringBuffer[stackConsumeIndex++];
 			if (stackConsumeIndex >= RING_BUFFER_END) {
				stackConsumeIndex = RING_BUFFER_START;
			}
 			consumeIndex = stackConsumeIndex;
 			return message;
		}

		public final int size() {
			return size(loadIndex, consumeIndex);
		}
		
		private int size(int stackLoadIndex, int stackConsumeIndex) {
			int maxConsumeBytes;
			if (stackLoadIndex >= stackConsumeIndex) {
				maxConsumeBytes = stackLoadIndex - stackConsumeIndex;
			} else {
				maxConsumeBytes = RING_BUFFER_END - (stackConsumeIndex - stackLoadIndex);
			}
			return maxConsumeBytes;
		}

		public final void signalDoneSendingMessages() {
			// once done is true, I am the only
			// thread accessing any of my variables, so we have no
			// more threading issues
			done = true;
		}
		
		private final boolean trueYield() {
			Thread.yield();
			return true;
		}
	}
}
