(* The Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Johannes Weißl
*)

let rec ack x y = match x, y with
  | 0, y -> y + 1
  | y, 0 -> ack (x - 1) 1
  | x, y -> ack (x - 1) (ack x (y - 1))

let rec fib n = match n with
  | 0 | 1 -> 1
  | n -> fib (n - 2) + fib (n - 1)

let rec fib_float n = match n with
  | 0. | 1. -> 1.
  | n -> fib_float (n -. 2.) +. fib_float (n -. 1.)

let rec tak x y z =
  if y < x then
      tak (tak (x -. 1.0) y z) (tak (y -. 1.0) z x) (tak (z -. 1.0) x y)
  else
      z

let rec tak_int x y z =
  if y < x then
      tak_int (tak_int (x - 1) y z) (tak_int (y - 1) z x) (tak_int (z - 1) x y)
  else
      z
;;

if Array.length Sys.argv > 1 then
  let a = int_of_string Sys.argv.(1) in
  let b = a - 1 in
  let c = 28.0 +. float_of_string Sys.argv.(1) in
  Printf.printf "Ack(3,%d): %d\n" a (ack 3 a) ;
  Printf.printf "Fib(%.1f): %.1f\n" c (fib_float c) ;
  Printf.printf "Tak(%d,%d,%d): %d\n" (3*b) (2*b) b (tak_int (3*b) (2*b) b);
  Printf.printf "Fib(3): %d\n" (fib 3) ;
  Printf.printf "Tak(3.0,2.0,1.0): %.1f\n" (tak 3.0 2.0 1.0)
