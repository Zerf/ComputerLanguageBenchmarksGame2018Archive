! The Great Computer Language Shootout
!   http://shootout.alioth.debian.org/
!
!   Contributed by Steve Decker
!   Version 2
!   Revised to be more naive, but using multiple do loops in the final series
!   as in C #2.
!
! compilation:
!   g95 -O3 -fomit-frame-pointer -funroll-loops partial2.f90
!   gfortran -O3 -fomit-frame-pointer -funroll-loops -ffast-math partial2.f90
!   ifort -O3 -ipo -static partial2.f90

program partial
  implicit none

  integer,   parameter :: dp = selected_real_kind(10)
  real(dp),  parameter :: TwoThirds = 2._dp/3._dp, Zero = 0._dp, One = 1._dp
  character, parameter :: Tab = achar(9)

  real(dp)         :: s = Zero
  integer          :: n, k
  character(len=8) :: argv

  call get_command_argument(1, argv)
  read (argv, "(i8)") n

  do k = 0, n  ;  s = s + TwoThirds**k  ;  end do
  write (*, "(f0.9,a)") s, Tab // "(2/3)^k"

  s = Zero
  ! 1/sqrt(k) = k^-0.5, as in C #2.
  do k = 1, n  ;  s = s + One / sqrt(real(k,dp))  ;  end do
  write (*, "(f0.9,a)") s, Tab // "k^-0.5"

  s = Zero
  do k = 1, n  ;  s = s + One / (k * (k + One))  ;  end do
  write (*, "(f11.9,a)") s, Tab // "1/k(k+1)"

  s = Zero
  do k = 1, n  ;  s = s + One / (real(k,dp)**3 * sin(real(k,dp))**2)  ;  end do
  write (*, "(f0.9,a)") s, Tab // "Flint Hills"

  s = Zero
  do k = 1, n  ;  s = s + One / (real(k,dp)**3 * cos(real(k,dp))**2)  ;  end do
  write (*, "(f0.9,a)") s, Tab // "Cookson Hills"

  s = Zero
  do k = 1, n  ;  s = s + One / k  ;  end do
  write (*, "(f0.9,a)") s, Tab // "Harmonic"

  s = Zero
  do k = 1, n  ;  s = s + One / (real(k,dp)**2)  ;  end do
  write (*, "(f0.9,a)") s, Tab // "Riemann Zeta"

  s = Zero
  do k = 1, n-1, 2  ;  s = s + One / k  ;  end do
  do k = 2, n,   2  ;  s = s - One / k  ;  end do
  write (*, "(f11.9,a)") s, Tab // "Alternating Harmonic"

  s = Zero
  do k = 1, 2*n-1, 4  ;  s = s + One / k  ;  end do
  do k = 3, 2*n,   4  ;  s = s - One / k  ;  end do
  write (*, "(f11.9,a)") s, Tab // "Gregory"  
end program partial
