# The Computer Language Shootout
# http://shootout.alioth.debian.org/
#
# contributed by Josh Goldfoot
# modified by Mike Klaas and Dani Nanz


from sys import argv
from math import sin as _sin, cos as _cos

def doit():
    n = (len(argv) > 1) and int(argv[1]) or 25000
    
    sum0 = alt = 1.0
    sum1 = sum2 = sum3 = sum4 = sum5 = sum6 = sum7 = sum8 = 0.0
    twoThirds = 2.0 / 3.0

    for k in xrange(1, n + 1):
        k = float(k)
        ki = 1.0 / k
        ks, kc = _sin(k), _cos(k)
        sum0 += twoThirds ** k
        sum1 += ki ** 0.5
        sum2 += ki / (k + 1.0)
        sum3 += ((ki / ks) * (ki / ks)) / k
        sum4 += ((ki / kc) * (ki / kc)) / k
        sum5 += ki
        sum6 += ki / k
        sum7 += alt / k
        sum8 += alt / ((k + k) - 1.0)
        alt = -alt

    print ('%0.9f\t(2/3)^k' + \
           '\n%0.9f\tk^-0.5' + \
           '\n%0.9f\t1/k(k+1)' + \
           '\n%0.9f\tFlint Hills'  + \
           '\n%0.9f\tCookson Hills' + \
           '\n%0.9f\tHarmonic' + \
           '\n%0.9f\tRiemann Zeta' + \
           '\n%0.9f\tAlternating Harmonic' + \
           '\n%0.9f\tGregory') % \
           (sum0, sum1, sum2, sum3, sum4, sum5, sum6, sum7, sum8) 

doit()
