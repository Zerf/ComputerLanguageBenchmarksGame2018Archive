/* start2.c
 * 2008-03-29, Joe Tucek
 * Clearly printf is worse than fputs, which is worse than write...
 */

#include <unistd.h>

int main() {
  write(1, "hello world\n", 12);
  return(0);
}
