/* -*- mode: c -*-
 *
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Sebastien Loisel
 * Optimized by Jason Garrett-Glaser
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double *LUT1;
double *LUT2;
double inline eval_A(int i, int j)
{
    return 1.0/((i+j)*(i+j+1)/2+i+1);
}

void inline generateLUT(int N)
{
    int i,j;
    
    LUT1 = malloc(N*N*sizeof(double));
    LUT2 = malloc(N*N*sizeof(double));
    
    for(i=0;i<N;i++)
        for(j=0;j<N;j++)
        {
            LUT1[i*N+j]=LUT2[j*N+i]=eval_A(i,j);
        }
}

void inline eval_A_times_u(int N, const double u[], double Au[])
{
    int i,j;
    for(i=0;i<N;i++)
    {
        Au[i]=0;
        for(j=0;j<N-8;j+=8)
        {
            Au[i]+=LUT1[i*N+j+0]*u[j+0];
            Au[i]+=LUT1[i*N+j+1]*u[j+1];
            Au[i]+=LUT1[i*N+j+2]*u[j+2];
            Au[i]+=LUT1[i*N+j+3]*u[j+3];
            Au[i]+=LUT1[i*N+j+4]*u[j+4];
            Au[i]+=LUT1[i*N+j+5]*u[j+5];
            Au[i]+=LUT1[i*N+j+6]*u[j+6];
            Au[i]+=LUT1[i*N+j+7]*u[j+7];
        }
        for(;j<N;j++)
            Au[i]+=LUT1[i*N+j]*u[j];
    }
}

void inline eval_At_times_u(int N, const double u[], double Au[])
{
    int i,j;
    for(i=0;i<N;i++)
    {
        Au[i]=0;
        for(j=0;j<N-8;j+=8)
        {
            Au[i]+=LUT2[i*N+j+0]*u[j+0];
            Au[i]+=LUT2[i*N+j+1]*u[j+1];
            Au[i]+=LUT2[i*N+j+2]*u[j+2];
            Au[i]+=LUT2[i*N+j+3]*u[j+3];
            Au[i]+=LUT2[i*N+j+4]*u[j+4];
            Au[i]+=LUT2[i*N+j+5]*u[j+5];
            Au[i]+=LUT2[i*N+j+6]*u[j+6];
            Au[i]+=LUT2[i*N+j+7]*u[j+7];
        }
        for(;j<N;j++)
            Au[i]+=LUT2[i*N+j]*u[j];
    }
}

void inline eval_AtA_times_u(int N, const double u[], double AtAu[])
{
    double v[N];
    eval_A_times_u(N,u,v);
    eval_At_times_u(N,v,AtAu);
}

int main(int argc, char *argv[])
{
    int i;
    int N = ((argc == 2) ? atoi(argv[1]) : 2000);
    double u[N],v[N],vBv,vv;

    generateLUT(N);

    for(i=0;i<N;i++)
        u[i]=1;

    for(i=0;i<10;i++)
    {
        eval_AtA_times_u(N,u,v);
        eval_AtA_times_u(N,v,u);
    }

    vBv=vv=0;
    for(i=0;i<N;i++) 
    {
        vBv+=u[i]*v[i];
        vv+=v[i]*v[i];
    }

    printf("%0.9f\n",sqrt(vBv/vv));
    return 0;
}

