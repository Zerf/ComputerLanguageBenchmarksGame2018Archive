(* -*- mode: sml -*-
 *
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * A concise version by Vesa Karvonen.
 *)

fun ack m n = if 0=m then n+1 else ack (m-1) (if 0=n then 1 else ack m (n-1))
val n = valOf (Int.fromString (hd (CommandLine.arguments ()))) handle _ => 1
val _ = app print ["Ack(3,",Int.toString n,"): ",Int.toString (ack 3 n),"\n"]
