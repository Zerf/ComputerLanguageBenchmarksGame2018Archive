option byval
function eval_A(i as integer, j as integer) as double            '50%
   
   return 1.0/((i+j)*(i+j+1)/2.0+i+1.0)
end function

sub eval_A_times_u(N as integer, u() as double, Au() as double)  '50%
   for i = 0 to N - 1
      Au(i) = 0
      for j = 0 to N - 1
         Au(i) += eval_A(i,j)*u(j)
      next j
   next i
end sub

sub eval_At_times_u(N as integer, u() as double, Au() as double) '49%
   for i = 0 to N-1
      Au(i) = 0
      for j = 0 to N-1
         Au(i) += eval_A(j,i) * u(j)
      next j
   next i
end sub

sub eval_AtA_times_u(N as integer, u() as double, AtAu() as double)
   dim v(N) as double
   eval_A_times_u(N,u(),v())
   eval_At_times_u(N,v(),AtAu() )
end sub

N = valint(COMMAND$)
if N < 1 then N = 100
dim as double u(N), v(N), vBv, vv
for i = 0 to N-1
   u(i) = 1
next i
for i = 0 to 9
   eval_AtA_times_u(N,u(),v() )
   eval_AtA_times_u(N,v(),u() )
next i
vBv=0 : vv=0
for i = 0 to N - 1
   vBv += u(i)*v(i) : vv += v(i)*v(i)
next i

dim result as double
result = sqr(vBv/vv)
REM this is necessary because "print using" rounds poorly
dim factor as longint
factor = 1000000000
result = clngint(result * factor) / factor
print str(result)