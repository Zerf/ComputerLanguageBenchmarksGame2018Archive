/* 
	The Computer Language
	Shootout Benchmarks

	http://shootout.alioth.debian.org/

	Contributed by Lester Vecsey
*/

#include <iostream>

#include <string>
#include <fstream>
using namespace std;

const int line_length = 60;

const long IM = 139968, IA = 3877, IC = 29573;

class random {

	int im, ia, ic;
	long last;

	public:

	random(int m, int a, int c) : im(m), ia(a), ic(c) { last = 42; };
	virtual double gen (double max) { last*=ia; last+=ic; last%=im; return max * last / im; }

	};

struct aminoacids {

	char c;
	double p;

	};

enum { REPEAT, RANDOM };

class aacids {

	random *R;

	int num_genes;

	struct aminoacids *genelist, *pga;

	string pick, id, desc, a;

	public:

	aacids(char *i, char *d, random *r, int n, struct aminoacids *g) : 
		R(r), num_genes(n), genelist(g) {
			assign(i, d);
			pga = g + n; init(); 
			}

	aacids(char *i, char *d, string a) : a(a) { assign(i, d); init(); }
	virtual ~aacids() { }
	
	virtual void assign(char *i, char *d) { id.append(i); desc.append(d); }
	virtual void aacids::display() { cout << '>' << id << ' ' << desc << '\n'; }

	virtual void init() { pick.resize(line_length+1); makeCumulative(); }

	virtual void run(int f, int todo);

	virtual void makeRepeatFasta (int n);
	virtual void makeRandomFasta (int todo);
	virtual void makeCumulative();
	virtual char selectRandom();

	};

void aacids::run(int f, int todo) {

	display();

	switch (f) {
		case REPEAT: makeRepeatFasta(todo); break;
		case RANDOM: makeRandomFasta(todo); break;
		};

	}

void aacids::makeCumulative() {

	struct aminoacids *p = genelist;

	for (double cp = 0; p < pga; p++) {
		p->p = cp += p->p; 
		}
		
	}

char aacids::selectRandom() {

	double rnd = R->gen(1.0);

	aminoacids *cur, *hi, *lo = genelist;

	if (rnd < lo->p) return lo->c;

	for (hi = pga - 1; hi > lo+1; ) {
		cur = lo + ((hi - lo) >> 1);
		if (rnd < cur->p) hi = cur; else lo = cur;
		}

	return hi->c;

	}

void aacids::makeRandomFasta (int todo) {

	for ( ; todo > 0 ; todo-=line_length) {
	
		int cur_line_len = (todo < line_length) ? todo : line_length;

		for (int count = 0; count < cur_line_len; count++) pick[count] = selectRandom();

		cout << pick.substr(0, cur_line_len) << '\n';

		}

	}

void aacids::makeRepeatFasta (int n) {

	int m, k = 0, finality_interval, a_len = a.length();

	for( ; n > 0; n-=line_length, k += m) {

		m = n > line_length ? line_length : n;

		finality_interval = a_len - k;

		if (m > finality_interval) {
			cout << a.substr(k, finality_interval);
			m-=finality_interval;
			k = 0;
			}

		cout << a.substr(k, m) << '\n';

		}

	}

struct aminoacids iub[] = {
    { 'a', 0.27 },
    { 'c', 0.12 },
    { 'g', 0.12 },
    { 't', 0.27 },

    { 'B', 0.02 },
    { 'D', 0.02 },
    { 'H', 0.02 },
    { 'K', 0.02 },
    { 'M', 0.02 },
    { 'N', 0.02 },
    { 'R', 0.02 },
    { 'S', 0.02 },
    { 'V', 0.02 },
    { 'W', 0.02 },
    { 'Y', 0.02 }
};

const int IUB_LEN = (sizeof (iub) / sizeof (struct aminoacids));

struct aminoacids homosapiens[] = {
    { 'a', 0.3029549426680 },
    { 'c', 0.1979883004921 },
    { 'g', 0.1975473066391 },
    { 't', 0.3015094502008 },
};

const int HOMOSAPIENS_LEN = (sizeof (homosapiens) / sizeof (struct aminoacids));

string alu(
	"GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG" \
	"GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA" \
	"CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT" \
	"ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA" \
	"GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG" \
	"AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC" \
	"AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA"
	);

int main(int argc, char *argv[]) {

	random r(IM, IA, IC);

	aacids rep("ONE", "Homo sapiens alu", alu),
		i("TWO", "IUB ambiguity codes", &r, IUB_LEN, iub), 
		h("THREE", "Homo sapiens frequency", &r, HOMOSAPIENS_LEN, homosapiens);
		
	int n = (argc>1) ? strtol(argv[1], 0, 10) : 1000;

	std::ios_base::sync_with_stdio(0);

	rep.run( REPEAT, (n*2));
	i.run( RANDOM, (n*3));
	h.run( RANDOM, (n*5));

	return 0;

	}
