#-- The Computer Language Shootout
#-- http://shootout.alioth.debian.org/
#-- contributed by Mike Pall, Tobias Polzin

import sys

def create_coroutine(n):
    if n > 1: 
        coroutine = create_coroutine(n-1)
        while 1:
            i = coroutine.next()
            yield i+1
    else:
        while 1:
            yield 1

def main():
    N = int( sys.argv[1] )
    coroutine = create_coroutine( 500 )
    count = 0
    for i in xrange( N ):
        count += coroutine.next()
    print count

main()
