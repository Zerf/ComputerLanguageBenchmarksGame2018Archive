{ The Great Computer Language Shootout
  http://shootout.alioth.debian.org

  contributed by Ian Osgood
}

program fannkuch;
uses Math, SysUtils;

var maxFlips : integer;
    a : array of integer;

procedure exch(var a,b : integer); inline;
var tmp : integer;
begin
  tmp := a; a := b; b := tmp;
end;

procedure flop(p : array of integer);
var flips,k,i : integer;
begin
  flips := 0;
  while p[0] > 1 do
  begin
    k := p[0]-1;
    for i := 0 to (k-1) div 2 do
      exch(p[i], p[k-i]);
    flips := flips + 1;
  end;
  maxFlips := max(flips, maxFlips);
end;

procedure roll(k : integer); inline;
var tmp,i : integer;
begin
  tmp := a[0];
  for i := 1 to k do a[i-1] := a[i];
  a[k] := tmp;
end;

procedure permute(depth : integer);
var i : integer;
begin
  if depth = low(a) then flop(a) else
    for i := low(a) to depth do
    begin
      permute(depth-1);
      roll(depth);
    end;
end;

var n,i : integer;
begin
  n := StrToInt(paramstr(1));
  SetLength(a,n);
  for i := 1 to n do a[i-1] := i;
  maxFlips := 0;
  permute(high(a));
  writeln('Pfannkuchen(',n,') = ',maxFlips);
end.
