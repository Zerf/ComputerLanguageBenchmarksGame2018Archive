;;; The Computer Language Benchmarks Game
;;; http://shootout.alioth.debian.org/
;;;
;;; Contributed by: Witali Kusnezow

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require :cl-ppcre)

#+sb-thread
(progn
  (define-alien-routine sysconf long (name int))
  (use-package  :sb-thread)))

(eval-when (:compile-toplevel)
(setf cl-ppcre:*regex-char-code-limit* 128))

(defconstant  +regex-list+
  '("agggtaaa|tttaccct"
    "[cgt]gggtaaa|tttaccc[acg]"
    "a[act]ggtaaa|tttacc[agt]t"
    "ag[act]gtaaa|tttac[agt]ct"
    "agg[act]taaa|ttta[agt]cct"
    "aggg[acg]aaa|ttt[cgt]ccct"
    "agggt[cgt]aa|tt[acg]accct"
    "agggta[cgt]a|t[acg]taccct"
    "agggtaa[cgt]|[acg]ttaccct"))

(defconstant  +alternatives+
  '(("B" "(c|g|t)")  ("D" "(a|g|t)")
    ("H" "(a|c|t)")  ("K" "(g|t)")
    ("M" "(a|c)")    ("N" "(a|c|g|t)")
    ("R" "(a|g)")    ("S" "(c|t)")
    ("V" "(a|c|g)")  ("W" "(a|t)")
    ("Y" "(c|t)")))

#+sb-thread
(progn
  (defconstant  +cpu-count+ (sysconf 84))
  (defmacro bg  (&body body) `(make-thread (lambda () ,@body)))
  (defparameter *semaphores* (loop repeat (length +regex-list+) collect (make-semaphore)))
  (defparameter *semaphore* (make-semaphore)))


(defun join (strings &optional (size (reduce #'+ (mapcar #'length strings))))
  (declare (type fixnum size))
  (loop with res-string = (make-string size :element-type 'base-char)
     with i of-type fixnum = 0
     for str in strings
     do (setf (subseq res-string i) (the simple-base-string str))
     (incf i (length (the simple-base-string str)))
     finally (return res-string)))

(defun length-to-replace (match)
  (loop for x in match
     sum (- (the fixnum (cdr x))
            (the fixnum (car x))) of-type fixnum))

(defun replace-aux
    (match replacement target-string result-string
     &key (match-begin 0) (match-end -1)
     (match-length (length match))
     &aux
     (len (length replacement))
     (first-match (if (zerop match-begin) '(0 . 0) (nth (1- match-begin) match)))
     (target-start (cdr first-match))
     (result-start (+ (the fixnum (* len match-begin))
                    (- target-start
                       (the fixnum (length-to-replace (subseq match 0 match-begin)))))))
  (declare (type fixnum match-begin match-end match-length target-start result-start len)
           (type list match)
           (type simple-base-string result-string target-string)
           (type vector replacement))
  (loop with (i j) of-type fixnum = (list result-start target-start)
     with mmatch = (if (> match-begin match-end)
                       match (subseq match match-begin match-end))
     for pair in mmatch
     do (setf (subseq result-string i) (subseq target-string j (car pair))
              i (+ i (- (the fixnum (car pair)) j))
              (subseq result-string i) replacement
              j (cdr pair)
              i (+ i len))
     finally (if (or (minusp match-end) (<= match-length match-end))
                 (setf (subseq result-string i ) (subseq target-string j))))
  nil)

#+sb-thread
(defun parts
    (parts-num len
     &aux
     (ranges (loop with (step rest) of-type fixnum =  (multiple-value-list (floor len parts-num))
                with i of-type fixnum = 0 while (< i len)
                collect i into res of-type fixnum
                do (incf i step)(if (plusp rest) (progn (incf i) (decf rest)) )
                finally (return (append res (list len))))
             ))
  (declare (type fixnum len parts-num)
           (type list ranges))
  (mapcar #'cons ranges (subseq ranges 1)))

(defun replace-all
    (regexp replacement target-string
     &aux (rmatch '()) (match '())
     (result-string (make-string 0 :element-type 'base-char)))
  (declare (type simple-base-string result-string target-string)
           (type vector replacement))
  (cl-ppcre:do-scans
      (match-start match-end reg-starts reg-ends regexp target-string nil)
    (push (cons match-start match-end) rmatch))
  (if rmatch
      (progn
        (setf match (reverse rmatch)
              result-string (make-string
                             (+ (- (length target-string)
                                   (length-to-replace match))
                                (the fixnum (* (length replacement)
                                               (length match)))) :element-type 'base-char))
        #-sb-thread
        (replace-aux match replacement target-string result-string)
        #+sb-thread
        (mapcar #'join-thread
                (loop with len of-type fixnum = (length match)
                   for range in (parts +cpu-count+ len)
                   collect
                   (bg (replace-aux match replacement target-string result-string
                                    :match-begin (car range) :match-end (cdr range)
                                    :match-length len))))
        result-string)
      target-string))

(defun read-all
    (stream &aux (buf-size (* 1024 1024))
     (size 0)
     (buf-list
      (loop
         for buf = (make-string buf-size :element-type 'base-char)
         for len = (read-sequence buf stream)
         do (incf size len)
         collect (if (< len buf-size) (subseq buf 0 len) buf)
         while (= len buf-size))))
  (declare (type fixnum size))
  (join buf-list size))

(defun main (&optional (stream *standard-input*)
             &aux (sequence (read-all stream))
             (size (length sequence)))
  (declare (type simple-base-string sequence))
  (setf sequence (replace-all ">[^\\n]*\\n|\\n" "" sequence))

  #-sb-thread
  (loop for regex in +regex-list+ do
       (format t "~a ~a~%" regex
               (/ (length
                   (cl-ppcre:all-matches regex sequence :element-type 'base-char)) 2)))

  #+sb-thread
  (let* ((threads
          (loop for regex in +regex-list+
             for idx of-type fixnum by 1
             collect
             (bg
               (let ((reg regex) len)
                 (wait-on-semaphore (nth idx *semaphores*))
                 (setf len
                       (/ (length
                           (the list (cl-ppcre:all-matches reg sequence :element-type 'base-char))) 2))
                 (signal-semaphore *semaphore*)
                 (format nil "~a ~a" reg len))))))
    (loop with active of-type fixnum = 0
       for idx of-type fixnum by 1 repeat (length +regex-list+)
       if (= active +cpu-count+)
       do (wait-on-semaphore *semaphore*) (decf active)
       do (incf active) (signal-semaphore (nth idx *semaphores*)))
    (format t "~{~a~%~}" (mapcar #'join-thread threads)))

  (format t "~%~a~%~a~%" size (length sequence))

  (loop for pair in +alternatives+ do
       (setf sequence (replace-all  (car pair) (cadr pair) sequence )))
  (format t "~a~%" (length sequence)))
