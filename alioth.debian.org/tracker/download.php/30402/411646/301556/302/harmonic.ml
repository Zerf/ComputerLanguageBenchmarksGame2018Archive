(*
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * contributed by Will M. Farr
 * tweaked by Jon Harrop
 *)

let sum_harmonic4 n =
  let sum = ref 1.0 and ifloat = ref 2.0 in
  for i = 2 to n do
    sum := !sum +. 1.0 /. !ifloat;
    ifloat := !ifloat +. 1.0
  done;
  !sum;;

Printf.printf "%0.9f\n" (sum_harmonic4 (int_of_string (Sys.argv.(1))));;
