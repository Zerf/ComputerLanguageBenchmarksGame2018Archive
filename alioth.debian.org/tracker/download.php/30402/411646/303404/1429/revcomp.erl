%% The Computer Language Shootout
%% http://shootout.alioth.debian.org/
%%
%% contributed by Mats Cronqvist

-module(revcomp).

-export([main/1]).

main([_Args]) ->
    do(io:get_line(''),[]),
    halt().

do(eof,RevCompSeq) -> 
    %% end of file. flush last sequence
    flush(RevCompSeq);
do(">"++_=Descr,RevCompSeq) -> 
    %% description. flush sequence, write description, get next line
    flush(RevCompSeq),
    io:fwrite("~s",[Descr]),
    do(io:get_line(''),[]);
do(Str,RevCompSeq) -> 
    %% sequence data. reverse-complement it and get next line
    do(io:get_line(''),rev_comp(Str,RevCompSeq)).

%% take reverse-complemented sequence, insert line-feed char every
%% 60th byte, toss to stdout
flush([]) -> ok;
flush(RevComp) -> flush(RevComp,60,[],[]).

flush([],_,Lin,Seq) -> io:put_chars([Seq,10,lists:reverse(Lin),10]);
flush(RevComp,0,Lin,[]) -> flush(RevComp,60,[],[lists:reverse(Lin)]);
flush(RevComp,0,Lin,Seq) -> flush(RevComp,60,[],[Seq,10,lists:reverse(Lin)]);
flush([H|RevComp],N,Lin,Seq) -> flush(RevComp,N-1,[H|Lin],Seq).

%% reverse-complement sequence data. toss line-feeds
rev_comp([$A|T],A) -> rev_comp(T,[$T|A]);
rev_comp([$a|T],A) -> rev_comp(T,[$T|A]);
rev_comp([$C|T],A) -> rev_comp(T,[$G|A]);
rev_comp([$c|T],A) -> rev_comp(T,[$G|A]);
rev_comp([$G|T],A) -> rev_comp(T,[$C|A]);
rev_comp([$g|T],A) -> rev_comp(T,[$C|A]);
rev_comp([$T|T],A) -> rev_comp(T,[$A|A]);
rev_comp([$t|T],A) -> rev_comp(T,[$A|A]);
rev_comp([$U|T],A) -> rev_comp(T,[$A|A]);
rev_comp([$u|T],A) -> rev_comp(T,[$A|A]);
rev_comp([$M|T],A) -> rev_comp(T,[$K|A]);
rev_comp([$m|T],A) -> rev_comp(T,[$K|A]);
rev_comp([$R|T],A) -> rev_comp(T,[$Y|A]);
rev_comp([$r|T],A) -> rev_comp(T,[$Y|A]);
rev_comp([$W|T],A) -> rev_comp(T,[$W|A]);
rev_comp([$w|T],A) -> rev_comp(T,[$W|A]);
rev_comp([$S|T],A) -> rev_comp(T,[$S|A]);
rev_comp([$s|T],A) -> rev_comp(T,[$S|A]);
rev_comp([$Y|T],A) -> rev_comp(T,[$R|A]);
rev_comp([$y|T],A) -> rev_comp(T,[$R|A]);
rev_comp([$K|T],A) -> rev_comp(T,[$M|A]);
rev_comp([$k|T],A) -> rev_comp(T,[$M|A]);
rev_comp([$V|T],A) -> rev_comp(T,[$B|A]);
rev_comp([$v|T],A) -> rev_comp(T,[$B|A]);
rev_comp([$H|T],A) -> rev_comp(T,[$D|A]);
rev_comp([$h|T],A) -> rev_comp(T,[$D|A]);
rev_comp([$D|T],A) -> rev_comp(T,[$H|A]);
rev_comp([$d|T],A) -> rev_comp(T,[$H|A]);
rev_comp([$B|T],A) -> rev_comp(T,[$V|A]);
rev_comp([$b|T],A) -> rev_comp(T,[$V|A]);
rev_comp([$N|T],A) -> rev_comp(T,[$N|A]);
rev_comp([$n|T],A) -> rev_comp(T,[$N|A]);
rev_comp([10],A) -> A.
