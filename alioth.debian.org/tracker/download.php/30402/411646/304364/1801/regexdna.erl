% The Computer Language Shootout
% http://shootout.alioth.debian.org/
% Contributed by: Sergei Matusevich 2007

-module(regexdna).

-export([main/1]).

-define( VARIANTS,
  [ "agggtaaa|tttaccct",
    "[cgt]gggtaaa|tttaccc[acg]",
    "a[act]ggtaaa|tttacc[agt]t",
    "ag[act]gtaaa|tttac[agt]ct",
    "agg[act]taaa|ttta[agt]cct",
    "aggg[acg]aaa|ttt[cgt]ccct",
    "agggt[cgt]aa|tt[acg]accct",
    "agggta[cgt]a|t[acg]taccct",
    "agggtaa[cgt]|[acg]ttaccct" ] ).

fold_lines(File, Arg, Fun) ->
  case io:get_line(File, '') of
    eof -> Arg;
    Str -> fold_lines( File, Fun(Arg, Str), Fun )
  end.

read_lines(File) ->
  fold_lines( File, {0, [[]]},
    fun( {SzTotal, [Seg|Segz]}, Str ) ->
      case size(Str) of
        0 -> { SzTotal, [Seg|Segz] };
        Len ->
          case split_binary(Str, 1) of
            {<<">">>, _} -> { SzTotal + Len, [[],Seg|Segz] };
            _ ->
              case split_binary(Str, Len - 1) of
                {Strip, <<"\n">>} -> { SzTotal + Len, [[Strip|Seg]|Segz] };
                _ -> { SzTotal + Len, [[Str|Seg]|Segz] }
              end
          end
      end
    end ).

n_matches(Str, Re) ->
  {match, M} = regexp:matches(Str, Re),
  length(M).

main(_) ->
  io:setopts(standard_io, [binary]),
  {SzTotal, [S3Raw,S2Raw,S1Raw|_]} = read_lines(standard_io),
  B1 = list_to_binary(lists:reverse(S1Raw)),
  B2 = list_to_binary(lists:reverse(S2Raw)),
  B3 = list_to_binary(lists:reverse(S3Raw)),
  [L1,L2,L3] = [size(B1),size(B2),size(B3)],
  S2 = binary_to_list(B2),
  S3 = binary_to_list(B3),
  lists:foreach(
    fun(Re) ->
      io:format("~s ~w~n", [Re, n_matches(S2, Re) + n_matches(S3, Re)])
    end, ?VARIANTS ),
  L2Subst = length( lists:flatmap(
    fun(Ch) ->
      case Ch of
        $B -> "(c|g|t)";
        $D -> "(a|g|t)";
        $H -> "(a|c|t)";
        $K -> "(g|t)";
        $M -> "(a|c)";
        $N -> "(a|c|g|t)";
        $R -> "(a|g)";
        $S -> "(c|g)";
        $V -> "(a|c|g)";
        $W -> "(a|t)";
        $Y -> "(c|t)";
        XX -> [XX]
      end
    end, S2 ) ),
  io:format("~n~w~n~w~n~w~n", [SzTotal, L1 + L2 + L3, L1 + L2Subst + L3]),
  halt(0).

