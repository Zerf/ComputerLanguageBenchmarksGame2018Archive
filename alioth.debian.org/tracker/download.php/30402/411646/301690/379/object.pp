{$mode objfpc}
program objectBench;
uses SysUtils;

type TToggle = class
  value : boolean;

  constructor Create(b : boolean);
  function Activate : boolean;
end;

constructor TToggle.Create(b : boolean);
begin
  value := b;
end;

function TToggle.Activate : boolean;
begin
  value := not value;
  Activate := value;
end;


type TNthToggle = class(TToggle)
  counter, maxCounter : integer;

  constructor Create(b : boolean; max : integer);
  function Activate : boolean;
end;

constructor TNthToggle.Create(b : boolean; max : integer);
begin
  inherited Create(b);
  maxCounter := max;
  counter := 0;
end;

function TNthToggle.Activate : boolean;
begin
  Inc(counter);
  if counter >= maxCounter then
  begin
    value := not value;
    counter := 0;
  end;
  Activate := value;
end;


const boolStr : array[boolean] of string = ( 'false', 'true' );

var n,i : integer;
    t1 : TToggle;
    nt1 : TNthToggle;
begin
  n := StrToInt(paramstr(1));
  t1 := TToggle.Create(true);
  for i := 1 to 5 do writeln(boolStr[t1.Activate]);
  for i := 1 to n do t1 := TToggle.Create(true);
  writeln;
  nt1 := TNthToggle.Create(true,3);
  for i := 1 to 8 do writeln(boolStr[nt1.Activate]);
  for i := 1 to n do nt1 := TNthToggle.Create(true,3);
end.
