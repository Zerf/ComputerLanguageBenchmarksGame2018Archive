﻿using System;
using System.Text.RegularExpressions;

internal class RegexDna
{
    private const RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.CultureInvariant;

    private static readonly IUB[] Codes = {
                                       new IUB("B", "(c|g|t)"),
                                       new IUB("D", "(a|g|t)"),
                                       new IUB("H", "(a|c|t)"),
                                       new IUB("K", "(g|t)"),
                                       new IUB("M", "(a|c)"),
                                       new IUB("N", "(a|c|g|t)"),
                                       new IUB("R", "(a|g)"),
                                       new IUB("S", "(c|g)"),
                                       new IUB("V", "(a|c|g)"),
                                       new IUB("W", "(a|t)"),
                                       new IUB("Y", "(c|t)")
                                   };
    
    private static readonly Regex[] Variants = {
                                             new Regex("agggtaaa|tttaccct", regexOptions),
                                             new Regex("[cgt]gggtaaa|tttaccc[acg]", regexOptions),
                                             new Regex("a[act]ggtaaa|tttacc[agt]t", regexOptions),
                                             new Regex("ag[act]gtaaa|tttac[agt]ct", regexOptions),
                                             new Regex("agg[act]taaa|ttta[agt]cct", regexOptions),
                                             new Regex("aggg[acg]aaa|ttt[cgt]ccct", regexOptions),
                                             new Regex("agggt[cgt]aa|tt[acg]accct", regexOptions),
                                             new Regex("agggta[cgt]a|t[acg]taccct", regexOptions),
                                             new Regex("agggtaa[cgt]|[acg]ttaccct", regexOptions),
                                         };

    private static readonly Regex Strip = new Regex(">.*|\n");

    private static void Main()
    {
        var sequence = Console.In.ReadToEnd();

        int initialLength = sequence.Length;

        sequence = Strip.Replace(sequence, "");

        int codeLength = sequence.Length;

        foreach (var variant in Variants)
        {
            Console.WriteLine("{0} {1}", variant, variant.Matches(sequence).Count);
        }

        foreach (var iub in Codes)
        {
            sequence = iub.expression.Replace(sequence, iub.alternatives);
        }

        Console.WriteLine();
        Console.WriteLine(initialLength);
        Console.WriteLine(codeLength);
        Console.WriteLine(sequence.Length);
    }

    private struct IUB
    {
        public readonly string alternatives;
        public readonly Regex expression;

        public IUB(string expression, string alternatives)
        {
            this.expression = new Regex(expression, regexOptions);
            this.alternatives = alternatives;
        }
    }
}