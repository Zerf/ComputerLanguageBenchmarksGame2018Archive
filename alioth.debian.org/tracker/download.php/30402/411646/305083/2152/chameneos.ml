(**
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * Contributed by David Teller
 *
 * Inspired from the Python version
 *)

type color = Blue | Red | Yellow 

type 'a coroutine = 
  | Done    of 'a
  | Yield of (unit -> 'a coroutine) 

let remaining_meetings      = ref 0
let first_seat, second_seat = ref None, ref None

let complement_color a b = match a,b with
  | Blue, Red    | Red,    Blue -> Yellow
  | Blue, Yellow | Yellow, Blue -> Red
  | Red,  Yellow | Yellow, Red  -> Blue
  | _                           -> a

let creature =
  let rec come_to_meeting met color  = match !second_seat with
      | Some _ ->                                  (*Both seats are taken, wait until meeting place clears*)
	  Yield (fun () -> come_to_meeting met color)
      | None   ->                                  (*At least one seat available.                         *)
	  match !first_seat with
	    | None when !remaining_meetings <=0 -> (*Oh, wait, everybody's gone !                         *)
		Done met
	    | None ->                              
		first_seat := Some color;
		let rec loop () =                  
		  match !second_seat with
		    | None       -> Yield (fun () -> loop ())
		    | Some other ->
			  second_seat := None;     (*Yeah, that's a meeting                               *)
			  first_seat  := None;
			  decr remaining_meetings; 
			  come_to_meeting (met + 1) (complement_color color other)
		in loop ()
	    | Some other ->                        
		second_seat := Some color;         
		Yield (fun () -> come_to_meeting (met + 1) (complement_color color other))
  in
    come_to_meeting 0

let rec schedule current next total =              
  match current with
    | [] when next = [] -> print_int total; print_newline ()
    | []                -> schedule next [] total
    | Done d::t         -> schedule t    next (total + d)
    | (Yield h)::t      -> schedule t    (h ()::next) total

	
let () =
  (remaining_meetings :=  try int_of_string(Array.get Sys.argv 1) with _ -> 100);
  schedule [creature Blue; creature Red; creature Yellow; creature Blue] [] 0

    

  

(*
# Trivial round-robin scheduler.
def schedule(threads):
    while 1:
        for thread in threads:
            thread()

# A bunch of colorful creatures.
threads = [
    creature(BLUE).next,
    creature(RED).next,
    creature(YELLOW).next,
    creature(BLUE).next]

schedule(threads)
*)
