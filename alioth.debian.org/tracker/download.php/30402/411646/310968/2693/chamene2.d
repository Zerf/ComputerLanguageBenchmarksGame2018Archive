// The Computer Language Benchmarks Game
//   http://shootout.alioth.debian.org/
// Contributed by Michael Barker
// Based on a contribution by Luzius Meisser
// Convert to D by dualamd
// Modified by The Anh Tran and bearophile

import std.stdio, std.conv, std.string;
import std.thread: Thread;
import std.c.linux.pthread;

enum Color {
    blue,
    red,
    yellow,
    Invalid
}

const string[3] ColorName = ["blue", "red", "yellow"];
int creatureID;

Color doCompliment(Color c1, Color c2) {
    switch (c1) {
        case Color.blue:
            switch (c2) {
                case Color.blue:
                    return Color.blue;
                case Color.red:
                    return Color.yellow;
                case Color.yellow:
                    return Color.red;
            }
        case Color.red:
            switch (c2) {
                case Color.blue:
                    return Color.yellow;
                case Color.red:
                    return Color.red;
                case Color.yellow:
                    return Color.blue;
            }
        case Color.yellow:
            switch (c2) {
                case Color.blue:
                    return Color.red;
                case Color.red:
                    return Color.blue;
                case Color.yellow:
                    return Color.yellow;
            }
    }

    throw new Exception("Invalid colour");
}


struct Pair {
    bool sameId, two_met = false;
    Color colour;

    void Meet(Pair* p) {
        sameId = p.sameId;
        colour = p.colour;
        two_met = p.two_met;
    }

    void Meet(bool sameid, Color c) {
        this.sameId = sameid;
        this.colour = c;
        two_met = true;
    }
}


class MeetingPlace {
    private:
        pthread_mutex_t mutex;
        int meetingsLeft;
        Color firstColour = Color.Invalid;
        int firstId;
        Pair* current;

    public:
        this(int meetings) {
            pthread_mutex_init(&mutex, null);
            this.meetingsLeft = meetings;
        }

        ~this() {
            pthread_mutex_destroy(&mutex);
        }

        bool meet(int id, Color c, Pair* rp) {
            pthread_mutex_lock(&mutex);
            bool notfinish = true;
            if (meetingsLeft > 0) {
                if (firstColour == Color.Invalid) {
                    firstColour = c;
                    firstId = id;
                    rp.two_met = false;
                    current = rp;
                } else {
                    Color newColour = doCompliment( c, firstColour );
                    rp.Meet(id == firstId, newColour);
                    current.Meet(rp);
                    firstColour = Color.Invalid;
                    meetingsLeft--;
                }
            } else
                notfinish = false;
            pthread_mutex_unlock(&mutex);
            return notfinish;
        }
}


class Creature: public Thread {
    private:
        MeetingPlace place;
        int count = 0;
        int sameCount = 0;
        Color colour;
        int id;

    public:
        this(MeetingPlace place, Color colour) {
            super(16 * 1024); // 16KB stack for each thread
            this.place = place;
            this.id = ++creatureID;
            this.colour = colour;
        }

        override int run() {
            scope auto p = new Pair();
            while (true) {
                if (place.meet(id, colour, p)) {
                    while (p.two_met == false)
                        Thread.yield();

                    colour = p.colour;
                    if (p.sameId)
                        sameCount++;
                    count++;
                } else
                    break;
            }
            return 1;
        }

        int getCount() {
            return count;
        }

        string getResult() {
            return std.string.toString(count) ~ getNumber(sameCount);
        }
}

void run(int n, Color[] colors ...) {
    MeetingPlace place = new MeetingPlace(n);
    Creature[] creatures = new Creature[colors.length];

    foreach (int i, cl; colors) {
        writef(ColorName[cl], " ");
        creatures[i] = new Creature(place, cl);
    }
    writefln();
    foreach (c; creatures)
        c.start();
    foreach (t; creatures)
        t.wait();
    int total = 0;
    foreach (cr; creatures) {
        writefln(cr.getResult());
        total += cr.getCount();
    }
    writefln(getNumber(total), "\n");
}


string getNumber(int n) {
    const string[10] NUMBERS = ["zero", "one", "two", "three", "four",
                                "five", "six", "seven", "eight", "nine"];

    string ret;
    foreach (c; std.string.toString(n))
        ret ~= " " ~ NUMBERS[c - '0'];
    return ret;
}


void printColors(Color c1, Color c2) {
    writefln("%s + %s -> %s", ColorName[c1], ColorName[c2],
             ColorName[doCompliment(c1, c2)]);
}


void printColors() {
    with (Color)
        foreach (c; [[blue, blue], [blue, red], [blue, yellow], [red, blue],
                     [red, red], [red, yellow], [yellow, blue], [yellow, red],
                     [yellow, yellow]])
            printColors(c[0], c[1]);
}


void main(string[] args) {
    int n = args.length == 2 ? toInt(args[1]) : 600;
    printColors();
    writefln();

    with (Color) {
        run(n, blue, red, yellow);
        run(n, blue, red, yellow, red, yellow, blue, red, yellow, red, blue);
    }
}