/* ------------------------------------------------------------------ */
/* The Great Computer Language Shootout                               */
/* http://shootout.alioth.debian.org/                                 */
/*                                                                    */
/* Contributed by Anthony Borla                                       */
/* Modified by Vaclav Haisman                                         */
/* ------------------------------------------------------------------ */

#include <cctype>
#include <string>
#include <algorithm>
#include <iterator>
#include <iostream>

using namespace std;

const int LINELENGTH = 60;
const char ENDLINE = '\n', SEGMARKER = '>';

typedef string Header;
typedef string Segment;

typedef ostreambuf_iterator<char> StreamIterOut;

inline Segment& transformSegment(Segment& segment);
void dumpSegment(Header const & header, Segment const& segment, ostream& out = cout);
inline char complement(char element);

/* ----------------------------- */

int main ()
{
  ios_base::sync_with_stdio(false);

  string line;
  Segment segment;
  Header header;

  while (getline(cin, line, ENDLINE))
    {
      if (line[0] == SEGMARKER)
        {
          if (! segment.empty ())
            dumpSegment(header, transformSegment(segment));
          header = line;
          segment.clear ();
        }
      else
        segment += line;
    }
  dumpSegment(header, transformSegment(segment));

  return 0;
}

/* ----------------------------- */

Segment& transformSegment(Segment& segment)
{
  Segment::iterator fwd = segment.begin ();
  Segment::reverse_iterator bwd = segment.rbegin ();
  unsigned count = segment.size ();
  if (count % 2 != 0)
    segment[count / 2 + 1] = complement (segment[count / 2 + 1]);
  count /= 2;
  for (; count != 0; ++fwd, ++bwd, --count)
    {
      char const tmp = complement (*fwd);
      *fwd = complement (*bwd);
      *bwd = tmp;
    }
  return segment;
}

/* ----------------------------- */

void dumpSegment(Header const & header, Segment const & segment, ostream& out)
{
  out << header << ENDLINE;

  Segment::const_iterator begin = segment.begin(), i = segment.begin(),
    end = segment.end ();

  while (distance (i, end) >= LINELENGTH)
    {
      advance (i, LINELENGTH);
      copy(begin, i, StreamIterOut(out)); 
      out << ENDLINE;
      begin = i;
    }
  if (begin != segment.end ())
    {
      copy(begin, segment.end (), StreamIterOut(out));
      out << ENDLINE;
    }
}

/* ----------------------------- */

char complement(char element)
{
  static const char charMap[] =
    {
      'T', 'V', 'G', 'H', '\0', '\0', 'C', 'D', '\0', '\0', 'M', '\0', 'K',
      'N', '\0', '\0', '\0', 'Y', 'S', 'A', 'A', 'B', 'W', '\0', 'R', '\0'
    };

  return charMap[toupper(element) - 'A'];
}
