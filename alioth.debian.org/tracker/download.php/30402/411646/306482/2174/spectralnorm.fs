// The Great Computer Language Shootout
// http://shootout.alioth.debian.org/
// 
// Based on ocaml version by Sebastien Loisel & Troestler Christophe
// Contributed by Robert Pickering
#light

let eval_A i j = 1. / float((i+j)*(i+j+1)/2+i+1)

let eval_A_times_u u v =
  let n = Array.length v - 1 in
  for i = 0 to  n do
    v.(i) <- 0.0
    for j = 0 to n do 
        v.(i) <- v.(i) + eval_A i j * u.(j)

let eval_At_times_u u v =
  let n = Array.length v -1 in
  for i = 0 to n do
    v.(i) <- 0.0
    for j = 0 to n do 
        v.(i) <- v.(i) + eval_A j i * u.(j)

let eval_AtA_times_u u v =
  let w = Array.create (Array.length u) 0.0
  eval_A_times_u u w 
  eval_At_times_u w v


let () =
  let n = try int_of_string(Array.get Sys.argv 1) with _ ->  2000 in
  let u = Array.create n 1.0 
  let v = Array.create n 0.0
  for i = 0 to 9 do
    eval_AtA_times_u u v
    eval_AtA_times_u v u

  let mutable vv = 0.0  
  let mutable vBv = 0.0
  for i=0 to n-1 do
    vv <- vv + v.(i) * v.(i)
    vBv <- vBv + u.(i) * v.(i)
  Printf.printf "%0.9f\n" (sqrt(vBv / vv))
