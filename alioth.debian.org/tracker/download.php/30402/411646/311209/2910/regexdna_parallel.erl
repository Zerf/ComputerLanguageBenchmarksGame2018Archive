% The Computer Language Benchmarks Game
% http://shootout.alioth.debian.org/
% Contributed by: Sergei Matusevich 2008
% Using pmap/2 from Luke Gorrie.

-module(regexdna_parallel).

-export([main/1]).

-define( VARIANTS,
  [ "agggtaaa|tttaccct",
    "[cgt]gggtaaa|tttaccc[acg]",
    "a[act]ggtaaa|tttacc[agt]t",
    "ag[act]gtaaa|tttac[agt]ct",
    "agg[act]taaa|ttta[agt]cct",
    "aggg[acg]aaa|ttt[cgt]ccct",
    "agggt[cgt]aa|tt[acg]accct",
    "agggta[cgt]a|t[acg]taccct",
    "agggtaa[cgt]|[acg]ttaccct" ] ).

pmap(F, L) ->
  Parent = self(),
  [ receive {Pid, Result} -> Result end
    || Pid <- [spawn(fun() -> Parent ! {self(), F(X)} end) || X <- L] ].

read_lines(File, SzTotal, [Seg|Segz]) ->
  case io:get_line(File, '') of
    eof -> {SzTotal, [Seg|Segz]};
    Str ->
      Len = size(Str),
      Eol = Len - 1,
      read_lines( File, SzTotal + Len,
        case Str of
          <<">",_/binary>>         ->     [[],Seg|Segz];
          <<Trim:Eol/binary,"\n">> -> [[Trim|Seg]|Segz];
          _                        ->  [[Str|Seg]|Segz]
        end )
  end.

main(_) ->
  io:setopts(standard_io, [binary]),
  {SzTotal, [SR1,SR2,SR3|_]} = read_lines(standard_io, 0, [[]]),
  [B1,B2,B3] = pmap(fun(S) -> list_to_binary(lists:reverse(S)) end, [SR3,SR2,SR1]),
  S2  = binary_to_list(B2),
  S3  = binary_to_list(B3),
  L2  = size(B2),
  L13 = size(B1) + size(B3),
  Pid = self(),
  spawn( fun() ->
    Pid ! length( lists:flatmap(
      fun(Ch) ->
        case Ch of
          $B -> "(c|g|t)";
          $D -> "(a|g|t)";
          $H -> "(a|c|t)";
          $K -> "(g|t)";
          $M -> "(a|c)";
          $N -> "(a|c|g|t)";
          $R -> "(a|g)";
          $S -> "(c|g)";
          $V -> "(a|c|g)";
          $W -> "(a|t)";
          $Y -> "(c|t)";
          XX -> [XX]
        end
      end, S2 ) ) end ),
  Out = pmap(
    fun(Re) ->
      {match, M2} = regexp:matches(S2, Re),
      {match, M3} = regexp:matches(S3, Re),
      [Re, length(M2) + length(M3)]
    end, ?VARIANTS ),
  lists:foreach(fun(P) -> io:format("~s ~w~n", P) end, Out),
  receive
    L2Sub -> io:format("~n~w~n~w~n~w~n", [SzTotal, L13 + L2, L13 + L2Sub])
  end,
  halt(0).

