#!/usr/bin/regina -a

/* ------------------------------------------------------------------ */
/* The Great Computer Language Shootout                               */
/* http://shootout.alioth.debian.org/                                 */
/*                                                                    */
/* % regina -v                                                        */
/* REXX-Regina_3.3(MT) 5.00 25 Apr 2004                               */
/* % uname -orvmp                                                     */
/* 2.6.5-1.358 #1 Wed Oct 13 17:49:34 EST 2004 i686 i686 GNU/Linux    */
/*                                                                    */
/* Contributed by Anthony Borla                                       */
/* ------------------------------------------------------------------ */

trace 'OFF'

num = ARG(1) ; if DATATYPE(num) \= 'NUM' | num < 1 then ; num = 1 

say "Ack(3," || num || "):" Ack(3, num) 

exit 0

/* ----------------------------- */

Ack :
  if ARG(1) == 0 then ; return ARG(2) + 1
  if ARG(2) == 0 then ; return Ack(ARG(1) - 1, 1)
  return Ack(ARG(1) - 1, Ack(ARG(1), ARG(2) - 1))
