(*
  The Computer Language Benchmarks Game
  http://shootout.alioth.debian.org

  contributed by Vitaly Trifonov
*)

{$INLINE ON}
{$CHECKPOINTER OFF}
{$RANGECHECKS OFF}
{$OVERFLOWCHECKS OFF}
{$VARSTRINGCHECKS OFF}
{$IOCHECKS OFF}
{$OPTIMIZATION TAILREC}

uses regexpr, sysutils;


var
  seqLen: Cardinal = 0;
  seqCleanLen: Cardinal = 0;


function getchar: Char; inline;
var
  ch: Char;
begin
  seqLen += 1;  read(ch);  getchar := ch
end;


var
  patterns : array[0..8] of PChar =
    (
      'agggtaaa|tttaccct',
      '[cgt]gggtaaa|tttaccc[acg]',
      'a[act]ggtaaa|tttacc[agt]t',
      'ag[act]gtaaa|tttac[agt]ct',
      'agg[act]taaa|ttta[agt]cct',
      'aggg[acg]aaa|ttt[cgt]ccct',
      'agggt[cgt]aa|tt[acg]accct',
      'agggta[cgt]a|t[acg]taccct',
      'agggtaa[cgt]|[acg]ttaccct'
    );


function findExpr( regexp, seq: PChar ): Longint; inline;
var
  cregexp : TRegExprEngine;
  count, index, tmpInt: Longint;
  tmpseq: PChar;
begin
  count := 0; tmpseq := seq;
  GenerateRegExprEngine( regexp, [ref_caseinsensitive], cregexp);

  while RegExprPos(cregexp, tmpseq, index, tmpInt) do
  begin
    count += 1;
    tmpseq := @(tmpseq[index + 1]);
  end;

  DestroyRegExprEngine( cregexp );
  findExpr := count
end;


function findSplitExpr ( regexp,seq: PChar ): Longint; inline;
var
  split: PChar;
  count: Longint;
begin
  split := strscan(regexp, '|');
  Byte(split^) := 0;

  count := findExpr(regexp, seq);
  count += findExpr(@split[1], seq);
  split^ := '|';
  findSplitExpr := count
end;

(* calculate needed size for malloc *)
function addSize( seq: PChar ): Cardinal; inline;
var
  count: Cardinal = 0;
  tmpseq: PChar;
begin
  tmpseq := seq;
  while Byte(tmpseq^) <> 0 do
  begin
    case tmpseq^ of
      'B':    count += 6;  //(c|g|t)
      'D':    count += 6;  //(a|g|t)
      'H':    count += 6;  //(a|c|t)
      'K':    count += 4;  //(g|t)
      'M':    count += 4;  //(a|c)
      'N':    count += 8;  //(a|c|g|t)
      'R':    count += 4;  //(a|g)
      'S':    count += 4;  //(c|t)
      'V':    count += 6;  //(a|c|g)
      'W':    count += 4;  //(a|t)
      'Y':    count += 4;  //(c|t)
    end;
    tmpseq := @tmpseq[1]
  end;
  addSize := count
end;

(* IUB replace *)
procedure replace( seq: PChar; var newSeq: PChar ); inline;
var
  itmpseq, otmpseq: PChar;
begin
  GetMem(newSeq, SizeOf(Char)*(seqCleanLen + addSize(seq) + 1));
  itmpseq := seq; otmpseq := newSeq;
  while Byte(itmpseq^) <> 0 do
  begin
    case itmpseq^ of
      'B':  otmpseq := strecopy(otmpseq,'(c|g|t)');
      'D':  otmpseq := strecopy(otmpseq,'(a|g|t)');
      'H':  otmpseq := strecopy(otmpseq,'(a|c|t)');
      'K':  otmpseq := strecopy(otmpseq,'(g|t)');
      'M':  otmpseq := strecopy(otmpseq,'(a|c)');
      'N':  otmpseq := strecopy(otmpseq,'(a|c|g|t)');
      'R':  otmpseq := strecopy(otmpseq,'(a|g)');
      'S':  otmpseq := strecopy(otmpseq,'(c|t)');
      'V':  otmpseq := strecopy(otmpseq,'(a|c|g)');
      'W':  otmpseq := strecopy(otmpseq,'(a|t)');
      'Y':  otmpseq := strecopy(otmpseq,'(c|t)');
    else
      otmpseq^ := itmpseq^;
      otmpseq := @otmpseq[1]
    end;
    itmpseq := @itmpseq[1]
  end;
end;


var
  seq, newSeq: PChar;
  maxSeqLen: Cardinal = 6000000;
  ch: Char;
  i: Longint;
begin
  GetMem(seq, SizeOf(Char)*(maxSeqLen+1));

(* Read FASTA format file from stdin
   Count length and remove the unwanted elements  *)
  while not eof do
  begin
    ch := getchar;

    if ch <> '>' then
    begin
      if seqCleanLen = maxSeqLen then
      begin
        maxSeqLen += 2000000;
        seq := ReAllocMem(seq, SizeOf(Char)*(maxSeqLen+2) );
      end;
      seq[seqCleanLen] := ch;
      seqCleanLen += 1;
      if eoln then
        getchar
    end
    else
    begin
      repeat
        getchar
      until eoln;
      getchar
    end
  end;
  Byte(seq[seqCleanLen]) := 0; //end read data

(* Count matches *)
(* The following slow, because unit regexpr in free pascal is very slow *)
  for i := 0 to 8 do
    writeln(patterns[i], ' ',findSplitExpr(patterns[i], seq));
    //writeln(patterns[i], ' ', findExpr(patterns[i], seq));

(* do replacements *)
  replace(seq, newSeq);

  writeln;
  writeln(seqLen);
  writeln(seqCleanLen);
  writeln(strlen(newSeq));

  FreeMem(newSeq);
  FreeMem(seq)
end.
