/*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org
 *
 * "reverse-complement" benchmark for Gnu C
 * contributed by Bob W (V3, 2008-05-14)
 */

#include <stdio.h>
#include <stdlib.h>

#define JBFSIZE 10280    // size of input buffer (>= 10245)
#define QBFSIZE 5100     // initial size of output buffer (>= 5085)

static const char XTAB[128] = {  // 7-bit character conversion table
  0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
  0,'T','V','G','H',0,  0,'C', 'D',  0,0,'M', 0,'K','N',0,
  0,  0,'Y','S','A',0,'B','W',   0,'R',0,  0, 0,  0,  0,0,
  0,'T','V','G','H',0,  0,'C', 'D',  0,0,'M', 0,'K','N',0,
  0,  0,'Y','S','A',0,'B','W',   0,'R',0,  0, 0,  0,  0,0
};


int errex(char *s, int n) {        // show message & value, return 1
  fprintf(stderr,"\n*** Error: %s [%d]!\n", s, n);
  return 1;
}


int main () {                            // ***** main *****
  char *pj, *pq, *pr;                    // buf. pointers: inp,out,/out
  char *jjj = malloc(JBFSIZE);           // allocate input buffer
  char *qqq = malloc(QBFSIZE);           // output buffer (dyn. size)
  char *pjstop, *pqstop = qqq+QBFSIZE;   // end-of-buffer pointers
  int  n;  char c;                       // temps

  if (!jjj || !qqq)
    return errex("Buffer allocation", !jjj + !qqq);

  n = fread(jjj, 1, JBFSIZE, stdin);     // get 1st block from stdin
  pj = jjj;  pjstop = pj+n;              // init buffer pointers
  if (n < 4)
    return errex("Input data size",n);
  if (*jjj != '>')
    return errex("1st char not '>'", 0);

  while (pj < pjstop) {                  // MAIN LOOP: process data

    for (pq=qqq; *pj!=10 && *pj!=13; ) { // LOOP: copy entire ID line
      *pq++ = *pj++;
      if (pj >= pjstop) {                // get more input data
        n = fread(jjj,1,JBFSIZE,stdin);  // fetch data block
        if (!n)
          return errex("ID line at EOF", pj-jjj);
        if (pq > (qqq+256))
          return errex("Long ID line", pq-qqq);
        pj = jjj;  pjstop = pj+n;        // update buffer pointers
      }
    }
    *pq++ = 0xA;                         // add LF
    fwrite(qqq, 1, pq-qqq, stdout);      // output ID line

    for (pq=qqq+1, pr=pqstop; *pj !='>'; ) {  // LOOP: convert & revert
      c=XTAB[*pj++ & 0x7f];              // use 7 bits to convert
      if (c) {                           // converted char accepted
        if (pr <= pq) {                  // check space (+LFs)
          char *newstop = pqstop + 12777888;
          char *newptr  = realloc(qqq, newstop-qqq);
          if (!newptr)
            return errex("Out of memory", 0);
          if (newptr != qqq) {           // new base: adjust pointers
            size_t x = newptr-qqq;       // calc offset
            newstop+=x;  pqstop+=x;
            pr+=x;  pq+=x;  qqq = newptr;
          }
          pr = __builtin_memmove(newstop-(pqstop-pr), pr, pqstop-pr);
          pqstop = newstop;              // buffer resize complete
        }
        *--pr = c;                       // move converted char
      }
      else  pq++;                        // create space for LFs

      if (pj >= pjstop) {                // get more input data
        n = fread(jjj,1,JBFSIZE,stdin);  // fetch data block
        if (!n)  break;                  // out-of-data terminates
        pj = jjj;  pjstop = pj+n;        // update buffer pointers
      }
    }

    for (pq = qqq; pr<pqstop; ) {        // LOOP: format output
      n = (pqstop-pr)<60 ? pqstop-pr : 60;  // limit line length
      __builtin_memmove(pq,pr,n);        // move line to free space
      pr+=n;  pq+=n;  *pq++ = 0xA;       // adjust pointers, add LF
    }
    fwrite(qqq, 1, pq-qqq, stdout);      // output converted data

  }
  return 0;
}
