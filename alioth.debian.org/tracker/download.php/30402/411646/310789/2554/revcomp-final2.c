/*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org
 *
 * "reverse-complement" benchmark for Gnu C
 * contributed by Bob W (V3, 2008-05-14)
 */

#include <stdio.h>
#include <stdlib.h>

#define JBFSIZE 256      // size of input buffer
#define QBFSIZE 5200     // initial size of output buffer

static const char XTAB[128] = {  // 7-bit character conversion table
  0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
  0,'T','V','G','H',0,  0,'C', 'D',  0,0,'M', 0,'K','N',0,
  0,  0,'Y','S','A',0,'B','W',   0,'R',0,  0, 0,  0,  0,0,
  0,'T','V','G','H',0,  0,'C', 'D',  0,0,'M', 0,'K','N',0,
  0,  0,'Y','S','A',0,'B','W',   0,'R',0,  0, 0,  0,  0,0
};


int errex(char *s, int n) {        // show message & value, return 1
  fprintf(stderr,"\n*** Error: %s [%d]!\n", s, n);
  return 1;
}


int main () {                             // ***** main *****
  char    *pj, *pq, *pr;                  // buf. pointers: inp,out,/out
  char    *jjj = malloc(JBFSIZE);         // allocate input buffer
  char    *qqq = malloc(QBFSIZE);         // output buffer (dyn. size)
  char    *pqstop = qqq+QBFSIZE;          // end-of-buffer pointers
  size_t   x;  char c;                    // temps

  if (!jjj || !qqq)
    return errex("Buffer allocation", !jjj + !qqq);

  pj = fgets(jjj, JBFSIZE, stdin);        // get 1st line from stdin
  if (!pj)
    return errex("No input data",0);
  if (*jjj != '>')
    return errex("1st char not '>'", 0);

  while (pj) {                            // MAIN LOOP: process data
    fputs(jjj, stdout);                   // output ID line

    for (pq=qqq+1, pr=pqstop; 1; pq++) {  // LOOP: fill output buffer
      pj = fgets(jjj, JBFSIZE, stdin);    // get line from stdin
      if (!pj || *jjj == '>')  break;     // EOF or new ID line

      while (*pj) {                       // LOOP: convert & revert line
        c = XTAB[*pj++ & 0x7f];           // convert using 7 bits
        if (c) {                          // converted char accepted
          if (pr <= pq) {                 // check space (+LFs)
            char *newstop = pqstop + 12777888;
            char *newptr  = realloc(qqq, newstop-qqq);
            if (!newptr)
              return errex("Out of memory", 0);
            if (newptr != qqq) {          // new base: adjust pointers
              size_t x = newptr-qqq;      // calc offset
              newstop+=x;  pqstop+=x;
              pr+=x;  pq+=x;  qqq = newptr;
            }
            pr = __builtin_memmove(newstop-(pqstop-pr), pr, pqstop-pr);
            pqstop = newstop;             // buffer resize complete
          }
          *--pr = c;                      // move converted char
        }
      }
    }

    for (pq = qqq; pr<pqstop; ) {         // LOOP: format output
      x=(pqstop-pr)<60 ? pqstop-pr : 60;  // limit line length
      __builtin_memmove(pq,pr,x);         // move line to free space
      pr+=x;  pq+=x;  *pq++ = 0xA;        // adjust pointers, add LF
    }
    fwrite(qqq, 1, pq-qqq, stdout);       // output converted data
  }
  return 0;
}
