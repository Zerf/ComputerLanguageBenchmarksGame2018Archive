;;; -*- mode: lisp -*-
;;;
;;; http://shootout.alioth.debian.org/
;;;
;;; Contributed by Witali Kusnezow 2009-02-19

(defclass btree ()
    ((item  :accessor item  :initarg :item  :type fixnum)
     (depth :accessor depth :initarg :depth :type fixnum)
     (size  :reader   size  :type fixnum)
     (pool  :accessor pool  :initarg :pool  :initform nil)))

(defmacro tree (item depth &optional (pool nil))
  `(make-instance 'btree :item ,item :depth ,depth :pool ,pool))
(defmacro check(tree)
  `(the fixnum (let ((a-tree ,tree))(check-tree (pool a-tree) (size a-tree)))))

(defmethod initialize-instance :after ((a-tree btree) &rest init-args)
  (declare (ignore init-args))
  (with-slots (item depth pool size) a-tree
    (setf size (1- (the fixnum (ash 2 depth))))
    (if (not pool)
        (setf pool (make-array size :initial-element 0)))
    (init-tree pool item depth)))

(defun init-tree (pool item depth)
  (declare (type fixnum item depth))
  (loop for i of-type fixnum to depth
     with (ii ii2 iit) of-type fixnum
     do (setf ii (ash 1 i) ii2 (+ ii ii -2) iit (* item ii))
     (loop
        for idx downfrom ii2
        for val downfrom iit
        repeat (the fixnum ii)
        do (setf (svref  pool idx) val))))

(defun check-tree (pool size &optional (idx 1)
               &aux (data (svref pool (1- idx))) (child (ash idx 1)))
  (declare (type fixnum idx child data size)
           (values fixnum))
  (if (< size child)
      data
      (- (+ data (check-tree pool size child)) (check-tree pool size (1+ child)))))

#+sb-thread
(eval-when (:compile-toplevel :load-toplevel :execute)
  (define-alien-routine sysconf long (name int))
  (use-package  :sb-thread))

#+sb-thread
(progn
  (defconstant  +cpu-count+ (sysconf 84))
  (defmacro bg  (&body body) `(make-thread (lambda () ,@body)))
  (defparameter *semaphores* nil)
  (defparameter *semaphore* (make-semaphore))
  (defun loop-depths (max-depth
                      &key (min-depth 4) (long-lived-tree (tree 0 max-depth))
                      &aux (count (1+ (ash (- max-depth min-depth) -1)))
                      (count+1 (1+ count)))
    (declare (type fixnum max-depth min-depth count count+1))
    (setf *semaphores* (loop repeat count+1 collect (make-semaphore)))
    (let((threads
          (concatenate
           'list
           (loop for idx of-type fixnum by 1 repeat count collect
                (bg
                  (let* ((depth (+ min-depth (the fixnum (ash idx 1))))
                         (iterations (ash 1 (+ max-depth min-depth (- depth)))))
                    (declare (type fixnum depth iterations))
                    (wait-on-semaphore (nth idx *semaphores*))
                    (loop with pool = (make-array (1- (the fixnum (ash 2 depth))) :initial-element 0)
                       for i of-type fixnum from 1 upto iterations
                       sum (+ (check (tree i depth pool)) (check (tree (- i) depth pool)))
                       into result of-type fixnum
                       finally
                       (signal-semaphore *semaphore*)
                       (return
                         (format nil "~D	 trees of depth ~D	 check: ~D"
                                 (the fixnum (+ iterations iterations )) depth result))))))
           (list (bg (wait-on-semaphore (nth count *semaphores*))
                     (format nil "long lived tree of depth ~D	 check: ~D"
                             max-depth (check long-lived-tree)))))))
      (loop with active of-type fixnum = 0
         for idx of-type fixnum by 1 repeat count+1
         if (= active +cpu-count+)
         do (wait-on-semaphore *semaphore*) (decf active)
         do (incf active) (signal-semaphore (nth idx *semaphores*)))
      (format t "~{~a~%~}" (mapcar #'join-thread threads))
      )))

#-sb-thread
(defun loop-depths (max-depth
                    &key (min-depth 4)  (long-lived-tree (tree 0 max-depth))
                    (pool (make-array (1- (ash 2 max-depth)) :initial-element 0)))
  (declare (type fixnum max-depth min-depth))
  (loop for d of-type fixnum from min-depth by 2 upto max-depth do
       (loop with iterations of-type fixnum = (ash 1 (+ max-depth min-depth (- d)))
          for i of-type fixnum from 1 upto iterations
          sum (+ (check (tree i d pool)) (check (tree (- i) d pool)))
          into result of-type fixnum
          finally
            (format t "~D	 trees of depth ~D	 check: ~D~%"
                    (the fixnum (+ iterations iterations )) d result)))
  (format t "long lived tree of depth ~D	 check: ~D~%"
          max-depth (check long-lived-tree)))

(defun main (&optional (n (parse-integer
                           (or (car (last #+sbcl sb-ext:*posix-argv*
                                          #+cmu  extensions:*command-line-strings*
                                          #+gcl  si::*command-args*))
                               "1")))
             &aux (n+1 (1+ n)))
  (declare (type fixnum n n+1))
  (format t "stretch tree of depth ~D	 check: ~D~%" n+1 (check (tree 0 n+1)))
  (loop-depths n))

