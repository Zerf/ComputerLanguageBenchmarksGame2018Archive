(* nsieve.ml -- na�ve Sieve of Eratosthenes
 *
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Christophe TROESTLER
 * Modified by Vladimir Silyaev
 *)

let max_array_length = 0x3FFFFF

let array_make_true n =
  Array.init (n/max_array_length + 1) (fun _ -> Array.create max_array_length true)

let clear a n = a.(n lsr 22).(n land max_array_length) <- false

let get a n:bool = a.(n lsr 22).(n land max_array_length)

let nsieve m =
  let a = array_make_true m in
  let count = ref 0 in
  for i = 2 to m - 1 do
    if get a i then (
      incr count;
      let j = ref(i lsl 1) in while !j < m do clear a !j ; j := !j+i done;
    )
  done;
  Printf.printf "Primes up to %8u %8u\n" m !count


let () =
  (* Use [Array.get] so it raises an exception even if compiled with -unsafe *)
  let n = try int_of_string (Array.get Sys.argv 1) with _ -> 2 in
  for i = 0 to 2 do nsieve(10000 lsl (n-i)) done
