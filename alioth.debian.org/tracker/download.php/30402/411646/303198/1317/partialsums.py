# The Computer Language Shootout
# http://shootout.alioth.debian.org/

# contributed by Giovanni Bajo
# optimized from the previous implementation by Josh Goldfoot

from sys import argv
from math import sin, cos

def main(n):
    twoThirds = 2.0 / 3.0
    print "%.9f\t%s" % (sum(twoThirds ** k for k in xrange(n)), "(2/3)^k")
    print "%.9f\t%s" % (sum(k ** -0.5 for k in xrange(1,n+1)), "k^-0.5")
    print "%.9f\t%s" % (sum(1.0/(k*(k+1.0)) for k in xrange(1,n+1)), "1/k(k+1)")
    print "%.9f\t%s" % (sum(1.0/(k ** 3.0 * sin(k) ** 2.0) for k in xrange(1,n+1)), "Flint Hills")
    print "%.9f\t%s" % (sum(1.0/(k ** 3.0 * cos(k) ** 2.0) for k in xrange(1,n+1)), "Cookson Hills")
    print "%.9f\t%s" % (sum(1.0/k for k in xrange(1,n+1)), "Harmonic")
    print "%.9f\t%s" % (sum(1.0/k ** 2.0 for k in xrange(1,n+1)), "Riemann Zeta")
    print "%.9f\t%s" % (sum(1.0/k for k in xrange(1,n+1,2)) - sum(1.0/k for k in xrange(2,n+1,2)), "Alternating Harmonic")
    print "%.9f\t%s" % (sum(1.0/(2.0*k -1.0) for k in xrange(1,n+1,2)) - sum(1.0/(2.0*k -1.0) for k in xrange(2,n+1,2)), "Gregory")

main((len(argv) > 1) and int(argv[1]) or 25000)
