{-  The Great Computer Language Shootout
    http://shootout.alioth.debian.org/
    compile with:  ghc -O2 -o fannkuch fannkuch.hs
    contributed by Josh Goldfoot, "fixing" the version by Greg Buchholz
     permutations function translated from the C version by Heiner Marxen -}

import System(getArgs)
import Data.Int

main = do   [n] <- getArgs
            let p = permutations [1..(read n)]
            mapM putStrLn $ map ((concat).(map show)) $ take 30 (permutations [1..(read n)])
            putStr $ "Pfannkuchen(" ++ n ++ ") = "
            print $ findmax 0 p
            
findmax :: Int8 -> [[Int8]] -> Int8            
findmax soFar [] = soFar
findmax soFar (x:xs) =
   max (flop 0 x) (findmax soFar xs)
   
flop :: Int8 -> [Int8] -> Int8
flop acc (1:xs) = acc
flop acc list@(x:xs) = flop (acc+1) mangle
    where   mangle = (reverse front) ++ back
            (front,back) = splitAt (fromIntegral x) list
            
permutations :: [Int8] -> [[Int8]]        
permutations arry =
   arry : (permuteloop arry [1..n] 1)
   where n = fromIntegral (length arry)
   
permuteloop :: [a] -> [Int8] -> Int8 -> [[a]]
permuteloop arry count r
   | r == ((fromIntegral . length) arry) = []
   | count' !! (fromIntegral r) > 0 = arry' : (permuteloop arry' (reload r count') 1)
   | otherwise = permuteloop arry' count' (r+1)
   where count' = (take (fromIntegral r) count) ++ ((count !! (fromIntegral r)) - 1):(drop (fromIntegral r+1) count)
         arry' = rotate (fromIntegral r) arry
         
rotate :: Int8 -> [a] -> [a]
rotate r (x:xs) = 
   begin ++ x:end
   where (begin, end) = splitAt (fromIntegral r) xs
   
reload :: Int8 -> [Int8] -> [Int8]
reload r count
   | r == 1 = count
   | otherwise = [1..r] ++ (drop (fromIntegral r) count)
