# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
# 
# Unknown credits:
#  Eckehard Berns, Heiner Marxen,
#   Hongwei Xi (ATS)
# Concurrency idea from Java code by The Anh Tran
# Based on code by by Daniel Nanz 
# modified by Syukri (2009-03)

import multiprocessing as mp

def next_perm(permutation, position):
    p0 = permutation.pop(0)
    permutation.insert(position, p0)

def get_flips(p, c=0):
    k = p[0]
    while k > 0:
        p[:k+1] = p[k::-1]
        c += 1
        k = p[0]
    return c

def permutations(n, pos_right):
    p = list(range(n))
    p[pos_right] = n-1
    p[n-1] = pos_right
    c = list(range(1, n + 1))
    p_insert = p.insert
    r = n
    
    yield p[:]
    while True:
        while r > 1:
            c[r-1] = r
            r -= 1
        while r < n-1:
            p0 = p[0]
            del p[0]
            p_insert(r, p0)
            c[r] -= 1
            if c[r] > 0:
                yield p[:]
                break
            r += 1
        else:
            raise StopIteration

def worker(id,n, remain_task, flip_max_arr, lock):
    perm = list(range(n))
    pos_right = 0
    while 1:
        if lock.acquire():
            pos_right = remain_task.value
            remain_task.value += 1
            lock.release()
        if pos_right < n-1:
        #Got new job, so start working
            for p in permutations(n, pos_right):
                flips = get_flips(p)
                if flip_max_arr[id]<flips:
                    flip_max_arr[id] = flips
        #Job finished. Poll new tasks
        else:
            break

def fannkuch(n):
    lock = mp.Lock()
    proc = []
    num_cpu = mp.cpu_count()
    remain_task = mp.RawValue('i',0)
    flip_max_arr = mp.RawArray('i',num_cpu)  #initiates a [0,0,...] for putting maximum flips
    for x in range(num_cpu):
        pr = mp.Process(target=worker,args=(x,n,remain_task,flip_max_arr,lock))
        pr.start()
        proc.append(pr)
    #meanwhile, let's tell viewers first 30 (dummy) permutations
    count = 0
    for p in permutations(n,n-1):
        print("".join(str(i+1) for i in p))
        count += 1
        if count >29:
            break
    #join all workers
    for pr in proc:
        pr.join()
    return max(flip_max_arr[:])

def main():
    from sys import argv
    try:
        n = int(argv[1])
    except:
        n = 7
    print("Pfannkuchen({0}) = {1}".format(n, fannkuch(n)))

if __name__=='__main__':
    main()
