#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <pthread.h>	// -lpthread

#define THREADS 500

#define STACKSZ (16*1024) // 16kb stack

typedef struct {
  pthread_t t;
  pthread_cond_t c,*nextc;
  pthread_mutex_t m,*nextm;
  int times;
  int condready,*nextcondready;
  int lastflag;
} threadinfo_t;

static void 
*work(threadinfo_t *me) {
  pthread_mutex_lock(&(me->m));
  for( ; me->times != 0; me->times--) {
    while(me->condready == 0)pthread_cond_wait(&(me->c),&(me->m));
    pthread_mutex_lock(me->nextm);
    if(me->lastflag == 0)
        *(me->nextcondready) += me->condready + 1;
    else
       *(me->nextcondready) += me->condready;
    me->condready = 0;
    pthread_cond_signal(me->nextc);
    pthread_mutex_unlock(me->nextm);
  }
  return(NULL);
}

int
main(int argc, char *argv[])
{
	unsigned int times;
	assert(argc == 2 && sscanf(argv[1], "%u", &times) == 1);

	pthread_attr_t attr;
        threadinfo_t t[THREADS];
	pthread_cond_t main_cond = PTHREAD_COND_INITIALIZER;
	pthread_mutex_t main_mutex = PTHREAD_MUTEX_INITIALIZER;
	int i,sum = 0;

	assert(pthread_attr_init(&attr) == 0);
	assert(pthread_attr_setstacksize(&attr, STACKSZ) == 0);
	for (i = 0; i < THREADS; i++) {
	  t[i].times = times;
	  t[i].nextc = i != (THREADS-1) ? &(t[i+1].c) : &main_cond;
	  t[i].nextm = i != (THREADS-1) ? &(t[i+1].m) : &main_mutex;
	  t[i].nextcondready = i != (THREADS-1) ? (&t[i+1].condready) : &sum;
	  t[i].condready = 0;
          t[i].lastflag = i == (THREADS - 1);

	  assert(pthread_cond_init(&(t[i].c),NULL) == 0);
	  assert(pthread_mutex_init(&(t[i].m),NULL) == 0);
	  assert(pthread_create(&(t[i].t), &attr, work,t+i) == 0);
	}

	assert(pthread_mutex_lock(&main_mutex) == 0);
	for(i = 0; i < times; i++) {
          int lastsum = sum;
          pthread_mutex_lock(&(t[0].m));
	  t[0].condready++;
          pthread_mutex_unlock(&(t[0].m));
	  assert(pthread_cond_signal(&(t[0].c)) == 0);
	  while(sum == lastsum)
	      assert(pthread_cond_wait(&main_cond,&main_mutex) == 0);
        }

	pthread_join(&(t[THREADS-1].t),NULL);
	return(printf("%d\n", sum),EXIT_SUCCESS);
}
