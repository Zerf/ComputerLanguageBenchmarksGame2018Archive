(defmacro psum (name f)
  `(format t "~,9F~A~A~%"
     (do ((k 1.0d0 (+ k 1.0d0))
          (sum 0.0d0 (+ sum ,f)))
         ((> k n) sum))
     #\tab ,name))

(defun main ()
  (let* ((args #+sbcl sb-ext:*posix-argv*
               #+cmu extensions:*command-line-strings*
               #+gcl si::*command-args*)
         (n (parse-integer (car (last args)))))
    (psum "(2/3)^k"       (expt (/ 2.0d0 3.0d0) (- k 1.0d0)))
    (psum "k^-0.5"        (/ 1.0d0 (sqrt k)))
    (psum "1/k(k+1)"      (/ 1.0d0 (* k (+ 1.0d0 k))))
    (psum "Flint Hills"   (let ((s (sin k)))
                            (/ 1.0d0 (* (* k k k) (* s s))))) 
    (psum "Cookson Hills" (let ((c (cos k)))
                            (/ 1.0d0 (* (* k k k) (* c c)))))
    (psum "Harmonic"      (/ 1.0d0 k))
    (psum "Riemann Zeta"  (/ 1.0d0 (* k k)))
    (let ((a -1.0d0))
      (psum "Alternating Harmonic" 
            (progn (setf a (- a))
                   (/ a k))))
    (let ((a -1.0d0))
      (psum "Gregory" 
            (progn (setf a (- a))
                   (/ a (1- (* 2.0d0 k))))))))
