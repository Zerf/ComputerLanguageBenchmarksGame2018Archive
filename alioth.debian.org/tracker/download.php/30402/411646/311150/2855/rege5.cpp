// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org/
//
// Contributed by Shyamal Prasad
// Modified by Paul Kitchin
// OpenMP by The Anh Tran


#include <iostream>
#include <vector>
#include <sys/mman.h>
#include <boost/regex.hpp>

#include <omp.h>

// This struct encapsules a line of data
struct Chunk
{
	static const int MAX_LEN = 256;

	char data[MAX_LEN];
	size_t data_len;
	
	// pointer to prev/next line, for linear browsing operation
	Chunk* prev;
	Chunk* next;

	Chunk()
	{
		data_len = 0;
		prev = next = 0;
	}
};

typedef std::vector<Chunk*> Sequence;


// This struct represents a linear pointer, for searching patterns
// If inc/dev step surpasses a data line, it'll wrap to prev/next line
struct linear_iterator : public std::iterator<std::bidirectional_iterator_tag, char>
{
	const char*		pdata;
	const Chunk*	dataline;

	linear_iterator() : pdata(0), dataline(0)
	{	}

	linear_iterator(const Sequence& seq)
	{
		dataline = seq[0];
		pdata = dataline->data;
	}
	linear_iterator(const Chunk* c, bool begin = true)
	{
		dataline = c;
		if (c != 0)
			pdata = dataline->data + (begin ? 0 : c->data_len);
		else
			pdata = 0;
	}

	inline bool operator == (const linear_iterator& rhs) const
	{
		return ((dataline == rhs.dataline) && (pdata == rhs.pdata));
	}
	inline bool operator != (const linear_iterator& rhs) const
	{
		return !(*this == rhs);
	}
	inline linear_iterator& operator ++()
	{
		if (__builtin_expect(++pdata >= dataline->data + dataline->data_len, false))
		{
			dataline = dataline->next;
			if (dataline != 0)
				pdata = dataline->data;
			else
				pdata = 0;
		}
		return (*this);
	}
	inline linear_iterator& operator --()
	{
		if (__builtin_expect(--pdata < dataline->data, false))
		{
			dataline = dataline->prev;
			if (dataline != 0)
				pdata = dataline->data + dataline->data_len -1;
			else
				pdata = 0;
		}
		return (*this);
	}
	inline char operator*() const
	{
		return *pdata;
	}
};


// read all redirected data from stdin
Sequence* ReadInput(size_t &size, size_t &szstrip)
{
	size = ftell(stdin);
	fseek(stdin, 0, SEEK_END);
	size = ftell(stdin) - size;
	fseek(stdin, 0, SEEK_SET);

	char* pdata = (char*)mmap(0, size, PROT_READ, MAP_PRIVATE|MAP_POPULATE, fileno(stdin), 0);

	Sequence* seq = new Sequence();
	seq->reserve (size / 60 + 4);

	boost::regex strip("(>[^\\n]*\\n)|(\\n)");
	boost::cmatch mtrs;

	Chunk* c1 = new Chunk();
	const char* psrc = pdata;
	const char* pend = pdata + size;
	szstrip = 0;

	while ( regex_search((const char*)psrc, (const char*)pend, mtrs, strip) )	{
		if (mtrs[2].matched)
		{			c1->data_len = mtrs[0].first - psrc;
			memcpy(c1->data, psrc, c1->data_len);
			szstrip += c1->data_len;
			seq->push_back (c1);

			Chunk* c2 = new Chunk();			c2->prev = c1;			c1->next = c2;
			c1 = c2;
		}
		psrc = mtrs[0].second;	}

	if (c1->prev != 0)		c1->prev->next = 0;	delete c1;

	munmap(pdata, size);	return seq;
}


void PrintPatternsMatch(const Sequence & seq)
{
	static const char* patterns[] = {
		"agggtaaa|tttaccct",
		"[cgt]gggtaaa|tttaccc[acg]",
		"a[act]ggtaaa|tttacc[agt]t",
		"ag[act]gtaaa|tttac[agt]ct",
		"agg[act]taaa|ttta[agt]cct",
		"aggg[acg]aaa|ttt[cgt]ccct",
		"agggt[cgt]aa|tt[acg]accct",
		"agggta[cgt]a|t[acg]taccct",
		"agggtaa[cgt]|[acg]ttaccct"
	};
	static const size_t npt = sizeof(patterns) / sizeof(patterns[0]);
	size_t count_match[npt] = {0};

	// Parallel search, work is divided by search patterns
	#pragma omp parallel for default(shared) schedule(dynamic, 1)
	for (size_t i = 0; i < npt; ++i)
	{
		const boost::regex search_ptn(patterns[i]);
		linear_iterator seq_ite(seq), seq_end;

		typedef boost::regex_iterator<linear_iterator> match_iterator;
		match_iterator mi(seq_ite, seq_end, search_ptn);
		match_iterator me;

		size_t count = 0;
		while (mi != me)
		{
			++count;
			++mi;
		}
		
		count_match[i] = count;
	}

	for (size_t i = 0; i < npt; ++i)
		std::cout << patterns[i] << ' ' << count_match[i] << std::endl;
}


size_t Replace_8mers(Sequence &seq)
{
	static const boost::regex search_ptn[] = 
	{	
		boost::regex("B"),	boost::regex("D"),	boost::regex("H"),	boost::regex("K"),	
		boost::regex("M"),	boost::regex("N"),	boost::regex("R"),	boost::regex("S"),	
		boost::regex("V"),	boost::regex("W"),	boost::regex("Y")
	};

	static const char* replace_ptn[] =
	{
		"(c|g|t)",	"(a|g|t)",	"(a|c|t)",	"(g|t)",	
		"(a|c)",	"(a|c|g|t)","(a|g)",	"(c|t)",	
		"(a|c|g)",	"(a|t)",	"(c|t)"
	};
	size_t n_search_ptn = sizeof(search_ptn)/sizeof(search_ptn[0]);
	static const size_t replace_ptn_len[] = 
	{
		7,	7,	7,	5,	
		5,	9,	5,	5,	
		7,	5,	5
	};

	size_t nline = seq.size();
	size_t new_seq_size = 0;

	// Parallel replace, work is divided by lines
	#pragma omp parallel for default(shared) schedule(dynamic, nline >>9) reduction(+:new_seq_size)
	for (size_t ln = 0; ln < nline; ++ln)	{
		Chunk* c = seq[ln];
		// scan data line for each regex pattern
		for (size_t ptn_idx = 0; ptn_idx < n_search_ptn; ++ptn_idx)		{
			char tmpbuf[Chunk::MAX_LEN];
			size_t deslen = 0;

			const char* psrc = c->data;
			const char* psrc_end = c->data + c->data_len;

			bool changed = false;
			boost::cmatch mt;
			
			while ( regex_search(psrc, psrc_end, mt, search_ptn[ptn_idx]) )
			{
				// copy [prefix - first char matched)
				size_t prefix = mt[0].first - psrc;
				memcpy(tmpbuf + deslen, psrc, prefix);
				deslen += prefix;

				// copy [replace_pattern]
				memcpy(tmpbuf + deslen, replace_ptn[ptn_idx], replace_ptn_len[ptn_idx]);
				deslen += replace_ptn_len[ptn_idx];

				// set pointer to next search position
				psrc = mt[0].second;
				changed = true;
			}

			if (changed)
			{
				// copy [last char matched - end line)
				size_t suffix = psrc_end - psrc;
				memcpy(tmpbuf + deslen, psrc, suffix);
				deslen += suffix;

				// replace existing data line
				memcpy(c->data, tmpbuf, deslen);
				c->data_len = deslen;
				c->data[deslen] = 0;
			}
		}

		// update new sequence size
		new_seq_size += c->data_len;
	}

	return new_seq_size;
}


int main()
{
	size_t initial_length = 0, strip_length = 0;
	Sequence* seq = ReadInput (initial_length, strip_length);

	PrintPatternsMatch(*seq);

	std::size_t replace_8m_length = Replace_8mers (*seq);

	std::cout << std::endl
		<< initial_length		<< std::endl
		<< strip_length		<< std::endl
		<< replace_8m_length	<< std::endl;

	return 0;
}

