--
-- The Great Computer Language Shootout
-- http://shootout.alioth.debian.org/
--
-- Written by Philippe Ribet
--
 
class NSIEVE
 
insert
    ARGUMENTS

creation {ANY}
    make

feature {}
    make is
        local
            n: INTEGER_8; flags: BIT_STRING; flags_count: INTEGER
        do
            check
                argument_count = 1 and argument(1).is_integer
            end
            n := argument(1).to_integer.to_integer_8
            check
                n > 1
            end
            flags_count := {INTEGER 1} |<< n
            flags_count := 10000 * flags_count
            create flags.make(flags_count)
            print_line(flags_count, nsieves(flags, flags_count))
            flags_count := flags_count #// 2
            flags.clear_all
            print_line(flags_count, nsieves(flags, flags_count))
            flags_count := flags_count #// 2
            flags.clear_all
            print_line(flags_count, nsieves(flags, flags_count))
        end

    nsieves (flags: BIT_STRING; flags_count: INTEGER) : INTEGER is
        local
            i, j: INTEGER
        do
            from
                i := 2
            until
                i = flags_count
            loop
                if not flags.item(i) then
                    from
                        j := i + i
                    until
                        j >= flags_count
                    loop
                        flags.put(True, j)
                        j := j + i
                    end
                    Result := Result + 1
                end
                i := i + 1
            end
        end

    print_line (flags_count, nsieves_result: INTEGER) is
        local
            buffer: STRING
        do
            buffer := once "........... local STRING buffer ...................."
            buffer.copy(once "Primes up to ")
            flags_count.append_in_format(buffer, 8)
            buffer.extend(' ')
            nsieves_result.append_in_format(buffer, 8)
            buffer.extend('%N')
            standard_streams.std_output.put_string(buffer)
        end

end
