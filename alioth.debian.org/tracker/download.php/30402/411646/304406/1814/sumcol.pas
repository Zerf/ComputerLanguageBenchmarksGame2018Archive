{ The Great Computer Language Shootout
  http://shootout.alioth.debian.org

  contributed by Ales Katona
  modified by Daniel Mantione
}

program sumcol;

var num, tot: longint;
    textbuf:array[0..8191] of char;

begin
  settextbuf(input,textbuf);
  while not eof do begin
    ReadLn(input, num);
    inc(tot,num);
  end;
  WriteLn(tot);
end.
