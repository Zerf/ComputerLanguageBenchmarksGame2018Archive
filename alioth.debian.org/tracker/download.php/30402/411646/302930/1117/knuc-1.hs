-- The Computer Language Shootout
-- http://shootout.alioth.debian.org/
--
-- Created by Chris Kuklewicz and Don Stewart
--
-- compile with: ghc -O3 -optc-O3 -fasm -fglasgow-exts knuc-1.hs -o knuc-1.ghc_run
--
-- run with: ./knuc-1.ghc_run +RTS -H40m -RTS %A

import GHC.Exts
import GHC.IOBase
import Control.Monad
import Foreign
import Text.Printf(printf)
import Data.List(isPrefixOf,sortBy)
import Data.Maybe(fromMaybe)
import Data.Char(ord,chr,toUpper)
import qualified Data.HashTable as T

searchStrings = ["GGT","GGTA","GGTATT","GGTATTTTAATT","GGTATTTTAATTTATAGT"]

main = do section <- getSection ">THREE"
          mapM_ (writeFreqs section) [1,2]
          mapM_ (writeFrame section) =<< mapM stringToSeq searchStrings

getSection prefix = do findPrefix
                       baseArray <- newArray0 0 =<< getRest =<< skipComments
                       size <- lengthArray0 0 baseArray
                       return (size,baseArray)
  where findPrefix = do line <- getLine; unless (isPrefixOf prefix line) findPrefix
        skipComments = do line <- getLine
                          if ';' == head line then skipComments else return line
        getRest line = liftM asBases getContents
          where asBases = (concatMap c2b).(takeWhile (('>'/=).head)).(line:).lines

newTable :: Int -> IO (T.HashTable (Ptr Word8) Int)
newTable frameSize = T.new (eqSeq frameSize) (hashSeq frameSize)

-- (countFreq (size,bases)) satisfies the requirement to "define a
-- procedure/function to update a hashtable of k-nucleotide keys and
-- count values, for a particular reading-frame"
countFreq (size,bases) frameSize table = mapSeq >> return table
  where mapSeq = mapM_ (countFrame . (advancePtr bases)) [0..(size-frameSize)]
        countFrame frame = do mOld <- T.lookup table frame
                              T.update table frame $! maybe 1 succ mOld

writeFreqs sb@(size,_) frameSize = do
  let printBSF (bs,f) = printf "%s %.3f\n" (showSeq frameSize bs) (percent f)
        where percent n = (100 * (fromIntegral n) / total) :: Double
              total = fromIntegral (size - frameSize + 1)
      freqAndKey (k1,x) (k2,y) = case compare y x of 
        EQ -> compare (showSeq frameSize k1) (showSeq frameSize k2)
        lt'gt -> lt'gt
  unsorted <- T.toList =<< countFreq sb frameSize =<< newTable frameSize
  mapM_ printBSF (sortBy freqAndKey unsorted) >> putChar '\n'

writeFrame sb@(size,_) (frameSize,frameSeq) = do
  mAnswer <- flip T.lookup frameSeq =<< countFreq sb frameSize =<< newTable frameSize
  putStrLn $ (show $ fromMaybe 0 mAnswer) ++ ('\t' : (showSeq frameSize frameSeq))

c2b = map (toEnum . ord . toUpper)

stringToSeq str = liftM ((,) (length str)) (newArray0 0 (c2b str))

showSeq fs ptr = unsafePerformIO $ peekArray fs ptr >>= return.(map (chr . fromEnum))

-- --
-- -- Performance tweaked routines for (HashTable Seq Int)
-- --

{-# INLINE inlinePerformIO #-}
inlinePerformIO (IO m) = case m realWorld# of (# _, r #) -> r

hashSeq (I# frameSize) (Ptr ptr) = inlinePerformIO $ IO $ (\s -> hashmem frameSize ptr 0# s)

{-# INLINE hashmem #-}
hashmem remainingSize ptr runningHash s = if remainingSize ==# 0# then (# s, toEnum (I# runningHash) #)
  else case readInt8OffAddr# ptr 0# s of { (# s, i8  #) -> 
         hashmem (remainingSize -# 1#) (plusAddr# ptr 1#) ((-1640531527#) *# runningHash +# i8) s }

eqSeq (I# frameSize) (Ptr ptr1) (Ptr ptr2) = inlinePerformIO $ IO $ (\s -> eqmem frameSize ptr1 ptr2 s)

{-# INLINE eqmem #-}
eqmem remainingSize ptr1 ptr2 s = if remainingSize ==# 0# then (# s , True #)
  else case readInt8OffAddr# ptr1 0# s of { (# s, i8a #) ->
       case readInt8OffAddr# ptr2 0# s of { (# s, i8b #) ->
              if i8a /=# i8b then (# s, False #)
              else eqmem (remainingSize -# 1#) (plusAddr# ptr1 1#) (plusAddr# ptr2 1#) s } }
