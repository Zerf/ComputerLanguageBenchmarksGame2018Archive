/*The Computer Language Benchmarks Game
  http://shootout.alioth.debian.org/

  contributed by Paolo Bonzini
  further optimized by Jason Garrett-Glaser
*/

#include <stdio.h>
#include <stdlib.h>

typedef float v4sf __attribute__ ((vector_size(16))); // vector of four floats
typedef short v8si __attribute__ ((vector_size(16))); // vector of eight 16-bit ints

int main (int argc, char **argv)
{
    int w, h, bit_num = 0;
    char byte_acc = 0;
    int i, iter = 50;
    float x, y;
    v4sf zero = { 0.0, 0.0, 0.0, 0.0 };
    v4sf four = { 4.0, 4.0, 4.0, 4.0 };
    v4sf nzero = -zero;

    /* Detect endianness.  */
    v8si mask = (v8si)nzero;
    short *pmask = (short *) &mask;
    if (pmask[1]) pmask++;

    w = h = atoi(argv[1]);

    char *data = malloc((w/4+2)*sizeof(char));
    
    float inverse_w = 2.0 / w;
    float inverse_h = 2.0 / h;

    printf("P4\n%d %d\n",w,h);

    for(y=0;y<h;++y)
    {
        for(bit_num=0,x=0;x<w;x+=4)
        {
    	    v4sf Crv = { x*inverse_w-1.5, (x+1.0)*inverse_w-1.5, (x+2.0)*inverse_w-1.5, (x+3.0)*inverse_w-1.5 };
    	    v4sf Civ = { y*inverse_h-1.0, y*inverse_h-1.0, y*inverse_h-1.0, y*inverse_h-1.0 };
    	    v4sf Zrv = { 0.0, 0.0, 0.0, 0.0 };
    	    v4sf Ziv = { 0.0, 0.0, 0.0, 0.0 };
            v4sf Trv = { 0.0, 0.0, 0.0, 0.0 };
    	    v4sf Tiv = { 0.0, 0.0, 0.0, 0.0 };
            i = 0;

            do {
                Ziv = (Zrv*Ziv) + (Zrv*Ziv) + Civ;
                Zrv = Trv - Tiv + Crv;
                Trv = Zrv * Zrv;
                Tiv = Ziv * Ziv;

                /* sign bit zeroed if 4.0 - Trv - Tiv >= 0.0 (i.e. |Z| <= 4.0).  */
                v4sf delta = four - Trv - Tiv;
                mask = (v8si)delta & (v8si)nzero;
            } while (++i < iter && !(pmask[0] & pmask[2] & pmask[4] & pmask[6]));

            byte_acc <<= 4;
            if(!pmask[0])
                byte_acc |= 0x08;
            if(!pmask[2])
                byte_acc |= 0x04;
            if(!pmask[4])
                byte_acc |= 0x02;
            if(!pmask[6])
                byte_acc |= 0x01;
            bit_num+=4;

            if(!(bit_num&7)) {
                data[(bit_num>>3) - 1] = byte_acc;
                byte_acc = 0;
            }
        }

        if(bit_num&7) {
            byte_acc <<= (8-w%8);
            bit_num += 8;
            data[bit_num>>3] = byte_acc;
            byte_acc = 0;
        }
        fwrite(data, 1, (bit_num>>3), stdout);
    }
    free(data);
    return 0;
}

