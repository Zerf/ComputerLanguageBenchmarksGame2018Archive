#!/usr/bin/python -OO
# The Computer Language Shootout Benchmarks
# http://shootout.alioth.debian.org/
#
# contributed by Kevin Carson
# modified by Tupteq

import sys
from math import sqrt

pi = 3.14159265358979323
solar_mass = 4 * pi * pi
days_per_year = 365.24


def advance(bodies, dt):
    bodies2 = bodies[1:]
    for b in bodies:
        for b2 in bodies2:
            dx = b['x'] - b2['x']
            dy = b['y'] - b2['y']
            dz = b['z'] - b2['z']
            distance = sqrt(dx*dx + dy*dy + dz*dz)
            distance3 = distance * distance * distance

            b_mass_x_mag = dt * b['mass'] / distance3
            b2_mass_x_mag = dt * b2['mass'] / distance3

            b['vx'] -= dx * b2_mass_x_mag
            b['vy'] -= dy * b2_mass_x_mag
            b['vz'] -= dz * b2_mass_x_mag
            b2['vx'] += dx * b_mass_x_mag
            b2['vy'] += dy * b_mass_x_mag
            b2['vz'] += dz * b_mass_x_mag
        del bodies2[:1]

    for b in bodies:
        b['x'] += dt * b['vx']
        b['y'] += dt * b['vy']
        b['z'] += dt * b['vz']

def energy(bodies):
    e = 0.0
    bodies2 = bodies[1:]
    for b in bodies:
        e += 0.5 * b['mass'] * (b['vx']*b['vx'] + b['vy']*b['vy'] + b['vz']*b['vz'])
        for b2 in bodies2:
            dx = b['x'] - b2['x']
            dy = b['y'] - b2['y']
            dz = b['z'] - b2['z']
            distance = sqrt(dx*dx + dy*dy + dz*dz)
            e -= (b['mass'] * b2['mass']) / distance
        del bodies2[:1]

    return e

def offset_momentum(bodies):
    global sun
    px = py = pz = 0.0

    for b in bodies:
        px += b['vx'] * b['mass']
        py += b['vy'] * b['mass']
        pz += b['vz'] * b['mass']

    sun['vx'] = - px / solar_mass
    sun['vy'] = - py / solar_mass
    sun['vz'] = - pz / solar_mass

sun = dict(x=0.0, y=0.0, z=0.0, vx=0.0, vy=0.0, vz=0.0, mass=solar_mass)
jupiter = dict(
    x = 4.84143144246472090e+00,
    y = -1.16032004402742839e+00,
    z = -1.03622044471123109e-01,
    vx = 1.66007664274403694e-03 * days_per_year,
    vy = 7.69901118419740425e-03 * days_per_year,
    vz = -6.90460016972063023e-05 * days_per_year,
    mass = 9.54791938424326609e-04 * solar_mass)

saturn = dict(
    x = 8.34336671824457987e+00,
    y = 4.12479856412430479e+00,
    z = -4.03523417114321381e-01,
    vx = -2.76742510726862411e-03 * days_per_year,
    vy = 4.99852801234917238e-03 * days_per_year,
    vz = 2.30417297573763929e-05 * days_per_year,
    mass = 2.85885980666130812e-04 * solar_mass)

uranus = dict(
    x = 1.28943695621391310e+01,
    y = -1.51111514016986312e+01,
    z = -2.23307578892655734e-01,
    vx = 2.96460137564761618e-03 * days_per_year,
    vy = 2.37847173959480950e-03 * days_per_year,
    vz = -2.96589568540237556e-05 * days_per_year,
    mass = 4.36624404335156298e-05 * solar_mass)

neptune = dict(
    x = 1.53796971148509165e+01,
    y = -2.59193146099879641e+01,
    z = 1.79258772950371181e-01,
    vx = 2.68067772490389322e-03 * days_per_year,
    vy = 1.62824170038242295e-03 * days_per_year,
    vz = -9.51592254519715870e-05 * days_per_year,
    mass = 5.15138902046611451e-05 * solar_mass)


def main():
    try:
        n = int(sys.argv[1])
    except:
        print "Usage: %s <N>" % sys.argv[0]
        sys.exit(2)

    bodies = [sun, jupiter, saturn, uranus, neptune]
    offset_momentum(bodies)

    print "%.9f" % energy(bodies)

    for i in xrange(n):
        advance(bodies, 0.01)

    print "%.9f" % energy(bodies)

if __name__ == '__main__':
    main()
