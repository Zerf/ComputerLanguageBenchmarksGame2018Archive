#!/usr/bin/python -OO
# The Computer Language Shootout Benchmarks
# http://shootout.alioth.debian.org/
#
# contributed by Kevin Carson
# modified by Taro Ogawa
import sys

X, Y, Z, VX, VY, VZ, MASS = 0, 1, 2, 3, 4, 5, 6
pi = 3.14159265358979323
solar_mass = 4 * pi * pi
days_per_year = 365.24

def body():
    return [0, 0, 0, 0, 0, 0, 0]

def advance(bodies, dt):
    n = len(bodies)
    for i in xrange(n):
        b = bodies[i]
        b_x = b[X]
        b_y = b[Y]
        b_z = b[Z]
        b_mass = b[MASS]

        for j in xrange(i+1, n):
            b2 = bodies[j]

            dx = b_x - b2[X]
            dy = b_y - b2[Y]
            dz = b_z - b2[Z]
            distance = (dx*dx + dy*dy + dz*dz)**0.5

            dt_over_distcubed = dt/(distance*distance*distance)
            b_mass_x_mag = dt_over_distcubed * b_mass
            b2_mass_x_mag = dt_over_distcubed * b2[MASS]

            b[VX] -= dx * b2_mass_x_mag
            b[VY] -= dy * b2_mass_x_mag
            b[VZ] -= dz * b2_mass_x_mag
            b2[VX] += dx * b_mass_x_mag
            b2[VY] += dy * b_mass_x_mag
            b2[VZ] += dz * b_mass_x_mag

    for b in bodies:
        b[X] += dt * b[VX]
        b[Y] += dt * b[VY]
        b[Z] += dt * b[VZ]

def energy(bodies) :
    e = 0.0
    n = len(bodies)
    for i in xrange(n):
        b = bodies[i]
        b_vx = b[VX]
        b_vy = b[VY]
        b_vz = b[VZ]
        e += 0.5 * b[MASS] * (b_vx*b_vx + b_vy*b_vy + b_vz*b_vz)

        for j in xrange(i+1, n):
            b2 = bodies[j]

            dx = b[X] - b2[X]
            dy = b[Y] - b2[Y]
            dz = b[Z] - b2[Z]
            distance = (dx*dx + dy*dy + dz*dz)**0.5

            e -= (b[MASS] * b2[MASS]) / distance
    return e

def offset_momentum(bodies):
    px = py = pz = 0.0

    for b in bodies:
        px += b[VX] * b[MASS]
        py += b[VY] * b[MASS]
        pz += b[VZ] * b[MASS]

    sun[VX] = -px / solar_mass
    sun[VY] = -py / solar_mass
    sun[VZ] = -pz / solar_mass

def run(n, bodies):
    offset_momentum(bodies)
    print "%.9f" % energy(bodies)

    for i in xrange(n):
        advance(bodies, 0.01)
    print "%.9f" % energy(bodies)


if __name__ == "__main__":
    sun = body()
    sun[X] = sun[Y] = sun[Z] = sun[VX] = sun[VY] = sun[VZ] = 0.0
    sun[MASS] = solar_mass

    jupiter = body()
    jupiter[X] = 4.84143144246472090e+00
    jupiter[Y] = -1.16032004402742839e+00
    jupiter[Z] = -1.03622044471123109e-01
    jupiter[VX] = 1.66007664274403694e-03 * days_per_year
    jupiter[VY] = 7.69901118419740425e-03 * days_per_year
    jupiter[VZ] = -6.90460016972063023e-05 * days_per_year
    jupiter[MASS] = 9.54791938424326609e-04 * solar_mass

    saturn = body()
    saturn[X] = 8.34336671824457987e+00
    saturn[Y] = 4.12479856412430479e+00
    saturn[Z] = -4.03523417114321381e-01
    saturn[VX] = -2.76742510726862411e-03 * days_per_year
    saturn[VY] = 4.99852801234917238e-03 * days_per_year
    saturn[VZ] = 2.30417297573763929e-05 * days_per_year
    saturn[MASS] = 2.85885980666130812e-04 * solar_mass

    uranus = body()
    uranus[X] = 1.28943695621391310e+01
    uranus[Y] = -1.51111514016986312e+01
    uranus[Z] = -2.23307578892655734e-01
    uranus[VX] = 2.96460137564761618e-03 * days_per_year
    uranus[VY] = 2.37847173959480950e-03 * days_per_year
    uranus[VZ] = -2.96589568540237556e-05 * days_per_year
    uranus[MASS] = 4.36624404335156298e-05 * solar_mass

    neptune = body()
    neptune[X] = 1.53796971148509165e+01
    neptune[Y] = -2.59193146099879641e+01
    neptune[Z] = 1.79258772950371181e-01
    neptune[VX] = 2.68067772490389322e-03 * days_per_year
    neptune[VY] = 1.62824170038242295e-03 * days_per_year
    neptune[VZ] = -9.51592254519715870e-05 * days_per_year
    neptune[MASS] = 5.15138902046611451e-05 * solar_mass

    try:
        n = int(sys.argv[1])
    except:
        n = 1000
    run(n, [sun, jupiter, saturn, uranus, neptune])
