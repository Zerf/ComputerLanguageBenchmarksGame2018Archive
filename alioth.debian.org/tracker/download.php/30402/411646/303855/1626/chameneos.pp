{ The Computer Language Shootout
  http://shootout.alioth.debian.org

  contributed by Marc Weustink
}
program chameneos;
{$mode objfpc}{$h-}
uses
  PThreads;

type
  TColor = (Blue, Red, Yellow, Faded);
  
var                      
  MeetingSem, ReadySemA, ReadySemB: TSemaphore;
  MeetingBarrier: TPthreadBarrier;
  MeetingsLeft: Integer;
  ThreadInfo: array[0..3] of record
    Id: TThreadId;
    StartColor: TColor;
    Count: Integer;
  end;
  ColorA, ColorB: TColor;

function ThreadFunc(AIndex: PtrInt): Pointer; cdecl;
const
  COMPL: array[Blue..Yellow, Blue..Yellow] of TColor = (
    (Blue, Yellow, Red),(Yellow, Red, Blue), (Red, Blue, Yellow));
var
  Color: TColor;
  Meetings: Integer;
begin
  Color := ThreadInfo[AIndex].StartColor;
  Meetings := 0;
  
  repeat
    sem_wait(MeetingSem);
    if MeetingsLeft = 0 then Break;

    if pthread_barrier_wait(MeetingBarrier) = 0 then begin
      // the one
      ColorA := Color;
      Dec(MeetingsLeft);
      sem_post(ReadySemA);
      sem_wait(ReadySemB);
      Color := COMPL[Color, ColorB];
      sem_post(MeetingSem);
    end else begin
      // the other
      ColorB := Color;
      sem_post(ReadySemB);
      sem_wait(ReadySemA);
      Color := COMPL[Color, ColorA];
      sem_post(MeetingSem);
    end;
    Inc(Meetings);
  until MeetingsLeft = 0;
  Color := Faded;
  ThreadInfo[AIndex].Count := Meetings;
end;
    

const
  COLOR: array[0..3] of TColor = (Blue, Red, Yellow, Blue);
    
var
  n: Integer;
  Attr: TThreadAttr;
  p: Pointer;
begin
  Val(paramstr(1), MeetingsLeft, n);
  if n <> 0 then exit;
  
  sem_init(MeetingSem, 0, 2);
  sem_init(ReadySemA, 0, 0);
  sem_init(ReadySemB, 0, 0);

  pthread_attr_init(Attr);
  pthread_attr_setdetachstate(Attr, 0);
  pthread_attr_setstacksize(Attr, 1024 * 16);
  
  pthread_barrier_init(@MeetingBarrier, nil, 2);
  
  for n := 0 to 3 do begin
    ThreadInfo[n].Count := 0;
    ThreadInfo[n].StartColor := COLOR[n];
    pthread_create(ThreadInfo[n].Id, Attr, TStartRoutine(@ThreadFunc), Pointer(n));
  end;
  
  for n := 0 to 3 do
    pthread_join(ThreadInfo[n].Id, p);

  WriteLN(ThreadInfo[0].Count + ThreadInfo[1].Count + ThreadInfo[2].Count + ThreadInfo[3].Count);
end.
