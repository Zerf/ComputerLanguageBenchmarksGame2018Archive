{-# OPTIONS -O2 -optc-O3 -optc-ffast-math -fbang-patterns -fexcess-precision -fglasgow-exts #-}
--
-- The Computer Language Benchmarks Game
-- http://shootout.alioth.debian.org/
--
-- Contributed by Don Stewart
-- A lazy bytestring solution.
-- Some more optimizations by Sterling Clover, 11/07
--
-- Add:
-- -optc-mfpmath=sse -optc-msse2
-- -funfolding-use-threshold=32 -O2 -optc-O3

import System.Environment
import Data.Word
import Control.Arrow
import Data.List
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString.Lazy.Char8 as C (pack,unfoldr)
import qualified Data.ByteString as S
import Data.ByteString.Base
import GHC.Int
import GHC.Base
import GHC.Float

main = do
    n <- getArgs >>= readIO . head
    writeFasta  "ONE"   "Homo sapiens alu"       (n*2) (L.cycle alu)
    g <- unfold "TWO"   "IUB ambiguity codes"    (n*3) (look iubs) (rands 42#)
    unfold      "THREE" "Homo sapiens frequency" (n*5) (look homs) g

------------------------------------------------------------------------
--
-- lazily unfold the randomised dna sequences
--

unfold l t n f !g = putStrLn (">" ++ l ++ " " ++ t) >> unroll f g n

unroll :: ([Float] -> (Word8, [Float])) -> [Float] -> Int -> IO [Float]
unroll f = loop
    where
        loop !r 0   = return r
        loop !r !i = case S.unfoldrN m (Just . f) r of
                        (!s, Just r') -> do
                            S.putStrLn s
                            loop r' (i-m)
          where m = min i 60

look :: [(Word8,Float)] -> [Float] -> (Word8,[Float])
look ds (!r:rs) = (choose ds r, rs)

choose :: [(Word8,Float)] -> Float -> Word8
choose [(b,_)]       _ = b
choose ((!b,!f):xs) p = if p < f then b else choose xs p

------------------------------------------------------------------------
--
-- only demand as much of the infinite sequence as we require

writeFasta label title n s = do
     putStrLn $ ">" ++ label ++ " " ++ title
     let (t:ts) = L.toChunks s
     go ts t n
  where
     go ss !s !n
        | l60 && n60 = S.putStrLn l               >> go ss        r (n-60)
        |        n60 = S.putStr s >> S.putStrLn a >> go (tail ss) b (n-60)
        | n <= ln    = S.putStrLn (S.take n s)
        | otherwise  = S.putStr s >> S.putStrLn (S.take (n-ln) (head ss))
        where
            !ln   = S.length s
            !l60  = ln >= 60
            !n60  = n  >= 60
            (l,r) = S.splitAt 60 s
            (a,b) = S.splitAt (60-ln) (head ss)

------------------------------------------------------------------------

rand :: Int# -> (# Float, Int# #)
rand seed = (# newran, newseed #)
    where
        newseed = (seed *# 3877# +# 29573#) `remInt#` 139968#
        newran  = F# (int2Float# newseed `divideFloat#` 139968.0#)

rands :: Int# -> [Float]
rands x = let (# r, s #) = rand x in r : rands s

------------------------------------------------------------------------

alu = C.pack
        "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG\
        \GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA\
        \CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT\
        \ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA\
        \GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG\
        \AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC\
        \AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA"

makeCumul :: [(Word8,Float)]->[(Word8,Float)]
makeCumul freqMap = tail . reverse . foldl' fm' [(undefined,0)] $ freqMap
    where fm' acc@((_,ct):rst) (w,f) = (w,ct + f) : acc

iubs :: [(Word8,Float)]
iubs = makeCumul $ map (first c2w)
        [('a',0.27),('t',0.27),('c',0.12),('g',0.12),('B',0.02)
        ,('D',0.02),('H',0.02),('K',0.02),('M',0.02),('N',0.02)
        ,('R',0.02),('S',0.02),('V',0.02),('W',0.02),('Y',0.02)]

homs :: [(Word8,Float)]
homs = makeCumul $ map (first c2w)
        [('a',0.3029549426680),('t',0.3015094502008)
        ,('g',0.1975473066391),('c',0.1979883004921)]