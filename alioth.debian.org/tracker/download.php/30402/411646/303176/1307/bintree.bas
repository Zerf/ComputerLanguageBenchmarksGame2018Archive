' The Computer Language Shootout
' http://shootout.alioth.debian.org/
'
' Converted from the gcc benchmark contributed by Kevin Carson
' FreeBASIC conversion by Simon Nash(yetifoot)

Type treeNode
  _left As treeNode ptr
  _right As treeNode ptr
  item As Long
End Type

Function NewTreeNode(_left As treeNode ptr,_
                     _right As treeNode ptr, item As Long) As treeNode ptr
  Dim new As treeNode ptr

    new = Allocate(sizeof(treeNode))

    new->_left = _left
    new->_right = _right
    new->item = item

    Return new
End Function

Function ItemCheck(tree As treeNode ptr) As Long
  If tree->_left = NULL Then
      Return tree->item
  Else
      Return tree->item + ItemCheck(tree->_left) - ItemCheck(tree->_right)
  End If
End Function

Function BottomUpTree(item As Long, depth As uInteger) As treeNode ptr
  If (depth > 0) Then
    Return NewTreeNode(BottomUpTree(2 * item - 1, depth - 1),_
                       BottomUpTree(2 * item, depth - 1),_
                       item)
  Else
    Return NewTreeNode(0, 0, item)
  End If
End Function

Sub DeleteTree(tree As treeNode ptr)
  If tree->_left <> NULL Then
    DeleteTree(tree->_left)
    DeleteTree(tree->_right)
  End If

  DeAllocate(tree)
End Sub

  Dim As uInteger N, depth, minDepth, maxDepth, stretchDepth
  Dim As treeNode ptr stretchTree, longLivedTree, tempTree
  Dim As Long i, iterations, check
  
    N = CInt(Command$(1))

    minDepth = 4

    If (minDepth + 2) > N Then
      maxDepth = minDepth + 2
    Else
      maxDepth = N
    End If

    stretchDepth = maxDepth + 1
    stretchTree = BottomUpTree(0, stretchDepth)
    Print "stretch tree of depth " & stretchDepth & chr(9) & " check: " & ItemCheck(stretchTree) & chr(10);

    DeleteTree(stretchTree)

    longLivedTree = BottomUpTree(0, maxDepth)

    For depth = minDepth To maxDepth Step 2
      iterations = 2 ^ (maxDepth - depth + minDepth)
      check = 0
      For i = 1 To iterations
          tempTree = BottomUpTree(i, depth)
          check += ItemCheck(tempTree)
          DeleteTree(tempTree)

          tempTree = BottomUpTree(-i, depth)
          check += ItemCheck(tempTree)
          DeleteTree(tempTree)
      Next i
      Print Str(iterations * 2) & chr(9) & " trees of depth " & depth & chr(9) & " check: " & check & chr(10);
    Next depth
    
    Print "long lived tree of depth " & maxDepth & chr(9) &" check: " & ItemCheck(longLivedTree) & chr(10);
