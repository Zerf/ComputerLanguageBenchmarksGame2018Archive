// The Great Computer Language Shootout
// http://shootout.alioth.debian.org/
//
// Compile: g++ -O2 -fomit-frame-pointer -fopenmp -msse2 -march=native -mtune=k8 -mfpmath=sse -o spectralnorm spectralnorm.cpp
//
// Original C contributed by Sebastien Loisel
// Conversion to C++ by Jon Harrop
// OpenMP parallelize by The Anh Tran

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <algorithm>

#include <omp.h>
#include <sched.h>


namespace Barrier
{
	int nthreads = 0;
	volatile int barrier = 0;
	volatile int junk = 0;

	void Sync()
	{
		__sync_fetch_and_sub(&barrier, 1);
	
		while ((0 < barrier) && (barrier < nthreads))
			++junk;
			//sched_yield();

		barrier = nthreads;
	}
}


double eval_A(int i, int j) 
{
	// 1.0 / (i + j) * (i + j +1) / 2 + i + 1;
	// n * (n+1) is even number. Therefore, just (>> 1) for (/2)
	int d = (	( (i + j) * (i + 1 +j) ) >> 1	) 	+ i + 1;

	return 1.0 / d; 
}

void eval_A_times_u (int N, const double u[], double Au[])
{
	// this var is shared by N threads
	volatile static int ite_A = 0;
	
	int i;
	while ((i = __sync_fetch_and_add(&ite_A, 1)) < N)	// atomic access
	{
		double sum = 0.0;
		for (int j = 0; j < N; j++)
			sum += eval_A(i, j) * u[j];
		Au[i] = sum;
	}

	Barrier::Sync();
	ite_A = 0;	// reset for next loop
}


void eval_At_times_u(int N, const double u[], double Au[])
{
	volatile static int ite_At = 0;

	int i;
	while ((i = __sync_fetch_and_add(&ite_At, 1)) < N)
	{
		double sum = 0.0;
		for (int j = 0; j < N; j++)
			sum += eval_A(j, i) * u[j];
		Au[i] = sum;
	}

	Barrier::Sync();
	ite_At = 0;
}

void eval_AtA_times_u(int N, const double u[], double AtAu[], double v[])
{
	eval_A_times_u( N, u, v); 
	eval_At_times_u( N, v, AtAu ); 
}


int GetThreadCount()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	for (int i = 0; i < 8; i++)
	{
		if (CPU_ISSET(i, &cs))
			count++;
	}
	return count;
}

double spectral_game(int N)
{
	double u[N];
	double tmp[N];
	double v[N];

	double vBv	= 0.0;
	double vv	= 0.0;

	// fill 1.0	
	// only 44kB, fit in L2 cache, not parallel worthy
	for (int i = 0; i < N; i++)
		u[i] = 1.0;

	Barrier::barrier = Barrier::nthreads = GetThreadCount();
	#pragma omp parallel default(shared) num_threads(Barrier::nthreads)
	{
		for (int ite = 0; ite < 10; ite++) 
		{
			eval_AtA_times_u(N, u, v, tmp);
			eval_AtA_times_u(N, v, u, tmp);
		}

		#pragma omp for schedule(static) reduction( + : vBv, vv ) nowait
		for (int i = 0; i < N; i++) 
		{
			vv += v[ i ] * v[ i ];
			vBv += u[ i ] * v[ i ];
		}
	} // end parallel region

	return sqrt( vBv / vv );
}


int main(int argc, char *argv[])
{
	int N = ((argc >= 2) ? atoi(argv[1]) : 2000);

	printf("%.9f\n", spectral_game(N));
	return 0;
}

