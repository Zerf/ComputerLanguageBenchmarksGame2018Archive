{-# LANGUAGE BangPatterns #-}
{-# OPTIONS_GHC -O2 #-}

-- The Computer Language Benchmarks Game
-- http://shootout.alioth.debian.org/
--
-- Stephen Blackheath's re-write (with some bits taken from Don Stewart's version)

import Text.Printf
import Data.ByteString.Internal
import qualified Data.ByteString.Char8 as S
import Control.Applicative
import Control.Monad
import Control.Concurrent
import Foreign.Storable
import Foreign.Marshal.Alloc
import Foreign.Marshal.Array
import Foreign.Ptr
import Foreign.ForeignPtr
import Data.Word
import Data.Bits
import Data.Char
import Data.List
import Data.Maybe
import Data.IORef

main = do
    genome <- extract (S.pack ">TH")
    let actions = [
                do
                    a <- printFreqsBySize genome 1
                    b <- printFreqsBySize genome 2
                    return $ a ++ b
            ] ++ map (printFreqsSpecific genome) specificSeqs
    output <- concat <$> parallel actions
    forM_ output putStrLn

-- Drop in replacement for sequence
parallel :: [IO a] -> IO [a]
parallel actions = do
    vars <- forM actions $ \action -> do
        var <- newEmptyMVar
        forkOS $ do
            answer <- action
            putMVar var answer
        return var
    forM vars takeMVar

specificSeqs = map S.pack ["GGT","GGTA","GGTATT","GGTATTTTAATT","GGTATTTTAATTTATAGT"]

extract p = do
    s <- S.getContents
    let (_, rem)  = S.breakSubstring p s
    return $! S.map toUpper             -- array fusion!
            . S.filter    ((/=) '\n')
            . S.dropWhile ((/=) '\n') $ rem

printFreqsBySize :: S.ByteString -> Int -> IO [String]
printFreqsBySize genome keySize = do
        ht <- htNew keySize 53
        forGenome genome keySize $ htInc ht
        l <- htToList ht
        htFree ht
        return $ map (draw $ S.length genome) (sortBy sortRule l) ++ [""]
    where
        draw :: Int -> (S.ByteString, Int) -> String
        draw genomeLen (key, count) = printf "%s %.3f" (S.unpack key) pct
            where pct   = (100 * (fromIntegral count) / total) :: Double
                  total = fromIntegral (genomeLen - keySize + 1)

forGenome :: S.ByteString -> Int -> (Ptr Word8 -> IO ()) -> IO ()
forGenome genome keySize f = do
    let (fp, offset, len) = toForeignPtr genome
    withForeignPtr fp $ \p_ -> do
        let p = p_ `plusPtr` offset
        forM_ [0..len-keySize] $ \idx -> do
            let key = p `plusPtr` idx
            f key

printFreqsSpecific :: S.ByteString -> S.ByteString -> IO [String]
printFreqsSpecific genome seq = do
    let keySize = S.length seq
    ht <- htNew keySize 24593
    forGenome genome keySize $ htInc ht
    let (fp, offset, len) = toForeignPtr seq
    count <- withForeignPtr fp $ \p_ -> do
        htGet ht (p_ `plusPtr` offset)
    htFree ht
    return [show count ++ ('\t' : S.unpack seq)]

sortRule :: (S.ByteString, Int) -> (S.ByteString, Int) -> Ordering
sortRule (a1, b1) (a2, b2) =
    case compare b2 b1 of
        EQ -> compare a1 a2
        x  -> x
                  
------ Hash table implementation ----------------------------------------------

-- Note: Hash tables are not generally used in functional languages, so there
-- are no available library implementations for Haskell.  This benchmark
-- requires a hash table.  This is why I have implemented the hash table here.

htNew :: Int -> Int -> IO Hashtable
htNew ksz slots = do
    let ssz = spineSize ksz slots
    sp <- mallocBytes ssz
    memset sp 0 (fromIntegral ssz)
    return $ Hashtable {
            keySize   = ksz,
            noOfSlots = slots,
            spine     = sp
        }

htFree :: Hashtable -> IO ()
htFree ht = do
    forSpine ht $ \entry -> do
        ch <- peek $ keyOf entry
        if ch == 0 then return ()  -- empty slot
                   else do
                       linked <- tail <$> allLinked entry
                       forM_ linked $ \(Entry ePtr) -> free ePtr
    free (spine ht)

htGet :: Hashtable -> Ptr Word8 -> IO Int
htGet ht key = htPayload ht key >>= peek

htInc :: Hashtable -> Ptr Word8 -> IO ()
htInc ht key = do
    pPayload <- htPayload ht key
    value <- peek pPayload
    poke pPayload (value+1)

htToList :: Hashtable -> IO [(S.ByteString, Int)]
htToList ht =
    htMap (\entry -> do
        keyStr <- keyString ht (keyOf entry)
        payload <- peek (payloadOf entry)
        return (keyStr, payload)) ht

keyString :: Hashtable -> Ptr Word8 -> IO S.ByteString
keyString ht key = S.pack . map w2c <$> peekArray (keySize ht) key

htMap :: (Entry -> IO a) -> Hashtable -> IO [a]
htMap f ht = mapM f =<< htEntries ht

htEntries :: Hashtable -> IO [Entry]
htEntries ht = do
    liftM concat $ forSpine ht $ \entry -> do
        ch <- peek $ keyOf entry
        if ch == 0 then return []  -- empty slot
                   else allLinked entry

allLinked :: Entry -> IO [Entry]
allLinked entry = do
    next <- peek $ nextPtrOf entry
    if next == nullPtr
        then return [entry]
        else do
            others <- allLinked (Entry next)
            return (entry:others)

forSpine :: Hashtable -> (Entry -> IO a) -> IO [a]
forSpine ht f = fs (noOfSlots ht-1) []
    where
        fs 0 acc = do
            v <- f $ indexEntry ht 0
            return (v:acc)
        fs n acc = do
            v <- f $ indexEntry ht n
            fs (n-1) (v:acc)

data Hashtable = Hashtable {
        keySize   :: Int,
        noOfSlots :: Int,
        spine     :: Ptr Word8
    }

wordSize :: Int
wordSize = max (sizeOf (nullPtr :: Ptr Word8)) (sizeOf (0 :: Int))

-- Round up to word size
roundUp :: Int -> Int
roundUp i = (i + wordSize - 1) .&. complement (wordSize - 1)

slotSize :: Int -> Int
slotSize !ksz = roundUp ksz + wordSize * 2

spineSize :: Int -> Int -> Int
spineSize ksz slots = slots * slotSize ksz

calcHash :: Hashtable -> Ptr Word8 -> IO Int
{-# INLINE calcHash #-}
calcHash !ht !key = do
    bytes <- peekArray (keySize ht) (castPtr key :: Ptr Word8)
    return $ foldl' (\h c -> h * 131 + fromIntegral c) 0 bytes `mod` (noOfSlots ht)

newtype Entry = Entry (Ptr Word8)

indexEntry :: Hashtable -> Int -> Entry
indexEntry !ht !hash =
    let hOffset = hash * slotSize (keySize ht)
    in  Entry $ spine ht `plusPtr` hOffset

entryMatches :: Hashtable -> Entry -> Ptr Word8 -> IO Bool
entryMatches !ht !entry !key = do
    let eKey = keyOf entry
    c <- memcmp key eKey (fromIntegral $ keySize ht)
    if c == 0
        then return True
        else do
            ch <- peek eKey
            if ch == 0
                then do
                    memcpy eKey key (fromIntegral $ keySize ht)  -- ick
                    return True
                else
                    return False

nextPtrOf :: Entry -> Ptr (Ptr Word8)
nextPtrOf !(Entry ePtr) = castPtr $ ePtr

payloadOf :: Entry -> Ptr Int
payloadOf !(Entry ePtr) = castPtr $ ePtr `plusPtr` wordSize

keyOf :: Entry -> Ptr Word8
keyOf !(Entry ePtr) = ePtr `plusPtr` (wordSize*2)

allocEntry :: Hashtable -> Ptr Word8 -> IO Entry
allocEntry ht key = do
    let esz = slotSize $ keySize ht
    ePtr <- mallocBytes esz
    memset ePtr 0 (fromIntegral esz)
    let entry = Entry ePtr
    memcpy (keyOf entry) key (fromIntegral $ keySize ht)
    return entry

htPayload :: Hashtable -> Ptr Word8 -> IO (Ptr Int)
htPayload !ht !key = do
        hash <- calcHash ht key
        entry <- findEntry (indexEntry ht hash)
        return $ payloadOf entry
    where
        findEntry :: Entry -> IO Entry
        findEntry !entry = do
            match <- entryMatches ht entry key
            if match
                then
                    return entry
                else do
                    let pNext = nextPtrOf entry
                    next <- peek pNext
                    if next == nullPtr
                        then do
                            newEntry@(Entry ePtr) <- allocEntry ht key
                            poke pNext ePtr
                            return newEntry
                        else
                            findEntry (Entry next)

