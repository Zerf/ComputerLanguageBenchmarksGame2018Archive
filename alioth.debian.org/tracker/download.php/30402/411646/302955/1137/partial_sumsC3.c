/*
The Computer Language Shootout
http://shootout.alioth.debian.org/
Contributed by Mike Pall
Modified for speed by bearophile, Jan 28 2006

Compile with:
-O3 -s -fomit-frame-pointer -funroll-loops -ffast-math
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void compute(int n) {
    register int k = n;
    double sum1 = 0.0, sum2 = 0.0, sum3 = 0.0, sum4 = 0.0, sum5 = 0.0, sum6 = 0.0;

    /*
    Yes, I (Mike Pall) tried using a double as a primary or secondary loop variable.
    But the x86 ABI requires a cleared x87 FPU stack before every call
    (e.g. to pow()) which nullifies any performance gains.
    */
    #define kd ((double)k)

    for(; k--; ) sum1 += pow(2.0/3.0, kd);
    printf("%.9f\t(2/3)^k\n", sum1);

	sum1 = 0.0;
    for (k = 1; k <= n; k++) {
        sum1 += pow(kd, -0.5);
        sum2 += 1.0/(kd*(kd+1.0));
        double sk = sin(kd);
        sum3 += 1.0/(kd*kd*kd * sk*sk);
        double ck = cos(kd);
        sum4 += 1.0/(kd*kd*kd * ck*ck);
        sum5 += 1.0/kd;
        sum6 += 1.0/(kd*kd);
    }
    printf("%.9f\tk^-0.5\n", sum1);
    printf("%.9f\t1/k(k+1)\n", sum2);
    printf("%.9f\tFlint Hills\n", sum3);
    printf("%.9f\tCookson Hills\n", sum4);
    printf("%.9f\tHarmonic\n", sum5);
    printf("%.9f\tRiemann Zeta\n", sum6);

    sum1 = 0.0;
    for (k = 1; k <= n-1; k += 2) sum1 += 1.0/kd;
    for (k = 2; k <= n; k += 2) sum1 -= 1.0/kd;
    printf("%.9f\tAlternating Harmonic\n", sum1);

    sum1 = 0.0;
    for (k = 1; k <= 2*n-1; k += 4) sum1 += 1.0/kd;
    for (k = 3; k <= 2*n; k += 4) sum1 -= 1.0/kd;
    printf("%.9f\tGregory\n", sum1);
}

int main(int argc, char **argv) {
	compute( atoi(argv[1]) );
	return 0;
}
