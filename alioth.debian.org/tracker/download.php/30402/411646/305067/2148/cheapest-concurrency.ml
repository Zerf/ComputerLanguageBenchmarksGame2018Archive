(* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   Contributed by David Teller (David.Teller@univ-orleans.fr)
*)

(** Extremely light-weight threads, aka purely functional coroutines*)
type 'a coroutine = 
  | Done  of 'a                          (**Final result*)
  | Yield of 'a * (unit -> 'a coroutine) (**Intermediate result and continuation*)

(** Initialise the chain of coroutines, then start feeding 0s*)
let create_coroutine ~iterations ~depth = 
  let rec aux = function                                             (*Main creation loop*)
    | 0 ->                       (*I'm the last one, I'll feed 0s, but only so many times*)
	let rec loop = function
	  | 1 -> Done 0
	  | n -> Yield (0, fun () -> loop (n - 1))
	in
	  loop iterations
    | n ->  
	let child = aux (n - 1)                                   (* Create your children*)
	in  (*Eat what you're fed by your child, grow it by one, feed it to your parent  *)
	let rec loop = function
	  | Done   r     -> Done  ( r  + 1 )                          (*  Last iteration *)
	  | Yield (r,co) -> Yield ( (r + 1), fun () -> loop (co ()) ) (*  Not done yet   *)
	in loop child
  in
    aux depth

(** Keep listening for messages until all communication has died*)
let sum channel_end =
  let rec aux total = function
    | Done r        -> total + r
    | Yield (r, co) -> aux (total + r) ( co () )
  in
    aux 0 channel_end 

let () =
  let n  = try int_of_string(Array.get Sys.argv 1) with _ -> 10
  in
    print_int(sum (create_coroutine 500 n));
    print_newline()
