(* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   Contributed by David Teller (David.Teller@univ-orleans.fr)
*)

let make_pipe () =
  let read, write = Unix.pipe() in
    (
      UnixLabels.in_channel_of_descr  read,
      UnixLabels.out_channel_of_descr write
    )

let quit () =
  failwith "Operation complete"

let rec make_chain current_in processes = function
  | 0 -> (current_in, processes)
  | n -> 
      (
	let next_in, next_out = make_pipe () in
	match UnixLabels.fork () with
	  | 0   -> (*The child now becomes a listener*)
	      while true do
		output_binary_int next_out ((input_binary_int current_in) + 1);
		flush next_out
	      done;
	      quit ()
	  | pid  ->  (*...while the parent continues forking*)
	      make_chain next_in (pid::processes) (n - 1)
      )


let sum (channel_in : in_channel) (channel_out:out_channel) = 
  let rec aux (total:int) = function
    | 0 -> total
    | n ->
	output_binary_int channel_out 0;
	flush channel_out;
	aux (total + ( input_binary_int channel_in) ) (n - 1)
  in
    aux 0

let () =
  let n = try int_of_string(Array.get Sys.argv 1) with _ -> 10
  and first_in, first_out = make_pipe ()                in
  let last_in, processes  = make_chain first_in [] 500  in
    print_int(sum last_in first_out n); 
    print_newline();
    ListLabels.iter processes ~f:(fun pid -> UnixLabels.kill ~pid ~signal:Sys.sigkill)
(*    first_close ();
    last_killer ()*)
(*    close_in last_in;
    close_out first_out*)


