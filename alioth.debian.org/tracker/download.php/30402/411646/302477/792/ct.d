/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Mitja Slenc
*/

import std.thread, std.stdio, std.string;

int result=0;

class MyThread : Thread
{
	MyThread next;
	bool haveit;
	int haveval;
	bool stop;

	public this(MyThread next)
	{
		this.next=next;
		haveit=false;
		haveval=0;
		stop=false;
	}

	int run()
	{
		while (!stop) {
			synchronized(this) {
				if (haveit) {
					if (next) {
						next.put(haveval+1);
						haveit=false;
					} else {
						result+=haveval+1;
						haveit=false;
					}
				}
			}
			yield();
		}
		return 0;
	}

	public void put(int val)
	{
		while (true) {
			bool didit=false;
			synchronized(this) {
				if (!haveit) {
					if (val<0) {
						stop=true;
						return;
					}
					haveit=true;
					haveval=val;
					didit=true;
				}
			}
			if (didit)
				break;
			yield();
		}
	}
}


int main(char[][] args)
{
	int nthreads=500;
	int N=args.length > 1 ? atoi(args[1]) : 1;

	MyThread[] threads=new MyThread[nthreads];
	for (int a=0; a<nthreads; a++) {
		threads[a]=new MyThread(a?threads[a-1]:null);
		threads[a].start();
	}
	for (int a=0; a<N; a++) {
		threads[nthreads-1].put(0);
	}
	for (int a=nthreads-1; a>=0; a--) {
		threads[a].put(-1);
		threads[a].wait();
	}
	writefln("%d", result);
	return 0;
}