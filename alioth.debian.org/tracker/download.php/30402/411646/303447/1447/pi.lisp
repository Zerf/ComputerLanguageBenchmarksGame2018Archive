(declaim (optimize (speed 3)))

(defconstant *digits-per-line* 10)
(defconstant *default-stop-digits* "1000")

(defun compute-pi (stop-digits z cur-state)
  (declare (fixnum stop-digits)
	   (type (simple-vector 4) z)
	   (type (simple-array fixnum (4)) cur-state))
  (do ((digits-out 0))
      ((>= digits-out stop-digits))
    (declare (fixnum digits-out))
    (let ((y (extr z 3)))
      (cond ((= y (extr z 4))
	     (format t "~D" y)
	     (when (zerop (mod (incf digits-out) *digits-per-line*))
	       (format t "~A:~D~%" #\Tab digits-out))
	     (update-1 z y))
	    (t (update-2 z cur-state))))))


(defun extr (z x)
  (declare (type (simple-vector 4) z)
	   (type (integer 0 10) x))
  (let* ((z0 (aref z 0)) (z1 (aref z 1)) (z2 (aref z 2)) (z3 (aref z 3))
	 (ret (floor (+ (* z0 x) z1)
		     (+ (* z2 x) z3))))
    (declare (type integer z0 z1 z2 z3) (type (integer 0 10) ret))
    ret))

(defun update-1 (z n)
  (declare (type (simple-vector 4) z)
	   (type (integer 0 10) n))
  (let ((z0 (aref z 0)) (z1 (aref z 1)) (z2 (aref z 2)) (z3 (aref z 3)))
    (declare (type integer z0 z1 z2 z3))
    (setf (aref z 0) (+ (* 10 z0)
			(* -10 n z2))
	  (aref z 1) (+ (* 10 z1)
			(* -10 n z3)))))


(defun update-2 (z cs)
  (declare (type (simple-vector 4) z)
	   (type (simple-array fixnum (4)) cs))
  (let ((z0 (aref z 0)) (z1 (aref z 1)) (z2 (aref z 2)) (z3 (aref z 3))
	(c0 (incf (aref cs 0))) (c1 (incf (aref cs 1) 4)) (c2 (aref cs 2)) (c3 (incf (aref cs 3) 2)))
    (declare (type integer z0 z1 z2 z3)
	     (type fixnum c0 c1 c2 c3))
    (setf (aref z 0) (+ (* z0 c0)
			(* z1 c2))
	  (aref z 1) (+ (* z0 c1)
			(* z1 c3))
	  (aref z 2) (+ (* z2 c0)
			(* z3 c2))
	  (aref z 3) (+ (* z2 c1)
			(* z3 c3)))))



(defun main ()
  (let ((n (parse-integer (or (car (last #+sbcl sb-ext:*posix-argv*
					 #+clisp ext:*args*
					 #+cmu (cdr extensions:*command-line-strings*)
					 #+gcl si::*command-args*)) *default-stop-digits*))))
    (compute-pi n
		(make-array 4 :element-type 'integer :initial-contents #(1 0 0 1))
		(make-array 4 :element-type 'fixnum :initial-contents #(0 2 0 1)))))