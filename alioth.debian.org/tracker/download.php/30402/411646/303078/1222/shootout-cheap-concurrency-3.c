/* [10]
 * compile with -Os -lpthread
 * tested on Debian with NPTL
 * 0.09sec on 700MHz Pentium III
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <pthread.h>	// -lpthread
#include <semaphore.h>

#define THREADS 500

#define STACKSZ (100*1024) // 100K
#define SWL_SLEEP (1000000/1000)

sem_t *s;

void
semwaitlocked(sem_t *sem) // profiling shows this is the bottleneck
{
	int v;
	for (sem_getvalue(sem, &v);
		v > 0;
		usleep(SWL_SLEEP),
		sem_getvalue(sem, &v));
}

void *
work(void *a)
{
	for (;;) {
		/*
		 * sem_wait() can get interrupted, just call it again
		 * TODO: see sys/kern/uipc_sem.c:618 in FreeBSD
		 */
		for (; sem_wait(s) != 0; pthread_yield());
		semwaitlocked(s);
	}
	return NULL;
}

int
main(int argc, char *argv[])
{
	unsigned int times;
	assert(argc == 2 && sscanf(argv[1], "%u", &times) == 1);
	
	int sum = 0;
	pthread_attr_t attr;
	pthread_t t[THREADS];
	int i, j;

	s = malloc(sizeof(sem_t));
	assert(s != NULL);
	assert(sem_init(s, 0, 0) == 0);
	assert(pthread_attr_init(&attr) == 0);
	//size_t sztmp;
	//assert(pthread_attr_getstacksize(&attr, &sztmp) == 0);
	//printf("default stack size: %ub\n", (unsigned int)sztmp);
	assert(pthread_attr_setstacksize(&attr, STACKSZ) == 0);
	for (i = 0; i < THREADS; i++) {
		assert(pthread_create(&t[i], &attr, work, NULL) == 0);
	}
	for (j = 0; j < times; j++) {
		for (i = 0; i < THREADS; i++) {
			/*
			 * from sem_post(3) in freebsd:
			 * > If there are threads blocked on the semaphore
			 * > when sem_post() is called, then the highest
			 * > priority thread that has been blocked the
			 * > longest on the semaphore will be allowed to
			 * > return from sem_wait().
			 */
			sem_post(s);
		}
		semwaitlocked(s);
		sum += THREADS;
	}
	printf("%d\n", sum);

	exit(0);
}

