(*  The Computer Language Benchmark Game
    http://shootout.alioth.debian.org/

    Contributed by Frank Thomsen
    
    Note:   This version uses regular OS-threads since it
            is unclear whether or not we are allowed to
            use the thread pool.
*)

#light
#nowarn "20"

open System.Threading

let ringLength = 503

type RingThread(name:int) =
    static let ring:option<RingThread>[] =  let x = Array.create ringLength None
                                            for i in 0..ringLength-1 do
                                                x.[i] <- Some(new RingThread(i))
                                                x.[i].Value.StartThread()
                                            x
    
    let waitHandle = new AutoResetEvent(false)
    let mutable token = -1
    
    let next() = ring.[(name+1)%ringLength].Value
    let rec threadFunc() =
        waitHandle.WaitOne()
        match token with
        | -1 -> printfn "%d" name
                next().GiveToken(-2) //halt
        | -2 -> next().GiveToken(-2) //halt
        | _ ->  next().GiveToken(token-1)
                threadFunc()
    
    member tfs.Name with get() = name
    member tfs.GiveToken(t:int) =
        token <- t
        waitHandle.Set()
        ()
    member tfs.StartThread() =
        let t = new Thread(ThreadStart(threadFunc))
        t.Start()
        ()
    static member StartRing(startToken:int) = ring.[0].Value.GiveToken(startToken)

RingThread.StartRing(try Int32.of_string Sys.argv.[1] with _ -> 10000000)