#!/usr/bin/python -OO

#
# The Great Computer Language Shootout
# http://shootout.alioth.debian.org/
#
# Contributed by Peter Crabtree
#
# This implementation of the fannkuch benchmark makes heavy
# uses of Pythonic list slicing to do most of the grunt work.
# Note that it is not suitable as a Python-Psyco implemenatation, as
# Psyco is able to much, much better optimize a more "manual" (C-like)
# implementation (one which implements list reversal manually).
#

import sys

def main():
	n = ((len(sys.argv) >= 2) and [int(sys.argv[1])] or [1])[0]
	print "Pfannkuchen(%d) = %ld" % (n, fannkuch(n))

def fannkuch(n):
	perm1 = range(1, n + 1)
	count = [0]*n
	r = n
	maxFlipsCount = 0
	
	while True:
		while r > 1:
			count[r - 1] = r
			r -= 1
		if not (perm1[0] == 1 or perm1[n - 1] == (n - 2)):
			perm = perm1[:]
			flips = 0
			while (perm[0] - 1):
				perm[:perm[0]] = perm[:perm[0]][::-1]
				flips += 1
			maxFlipsCount = max(maxFlipsCount, flips)
		
		while True:
			if r == n:
				return maxFlipsCount
			perm1[:r], perm1[r] = perm1[1:r + 1], perm1[0]
			count[r] -= 1
			if count[r] > 0:
				break
			r += 1
main()