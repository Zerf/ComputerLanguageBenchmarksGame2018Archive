//
// The Computer Language Shootout
// http://shootout.alioth.debian.org/
//
// Contributed by Shyamal Prasad
//


// This implementation of regexdna does not use the POSIX regex
// included with the GNU libc. Instead it uses the Boost C++ libraries
//
// http://www.boost.org/libs/regex/doc/index.html
//
// (On Debian: apt-get install libboost-regex-dev before compiling,
//  and then "g++ -O3 -lboost_regex regexdna.cc -o regexdna
//  Gentoo seems to package boost as, well, 'boost')
//


#include <iostream>
#include <string>
#include <map>
#include <boost/regex.hpp>

// Use plain old arrays for the 8-mers and IUB code alternatives
static const std::string patterns[] = {
  "agggtaaa|tttaccct",
  "[cgt]gggtaaa|tttaccc[acg]",
  "a[act]ggtaaa|tttacc[agt]t",
  "ag[act]gtaaa|tttac[agt]ct",
  "agg[act]taaa|ttta[agt]cct",
  "aggg[acg]aaa|ttt[cgt]ccct",
  "agggt[cgt]aa|tt[acg]accct",
  "agggta[cgt]a|t[acg]taccct",
  "agggtaa[cgt]|[acg]ttaccct"
};

typedef std::pair<std::string, std::string> Pair;
static const Pair alternatives[] = {
  Pair("B", "(c|g|t)"), Pair("D", "(a|g|t)"), Pair("H", "(a|c|t)"), 
  Pair("K", "(g|t)"), Pair("M", "(a|c)"), Pair("N", "(a|c|g|t)"), 
  Pair("R", "(a|g)"), Pair("S", "(c|t)"), Pair("V", "(a|c|g)"), 
  Pair("W", "(a|t)"), Pair("Y", "(c|t)")
};    

int main()
{
  // Read file, remove headers and newlines to get sequence
  std::string line, buf;
  while ( std::getline(std::cin, line) ){
    buf += line;
    buf += '\n'; // FASTA files always add a newline at line end
  }
  const boost::regex lose(">[^\\n]*\\n|\\n");
  std::string sequence = boost::regex_replace(buf, lose, "");


  // Count the number of times patterns occur in sequence
  for (unsigned i = 0; i < sizeof(patterns)/sizeof(std::string); i++) {
    const boost::regex e(patterns[i]);
    boost::sregex_iterator m(sequence.begin(), sequence.end(), e);
    const boost::sregex_iterator end;
 
    unsigned sum = 0;
    while ( m != end )
      ++sum, ++m;
    
    std::cout << patterns[i] << ' ' << sum << std::endl;
  }

  std::cout << std::endl << buf.length() << std::endl;
  std::cout << sequence.length() << std::endl;  
  
  // Insert alternatives into sequence
  for ( unsigned i = 0; i < sizeof(alternatives)/sizeof(Pair); i++ ){
    const boost::regex e(alternatives[i].first);
    sequence = boost::regex_replace(sequence, e, 
					   alternatives[i].second);
  }
  std::cout << sequence.length() << std::endl;
  
  return 0;
}

// Local Variables: *
// compile-command: "g++ -Wall -Wextra -lboost_regex regexdna.cc -o regexdna"*
// End: *
