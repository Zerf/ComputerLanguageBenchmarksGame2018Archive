{-# OPTIONS_GHC -funbox-strict-fields #-}
import System
import Text.Printf

data Tree = Nil | Node !Int Tree Tree

main = do 
  [n] <- getArgs
  let max' = max (min'+2) (read n)
  let t = make 0 (max'+1)
  printf "stretch tree of depth %d\t check: %d\n" (max'+1) (itemCheck t)
  let longlived = make 0 max'
  depthLoop min' max'
  printf "long lived tree of deptch %d\t check: %d\n" max' (itemCheck longlived)

min' :: Int
min' = 4

depthLoop d m | d > m = return ()
depthLoop d m = do
   printf "%d\t trees of depth %d\t check: %d\n" (2*n) d (sumLoop n d 0)
   depthLoop (d+2) m
   where n = 2^(m - d + min')

sumLoop :: Int -> Int -> Int -> Int
sumLoop 0 d acc = acc
sumLoop k d acc = c `seq` sumLoop (k-1) d (acc + c + c')
    where c  = itemCheck (make k d)
          c' = itemCheck (make (-1*k) d)

make :: Int -> Int -> Tree
make i 0 = i `seq` Nil
make i d = Node i (make ((2*i)-1) (d-1)) (make (2*i) (d-1))

itemCheck Nil = 0
itemCheck (Node x l r) = x + itemCheck l - itemCheck r
