{ The Computer Language Benchmarks Game
  http://shootout.alioth.debian.org

  contributed by Ales Katona
  modified by Daniel Mantione
  modified by Steve Fisher
}


function to_int( s: string): longint;
var  num, sign, p : longint;
begin
  p := 1;  sign := 1;  num := 0;
  if s[1] = '-' then
    begin  p := 2;  sign := -1  end;
  for p := p to length(s) do
    num := num*10 + byte(s[p]) - byte('0');
  exit(num * sign);
end;

var tot: longint;
    textbuf:array[0..8191] of char;
    s: string;

begin
  settextbuf(input,textbuf);
  repeat
    readln(s);
    inc( tot, to_int( s));
  until eof;
  WriteLn(tot);
end.
