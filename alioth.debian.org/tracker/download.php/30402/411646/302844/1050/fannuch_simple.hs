import System


{-
 Simple functional implementation.
 Just the easiest possible implementation, no optimization at all.
 By Sebastian Sylvan
-}

-- generate permutations
selections []     = []
selections (x:xs) = (x,xs) : [(y,x:ys) | (y,ys) <- selections xs]

perms [] = [[]]
perms xs = [y : zs | (y,ys) <- selections xs, zs <- perms ys]

flop (1:_) = 0
flop xs@(x:_) = 1 + flop (rev x xs)

rev x xs = reverse a ++ b
    where (a,b) = splitAt x xs

fannuch xs = maximum $ map flop xs

main = do [n] <- getArgs
          let n' = read n
              xs = perms [1..n']
          putStr $ unlines $ map (concatMap show) $ take 30 xs
          putStr $ "Pfannkuchen(" ++ n ++ ") = "
          print (fannuch xs)