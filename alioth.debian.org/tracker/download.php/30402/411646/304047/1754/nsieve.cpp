/* The Computer Language Shootout
   http://shootout.alioth.debian.org/
   contributed by Daniel Skiles
   modified by Vaclav Haisman
   further modified by Paul Kitchin
*/

#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

unsigned int nsieve (std::vector< bool > & is_prime)
{
	unsigned int count = 0;
	std::size_t const max = is_prime.size();
	for (std::size_t i = 2; i < max; ++i)
	{
		if (is_prime[i])
		{
			for (std::size_t k = i * 2; k < max; k += i)
			{
				if (is_prime[k])
				{
					is_prime[k] = false;
				}
			}
			++count;
		}
	}
	return count;
}


int main(int argc, char * * argv)
{
	if (argc != 2)
	{
		std::cerr << "usage: " << argv[0] << " <n>\n";
		return 1;
	}
	int power_of_two;
	{
		std::istringstream convertor(argv[1]);
		if (!(convertor >> power_of_two) || !convertor.eof())
		{
			std::cerr << "usage: " << argv[0] << " <n>\n";
			std::cerr << "   n must be an integer\n";
			return 1;
		}
	}
	unsigned int max = (1 << power_of_two) * 10000;
	std::vector< bool > flags(max, true);
	std::cout << "Primes up to " << std::setw(8) << max << " " << std::setw(8) << nsieve(flags) << std::endl;
	max = (1 << (power_of_two - 1)) * 10000;
	flags.assign(max, true);
	std::cout << "Primes up to " << std::setw(8) << max << " " << std::setw(8) << nsieve(flags) << std::endl;
	max = (1 << (power_of_two - 2)) * 10000;
	flags.assign(max, true);
	std::cout << "Primes up to " << std::setw(8) << max << " " << std::setw(8) << nsieve(flags) << std::endl;
}
