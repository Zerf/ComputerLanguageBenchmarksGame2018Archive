% ------------------------------------------------------------------------------
% The Great Computer Language Shootout
% http://shootout.alioth.debian.org/
%
% Assumes execution using the following command-line usage:
%
%   mmc --make --debug pidigits ; ./pidigits 2500
%   mmc --grade hlc.gc --make pidigits ; ./pidigits 2500
%
% Based on work by Anthony Borla (SWI-Prolog), 
%	Christophe Troestler, Matthias Giovannini, and David Teller (OCaml),
%	Mike Pall and Stefan Krause (Java)
% Modified for Mercury by Glendon Holst
% ------------------------------------------------------------------------------

:- module pidigits.
:- interface.
:- import_module io.

:- pred main(io::di, io::uo) is det.

% ----------------------------------------------------------------------

:- implementation.

:- import_module list.
:- import_module int.
:- import_module integer.
:- import_module bool.
:- import_module string.
:- import_module char.
:- import_module exception.

% ----------------------------------------------------------------------

main(!IO) :-
	io.command_line_arguments(Args, !IO),
	(
		if
			Args = [AString|_],
			string.to_int(string.strip(AString), Aint)
		then
			N = Aint
		else
			N is 2500
	),
	pidigits(N, !IO).


% ----------------------------------------------------------------------
:- type integer_digits ---> integer_digits(	q :: integer, 
											r :: integer, 
											t :: integer).

% ------------------------------- %

:- pred pidigits(int::in, io::di, io::uo) is det.


pidigits(N, !IO) :- 
	PIDigits = integer_digits(one, zero, one),
	pidigits_(1, N, zero, PIDigits, !IO),
	(
		N mod 10 \= 0 ->
		io.format("%s\t:%i\n",
			[s(string.pad_right("", ' ', (10 - (N mod 10)))), i(N)], 
			!IO)
	;
		true
	).

% ------------- %

:- pred pidigits_(int, int, integer, integer_digits, io, io) is det.
:- mode pidigits_(in, in, in, in, di, uo) is det.

pidigits_(I, N, K, IntDigits0, !IO) :-
(
	I =< N ->
	DigitNext = extract(integer(3), IntDigits0), 
    DigitSafe = extract(integer(4), IntDigits0),
    (
		DigitNext \= DigitSafe ->
		K1 = K + one,
		consume(K1, IntDigits0, IntDigits1),
		pidigits_(I, N, K1, IntDigits1, !IO)
	;
		io.write_int(integer.int(DigitNext), !IO),
		(
			I mod 10 = 0 ->
			io.format("\t:%i\n",[i(I)], !IO)
		;
			true
		),
		produce(DigitNext, IntDigits0, IntDigits1),
		pidigits_(I + 1, N, K, IntDigits1, !IO)
	)
;
	true
).

% ------------- %
% simple division using subtraction more optimal for single digit results (i.e., X and Y +ve,
% X ~< 10*Y) than more general integer.div or integer.//

:- func simple_div(integer, integer) = int.

simple_div(X, Y) = Result :- simple_div_(X, Y, _, Result).
		 
:- pred simple_div_(integer, integer, integer, int).
:- mode simple_div_(in, in, out, out).
		 
simple_div_(X, Y, Rem, Result) :-
(
	X < Y ->
	Rem = X,
	Result = 0
;
	simple_div_(X - Y, Y << 1, Rem0, R1),
	simple_div_(Rem0, Y, Rem, R2),
	Result = 1 + (2 * R1) + R2 
).

% ------------- %

:- func extract(integer, integer_digits) = integer.

extract(X, IntDigits) = integer(simple_div(((IntDigits^q * X) + IntDigits^r) , (IntDigits^t))).

% ------------- %

:- pred produce(integer::in, integer_digits::in, integer_digits::out) is det.

produce(Digit, IntDigits0, IntDigits) :-
	IntDigits1 = (IntDigits0^r := (IntDigits0^r * integer(10)) + 
				  (IntDigits0^t * integer(-10) * Digit)),
	IntDigits = (IntDigits1^q := IntDigits1^q * integer(10)).
	
% ------------- %

:- pred consume(integer::in, integer_digits::in, integer_digits::out) is det.

consume(K, IntDigits0, IntDigits) :-
	Den = (K << 1) + one,		%% Den = integer(2) * K + one,
	Den2 = Den << 1,			%% Den2 = integer(2) * Den,		 
	IntDigits1 = (IntDigits0^r := (IntDigits0^r * Den) + (IntDigits0^q * Den2)),
	IntDigits3 = (IntDigits1^t := (IntDigits1^t * Den)),
	IntDigits = (IntDigits3^q := IntDigits3^q * K).
	
% ------------------------------- %
