// The Computer Language Shootout
// http://shootout.alioth.debian.org/

// Converted and improved from C to D by bearophile
// Compile:  dmd -O -release fannkuch.d

// This code doesn't look nice because it's optimized
//   for speed. D code that looks better is possible.

import std.string: atoi;
import std.c.stdlib: calloc;
import std.c.string: memcpy;

void main(char[][] args) {
    size_t n = args.length > 1 ? atoi(args[1]) : 1;
    printf("Pfannkuchen(%d) = %ld\n", n, fannkuch(n));
}

size_t fannkuch(size_t n) {
    size_t* perm;
    size_t* perm1;
    size_t* count;
    size_t flips, flipsMax, aux, r, i, k, didpr;
    size_t n1 = n - 1;

    if (n < 1)
        return 0;

    perm = cast(typeof(perm))calloc(n, (*perm).sizeof);
    perm1 = cast(typeof(perm1))calloc(n, (*perm1).sizeof);
    count = cast(typeof(count))calloc(n, (*count).sizeof);

    for (i = 0 ; i < n ; ++i)
        perm1[i] = i;

    r = n;
    didpr = 0;
    flipsMax = 0;
    for ( ; ; ) {
        if (didpr < 30) {
            for(i = 0; i < n; ++i)
                printf("%d", 1 + perm1[i]);
            printf("\n");
            ++didpr;
        }
        for ( ; r != 1; --r)
            count[r-1] = r;

        if (!(perm1[0] == 0 || perm1[n1] == n1)) {
            flips = 0;
            // for (i = 1; i < n; ++i) perm[i] = perm1[i];
            memcpy(&(perm[1]), &(perm1[1]), (*perm).sizeof*(n-1));
            k = perm1[0];
            do {
                size_t j;
                for (i = 1, j = k-1; i < j; ++i, --j) {
                    aux = perm[i];
                    perm[i] = perm[j];
                    perm[j] = aux;
                }
                ++flips;
                j = perm[k];
                perm[k] = k;
                k = j;
            } while (k);
            if (flipsMax < flips)
                flipsMax = flips;
        }

        for ( ; ; ) {
            if(r == n)
                return flipsMax;
            {
                size_t perm0 = perm1[0];
                i = 0;
                while (i < r) {
                    k = i+1;
                    perm1[i] = perm1[k];
                    i = k;
                }
                perm1[r] = perm0;
            }
            if ((count[r] -= 1) > 0)
                break;
            ++r;
        }
    }
}