import Text.Regex

main = getContents >>= putStrLn . output

output s = unlines $ countMatches ++ map show [length s, length s', finalLen]
  where
      s'           = replace ">.*\n|\n" "" s
      finalLen     = length $ replaceIubs s'
      countMatches = map (\v -> v ++ ' ': show (numHits v s')) variants

replaceIubs = foldl1 (.) (map (uncurry replace) pairs)

numHits = gmatch $ \m _ loop -> maybe 0 (\(_,_,t,_) -> 1 + loop t) m

replace pat r = gmatch (\m s loop -> maybe s (\(a,_,b,_) -> a ++ r ++ loop b) m) pat

gmatch fn pat buf = loop buf
  where
      regex  = mkRegexWithOpts pat True True
      loop s = fn (matchRegexAll regex s) s loop

variants = 
  ["agggtaaa|tttaccct","[cgt]gggtaaa|tttaccc[acg]","a[act]ggtaaa|tttacc[agt]t"
  ,"ag[act]gtaaa|tttac[agt]ct","agg[act]taaa|ttta[agt]cct","aggg[acg]aaa|ttt[cgt]ccct"
  ,"agggt[cgt]aa|tt[acg]accct","agggta[cgt]a|t[acg]taccct","agggtaa[cgt]|[acg]ttaccct"]

pairs = 
  [("B","(c|g|t)"),("D","(a|g|t)"),("H","(a|c|t)"),("K","(g|t)"),("M","(a|c)")
  ,("N","(a|c|g|t)"),("R","(a|g)"),("S","(c|g)"),("V","(a|c|g)"),("W","(a|t)")
  ,("Y","(c|t)") ]
