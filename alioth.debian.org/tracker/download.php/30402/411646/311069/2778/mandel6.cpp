/*	The Computer Language Benchmarks Game
	http://shootout.alioth.debian.org/

	contributed by Paolo Bonzini
	further optimized by Jason Garrett-Glaser
	OpenMP by The Anh Tran
*/

#include <stdio.h>
#include <stdlib.h>

// need "-fopenmp" flag when compile
#include <omp.h>

typedef double	v2d	__attribute__ ((vector_size(16))); // vector of two doubles


void mandelbrot(char* data, int* nbyte_each_line, int width, int height)
{
	v2d v10	= { 1.0, 1.0 };
	v2d v15	= { 1.5, 1.5 };
	v2d four	= { 4.0, 4.0 };

	v2d inverse_w = {2.0 / width, 2.0 / width};
	v2d inverse_h = {2.0 / height, 2.0 / height };

	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(guided) nowait
		for (int y = 0; y < height; ++y)
		{
			int bit_num = 0;
			int byte_acc = 0;

			v2d Civ = {y, y};
			Civ = Civ * inverse_h - v10;

			for (int x = 0; x < width; x += 2)
			{
				v2d Crv = {x +1, x};
				Crv = Crv * inverse_w - v15;

				// avoid (2 multiplies and 2 adds) in 1st iteration: zero*zero + zero*zero + Civ
				v2d Zrv	= Crv;
				// avoid (1 sub and 1 add): zero - zero + Crv
				v2d Ziv	= Civ;
				v2d Trv	= Zrv * Zrv;
				v2d Tiv	= Ziv * Ziv;

				// assume that element [x, y] and element [x+1, y] belong to mandelbrot set
				int result = 3;

				// skip zero*zero => start from 1
				int i = 1;
				do 
				{
					Ziv = (Zrv*Ziv) + (Zrv*Ziv) + Civ;
					Zrv = Trv - Tiv + Crv;
				
					Trv = Zrv * Zrv;
					Tiv = Ziv * Ziv;

					v2d delta = four - (Trv + Tiv);

					// sign bit is zero if 4.0 - (Trv + Tiv) >= 0.0 (i.e. |Z| <= 4.0)
					// Z1 is [x, y]	Z2 is [x+1, y]
					// mask = 0 <=> |Z2| <= 4	|Z1| <= 4
					// mask = 1 <=> |Z2| > 4	|Z1| <= 4
					// mask = 2 <=> |Z2| <= 4	|Z1| > 4
					// mask = 3 <=> |Z2| > 4 	|Z1| > 4
					int mask = __builtin_ia32_movmskpd(delta);

					// mask-out not-mandelbrot element
					result &= ~mask;
				}	
				while ( (++i < 50)	&&	(result != 0) );
			
				byte_acc <<= 2;
				byte_acc |= result;

				bit_num += 2;
				if ( bit_num == 8 ) 
				{
					int * xbc = &(nbyte_each_line[y]);
					data[ y * width + (*xbc)  ] = (char)byte_acc;

					bit_num = byte_acc = 0;
					*xbc += 1;
				}
			} // end foreach( column )

			if ( bit_num != 0 )
			{
				byte_acc <<= (8 - (width & 7));

				int * xbc = &(nbyte_each_line[y]);
				data[ y * width + (*xbc)  ] = (char)byte_acc;

				*xbc += 1;
			}
		}	// end foreach( line )
	} // end parallel region
}

int main (int argc, char **argv)
{
	int width = 200, height = 200;

	if  (argc == 2)
		width = height = atoi( argv[1] );
	printf("P4\n%d %d\n", width, height);

	char *data = (char*)malloc( width * height * sizeof(char) );
	int* nbyte_each_line = (int*)calloc( height, sizeof(int) );

	mandelbrot(data, nbyte_each_line, width, height);

	for (int y = 0; y < height; y++)
		fwrite( &(data[y * width]), nbyte_each_line[y], 1, stdout);

	free(data);
	free(nbyte_each_line);

	return 0;
}

