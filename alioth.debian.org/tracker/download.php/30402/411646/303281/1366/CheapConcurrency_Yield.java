/* 
 The Computer Language Shootout
 http://shootout.alioth.debian.org/

 contributed by tt@kyon.de
 modified by tt@kyon.de
 */
public final class CheapConcurrency_Yield extends Thread {

	private static final int THREADS = 500;
	private static int msgCount;
	private final CheapConcurrency_Yield nextThread;
	private int[] messages = new int[msgCount];
	private int todo;

	public static void main(String args[]) {
		msgCount = Integer.parseInt(args[0]);
		CheapConcurrency_Yield thread = null;
		for (int i = THREADS; --i >= 0;) {
			(thread = new CheapConcurrency_Yield(thread)).start();
		}
		synchronized (thread) {
			for (int i = msgCount; --i >= 0;) {
				thread.send(0);
			}
		}
	}
	private CheapConcurrency_Yield(CheapConcurrency_Yield next) {
		nextThread = next;
	}
	public void run() {
		try {
			for (;;) {
				synchronized (this) {
					if (todo != 0) {
						break;
					}
				}
				yield();
			}
			if (nextThread != null) {
				pass();
			} else {
				add();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void pass() throws Exception {
		int done = 0;
		for (;;) {
			synchronized (this) {
				synchronized (nextThread) {
					do {
						nextThread.send(messages[done++] + 1);
					} while (done < todo);
				}
			}
			while (done == todo) {
				// no unsynchronized todos left
				yield();
			}
		}
	}
	private void add() throws Exception {
		int sum = 0;
		int done = 0;
		for (;;) {
			synchronized (this) {
				do {
					sum += messages[done++] + 1;
				} while (done < todo);
			}
			while (done == todo) {
				// no unsynchronized todos left
				if (done == msgCount) {
					System.out.println(sum);
					System.exit(0);
				}
				yield();
			}
		}
	}
	private void send(int message) {
		messages[todo++] = message;
	}
}
