# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
# Written by Dima Dorfman, 2004
# modified by Heinrich Acker (Python)
# modified by Pilho Kim (Jython)

def nsieve(k):
    m = (1 << k) * 10000
    a = [True] * (m+1)
    n = 0
    i = 2
    while i <= m:
        if a[i]:
            k = i + i
            while k <= m:
                a[k] = False
                k += i
            n += 1
        i += 1

    print 'Primes up to %8d %8d' % (m, n)

import sys

n = 2
if len(sys.argv) > 1:
    n = max(int(sys.argv[1]), 2)
nsieve(n)
nsieve(n-1)
nsieve(n-2)

