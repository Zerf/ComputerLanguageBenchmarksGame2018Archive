/*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 * contributed by Alexander Suhoverhov
 */

import std.stdio: writef;
import str = std.string: toString;
import std.thread : Thread;
import std.conv: toInt;

const NUM_THREADS = 503;

int n;

bool finished = false;

alias char[] string;

class MessageThread: Thread {
	MessageThread next_;
	int message_;
	string name_;

	this(string name) {
		super();
		name_ = name;
	}

	MessageThread next(MessageThread n) {
		return next_ = n;
	}

	MessageThread next() {
		return next_;
	}

	void message(int m) {
		message_ = m;
	}

	int run() {
		while(!finished) {
			while(message_ == 0 && !finished) {
				yield;
			}
			
			if (finished)
				return 0;

			if(message_ == n) {
				finished = true;
				writef(name_);
				return 0;
			}

			auto m = message_;
			message_ = 0;
			next_.message(m + 1);
		}
		return 0;
	}
}

void main(string[] args) {

	n = toInt(args[1]);

	auto head = new MessageThread("1");
	{
		auto tail = head;

		for(auto i = 2; i <= NUM_THREADS; ++i) {
			tail.next = new MessageThread(toString(i));
			tail = tail.next;
		}
		tail.next = head;
	}

	auto current = head;
	for(int i = 0; i < NUM_THREADS; ++i) {
		current.start;
		current = current.next;
	}

	head.message(1);

	for(int i = 0; i < NUM_THREADS; ++i) {
		current.wait;
		current = current.next;
	}
}
