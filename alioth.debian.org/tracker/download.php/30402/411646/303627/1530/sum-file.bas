option escape
#include "crt.bi"
file_handle = freefile()
open cons for input as file_handle

total = 0
while not eof(file_handle)
   line input #file_handle, a$
   total += valint(a$)
wend   
close
printf("%d\n", total)