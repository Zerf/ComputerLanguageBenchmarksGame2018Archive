;;; The Computer Language Shootout
;;; http://shootout.alioth.debian.org/
;;; Contributed by Robbert Haarman
;;;
;;; fannkuch for Lisp (SBCL)
;;;
;;; Compile: sbcl --load fannkuch.lisp --eval "(save-lisp-and-die \"fannkuch.core\" :purify t :toplevel (lambda () (main) (quit)))"
;;;
;;; Run: sbcl --noinform --core fannkuch.core %A

(proclaim '(optimize (speed 3)
	(safety 0) (debug 0) (space 0) (compilation-speed 0)))

(defparameter *print-perms* 30)
(proclaim '(fixnum *print-perms*))

(defun print-permutation (permutation)
	(format t "~{~A~}~%" (coerce permutation 'list)))

(defun flip (permutation)
	(declare (type simple-vector permutation))
	(let ((n (aref permutation 0)))
		(declare (fixnum n))
		(let ((half (floor n 2)))
			(loop for i from 0 to (1- half)
				do (rotatef (aref permutation i)
						(aref permutation (+ i n -1)))
					(setf n (- n 2))))))

(defun count-flips (permutation)
	(declare (type simple-vector permutation))
	(loop for flips fixnum = 0 then (incf flips)
		until (= (the fixnum (aref permutation 0)) 1)
		do (flip permutation)
		finally (return flips)))

(defun make-permutation (n)
	(declare (fixnum n))
	(make-array (list n)
		:initial-contents (loop for i from 1 to n collecting i)))

(defun rotate-first (permutation n)
	(declare (type simple-vector permutation)
		(fixnum n))
	(let ((x (aref permutation 0)))
		(dotimes (i (1- n))
			(setf (aref permutation i)
				(aref permutation (1+ i))))
		(setf (aref permutation (1- n)) x)))

(defun call-with-permutations-rec (fun permutation len)
	(declare (function fun) (fixnum len))
	(let ((perm (copy-seq permutation)))
		(if (= len 1) (funcall fun perm)
			(progn
				(dotimes (i (1- len))
					(call-with-permutations-rec
						fun perm (1- len))
					(rotate-first perm len))
				(call-with-permutations-rec
					fun perm (1- len))))))

(defun call-with-permutations (fun initial-permutation)
	(call-with-permutations-rec fun initial-permutation
		(array-dimension initial-permutation 0)))

(defun fannkuch (n)
	(let ((printed 0) (max-flips 0))
		(declare (fixnum printed max-flips))
		(call-with-permutations
			(lambda (permutation)
				(when (< printed *print-perms*)
					(print-permutation permutation)
					(incf printed))
				(let ((flips (count-flips permutation)))
					(declare (fixnum flips))
					(if (> flips max-flips)
						(setf max-flips flips))))
			(make-permutation n))
		max-flips))

(defun main ()
	(let ((kuchen (parse-integer (second *posix-argv*))))
		(format t "Pfannkuchen(~A) = ~A~%" kuchen (fannkuch kuchen))))
