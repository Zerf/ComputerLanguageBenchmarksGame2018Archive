/*
  The Computer Language Benchmarks Game
  http://shootout.alioth.debian.org/
  modified to work with dk.brics.automaton
  http://www.brics.dk/~amoeller/automaton/
  Razii aka Amirk 
  
  gcj -c  -fno-bounds-check -fno-store-check -O3 --classpath=automaton.zip regexdna.java
  gcj -c  -fno-bounds-check -fno-store-check -O3 automaton.zip
  gcj --main=regexdna -o Program automaton.o regexdna.o

  javac -cp automaton.zip regexdna.java
  java -cp automaton.zip:. regexdna < reg2.txt

*/

import java.io.*;
import dk.brics.automaton.*;

public final class regexdna
{
    public static void main(String[] args)
    throws IOException
    {
        BufferedReader br = new BufferedReader (new InputStreamReader(System.in), 65536);
        StringBuilder sb = new StringBuilder(10240 * 8);
        int initialLength = 0;
        for (String line = ""; line != null; line = br.readLine())
        {
            initialLength += line.length() + 1;
            if (line.startsWith(">")) continue;
            sb.append(line);
        }

        String sequence = sb.toString();
        int codeLength = sequence.length();

        String[] variants =
        {
            "agggtaaa|tttaccct" ,"[cgt]gggtaaa|tttaccc[acg]", "a[act]ggtaaa|tttacc[agt]t",
            "ag[act]gtaaa|tttac[agt]ct", "agg[act]taaa|ttta[agt]cct", "aggg[acg]aaa|ttt[cgt]ccct",
            "agggt[cgt]aa|tt[acg]accct", "agggta[cgt]a|t[acg]taccct", "agggtaa[cgt]|[acg]ttaccct"
        };


        for (int k = 0; k < variants.length; k++)
        {
            int count = 0;
            RegExp r = new RegExp (variants[k]);
            Automaton a = r.toAutomaton();
            RunAutomaton run = new RunAutomaton(a, true);

            int i = 0;
            int offset = 0;
            while (offset < codeLength) {
                i = run.run(sequence, offset);
                if (i != -1) {
                    offset += i;
                    count++;
                } else {
                    offset += 1;
                }
            }

            System.out.println(variants[k] + " " + count);
        }

        sb = new StringBuilder(10240 * 8);
        for (int i=0; i<sequence.length(); i++)
        {
            char c = sequence.charAt(i);
            switch (c)
            {
            case 'B':
                sb.append("(c|g|t)");
                break;
            case 'D':
                sb.append("(a|g|t)");
                break;
            case 'H':
                sb.append("(a|c|t)");
                break;
            case 'K':
                sb.append("(g|t)");
                break;
            case 'M':
                sb.append("(a|c)");
                break;
            case 'N':
                sb.append("(a|c|g|t)");
                break;
            case 'R':
                sb.append("(a|g)");
                break;
            case 'S':
                sb.append("(c|g)");
                break;
            case 'V':
                sb.append("(a|c|g)");
                break;
            case 'W':
                sb.append("(a|t)");
                break;
            case 'Y':
                sb.append("(c|t)");
                break;
            default:
                sb.append(c);
                break;
            }
        }

        System.out.println();
        System.out.println(initialLength-1); // Assume file does not end with \n
        System.out.println(codeLength);
        System.out.println(sb.length());
    }
}

