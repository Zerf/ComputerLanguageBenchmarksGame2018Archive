(* -*- mode: sml -*-
 * $Id: random-mlton.code,v 1.4 2004/11/13 07:42:30 bfulgham Exp $
 * http://www.bagley.org/~doug/shootout/
 * Shortened by Vesa Karvonen.
 *)

val im = 139968
val ia = 3877
val ic = 29573

fun rnd seed res max n =
    if 0=n then res else
    let val seed = (seed * ia + ic) mod im
    in rnd seed (max * Real.fromInt seed / Real.fromInt im) max (n-1) end

val n = valOf (Int.fromString (hd (CommandLine.arguments ()))) handle _ => 1000
val _ = app print [Real.fmt (StringCvt.FIX (SOME 9)) (rnd 42 0.0 100.0 n),"\n"]
