;;; The Great Computer Language Shootout
;;; http://shootout.alioth.debian.org/
;;; implemented by Greg Buchholz
;;; optimized by Alex Shinn
;;;
;;; compile:  csc mandelbrot.chicken -O3 -d0 -disable-interrupts -block -lambda-lift -o mandelbrot

(define (main args)
  (let ((n (string->number (car args))))
    (display "P4") (newline) (display n) (display " ") (display n) (newline)
    (printPBM n)))

(define-inline (square x) (fx* x x))

(define (points x y n)
  (mandel (fp- (fp* 2.0 (exact->inexact (/ x n))) 1.5)
          (fp- (fp* 2.0 (exact->inexact (/ y n))) 1.0)
          0.0
          0.0
          50))

(define (mandel c1 c2 z1 z2 iter)
  (if (fx= iter 0)
    1
    (let* ((z3 (fp- (fp* z1 z1) (fp* z2 z2)))
           (z4 (fp+ (fp* z1 z2) (fp* z2 z1)))
           (n1 (fp+ c1 z3))
           (n2 (fp+ c2 z4)))
      (if (fp> (fp+ (fp* n1 n1) (fp* n2 n2)) 4.0)
        0
        (mandel c1 c2 n1 n2 (fx- iter 1))))))

(define (printPBM n)
  (do ((y 0 (fx+ y 1)))
      ((fx= y n))
    (let lp ((x 1) (acc (points 0 y n)))
      (cond
        ((fx= x n)
         (display (integer->char (fx* acc (square (fxand x #b111))))))
        ((fx= 0 (fxand x #b111))
         (display (integer->char acc))
         (lp (fx+ x 1) (points x y n)))
        (else
         (lp (fx+ x 1) (fx+ (fxshl acc 1) (points x y n))))))))

(main (command-line-arguments))
