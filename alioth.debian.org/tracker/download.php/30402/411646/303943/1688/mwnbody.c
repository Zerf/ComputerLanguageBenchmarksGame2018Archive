/* The Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Mirco Wahab
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define sqr(x)     ( (x) * (x) )
#define my_pi      3.141592653589793
#define SUNMASS    (4.0 * my_pi * my_pi)
#define DAYS       365.24

typedef double Celestial[7];
enum { pX, pY, pZ,     vX, vY, vZ,    Mass };

 Celestial Bodies[] = {
 { // Sun
   0, 0, 0,
   0, 0, 0,
   SUNMASS
 },
 { // Jupiter
  4.84143144246472090e+00,     -1.16032004402742839e+00,      -1.03622044471123109e-01,
  1.66007664274403694e-03*DAYS, 7.69901118419740425e-03*DAYS, -6.90460016972063023e-05*DAYS,
  9.54791938424326609e-04*SUNMASS,
 },
 { // Saturn
  8.34336671824457987e+00,      4.12479856412430479e+00,      -4.03523417114321381e-01,
 -2.76742510726862411e-03*DAYS, 4.99852801234917238e-03*DAYS,  2.30417297573763929e-05*DAYS,
  2.85885980666130812e-04*SUNMASS,
 },
 { // Uranus
  1.28943695621391310e+01,     -1.51111514016986312e+01,      -2.23307578892655734e-01,
  2.96460137564761618e-03*DAYS, 2.37847173959480950e-03*DAYS, -2.96589568540237556e-05*DAYS,
  4.36624404335156298e-05*SUNMASS,
 },
 { // Neptune
  1.53796971148509165e+01,     -2.59193146099879641e+01,       1.79258772950371181e-01,
  2.68067772490389322e-03*DAYS, 1.62824170038242295e-03*DAYS, -9.51592254519715870e-05*DAYS,
  5.15138902046611451e-05*SUNMASS,
 } };

 void init(int n)
{
 double m, *pv, mom[] = {0, 0, 0};
 int i, j;

 for(i=0; i<n; i++) {
    pv = Bodies[i]+vX, m = Bodies[i][Mass];
    for(j=0; j<3; j++) mom[j] += pv[j] * m;
 }
 for(j=0; j<3; j++)
    Bodies[0][vX+j] = -mom[j] / Bodies[0][Mass];
}

 double energy(int n)
{
 double e, d, *p, *q;
 int i, j;

 for(i=0,e=0; i<n; i++) {
    p = Bodies[i];
    e += 0.5 * p[Mass] * ( sqr(p[vX]) + sqr(p[vY]) + sqr(p[vZ]) );
    for(j=i+1; j<n; j++) {
       q = Bodies[j];
       d = sqrt( sqr(p[pX]-q[pX]) + sqr(p[pY]-q[pY]) + sqr(p[pZ]-q[pZ]) );
       e -= p[Mass] * q[Mass] / d;
    }
  }
  return e;
}

 void advance(int n, double dt)
{
 double d, dx,dy,dz, mp, mq, mag, *p, *q;
 int i, j;

 for(i=0; i<n; i++) {
    p  = Bodies[i];
    mp = p[Mass];
    for(j=i+1; j<n; j++) {
       q   = Bodies[j];
       mq  = q[Mass];
       dx  = p[pX]-q[pX], dy = p[pY]-q[pY], dz = p[pZ]-q[pZ];
       d   = sqrt( dx*dx + dy*dy + dz*dz );
       mag = dt / (d * d * d);
       dx *= mag, dy *= mag, dz *= mag;
       p[vX] -= dx*mq,  p[vY] -= dy*mq,  p[vZ] -= dz*mq,
       q[vX] += dx*mp;  q[vY] += dy*mp;  q[vZ] += dz*mp;
    }
 }

 for(i=0; i<n; i++) {
    for(j=0,p=Bodies[i]; j<3; j++)
       p[j] += dt * p[vX+j];
 }
}

 int main(int argc, char **argv)
{
 int nbodies = sizeof Bodies / sizeof Bodies[0];
 int i, nsteps = atoi( argv[1]);

 init(nbodies);
 printf("%.9f\n", energy(nbodies));
 for(i=0; i<nsteps; i++)
    advance(nbodies, 0.01);
 printf("%.9f\n", energy(nbodies));
 return 0;
}
