#!/usr/bin/regina -a

/* ------------------------------------------------------------------ */
/* The Great Computer Language Shootout                               */
/* http://shootout.alioth.debian.org/                                 */
/*                                                                    */
/* % regina -v                                                        */
/* REXX-Regina_3.3(MT) 5.00 25 Apr 2004                               */
/* % uname -orvmp                                                     */
/* 2.6.5-1.358 #1 Wed Oct 13 17:49:34 EST 2004 i686 i686 GNU/Linux    */
/*                                                                    */
/* Contributed by Anthony Borla                                       */
/* ------------------------------------------------------------------ */

trace 'OFF'

N = ARG(1) ; if DATATYPE(N) \= 'NUM' | N < 1 then ; N = 1

say TAK(N * 3.0, N * 2.0, N * 1.0)

exit 0

/* ----------------------------- */

TAK :
  if ARG(2) >= ARG(1) then ; return ARG(3)
  return TAK(TAK(ARG(1) - 1.0, ARG(2), ARG(3)), TAK(ARG(2) - 1.0, ARG(3), ARG(1)), TAK(ARG(3) - 1.0, ARG(1), ARG(2)))
