# The Computer Language Shootout
# http://shootout.alioth.debian.org/
#
# Contributed by Markus Peter
# Optimised by Adam Kennedy

$N = (@ARGV ? $ARGV[0] : 500) - 1;
@u = (1) x $N; A() for 0..19;
@v = @u; A();
$x = $y = 0;
for ( 0..$N ) {
    $x += $u[$i] * $v[$i];
    $y += $v[$i] * $v[$i];
}
printf "%0.9f\n", sqrt($x/$y);

sub A {
	for my $i ( 0..$N ) {
		$t[$i] = 0;
		($t[$i] += $u[$_] / ( ($i + $_) * ($i + $_ + 1) / 2 + $i + 1 )) for 0..$N;
	}
	for my $i ( 0..$N ) {
		$u[$i] = 0;
		($u[$i] += $t[$_] / ( ($_ + $i) * ($_ + $i + 1) / 2 + $_ + 1 )) for 0..$N;
	}
}
