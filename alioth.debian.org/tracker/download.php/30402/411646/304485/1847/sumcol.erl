%erl -noshell -noinput -run sumcol main whatever < sumcol-input.txt 

-module(sumcol).
-export([main/1]).
-define(stdin,0).
-define(stdout,1).
-define(maxline,128).

main(_)->
    open_port({fd, ?stdin, ?stdout}, [{line, ?maxline}, eof]), 
    io:format("~p~n",[read(0)]),
    halt().


read(Sum)->
    receive 
	{_, eof}            -> Sum;
	{_, {data, {_, S}}} -> read(Sum + element(1, string:to_integer(S)))
    end.




