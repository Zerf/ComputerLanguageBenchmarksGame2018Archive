# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
#
# contributed by Josh Goldfoot
# modified by Mike Klaas, Dani Nanz and Tupteq

import sys

def main():
    from math import sin, cos
    n = int(sys.argv[1])

    a1 = alt = 1.
    a2 = a3 = a4 = a5 = a6 = a7 = a8 = a9 = 0.
    twoThirds = 2. / 3.

    for k in xrange(1, n + 1):
        k = float(k)
        ki = 1. / k
        ks, kc = sin(k), cos(k)

        a1 += twoThirds ** k
        a2 += ki ** .5
        a3 += ki / (k + 1.)
        a4 += ki / ks * (ki / ks) / k
        a5 += ki / kc * (ki / kc) / k
        a6 += ki
        a7 += ki / k
        a8 += alt / k
        a9 += alt / (k + k - 1.)
        alt = -alt

    print ('%0.9f\t(2/3)^k\n'
           '%0.9f\tk^-0.5\n'
           '%0.9f\t1/k(k+1)\n'
           '%0.9f\tFlint Hills\n'
           '%0.9f\tCookson Hills\n'
           '%0.9f\tHarmonic\n'
           '%0.9f\tRiemann Zeta\n'
           '%0.9f\tAlternating Harmonic\n'
           '%0.9f\tGregory') % (a1, a2, a3, a4, a5, a6, a7, a8, a9)

main()
