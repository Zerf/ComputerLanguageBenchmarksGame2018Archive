%% The Computer Language Benchmarks Game
%% http://shootout.alioth.debian.org/
%% Contributed by: Sergei Matusevich 2007
%% Modified by: Maria Christakis
%% Date: 8 Apr 2009

-module(regexdna).

-export([main/1, matches/2, substitutes/1, get_binary/1]).

-define(VARIANTS,
  ["agggtaaa|tttaccct",
   "[cgt]gggtaaa|tttaccc[acg]",
   "a[act]ggtaaa|tttacc[agt]t",
   "ag[act]gtaaa|tttac[agt]ct",
   "agg[act]taaa|ttta[agt]cct",
   "aggg[acg]aaa|ttt[cgt]ccct",
   "agggt[cgt]aa|tt[acg]accct",
   "agggta[cgt]a|t[acg]taccct",
   "agggtaa[cgt]|[acg]ttaccct"]).

read_lines(File) ->
  case io:get_line(File, '') of
    eof -> {0, [[]]};
    Str ->
      Len = size(Str),
      {TotalLen, [H|T]} = read_lines(File),
      Eol = Len - 1,
      {Line, Bool} =
        case Str of
          <<">", _/binary>>         -> {[], true};
          <<Trim:Eol/binary, "\n">> -> {[Trim], false}
        end,
      {Len + TotalLen, 
        case Bool of
          true -> [[]|[H|T]];
          false -> [(Line ++ H)|T]
        end}
  end.

matches(V, LstInp) ->
  case re:run(LstInp, V, [global]) of
    {match, M} -> {V, length(M)};
    _Other -> {V, 0}
  end.

substitutes(Lst) ->
  length(lists:flatmap(
    fun(Ch) ->
      case Ch of
        $B -> "(c|g|t)";
        $D -> "(a|g|t)";
        $H -> "(a|c|t)";
        $K -> "(g|t)";
        $M -> "(a|c)";
        $N -> "(a|c|g|t)";
        $R -> "(a|g)";
        $S -> "(c|g)";
        $V -> "(a|c|g)";
        $W -> "(a|t)";
        $Y -> "(c|t)";
        XX -> [XX]
      end
    end, Lst)).

get_binary(InpCase) ->
  binary_to_list(list_to_binary(InpCase)).

main(_) ->
  io:setopts(standard_io, [binary]),
  {InitSz, Inp} = read_lines(standard_io),
  TrimSz = size(BinInp = list_to_binary(lists:flatten(Inp))),
  LstInp = binary_to_list(BinInp),
  Output =
    rpc:pmap({?MODULE, matches}, [LstInp], ?VARIANTS),
  lists:foreach(
    fun({V, LenM}) ->
      io:format("~s ~w~n", [V, LenM])
    end, Output),
  Subst =
    rpc:pmap({?MODULE, substitutes}, [],
             rpc:pmap({?MODULE, get_binary}, [], Inp)),
  SubstSz = lists:foldl(fun(X, Sum) -> X + Sum end, 0, Subst),
  io:format("~n~w~n~w~n~w~n", [InitSz, TrimSz, SubstSz]),
  halt(0).
