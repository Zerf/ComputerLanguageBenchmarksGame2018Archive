/* The Computer Language Shootout Benchmarks
   http://shootout.alioth.debian.org/

   contributed by Kevin Carson
   modified by Cédric Krier
   compilation:
       gcc -O3 -fomit-frame-pointer -funroll-loops -fopenmp -static binarytrees.c -lpthread
*/

#define _GNU_SOURCE
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sched.h>
#include <omp.h>


typedef struct tn {
    struct tn*    left;
    struct tn*    right;
    long          item;
} treeNode;

typedef struct pt {
    treeNode*     pool;
    treeNode*     new;
} poolTree;

typedef struct tc {
    long iterations;
    long depth;
    long check;
} treeCheck;


treeNode* NewTreeNode(treeNode* left, treeNode* right, long item, poolTree* pool)
{
    treeNode* new = pool->new++;

    new->left = left;
    new->right = right;
    new->item = item;

    return new;
} /* NewTreeNode() */


long ItemCheck(treeNode* tree)
{
    long result;
    if (tree->left == NULL)
        result = tree->item;
    else
        result = tree->item + ItemCheck(tree->left) - ItemCheck(tree->right);
    return result;
} /* ItemCheck() */


treeNode* BottomUpTree(long item, unsigned depth, poolTree* pool)
{
    if (depth > 0)
        return NewTreeNode
        (
            BottomUpTree(2 * item - 1, depth - 1, pool),
            BottomUpTree(2 * item, depth - 1, pool),
            item,
            pool
        );
    else
        return NewTreeNode(NULL, NULL, item, pool);
} /* BottomUpTree() */


poolTree* NewPoolTree(long size)
{
    poolTree* new;

    new = (poolTree*)malloc(sizeof(poolTree));

    new->pool = (treeNode*)malloc(size * sizeof(treeNode));
    new->new = new->pool;

   return new;
} /* NewPoolTree() */


void DeletePoolTree(poolTree* pool)
{
    free(pool->pool);
    free(pool);
} /* DeletePoolTree() */


int GetMaxThread()
{
    unsigned i, count = 0;
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    sched_getaffinity(0, sizeof(cpuset), &cpuset);

    for (i = 0; i < 8; i++)
    {
        if (CPU_ISSET(i, &cpuset))
            count++;
    }
    return count;
} /* GetMaxThread() */


int main(int argc, char* argv[])
{
    unsigned   N, minDepth, maxDepth, stretchDepth;
    long depth;
    treeNode   *stretchTree, *longLivedTree;
    poolTree *pool;

    N = atol(argv[1]);

    minDepth = 4;

    if ((minDepth + 2) > N)
        maxDepth = minDepth + 2;
    else
        maxDepth = N;

    stretchDepth = maxDepth + 1;

    pool = NewPoolTree((1 << (stretchDepth + 1)) -1);
    stretchTree = BottomUpTree(0, stretchDepth, pool);
    printf
    (
        "stretch tree of depth %u\t check: %li\n",
        stretchDepth,
        ItemCheck(stretchTree)
    );
    DeletePoolTree(pool);

    pool = NewPoolTree((1 << (maxDepth + 1)) -1);
    longLivedTree = BottomUpTree(0, maxDepth, pool);

    treeCheck *treeChecks = (treeCheck*)malloc(stretchDepth * sizeof(treeCheck));

#pragma omp parallel for default(shared) num_threads(GetMaxThread()) schedule(dynamic, 1)
    for (depth = minDepth; depth <= maxDepth; depth += 2)
    {
        long    i, iterations, check;
        poolTree *ompPool;

        iterations = 1 << (maxDepth - depth + minDepth);

        check = 0;

        for (i = 1; i <= iterations; i++)
        {
            ompPool = NewPoolTree((1 << (depth + 1)) -1);
            check += ItemCheck(BottomUpTree(i, depth, ompPool));
            DeletePoolTree(ompPool);
            ompPool = NewPoolTree((1 << (depth + 1)) -1);
            check += ItemCheck(BottomUpTree(-i, depth, ompPool));
            DeletePoolTree(ompPool);
        } /* for(i = 1...) */

        treeChecks[depth].iterations = iterations * 2;
        treeChecks[depth].depth = depth;
        treeChecks[depth].check = check;
    } /* for(depth = minDepth...) */

    for (depth = minDepth; depth <= maxDepth; depth += 2)
    {
        printf
        (
            "%li\t trees of depth %li\t check: %li\n",
            treeChecks[depth].iterations,
            treeChecks[depth].depth,
            treeChecks[depth].check
        );
    } /* for (depth = minDepth...) */
    free(treeChecks);

    printf
    (
        "long lived tree of depth %u\t check: %li\n",
        maxDepth,
        ItemCheck(longLivedTree)
    );
    DeletePoolTree(pool);

    return 0;
} /* main() */

