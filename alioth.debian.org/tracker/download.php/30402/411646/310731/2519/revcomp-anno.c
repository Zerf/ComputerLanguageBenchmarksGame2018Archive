/*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org
 *
 * "reverse-complement" benchmark for Gnu C
 * contributed by Bob W (2008-05-05)
 *
 * Further optimisation is possible by removing data integrity
 * checks and expecting source to be in LF terminated text format.
 */

#include <stdio.h>

static const char XTAB[128] = {  // 7-bit character conversion table
  0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
  0,'T','V','G','H',0,  0,'C', 'D',  0,0,'M', 0,'K','N',0,
  0,  0,'Y','S','A',0,'B','W',   0,'R',0,  0, 0,  0,  0,0,
  0,'T','V','G','H',0,  0,'C', 'D',  0,0,'M', 0,'K','N',0,
  0,  0,'Y','S','A',0,'B','W',   0,'R',0,  0, 0,  0,  0,0
};


int errex(char *s, int n) {      // display message & value, return 1
  printf("*** Error: %s [%d]!\n", s, n);
  return 1;
}


int main () {                            // ***** main *****
  char c;                                // temp
  int  lile;                             // output sequence line length
  char *pj, *pjstop, *pq, *pr, *prlo;    // pointers to I/O buffers

  char   bbj[11888], bbq[11888];         // input and output buffers
  size_t bbjsz=sizeof(bbj);              // max. size of input buffer
  size_t bbjle=0;                        // actual length of buffer data

  bbjle = fread(bbj, 1, bbjsz, stdin);   // get input from stdin
  if (bbjle < 999 || bbjle > (bbjsz-9))  // (# remove check if desired #)
    return errex("Input data size",bbjle);
  if (*bbj!='>')                         // (# remove check if desired #)
    return errex("expected '>' as 1st char", 0);
  if (!bbj[bbjle-1])                     // discard ev. trailing zero
    bbjle--;

  pj=bbj;  pq=bbq;  pjstop=pj+bbjle;     // init pointers for good ol'C

  while (pj<pjstop) {                    // loop: read input data
    while (*pj!=0xA && *pj!=0xD) {       // loop: copy ID line
      if (pj>=pjstop)                    // (# remove check if desired #)
        return errex("Unexpected end of data", (int)(pj-bbj));
      *pq++ = *pj++;                     // copy line contents (chars)
    }
    *pq++ = 0xa;  prlo=pj;               // add LF; mark start of data

    while (*pj!='>' && pj<pjstop)        // find seq. data block end
      pj++;                              // adjust pointer accordingly

    pr=pj-1;  lile=0;                    // init reverse ptr and line len.
    if (pr<=prlo)                        // (# remove check if desired #)
      return errex("Sequence too short",(int)(prlo-bbj));

    while (pr>=prlo) {                   // loop: revert & convert
      c=XTAB[*pr-- & 0x7f];              // get char, use 7 bits
      if (c) {                           // conversion possible
        *pq++ = c;  lile++;              // output to buffer; inc. length
        if (lile>59)                     // long line - EOL required
          { lile=0;  *pq++ = 0xa; }      // reset line length; add LF
      }
    }

    if (lile)  *pq++ = 0xa;              // last line not empty, add LF
  }

  fwrite(bbq, 1, (int)(pq-bbq), stdout); // dump buffer to stdout
  return 0;
}
