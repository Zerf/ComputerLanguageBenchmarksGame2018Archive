#include <stdio.h>


static const char XTAB[128] = {
  0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
  0,'T','V','G','H',0,  0,'C', 'D',  0,0,'M', 0,'K','N',0,
  0,  0,'Y','S','A',0,'B','W',   0,'R',0,  0, 0,  0,  0,0,
  0,'T','V','G','H',0,  0,'C', 'D',  0,0,'M', 0,'K','N',0,
  0,  0,'Y','S','A',0,'B','W',   0,'R',0,  0, 0,  0,  0,0
};


int errex(char *s, int n) {
  printf("*** Error: %s [%d]!\n", s, n);
  return 1;
}


int main () {
  char c;
  int  lele;
  char *pj, *pjstop, *pq, *pr, *prlo;

  char   bbj[11888], bbq[11888];
  size_t bbjsz=sizeof(bbj);
  size_t bbjle=0;

  bbjle = fread(bbj, 1, bbjsz, stdin);
  if (bbjle < 999 || bbjle > (bbjsz-9))
    return errex("File size",bbjle);
  if (*bbj!='>')
    return errex("expected '>' @ BOF", 0);
  if (!bbj[bbjle-1])
    bbjle--;

  pj=bbj;  pq=bbq;  pjstop=pj+bbjle;

  while (pj<pjstop) {
    while (*pj!=0xa && *pj!=0x0d) {
      if (pj>=pjstop)
        return errex("Unexpected EOF", (int)(pj-bbj));
      *pq++ = *pj++;
    }
    *pq++ = 0xa;  prlo=pj;

    while (*pj!='>' && pj<pjstop)
      pj++;    

    pr=pj-1;  lele=0;
    if (pr<=prlo)
      return errex("Seq. too short",(int)(prlo-bbj));

    while (pr>=prlo) {
      c=XTAB[*pr-- & 0x7f];
      if (c) {
        *pq++ = c;  lele++;
        if (lele>59)
          { lele=0;  *pq++ = 0xa; }
      }
    }

    if (lele)  *pq++ = 0xa;
  }

  fwrite(bbq, 1, (int)(pq-bbq), stdout);
  return 0;
}
