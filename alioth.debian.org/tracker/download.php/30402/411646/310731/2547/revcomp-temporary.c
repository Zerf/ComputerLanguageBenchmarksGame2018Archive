/*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org
 *
 * "reverse-complement" benchmark for Gnu C
 * contributed by Bob W (V2, 2008-05-10)
 *
 * - Further optimisation is possible by removing data integrity checks
 * - Chars 'j' and 'q' are shortcut indicators for I/O buffer variables
 */

#include <stdio.h>
#include <string.h>

#define BFSIZE 13444888

static const char XTAB[128] = {  // 7-bit character conversion table
  0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
  0,'T','V','G','H',0,  0,'C', 'D',  0,0,'M', 0,'K','N',0,
  0,  0,'Y','S','A',0,'B','W',   0,'R',0,  0, 0,  0,  0,0,
  0,'T','V','G','H',0,  0,'C', 'D',  0,0,'M', 0,'K','N',0,
  0,  0,'Y','S','A',0,'B','W',   0,'R',0,  0, 0,  0,  0,0
};


int errex(char *s, int n) {          // show message & value, return 1
  printf("*** Error: %s [%d]!\n", s, n);
  return 1;
}


int main () {                            // ******* main *******
  char c;  int n;                        // temps
  char *pj, *pq, *pr;                    // buffer pointers: inp, out, /out
  char *jjj = malloc(BFSIZE);            // input buffer
  char *qqq = malloc(BFSIZE);            // output buffer
  char *pjstop, *pqstop = qqq+BFSIZE;    // end-of-buffer pointers

  if (!jjj || !qqq)
    return errex("Unable to allocate buffers",0);

  n = fread(jjj, 1, BFSIZE, stdin);      // get 1st input block from stdin
  pj = jjj;  pjstop = pj+n;              // init input buffer pointers
  if (n < 4)
    return errex("Input data size",n);
  if (*jjj != '>')
    return errex("1st char is not '>'", 0);

  while (pj < pjstop) {                  // MAIN LOOP: process data

    for (pq=qqq; *pj!=10 && *pj!=13; ) { // LOOP: copy entire ID line
      *pq++ = *pj++;                     // copy line contents (chars)
      if (pj >= pjstop) {                // need more input data
        n = fread(jjj,1,BFSIZE,stdin);   // get data block from stdin
        if (!n)
          return errex("EOD within ID line", pj-jjj);
        if (pq > (qqq+60))
          return errex("ID line too long", pq-qqq);
        pj = jjj;  pjstop = pj+n;        // update input buffer pointers
      }
    }
    *pq++ = 0xA;                         // add LF to ID line

    for (pr=pqstop; *pj != '>'; ) {      // LOOP: revert & convert data
      c=XTAB[*pj++ & 0x7f];              // get & convert char, use 7 bits
      if (c) {                           // conversion result is useful
        if (pr <= pq)
          return errex("Output buffer overflow", BFSIZE);
        *--pr = c;                       // move converted char to buffer
      }
      if (pj >= pjstop) {                // try to get more input data
        n = fread(jjj,1,BFSIZE,stdin);   // fetch data block from stdin
        if (!n)  break;                  // no more data: terminate loop
        pj = jjj;  pjstop = pj+n;        // update input buffer pointers
      }
    }

    if ((pr-pq) < (BFSIZE/60))
      return errex("No space to insert LFs", n);

    while (pr<pqstop) {                  // LOOP: format output
      n = (pqstop-pr);                   // calc remaining chunk size
      if (n > 60)  n = 60;               // move 1 line at a time
      memmove(pq,pr,n);                  // move line to empty space
      pr+=n;  pq+=n;  *pq++ = 0xA;       // adjust pointers, add LF
    }
    fwrite(qqq, 1, pq-qqq, stdout);      // output converted data

  }
  return 0;
}
