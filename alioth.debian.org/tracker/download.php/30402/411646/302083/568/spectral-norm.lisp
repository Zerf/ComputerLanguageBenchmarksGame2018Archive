;;   The Great Computer Language Shootout
;;    http://shootout.alioth.debian.org/
;;  
;;    Adapted from the C (gcc) code by Sebastien Loisel
;;
;;    Contributed by Christopher Neufeld


;; Note that sbcl is at least 10 times faster than either clisp or gcl
;; on this program, running with an argument of 500.  It would be nice
;; to know why the others are so slow.


(declaim (inline eval-A))

(defun eval-AtA-times-u (n u)
  (eval-At-times-u n (eval-A-times-u n u)))


;; This is our most expensive function.  Optimized with the knowledge
;; that 'n' will never be "huge".  This will break if 'n' exceeds
;; approximately half of the square root of the largest fixnum
;; supported by the implementation.  On sbcl 0.9.3,
;; 'most-positive-fixnum' is 536870911, and we can support values of
;; 'n' above 11000.
(defun eval-A (i j)
  (declare (type fixnum i) (type fixnum j))
  (declare (optimize (speed 3) (safety 0) (space 0)))
  (the double-float (/ 1.0d0 (float (+ (the fixnum (/ (the fixnum (* (the fixnum (+ i j)) (the fixnum (+ i j 1)))) 2)) i 1) 1.0d0))))
  

(defun eval-A-times-u (n u)
  (declare (type fixnum n))
  (declare (type (array double-float 1) u))
  (declare (optimize (speed 3) (safety 0) (space 0)))
  (let ((retval (make-array n :element-type 'double-float :initial-element 0.0d0)))
    (dotimes (i n)
      (dotimes (j n)
        (incf (aref retval i) (* (eval-A i j) (aref u j)))))
    retval))

(defun eval-At-times-u (n u)
  (declare (type fixnum n))
  (declare (type (array double-float 1) u))
  (declare (optimize (speed 3) (safety 0) (space 0)))
  (let ((retval (make-array n :element-type 'double-float :initial-element 0.0d0)))
    (dotimes (i n)
      (dotimes (j n)
        (incf (aref retval i) (* (eval-A j i) (aref u j)))))
    retval))


(defun main (&optional n-supplied)
  (let ((n (or n-supplied
               (parse-integer (or (car (last #+sbcl sb-ext:*posix-argv*
                                         #+clisp ext:*args*
					 #+gcl  si::*command-args*)) 
                                  "2000")))))

    (or (typep (* (- (* 2 n) 1) (- (* 2 n) 2)) 'fixnum)
        (error "The supplied value of 'n' breaks the optimizations in EVAL-A"))

    (let ((u (make-array n :element-type 'double-float :initial-element 1.0d0))
          (v (make-array n :element-type 'double-float)))
      (dotimes (i 10)
        (setf v (eval-AtA-times-u n u))
        (setf u (eval-AtA-times-u n v)))
      (let ((vBv 0.0d0) 
            (vv 0.0d0))
        (dotimes (i n)
          (incf vBv (* (aref u i) (aref v i)))
          (incf vv (* (aref v i) (aref v i))))
        
        (format t "~11,9F~%" (sqrt (/ vBv vv)))))))

          

;; #+sbcl (progn
;;          (sb-profile:profile eval-AtA-times-u)
;;          (sb-profile:profile eval-A)
;;          (sb-profile:profile eval-A-times-u)
;;          (sb-profile:profile eval-At-times-u)
;;          (sb-profile:profile main))
