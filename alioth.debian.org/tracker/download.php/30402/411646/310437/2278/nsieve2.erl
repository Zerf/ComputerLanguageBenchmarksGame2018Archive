%  The Great Computer Language Shootout
%   http://shootout.alioth.debian.org/
%
%   contributed by Mark Scandariato
%   bug workaround by Fredrik Svahn
%
%   erl -noshell -noinput -run nsieve main 9

-module(nsieve2).
-export([main/1]).

main([Arg]) ->
    N = list_to_integer(Arg),
    main(N),
    erlang:halt(0);

main(N) when N >= 2 -> ns(N), ns(N-1), ns(N-2).


ns(N) ->
    M = (1 bsl N)*10000,
    clear(M, 0),
    io:fwrite("Primes up to ~8.10B ~8.10B~n", [M, ns(2, M, 0)]).


ns(I, M, C) when I > M -> C;
ns(I, M, C) ->
    case get(I) of
        true  -> mark(I, M, I+I), ns(I+1, M, C+1);
        false -> ns(I+1, M, C)
    end.


mark(_, M, K) when K > M -> ok;
mark(I, M, K) -> put(K, false), mark(I, M, K+I).


clear(M, M) -> ok;
clear(M, I) -> put(I, true), clear(M, I+1).

