-- Fasta
--
-- Jim Rogers
--

with Fasta_Utilities; use Fasta_Utilities;
with Ada.Command_Line; use Ada.Command_Line;

procedure Fasta is
   N : Positive := 1000;
begin
   if Argument_Count > 0 then
      N := Positive'Value(Argument(1));
   end if;
   Make_Cumulative(Iub);
   Make_Cumulative(Homosapiens);
   Make_Repeat_Fasta("ONE", "Homo sapiens alu", Alu, N*2);
   Make_Random_Fasta("TWO", "IUB ambiguity codes", Iub, N * 3);
   Make_Random_Fasta("THREE", "Homosapiens frequency", homosapiens, N * 5);
end Fasta;


--
-- package fasta_utilities
--
-- Jim Rogers
--

package Fasta_Utilities is
   function Random(Max : Long_Float) return Long_Float;
   
   type Amino_Acids is record
      C : Character;
      P : Long_Float;
   end record;
   
   type Gene_Array is array(Positive range <>) of Amino_Acids;
   
   procedure Make_Cumulative(Gene_List : in out Gene_Array);
   function Select_Random(Gene_List : Gene_Array) return Character; 
   procedure Make_Random_Fasta(Id : String; Desc : String; Gene_List : Gene_Array; N : Positive);
   procedure Make_Repeat_Fasta(Id : String; Desc : String; S : String; N : Positive);
   
   Iub : Gene_Array := (
      ('a', 0.27), 
      ('c', 0.12),
      ('g', 0.12),
      ('t', 0.27),
         
      ('B', 0.02),
      ('D', 0.02),
      ('H', 0.02),
      ('K', 0.02),
      ('M', 0.02),
      ('N', 0.02),
      ('R', 0.02),
      ('S', 0.02),
      ('V', 0.02),
      ('W', 0.02),
      ('Y', 0.02));
      
   Homosapiens : Gene_Array := (
      ('a', 0.3029549426680), 
      ('c', 0.1979883004921),
      ('g', 0.1975473066391),
      ('t', 0.3015094502008));
      
   Alu : String := 
      "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG" &
      "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA" &
      "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT" &
      "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA" &
      "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG" &
      "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC" &
      "AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA";

end Fasta_Utilities;


--
-- package fasta_utilities
--
-- Jim Rogers
--
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Strings.Bounded;
with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;

package body Fasta_Utilities is
   Line_Length : constant := 60;
   Max_Lines   : constant := 1024;
   
   package Out_String is new Ada.Strings.Bounded.Generic_Bounded_Length(Max_Lines * (Line_Length + 1));
   use Out_String;
   
   Last : Integer := 42;

   ---------------------
   -- Make_Cumulative --
   ---------------------

   procedure Make_Cumulative (Gene_List : in out Gene_Array) is
      Cp : Long_Float := 0.0;
   begin
      for I in Gene_List'range loop
         Cp := Cp + Gene_List(I).p;
         Gene_List(I).P := Cp;
      end loop;
   end Make_Cumulative;

   -----------------------
   -- Make_Random_Fasta --
   -----------------------

   procedure Make_Random_Fasta(Id : String; Desc : String; Gene_List : Gene_Array; N : Positive) is
      Todo : Integer := N;
      M : Natural := 0;
      Outline : Bounded_String := Null_Bounded_String;
      Line_Count : Natural := 0;
   begin
      Put_Line('>' & Id & " " & Desc);
      while Todo > 0 loop
         if Todo < Line_Length then
            M := Todo;
         else
            M := Line_Length;
         end if;
         for I in 1..M loop
            append(Outline, Select_Random(Gene_List));
         end loop;
         Append(Outline, Lf); 
         Todo := Todo - Line_Length;
         Line_Count := Line_Count + 1;
         if Line_Count = Max_Lines then
            Put(To_String(Outline));
            Outline := Null_Bounded_String;
            Line_Count := 0;
         end if;
      end loop;
      if Line_Count > 0 then
         Put(To_String(Outline));
      end if;
   end Make_Random_Fasta;

   -----------------------
   -- Make_Repeat_Fasta --
   -----------------------

   procedure Make_Repeat_Fasta(Id : String; Desc : String; S : String; N : Positive) is
      K : Natural := S'First;
      M : Natural := 0;
      Todo : Integer := N;
      Outline : Bounded_String;
      Line_Count : Natural := 0;
   begin
      Put_Line('>' & Id & " " & Desc);
      while Todo > 0 loop
         if Todo < Line_Length then
            M := Todo;
         else
            M := Line_Length;
         end if;
         
         for I in 1..M loop
            Append(Outline, S(K));
            if K = S'Last then
               K := S'First;
            else
               K := K + 1;
            end if;
         end loop;
         Append(Outline, Lf);
         Line_Count := Line_Count + 1;
         Todo := Todo - Line_Length;
         if Line_Count = Max_Lines then
            Put(To_String(Outline));
            Line_Count := 0;
         end if;
      end loop;
      if Line_Count > 0 then
         Put(To_String(Outline));
      end if;
   end Make_Repeat_Fasta;

   ------------
   -- Random --
   ------------

   function Random (Max : Long_Float) return Long_Float is
      Im : constant := 139968;
      Ia : constant := 3877;
      Ic : constant := 29573;
   begin
      Last := (Last * Ia + Ic) mod Im;
      return Max * Long_Float(Last) / Long_Float(Im);
   end Random;

   -------------------
   -- Select_Random --
   -------------------

   function Select_Random(Gene_List : Gene_Array) return Character is
      R : Long_Float := Random(1.0);
   begin
      for I in Gene_List'range loop
         if R < Gene_List(I).P then
            return Gene_List(I).C;
         end if;
      end loop;
      return Gene_List(Gene_List'Last).c;
   end Select_Random;

end Fasta_Utilities;

