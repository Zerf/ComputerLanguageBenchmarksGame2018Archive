/* The Computer Language Shootout
   http://shootout.alioth.debian.org/

   written by Jon Harrop, 2005
	modified by Paul Kitchin, 2006
*/

#include <iostream>
#include <iomanip>
#include <limits>
#include <sstream>
#include <vector>

template < bool condition, typename true_result, typename false_result >
struct static_if
{
	typedef false_result result_type;
};

template < typename true_result, typename false_result >
struct static_if< true, true_result, false_result >
{
	typedef true_result result_type;
};

template < std::size_t n >
struct mod_power_of_two
{
	std::size_t operator()(std::size_t value) const
	{
		return value & (n - 1);
	}
};

template < std::size_t n >
struct mod_non_power_of_two
{
	std::size_t operator()(std::size_t value) const
	{
		return value % n;
	}
};

template < std::size_t n >
struct power_of_2
{
	static bool const result = power_of_2< n / 2 >::result && ((n % 2) == 0);
};

template <>
struct power_of_2< 1 >
{
	static bool const result = true;
};

template < std::size_t n >
struct mod
{
	typedef typename static_if< power_of_2< n >::result, mod_power_of_two< n >, mod_non_power_of_two< n > >::result_type mod_function_t;
	static mod_function_t const mod_function;
	std::size_t operator()(std::size_t value)
	{
		return mod_function(value);
	}
};

template < std::size_t n >
typename mod< n >::mod_function_t const mod< n >::mod_function = typename mod< n >::mod_function_t();

int nsieve(int max)
{
	std::size_t const bits = std::numeric_limits< unsigned char >::digits;
	unsigned char masks[bits];
	for (std::size_t mask = 0; mask < bits; ++mask)
	{
		masks[mask] = 1 << mask;
	}
	mod< bits > mod_function;
	std::vector< unsigned char > sieve((max / bits) + 1);
	int count = 0;
	for (int value = 2; value <= max; ++value)
	{
		if (!(sieve[value / bits] & masks[mod_function(value)]))
		{
			++count;
			for (int multiple = value * 2; multiple <= max; multiple += value)
			{
				sieve[multiple / bits] |= masks[mod_function(multiple)];
			}
		}
	}
	return count;
}

void test(int n)
{
	int m = (1 << n) * 10000;
	std::cout << "Primes up to " << std::setw(8) << m << " " << std::setw(8) << nsieve(m) << std::endl;
}

int main(int argc, char * * argv)
{
	if (argc != 2)
	{
		std::cerr << "usage: nsieve <n>\n";
		return 1;
	}
	std::istringstream convertor(argv[1]);
	int n;
	if (!(convertor >> n) || !convertor.eof())
	{
		std::cerr << "usage: nsieve <n>\n";
		std::cerr << "   n must be an integer\n";
		return 1;
	}
	test(n);
	if (n >= 1)
	{
		test(n - 1);
	}
	if (n >= 2)
	{
		test(n - 2);
	}
}