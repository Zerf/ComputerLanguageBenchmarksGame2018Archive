/* 
	The Computer Language 
	Shootout Benchmarks
	
	http://shootout.alioth.debian.org/

	Contributed by Lester Vecsey
*/

#include <stdio.h>
#include <stdlib.h>

struct node {
	struct node *left, *right;
	signed long item;
	};

int allocate_nodes(struct node **root, int node_depth) {

	if (root==NULL || node_depth<0) return -1;

	*root = (struct node*) malloc(sizeof(struct node) * ((2 << node_depth) - 1) );

	return (*root==NULL) * -1;

	}

void tree_assign_items(struct node **alloc_in, struct node **node_out, signed long item, int depth) {

	struct node *c = (*alloc_in)++;

	if (depth==0) { c->left = c->right = NULL; c->item = 0; }
		else {
		tree_assign_items(alloc_in, & (c->left), 2*item-1, depth-1);
		tree_assign_items(alloc_in, & (c->right), 2*item, depth-1);
		c->item = item;
		}

	*node_out = c;
	
	}

signed long item_check(struct node *n) {

	return n->item + (n->left==NULL ? 0 : item_check(n->left) - item_check(n->right));

	}

int alloc_assign_chk(struct node **root, signed long *chk_result, int count, int depth) {

	if (root==NULL || depth<0) return -1;

	if (allocate_nodes(root, depth) == -1) return -1;

		else {
			struct node *x = *root;
			tree_assign_items(&x, root, count, depth);
		}

	if (chk_result!=NULL) *chk_result += item_check(*root);

	return 0;

	}

void display_line(char *prefix, int depth, signed long chk, signed long iterations) {

	if (prefix!=NULL) printf("%s ", prefix); else printf("%lu\t  ", iterations);

	printf("%s of depth %d\t  check: %li\n", iterations ? "trees" : "tree", depth, chk);

	}

int iterative_lines(struct node **root, int min, int max, char *prefix) {

	signed long base_item, chk, cleave, b=(prefix==NULL), iter = (1<<max) * 4;

	for ( cleave = b?iter>>1:iter-1; min <= max; min+=2, iter>>=1, cleave = iter>>1) {

		for (chk=0, base_item = b; iter>cleave; iter--) {

			if (alloc_assign_chk(root, &chk, base_item, min) == -1) return -1;
			free(*root);

			base_item *= -1; base_item += base_item > 0;

			}

		display_line(prefix, min, chk, iter * b);
		
		}

	return 0;
	
	}

int main(int argc, char *argv[]) {

	struct node *long_lived, *t;

	int min = 4, max = argc > 1 ? strtol(argv[1], 0, 10) : 10;

	if (iterative_lines(&t, max+1, max+1, "stretch") == -1) return -1;

	if (alloc_assign_chk(&long_lived, NULL, 0, max) == -1) return -1;

	if (iterative_lines(&t, min, max, NULL) == -1) return -1;

	display_line("long lived", max, item_check(long_lived), 0);
	
	return 0;

	}
