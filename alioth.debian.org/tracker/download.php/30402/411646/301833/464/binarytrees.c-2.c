/* 
	The Computer Language 
	Shootout Benchmarks
	
	http://shootout.alioth.debian.org/

	Contributed by Lester Vecsey
*/

#include <stdio.h>
#include <stdlib.h>

struct tn {
	struct tn *left, *right;
	signed long item;
	};

struct tr {
	struct tn *root, *nodes;
	int num_nodes;
	};

struct tr_clamp {
	struct tn *pos, *end;
	};

int allocate_tree(struct tr *t, int node_depth) {

	if (t==NULL || node_depth <= 0) return -1;

	t->num_nodes = (2 << node_depth) - 1;

	t->nodes = (struct tn*) malloc(sizeof(struct tn) * t->num_nodes);
	if (t->nodes==NULL) return -1;

	return 0;

	}

int init_clamp(struct tr *t, struct tr_clamp *x) {

	if (t==NULL || x==NULL) return -1;

	x->pos = t->nodes;
	x->end = x->pos + t->num_nodes;

	return 0;

	}

int tree_assign_pointers(struct tr_clamp *x, struct tn **n, signed long item, int depth) {

	struct tn *c;

	if (x==NULL || n==NULL) return -1;

	if (x->pos >= x->end) return -1;

	c = x->pos++;

	if (depth==0) { c->left = c->right = NULL; c->item = 0; }
		else {
		tree_assign_pointers(x, & (c->left), 2*item-1, depth-1);
		tree_assign_pointers(x, & (c->right), 2*item, depth-1);
		c->item = item;
		}

	*n = c;

	return 0;
	
	}

signed long item_check(struct tn *n) {

	return n->item + (n->left==NULL ? 0 : item_check(n->left) - item_check(n->right));

	}

int alloc_walk_chk(struct tr *t, signed long *chk_result, int count, unsigned int depth) {

	struct tr_clamp clamp;

	if (t==NULL) return -1;

	if (allocate_tree(t, depth) == -1) return -1;

	init_clamp(t, &clamp);
	if (tree_assign_pointers(&clamp, &t->root, count, depth) == -1) return -1;

	if (chk_result!=NULL) *chk_result += item_check(t->root);

	return 0;

	}

int main(int argc, char *argv[]) {

	struct tr long_lived, t;

	unsigned int min = 4, max = argc > 1 ? strtol(argv[1], 0, 10) : 10;

	unsigned int depth, stretch_depth = max + 1;

	unsigned long iterations = (1<<max);

	signed long chk = 0;

	int count;

	if (alloc_walk_chk(&t, &chk, 0, stretch_depth) == -1) return -1;
	if (chk != -1) return -1;

	free(t.nodes);

	printf("stretch tree of depth %u\t  check: %li\n", stretch_depth, chk);

	if (alloc_walk_chk(&long_lived, NULL, 0, stretch_depth) == -1) return -1;

	for (depth = min; depth <= max; depth+=2, iterations>>=2) {

		for (count=1, chk=0; count <= iterations*2; count*=-2) {

			if (alloc_walk_chk(&t, &chk, count, depth) == -1) return -1;
			free(t.nodes);
			
			}		
		
		printf("%lu\t trees of depth %d\t  check: %li\n", iterations*2, depth, chk);

		}

	chk = item_check(long_lived.nodes);
	printf("long lived tree of depth %d\t  check: %li\n", max, chk);
	
	return 0;

	}
