/* 
	The Computer Language 
	Shootout Benchmarks
	
	http://shootout.alioth.debian.org/

	Contributed by Lester Vecsey
*/

#include <stdio.h>
#include <stdlib.h>

struct node {
	struct node *left, *right;
	signed long item;
	};

int allocate_nodes(struct node **root, unsigned int node_depth) {

	if (root==NULL) return -1;

	*root = (struct node*) malloc(sizeof(struct node) * ((2 << node_depth) - 1) );

	return (*root==NULL) * -1;

	}

int tree_assign_items(struct node **alloc_in, struct node **node_out, signed long item, int depth) {

	struct node *c;
	
	if (alloc_in==NULL || node_out==NULL) return -1;

	c = (*alloc_in)++;

	if (depth==0) { c->left = c->right = NULL; c->item = 0; }
		else {
		tree_assign_items(alloc_in, & (c->left), 2*item-1, depth-1);
		tree_assign_items(alloc_in, & (c->right), 2*item, depth-1);
		c->item = item;
		}

	*node_out = c;

	return 0;
	
	}

signed long item_check(struct node *n) {

	return n->item + (n->left==NULL ? 0 : item_check(n->left) - item_check(n->right));

	}

int alloc_assign_chk(struct node **root, signed long *chk_result, int count, unsigned int depth) {

	if (root==NULL) return -1;

	if (allocate_nodes(root, depth) == -1) return -1;

		else {
			struct node *x = *root;
			if (tree_assign_items(&x, root, count, depth) == -1) return -1;
		}

	if (chk_result!=NULL) *chk_result += item_check(*root);

	return 0;

	}

int stretch_process(struct node **root, int stretch_depth) {

	signed long chk = 0;

	if (alloc_assign_chk(root, &chk, 0, stretch_depth) == -1) return -1;
	if (chk != -1) return -1;

	free(*root);

	printf("stretch tree of depth %u\t  check: %li\n", stretch_depth, chk);

	return 0;

	}

int iterative_process(struct node **root, int min, int max) {

	signed long base_item, chk, iterations = (1<<max);

	for ( ; min <= max; min+=2, iterations>>=2) {

		for (base_item=1, chk=0; base_item <= iterations; ) {

			if (alloc_assign_chk(root, &chk, base_item, min) == -1) return -1;
			free(*root);

			base_item *= -1; base_item += base_item > 0;

			}		
		
		printf("%lu\t trees of depth %d\t  check: %li\n", iterations*2, min, chk);

		}

	return 0;
	
	}

int main(int argc, char *argv[]) {

	struct node *long_lived, *t;

	unsigned int max = argc > 1 ? strtol(argv[1], 0, 10) : 10;

	if (stretch_process(&t, max + 1) == -1) return -1;

	if (alloc_assign_chk(&long_lived, NULL, 0, max) == -1) return -1;

	if (iterative_process(&t, 4, max) == -1) return -1;

	printf("long lived tree of depth %d\t  check: %li\n", max, item_check(long_lived));
	
	return 0;

	}
