{ The Computer Language Shootout
  http://shootout.alioth.debian.org/

  contributed by Florian Klaempfl
  modified by Micha Nelissen
  modified by Vincent Snijders 
  modified by Steve Fisher
}


type
  TIntegerArray = Array[0..99] of longint;

var
  permu, permu_copy, count: TIntegerArray;
  r, n, answer : longint;

procedure swap(var a, b: longint); inline;
var  tmp: longint;
begin   tmp := a;  a := b;  b := tmp   end;

procedure roll_down( var a : array of longint ); inline;
var  tmp : longint;
begin
  tmp := a[ 0 ];
  move( a[1], a[0], high(a)*sizeof(longint) );
  a[ high(a) ] := tmp
end;

function next_permutation: boolean;
var
  r0: longint;
begin
  r0 := r; // use local variable
  next_permutation := true;
  repeat
    if r0 = n then
    begin
      next_permutation := false;
      break
    end;
    roll_down( permu[ 0..r0 ] );
    dec(count[r0]);
    if count[r0] > 0 then
      break;
    inc(r0)
  until false;
  r := r0
end;


procedure reverse_copy( last: longint ); inline;
var
  pi, pj : pLongint;
begin
  pi := @permu_copy[1];
  pj := @permu_copy[last];
  repeat
    swap(pi^, pj^);
    inc(pi);
    dec(pj)
  until pi >= pj
end;


function flips: longint;  inline;
var last: longint;
begin
  flips := 0;
  last := permu_copy[0];
  repeat
    if last > 2 then   reverse_copy( last-1 );
    swap( permu_copy[ last ], last );
    flips += 1
  until last = 0
end;

function fannkuch: longint;
var
  print30, m, i, tmp: longint;
begin
  fannkuch := 0;
  print30 := 0;
  m := n - 1;

  // Initial permutation.
  for i := 0 to m do   permu[i] := i;

  r := n;
  repeat
    if print30 < 30 then
    begin
      for i := 0 to m do   write(permu[i] + 1);
      writeln;
      inc(print30)
    end;

    while r > 1 do
    begin
      count[r-1] := r;
      dec(r)
    end;

    if (permu[0] <> 0) and (permu[m] <> m) then
    begin
      move(permu[0], permu_copy[0], n*sizeof(longint));
      tmp := flips;
      if tmp > fannkuch then   fannkuch := tmp
    end
  until not next_permutation
end;

begin
  n := 7;
  if paramCount = 1 then   val( paramStr(1), n );
  answer := fannkuch;
  writeln( 'Pfannkuchen(', n, ') = ', answer )
end.
