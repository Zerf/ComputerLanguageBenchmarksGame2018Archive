/* The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 *
 * contributed by Isaac Gouy
 * modified by Uffe Seerup
 * Add multithread by The Anh Tran
 */

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Threading;

sealed class RegexDNA
{
	private static ArrayList source_as_segments, source_as_lines;

	private static string   search_result;
	private static int      replace_len;

	private static int nthreads = Environment.ProcessorCount;
	private static Barrier[] barriers;

	static void Main()
	{
		string input = ReadInput();
		int init_len = input.Length;

		int strip_len = StripHeader(input);
		input = null;

		barriers = new Barrier[r_search_pattern.Length];
		for (int i = 0; i < barriers.Length; ++i)
			barriers[i] = new Barrier(nthreads);

		// spawn threads
		Thread[] threads = new Thread[nthreads];
		for (int i = 0; i < threads.Length; ++i)
		{
			threads[i] = new Thread(worker_function);
			threads[i].Start();
		}

		foreach (Thread t in threads)
			t.Join();

		Console.WriteLine("{0}\n{1}\n{2}\n{3}",
			search_result, init_len, strip_len, replace_len);
	}

	private static string ReadInput()
	{
		string input = Console.In.ReadToEnd();
		return input;
	}

	private static int StripHeader(string input)
	{
		Regex r = new Regex(">.*\n|\n", RegexOptions.Compiled);
		Match match = r.Match(input);

		source_as_segments = new ArrayList();
		source_as_lines = new ArrayList(input.Length / 60 + 8);

		StringBuilder segment = null;
		char[] line = new char[256];
		int copy_index = 0;
		int strip_len = 0;

		while (match.Success)
		{
			if (match.Length > 1) // header line
			{
				if (segment != null)
					source_as_segments.Add(segment.ToString());
				segment = new StringBuilder();
			}

			int copy_len = match.Index - copy_index;
			if (copy_len > 0)
			{
				input.CopyTo(copy_index, line, 0, copy_len);
				segment.Append(line, 0, copy_len);

				String tmp = new String(line, 0, copy_len);
				source_as_lines.Add(tmp);

				strip_len += copy_len;
			}

			copy_index = match.Index + match.Length;
			match = match.NextMatch();
		}

		if (segment != null)
			source_as_segments.Add(segment.ToString());

		return strip_len;
	}

	private static void worker_function()
	{
		CountMatch();
		Replace();
	}

	// C# doesn't allow static var inside function scope :|
	// => have to put static var here for multithread sharing
	private static string[] c_search_pattern = 
	{
		 "agggtaaa|tttaccct",
		 "[cgt]gggtaaa|tttaccc[acg]",
		 "a[act]ggtaaa|tttacc[agt]t",
		 "ag[act]gtaaa|tttac[agt]ct",
		 "agg[act]taaa|ttta[agt]cct",
		 "aggg[acg]aaa|ttt[cgt]ccct",
		 "agggt[cgt]aa|tt[acg]accct",
		 "agggta[cgt]a|t[acg]taccct",
		 "agggtaa[cgt]|[acg]ttaccct"
	};
    
    // this array stores: how many segments are processed of pattern X
    // Eg: ptn_processed[3] = 2 means there are 2 segments have been processed by pattern 4th "agg[act]taaa|ttta[agt]cct"
    private static int[]    c_pattern_processed = new int[c_search_pattern.Length];
    
    // this array stores: how many matched of pattern X
    private static int[]    c_counting_results  = new int[c_search_pattern.Length];
	private static int      c_nthread_passed    = 0;

	private static void CountMatch()
	{
		int total_segment = source_as_segments.Count;

        // foreach search pattern
		for (int ptn_idx = 0; ptn_idx < c_search_pattern.Length; ++ptn_idx)
		{
            // build engine
			Regex rx_engine = new Regex(c_search_pattern[ptn_idx], RegexOptions.Compiled);

			int fetch_segment_index;
			while ((fetch_segment_index = Interlocked.Increment(ref c_pattern_processed[ptn_idx]) - 1) < total_segment)
			{
				String fetched_segment = (String)source_as_segments[fetch_segment_index];
				for (Match m = rx_engine.Match(fetched_segment); m.Success; m = m.NextMatch())
					Interlocked.Increment(ref c_counting_results[ptn_idx]);
			}
		}

		// Allow only the last thread reaching this code block to print result
        if (Interlocked.Increment(ref c_nthread_passed) == nthreads)
		{
			StringBuilder s = new StringBuilder();

			for (int ptn_idx = 0; ptn_idx < c_search_pattern.Length; ++ptn_idx)
				s.AppendFormat("{0} {1}\n", c_search_pattern[ptn_idx], c_counting_results[ptn_idx]);

			search_result = s.ToString();
			source_as_segments = null;
		}
	}


	private static string[] r_search_pattern = 
	{
		"B", "D", "H", "K",
		"M", "N", "R", "S",
		"V", "W", "Y"
	};
	private static string[] r_replace_pattern = 
	{
		"(c|g|t)", "(a|g|t)", "(a|c|t)", "(g|t)",
		"(a|c)", "(a|c|g|t)", "(a|g)", "(c|g)",
		"(a|c|g)", "(a|t)", "(c|t)"
	};

	// this array stores: how many lines are processed of pattern X
	// Eg: ptn_processed[3] = 123 means there are 123 lines have been processed by pattern "K"
	private static int[] r_pattern_processed = new int[r_search_pattern.Length];

    private static void Replace()
	{
		int total_lines = source_as_lines.Count;

		// foreach search pattern
		for (int ptn_idx = 0; ptn_idx < r_search_pattern.Length; ++ptn_idx)
		{
			// build regex engine
			Regex rx_engine = new Regex(r_search_pattern[ptn_idx], RegexOptions.Compiled);

			// fetch not yet processed line
			int line_fetch_index;
			while ((line_fetch_index = Interlocked.Increment(ref r_pattern_processed[ptn_idx]) - 1) < total_lines)
			{
                // get ref to line object from container
				String data_line = (String)source_as_lines[line_fetch_index];

				if (rx_engine.IsMatch(data_line)) // skip line that doesn't match
					source_as_lines[line_fetch_index] = rx_engine.Replace(data_line, r_replace_pattern[ptn_idx]);

				// if this is the last pattern, update replace_len
				if (ptn_idx == r_search_pattern.Length - 1)
				{
					String ln = (String)source_as_lines[line_fetch_index];
					Interlocked.Add(ref replace_len, ln.Length);
				}
			}

			// threads have to wait at the end of each replace pattern
			// to avoid concurrent modifying the same data line (at different ptn_idx values)
			barriers[ptn_idx].Wait();
		}

        source_as_lines = null;
	}
}

internal sealed class Barrier
{
	private volatile int barrier;
	private int nthreads;

	public Barrier(int nthreads)
	{
		this.barrier = this.nthreads = nthreads;
	}

	public void Wait()
	{
		System.Threading.Monitor.Enter(this);
		--barrier;

		if ((0 < barrier) && (barrier < nthreads))
			System.Threading.Monitor.Wait(this);
		else
		{
			barrier = nthreads;
			System.Threading.Monitor.PulseAll(this);
		}

		System.Threading.Monitor.Exit(this);
	}
};
