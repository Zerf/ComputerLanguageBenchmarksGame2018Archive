# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
#
# contributed by Dani Nanz

from partialSums import get_series_from_C_extension

def get_and_print_series(n):
    '''int n --> series up to n as defined at http://shootout.alioth.debian.org/'''
    
    s0, s1, s2, s3, s4, s5, s6, s7, s8 = get_series_from_C_extension(n)
    print ('\n%0.9f\t(2/3)^k' + \
           '\n%0.9f\tk^-0.5' + \
           '\n%0.9f\t1/k(k+1)' + \
           '\n%0.9f\tFlint Hills'  + \
           '\n%0.9f\tCookson Hills' + \
           '\n%0.9f\tHarmonic' + \
           '\n%0.9f\tRiemann Zeta' + \
           '\n%0.9f\tAlternating Harmonic' + \
           '\n%0.9f\tGregory') % \
           (s0, s1, s2, s3, s4, s5, s6, s7, s8)
    return True


if __name__ == '__main__':
    
    from sys import argv
    n = (len(argv) > 1) and int(argv[1]) or 25000
    foo = get_and_print_series(n)
