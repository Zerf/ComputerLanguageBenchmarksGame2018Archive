from distutils.core import setup, Extension

partialSums = Extension('partialSums',
                        sources = ['partialSums.c'],
                        extra_compile_args = ['-ffast-math'])

setup (name = 'partialSums',
       version = '1.0',
       description = 'Calculates partial series in C extension',
       ext_modules = [partialSums])

