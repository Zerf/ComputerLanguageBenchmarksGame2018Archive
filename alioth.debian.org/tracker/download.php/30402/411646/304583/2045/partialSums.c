#include <Python.h>
#include <math.h>

#define kd ((double)k)

static PyObject * get_series_from_C_extension(PyObject *self, PyObject *args) {

    int k, n;
    double k2, k3, sk, ck, a = 1.0;
    double s0 = 0.0, s1 = 0.0, s2 = 0.0, s3 = 0.0, s4 = 0.0, 
           s5 = 0.0, s6 = 0.0, s7 = 0.0, s8 = 0.0;

    if (!PyArg_ParseTuple(args, "i", &n))  return NULL;
    for (k = 0; k <= n; k++) s0 += pow(2.0 / 3.0, kd);
    for (k = 1; k <= n; k++) {
        k2 = kd * kd; 
        k3 = k2 * kd;
        sk = sin(kd); 
        ck = cos(kd);
        s1 += 1.0 / sqrt(kd);
        s2 += 1.0 / (k2 + kd);
        s3 += 1.0 / (k3 * sk * sk);
        s4 += 1.0 / (k3 * ck * ck);
        s5 += 1.0 / kd;
        s6 += 1.0 / k2;
        s7 += a / kd;
        s8 += a / (kd + kd - 1.0);
        a = -a;
    }
    return Py_BuildValue("ddddddddd", s0, s1, s2, s3, s4, s5, s6, s7, s8);
}

static PyMethodDef partialSumsMethods[] = 
    {{"get_series_from_C_extension",  get_series_from_C_extension, 
      METH_VARARGS, "Calculate partial series sums."}};

PyMODINIT_FUNC initpartialSums(void)  {
    (void) Py_InitModule("partialSums", partialSumsMethods);
}




