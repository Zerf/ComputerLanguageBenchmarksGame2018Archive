/* The Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Josh Goldfoot
   adapted from the C version
*/

#include <iostream>
using namespace std;

class permuter {
public:
   permuter(int nx);
   ~permuter();
   void next();
   void print();
   void copy(int* x) { memcpy(x, array, sizeof(int) * n); }
   bool finished() { return done; }
private:
   int n, r;
   int *array, *count;
   bool done;
};

permuter::permuter (int nx)
{
  r = n = nx;
  done = false;
  if (n < 1)
    return;
  array = new int[n];
  count = new int[n];
  for (int i = 0; i < n; ++i)
    array[i] = i;  

}

permuter::~permuter ()
{
  if (array)
    delete array;
  if (count)
    delete count;
}

void permuter::next ()
{
  int i, k, array0;

  for (; r != 1; --r)
    count[r - 1] = r;
  for (;;)
    {
      if (r == n)
	{
	  done = true;
	  break;		// No more permutations.
	}
      // rotate down array[0..r] by one
      array0 = array[0];
      i = 0;
      while (i < r)
	{
	  k = i + 1;
	  array[i] = array[k];
	  i = k;
	}
      array[r] = array0;
      if ((count[r] -= 1) > 0)
	break;
      r++;
    }

}

void permuter::print ()
{
  for (int i = 0; i < n; i++)
    cout << 1 + array[i];
  cout << endl;
}



int main (int argc, char *argv[])
{
  long flips, flipsMax;
  int n, didpr, i, n1, j, k, tmp, *array;

  n = (argc > 1) ? atoi (argv[1]) : 0;
  if (n < 1)
    return -1;
  n1 = n - 1;
  array = new int[n];
  flipsMax = didpr = 0;

  permuter p (n);
  while (!p.finished ())
    {
      if (didpr < 30)
	{
	  p.print ();
	  didpr++;
	}
      p.copy (array);
      if (!(array[0] == 0 || array[n1] == n1))
	{
	  flips = 0;
	  k = array[0];
	  do
	    {
	      for (i = 1, j = k - 1; i < j; ++i, --j)
		{
		  tmp = array[i];
		  array[i] = array[j];
		  array[j] = tmp;
		}
	      ++flips;
	      j = array[k];
	      array[k] = k;
	      k = j;
	    }
	  while (k);

	  if (flipsMax < flips)
	    flipsMax = flips;
	}
      p.next ();
    }
  cout << "Pfannkuchen(" << n << ") = " << flipsMax << endl;
  delete array;
  return 0;
}
