# The Computer Language Shootout
# http://shootout.alioth.debian.org/
# contributed by Josh Goldfoot

import bisect, itertools, sys

alu="GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGGGAGGCCGAGGCGGGCGGATCACCTGAGGTC"\
    "AGGAGTTCGAGACCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAATACAAAAATTAGCCGGGCG"\
    "TGGTGGCGCGCGCCTGTAATCCCAGCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGGAGGCGG"\
    "AGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCCAGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA"
            
iub = ( "acgtBDHKMNRSVWY", [0.27, 0.12, 0.12, 0.27, 0.02, 0.02, 0.02, 
                            0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02] )
homosapiens = ( "acgt", [0.3029549426680, 0.1979883004921, 0.1975473066391,
                         0.3015094502008])
lastrand = 42

def spigot(genelist):
    global lastrand
    while True:
        lastrand = (lastrand * 3877 + 29573) % 139968
        r = lastrand / 139968.0
        yield genelist[0][bisect.bisect(genelist[1],r)]

def writeFasta(header, iterator, n):
    print header
    while n > 0:
        printlen = n < 60 and n or 60
        print "".join([iterator.next() for i in xrange(printlen)])
        n -= printlen

def makeCumulative(genelist):
    cp = 0.0
    for i in xrange(len(genelist)):
        cp += genelist[i]
        genelist[i] = cp

makeCumulative(iub[1])
makeCumulative(homosapiens[1])

n = len(sys.argv)> 1 and int(sys.argv[1]) or 200
writeFasta(">ONE Homo sapiens alu", itertools.cycle(iter(alu)), n * 2)
writeFasta(">TWO IUB ambiguity codes", spigot(iub), n * 3)
writeFasta(">THREE Homo sapiens frequency", spigot(homosapiens), n * 5)
