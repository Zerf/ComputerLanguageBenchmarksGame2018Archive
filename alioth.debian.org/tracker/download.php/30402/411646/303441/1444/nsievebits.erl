%  The Computer Language Shootout
%  http://shootout.alioth.debian.org/
%  contributed by Richard Carlsson

-module(nsievebits).
-export([main/0, main/1]).
-compile([inline]).

-define(WORDSIZE, 27).
-define(TRUEBITS(N), ((1 bsl (N)) - 1)).

main() -> main(["2"]).

main([Arg]) ->
    N = list_to_integer(Arg),
    lists:foreach(fun(I) -> sieve(10000 bsl (N-I)) end, [0,1,2]),
    halt(0).

sieve(M) ->
    Arr = make_array(M),
    Count = sieve(2, lookup_word(Arr, 0), 0, 0, M div ?WORDSIZE, Arr),
    io:format("Primes up to ~8w ~8w\n", [M, Count]),
    ets:delete(Arr),
    Count.

sieve(Bit, Curr, Word, Count, LastWord, Arr) when Bit < ?WORDSIZE ->
    case Curr band (1 bsl Bit) of
	0 ->
	    sieve(Bit + 1, Curr, Word, Count, LastWord, Arr);
	_ ->
	    clear(Bit, Curr, Word, LastWord, Arr),
	    sieve(Bit + 1, lookup_word(Arr, Word), Word,
		  Count + 1, LastWord, Arr)
    end;
sieve(_Bit, _Curr, Word, Count, LastWord, Arr) ->
    NextWord = Word + 1,
    if NextWord =< LastWord ->
	    sieve(0, lookup_word(Arr, NextWord),
		  NextWord, Count, LastWord, Arr);
       true ->
	    Count
    end.

clear(Bit, Curr, Word, LastWord, Arr) ->
    Step = ?WORDSIZE * Word + Bit,
    clear_next(Bit + Step, Curr, Word, Step, LastWord, Arr).

clear(Bit, Curr, Word, Step, LastWord, Arr) ->
    clear_next(Bit + Step, Curr band (bnot (1 bsl Bit)),
	       Word, Step, LastWord, Arr).

clear_next(Bit, Curr, Word, Step, LastWord, Arr) when Bit < ?WORDSIZE ->
    clear(Bit, Curr, Word, Step, LastWord, Arr);
clear_next(Bit, Curr, Word, Step, LastWord, Arr) ->
    ets:insert(Arr, {Word, Curr}),
    NextWord = Word + Bit div ?WORDSIZE,
    if NextWord =< LastWord ->
	    clear(Bit rem ?WORDSIZE, lookup_word(Arr, NextWord),
		  NextWord, Step, LastWord, Arr);
       true -> ok
    end.

lookup_word(Arr, Word) ->
    element(2,hd(ets:lookup(Arr, Word))).

make_array(M) ->
    T = ets:new(?MODULE, [ordered_set, private]),
    init_array(0, M div ?WORDSIZE, M rem ?WORDSIZE, T).

init_array(I, W, R, T) when I < W ->
    ets:insert(T, {I, ?TRUEBITS(?WORDSIZE)}),
    init_array(I + 1, W, R, T);
init_array(I, _, R, T) ->
    ets:insert(T, {I, ?TRUEBITS(R)}),
    T.
