/*
* The Computer Language Benchmarks Game
* http://shootout.alioth.debian.org
*
* Contributed by Paul Kitchin
* contributed by Bob W
* OpenMP by The Anh Tran
* Compile: g++ -O3 -fomit-frame-pointer -mtune=native -msse2 reverse.cpp -o reverse -fopenmp
*/

#include <omp.h>
#include <sched.h>
#include <cstdio>
#include <cstdlib>
#include <memory>


#define Z4		0, 0, 0, 0
#define Z16     Z4, Z4, Z4, Z4
#define Z64     Z16, Z16, Z16, Z16

#define V32     0, 'T', 'V', 'G',	'H', 0, 0, 'C',		\
				'D', 0, 0, 'M',		0, 'K', 'N', 0,		\
				0, 0, 'Y', 'S',		'A', 0, 'B', 'W',	\
				0, 'R', 0, 0,		Z4

#define V256    Z64, V32, V32, Z64, Z64

typedef unsigned int uint;
// char conversion table
static uint const complement_table[] = {V256};
static int const LINE_SIZE = 60;

struct DNA_Marker
{
	int header_offset;
	int header_len;
	
	int data_offset;
	int data_len;
	
	DNA_Marker* next;
	DNA_Marker* previous;
	
	static int total_dna;

	DNA_Marker(DNA_Marker* prev)
	{
		memset (this, 0, sizeof(*this));
		previous = prev;
	}
	
	~DNA_Marker()
	{
		if ( next != 0 )
			delete next;
	}
};
int DNA_Marker::total_dna = 0;


static
long GetInputSize()
{
	long fsz = ftell(stdin);
	fseek(stdin, 0, SEEK_END);
	fsz = ftell(stdin) - fsz;
	fseek(stdin, 0, SEEK_SET);

	return fsz;
}


static
void ReadInput(DNA_Marker* &datamarker, char* &input_buf, char* &output_buf)
{
	// retrieve input size
	long input_size = GetInputSize ();
	DNA_Marker* current_marker = 0;
	DNA_Marker::total_dna = 0;

	// alloc storage
	input_buf = (char*)malloc(input_size);
	output_buf = (char*)malloc(input_size);
	
	char* current_position = input_buf;
	long remain_bytes = input_size;

	// read line-by-line, according to game rule
	while (fgets_unlocked(current_position, remain_bytes, stdin) != 0)
	{
		int len = strlen(current_position);

		if (current_position[0] != '>') // this is data line
		{
			len -= 1; // exclude '\n'
			// update appended data length
			current_position += len;
			current_marker->data_len += len;
			remain_bytes -= len;
		}
		else // this is DNA header description line
		{
			DNA_Marker* new_marker = new DNA_Marker(current_marker);
			DNA_Marker::total_dna++;

			if (current_marker != 0)
				current_marker->next = new_marker;
			else
				datamarker = new_marker;
			current_marker = new_marker;

			current_marker->header_offset = current_position - input_buf;
			current_marker->header_len = len;
			memcpy(output_buf + current_marker->header_offset, current_position, len);
			
			current_marker->data_offset = current_marker->header_offset + len;

			current_position += len;
			remain_bytes -= len;
		}
	}
}

static
void PrintOutput(DNA_Marker const * marker, char const * const output_buffer)
{
	while (marker != 0)
	{
		// write DNA header
		fwrite_unlocked(output_buffer + marker->header_offset, marker->header_len, 1, stdout);

		char const * current_position = output_buffer + marker->data_offset;
		int bytes_remain = marker->data_len;
		int bytes_to_print = std::min(LINE_SIZE, bytes_remain);

		while (bytes_to_print > 0)
		{
			fwrite_unlocked(current_position, bytes_to_print, 1, stdout);
			fputc_unlocked('\n', stdout);

			current_position += bytes_to_print;
			bytes_remain -= bytes_to_print;
			bytes_to_print = std::min(LINE_SIZE, bytes_remain);
		}

		marker = marker->next;
	}
}

static
int GetThreadCount()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	for (int i = 0; i < 8; i++)
	{
		if (CPU_ISSET(i, &cs))
			count++;
	}
	return count;
}

static
void ReverseGame(DNA_Marker const * const dna_marker, char const * const input_buf, char* const output_buf)
{
	// Spawn N threads
	#pragma omp parallel default(shared) num_threads(GetThreadCount())
	{
		int const nthreads = omp_get_num_threads();
		int const thread_id = omp_get_thread_num();
		
		// iterate DNA entries
		DNA_Marker const * marker = dna_marker;
		while (marker != 0)
		{
			int const chunk = marker->data_len / nthreads;
			
			// calculate each thread chunk
			int const r_begin = thread_id * chunk;
			int const r_end = (thread_id < (nthreads -1)) ? (r_begin + chunk) : marker->data_len;

			//char const * src_offset = input_buf + marker->data_offset + marker->data_len -1;
			//char * des_offset = output_buf + marker->data_offset;
			int src_offset = marker->data_offset + marker->data_len -1;
			int des_offset = marker->data_offset;

			// copy reverse - complement char from source buffer to des buffer
			for (int i = r_begin; i < r_end; i++, src_offset--, des_offset++)
			{
				uint c = input_buf[src_offset];
				c = complement_table[c];
				output_buf[des_offset] = (char)c;
			}

			marker = marker->next;
		}
	}
}

int main () 
{
	DNA_Marker* marker = 0;
	char *input_data =0, *output_data =0; 

	ReadInput (marker, input_data, output_data);
	ReverseGame(marker, input_data, output_data);
	PrintOutput(marker, output_data);

	free(input_data);
	free(output_data);
	delete marker;

	return 0;
}

