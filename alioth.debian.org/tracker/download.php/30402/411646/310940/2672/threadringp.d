/*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 * Compiler: D from DigitalMars
 * http://www.digitalmars.com/d/download.html
 * Build command: dmd ringthread.d -O -release
 */

module ThreadRing;

import std.stdio : writefln;
import std.thread : Thread;
import std.conv;

const uint NUM_THREADS = 503;
const uint STACK_SIZE = 16*1024;

RingThread[NUM_THREADS]	rt_arr;
Semaphore[NUM_THREADS]	rt_sema;

int	token = -1;
bool finished = false;

class RingThread : public Thread
{
public:
	uint node_id;
	uint next_id;
	
	this(uint id)
	{
		//super( &run, STACK_SIZE); // tango style
		super( STACK_SIZE ); // phobos style
		
		node_id = id;
		next_id = id +1;
	}

	int run()
	{
		while (true)
		{
			// acquire lock
			rt_sema[node_id].wait();

			if (token != 0)
				token--;
			else
			{
				if (!finished)
				{
					finished = true;
					writefln(node_id +1);
				}
				
				rt_sema[next_id].notify();
				break;
			}
			
			// release lock for next thread
			rt_sema[next_id].notify();
			// give timeslice for next node
			yield();
		}

		return 1;
	}
}

int main(string[] args)
{
	// create NUM_THREADS objects
	foreach (uint i, ref t; rt_arr)
	{
		t = new RingThread( i );
	}

	// link tail node to head node
	rt_arr[$ -1].next_id = 0;
	
	// create semaphore objects
	foreach (ref s; rt_sema)
	{
		s = new Semaphore( 1 );
		s.wait(); // set semaphore state to LOCKED
	}

	try
	{
		token = toInt(args[1]);
	}
	catch (Exception e)	
	{
		token = 1000; // test case
	}

	// let's roll
	foreach (t; rt_arr)
		t.start();
	
	// release head node's lock
	rt_sema[0].notify();

	// waiting for result
	foreach (t; rt_arr)
		t.wait();

	return 1;
}
//-----------------------------------------------------------------------------
// The above source will compile successfully if we use Tango library.
// However, Alioth is using D std lib - Phobos.
// Because Phobos has no semaphore class,
// i have to copy & paste Semaphore class from Tango library.
//-----------------------------------------------------------------------------
version ( linux )
{
	// copy from tango.core.sync.semaphore.d
	private class Semaphore
	{
	private:
		sem_t   m_hndl;

	public:
		this( uint count )
		{
			int rc = sem_init( &m_hndl, 0, count );
			if ( rc )
				throw new Exception( "Unable to create semaphore" );
		}

		~this()
		{
			int rc = sem_destroy( &m_hndl );
			assert( !rc, "Unable to destroy semaphore" );
		}

		void wait()
		{
			while( true )
			{
				if( !sem_wait( &m_hndl ) )
					return;

				if( errno != EINTR )
					throw new Exception( "Unable to wait for semaphore" );
			}
		}

		void notify()
		{
			int rc = sem_post( &m_hndl );

			if( rc )
				throw new Exception( "Unable to notify semaphore" );
		}
	}
	// copy from tango.stdc.errno.d
	int errno()		{	return getErrno();	}
	int errno(int e)	{	return setErrno(e);	}
	const EINTR	= 4;        // Interrupted system call

	extern (C) // import OS API
	{
		// copy from tango.stdc.posix.semaphore.d
		private alias int __atomic_lock_t;
		private alias long c_long;

		private struct _pthread_fastlock
		{
			c_long            __status;
			__atomic_lock_t   __spinlock;
		}

		struct sem_t
		{
			_pthread_fastlock __sem_lock;
			int               __sem_value;
			void*             __sem_waiting;
		}

		const SEM_FAILED    = cast(sem_t*) null;

		int sem_destroy(sem_t*);
		int sem_init(sem_t*, int, uint);
		int sem_post(sem_t*);
		int sem_wait(sem_t*);

		// copy from tango.stdc.errno.d
		int getErrno();
		int setErrno(int v);

	} // end extern (C)
} // end version (Linux)

