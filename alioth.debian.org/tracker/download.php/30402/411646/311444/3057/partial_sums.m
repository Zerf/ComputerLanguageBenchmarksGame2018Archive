n = 2500000;
k = 1:n;
k2 = k .* k;
k3 = k2 .* k;
sink = sin(k);
cosk = cos(k);
sink2 = sink .* sink;
cosk2 = cosk .* cosk;

s = 0;
a = (2/3).^k;
s = sum(a) + 1;
[str, err] = sprintf('%.9f\t(2/3)^k', s);
disp(str)

s = 0;
a = 1./sqrt(k);
s = sum(a);
[str, err] = sprintf('%.9f\tk^-0.5', s);
disp(str)

s = 0;
a = 1./(k.*(k + 1));
s = sum(a);
[str, err] = sprintf('%.9f\t1/k(k+1)', s);
disp(str)

s = 0;
a = 1./(k3 .* sink2);
s = sum(a);
[str, err] = sprintf('%.9f\tFlint Hills', s);
disp(str)

s = 0;
a = 1./(k3 .* cosk2);
s = sum(a);
[str, err] = sprintf('%.9f\tCookson Hills', s);
disp(str)

s = 0;
a = 1./k;
s = sum(a);
[str, err] = sprintf('%.9f\tHarmonic', s);
disp(str)

s = 0;
a = 1./k2;
s = sum(a);
[str, err] = sprintf('%.9f\tReimann Zeta', s);
disp(str)

s = 0;
a(1:2:n) = 1;
a(2:2:n) = -1;
s = sum(a./k);
[str, err] = sprintf('%.9f\tAlternating Harmonic', s);
disp(str)

s = 0;
a(1:2:n) = 1;
a(2:2:n) = -1;
s = sum(a./(2.*k - 1));
[str, err] = sprintf('%.9f\tGregory', s);
