//==================================================================================================
// The Great Computer Language Shootout
// http://shootout.alioth.debian.org
// 
// This submission is a little bit crazy because it compiles and loads an inline Java class.  
// The compilation is performed entirely in RAM, so no additional files are created, and no 
// system processes are called.  It's nothing but Groovy code implementing standard JDK APIs. 
// 
// The inline Java function here implements the tr functionality found in Perl and Ruby, 
// and results in a huge speed up over the previous Groovy implementation.  
// 
// The Groovy code to compile inline java classes is derived from this example:
// http://www.velocityreviews.com/forums/t318697-javacompilertool.html
//
// There is a proposal floating around to build this kind of Java inlining right into 
// Groovy so that the verbose incancantation at the end of this file isn't needed.  
// 
// Contributed by James Durbin
// 
// 

import java.io.*;
import java.util.*;
import javax.tools.*;
import javax.tools.JavaCompiler.*;
import javax.tools.SimpleJavaFileObject;
import com.sun.tools.javac.util.*;
import java.net.URI;


disableStdOutLineFlushing();

//===============================================================================
//  Define an inline Java helper class..
//===============================================================================

// A little java class to implement perl-like tr (translate) functionality. 
// The Groovy multi-line triple quote syntax makes it really easy to write this out
String javaClass = """ 

package inline.java;
import java.util.*;
public class JavaHelper{

  public static StringBuilder tr(String pattern1,String pattern2, StringBuilder source){		
		
		Map<Character,Character> m = new HashMap<Character,Character>();
		for(int i = 0;i < pattern1.length();i++){
			m.put(pattern1.charAt(i),pattern2.charAt(i));
		}
		
		for(int i = 0;i < source.length();i++){
			char c = source.charAt(i);
			if (m.containsKey(c)){
				source.setCharAt(i,m.get(c));
			}else{
				source.setCharAt(i,c);
			}
		}
		return(source);
	} 		 
}                              

""";

// Compile and load this helper class...
javaHelp = getHelperClass(javaClass,"inline.java.JavaHelper");


//===============================================================================
//  Main Groovy code.
//===============================================================================

def printRevComp(buf) {
	buf.reverse();
		
	// Using our java class..		
	buf = javaHelp.tr("wsatugcyrkmbdhvnATUGCYRKMBDHVN",
                    "WSTAACGRYMKVHDBNTAACGRYMKVHDBN",buf);	
	
	int stop = buf.length()/60;
	int lineNum = 0,j=0;
	while (lineNum < stop) {
		System.out.println(buf.subSequence(j,j+60));
		j+=60;
		lineNum++;
	}
	if (j < buf.length()) {
		System.out.println(buf.subSequence(j,buf.length()));
	}
}
		
def buf = new StringBuilder();
def i = 0;
System.in.readLines().each{line->
	if (line.startsWith(">")) {
		printRevComp(buf)
		System.out.println(line);
		buf = new StringBuilder();
	} else {
		buf.append(line);
	}
}
printRevComp(buf)
System.out.flush(); // No auto-flush on exit... Groovy bug?


//===================================================================================
// Now for the nasty part, compiling and loading a Java class from a string entirely 
// in RAM. This code is generic and could be used to compile any Java class defined 
// in a String. 
//===================================================================================

//============================================================
// This is kind of doing double duity, depending on which 
// constructor is called.  
class RAMJavaFileObject extends SimpleJavaFileObject {
  
  def baos;
	def contents = "";

	RAMJavaFileObject(name,kind) {super(new URI(name), kind);}
	
	RAMJavaFileObject(String className,String contents){
    super(new URI(className), JavaFileObject.Kind.SOURCE);
		this.contents = contents;
	}
	
	public CharSequence getCharContent(boolean ignoreEncodingErrors){return(contents);}

	public InputStream openInputStream(){
		return new ByteArrayInputStream(baos.toByteArray());
	}

	public OutputStream openOutputStream(){
		return baos = new ByteArrayOutputStream();
	}

}

//============================================================
//
public class RAMJavaFileManager extends ForwardingJavaFileManager<DefaultFileManager>{   

  Map<String, JavaFileObject> output;
  
  public RAMJavaFileManager(DefaultFileManager dfm,Map<String, JavaFileObject> output){
    super(dfm)
    this.output = output;
  }
  
  public JavaFileObject getJavaFileForOutput(JavaFileManager.Location location, String name, 
    JavaFileObject.Kind kind,FileObject sibling){
      JavaFileObject jfo = new RAMJavaFileObject(name,kind);
      output.put(name,jfo);
      return(jfo);
    }  
}

//============================================================
//
public class RAMClassLoader extends ClassLoader{
  
  Map<String, JavaFileObject> output;
  
  RAMClassLoader(Map<String, JavaFileObject> output){this.output = output;}
    
  protected Class<?> findClass(String name){
    JavaFileObject jfo = output.get(name);
    if (jfo != null){
      byte[] bytes = ((RAMJavaFileObject) jfo).baos.toByteArray();
      return(defineClass(name,bytes,0,bytes.length));
    }
    return(super.findClass(name));
  }
}

//==========================================
// Function to compile and create the helper class given by 
// javaFileContents. 
// 
def getHelperClass(String javaFileContents,String className){
  
  Map<String, JavaFileObject> output = new HashMap<String, JavaFileObject>(); 

  JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
  jfm = new RAMJavaFileManager(compiler.getStandardFileManager(null,null,null),output)  
  
  javaFileObject = new RAMJavaFileObject("JavaHelper.java", javaFileContents);		
  compiler.getTask(null,jfm,null, null, null,[javaFileObject]).call();
  
  ClassLoader cl = new RAMClassLoader(output);
  Class<?> javaHelp = Class.forName(className,false,cl);
  
  return(javaHelp)
}


/******************************
* Disable line buffering. 
* By default, System.out flushes the buffer every time a newline is encountered. 
* Here we disable this and use a nice big buffer instead. 
*/
def disableStdOutLineFlushing(){				
	FileOutputStream fdout = new FileOutputStream(FileDescriptor.out);
	BufferedOutputStream bos = new BufferedOutputStream(fdout,65536);
	PrintStream ps = new PrintStream(bos,false);
	System.setOut(ps);		
}


