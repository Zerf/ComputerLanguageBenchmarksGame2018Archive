"* The Computer Language Shootout
    http://shootout.alioth.debian.org/
    contributed by Carlo Teixeira*"!

'From VisualWorks® NonCommercial, 7.4.1 of May 30, 2006 on January 22, 2008 at 1:52:52 am'!


Object subclass: #ChameneosColour
	instanceVariableNames: 'color '
	classVariableNames: 'Blue Red Yellow '
	poolDictionaries: ''
	category: 'chameleon'!

ChameneosColour class
	instanceVariableNames: ''!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!ChameneosColour class methodsFor: 'accessing'!

blue
	^Blue!

blue: anObject
	Blue := anObject!

red
	^Red!

red: anObject
	Red := anObject!

yellow
	^Yellow!

yellow: anObject
	Yellow := anObject! !

!ChameneosColour class methodsFor: 'initialize-release'!

createBlue
	"comment stating purpose of message"

	^super new color: #blue!

createRed
	"comment stating purpose of message"

	^super new color: #red!

createYellow
	"comment stating purpose of message"

	^super new color: #yellow!

initialize
	"self initialize"

	Red := self createRed.
	Blue := self createBlue.
	Yellow := self createYellow! !

!ChameneosColour class methodsFor: 'printing'!

generateReportOfColours
	| readOut colours |
	colours := Array 
				with: Blue
				with: Red
				with: Yellow.
	readOut := WriteStream on: String new.
	colours do: 
			[:aColour | 
			colours do: 
					[:anotherColour | 
					aColour printOn: readOut.
					readOut nextPutAll: ' + '.
					anotherColour printOn: readOut.
					readOut nextPutAll: ' -> '.
					(aColour complementaryColourFor: anotherColour) printOn: readOut.
					readOut cr]].
	^readOut! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!ChameneosColour methodsFor: 'as yet unclassified'!

complementaryColourFor: aChameneosColour 
	"determine the complementary colour defined as..."

	self == aChameneosColour ifTrue: [^self].
	self isBlue 
		ifTrue: 
			[aChameneosColour isRed 
				ifTrue: [^self class yellow]
				ifFalse: [^self class red]].
	self isRed 
		ifTrue: 
			[aChameneosColour isBlue 
				ifTrue: [^self class yellow]
				ifFalse: [^self class blue]].
	aChameneosColour isBlue 
		ifTrue: [^self class red]
		ifFalse: [^self class blue]! !

!ChameneosColour methodsFor: 'testing'!

hasSameColorAs: aChameneos 
	^self color == aChameneos color!

isBlue
	^self == self class blue!

isRed
	^self == self class red!

isYellow
	^self == self class yellow! !

!ChameneosColour methodsFor: 'accessing'!

color
	^color!

color: aColor 
	color := aColor! !

!ChameneosColour methodsFor: 'printing'!

printOn: aStream 
	aStream nextPutAll: self color! !

ChameneosColour initialize!

Object subclass: #Creature
	instanceVariableNames: 'creatureName colour selfMet creaturesMet '
	classVariableNames: ''
	poolDictionaries: ''
	category: 'chameleon'!

Creature class
	instanceVariableNames: ''!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Creature class methodsFor: 'initialize-release'!

withName: aName colour: aColour 
	| creature |
	creature := Creature new initialize.
	creature name: aName.
	creature colour: aColour.
	^creature! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Creature methodsFor: 'accessing'!

colour
	^colour!

colour: anObject 
	colour := anObject!

creaturesMet
	^creaturesMet!

creaturesMet: anObject 
	creaturesMet := anObject!

name
	^creatureName!

name: anObject 
	creatureName := anObject!

selfMet
	^selfMet!

selfMet: anObject 
	^selfMet := anObject! !

!Creature methodsFor: 'initialize-release'!

initialize
	selfMet := 0.
	creaturesMet := 0! !

!Creature methodsFor: 'controlling'!

visitMall: mall 
	
	[| partner |
	partner := mall visitWith: self.
	partner ifNotNil: 
			[colour := colour complementaryColourFor: partner colour.
			self == partner ifTrue: [selfMet := selfMet + 1].
			creaturesMet := creaturesMet + 1].
	partner isNil] 
			whileFalse! !

Object subclass: #Mall
	instanceVariableNames: 'guard maxRendezvous open process queue cache pairCache '
	classVariableNames: 'Units '
	poolDictionaries: ''
	category: 'chameleon'!

Mall class
	instanceVariableNames: ''!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Mall class methodsFor: 'as yet unclassified'!

runBenchMark
|firstTestColours secondTestColours|

Transcript show: ChameneosColour generateReportOfColours contents; cr.

firstTestColours :=Array with: ChameneosColour  createBlue with: ChameneosColour createRed  with: ChameneosColour createYellow.

secondTestColours :=(OrderedCollection new) 
add: ChameneosColour  createBlue; add: ChameneosColour createRed ; add: ChameneosColour createYellow; add:ChameneosColour createRed ; add:ChameneosColour createYellow; add:ChameneosColour createBlue; add:ChameneosColour createRed; add:ChameneosColour createYellow; add:ChameneosColour createRed; add:ChameneosColour createBlue; yourself.

Mall openMallWith: firstTestColours  forNumberOfMeets: 6000.
Transcript cr.
Mall openMallWith: secondTestColours  forNumberOfMeets: 6000.
!

runBenchMark: number
|firstTestColours secondTestColours|

Transcript show: ChameneosColour generateReportOfColours contents; cr.

firstTestColours :=Array with: ChameneosColour  createBlue with: ChameneosColour createRed  with: ChameneosColour createYellow.

secondTestColours :=(OrderedCollection new) 
add: ChameneosColour  createBlue; add: ChameneosColour createRed ; add: ChameneosColour createYellow; add:ChameneosColour createRed ; add:ChameneosColour createYellow; add:ChameneosColour createBlue; add:ChameneosColour createRed; add:ChameneosColour createYellow; add:ChameneosColour createRed; add:ChameneosColour createBlue; yourself.

Mall openMallWith: firstTestColours  forNumberOfMeets: number.
Transcript cr.
Mall openMallWith: secondTestColours  forNumberOfMeets: number.
! !

!Mall class methodsFor: 'printing'!

generateReportFor: creatures 
	| sum readOut |
	readOut := WriteStream on: String new.
	creatures do: 
			[:aCreature | 
			aCreature creaturesMet printOn: readOut.
			readOut space.
			aCreature selfMet printOn: readOut.
			readOut cr].
	sum := creatures inject: 0 into: [:accum :each | accum + each creaturesMet].
	readOut space.
	sum printString do: 
			[:el | 
			readOut
				nextPutAll: (self units at: el digitValue + 1);
				space].
	^readOut!

generateReportForColours: colours 
	| readOut |
	readOut := WriteStream on: String new.
	colours do: 
			[:colour | 
			colour printOn: readOut.
			readOut space].
	^readOut! !

!Mall class methodsFor: 'initialize-release'!

createCreaturesWith: aCollectionOfColours 
	| aName |
	aName := 0.
	^aCollectionOfColours collect: 
			[:aColour | 
			aName := aName + 1.
			Creature withName: aName colour: aColour]!

initialize
	"self initialize"

	Units := #('zero' 'one' 'two' 'three' 'four' 'five' 'six' 'seven' 'eight' 'nine')!

new
	^super new initialize!

openMallWith: aCollectionOfColours forNumberOfMeets: aNumber 
	| mall creatures guard readOut |
	readOut := WriteStream on: String new.
	guard := Semaphore new.
	mall := Mall new.
	mall maxRendezvous: aNumber.
	mall run.
	readOut
		nextPutAll: (self generateReportForColours: aCollectionOfColours) contents;
		cr.
	creatures := self createCreaturesWith: aCollectionOfColours.
	self 
		openMall: mall
		forCreatures: creatures
		usingGuard: guard.
	self 
		waitForClosingOfMall: mall
		withCreatures: creatures
		usingGuard: guard.
	readOut
		nextPutAll: (self generateReportFor: creatures) contents;
		cr.
	^readOut! !

!Mall class methodsFor: 'private'!

closeMall: aMall forCreatures: creatures usingGuard: guard 
	creatures size timesRepeat: [guard wait]!

openMall: aMall forCreatures: creatures usingGuard: sema 
	| processes |
	processes := creatures 
				collect: [:aCreature | 
					[aCreature visitMall: aMall.
					sema signal] newProcess].
	processes do: [:proc | proc resume]!

waitForClosingOfMall: aMall withCreatures: creatures usingGuard: guard 
	creatures size timesRepeat: [guard wait].
	aMall close! !

!Mall class methodsFor: 'accessing'!

units
	^Units! !

!Mall class methodsFor: 'public'!

performTest
	self runBenchMark: Tests arg on: Tests stdout.
	^''!

runBenchMark: number on: anOutputStream 
"
self runBenchMark: 60000 on: Transcript.
"
	| firstTestColours secondTestColours |
	anOutputStream
		nextPutAll: ChameneosColour generateReportOfColours contents;
		cr.
	firstTestColours := Array 
				with: ChameneosColour blue
				with: ChameneosColour red
				with: ChameneosColour yellow.
	secondTestColours := (OrderedCollection new)
				add: ChameneosColour blue;
				add: ChameneosColour red;
				add: ChameneosColour yellow;
				add: ChameneosColour red;
				add: ChameneosColour yellow;
				add: ChameneosColour blue;
				add: ChameneosColour red;
				add: ChameneosColour yellow;
				add: ChameneosColour red;
				add: ChameneosColour blue;
				yourself.
	anOutputStream 
		nextPutAll: (Mall openMallWith: firstTestColours forNumberOfMeets: number) 
				contents.
	anOutputStream cr.
	anOutputStream 
		nextPutAll: (Mall openMallWith: secondTestColours forNumberOfMeets: number) 
				contents.
	anOutputStream flush! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


Mall comment:
''!

!Mall methodsFor: 'accessing'!

maxRendezvous: max 
	maxRendezvous := max! !

!Mall methodsFor: 'private'!

obtainPair
	^cache removeFirst!

processVisitors
	[open] whileTrue: 
			[1 to: maxRendezvous
				do: 
					[:x | 
					| first second |
					first := queue next.
					second := queue next.
					self setPartnersOn: first and: second.
					first signal.
					second signal].
			[queue isEmpty] whileFalse: [queue next signal]].
	process terminate.
	process := nil!

releasePair: pair 
	pair release.
	cache addFirst: pair!

setPartnersOn: first and: second
	first partner: second me.
	second partner: first me.
!

shutdown
	[queue isEmpty] whileFalse: [queue next signal].
	process terminate.
	process := nil! !

!Mall methodsFor: 'initialize-release'!

initialize
	guard := Semaphore forMutualExclusion.
	queue := SharedQueue new.
	cache := OrderedCollection new.
	"Use 11 here as it is more than we need so that we do not unneccasarily grow the cache"
	1 to: 11 do: [:x | cache add: Pair new]!

run
	open := true.
	process ifNil: 
			[process := [self processVisitors] newProcess.
			process priority: Processor userBackgroundPriority].
	process resume! !

!Mall methodsFor: 'controlling'!

close
	open := false!

visitWith: aChameneos 
	| pair partner |
	pair := self obtainPair.
	pair me: aChameneos.
	queue nextPut: pair.
	pair wait.
	partner := pair partner.
	self releasePair: pair.
	^partner! !

Mall initialize!

Object subclass: #Pair
	instanceVariableNames: 'partner me sema '
	classVariableNames: ''
	poolDictionaries: ''
	category: '(none)'!

Pair class
	instanceVariableNames: ''!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Pair class methodsFor: 'instance creation'!

new
	"Answer a newly created and initialized instance."
	^super new initialize.!

with: me 
	"Answer a newly created and initialized instance."
self halt.
	^super new initialize me: me! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


Pair comment:
''!

!Pair methodsFor: 'accessing'!

me
	^me!

me: anObject
	me := anObject!

partner
	^partner!

partner: anObject
	partner := anObject! !

!Pair methodsFor: 'initialize-release'!

initialize
	"Initialize a newly created instance. This method must answer the receiver."

	partner := nil.
	me := nil.
	sema := Semaphore new.
	^self!

release
partner:=nil.!

signal
	sema signal!

wait
	sema wait! !

Object subclass: #Tests
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Shootout'!

Tests class
	instanceVariableNames: ''!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!Tests class methodsFor: 'platform'!

arg
   ^CEnvironment commandLine last asNumber!

stdin
   ^Stdin!

stdinSpecial
   ^ExternalReadStream on:
      (ExternalConnection ioAccessor: (UnixDiskFileAccessor new handle: 0))!

stdout
   ^Stdout!

stdoutSpecial
   ^ExternalWriteStream on:
      (ExternalConnection ioAccessor: (UnixDiskFileAccessor new handle: 1))! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

