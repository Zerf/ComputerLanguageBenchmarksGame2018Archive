import System
import Data.List(foldl')
import GHC.Base

{-
 A simple, clear functinal implementation.

 Slightly more convoluted permutation generator to conform to a C-style
 solution (which seems to be what this benchmarks is _really_ about)

 By Sebastian Sylvan
-}

{-# INLINE rotate #-}
rotate n (x:xs) = let (a,b) = splitAt (n-1) xs in a ++ x : b

{-# INLINE perms #-}
perms l = foldr perm' [l] [2..length l] 
    where perm' n ls = concat [take n (iterate (rotate n) l) | l <- ls]

{-# INLINE flop #-}
flop (1:_) = 0
flop xs = 1 + flop (rev xs)

{-# INLINE rev #-}
rev (x:xs) = reverse a ++ x : b
      where (a,b) = splitAt (x-1) xs

fannuch xs = foldl' max 0 $ map flop xs

main = do [n] <- getArgs
          let xs = perms [1..read n]
          putStr $ unlines $ map (concatMap show) $ take 30 xs
          putStrLn $ "Pfannkuchen(" ++ n ++ ") = " ++ show (fannuch xs)