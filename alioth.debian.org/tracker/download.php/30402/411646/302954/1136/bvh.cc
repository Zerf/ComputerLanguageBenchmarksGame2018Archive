// The Great Computer Language Shootout
// http://shootout.alioth.debian.org/
// contributed by Jon Harrop, 2005
// rewritten by Thierry Berger-Perrin, 2005
// for a discussion, see comp.graphics.rendering.raytracing 'mini ray tracer' thread (may 2005).
// Compile: g++ -Wall -O3 -ffast-math bvh.cc -o bvh

#include <cmath>
#include <iostream>

static const double
  	infinity = std::numeric_limits<double>::infinity(),
	epsilon = sqrt(std::numeric_limits<double>::epsilon());

struct vec_t {
	double x,y,z; vec_t() {}
	vec_t(double a,double b,double c): x(a),y(b),z(c) {}
	vec_t operator+(const vec_t &v) const { return vec_t(x+v.x,y+v.y,z+v.z); }
	vec_t operator-(const vec_t &v) const { return vec_t(x-v.x,y-v.y,z-v.z); }
	vec_t operator-() const { return vec_t(-x,-y,-z); }
	vec_t operator*(const double d) const { return vec_t(x*d,y*d,z*d); }
	double dot(const vec_t &v) const { return x*v.x + y*v.y + z*v.z; }
	double magsqr() const { return dot(*this); }
	vec_t norm() const { return *this*(1./sqrt(magsqr())); }
};

static const vec_t light(vec_t(-1, -3, 2).norm());

struct ray_t { 
	vec_t o, d; ray_t(const vec_t &v) : o(v) {}
	ray_t(const vec_t &v, const vec_t &w) : o(v),d(w) {}
};

struct hit_t { vec_t n; double t; hit_t() : n(vec_t(0,0,0)), t(infinity) {} };

struct sphere_t {
	vec_t  o; double  r;
	sphere_t() {} 
	sphere_t(const vec_t &v, double d) : o(v),r(d) {} // 1/r, constant
	vec_t get_normal(const vec_t &v) const { return (v-o)*(1./r); } 
	double intersect(const ray_t &ray) const {
		const vec_t v(o-ray.o);
		const double b = ray.d.dot(v), disc = b*b - v.magsqr() + r*r;
		if (disc < 0.) return infinity; // branch away from the square root
		// this is where most cycles are wasted
		const double d = sqrt(disc), t2 = b + d, t1 = b - d;
		if (t2 < 0.) return infinity;
		else return (t1 > 0. ? t1 : t2); // cond. move
	}
};

struct node_t;
static node_t *pool = 0, *end = 0;
struct node_t { // used in array form + skip for navigation.
	sphere_t bound, leaf;
	long diff; // gah, 2*32 + some (poor alignment). diff should be moved.
	node_t() {}
	node_t(const sphere_t &b,const sphere_t &l, const int jump) : bound(b), leaf(l), diff(jump) {}
	
	// see Brian Smits paper, http://www.cs.utah.edu/%7Ebes/papers/fastRT/paper-node12.html
	template<bool shadow> static void intersect(const ray_t &ray, hit_t &hit) {
		//use 'const node_t *p = pool;' if your compiler doesn't support it
		const node_t * __restrict__ p = pool;
		while (p < end) {
			if (p->bound.intersect(ray) >= hit.t)		// missed bound?
				p = (p->diff > 0) ? p+p->diff : end;        	// skip
			else {
				const double t = p->leaf.intersect(ray); 
				
				if (t < hit.t) { // if hit, update
					hit.t = t; 
					if (shadow) break; // shadows don't need no normal
					hit.n = p->leaf.get_normal(ray.o + ray.d*t);
				}
				++p;  // next
			}
		}
	}
};

static double ray_trace(const node_t * const scene, const ray_t &ray) {
	hit_t hit; scene->intersect<false>(ray, hit);		// trace primary
	const double diffuse = (hit.t == infinity) ? 0. : -hit.n.dot(light);
	if (diffuse <= 0.) return 0.;
	const ray_t sray(ray.o + (ray.d*hit.t) + (hit.n*epsilon), -light);
	hit_t shit; scene->intersect<true>(sray,shit);	// trace shadow
	return (shit.t == infinity) ? diffuse : 0.;
}

template <int ss> static void trace_grid_aa(const int width, const int height) {
	ray_t ray(vec_t(0, 0, -4)); enum { ss_sqr = ss*ss };
	const double w = width, h = height, scale = 255. / double(ss_sqr);
	vec_t grid[ss*ss]; // pre compute a regular grid (a rg(ss) would be better...)
	{ const double rcp_ss = 1./double(ss), halfw = w/2, halfh = h/2; 
		for (int idx=0, dx=0; dx<ss; ++dx) for (int dy=0; dy<ss; ++dy)
			grid[idx++] = vec_t(double(dx)*rcp_ss-halfw, double(dy)*rcp_ss-halfh, 0);
	} // ok, now let's get back to work
	
	vec_t v(0,w-1, std::max(w,h));  // scan line
	for (int i=height-1; i>=0; --i) {
		for (int j=width-1; j>=0; --j) {
			double g=0;
			for (int idx = 0; idx < ss_sqr; ++idx) {	 // apply grid
				ray.d = (v+grid[idx]).norm();
				g += ray_trace(pool, ray);		// trace
			}
			std::cout << char(.5 + scale*g);
			v.x += 1.;  // next pixel
		}
		v.x = 0; v.y -= 1.; // next line
	}
}

static node_t *create(node_t *node, const int level, int dist, const double r, const vec_t &v) {
	// directly write the tree as an array via placement new,
	// as that tree is complete, we can compute each node address
	// and write that tree in one pass instead of two.
	if (level > 1) {
		// we can use a tighter bound.
		// node = 1+new (node) node_t(sphere_t(v,3.*r),sphere_t(v, r), dist+1);
		node = 1+new (node) node_t(sphere_t(v+vec_t(0,r,0),2.4330128*r),sphere_t(v, r), dist+1);
		dist = std::max((dist-4)/4, 0); // *(distance>1?4:0) + 4;
		const double rn = (3/sqrt(12.))*r;
		for (int dz=-1; dz<=1; dz+=2)
			for (int dx=-1; dx<=1; dx+=2) // go down
				node = create(node, level-1, dist,0.5*r,v+vec_t(-dx*rn,+rn,-dz*rn));
		return node;
	}
	else return 1+new (node) node_t(sphere_t(v, r),sphere_t(v, r),1); // final
}

int main(int argc, char *argv[]) {
	enum { level = 6, ss = 4 };
	const int n = (argc==2 ? atoi(argv[1]) : 256);
	int count = 4, dec = level; while (--dec > 1) count = (count*4) + 4;
	pool = new node_t[count+1]; end = pool + count; // raw
	create(pool, level, count, 1., vec_t(0, -1, 0)); // cooked
	std::cout << "P5\n" << n << " " << n << "\n255\n";
	trace_grid_aa<ss>(n,n); // served	
	delete [] pool;
	return 0;  
}
