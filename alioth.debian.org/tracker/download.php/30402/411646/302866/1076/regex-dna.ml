(* 
 *
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * compilation: ocamlopt -noassert -unsafe -ccopt -O3  str.cmxa regex-dna.ml -o regex-dna 
 *
 * Contributed by Matthieu Dubuget
 *)

let eat s =
  let b = Buffer.create 0x19000 in    
    try 
      while true do
	Buffer.add_string b ((input_line s) ^ "\n")
      done; 
      ""
    with End_of_file -> Buffer.contents b

let clean s =
  let r = Str.regexp "^>.*$\\|\n" in
    Str.global_replace r "" s

let occurs s r =
  let rec cpt_rec r s offset c =
    let eos = 
      try
	ignore(Str.search_forward r s offset);
	false
      with Not_found -> true in
      if eos then c
      else 
	begin
	  cpt_rec r s (Str.match_end ()) (succ c)
	end in
    cpt_rec r s 0 0

let patterns =  ["agggtaaa\\|tttaccct";
		 "[cgt]gggtaaa\\|tttaccc[acg]";
		 "a[act]ggtaaa\\|tttacc[agt]t";
		 "ag[act]gtaaa\\|tttac[agt]ct";
		 "agg[act]taaa\\|ttta[agt]cct";
		 "aggg[acg]aaa\\|ttt[cgt]ccct";
		 "agggt[cgt]aa\\|tt[acg]accct";
		 "agggta[cgt]a\\|t[acg]taccct";
		 "agggtaa[cgt]\\|[acg]ttaccct"]

let apply_pattern ch rs =
  Printf.printf "%s %n\n" rs (occurs ch (Str.regexp_case_fold rs))

let alternatives =   ["B","(c|g|t)"; "D","(a|g|t)"; "H","(a|c|t)"; 
		      "K","(g|t)";   "M","(a|c)";   "N","(a|c|g|t)";
		      "R","(a|g)";   "S","(c|g)";   "V","(a|c|g)"; 
		      "W","(a|t)";   "Y","(c|t)";] 

let apply_alternatives ch (s,repl) = 
  Str.global_replace (Str.regexp_string_case_fold s) repl ch

let main () =
  let ch = eat stdin in
  let sz1 = String.length ch in
  let ch = clean ch in
  let sz2 = String.length ch in
  let () = List.iter (apply_pattern ch) patterns in
  let ch = List.fold_left apply_alternatives ch alternatives in
  let sz3 = String.length ch in
    Printf.printf "\n%d\n%d\n%d\n" sz1 sz2 sz3 ;;

main ()
  
