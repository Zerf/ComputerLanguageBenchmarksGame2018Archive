# The Great Computer Language Shootout
# http://shootout.alioth.debian.org/
# recursive test, by bearophile, Jan 24 2006

def Ack(x, y):
    if x == 0: return y+1
    if y == 0: return Ack(x-1, 1)
    return Ack(x-1, Ack(x, y-1))

def Fib(n):
    if n < 2: return 1
    return Fib(n-2) + Fib(n-1)

def FibFP(n):
    if n < 2.0: return 1.0
    return FibFP(n-2.0) + FibFP(n-1.0)

def Tak(x, y, z, s):
    if y < x: return Tak( Tak(x-s,y,z,s), Tak(y-s,z,x,s), Tak(z-s,x,y,s), s)
    return z

import psyco; psyco.full()
from sys import argv
n = int(argv[1]) - 1
print "Ack(3,%d):" % (n+1), Ack(3, n+1)
print "Fib(%.1f): %.1f" % (29.0+n, FibFP(29.0+n))
print "Tak(%d,%d,%d): %d" % (3*n, 2*n, n, Tak(3*n, 2*n, n, 1))
print "Fib(3):", Fib(3)
print "Tak(3.0,2.0,1.0):", Tak(3.0, 2.0, 1.0, 1.0)