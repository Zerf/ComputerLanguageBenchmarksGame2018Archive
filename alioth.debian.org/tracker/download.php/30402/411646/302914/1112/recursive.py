# The Great Computer Language Shootout
# http://shootout.alioth.debian.org/
# recursive test, by bearophile, Jan 22 2006

def Ack(x, y):
    if x == 0: return y+1
    if y == 0: return Ack(x-1, 1)
    return Ack(x-1, Ack(x, y-1))

def Fib(n, s):
    if n < 2: return s
    return Fib(n-2, s) + Fib(n-1, s)

def Tak(x, y, z):
    if y < x: return Tak( Tak(x-1.0,y,z), Tak(y-1.0,z,x), Tak(z-1.0,x,y) )
    return z

import psyco; psyco.full()
print "Ack(3,6):", Ack(3, 6)
print "Fib(24):", Fib(24, 1)
print "Tak(24,16,8):", Tak(24, 16, 8)
print "Fib(30):", Fib(30.0, 1.0)
print "Tak(24,16,8):", Tak(24.0, 16.0, 8.0)