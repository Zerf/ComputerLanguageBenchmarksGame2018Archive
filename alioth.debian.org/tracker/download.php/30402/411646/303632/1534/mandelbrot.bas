option explicit

#define iter 50 
 
dim shared w,h

sub calcmandel(x,y,byte_acc) 
  dim as double limit, zr, zi, cr, ci, tr, ti
  dim i
  
  limit=4.0
  zr = 0.0 : zi = 0.0
  tr=0.0 :ti=0.0 
  cr = (2.0*x/w - 1.5) : ci=(2.0*y/h - 1.0)
  for i = 0 to iter-1
    zi = 2.0*zr*zi + ci
    zr = tr - ti + cr
    tr=zr*zr : ti=zi*zi
    if tr+ti > limit then exit for
  next i
  byte_acc shl= 1
  if tr+ti<=limit then byte_acc or=1      
end sub

dim  x, y,  i, bit_num,byte_acc

  w = val(command$)
  if w < 1 then w = 300
  h = w

  print  "P4"
  print  str(w);h

  for y = 0 to h-1
      for x = 0 to w-1
          
         calcmandel(x,y,byte_acc)       
         bit_num += 1

          if bit_num = 8 then               'eob 
              print chr(byte_acc);
              byte_acc = 0
              bit_num = 0
          elseif x = w-1 then              'eol
              byte_acc = byte_acc shl (8 - w mod 8)
              print chr(byte_acc);
              byte_acc = 0
              bit_num = 0
          end if
      next x
  next y
end

