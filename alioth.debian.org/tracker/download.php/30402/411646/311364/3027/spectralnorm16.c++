// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org/
//
// Original C contributed by Sebastien Loisel
// Conversion to C++ by Jon Harrop
// OpenMP parallelize by The Anh Tran

// Fastest with this flag: -Os
// g++ -pipe -Os -fomit-frame-pointer -march=native -fopenmp -mfpmath=sse -msse2 ./spec.c++ -o ./spec.run

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <sched.h>
#include <omp.h>

using namespace std;
typedef double v2dt __attribute__((vector_size(16)));

union v2du
{
	v2dt		xmm;
	double		mem[2];
};

struct Param
{
	double* u;
	double* tmp;
	double* v;
	
	int N;
	int N2;
	int r_begin;
	int r_end;
};

static
double eval_A(int i, int j)
{
	// 1.0 / (i + j) * (i + j +1) / 2 + i + 1;
	// n * (n+1) is even number. Therefore, just (>> 1) for (/2)
	int d = (((i+j) * (i+j+1)) >> 1) + i+1;

	return 1.0 / d;
}

template <bool inc_i>
static
v2dt eval_A_sse(int i, int j)
{
	int d1 = (((i+j) * (i+j+1)) >> 1) + i+1;

	int d2;
	if (inc_i)
		d2 = (((i+1 +j) * (i+1 +j+1)) >> 1) + (i+1) +1;
	else
		d2 = (((i+ j+1) * (i+ j+1 +1)) >> 1) + i+1;

	v2dt const v1 = {1.0, 1.0};
	v2dt r = {d1, d2};
	return v1 / r; 
}



static
void eval_A_times_u (Param const &p)
{
	v2dt const * pU = (v2dt const*)p.u;
	v2du sum;
	
	for (int i = p.r_begin, ie = p.r_end; i < ie; i++)
	{
		sum.xmm ^= sum.xmm;

		int j;
		for (j = 0; j < p.N2; j++)
			sum.xmm += pU[j] * eval_A_sse <false> (i, j*2);

		p.tmp[i] = sum.mem[0] + sum.mem[1];

		// odd size array, *should be never*
		for (j = j*2; __builtin_expect(j < p.N, false); j++) 
			p.tmp[i] += eval_A(i, j) * p.u[j];
		
	}
}

static
void eval_At_times_u(Param const &p)
{
	v2dt const * pT = (v2dt const*)p.tmp;
	v2du sum;
	
	for (int i = p.r_begin, ie = p.r_end; i < ie; i++)
	{
		sum.xmm ^= sum.xmm;

		int j;
		for (j = 0; j < p.N2; j++)
			sum.xmm += pT[j] * eval_A_sse <true> (j*2, i);

		p.v[i] = sum.mem[0] + sum.mem[1];

		// odd size array, *should be never*
		for (j = j*2; __builtin_expect(j < p.N, false); j++) 
			p.v[i] += eval_A( j, i ) * p.tmp[j];
	}
}

static
void eval_AtA_times_u(Param const &p)
{
	eval_A_times_u( p );
	#pragma omp barrier

	eval_At_times_u( p );
	#pragma omp barrier
}

static
int GetThreadCount()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	for (int i = 0; i < 8; i++)
	{
		if (CPU_ISSET(i, &cs))
			count++;
	}
	return count;
}

static
double spectral_game(int N)
{
	__attribute__((aligned(64))) double u[N];
	__attribute__((aligned(64))) double tmp[N];
	__attribute__((aligned(64))) double v[N];

	double vBv	= 0.0;
	double vv	= 0.0;

	#pragma omp parallel default(shared) num_threads(GetThreadCount())
	{
		#pragma omp for schedule(static)
		for (int i = 0; i < N; i++)
			u[i] = 1.0;

		// this block will be executed by NUM_THREADS
		// variable declared in this block is private for each thread
		int threadid	= omp_get_thread_num();
		int threadcount	= omp_get_num_threads();
		int chunk		= N / threadcount;

		Param my_param;
		my_param.tmp	= tmp;
		my_param.N		= N;
		my_param.N2		= N/2;
		
		// calculate each thread's working range [range1 .. range2) => static schedule here
		my_param.r_begin	= threadid * chunk;
		my_param.r_end		= (threadid < (threadcount -1)) ? (my_param.r_begin + chunk) : N;

		for (int ite = 0; ite < 10; ite++)
		{
			my_param.u = u;
			my_param.v = v;
			eval_AtA_times_u(my_param);

			my_param.u = v;
			my_param.v = u;
			eval_AtA_times_u(my_param);
		}

		// multi thread adding
		#pragma omp for schedule(static) reduction( + : vBv, vv ) nowait
		for (int i = 0; i < N; i++)
		{
			vv	+= v[i] * v[i];
			vBv	+= u[i] * v[i];
		}
	} // end parallel region

	return sqrt( vBv/vv );
}


int main(int argc, char *argv[])
{
   int N = ((argc >= 2) ? atoi(argv[1]) : 2000);

   printf("%.9f\n", spectral_game(N));
   return 0;
}

