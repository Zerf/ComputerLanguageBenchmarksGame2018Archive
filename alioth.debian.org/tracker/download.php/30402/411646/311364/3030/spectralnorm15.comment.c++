// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org/
//
// Original C contributed by Sebastien Loisel
// Conversion to C++ by Jon Harrop
// OpenMP parallelize by The Anh Tran

// Fastest with this flag: -Os
// g++ -pipe -Os -fomit-frame-pointer -march=native -fopenmp -mfpmath=sse -msse2 ./spec.c++ -o ./spec.run

/*
	Single thread in C# reference:
	{
		for i = 0 to 10
		{
			eval(u, tmp);
			eval(tmp, v);
			eval(v, tmp);
			eval(tmp, u);
		}
	}
	
	eval(double source[], double destination[])
	{
		for i = 0 to desti.len
		{
			desti[i] = 0;
			for j = 0 to source.len
				desti[i] += source[j] * multiplier(i, j);
		}
	}
	
	Multithread: 
		1. spawn N threads. 
		2. Each thread fills its chunk in destination array. 
		3. chunk size = desti.len / Nthreads.

		eval(double source[], double destination[])
		{
			for i = chunk.begin to chunk.end
			{
				desti[i] = 0;
				for j = 0 to source.len
					desti[i] += source[j] * multiplier(i, j);
			}
		}
*/

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <sched.h>
#include <omp.h>

// Input: i, j
// Return: 1.0 / (i + j) * (i + j +1) / 2 + i + 1;
static
double eval_A(int i, int j)
{
	// n * (n+1) is even number. Therefore, just (>> 1) for (/2)
	int d = (   ( (i + j) * (i + 1 +j) ) >> 1   )    + i + 1;

	return 1.0 / d;
}

// Called by N threads. Each thread calculates its chunk [c_begin, c_end), on desti array.
static
void eval_A_times_u (double const source[], int src_len, double desti[], int c_begin, int c_end)
{
	for (int i = c_begin; i < c_end; i++)
	{
		double sum = 0.0;
		for (int j = 0; j < src_len; j++)
			sum += source[j] * eval_A( i, j );

		desti[i] = sum;
	}
}

static
void eval_At_times_u (double const source[], int src_len, double desti[], int c_begin, int c_end)
{
	for (int i = c_begin; i < c_end; i++)
	{
		double sum = 0.0;
		for (int j = 0; j < src_len; j++)
			sum += source[j] * eval_A(j, i);

		desti[i] = sum;
	}
}

// Called by N threads. They concurrently modify desti array, then that desti array 
// is the source array for next calculation phase => barrier needed to sync access
static
void eval_AtA_times_u(const double source[], double desti[], double tmp[], int src_len, int c_begin, int c_end)
{
	eval_A_times_u( source, src_len, tmp, c_begin, c_end );
	#pragma omp barrier

	eval_At_times_u( tmp, src_len, desti, c_begin, c_end );
	#pragma omp barrier
}

// Shootout uses affinity to emulate single core processor.
// This function detects appropriate cpu type.
static
int GetThreadCount()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	for (int i = 0; i < 8; i++)
	{
		if (CPU_ISSET(i, &cs))
			count++;
	}
	return count;
}

static
double spectral_game(int N)
{
	// 64 byte alignment = L2 cache line boundary
	__attribute__((aligned(64))) double u[N];
	__attribute__((aligned(64))) double tmp[N];
	__attribute__((aligned(64))) double v[N];

	double vBv	= 0.0;
	double vv	= 0.0;

	// spawn N threads, execute this block
	#pragma omp parallel default(shared) num_threads(GetThreadCount())
	{
		// Fill 1.0 to source array. Each thread fill its chunk.
		// Task division is OpenMP pragma intrinsic.
		#pragma omp for schedule(static)
		for (int i = 0; i < N; i++)
			u[i] = 1.0;

		{
			// this block will be executed by NUM_THREADS
			// variable declared in this block is private for each thread
			int threadid      = omp_get_thread_num();
			int threadcount   = omp_get_num_threads();
			int chunk   = N / threadcount;

			// calculate each thread's working range [range1 .. range2)
			// --> static schedule here (ie: manual task division)
			int myRange1 = threadid * chunk;
			int myRange2 = ( threadid < (threadcount -1) ) ? (myRange1 + chunk) : N;

			for (int ite = 0; ite < 10; ite++)
			{
				eval_AtA_times_u(u, v, tmp, N, myRange1, myRange2);
				eval_AtA_times_u(v, u, tmp, N, myRange1, myRange2);
			}
		}
		
		// multi thread adding
		#pragma omp for schedule(static) reduction( + : vBv, vv ) nowait
		for (int i = 0; i < N; i++)
		{
			vv	+= v[i] * v[i];
			vBv	+= u[i] * v[i];
		}
	} // end parallel region

	return sqrt( vBv/vv );
}


int main(int argc, char *argv[])
{
   int N = ((argc >= 2) ? atoi(argv[1]) : 2000);

   printf("%.9f\n", spectral_game(N));
   return 0;
}

