;;;
;;; The Great Computer Language Shootout
;;;   http://shootout.alioth.debian.org/
;;;
;;; resubmitted by Wade Humeniuk (Fix Stream Problem)
;;; Original contributed by Yannick Gingras <ygingras@ygingras.net>
;;;
;;; To compile
;;; sbcl --load mandelbrot.lisp --eval "(save-lisp-and-die \"mandelbrot.core\" :purify t :toplevel (lambda () (main) (quit)))"
;;; To run
;;; sbcl --noinform --core mandelbrot.core %A

(defun render (size stream)
  (declare (type fixnum size) (type sb-sys:fd-stream stream)
	   (optimize speed (safety 0) (debug 0)))
  (assert (and (<= 8 size 10000) (zerop (mod size 8))))
  (let* ((code 0)
         (mask 128)
         (zi 0.0d0)
         (zr 0.0d0)
         (tr 0.0d0)
         (base-imag -1.0d0)
         (base-real -1.5d0)
         (buffer (make-array (* size (ceiling size 8)) :element-type '(unsigned-byte 8)))
         (position -1))
    (declare (type (unsigned-byte 8) code mask)
             (type double-float zr zi tr base-real base-imag)
             (type fixnum position))
    (dotimes (y size)
      (setf base-imag (+ -1.0d0 (/ (* 2d0 y) size)))
      (dotimes (x size)
	(setf base-real (+ -1.5d0 (/ (* 2d0 x) size))
	      zr 0.0d0
	      zi 0.0d0)
        (when (not (dotimes (n 51)
                     (when (< 4.0d0 (+ (* zr zr) (* zi zi)))
                       (return t))
                     (setf tr (+ (* zr zr) (- (* zi zi)) base-real)
			   zi (+ (* 2.0d0 zr zi) base-imag)
			   zr tr)))
          (setf code (logior mask code)))
        (setf mask (ash mask -1))
        (when (zerop mask)
          (setf mask 128
		(aref buffer (incf position)) code
		code 0)))
      (when (/= 128 mask)
	(setf (aref buffer (incf position)) code
	      code 0
	      mask 128)))
    (write-sequence buffer stream)))

(defun main ()
  (declare (optimize (speed 2) (safety 2)))
  (let* ((args sb-ext:*posix-argv*)
         (n (parse-integer (car (last args))))
	 (bistream (sb-sys:make-fd-stream (sb-sys:fd-stream-fd sb-sys:*stdout*)
					  :element-type '(unsigned-byte 8)
					  :buffering :none
					  :output t :input nil)))
    (format *standard-output* "P4~%~d ~d~%" n n)
    (finish-output *standard-output*)
    (render n bistream)))
