/* The Computer Language Shootout
  http://shootout.alioth.debian.org/

  contributed by Przemyslaw Szymanski
 */
#include <algorithm>
#include <iostream>
#include <iterator>
#include <cstdlib>
#include <cstring>

void dump_seq(const int* seq, int size)
{
  std::copy(seq, seq + size, std::ostream_iterator<int>(std::cout));
  std::cout << std::endl;
}

int main(int argc, char* argv[])
{
  int size       = (argc == 2 ? std::atoi(argv[1]) : 7),
      array_size = size * sizeof(int),
      tmp        = size;
  std::auto_ptr<int> auto_seq(new int[size]),
                     auto_work(new int[size]);
  int* seq  = auto_seq.get();
  int* work = auto_work.get();

  while (tmp--)
    seq[tmp] = tmp + 1;

  int flips = 0, max_flips = 0, count30 = 30;
  do {
    if (count30) dump_seq(seq, size), count30--;

    std::memcpy(work, seq, array_size);
    for (flips = 0; work[0] != 1; flips++)
      std::reverse(work, work + work[0]);
      
    if (flips > max_flips) max_flips = flips;
  } while (std::next_permutation(seq, seq + size));
  
  std::cout << "Pfannkuchen(" << size << ") = " << max_flips << std::endl;
  return 0;
}

