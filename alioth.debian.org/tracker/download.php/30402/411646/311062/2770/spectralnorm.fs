﻿// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org/
//
// Based on ocaml version by Sebastien Loisel & Troestler Christophe
// Contributed by Robert Pickering
// Modified by Valentin Kraevskiy
#light

open Array

let eval_A i j = 1.0 / float((i + j) * (i + j + 1) / 2 + i + 1)

let eval_A_times_u (u : float []) v =
    let n = length v - 1 in
    for i in 0 .. n do
        v.[i] <- 0.0
        for j in 0 .. n do
            v.[i] <- v.[i] + eval_A i j * u.[j]

let eval_At_times_u (u : float []) v =
    let n = length v - 1 in
    for i in 0 .. n do
        v.[i] <- 0.0
        for j in 0 .. n do
            v.[i] <- v.[i] + eval_A j i * u.[j]

let eval_AtA_times_u u v =
    let w = create (length u) 0.0
    eval_A_times_u u w
    eval_At_times_u w v

let n = try int <| get Sys.argv 1 with _ -> 5500

let u = Array.create n 1.0
let v = Array.create n 0.0
for i in 0 .. 9 do
    eval_AtA_times_u u v
    eval_AtA_times_u v u

let mutable vv = 0.0
let mutable vBv = 0.0
for i in 0 .. n - 1 do
    vv <- vv + v.[i] * v.[i]
    vBv <- vBv + u.[i] * v.[i]

printf "%0.9f\n" <| sqrt (vBv / vv)
  
  