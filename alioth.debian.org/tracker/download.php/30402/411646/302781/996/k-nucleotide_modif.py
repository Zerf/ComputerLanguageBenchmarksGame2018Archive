from sys import stdin

class k_nucleotide:
    def __init__(self, sequence):
        self.seq = sequence
        self.seq_length = len(sequence)
        self.frequencies = {}


    def k_frequency(self, frame):
        n = self.seq_length + 1 - frame
        self_seq = self.seq
        self_frequencies = self.frequencies
        for i in xrange(n):
            k_nucleo = self_seq[i:i + frame]
            if k_nucleo in self_frequencies:
                self_frequencies[k_nucleo] += 1
            else:
                self_frequencies[k_nucleo] = 1


    def generate_frequencies(self, length):
        self.frequencies.clear()
        self.k_frequency(length)


    def write_frequencies(self, nucleotide_length):
        self.generate_frequencies(nucleotide_length)

        fcp = sorted(self.frequencies.items(), reverse=True, key=lambda (seq,freq): (freq,seq))

        sum1 = 0
        for seq, freq in fcp:
            sum1 += freq

        for seq, freq in fcp:
            print "%s %.3f" % (seq, (freq * 100.0) / sum1)
        print


    def write_count(self, fragment):
        self.generate_frequencies(len(fragment))
        count1 = self.frequencies.get(fragment, 0)
        print "%d\t%s" % (count1, fragment)


def main():
    for line in stdin:
        if line[0:3] == ">TH":
            break

    seq = []
    for line in stdin:
        if line[0] in ">;":
            break
        seq.append( line[:-1] )
    sequence = ("".join(seq)).upper()

    kn = k_nucleotide(sequence)

    kn.write_frequencies(1)
    kn.write_frequencies(2)

    kn.write_count("GGT")
    kn.write_count("GGTA")
    kn.write_count("GGTATT")
    kn.write_count("GGTATTTTAATT")
    kn.write_count("GGTATTTTAATTTATAGT")

import psyco; psyco.bind(k_nucleotide.k_frequency)
main()