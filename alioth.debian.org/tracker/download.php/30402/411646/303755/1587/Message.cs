/*
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * Contributed by: Alexandre Alapetite http://alexandre.alapetite.net/
 * Date: 2006-08-13
 *
 * Specifications:
 *
 * Each program should create, keep alive, and send integer messages between N explicitly-linked threads.
 * Programs may use kernel threads, lightweight threads, cooperative threads...
 *
 * Each program should
 ** create 500 threads - each thread should
 *** hold and use a reference to the next thread
 *** take, and increment, an integer message
 *** put the incremented message on the next thread
 ** N times
 *** put the integer message 0 on the first thread
 *** add the message taken and incremented by the last thread to a sum
 ** print the sum of incremented integer messages - a count of takes
 *
 ** diff program output N = 10 with this output file (5000) to check your program is correct before contributing.
 *
 * Similar benchmarks are described in Performance Measurements of Threads in Java and Processes in Erlang, 1998;
 * and A Benchmark Test for BCPL Style Coroutines, 2004.
 * For some language implementations increasing the number of threads quickly results in Death by Concurrency.
 */

//#define NET_2_0 //If generics are implemented (C# 2.0, from Microsoft .NET 2.0, Mono gmcs)

using System;
#if NET_2_0
using System.Collections.Generic;
#else
using System.Collections;
#endif
using System.Threading;

namespace cheap_concurrency
{
	public sealed class MyMessage
	{
		private const int numberOfThreads = 500;
		internal static int numberOfMessagesToSend;

		public static void Main(string[] args)
		{
			numberOfMessagesToSend = int.Parse(args[0]);

			MessageThread chain = null;
			for (int i = numberOfThreads; i > 0; i--)
			{
				chain = new MessageThread(chain);
				new Thread(new ThreadStart(chain.run)).Start();
			}

			for (int i = 0; i < numberOfMessagesToSend; i++)
				chain.Enqueue(0);
		}
	}

	internal sealed class MessageThread
	{
		private MessageThread nextThread;
		#if NET_2_0
		private Queue<int> list = new Queue<int>();
		#else
		private Queue list = new Queue();
		#endif
		private int numberOfMessagesToSend;

		internal MessageThread(MessageThread nextThread)
		{
			this.nextThread = nextThread;
			this.numberOfMessagesToSend = MyMessage.numberOfMessagesToSend;
		}

		internal void run()
		{
			if (nextThread == null)
			{
				int sum = 0;
				while (numberOfMessagesToSend > 0)
				{
					#if NET_2_0
					lock (list)
					#else
					lock (list.SyncRoot)
					#endif
						while (list.Count > 0)
						{
							#if NET_2_0
							sum += list.Dequeue();
							#else
							sum += (int)list.Dequeue();
							#endif
							numberOfMessagesToSend--;
						}
					Thread.Sleep(0);
				}
				Console.WriteLine(sum);
			}
			else
			{
				while (numberOfMessagesToSend > 0)
				{
					#if NET_2_0
					lock (list)
					#else
					lock (list.SyncRoot)
					#endif
						while (list.Count > 0)
						{
							nextThread.Enqueue((int)list.Dequeue());
							numberOfMessagesToSend--;
						}
					Thread.Sleep(0);
				}
			}
		}

		internal void Enqueue(int mess)
		{
			#if NET_2_0
			lock (list)
			#else
			lock (list.SyncRoot)
			#endif
				list.Enqueue(mess + 1);
		}
	}
}
