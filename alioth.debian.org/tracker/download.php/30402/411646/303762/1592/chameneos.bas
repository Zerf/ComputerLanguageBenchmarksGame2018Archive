option explicit
option byval
option escape
#include "crt.bi"
option nokeyword color

enum color
 blue
 red
 yellow
 green
 faded
end enum

dim shared nmeetings,report
dim shared as color color1
dim shared cond1,cond12
dim shared meeting
'
'--------------------------------------------------------
Function complementaryColor (c1 as color, c2 as color) as color
  if (c2 = Faded) then return Faded
  if (c1 = c2) then return c1
  select case c1
    case Blue:return iif (c2 = Red , Yellow , Red)
    case Red:return iif (c2 = Blue , Yellow , Blue)
    case Yellow:return iif(c2 = Blue, Red , Blue)
    case else:return c1
   end select
end function
'
'---------------------------------------------------------
sub chameneos(index)
dim count
dim as color clr
clr=index
do
select case meeting
 case 0
  if nmeetings=0 then
    clr=faded
    report+=count
    exit sub
  else
    meeting=1
    condwait cond1
    'someone else is at the meeting point
    clr=complementarycolor (clr,color1)
    nmeetings-=1
    count+=1
    color1=0
    meeting=0
    condbroadcast cond12
  end if
case 1
  meeting=2
  color1=clr
  count+=1
  condsignal cond1
case else
  condwait cond12
end select
loop
end sub
'
'-----------------------------------------------------------
dim i
nmeetings =valint(command):if nmeetings=0 then nmeetings=100

cond1=condcreate
cond12=condcreate

dim thechameneos(3)
for i=0 to 3
   thechameneos(i)=threadcreate(@chameneos,i)
   if thechameneos(i)=0 then print "Error Crating thread "& i:end
next

for i=0 to 3
threadwait(thechameneos(i))
next

printf ("%d\n",report)
conddestroy (cond1)
conddestroy(cond12)