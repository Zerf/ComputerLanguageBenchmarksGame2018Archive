!
! The Great Computer Language Shootout
!   http://shootout.alioth.debian.org/
!
!
!     /* The Computer Language Benchmarks Game
!        http://shootout.alioth.debian.org/
! 
!        contributed by Simon Geard, translated from  Mark C. Williams nbody.java
!        modified by Knut Erik Teigen
!     */


program nbody

  implicit none
  integer  num, k
  character(len=8) argv
  real*8, parameter :: dt = 0.01d0
  real*8, parameter ::  PI = 3.141592653589793d0
  real*8, parameter ::  SOLAR_MASS = 4 * PI * PI
  real*8, parameter ::  DAYS_PER_YEAR = 365.24d0
  real*8 :: e
  type body
     real*8, dimension(3) :: x,v
     real*8 :: mass
  end type body
  type(body), parameter :: jupiter = body( &
     (/4.84143144246472090d0, &
      -1.16032004402742839d0, &
      -1.03622044471123109d-01/), &
     (/1.66007664274403694d-03, &
       7.69901118419740425d-03, &
      -6.90460016972063023d-05/)*DAYS_PER_YEAR, &
       9.54791938424326609d-04 * SOLAR_MASS)

  type(body), parameter :: saturn = body( &
     (/8.34336671824457987d+00, &
       4.12479856412430479d+00, &
      -4.03523417114321381d-01/), &
    (/-2.76742510726862411d-03, &
       4.99852801234917238d-03, &
       2.30417297573763929d-05/)*DAYS_PER_YEAR, &
       2.85885980666130812d-04 * SOLAR_MASS)

  type(body), parameter :: uranus = body( &
    (/1.28943695621391310d+01, &
     -1.51111514016986312d+01, &
     -2.23307578892655734d-01/), &
    (/2.96460137564761618d-03, &
      2.37847173959480950d-03, &
     -2.96589568540237556d-05/)*DAYS_PER_YEAR, &
      4.36624404335156298d-05 * SOLAR_MASS )

  type(body), parameter :: neptune = body( &
     (/1.53796971148509165d+01, &
      -2.59193146099879641d+01, &
       1.79258772950371181d-01/), &
     (/2.68067772490389322d-03, &
       1.62824170038242295d-03, &
      -9.51592254519715870d-05/)*DAYS_PER_YEAR, &
       5.15138902046611451d-05 * SOLAR_MASS)

  type(body), parameter :: sun = body((/0.0d0, 0.0d0, 0.0d0/), &
    (/0.0d0, 0.0d0, 0.0d0/), SOLAR_MASS)

  integer, parameter :: NBODIES = 5

  type(body), dimension(NBODIES) :: bodies
  bodies = (/ sun, jupiter, saturn, uranus, neptune /)

  call getarg(1,argv)
  read(argv,*) num

  call offsetMomentum(1,bodies)
  e = energy(bodies)
  write(*,'(f12.9)') e

  do k=1,num
    call advance(dt,bodies)
  end do

  e = energy(bodies)
  write(*,'(f12.9)') e

contains

  subroutine offsetMomentum(k, bodies)
    integer, intent(in) :: k
    type(body), dimension(:), intent(inout) :: bodies
    integer i
    real*8, dimension(3) :: p = 0

    do i=1,NBODIES
      p = p + bodies(i)%v*bodies(i)%mass
    end do
    bodies(k)%v = -p/SOLAR_MASS
  end subroutine offsetMomentum

  subroutine advance(dt, bodies)
  real*8, intent(in) :: dt
  type(body), dimension(:), intent(inout) :: bodies
  integer i,j
  real*8 distance, mag
  real*8, dimension(3) :: d

    do i=1,NBODIES
      do j=i+1,NBODIES
        d = bodies(i)%x - bodies(j)%x
   
        distance = sqrt(dot_product(d,d))
        mag = dt / (distance * distance * distance)
   
        bodies(i)%v = bodies(i)%v - d * bodies(j)%mass * mag
        bodies(j)%v = bodies(j)%v + d * bodies(i)%mass * mag
      end do
    end do
   
    do i=1,NBODIES
      bodies(i)%x = bodies(i)%x + dt * bodies(i)%v
    end do
  
  end subroutine advance

  real*8 function energy(bodies)
    type(body), dimension(:), intent(in) :: bodies
    real*8 distance
    real*8, dimension(3) :: d
    integer i, j

    energy = 0.0d0
    do i=1,NBODIES
      energy = energy + 0.5d0 * &
        bodies(i)%mass * dot_product(bodies(i)%v,bodies(i)%v)

      do j=i+1,NBODIES
         d = bodies(i)%x - bodies(j)%x
         distance = sqrt(dot_product(d,d))
         energy = energy - (bodies(i)%mass * bodies(j)%mass) / distance;
      end do
    end do
  end function energy

end program nbody
