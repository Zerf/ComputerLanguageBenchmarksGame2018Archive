/* The Computer Language Shootout
 * http://shootout.alioth.debian.org/
 * contributed by anon
 * modified by - Yakov Kravets
 * compile with -Os -lpthread
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <pthread.h>    // -lpthread

#define THREADS 500

#define STACKSZ (100*1024) // 100K
#define SWL_SLEEP (1000000/1000)

struct work_data {
        pthread_t thread;
        pthread_mutex_t mutex;
        pthread_cond_t condition;
        int message;
        struct work_data *next;
};

void *
work(void *ptr)
{
        struct work_data *this = (struct work_data *)ptr;
        struct work_data *next = this->next;
        if (this == NULL || next == NULL)
                return NULL;

        for (;;) {
                pthread_mutex_lock(&(this->mutex));
                /* Wait for a signal from the links thread */
                pthread_cond_wait(&(this->condition),&(this->mutex));

                /* Receive and Pass the message */
                pthread_mutex_lock(&(next->mutex));
                next->message = this->message + 1;
                pthread_mutex_unlock(&(next->mutex));

                /* Signal linked thread */
                pthread_cond_signal(&(next->condition));

                pthread_mutex_unlock(&(this->mutex));
        }
        return NULL;
}

int
main(int argc, char *argv[])
{
        struct work_data *wdata;
        unsigned int times;

        assert(argc == 2 && sscanf(argv[1], "%u", &times) == 1);

        int sum = 0;
        pthread_attr_t attr;
        int i, j;

        wdata = malloc(sizeof(struct work_data) * THREADS);
        assert(wdata != NULL);
        assert(pthread_attr_init(&attr) == 0);
        assert(pthread_attr_setstacksize(&attr, STACKSZ) == 0);

        /* Initialize threads data */
        for (i = 0; i < THREADS; i++) {
                pthread_mutex_init(&wdata[i].mutex, NULL);
                pthread_cond_init(&wdata[i].condition, NULL);
                wdata[i].message = 0;
                wdata[i].next = &wdata[i + 1];
                pthread_create(&wdata[i].thread, &attr, work, &wdata[i]);
        }
        /* Initialize main thread */
        pthread_mutex_init(&wdata[THREADS].mutex, NULL);
        pthread_cond_init(&wdata[THREADS].condition, NULL);
        wdata[THREADS].message = 0;

        for (j = 0; j < times; j++) {
                /* Pass the message */
                pthread_mutex_lock(&wdata[0].mutex);
                wdata[0].message = 0;
                pthread_mutex_unlock(&wdata[0].mutex);

                /* Signal first thread */
                pthread_cond_signal(&wdata[0].condition);

                /* Wait for a signal from the last thread */
                pthread_mutex_lock(&wdata[THREADS].mutex);
                pthread_cond_wait(&wdata[THREADS].condition,&wdata[THREADS].mutex);
                /* Read the message */
                sum += wdata[THREADS].message;
                pthread_mutex_unlock(&wdata[THREADS].mutex);
        }
        printf("%d\n", sum);

        exit(0);
}