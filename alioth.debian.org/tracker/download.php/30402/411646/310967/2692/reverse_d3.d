// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org
// Low-level style version adapted from Bob W version by bearophile

import std.c.stdio, std.c.stdlib, std.c.string;

int errex(char* s, int n) {                    // error message+value, return 1
    fprintf(stderr,"\n*** Error: %s [%d]!\n", s, n);
    return 1;
}

void main() {
    const JBFSIZE = 82;                        // line input buffer size
    const QBFSIZE = 5200;                      // output buffer initial size

    char* pj;                                  // buffer pointers: inp
    char* pq;                                  // buffer pointers: out
    char* pr;                                  // buffer pointers: /out
    auto jjj = cast(char*)malloc(JBFSIZE);     // allocate input line buffer
    auto qqq = cast(char*)malloc(QBFSIZE);     // output buffer (dyn. size)
    char* pqstop = qqq + QBFSIZE;              // end-of-buffer pointer

    byte[256] xtab;                            // char conversion table
    foreach (i, c; "ACBDGHKMNSRUTWVYacbdghkmnsrutwvy")
        xtab[c] = "TGVHCDMKNSYAAWBRTGVHCDMKNSYAAWBR"[i];

    if (!jjj || !qqq)
        return errex("Buffer allocation", !jjj + !qqq);

    pj = fgets(jjj, JBFSIZE, stdin);           // fetch 1st line
    if (!pj)
        return errex("No input data", 0);
    if (*jjj != '>')
        return errex("1st char not '>'", 0);

    while (pj) {                               // MAIN LOOP: process data
        fputs(jjj, stdout);                    // output ID line

        for (pq = qqq + 1, pr = pqstop; ; pq++) { // LOOP: fill output buffer
            pj = fgets(jjj, JBFSIZE, stdin);   // get line from stdin
            if (!pj || (*jjj == '>'))
                break;                         // EOF or new ID line

            if (pr <= (pq + 61)) {             // need to resize buffer
                char *newstop = pqstop + 12777888;
                char *newptr = cast(char*)realloc(qqq, newstop-qqq);
                if (!newptr)
                    return errex("Out of memory", 0);

                if (newptr != qqq) {           // new base: adj. pointers
                    size_t x = newptr - qqq;   // offset for pointer update
                    pq += x;
                    pr += x;
                    qqq += x;
                    newstop += x;
                    pqstop += x;
                }

                pr = cast(char*)memmove(newstop-(pqstop-pr), pr, pqstop-pr);
                pqstop = newstop;              // buffer resize complete
            }

            while (*pj) {                      // LOOP: conv. & revert line
                byte c = xtab[*pj++];
                if (c > 0)                     // conversion valid
                    *(--pr) = c;
            }
        }

        for (pq = qqq; pr<pqstop; ) {          // LOOP: format output
            size_t x = (pqstop - pr) < 60 ? pqstop-pr : 60;
            memmove(pq, pr, x);                // move line to free space
            pr +=x;                            // adjust pointers, add LF
            pq += x;
            *(pq++) = 0xA;
        }

        fwrite(qqq, 1, pq-qqq, stdout);        // output converted data
    }
}