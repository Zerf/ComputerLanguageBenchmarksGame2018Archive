;; The Computer Language Benchmarks Game
;;  http://shootout.alioth.debian.org/
;; contributed by Graham Fawcett <graham.fawcett@gmail.com>

;; compilation: csc -O2 ring.scm mini-mailbox.scm -o ring

;; SPLITFILE=ring.scm

;; This program uses a stripped-down version of the mailbox egg. The
;; full mailbox egg is available for download from the Chicken Eggs
;; repository.

(declare
 (uses srfi-1 srfi-18 mini-mailbox)
 (no-procedure-checks)
 (fixnum-arithmetic)
 (bound-to-procedure thread-act))

(define N (string->number (car (command-line-arguments))))

(define thread-act
  (lambda ()
    (let loop ((mbs (thread-specific (current-thread))))
      (let ((v (mailbox-receive! (cdr mbs))))
        (cond ((< v N)
               (mailbox-send! (car mbs) (add1 v))
               (loop mbs))
              (#t (print (thread-name (current-thread)))
                  (exit 0)))))))

(let* ((first-mb (make-mailbox "one"))
       (threads
        (let loop ((n 503) (acc '()) (prev-mb first-mb))
          (if (zero? n)
              (begin (set-cdr! (thread-specific (car acc)) first-mb)
                     acc)
              (let ((t (make-thread thread-act (->string n)))
                    (new-mb (make-mailbox (->string n))))
                (thread-specific-set! t (cons prev-mb new-mb))
                (loop (sub1 n) (cons t acc) new-mb))))))
  (for-each thread-start! threads)
  (mailbox-send! first-mb 0)
  (thread-join! (car threads)))


;; SPLITFILE=mini-mailbox.scm

;; This is a stripped-down version of the mailbox egg. The full
;; mailbox egg is available for download from the Chicken Eggs
;; repository. Mailbox's license appears below:

; Copyright (c) 2000-2003, Felix L. Winkelmann
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions
; are met:
;
;   Redistributions of source code must retain the above copyright
;   notice, this list of conditions and the following disclaimer.
;   Redistributions in binary form must reproduce the above copyright
;   notice, this list of conditions and the following disclaimer in
;   the documentation and/or other materials provided with the
;   distribution.  Neither the name of the author nor the names of its
;   contributors may be used to endorse or promote products derived
;   from this software without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
; COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
; OF THE POSSIBILITY OF SUCH DAMAGE.

(declare
 (unit mini-mailbox)
  (uses srfi-18)
  (disable-interrupts)
  (usual-integrations)
  (no-procedure-checks))

(define-inline (undefined? x)
  (##core#inline "C_undefinedp" x))

(define-record mailbox queue thread-queue name)

(define make-mailbox
  (let ((mm make-mailbox))
    (lambda (n)
      (mm (cons #f #f) '() n))))

(define (%mailbox-ready! ch)
  ; Ready oldest waiting thread
  (let ([ts (mailbox-thread-queue ch)])
    (unless (null? ts)
      (let ([thread (car ts)])
        ;; Pop thread queue
        (mailbox-thread-queue-set! ch (cdr ts))
        ;; Ready the thread based on wait mode
        (thread-resume! thread)))))

(define (%mailbox-wait! caller ch)
  ;; Push current thread on mailbox waiting queue
  (mailbox-thread-queue-set!
   ch
   (append (mailbox-thread-queue ch)
           (list ##sys#current-thread)))
  (thread-suspend! ##sys#current-thread))

(define (%mailbox-maybe-wait! caller ch)
  (unless (car (mailbox-queue ch))
    (apply %mailbox-wait! caller ch '())))

(define (mailbox-send! ch x)
  (##sys#check-structure ch 'mailbox 'mailbox-send!)
  (set-car! (mailbox-queue ch) x)
  (%mailbox-ready! ch))

(define (mailbox-receive! ch)
  (##sys#check-structure ch 'mailbox 'mailbox-receive!)
  (let ([res (apply %mailbox-maybe-wait!
                    'mailbox-receive! ch '())])
    (if (undefined? res)
        (let ((v (car (mailbox-queue ch))))
          (set-car! (mailbox-queue ch) #f)
          v)
        res)))
