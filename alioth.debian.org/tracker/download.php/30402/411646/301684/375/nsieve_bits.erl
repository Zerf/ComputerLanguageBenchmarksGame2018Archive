%% The Great Computer Language Shootout
%% http://shootout.alioth.debian.org/
%% contributed by Alexey Shchepin <alexey@sevcom.net>

-module(nsieve_bits).
-export([main/0, main/1]).

main() -> main(['1']).
main([Arg]) ->
    N = list_to_integer(atom_to_list(Arg)),
    nsieve((1 bsl N) * 10000),
    nsieve((1 bsl (N - 1)) * 10000),
    nsieve((1 bsl (N - 2)) * 10000),
    halt(0).

nsieve(M) ->
    nsieve(2, M, 0, 0).

nsieve(I, M, Sieve, Counter) when I =< M ->
    if
	Sieve band (1 bsl I) == 0 ->
	    NewSieve = sieve_num(2*I, I, M, Sieve),
	    nsieve(I+1, M, NewSieve, Counter + 1);
	true ->
	    nsieve(I+1, M, Sieve, Counter)
    end;
nsieve(_I, M, _S, Counter) ->
    io:format("Primes up to ~8w~8w\n", [M, Counter]),
    Counter.

sieve_num(J, I, M, Sieve) when J =< M ->
    sieve_num(J + I, I, M, Sieve bor (1 bsl J));
sieve_num(_J, _I, _M, Sieve) ->
    Sieve.


