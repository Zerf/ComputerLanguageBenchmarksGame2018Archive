/*	The Computer Language Benchmarks Game
	http://shootout.alioth.debian.org/

	contributed by Paolo Bonzini
	further optimized by Jason Garrett-Glaser
	OpenMP by The Anh Tran
*/

#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <sched.h>

// need "-fopenmp" flag when compile
#include <omp.h>

int GetThreadCount()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	for (int i = 0; i < 8; i++)
	{
		if (CPU_ISSET(i, &cs))
			count++;
	}
	return count;
}

void mandelbrot(int N, char* data, int width_bytes, int* nbyte_each_line)
{
	// counter of each line, how many columns are processed
	int jobs[N];
	memset(jobs, 0, sizeof(jobs));

	#pragma omp parallel default(shared) num_threads(GetThreadCount())
	{
		const double inverse_n = 2.0 / N;

		char* pdata = data; 
		for (int y = 0; y < N; ++y, pdata += width_bytes) // for each line
		{
			// count how many bytes (of current line), which this thread will output
			int byte_count = 0;	

			double Civ = y * inverse_n - 1.0;

			int* current_jobs = jobs + y;
			int x;
			
			// fetch 256 not-yet-process columns
			// 256/8 = 32 bytes output. Fit cache line size.
			while ((x = __sync_fetch_and_add(current_jobs, 256)) < N)
			{
				int limit = std::min(x +256, N);
				
				int bit_num = 0;
				int byte_acc = 0;

				for (; x < limit; x++)
				{
					double Crv = x * inverse_n - 1.5;

					double Zrv	= Crv;
					double Ziv	= Civ;
					double Trv	= Crv * Crv;
					double Tiv	= Civ * Civ;

					int i = 49;
					do
					{
						Ziv = (Zrv*Ziv) + (Zrv*Ziv) + Civ;
						Zrv = Trv - Tiv + Crv;
				
						Trv = Zrv * Zrv;
						Tiv = Ziv * Ziv;
					}	while ( ((Trv + Tiv) <= 4.0) && (--i > 0) );
			
					byte_acc <<= 1;
					byte_acc |= (i == 0);

					if ( __builtin_expect((++bit_num == 8), false) )
					{
						pdata[ x >> 3 ] = (char)byte_acc;
						byte_count++;
						bit_num = byte_acc = 0;
					}
				} // end foreach (column)

				if ( __builtin_expect((bit_num != 0), false) ) // write left over bits
				{
					byte_acc <<= (8 - (N & 7));
					pdata[ x >> 3 ] = (char)byte_acc;
					byte_count++;
				}
			}
			
			#pragma omp atomic
			nbyte_each_line[y] += byte_count;
		} // end foreach (line)
	} // end parallel region
}

int main (int argc, char **argv)
{
	int N = 200;

	if  (argc == 2)
		N = atoi( argv[1] );
	printf("P4\n%d %d\n", N, N);

	int width_bytes = N/8 +1;

	char *data = (char*)malloc( width_bytes * N * sizeof(char) );
	int* nbyte_each_line = (int*)calloc( N, sizeof(int) );

	mandelbrot(N, data, width_bytes, nbyte_each_line);

	char* pdata = data;
	for (int y = 0; y < N; y++)
	{
		fwrite( pdata, nbyte_each_line[y], 1, stdout);
		pdata += width_bytes;
	}

	free(data);
	free(nbyte_each_line);

	return 0;
}

