(*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Christophe TROESTLER
 * Enhanced by Christian Szegedy, Yaron Minsky.
 * Optimized by Mauricio Fernandez.
 *
 *)

let niter = 50
let limit = 2.

type complex = { mutable r: float; mutable i: float }

let () =
  let w = int_of_string(Array.get Sys.argv 1) in
  let h = w in
  let fw = float w
  and fh = float h in
  Printf.printf "P4\n%i %i\n" w h;
  let z = {r=0.; i=0.;} in
  let limit2 = limit *. limit in
  let byte = ref 0 in
  for y = 0 to h - 1 do
    let ci = 2. *. float y /. fh -. 1. in
      for x = 0 to w - 1 do
        let cr = 2. *. float x /. fw -. 1.5 in
          z.r <- 0.; z.i <- 0.;
          let bit = ref 1 and i = ref niter in
            while !i > 0 do
              let zr = z.r and zi = z.i in
              let zi = 2. *. zr *. zi +. ci and zr = zr *. zr -. zi *. zi +. cr in
                z.r <- zr;
                z.i <- zi;
                decr i;
                if zr *. zr +. zi *. zi > limit2 then begin
                  bit := 0;
                  i := 0;
                end;
            done;
            byte := (!byte lsl 1) lor !bit;
            if x land 0x7 = 7 then output_byte stdout !byte;
      done;
    if w mod 8 != 0 then (* the row doesnt divide evenly by 8*)
      output_byte stdout (!byte lsl (8-w mod 8)); (* output last few bits *)
    byte := 0;
  done
