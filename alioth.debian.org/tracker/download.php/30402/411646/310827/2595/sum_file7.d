// The Computer Language Benchmarks Game
//   http://shootout.alioth.debian.org/
// Alternative version by bearophile

import std.c.stdio: printf, stdin, fread;

void main() {
    int tot, nread, current, sign = 1;
    char[1 << 12] buffer;

    while ((nread = fread(&buffer, 1, buffer.length, stdin)) > 0) {
        char* c = &buffer[0];
        char* end = &buffer[0] + nread;

        while (c < end) {
            if (*c >= '0' && *c <= '9')
                current = current * 10 + *c++ - '0';
            else if (*c == '-') {
                sign = -1;
                c++;
            } else {
                tot += current * sign;
                current = 0;
                sign = 1;
                c++;
            }
        }
    }

    printf("%d\n", tot);
}