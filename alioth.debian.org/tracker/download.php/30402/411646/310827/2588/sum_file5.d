// The Computer Language Benchmarks Game
//   http://shootout.alioth.debian.org/
// Alternative version by bearophile

import std.c.stdio: printf, stdin, fread;

void main() {
    int tot, nread, n, sign = 1;
    char[1 << 12] buffer;

    while ((nread = fread(buffer.ptr, 1, buffer.length, stdin)) > 0) {
        int subtot;

        foreach (c; buffer[0 .. nread]) {
            if (c >= '0' && c <= '9')
                n = n * 10 + c - '0';
            else if (c == '-')
                sign = -1;
            else {
                subtot += (n * sign);
                n = 0;
                sign = 1;
            }
        }

        tot += subtot;
    }

    printf("%d\n", tot);
}