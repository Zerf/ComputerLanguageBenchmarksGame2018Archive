(* The Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Josh Goldfoot
*)

program pfannkuchen;
uses SysUtils;

type
    TIntegerArray = Array[0..99] of Integer;

function fannkuch(n : integer): integer;
var
   perm, perm1, count: TIntegerArray;
   print30, m, r, i, k, temp, mostFlips, flips, perm0: integer;
   keepLooping0, keepLooping1 : boolean;
begin
   print30 := 0;
   mostFlips := 0;
   m := n - 1;
   for i := 0 to m do
       perm1[i] := i;
   r := n;
   keepLooping0 := true;
   repeat
      if print30 < 30 then
      begin
         for i := 0 to m do
            write(perm1[i] + 1);
         writeln;
         print30 := print30 + 1;
      end;
      while r <> 1 do
      begin
         count[r-1] := r;
         r := r - 1;
      end;
      if not ((perm1[0]=0) or (perm1[m]=m)) then
      begin
         for i := 0 to m do
            perm[i] := perm1[i];
         flips := 0;
         repeat
            k := perm[0];
            if k <> 0 then
            begin
               for i := 0 to ((k+1) div 2) - 1 do
               begin
                  temp := perm[i];
                  perm[i] := perm[k-i];
                  perm[k-i] := temp;
               end;
               flips := flips + 1;
            end;
         until k = 0;
         if flips > mostFlips then
            mostFlips := flips;
      end;
      keepLooping1 := r <> n;
      while keepLooping1 do
      begin
         if r = n then
         begin
            fannkuch := mostFlips;
            keepLooping1 := False;
            keepLooping0 := False;
         end
         else
         begin
            perm0 := perm1[0];
            i := 0;
            while i < r do
            begin
               k := i + 1;
               perm1[i] := perm1[k];
               i := k;
            end;
            perm1[r] := perm0;
            count[r] := count[r] - 1;
            if count[r] > 0 then
               keepLooping1 := False
            else
               r := r + 1;
         end;
      end;
   until not keepLooping0;
end;

var
   n, Code, answer : integer;

begin
   if paramCount() = 1
      then Val (ParamStr (1),n,Code)
   else n := 7;
   answer := fannkuch(n);
   writeln('Pfannkuchen(',n,') = ', answer);
end.
