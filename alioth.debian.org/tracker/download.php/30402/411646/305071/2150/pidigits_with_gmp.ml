(*
 * The Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * contributed by Christophe TROESTLER
 * modified by Mat%G�%@as Giovannini
 * ported to Gmp by David Teller
 *)
open Printf
open Gmp.Z.Infixes

let big_0      = Gmp.Z.zero
let big_1      = Gmp.Z.one
let big_3      = Gmp.Z.of_int 3
let big_4      = Gmp.Z.of_int 4
let big_10     = Gmp.Z.of_int 10
let big_10_neg = Gmp.Z.of_int (-10)

(* Entier part of the linear fractional transform qrst of x *)
let ext (q,r,s,t) x = ((x *! q +! r) /! (x *! s +! t))

(* Multiply small int matrix qrst by big int matrix qrst' (small on left) *)
let mml (q,r,s,t) (q',r',s',t') =
  q *! q'  +!  r *! s',  q *! r'  +!  r *! t',
  s *! q'  +!  t *! s',  s *! r'  +!  t *! t'

(* Multiply big int matrix qrst by small int matrix qrst' (small on right) *)
let mmr (q,r,s,t) (q',r',s',t') =
  q' *! q  +!  s' *! r,  r' *! q  +!  t' *! r,
  q' *! s  +!  s' *! t,  r' *! s  +!  t' *! t

let unit = (Gmp.Z.one,Gmp.Z.zero,Gmp.Z.zero,Gmp.Z.one)

let next z   = ext z big_3
and safe z n = Gmp.Z.equal (ext z big_4) n
and prod z n = mml (big_10, big_10_neg *! n, big_0, big_1) z
and cons z k =
  let den = Gmp.Z.of_int (2*k + 1) in
  mmr z (Gmp.Z.of_int k, Gmp.Z.mul_2exp den 1, big_0, den)

let rec digit k z n row col =
  if n == 0 then printf "%*s\t:%i\n" (10-col) "" (row+col) else
  let d = next z in
  if safe z d then
    if col = 10 then begin
      let row = row + 10 in
      printf "\t:%i\n%s" row (Gmp.Z.to_string d);
      digit k (prod z d) (n-1) row 1
    end else begin
      print_string (Gmp.Z.to_string d);
      digit k (prod z d) (n-1) row (col+1)
    end
  else digit (k+1) (cons z k) n row col

let digits n = digit 1 unit n 0 0

let () = digits (try int_of_string (Array.get Sys.argv 1) with _ -> 27)
