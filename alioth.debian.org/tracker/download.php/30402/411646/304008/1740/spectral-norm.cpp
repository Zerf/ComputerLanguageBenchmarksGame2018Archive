// -*- mode: c++ -*-
//
// The Great Computer Language Shootout
// http://shootout.alioth.debian.org/
//
// Original C contributed by Sebastien Loisel
// Conversion to C++ by Jon Harrop
// Compile: g++ -O3 -o spectralnorm spectralnorm.cpp

#include <cmath>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <emmintrin.h>

template < typename Type >
class scoped_array
{

	public:

		scoped_array(Type * array)
			:
			array_(array)
		{
		}

		~scoped_array()
		{
			delete[] array_;
		}

		Type * get()
		{
			return array_;
		}

	private:

		Type * array_;

};

unsigned int matrix_size;

typedef double aligned_doubles[2] __attribute__((aligned(16)));

inline __m128d evaluate_A_i(int i, int j, double n) __attribute__((fastcall, pure, nothrow));

__m128d evaluate_A_i(int i, int j, double n)
{
	aligned_doubles values = {n / ((i + j) * (i + j + 1) / 2 + i + 1), n / ((i + j + 1) * (i + j + 2) / 2 + i + 2)};
	return _mm_load_pd(values);
}

inline __m128d evaluate_A_j(int i, int j, double n) __attribute__((fastcall, pure, nothrow));

__m128d evaluate_A_j(int i, int j, double n)
{
	aligned_doubles values = {n / ((i + j) * (i + j + 1) / 2 + i + 1), n / ((i + j + 1) * (i + j + 2) / 2 + i + 1)};
	return _mm_load_pd(values);
}

void evaluate_A_times_u(double const * u, double * result)
{
	unsigned int i = 0;
	do
	{
		__m128d sse_values = evaluate_A_i(i, 0, u[0]);
		for (unsigned int j = 1; j < matrix_size; ++j)
		{
			__m128d sse_a_values = evaluate_A_i(i, j, u[j]);
			sse_values = _mm_add_pd(sse_values, sse_a_values);
		}
		_mm_store_pd(result + i, sse_values);
		i += 2;
	} while (i < matrix_size);
}

void evaluate_At_times_u(double const * u, double * result)
{
	unsigned int i = 0;
	do
	{
		__m128d sse_values = evaluate_A_j(0, i, u[0]);
		for (unsigned int j = 1; j < matrix_size; ++j)
		{
			__m128d sse_a_values = evaluate_A_j(j, i, u[j]);
			sse_values = _mm_add_pd(sse_values, sse_a_values);
		}
		_mm_store_pd(result + i, sse_values);
		i += 2;
	} while (i < matrix_size);
}

void evaluate_AtA_times_u(double const * u, double * v, double * w)
{
	evaluate_A_times_u(u, w);
	evaluate_At_times_u(w, v);
}

int main(int argc, char * * argv)
{
	if (argc != 2)
	{
		std::cerr << "Usage: " << argv[0] << " <n>\n";
		return 1;
	}
	{
		std::istringstream convertor(argv[1]);
		if (!(convertor >> matrix_size) || !convertor.eof())
		{
			std::cerr << "Usage: " << argv[0] << " <n>\n";
			std::cerr << "\tn must be an integer\n";
			return 1;
		}
	}
	scoped_array< double > u_array(new double[matrix_size + 2]);
	scoped_array< double > v_array(new double[matrix_size + 2]);
	scoped_array< double > w_array(new double[matrix_size + 2]);
	double * u = reinterpret_cast< double * >(reinterpret_cast< std::size_t >(u_array.get() + 1) & ~0xf);
	double * v = reinterpret_cast< double * >(reinterpret_cast< std::size_t >(v_array.get() + 1) & ~0xf);
	double * w = reinterpret_cast< double * >(reinterpret_cast< std::size_t >(w_array.get() + 1) & ~0xf);
	std::fill(u, u + matrix_size, 1.0);
	for (unsigned int i = 0; i < 10; ++i)
	{
		evaluate_AtA_times_u(u, v, w);
		evaluate_AtA_times_u(v, u, w);
	}
	__m128d vs = _mm_load_pd(v);
	__m128d us = _mm_load_pd(u);
	__m128d vvs = _mm_mul_pd(vs, vs);
	__m128d vBvs = _mm_mul_pd(us, vs);
	for (unsigned int i = 2; i < (matrix_size & ~1); i += 2)
	{
		vs = _mm_load_pd(v + i);
		us = _mm_load_pd(u + i);
		__m128d temp1 = _mm_mul_pd(vs, vs);
		__m128d temp2 = _mm_mul_pd(us, vs);
		vvs = _mm_add_pd(vvs, temp1);
		vBvs = _mm_add_pd(vBvs, temp2);
	}
	aligned_doubles vBv;
	aligned_doubles vv;
	_mm_store_pd(vv, vvs);
	_mm_store_pd(vBv, vBvs);
	if (matrix_size & 1)
	{
		vBv[0] += u[matrix_size - 1] * v[matrix_size - 1];
		vv[0] += v[matrix_size - 1] * v[matrix_size - 1];
	}
	std::cout << std::setprecision(10) << std::sqrt((vBv[0] + vBv[1]) / (vv[0] + vv[1])) << '\n';
}
