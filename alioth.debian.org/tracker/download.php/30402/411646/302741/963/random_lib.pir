.const float IM = 139968.0
.const float IA = 3877.0
.const float IC = 29573.0

.sub init :load :anon
	.local pmc last
	last = new .Float
	last = 42.0
	global "last" = last
.end

.sub gen_random
	.param float max
	.local pmc last
	last = global "last"
	$N0 = last
	$N0 *= IA
	$N0 += IC
	$N0 %= IM
	$N1 = max
	$N1 *= $N0
	$N1 /= IM
	last = $N0
	.return($N1)
.end

