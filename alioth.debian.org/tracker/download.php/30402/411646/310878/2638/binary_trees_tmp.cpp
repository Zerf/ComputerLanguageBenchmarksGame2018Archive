/* binary_trees_tmp.cpp
 *
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Calum Grant
 */

#include <iostream>
#include <memory>

template<typename Item, typename Left, typename Right> struct node
{
	template<typename I, typename L, typename R>
		node(I i, L l, R r) : item(i), left(l), right(r) { }
	const Item item;
	const Left left;
	const Right right;
};

template<typename Item> struct leaf_node
{
	template<typename I> leaf_node(I i) : item(i) { }
	const Item item;
};

template<typename Item> int check(const leaf_node<Item> & node)
{
	return node.item;
}

template<typename Item, typename Left, typename Right> 
	int check(const node<Item,Left,Right> & node)
{
	return node.item + check(node.left) - check(node.right);
}

template<int Depth> struct make : public node<int, make<Depth-1>, make<Depth-1> >
{
	make(int i) : node< int, make<Depth-1>, make<Depth-1> >(i, 2*i-1, 2*i) { }
};

template<> struct make<0> : public leaf_node<int>
{
	make(int i) : leaf_node<int>(i) { }
};

template<int Depth> std::auto_ptr<make<Depth> > make_tree(int i) 
{ 
	return std::auto_ptr< make<Depth> >(new make<Depth>(i)); 
}

template<int MinDepth, int MaxDepth=24> struct test
{
	template<int D=MinDepth, bool=D<=MaxDepth> struct loop
	{
		loop()
		{
			const int iterations = 1 << (MaxDepth - D + MinDepth);
			int c=0;

			for(int i=1; i<=iterations; ++i)
			{
				c += check(*make_tree<D>(i)) + check(*make_tree<D>(-i));
			}

			std::cout << (2*iterations) << "\t trees of depth " << D << "\t "
				<< "check: " << c << std::endl;

			loop<D+2>();
		}
	};

	template<int D> struct loop<D, false> { };

	static void run(int max_depth)
	{
		if(max_depth < MaxDepth) 
		{
			test<MinDepth, MaxDepth-1>::run(max_depth);
		}
		else
		{
			const int stretch_depth = MaxDepth+1;
		    std::cout << "stretch tree of depth " << stretch_depth << "\t "
				<< "check: " << check(*make_tree<stretch_depth>(0)) << std::endl;

			std::auto_ptr< make<MaxDepth> > long_lived_tree = make_tree<MaxDepth>(0);

			loop<>();

			std::cout << "long lived tree of depth " << MaxDepth << "\t "
				<< "check: " << check(*long_lived_tree) << "\n";
		}
	}
};

template<int MinDepth> struct test<MinDepth, 0>
{
	static void run(int) { }
};

int main(int argc, char *argv[]) 
{
	const int min_depth=4;
    int max_depth = std::max(min_depth+2, (argc == 2 ? atoi(argv[1]) : 10));
	test<min_depth>::run(max_depth);
	return 0;
}
