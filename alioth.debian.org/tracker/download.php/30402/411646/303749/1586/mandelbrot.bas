option explicit
option escape
#include "crt.bi"
#define iter 50 
 
dim shared w,h
const  limit=4.0
function calcmandel(byval x,byval y) 
  dim as double zr=0.0, zi=0.0, cr, ci, tr, ti
  dim ii
  cr = (2.0*x/w - 1.5) : ci=(2.0*y/h - 1.0)
  for ii = 0 to iter-1
    zi = 2.0*zr*zi + ci
    zr = tr - ti + cr
    tr=zr*zr : ti=zi*zi
    if tr+ti > limit then return 0
  next 
  return -1
end function

dim  x, y,  i,w1,bb,i1
dim b as uinteger
  w = val(command$)
  if w < 1 then w = 300
  h = w
  w1=w-1
  printf("P4\n%d %d\n",w,h)
  '
  for y = 0 to h-1
    for x = 0 to w1 step 8
      b=0:bb=&h80  
      i1=iif (x+7>w1,w1,x+7)    
      for i =x to  i1
       b or= bb and calcmandel(i,y)
       bb shr=1     
      next 
      putchar b 
    next    
  next 
end

