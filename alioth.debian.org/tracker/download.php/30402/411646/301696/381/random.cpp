// -*- mode: c++ -*-
// $Id: random-gpp.code,v 1.7 2005/03/21 08:36:41 bfulgham Exp $
// http://shootout.alioth.debian.org/

#include <iostream>
#include <stdlib.h>
#include <math.h>

using namespace std;

#define IM 139968
#define IA 3877
#define IC 29573

inline double gen_random(double max) {
    static long last = 42;
    last = (last * IA + IC) % IM;
    return( max * last / IM );
}

int main(int argc, char *argv[]) {
    int N = ((argc == 2) ? atoi(argv[1]) : 1) - 1;

    while (N--) {
        gen_random(100.0);
    }
    cout.precision(9);
    cout.setf(ios::fixed);
    cout << gen_random(100.0) << endl;
    return(0);
}
