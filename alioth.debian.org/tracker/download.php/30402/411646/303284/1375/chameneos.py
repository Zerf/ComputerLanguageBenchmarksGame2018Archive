# The Computer Language Shootout
#  http://shootout.alioth.debian.org/
#
#  contributed by drigz
#  modified by Josh Goldfoot

import sys
import threading
import Queue

RED, BLUE, YELLOW, FADED = xrange(1,5)
mp_n = len(sys.argv) > 1 and int(sys.argv[1]) or 100
mp_first = None
mp_entrylock = threading.Lock()
mp_meetnotify = Queue.Queue(1)

class Creature:  # old-style classes faster
    def __init__(self, icolor):
        self.color = icolor
        self.met = 0

    def socialize(self):
        global mp_n, mp_first, mp_entrylock, mp_meetnotify, reports
        # optimize to minimize lookups of class variables and functions
        self_color = self.color
        self_met = self.met
        mp_entrylock_acquire = mp_entrylock.acquire
        mp_entrylock_release = mp_entrylock.release
        mp_meetnotify_put = mp_meetnotify.put
        mp_meetnotify_get = mp_meetnotify.get
        while 1: #keep looking for creatures to meet
            mp_entrylock_acquire()
            if mp_first == None: #noone's here
                if mp_n < 1: #check if meeting place is done
                    mp_entrylock_release()
                    self.met = self_met
                    return
                mp_n -= 1
                mp_first = self_color #register my presence
                mp_entrylock_release() #let the people enter so we can meet
                other = mp_meetnotify_get() #wait for the next person
                mp_first = None #we both leave
                mp_entrylock_release() #let others enter, our meeting is over
            else: #we've met someone else
                other = mp_first 
                mp_meetnotify_put(self_color)
            self_met += 1
            if other == FADED:
                self_color = FADED
            elif self_color != other:  # Calculate complementary color
                self_color = 6 - self_color - other
                
def main():
    colors = [BLUE, RED, YELLOW, BLUE]
    creatures = [Creature(c) for c in colors]
    threads = [threading.Thread(target=c.socialize) for c in creatures]
    for t in threads:
        t.start()
    for t in threads:
        t.join()
    print sum(c.met for c in creatures)

main()

