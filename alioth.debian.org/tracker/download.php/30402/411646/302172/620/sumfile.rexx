#!/usr/bin/regina -a

/* ------------------------------------------------------------------ */
/* The Great Computer Language Shootout                               */
/* http://shootout.alioth.debian.org/                                 */
/*                                                                    */
/* % regina -v                                                        */
/* REXX-Regina_3.3(MT) 5.00 25 Apr 2004                               */
/* % uname -orvmp                                                     */
/* 2.6.5-1.358 #1 Wed Oct 13 17:49:34 EST 2004 i686 i686 GNU/Linux    */
/*                                                                    */
/* Contributed by Anthony Borla                                       */
/* ------------------------------------------------------------------ */

trace 'OFF' ; sum = 0

do while LINES() > 0
  sum = sum + LINEIN()
end

say sum
