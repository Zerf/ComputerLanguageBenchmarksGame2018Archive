(* The Computer Language Shootout
   http://shootout.alioth.debian.org/
   
   contributed by Florian Klaempfl
   modified by Micha Nelissen
   modified by Vincent Snijders *)

program pfannkuchen;

type
    TIntegerArray = Array[0..99] of longint;
    
procedure swap(var a, b: longint); inline;
var
  tmp: longint;
begin
  tmp := a;
  a := b;
  b := tmp;
end;

function fannkuch(n : longint): longint;
var
   perm, perm1, count: TIntegerArray;
   print30, m, r, i, k, mostFlips, flips, perm0: longint;
   pi, pj : PLongint;
begin
   print30 := 0;
   mostFlips := 0;
   m := n - 1;
   for i := 0 to m do
       perm1[i] := i;
   r := n;
   repeat
      if print30 < 30 then
      begin
         for i := 0 to m do
            write(perm1[i] + 1);
         writeln;
         inc(print30);
      end;
      while r <> 1 do
      begin
         count[r-1] := r;
         dec(r);
      end;
      if not ((perm1[0]=0) or (perm1[m]=m)) then
      begin
         for i := 0 to m do
            perm[i] := perm1[i];
         flips := 0;
         repeat
           pi := @perm[0];
           pj := @perm[perm[0]];
           while pi<pj do
             begin
               swap(pi^,pj^);
               inc(pi);
               dec(pj);
             end;
           inc(flips);
         until perm[0] = 0;
         if flips > mostFlips then
            mostFlips := flips;
      end;
      while true do
      begin
         if r = n then
         begin
            fannkuch := mostFlips;
            exit;
         end
         else
         begin
            perm0 := perm1[0];
            i := 0;
            while i < r do
            begin
               k := i + 1;
               perm1[i] := perm1[k];
               i := k;
            end;
            perm1[r] := perm0;
            dec(count[r]);
            if count[r] > 0 then
              break;
            inc(r);
         end;
      end;
   until false;
end;

var
   n, answer : integer;

begin
   if paramCount() = 1 then
     Val(ParamStr(1), n)
   else
     n := 7;
   answer := fannkuch(n);
   writeln('Pfannkuchen(',n,') = ', answer);
end.

