'compile with fbc chameneos.bas

#include "crt.bi"
#Undef color

enum color
 blue
 red
 yellow
 green
 faded
end enum

dim Shared As integer  nmeetings,report
dim shared as color color1
dim shared as any ptr cond1,cond12
dim shared  As integer meeting
'
'--------------------------------------------------------
Function complementaryColor (ByVal c1 as color,byval c2 as color) as color
  if (c2 = Faded) then return Faded
  if (c1 = c2) then return c1
  select case as const c1
    case Blue:return iif (c2 = Red , Yellow , Red)
    case Red:return iif (c2 = Blue , Yellow , Blue)
    case Yellow:return iif(c2 = Blue, Red , Blue)
    case else:return c1
   end select
end function
'
'---------------------------------------------------------
sub chameneos(ByVal index As integer )
dim As Integer count
dim as color clr
clr=index
do
select case as const meeting
 case 0
  if nmeetings=0 then
    clr=faded
    report+=count
    condbroadcast cond12
    exit sub
  else
    meeting=1
    'condbroadcast cond12
    condwait cond1
    'someone else is at the meeting point and has signaled cond1
    clr=complementarycolor (clr,color1)
    nmeetings-=1
    count+=1
    color1=0
    meeting=0
    condbroadcast cond12
  end if
  
case 1
  meeting=2
  color1=clr
  count+=1
  condsignal cond1
case else
  condwait cond12
end select
loop
end sub
'
'-----------------------------------------------------------

nmeetings =valint(command):if nmeetings=0 then nmeetings=100

cond1=condcreate
cond12=condcreate

dim as any ptr thechameneos(3)
for i As integer=0 to 3
   thechameneos(i)=threadcreate(@chameneos,Cast(Any ptr,i))
   if thechameneos(i)=0 then print "Error Creating thread "& i:end
next

for i As integer=0 to 3
threadwait(thechameneos(i))
next

printf ("%d\n",report)
conddestroy (cond1)
conddestroy(cond12)