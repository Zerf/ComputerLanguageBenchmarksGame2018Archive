# The Computer Language Shootout
# http://shootout.alioth.debian.org/
# Contributed by Mario Pernici, based on the Python version by J. Goldfoot

from math import sin, cos, sqrt
from sys import argv

def main_function(n, cos=cos, sin=sin, sqrt=sqrt):
    s0 = s1 = s2 = s2 = s3 = s4 = s5 = s6 = s7 = s8 = 0.
    twoThirds = 2.0/3.0
    alt = 1.
    for k in range(1,n+1):
      r = 1.0/k
      sk = sin(k)
      ck = cos(k)
      d2 = r*r
      d3 = d2*r
      s0 += twoThirds ** (k-1)
      s1 += sqrt(r)
      s2 += r/(k+1)
      s3 += d3/(sk*sk)
      s4 += d3/(ck*ck)
      s5 += r
      s6 += d2
      s7 += alt*r
      s8 += alt/(2 * k - 1)
      alt = -alt

    print "%0.9f\t(2/3)^k" % s0
    print "%0.9f\tk^-0.5" % s1
    print "%0.9f\t1/k(k+1)" % s2
    print "%0.9f\tFlint Hills" % s3
    print "%0.9f\tCookson Hills" % s4
    print "%0.9f\tHarmonic" % s5
    print "%0.9f\tRiemann Zeta" % s6
    print "%0.9f\tAlternating Harmonic" % s7
    print "%0.9f\tGregory" % s8


import psyco; psyco.bind(main_function)
main_function( int(argv[1]) )

