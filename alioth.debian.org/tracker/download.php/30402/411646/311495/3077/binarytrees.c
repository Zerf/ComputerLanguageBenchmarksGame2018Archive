/* The Computer Language Shootout Benchmarks
   http://shootout.alioth.debian.org/

   contributed by Kevin Carson
   based on code by Eckehard Berns
   modified by Cédric Krier
   compilation:
       gcc -O3 -fomit-frame-pointer -funroll-loops -static binarytrees.c -lpthread
       icc -O3 -ip -unroll -static binarytrees.c -lpthread
*/

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


typedef struct tn {
    struct tn*    left;
    struct tn*    right;
    long          item;
} treeNode;

typedef struct pt {
    treeNode*     pool;
    treeNode*     new;
} poolTree;

typedef struct worker_args {
    long iter, check;
    int depth;
    pthread_t id;
    struct worker_args *next;
} worker_args;


treeNode* NewTreeNode(treeNode* left, treeNode* right, long item, poolTree* pool)
{
    treeNode* new = pool->new++;

    new->left = left;
    new->right = right;
    new->item = item;

    return new;
} /* NewTreeNode() */


long ItemCheck(treeNode* tree)
{
    long result;
    if (tree->left == NULL)
        result = tree->item;
    else
        result = tree->item + ItemCheck(tree->left) - ItemCheck(tree->right);
    return result;
} /* ItemCheck() */


treeNode* BottomUpTree(long item, unsigned depth, poolTree* pool)
{
    if (depth > 0)
        return NewTreeNode
        (
            BottomUpTree(2 * item - 1, depth - 1, pool),
            BottomUpTree(2 * item, depth - 1, pool),
            item,
            pool
        );
    else
        return NewTreeNode(NULL, NULL, item, pool);
} /* BottomUpTree() */


poolTree* NewPoolTree(long size)
{
    poolTree* new;

    new = (poolTree*)malloc(sizeof(poolTree));

    new->pool = (treeNode*)malloc(size * sizeof(treeNode));
    new->new = new->pool;

   return new;
} /* NewPoolTree() */


void DeletePoolTree(poolTree* pool)
{
    free(pool->pool);
    free(pool);
} /* DeletePoolTree() */


static void* CheckTree(void* _args)
{
    worker_args *args = _args;
    poolTree *pool;
    long i;
    for (i = 1; i <= args->iter; i++)
    {
        pool = NewPoolTree((1 << (args->depth + 1)) -1);
        args->check += ItemCheck(BottomUpTree(i, args->depth, pool));
        DeletePoolTree(pool);
        pool = NewPoolTree((1 << (args->depth + 1)) -1);
        args->check += ItemCheck(BottomUpTree(-i, args->depth, pool));
        DeletePoolTree(pool);
    } /* for (i = 1...) */
    return NULL;
} /* CheckTree() */


int main(int argc, char* argv[])
{
    unsigned   N, depth, minDepth, maxDepth, stretchDepth;
    treeNode   *stretchTree, *longLivedTree;
    struct worker_args *args, *targs, *hargs;
    poolTree *pool;

    N = atol(argv[1]);

    minDepth = 4;

    if ((minDepth + 2) > N)
        maxDepth = minDepth + 2;
    else
        maxDepth = N;

    stretchDepth = maxDepth + 1;

    pool = NewPoolTree((1 << (stretchDepth + 1)) -1);
    stretchTree = BottomUpTree(0, stretchDepth, pool);
    printf
    (
        "stretch tree of depth %u\t check: %li\n",
        stretchDepth,
        ItemCheck(stretchTree)
    );
    DeletePoolTree(pool);

    pool = NewPoolTree((1 << (maxDepth + 1)) -1);
    longLivedTree = BottomUpTree(0, maxDepth, pool);

    hargs = NULL;
    targs = NULL;
    for (depth = minDepth; depth <= maxDepth; depth += 2)
    {
        args = malloc(sizeof(struct worker_args));
        args->iter = 1 << (maxDepth - depth + minDepth);
        args->depth = depth;
        args->check = 0;
        args->next = NULL;

        if (targs == NULL) {
            hargs = args;
            targs = args;
        } else {
            targs->next = args;
            targs = args;
        }

        pthread_create(&args->id, NULL, CheckTree, args);
    } /* for(depth = minDepth...) */

    while (hargs != NULL) {
        args = hargs;
        pthread_join(args->id, NULL);
        printf
        (
            "%li\t trees of depth %u\t check: %li\n",
            args->iter * 2,
            args->depth,
            args->check
        );
        hargs = args->next;
        free(args);
    } /* while (hargs != NULL) */

    printf
    (
        "long lived tree of depth %u\t check: %li\n",
        maxDepth,
        ItemCheck(longLivedTree)
    );

    return 0;
} /* main() */

