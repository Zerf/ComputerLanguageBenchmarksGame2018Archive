#!/usr/bin/perl
# The Computer Language Shootout
# http://shootout.alioth.debian.org/
# Perl version of floating point Tak function
# by Greg Buchholz
# memory consumption fixed by Danny Sauer

my $n = $ARGV[0];
sub tak($$$);

printf "%.1f\n", tak(3*$n, 2*$n, $n);

sub tak($$$)
{
    return ($_[1] >= $_[0]) ? 
           $_[2] : 
           tak( tak($_[0]-1.0, $_[1], $_[2]),
                tak($_[1]-1.0, $_[2], $_[0]),
                tak($_[2]-1.0, $_[0], $_[1])
              )
}
