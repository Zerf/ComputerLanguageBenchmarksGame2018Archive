with Ada.Command_Line;use Ada.Command_Line;
with Ada.Text_Io;use Ada.Text_Io;
with Ada.Integer_Text_Io;use Ada.Integer_Text_Io;

procedure Nsievebits is 
   
   function Count (M : in     Natural ) return Natural is 
      type Boolean_Array is array (2 .. M) of Boolean; 
      pragma Pack (Boolean_Array);
      C : Natural       := 0;  
      S : Boolean_Array := (others => True);  
      I : Positive;  
   begin
      for K in S'range loop
         if S(K) then
            C := C + 1;
            I := K;
            loop
               I := I + K;
               exit when I > M;
               S(I) := False;
            end loop;
         end if;
      end loop;
      return C;
   end Count;
   
   procedure Run (N : in     Natural ) is 
      M : Natural;  
   begin
      M := 2 ** N * 10_000;
      Put ("Primes up to ");
      Put (Item  => M, Width => 8);
      Put (Item  => Count (M), Width => 8);
      New_Line;
   end Run;
   
   N : constant Natural := Natural'Value (Argument (1));  
begin
   Run (N);
   Run (N - 1);
   Run (N - 2);
end Nsievebits; 
   
