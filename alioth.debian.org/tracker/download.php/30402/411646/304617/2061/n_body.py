# The Computer Language Shootout Benchmarks
# http://shootout.alioth.debian.org/
#
# contributed by Dani Nanz

pi = 3.14159265358979323
solar_mass = 4.0 * pi * pi
days_per_year = 365.24
default_dt = 0.01
default_step_no = 1000

known_bodies = {
    'sun':     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, solar_mass],
    'jupiter': [4.84143144246472090e+00,
                -1.16032004402742839e+00,
                -1.03622044471123109e-01,
                1.66007664274403694e-03  * days_per_year,
                7.69901118419740425e-03 * days_per_year,
                -6.90460016972063023e-05 * days_per_year,
                9.54791938424326609e-04 * solar_mass],
    'saturn':  [8.34336671824457987e+00,
                4.12479856412430479e+00,
                -4.03523417114321381e-01,
                -2.76742510726862411e-03 * days_per_year,
                4.99852801234917238e-03 * days_per_year,
                2.30417297573763929e-05 * days_per_year,
                2.85885980666130812e-04 * solar_mass],
    'uranus':  [1.28943695621391310e+01,
                -1.51111514016986312e+01,
                -2.23307578892655734e-01,
                2.96460137564761618e-03 * days_per_year,
                2.37847173959480950e-03 * days_per_year,
                -2.96589568540237556e-05 * days_per_year,
                4.36624404335156298e-05 * solar_mass],
    'neptune': [1.53796971148509165e+01,
                -2.59193146099879641e+01,
                1.79258772950371181e-01,
                2.68067772490389322e-03 * days_per_year,
                1.62824170038242295e-03 * days_per_year,
                -9.51592254519715870e-05 * days_per_year,
                5.15138902046611451e-05 * solar_mass]}


class body():

    def __init__(self, name):

        self.x = known_bodies[name][0]
        self.y = known_bodies[name][1]
        self.z = known_bodies[name][2]
        self.vx = known_bodies[name][3]
        self.vy = known_bodies[name][4]
        self.vz = known_bodies[name][5]
        self.mass = known_bodies[name][6]


class body_system():
    
    def __init__(self, body_names, offset_momentum = 'no'):

        self.bodies = list(body(name) for name in body_names)
        if offset_momentum == 'yes': self.offset_momentum()
        self.update_energy()


    def offset_momentum(self):

        px = py = pz = 0.0
        for b in self.bodies:
            px += b.vx * b.mass
            py += b.vy * b.mass
            pz += b.vz * b.mass
        self.bodies[0].vx = - px / self.bodies[0].mass
        self.bodies[0].vy = - py / self.bodies[0].mass
        self.bodies[0].vz = - pz / self.bodies[0].mass


    def update_energy(self):

        e = 0.0
        for index, b1 in enumerate(self.bodies):
            e += (b1.mass / 2.0) * (b1.vx * b1.vx + b1.vy * b1.vy + b1.vz * b1.vz)
            for b2 in self.bodies[index + 1 :]:
                dx, dy, dz = b1.x - b2.x, b1.y - b2.y, b1.z - b2.z
                d = (dx * dx + dy * dy + dz * dz) ** 0.5
                e -= (b1.mass * b2.mass) / d
        self.energy = e


    def advance(self, dt, n):

        for b in self.bodies: b.mt = b.mass * dt
        for i in xrange(n):
            for index, b1 in enumerate(self.bodies):
                b1x, b1y, b1z, b1mt = b1.x, b1.y, b1.z, b1.mt
                for b2 in self.bodies[index + 1 :]:
                    dx, dy, dz = b1x - b2.x, b1y - b2.y, b1z - b2.z
                    d3 = (dx * dx + dy * dy + dz * dz) ** 1.5
                    mag1 = b1mt / d3
                    mag2 = b2.mt / d3
                    b1.vx -= dx * mag2
                    b1.vy -= dy * mag2
                    b1.vz -= dz * mag2
                    b2.vx += dx * mag1
                    b2.vy += dy * mag1
                    b2.vz += dz * mag1
                b1.x += dt * b1.vx
                b1.y += dt * b1.vy
                b1.z += dt * b1.vz       
        self.update_energy()

           
if __name__ == '__main__':

    from sys import argv 
    
    n = (len(argv) > 1) and int(argv[1]) or default_step_no
    bodies = ['sun', 'jupiter', 'saturn', 'uranus', 'neptune']
    bs = body_system(bodies, offset_momentum = 'yes')
    print '%.9f' % bs.energy
    bs.advance(default_dt, n)
    print '%.9f' % bs.energy
