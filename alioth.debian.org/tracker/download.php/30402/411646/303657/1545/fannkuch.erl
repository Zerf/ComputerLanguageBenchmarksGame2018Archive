%%%-------------------------------------------------------------------
%%% File    : fannkuch.erl
%%% Author  : Mats Cronqvist <locmacr@mwlx084>
%%% Description : 
%%%
%%% Created : 25 Apr 2006 by Mats Cronqvist <locmacr@mwlx084>
%%%-------------------------------------------------------------------
-module(fannkuch).

-export([main/1]).

main([Arg]) ->
    N = list_to_integer(Arg),
    main(N),
    erlang:halt(0);
main(N) when N > 0 ->
    put(max,0),
    put(print,30),
    perm(N,init(N)),
    io:fwrite("Pfannkuchen(~p) = ~p~n", [N, get(max)]).

%% perm and prot generates the permutations in the right order and 
%% calls x/1 for each one

perm(N,Data) when length(Data) < N -> Data;
perm(3,Data) -> x(flip(1,x(flip(2,x(flip(1,x(flip(2,x(flip(1,x(Data)))))))))));
perm(N,Data) -> prot(N,N,Data).

prot(_N,0,Data) -> Data;
prot(N,M,Data) -> perm(N-1,Data), prot(N,M-1,rotate(N,Data)).

%% x/1 is the action function. called once for each permutation. calls
%% kuch/1 to get the number of flips
x(Data) ->
    case get(print) of
 	0 -> ok;
 	N -> flush(Data),put(print,N-1)
    end,
    case get(max) < (K = kuch(Data,0)) of
 	true -> put(max,K);
 	false -> ok
    end,
    Data.

%% this is the actual flipping function. return number of flips
kuch(Data,N) ->
    case head(Data) of
	1 -> N;
	M -> kuch(reverse(M,Data),N+1)
    end.


%% primitives for our data model
%%
%% init(Length) - create new struct
%% head(Data) - return first element 
%% reverse(N, Data) - reverse first N elements
%% rotate(N, Data) - rotate first N elements
%% flip(N, Data) - exchange N:th and N+1:th element
%% flush(Data) - flush to screen

init(N) -> lists:seq(1,N).

head(L) -> hd(L).

reverse(2,[H1,H2|L]) -> 
    [H2,H1|L];
reverse(3,[H1,H2,H3|L]) ->  
    [H3,H2,H1|L];
reverse(4,[H1,H2,H3,H4|L]) ->  
    [H4,H3,H2,H1|L];
reverse(5,[H1,H2,H3,H4,H5|L]) ->  
    [H5,H4,H3,H2,H1|L];
reverse(6,[H1,H2,H3,H4,H5,H6|L]) ->  
    [H6,H5,H4,H3,H2,H1|L];
reverse(7,[H1,H2,H3,H4,H5,H6,H7|L]) ->  
    [H7,H6,H5,H4,H3,H2,H1|L];
reverse(8,[H1,H2,H3,H4,H5,H6,H7,H8|L]) ->  
    [H8,H7,H6,H5,H4,H3,H2,H1|L];
reverse(9,[H1,H2,H3,H4,H5,H6,H7,H8,H9|L]) ->  
    [H9,H8,H7,H6,H5,H4,H3,H2,H1|L];
reverse(10,[H1,H2,H3,H4,H5,H6,H7,H8,H9,H0|L]) ->  
    [H0,H9,H8,H7,H6,H5,H4,H3,H2,H1|L];
reverse(M,L) -> 
    {H,T} = lists:split(M,L), 
    lists:reverse(H)++T.
    
rotate(4,[H1,H2,H3,H4|T]) -> [H2,H3,H4,H1|T];
rotate(5,[H1,H2,H3,H4,H5|T]) -> [H2,H3,H4,H5,H1|T];
rotate(N,L) -> {[H|Hs],Ts} = lists:split(N,L), Hs++[H|Ts].

flip(1,[H1,H2|T]) -> [H2,H1|T];
flip(2,[H,H1,H2|T]) -> [H,H2,H1|T].

flush(Data) ->
    lists:foreach(fun(I)->io:fwrite("~p",[I]) end, Data),
    io:fwrite("~n").
