{ The Great Computer Language Shootout
  http://shootout.alioth.debian.org

  contributed by Ian Osgood
}

program nsieveBits;
uses SysUtils;

const bpc = SizeOf(cardinal) * 8;

procedure primes(n : integer);
var size,count,prime,i : longint;
    flags : array of cardinal;

  function IsSet(i : longint) : boolean; inline;
  var offset : longint; mask : cardinal;
  begin
    offset := i div bpc;
    mask   := 1 shl (i mod bpc);
    IsSet := (flags[offset] and mask) <> 0;
  end;

  procedure Clear(i : longint); inline;
  var offset : longint; mask : cardinal;
  begin
    offset := i div bpc;
    mask   := 1 shl (i mod bpc);
    flags[offset] := flags[offset] and not mask;
  end;

begin
  size := 10000 shl n;
  SetLength(flags, size div bpc + 1);
  for i := low(flags) to high(flags) do flags[i] := high(cardinal);
  count := 0;
  for prime := 2 to size do
    if IsSet(prime) then
    begin
      count := count + 1;
      i := prime + prime;
      while i <= size do
      begin
        Clear(i);
        i := i + prime;
      end;
    end;
  writeln('Primes up to', size:9, count:9);
end;

var n : integer;
begin
  n := StrToInt(paramstr(1));
  primes(n);
  primes(n-1);
  primes(n-2);
end.
