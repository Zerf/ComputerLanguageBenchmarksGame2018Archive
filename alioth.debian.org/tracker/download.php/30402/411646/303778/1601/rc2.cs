/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Isaac Gouy
   modified by Robert F. Tobler to perform actual computations on byte arrays
*/

using System;
using System.IO;
using System.Text;
using System.Collections;

class revcomp
{
   static void Main(string[] args){
      InitializeComplements();

      ArrayList seq = new ArrayList();
      String line, desc = "";

      using (StreamReader r = new StreamReader(Console.OpenStandardInput())){
         using (StreamWriter w = new StreamWriter(Console.OpenStandardOutput())){
            while ((line = r.ReadLine()) != null) {
               char c = line[0];
               if (c == '>'){
                  if (desc.Length > 0){
                     WriteReverseSeq(desc, seq, w);
                     seq = new ArrayList();
                  }
                  desc = line;
               }
               else if (c != ';'){
                  byte[] byteLine = new byte[line.Length];
                  for (int i = 0; i < line.Length; i++)
                     byteLine[i] = (byte)line[i];
                  seq.Add(byteLine);
               }
            }
            if (seq.Count > 0){
               WriteReverseSeq(desc, seq, w);
            }
         }
      }
   }

   static byte[] iubComp = new byte[256];
   static void InitializeComplements(){
      for (byte i = 0; i < 255; i++) iubComp[i] = i;
          
      iubComp[(byte)'A'] = (byte)'T'; iubComp[(byte)'a'] = (byte)'T';
      iubComp[(byte)'B'] = (byte)'V'; iubComp[(byte)'b'] = (byte)'V';
      iubComp[(byte)'C'] = (byte)'G'; iubComp[(byte)'c'] = (byte)'G';
      iubComp[(byte)'D'] = (byte)'H'; iubComp[(byte)'d'] = (byte)'H';
      iubComp[(byte)'G'] = (byte)'C'; iubComp[(byte)'g'] = (byte)'C';
      iubComp[(byte)'H'] = (byte)'D'; iubComp[(byte)'h'] = (byte)'D';
      iubComp[(byte)'K'] = (byte)'M'; iubComp[(byte)'k'] = (byte)'M';
      iubComp[(byte)'M'] = (byte)'K'; iubComp[(byte)'m'] = (byte)'K';
      iubComp[(byte)'R'] = (byte)'Y'; iubComp[(byte)'r'] = (byte)'Y';
      iubComp[(byte)'T'] = (byte)'A'; iubComp[(byte)'t'] = (byte)'A';
      iubComp[(byte)'V'] = (byte)'B'; iubComp[(byte)'v'] = (byte)'B';
      iubComp[(byte)'Y'] = (byte)'R'; iubComp[(byte)'y'] = (byte)'R';
   }

   static void WriteReverseSeq(String desc, ArrayList b, StreamWriter s) {
      const int LineLength = 60;
      char[] buffer = new char[LineLength];
      s.WriteLine(desc);

      int ci = 0;
      for (int rli = b.Count-1; rli >= 0; rli--) {
         byte[] line = (byte[])b[rli];
         for (int rci = line.Length-1; rci >= 0; rci--) {
            buffer[ci++] = (char)iubComp[line[rci]];
            if (ci >= LineLength) { s.WriteLine(buffer); ci = 0; }
         }
      }
      if (ci > 0) { s.WriteLine(buffer, 0, ci); }
   }
}

