(* Written by sweeks@sweeks.com.
 * Based on nsieve.ocaml.
 * Optimized and shortened by Vesa Karvonen.
 *)

fun lsl (i, j) = Word.toInt (Word.<< (Word.fromInt i, Word.fromInt j))
val i2s = StringCvt.padLeft #" " 8 o Int.toString
fun nsieve m =
    let val s = Array.array (m + 1, 0w1 : Word8.word)
        fun step (i, cnt) =
            let fun whack j = if m < j then ()
                              else (Array.update (s, j, 0w0) ; whack (j + i))
            in if m < i then cnt
               else step (i + 1, if 0w0 = Array.sub (s, i) then cnt
                                 else (whack (lsl (i, 1)) ; cnt + 1)) end
    in app print ["Primes up to ", i2s m, " ", i2s (step (2, 0)), "\n"] end
val n = valOf (Int.fromString (hd (CommandLine.arguments ())))
val _ = (nsieve (lsl (1, n) * 10000)
       ; nsieve (lsl (1, n - 1) * 10000)
       ; nsieve (lsl (1, n - 2) * 10000))
