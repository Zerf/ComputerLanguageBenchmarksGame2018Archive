/* ------------------------------------------------------------------ */
/* The Great Computer Language Shootout                               */
/* http://shootout.alioth.debian.org/                                 */
/*                                                                    */
/* Contributed by Anthony Borla                                       */
/* ------------------------------------------------------------------ */

#include <cstdlib>

#include <complex>

#include <functional>
#include <algorithm>

#include <iterator>
#include <sstream>
#include <iostream>

using namespace std;

typedef complex<double> Complex;
typedef ostreambuf_iterator<char> StreamIterOut;

/* ----------------------------- */

// STL 'lookalike' algorithm [based on STL's 'generate_n']
template <class OutputIter_, class Generator_, class Predicate_>
OutputIter_ generate_while(OutputIter_ first_, Generator_& gen_, Predicate_ pred_)
{
  typename std::iterator_traits<OutputIter_>::value_type value_(gen_());

  for ( ; pred_(value_); ++first_)
  {
    *first_ = value_; value_ = gen_();
  }

  return first_;
}

/* ----------------------------- */

class MandelbrotGenerator
{
public:
  enum { EOS = -1 };

public:
  MandelbrotGenerator(int height, int width);

  bool done() const { return eos_; }

  char next();
  void reset();
  void header(ostream& out) const;

  operator bool() const { return !done(); }
  char operator()() { return next(); }

public:
  struct Done
  {
  public:
    Done(MandelbrotGenerator& mref) : mref_(mref) { mref_.reset(); }
    bool operator()(char) const { return !mref_.done(); }

  private:
    MandelbrotGenerator& mref_;
  };

private:
  MandelbrotGenerator(const MandelbrotGenerator&);
  MandelbrotGenerator& operator=(const MandelbrotGenerator&);

  static int mandel(int n, const Complex& z, const Complex& c);

private:
  int x_, y_, height_, width_;
  bool eos_; 
};

/* ----------------------------- */

int main(int argc, char* argv[])
{
  ios_base::sync_with_stdio(false);

  if (argc != 2) { cerr << "Usage: " << argv[0] << " height"; return EXIT_FAILURE; }
  int n; if (!(istringstream(argv[1]) >> n) || n < 1) n = 100;
  
  MandelbrotGenerator mandel(n, n);

  mandel.header(cout);
  generate_while(StreamIterOut(cout), mandel, MandelbrotGenerator::Done(mandel));

  return EXIT_SUCCESS;
}

/* ----------------------------- */

MandelbrotGenerator::MandelbrotGenerator(int height, int width)
  : x_(0), y_(0), height_(height), width_(width), eos_(false)
{
}

/* ---------- */

char MandelbrotGenerator::next()
{
  char byte = 0; int bitNumber = 0, limitMarker; bool output = false;

  for ( ; y_ < height_; ++y_)
  {
    for ( ; x_ < width_; ++x_)
    {
      Complex z, c(2.0 * x_ / width_ - 1.5, 2.0 * y_ / height_ - 1.0);

      limitMarker = mandel(50, z, c);

      bitNumber += 1; if (bitNumber == 8) output = true;

      byte = (byte << 1) | limitMarker;

      if (x_ == width_ - 1 && bitNumber != 8)
      {
        byte = byte << (8 - width_ % 8); output = true;
      }

      if (output) { ++x_; return byte; }
    }

    x_ = 0;
  }

  eos_ = true ; return EOS;
}

/* ----------- */

void MandelbrotGenerator::reset()
{
  x_ = 0; y_ = 0; eos_ = false;
}

/* ----------- */

void MandelbrotGenerator::header(ostream& out) const 
{
  out << "P4" << "\n" << width_ << " " << height_ << endl;
}

/* ----------- */

int MandelbrotGenerator::mandel(int n, const Complex& z, const Complex& c)
{
  if (real(z * conj(z)) > 4.0) return 0;
  if (n == 0) return 1;
  return MandelbrotGenerator::mandel(--n, z * z + c, c);
}

