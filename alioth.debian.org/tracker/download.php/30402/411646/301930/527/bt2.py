#!/usr/bin/python -OO
# The Computer Language Shootout Benchmarks
# http://shootout.alioth.debian.org/
#
# contributed by Antoine Pitrou

import sys
import psyco
#~ psyco.full()

def make_tree(item, depth):
    if depth > 0:
        i = item * 2
        depth -= 1
        return (item, make_tree(i - 1, depth), make_tree(i, depth))
    else:
        return (item, None, None)

def check_tree((item, left, right)):
    if left is not None:
        return item + check_tree(left) - check_tree(right)
    else:
        return item

def main():
    N = int(sys.argv[1])

    min_depth = 4
    max_depth = max(min_depth + 2, N)
    stretch_depth = max_depth + 1

    strecth_tree = make_tree(0, stretch_depth)
    print "stretch tree of depth %d\t  check: %d" \
        % (stretch_depth, check_tree(strecth_tree))
    strecth_tree = None

    long_lived_tree = make_tree(0, stretch_depth)
    for depth in xrange(min_depth, max_depth + 1, 2):
        iterations = 2**(max_depth - depth + min_depth)
        check = 0

        for i in xrange(1, iterations + 1):
            temp_tree = make_tree(i, depth)
            check += check_tree(temp_tree)
            temp_tree = make_tree(-i, depth)
            check += check_tree(temp_tree)

        print "%d\t  trees of depth %d\t  check: %d" \
            % (iterations * 2, depth, check)

    print "long lived tree of depth %d\t  check: %d" \
        % (max_depth, check_tree(long_lived_tree))


main()
