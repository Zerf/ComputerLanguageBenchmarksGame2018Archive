{ The Great Computer Language Shootout
  http://shootout.alioth.debian.org

  contributed by Ian Osgood
}

{$mode objfpc}

program fasta;

uses Math;

const ALU : AnsiString =
  'GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG' +
  'GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA' +
  'CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT' +
  'ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA' +
  'GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG' +
  'AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC' +
  'AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA';

const codes = 'acgtBDHKMNRSVWY';

const IUB : array[0..14] of double = ( 0.27, 0.12, 0.12, 0.27,
  0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02 );

const HomoSap : array[0..3] of double = (
  0.3029549426680, 0.1979883004921, 0.1975473066391,  0.3015094502008 );

const LineLen = 60;

var n, z : longint;

procedure fastaRepeat(n : integer);
var line : string;
    posALU, lenOut : integer;
begin
  posALU := 1;
  repeat
    lenOut := min(n, LineLen);
    line := Copy(ALU, posALU, lenOut);
    if length(line) = lenOut then posALU := posALU + lenOut else
    begin
      posALU := lenOut - length(line) + 1;
      line := line + Copy(ALU, 1, posALU-1);
    end;
    writeln(line);
    n := n - lenOut;
  until n = 0;
end;

procedure fastaRandom(n : integer; probs: array of double);
var line : string;
    i : integer;

  function genRandom(limit : integer): double;
  const seed : longint = 42;
  begin
    seed := (seed * 3877 + 29573) mod 139968;
    genRandom := limit * seed / 139968;
  end;

  function chooseCode : char;
  var r : double;
      i : integer;
  begin
    r := genRandom(1);
    for i := low(probs) to high(probs) do
      if r < probs[i] then break;
    chooseCode := codes[i-low(probs)+1];
  end;

begin
  { make cumulative }
  for i := low(probs)+1 to high(probs) do
    probs[i] := probs[i] + probs[i-1];

  SetLength(line,lineLen);
  while n > lineLen do
  begin
    for i := 1 to lineLen do
      line[i] := chooseCode;
    writeln(line);
    n := n - lineLen;
  end;

  SetLength(line,n);
  for i := 1 to n do
    line[i] := chooseCode;
  writeln(line);
end;


begin
  Val(ParamStr(1), n, z);

  writeln('>ONE Homo sapiens alu');
  fastaRepeat(n*2);

  writeln('>TWO IUB ambiguity codes');
  fastaRandom(n*3, IUB);

  writeln('>THREE Homo sapiens frequency');
  fastaRandom(n*5, HomoSap);
end.