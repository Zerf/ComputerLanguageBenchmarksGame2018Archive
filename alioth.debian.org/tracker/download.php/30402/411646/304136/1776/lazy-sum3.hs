{-# OPTIONS -fbang-patterns #-}
--
-- The Computer Language Shootout
-- http://shootout.alioth.debian.org/
--
-- Contributed by Don Stewart
--
import Data.Char
import Data.ByteString.Base
import qualified Data.ByteString.Char8      as S
import qualified Data.ByteString.Lazy.Char8 as L

main = print . new 0 . L.toChunks =<< L.getContents

new  i []      = i
new !i (s:t:ts) | S.last s /= '\n' = new (add i s') ts'
  where
    (s',r)  = S.breakEnd (=='\n') s
    (r',rs) = S.break    (=='\n') t
    ts'     = S.concat [r,r',S.singleton '\n'] : unsafeTail rs : ts
new  i (s: ss) = new (add i s) ss

--
-- now jump into the fast path
--
add !i !s    | S.null s  = i
             | x == '-'  = sub i 0 xs
             | otherwise = pos i (parse x) xs
  where (x,xs) = uncons s

sub !i !n !t | y == '\n'  = add (i-n) ys
             | otherwise  = sub i n' ys
  where (y,ys) = uncons t
        n'     = parse y + 10 * n

pos !i !n !t | y == '\n' = add (i+n) ys
             | otherwise = pos i n' ys
  where (y,ys) = uncons t
        n'     = parse y + 10 * n

parse c  = ord c - ord '0'

uncons s = (w2c (unsafeHead s), unsafeTail s)
