/*
-- The Great Computer Language Shootout
-- http://shootout.alioth.debian.org/
--
-- contributed by Daniel Skiles 
-- modified by Rafal Rusin
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

int nsieve(int, vector<bool> &);

int main(int argc, char *argv[])
{
    int n = (argc == 2) ? atoi(argv[1]) : 2;
    int m;
    vector<bool> flags;

    m = (1<<n)*10000;
    flags.resize(m);
    cout << "Primes up to " << setw(8) << m << " " << setw(8) << nsieve(m, flags) << endl;

    m = (1<<n-1)*10000;
    flags.resize(m);
    cout << "Primes up to " << setw(8) << m << " " << setw(8) << nsieve(m, flags) << endl;

    m = (1<<n-2)*10000;
    flags.resize(m);
    cout << "Primes up to " << setw(8) << m << " " << setw(8) << nsieve(m, flags) << endl;

    return EXIT_SUCCESS;
}

int nsieve(int m, vector<bool> &isPrime)
{
    int count = 0;

    for (int i=2; i<=m; i++)
        isPrime[i] = true;

    for(int i=2; i<=m; i++){
        if(isPrime[i]){
            for(int k=i*2; k<=m; k+=i){
                isPrime[k] = false;
            }
            count++;
        }
    }
    return count;
}
