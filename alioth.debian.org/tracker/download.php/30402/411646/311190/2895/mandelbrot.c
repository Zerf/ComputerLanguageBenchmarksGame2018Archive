/*The Computer Language Benchmarks Game
  http://shootout.alioth.debian.org/

  contributed by Paolo Bonzini
  further optimized by Jason Garrett-Glaser
  pthreads added by Eckehard Berns
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

typedef double v2df __attribute__ ((vector_size(16))); /* vector of two doubles */
typedef int v4si __attribute__ ((vector_size(16))); /* vector of four ints */

#define NWORKERS 8

int w, h;
v2df zero = { 0.0, 0.0 };
v2df four = { 4.0, 4.0 };
v2df nzero;
double inverse_w;
double inverse_h;

char *whole_data;
int y_pick;
pthread_mutex_t y_mutex = PTHREAD_MUTEX_INITIALIZER;

static void *
worker(void *_args)
{
    char *data;
    double x, y;
    int i, bit_num;
    char byte_acc = 0;
    v4si mask;
    int *pmask;

    /* Detect endianness.  */
    mask = (v4si)nzero;
    pmask = (int *) &mask;
    if (pmask[1]) pmask++;

    for (;;) {
        pthread_mutex_lock(&y_mutex);
        y = y_pick;
        y_pick = y + 1;
        pthread_mutex_unlock(&y_mutex);
        if (y >= h)
            return NULL;
        data = &whole_data[(w >> 3) * (int)y];

        for(bit_num=0,x=0;x<w;x+=2)
        {
            v2df Crv = { x*inverse_w-1.5, (x+1.0)*inverse_w-1.5 };
            v2df Civ = { y*inverse_h-1.0, y*inverse_h-1.0 };
            v2df Zrv = { 0.0, 0.0 };
            v2df Ziv = { 0.0, 0.0 };
            v2df Trv = { 0.0, 0.0 };
            v2df Tiv = { 0.0, 0.0 };
            i = 0;

            do {
                v2df delta;
                Ziv = (Zrv*Ziv) + (Zrv*Ziv) + Civ;
                Zrv = Trv - Tiv + Crv;
                Trv = Zrv * Zrv;
                Tiv = Ziv * Ziv;

                /* sign bit zeroed if 4.0 - Trv - Tiv >= 0.0 (i.e. |Z| <= 4.0).  */
                delta = four - Trv - Tiv;
                mask = (v4si)delta & (v4si)nzero;
            } while (++i < 50 && !(pmask[0] & pmask[2]));

            byte_acc <<= 2;
            if(!pmask[0])
                byte_acc |= 0x02;
            if(!pmask[2])
                byte_acc |= 0x01;
            bit_num+=2;

            if(!(bit_num&7)) {
                data[(bit_num>>3) - 1] = byte_acc;
                byte_acc = 0;
            }
        }

        if(bit_num&7) {
            byte_acc <<= (8-w%8);
            bit_num += 8;
            data[bit_num>>3] = byte_acc;
            byte_acc = 0;
        }
    }
}


int main (int argc, char **argv)
{
    pthread_t ids[NWORKERS];
    int i;

    nzero = -zero;

    w = h = atoi(argv[1]);

    inverse_w = 2.0 / w;
    inverse_h = 2.0 / h;

    y_pick = 0;
    whole_data = malloc(w * (w >> 3));

    for (i = 0; i < NWORKERS; i++)
        pthread_create(&ids[i], NULL, worker, NULL);
    for (i = 0; i < NWORKERS; i++)
        pthread_join(ids[i], NULL);
    pthread_mutex_destroy(&y_mutex);

    printf("P4\n%d %d\n",w,h);
    fwrite(whole_data, h, w >> 3, stdout);

    free(whole_data);

    return 0;
}

