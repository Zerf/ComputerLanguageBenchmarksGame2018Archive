% The Computer Language Benchmarks Game
% http://shootout.alioth.debian.org/
% Contributed by Valentin Kraevskiy
% Should be compiled with: -H

:- module binarytrees.

:- interface.
:- import_module io.
:- pred main(io, io).
:- mode main(di, uo) is cc_multi.

:- implementation.
:- import_module int, string, list.
	
:- type tree(A) ---> nil; node(tree(A), A, tree(A)).

:- func newTree(int, int) = tree(int).
newTree(Item, Depth) =
    ( if Depth > 0
      then node(newTree(2 * Item - 1, Depth - 1),
                Item,
                newTree(2 * Item, Depth - 1))
      else nil).

:- func treeCheck(tree(int)) = int.

treeCheck(nil) = 0.
treeCheck(node(L, I, R)) = I + treeCheck(L) - treeCheck(R).

:- pred checkTree({int, int}, int, int).
:- mode checkTree(in, in, out) is det.

checkTree({Start, Depth}, !Check) :-
     !:Check = !.Check + treeCheck(newTree(Start, Depth))
                       + treeCheck(newTree(-Start, Depth)).

:- pred printTree(int, int, int, io, io).
:- mode printTree(in, in, in, di, uo) is cc_multi.

printTree(Depth, MinDepth, MaxDepth, !IO) :-
     ( if Depth =< MaxDepth
       then Max = 1 << (MaxDepth - Depth + MinDepth),
            Iterations = list.series({0, Depth},
                                     pred({X, _}::in) is semidet :- X < Max,
                                     func({X, D}) = {X + 1, D}),
            list.foldl(checkTree, Iterations, 0, Res),
            io.format("%i\t trees of depth %i\t check: %i\n",
                      [i(2 * Max), i(Depth), i(Res)], !IO),
            printTree(Depth + 2, MinDepth, MaxDepth, !IO)
       else !:IO = !.IO).
                               
main(!IO) :-
    MinDepth = 4,
    io.command_line_arguments(Args, !IO),
    ( Args = [I | _], to_int(I, N), N >= MinDepth + 2
    ; N = MinDepth + 2),
    io.format("stretch tree of depth %i\t check: %i\n",
              [i(N + 1), i(treeCheck(newTree(0, N + 1)))], !IO),
    LongLivedTree = newTree(0, N),   
    printTree(MinDepth, MinDepth, N, !IO),
    io.format("long lived tree of depth %i\t check: %i\n",
              [i(N), i(treeCheck(LongLivedTree))], !IO).


:- promise all [N] (newTree(0, N) = nil).


  