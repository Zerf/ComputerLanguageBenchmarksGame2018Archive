(*
 * The Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Ewan Mellor.
 *
 * To compile use
 *
 * ocamlopt -thread -noassert -unsafe -ccopt -O3 unix.cmxa threads.cmxa chameneos.ml
 *)


let empty  = -1
let red    = 0
let yellow = 1
let blue   = 2
let faded  = 3


let complement c1 c2 =
  if c1 = c2 then
    c1
  else
    3 - c1 - c2


let required =
  if Array.length Sys.argv <= 1 then
    100000
  else
    int_of_string Sys.argv.(1)


(* Access to total, place1, place2, and condition is allowed only under the
   protection of mutex.  The first thread to rendevous places its colour in
   place1, and the second thread places the _new_ colour in place2.  If both
   places are full, then an exchange is occurring.  The condition is used to
   signal the first thread once the second has arrived and written the new
   colour.  *)

let total = ref 0
let place1 = ref empty
let place2 = ref empty
let condition = Condition.create()
let mutex = Mutex.create()


(* The top-level loop for each thread. *)
let animal_loop colour' result' =
  let rec loop colour result =
    Mutex.lock mutex;
    let new_colour, new_result =
      if !total = required then
        (* Done. *)
        faded, result
      else if !place1 = empty then
        begin
          (* No-one is waiting here. *)
          
          place1 := colour;
          Condition.wait condition mutex;

          (* We have met with someone -- the new colour is in place2, and we must clear both places
             when we are done. *)
          let nc = !place2 in
          total := !total + 1;
          place1 := empty;
          place2 := empty;
          nc, result + 1
        end
      else if !place2 = empty then
        begin
          (* Someone is waiting, and I am the one to meet with them. *)
          
          let other_colour = !place1 in
          let nc = complement colour other_colour in
          place2 := nc;
          Condition.signal condition;
          nc, result + 1
        end
      else
        begin
          (* Two people are here -- I must release the lock so that they can complete. *)
          colour, result
        end
    in

    Mutex.unlock mutex;

    if new_colour = faded then
      new_result
    else
      loop new_colour new_result
  in
  result' := loop colour' 0


let animal colour =
  let result = ref 0 in
  (result, Thread.create (animal_loop colour) result)


let animals = List.map animal [blue; red; yellow; blue]


let _ =
  List.iter (fun (_, thread) -> Thread.join thread) animals;
  Printf.printf "%d\n" (List.fold_left (fun x (result, _) -> x + !result) 0 animals)
