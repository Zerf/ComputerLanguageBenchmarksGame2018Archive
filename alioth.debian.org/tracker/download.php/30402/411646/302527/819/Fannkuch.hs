--
-- The Great Computer Language Shootout
-- http://shootout.alioth.debian.org/
--
-- compile with:  ghc -O2 -o Fannkuch Fannkuch.hs
-- contributed by Greg Buchholz
-- modified by Mirko Rahn
--

import System(getArgs)

main :: IO ()
main = getArgs >>= \ (n:_) -> do
       putStr $ "Pfannkuchen(" ++ n ++ ") = "
       print $ maximum $ map fannkuch $ permutations [1..(read n)]

fannkuch :: [Int] -> Int
fannkuch (1:_) = 0
fannkuch (2:x:xs) = succ $ fannkuch $ x:2:xs
fannkuch (3:x:y:xs) = succ $ fannkuch $ y:x:3:xs
fannkuch (4:x:y:z:xs) = succ $ fannkuch $ z:y:x:4:xs
fannkuch (5:x:y:z:a:xs) = succ $ fannkuch $ a:z:y:x:5:xs
fannkuch (6:x:y:z:a:b:xs) = succ $ fannkuch $ b:a:z:y:x:6:xs
fannkuch (7:x:y:z:a:b:c:xs) = succ $ fannkuch $ c:b:a:z:y:x:7:xs
fannkuch (8:x:y:z:a:b:c:d:xs) = succ $ fannkuch $ d:c:b:a:z:y:x:8:xs
fannkuch (9:x:y:z:a:b:c:d:e:xs) = succ $ fannkuch $ e:d:c:b:a:z:y:x:9:xs
fannkuch xs@(x:_) = succ $ fannkuch $ (reverse $ take x xs) ++ (drop x xs)
fannkuch _ = error "upps"

permutations :: [Int] -> [[Int]]
permutations []     = [[]]
permutations (x:xs) = [ zs | ys <- permutations xs , zs <- everywhere x ys ]

everywhere :: Int -> [Int] -> [[Int]]
everywhere x []     = [[x]]
everywhere x (y:ys) = (x:y:ys) : [ y:zs | zs <- everywhere x ys ]