/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Greg Buchholz
   converted to D by Dave Fladebo
   modified by Knud Soerensen
   compile: dmd -O -inline -release mandelbrot.d
*/

import std.stdio, std.string;

void main(char[][] args) {
    char byte_acc = 0;
    int iter = 50,bignum=0;
    int size = args.length > 1 ? atoi(args[1]) : 1;
    double limit = 4;
    cdouble Z,C;

    writefln("P4\n%d %d",size,size);

    for(int y=0; y < size ;y++) {
        for(int x = 0; x < size; x++) {
           Z = 0.0+0.0i;
           C = (2*cast(double)x/size - 1.5)+(2i*cast(double)y/size - 1i);
	    
	   for(int k=0;k<iter;k++) {
              Z=Z*Z+C; 
              if(Z.re*Z.re+Z.im*Z.im > limit) break;
           }
	    
	   byte_acc = (byte_acc << 1);	
           if(!(Z.re*Z.re+Z.im*Z.im > limit)) byte_acc |=0x01;            
           
           if(++bignum== 8) {
                putc(byte_acc,stdout);
	        byte_acc = 0;
           	bignum=0;
           }
        }
	if(size%8) {
           byte_acc = byte_acc << (8-size%8);
	   putc(byte_acc,stdout);
           byte_acc = 0;
	}
    }
}