{-# OPTIONS -O2 -funbox-strict-fields #-}
{-
   The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Jeff Newbern
   Modified to fastest.hs by Chris Kuklewicz, 6 Jan 2006

   Uses random generation code derived from Simon Marlow and Einar
   Karttunen's "random" test entry.

   Compile with ghc --make filename.hs -o filename
-}

import Control.Monad.State
import Data.Char(chr,ord)
import Data.List(mapAccumL)
import Data.Word(Word8)
import Foreign
import System(getArgs)
import System.IO(stdout,hPutBuf)

type Base = Word8
type Sequence = [Base]
data BaseFrequency = BF !Base !Int 
data Seed = S !Int
type Pseudo a = StateT Seed IO a

instance Show BaseFrequency where
  show (BF b f) = (b2c b):" "++(show f)
  

toBF (b,f) = BF b f
c2b :: Char -> Word8
c2b = toEnum . ord
b2c :: Word8 -> Char
b2c = chr . fromEnum

alu :: String -- predefined sequence
alu = "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG" ++
      "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA" ++
      "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT" ++
      "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA" ++
      "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG" ++
      "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC" ++
      "AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA"

im = 139968 :: Double  -- 1/ (7.144490169181527e-6)
tweak = 1.01/im

iub :: [BaseFrequency]
iub = map toBF $ snd $ mapAccumL (\rt (c,f) -> (f+rt,(c2b c,round $ im *(f+rt)))) 0.0 $
      [ ('a', 0.27+tweak), ('c', 0.12-tweak), ('g', 0.12), ('t', 0.27+tweak), ('B', 0.02),
        ('D', 0.02-tweak), ('H', 0.02+tweak), ('K', 0.02), ('M', 0.02-tweak), ('N', 0.02+tweak),
        ('R', 0.02-tweak), ('S', 0.02), ('V', 0.02+tweak), ('W', 0.02-tweak), ('Y', 0.02) ]

homosapiens :: [BaseFrequency]
homosapiens =  map toBF $ snd $ mapAccumL (\rt (c,f) -> (f+rt,(c2b c,round $ im * (f+rt)))) 0.0 $
              [ ('a', 0.3029549426680), ('c', 0.1979883004921+tweak),
                ('g', 0.1975473066391), ('t', 0.3015094502008-tweak) ]

chooseBase :: [BaseFrequency] -> Int -> Base
chooseBase [(BF b _)]    _ = b
chooseBase ((BF b f):xs) p | p < f     = b
                           | otherwise = chooseBase xs p

prng :: Pseudo Seed
prng = let nextSeed (S s) = S $ (s * ia + ic) `mod` im
           im = 139968; ia = 3877; ic = 29573
       in do seed <- get
             let seed' = nextSeed seed
             put seed'
             return seed'

writeFastaHeader label title = liftIO $ putStrLn $ ">" ++ (label ++ (" " ++ title))

writeFasta label title sequence =
  do writeFastaHeader label title
     writeWrapped 60 sequence
  where writeWrapped _   []  = return ()
        writeWrapped len str = do let (s1,s2) = splitAt len str
                                  putStrLn s1
                                  writeWrapped len s2

--writeWrapped' :: Int -> Int -> (Int->Base) -> Pseudo ()
writeWrapped' wrap total trans = do
    buf <- liftIO $ mallocArray wrap
    let (full,end) = divMod total wrap
        fill 0 _   = return ()
        fill i ptr = do (S b) <- prng
                        liftIO (poke ptr (trans b))
                        fill (pred i) (advancePtr ptr 1)
        workFull = do fill wrap buf
                      liftIO (hPutBuf stdout buf wrap >> putStrLn "")
        workEnd = do fill end buf
                     liftIO (hPutBuf stdout buf end >> putStrLn "")
    replicateM_ full workFull >> if (end>0) then workEnd else return ()

writeWrapped = writeWrapped' 60

main = do args <- getArgs
          let n = if (null args) then (2500000::Int) else read (head args)
          writeFasta "ONE" "Homo sapiens alu" (take (2*n) (cycle alu))
          writeFastaHeader "TWO" "IUB ambiguity codes"
          seed' <- execStateT (writeWrapped (3*n) (chooseBase iub))  (S 42)
          writeFastaHeader "THREE" "Homo sapiens frequency"
          execStateT (writeWrapped (5*n) (chooseBase homosapiens)) seed'
