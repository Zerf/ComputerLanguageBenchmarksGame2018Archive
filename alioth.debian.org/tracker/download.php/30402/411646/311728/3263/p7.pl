#!/usr/bin/perl

# The Great Computer Language Shootout
# http://shootout.alioth.debian.org/
#
# Contributed by dmpk2k

use 5.010;
use POSIX;


$/ = '>';                           # change line delimiter to > instead of \n
<STDIN>;                            # skip first result


while (<STDIN>) {
  s/^(.*?)\n//;                     # chop off description and print it
  say '>', $1;

  tr{wsatugcyrkmbdhvnATUGCYRKMBDHVN\n>}               # translate sequence
    {WSTAACGRYMKVHDBNTAACGRYMKVHDBN}d;

  my $sequence   = reverse;
  my $iterations = ceil(length($sequence) / 60) - 1;

  for my $count (0 .. $iterations) {                  # print reverse complement
    say substr($sequence, $count * 60, 60);
  }
}
