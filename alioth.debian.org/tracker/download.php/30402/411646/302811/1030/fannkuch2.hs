{-  The Computer Language Shootout
    http://shootout.alioth.debian.org/
    compile: ghc -O3 -o fannkuch fannkuch.hs
    contributed by Josh Goldfoot
     permutations function translated from the C version by Heiner Marxen -}

import System(getArgs)
import Data.Int

main = do   [n] <- getArgs
            let p = permutations [1..(read n)]
            mapM putStrLn $ map ((concat).(map show)) $ take 30 p
            putStr $ "Pfannkuchen(" ++ n ++ ") = "
            print $ pfann p

pfann p = maximum (map numFlips p)

numFlips (1:xs) = 0
numFlips list = 1 + numFlips (mangle list)
 
permutations :: [Int8] -> [[Int8]]
permutations arry = arry : (permuteloop arry [1..n] 1)
   where n = fromIntegral (length arry)

permuteloop arry count r
   | r == length arry = []
   | count' !! r > 0 = arry' : (permuteloop arry' (reload r count') 1)
   | otherwise = permuteloop arry' count' (r+1)
   where count' = (take r count) ++ ((count !! r) - 1):(drop (r+1) count)
         arry' = rotate r arry

reload r count
   | r == 1 = count
   | otherwise = [1..r] ++ (drop r count)

mangle (2:x2:xs) = x2:2:xs
mangle (3:x2:x3:xs) = x3:x2:3:xs
mangle (4:x2:x3:x4:xs) = x4:x3:x2:4:xs
mangle (5:x2:x3:x4:x5:xs) = x5:x4:x3:x2:5:xs
mangle (6:x2:x3:x4:x5:x6:xs) = x6:x5:x4:x3:x2:6:xs
mangle (7:x2:x3:x4:x5:x6:x7:xs) = x7:x6:x5:x4:x3:x2:7:xs
mangle (8:x2:x3:x4:x5:x6:x7:x8:xs) = x8:x7:x6:x5:x4:x3:x2:8:xs
mangle (9:x2:x3:x4:x5:x6:x7:x8:x9:xs) = x9:x8:x7:x6:x5:x4:x3:x2:9:xs
mangle (10:x2:x3:x4:x5:x6:x7:x8:x9:x10:xs) = x10:x9:x8:x7:x6:x5:x4:x3:x2:10:xs
mangle list@(x:xs) = (reverse front) ++ back where (front,back) = splitAt (fromIntegral x) list

rotate 0 x = x
rotate 1 (x1:x2:xs) = x2:x1:xs
rotate 2 (x1:x2:x3:xs) = x2:x3:x1:xs
rotate 3 (x1:x2:x3:x4:xs) = x2:x3:x4:x1:xs
rotate 4 (x1:x2:x3:x4:x5:xs) = x2:x3:x4:x5:x1:xs
rotate 5 (x1:x2:x3:x4:x5:x6:xs) = x2:x3:x4:x5:x6:x1:xs
rotate 6 (x1:x2:x3:x4:x5:x6:x7:xs) = x2:x3:x4:x5:x6:x7:x1:xs
rotate 7 (x1:x2:x3:x4:x5:x6:x7:x8:xs) = x2:x3:x4:x5:x6:x7:x8:x1:xs
rotate 8 (x1:x2:x3:x4:x5:x6:x7:x8:x9:xs) = x2:x3:x4:x5:x6:x7:x8:x9:x1:xs
rotate 9 (x1:x2:x3:x4:x5:x6:x7:x8:x9:x10:xs) = x2:x3:x4:x5:x6:x7:x8:x9:x10:x1:xs
rotate r (x:xs) =
   begin ++ x:end
   where (begin, end) = splitAt r xs


