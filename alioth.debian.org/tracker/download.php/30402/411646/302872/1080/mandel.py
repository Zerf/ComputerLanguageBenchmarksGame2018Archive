# http://shootout.alioth.debian.org/
# Mandelbrot
# Adapted from Eiffel version by bearophile, Jan 9 2006

import psyco, sys

def mandel():
    chrd = dict((i, chr(i)) for i in xrange(256))
    size = int(sys.argv[1])
    print "P4"
    print size, size

    limit = 4.0
    iter = 50
    bit = 0x80
    bit_accu = 0
    gone = False
    for y in xrange(size):
        ci = 2.0 * y / size - 1.0

        for x in xrange(size):
            cr = 2.0 * x / size - 1.5

            zr = 0; zi = 0; pr = 0; pi = 0; i = 0

            for i in xrange(iter):
                zi = 2.0 * zr * zi + ci
                zr = pr - pi + cr
                pi = zi * zi
                pr = zr * zr
                if pi+pr > limit:
                    gone = True
                    break

            if gone:
                gone = False
            else:
                bit_accu |= bit

            if bit == 1:
                sys.stdout.write(chrd[bit_accu])
                bit_accu = 0
                bit = 0x80
            else:
                bit >>= 1

        if bit != 0x80:
            sys.stdout.write(chrd[bit_accu])
            bit_accu = 0
            bit = 0x80

psyco.bind(mandel)
mandel()