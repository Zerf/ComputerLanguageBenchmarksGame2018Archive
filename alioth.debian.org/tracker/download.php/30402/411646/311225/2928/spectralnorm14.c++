// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org/
//
// Original C contributed by Sebastien Loisel
// Conversion to C++ by Jon Harrop
// OpenMP parallelize by The Anh Tran

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <sched.h>
#include <omp.h>


#define	N_REG	2
#define	ALG		64

struct Param
{
	double* u;
	double* tmp;
	double* v;
	
	int N;
	int r_begin;
	int r_end;
};

__attribute__(( regparm(N_REG) ))
double (*eval_A)(int i, int j);

__attribute__(( regparm(N_REG) ))
double f_eval_A(int i, int j)
{
	// Original is: 1.0 / (i+j) * (i+j+1) /2 + i+1;
	int d = ( ((i+j) * (i+1+j)) >> 1 )  + i+1;
	return 1.0 / d;
}

void eval_A_times_u (Param &p)
{
	for (int i = p.r_begin, ie = p.r_end; i < ie; i++)
	{
		double sum = 0.0;
		for (int j = 0, je = p.N; j < je; j++)
			sum += p.u[j] * eval_A(i, j);
		p.tmp[i] = sum;
	}
}

void eval_At_times_u(Param &p)
{
	for (int i = p.r_begin, ie = p.r_end; i < ie; i++)
	{
		double sum = 0.0;
		for (int j = 0, je = p.N; j < je; j++)
			sum += p.tmp[j] * eval_A(j, i);
		p.v[i] = sum;
	}
}

void eval_AtA_times_u(Param &p)
{
	eval_A_times_u( p );
	#pragma omp barrier

	eval_At_times_u( p );
	#pragma omp barrier
}

int GetThreadCount()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	for (int i = 0; i < 8; i++)
	{
		if (CPU_ISSET(i, &cs))
			count++;
	}
	return count;
}

double spectral_game(int N )
{
	int nthreads = GetThreadCount();
	int chunk = N / nthreads;
	
	__attribute__((aligned(ALG)))	double	u	[N];
	__attribute__((aligned(ALG)))	double	tmp	[N];
	__attribute__((aligned(ALG)))	double	v	[N];
	
	double vBv	= 0.0;
	double vv	= 0.0;

	#pragma omp parallel default(shared) num_threads(nthreads)
	{
		#pragma omp for schedule(static)
		for (int i = 0; i < N; i++)
			u[i] = 1.0;

		int threadid	= omp_get_thread_num();

		Param my_param;
		my_param.tmp	= tmp;
		my_param.N		= N;
		
		// calculate each thread's working range [range1 .. range2) => static schedule here
		my_param.r_begin = threadid * chunk;
		my_param.r_end = (threadid < (nthreads -1)) ? (my_param.r_begin + chunk) : N;

		for (int ite = 0; ite < 10; ite++)
		{
			my_param.u = u;
			my_param.v = v;
			eval_AtA_times_u(my_param);

			my_param.u = v;
			my_param.v = u;
			eval_AtA_times_u(my_param);
		}

		#pragma omp for schedule(static) reduction( + : vBv, vv ) nowait
		for (int i = 0; i < N; i++)
		{
			vv	+= v[i] * v[i];
			vBv	+= u[i] * v[i];
		}
	} // end parallel region

	return sqrt( vBv/vv );
}


int main(int argc, char *argv[])
{
	int N = ((argc >= 2) ? atoi(argv[1]) : 2000);

	eval_A = f_eval_A;

	printf("%.9f\n", spectral_game(N));
	return 0;
}

