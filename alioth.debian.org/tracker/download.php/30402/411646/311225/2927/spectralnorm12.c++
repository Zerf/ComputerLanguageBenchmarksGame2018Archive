// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org/
//
// Original C contributed by Sebastien Loisel
// Conversion to C++ by Jon Harrop
// OpenMP parallelize by The Anh Tran

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <sched.h>
#include <omp.h>


#if defined(__x86_64__) || defined(_AMD64_)
	#define	N_REG	6
#else
	#define	N_REG	3
#endif

#define		ALG		64



__attribute__(( aligned(ALG), regparm(N_REG) ))
double (*eval_A)(int i, int j);

__attribute__(( regparm(N_REG) ))
double f_eval_A(int i, int j)
{
	// Original is: 1.0 / (i+j) * (i+j+1) /2 + i+1;
	int d = ( ((i+j) * (i+1+j)) >> 1 )  + i+1;
	return 1.0 / d;
}


__attribute__(( aligned(ALG), regparm(N_REG) ))
void (*eval_A_times_u) (const double u[], int inRange, double Au[], int outRange1, int outRange2);

__attribute__(( regparm(N_REG) ))
void f_eval_A_times_u (const double u[], int inRange, double Au[], int outRange1, int outRange2)
{
	for (int i = outRange1; i < outRange2; i++)
	{
		double sum = 0.0;
		for (int j = 0; j < inRange; j++)
			sum += u[j] * eval_A(i, j);
		Au[i] = sum;
	}
}


__attribute__(( aligned(ALG), regparm(N_REG) ))
void (*eval_At_times_u)(const double u[], int inRange, double Au[], int outRange1, int outRange2);

__attribute__(( regparm(N_REG) ))
void f_eval_At_times_u(const double u[], int inRange, double Au[], int outRange1, int outRange2)
{
	for (int i = outRange1; i < outRange2; i++)
	{
		double sum = 0.0;
		for (int j = 0; j < inRange; j++)
			sum += u[j] * eval_A(j, i);
		Au[i] = sum;
	}
}

__attribute__(( aligned(ALG), regparm(N_REG) ))
void (*eval_AtA_times_u)(const double u[], double AtAu[], double v[], int inRange, int range1, int range2);

__attribute__(( regparm(N_REG) ))
void f_eval_AtA_times_u(const double u[], double AtAu[], double v[], int inRange, int range1, int range2)
{
	eval_A_times_u( u, inRange, v, range1, range2 );
	#pragma omp barrier

	eval_At_times_u( v, inRange, AtAu, range1, range2 );
	#pragma omp barrier
}

int GetThreadCount()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	for (int i = 0; i < 8; i++)
	{
		if (CPU_ISSET(i, &cs))
			count++;
	}
	return count;
}

__attribute__(( regparm(N_REG) ))
double spectral_game(int N)
{
	__attribute__((aligned(ALG)))	double u[N];
	__attribute__((aligned(ALG)))	double tmp[N];
	__attribute__((aligned(ALG)))	double v[N];

	double vBv	= 0.0;
	double vv	= 0.0;


	#pragma omp parallel default(shared) num_threads(GetThreadCount())
	{
		#pragma omp for schedule(static)
		for (int i = 0; i < N; i++)
			u[i] = 1.0;

		int threadid      = omp_get_thread_num();
		int threadcount   = omp_get_num_threads();
		int chunk   = N / threadcount;

		// calculate each thread's working range [range1 .. range2) => static schedule here
		int myRange1 = threadid * chunk;
		int myRange2 = ( threadid < (threadcount -1) ) ? (myRange1 + chunk) : N;

		for (int ite = 0; ite < 10; ite++)
		{
			eval_AtA_times_u(u, v, tmp, N, myRange1, myRange2);
			eval_AtA_times_u(v, u, tmp, N, myRange1, myRange2);
		}

		#pragma omp for schedule(static) reduction( + : vBv, vv ) nowait
		for (int i = 0; i < N; i++)
		{
			vv	+= v[i] * v[i];
			vBv	+= u[i] * v[i];
		}
	} // end parallel region

	return sqrt( vBv/vv );
}


int main(int argc, char *argv[])
{
	int N = ((argc >= 2) ? atoi(argv[1]) : 2000);

	eval_A = f_eval_A;
	eval_A_times_u	= f_eval_A_times_u;
	eval_At_times_u	= f_eval_At_times_u;
	eval_AtA_times_u = f_eval_AtA_times_u;

	printf("%.9f\n", spectral_game(N));
	return 0;
}

