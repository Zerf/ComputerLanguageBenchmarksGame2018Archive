rem The Computer Language Shootout
rem http://shootout.alioth.debian.org/
rem contributed by Josh Goldfoot

function AckInt(x as integer, y as integer) as integer
   if x = 0 then return (y + 1)
   if y = 0 then return (AckInt(x-1, 1)) 
   return (AckInt(x-1, AckInt(x, y-1)))
end function

function Fib(n as integer) as integer
   if n < 2 then return 1
   return Fib(n-2) + Fib(n-1)
end function

function FibFP(n as double) as double
   if n < 2.0 then return 1.0
   return FibFP(n-2.0) + FibFP(n-1.0)
end function

function Tak(x as integer, y as integer, z as integer) as integer
   if y < x then return Tak( Tak(x-1, y, z), Tak(y-1, z, x), Tak(z-1, x, y) )
   return z
end function

function TakFP(x as double, y as double, z as double) as double
   if y < x then return TakFP( TakFP(x-1.0, y, z), TakFP(y-1.0, z, x), TakFP(z-1.0, x, y) )
   return z
end function

dim n as integer
n = valint(COMMAND$)
if n = 0 then n = 3
print "Ack(3,"; str(n+1) ;"): "; str(AckInt(3, n+1))
print "Fib(";str(28+n);".0): ";str(FibFP(28.0+n));".0"
print "Tak(";str(3*n);",";str(2*n);",";str(n);"): "; str(Tak(3*n, 2*n, n))
print "Fib(3): "; str(Fib(3))
print "Tak(3.0,2.0,1.0): "; str(TakFP(3.0, 2.0, 1.0));".0"


