%The Computer Language Benchmarks Game
%http://shootout.alioth.debian.org/

%contributed by Fredrik Svahn

%compile: erlc +native '+{hipe, [o3]}' sumcol.erl
%run: erl +T 9 +Mea min -noinput -run sumcol main foo < sumcol-input.txt 

%This program uses the port method to read stdin one line at a time. This is
%a lot more efficient than the old method.

-module(sumcol).
-export([main/1]).
-define(stdin,0).
-define(stdout,1).
-define(maxline,128).

main(_)->
    %Open stdin port, results will be sent to this process one line at a time
    Port = open_port({fd, ?stdin, ?stdout}, [{line, ?maxline}, eof]), 
    Sum = read8({0, go}),
    port_command(Port, integer_to_list(Sum) ++ "\n"), 
    halt(0).

%Main read loop. Read 8 lines at a time
read8({Sum, eof})-> Sum;
read8({Sum, _})-> read8(ln(ln(ln(ln(ln(ln(ln(ln({Sum, go}))))))))).

%Read one line (actually it is more like "wait until we receive one line from
%stdin or something else that another process would like to send to us") 
ln({Sum, eof}) -> {Sum, eof};
ln({Sum, _})   -> receive
		      {_, eof}         -> {Sum, eof};
		      {_,{_,{_,Line}}} -> {Sum + list_to_integer(Line), go};
		      _OtherMessage    -> {Sum, go}  %ignored in this case
		  end.

