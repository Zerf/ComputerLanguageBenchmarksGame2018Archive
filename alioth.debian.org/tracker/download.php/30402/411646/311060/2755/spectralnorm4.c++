// The Great Computer Language Shootout
// http://shootout.alioth.debian.org/
//
// Compile: g++ -O3 -fopenmp -msse2 -ffast-math -o spectralnorm spectralnorm.cpp
//
// Original C contributed by Sebastien Loisel
// Conversion to C++ by Jon Harrop
// Vectorizing & tweaking by The Anh Tran

//-------------------------------------------------------------------------------
#include <cmath>
#include <vector>
#include <iostream>
#include <iomanip>

// Use OpenMP. Supported from GCC >= v4.2.xx
// need "-fopenmp" flag when compile
#include <omp.h>

// how many thread???
#define NUM_THREADS 4

// comment the following define to use standard FP divide instruction
#define USE_FAST_DIV

using namespace std;
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
typedef float	v4f	__attribute__((vector_size(16)));
typedef double	v2d	__attribute__((vector_size(16)));

union v2du
{
	v2d		xmm;
	double	mem[2];
};
//-------------------------------------------------------------------------------



//-------------------------------------------------------------------------------
// original eval_A, for odd size array. Should be *never* called
__attribute__((const, nothrow)) 
inline
double eval_A(int i, int j) 
{
	int d = (i + j) * (i + j +1) / 2 + i + 1;
	return 1.0 / d; 
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
//	vectorized eval_A. Use reciprocal & Newton-Raphson instead of FP divide.
//	Input:	scalar type	i, j
//	Return:	vector type	1/[i, j]	1/[i, j+1]
//-------------------------------------------------------------------------------
__attribute__((const, nothrow)) 
inline
v2d eval_Aj(int i, int j)
{
	int d1 = (i + j		) * (i + j 		+1) / 2 + i + 1;
	int d2 = (i + j +1	) * (i + j +1	+1) / 2 + i + 1;

#ifdef USE_FAST_DIV

	// df = d1 d2 0 0	(float)
	v4f df	= {d1, d2};

	// reciprocal packed single precision (SP)
	// precision is 12 bit ~3rd digit after '.'
	// rdf = 1/d1 1/d2 Inf Inf	(float)
	v4f rdf	= __builtin_ia32_rcpps(df);

	// d		= d1 d2	(double)
	v2d d	= {d1, d2};

	// x0		= 1/d1 1/d2	(double)
	v2d x0	= __builtin_ia32_cvtps2pd(rdf); // convert packed SP to DP

	// 1st newton-raphson iteration, to improve precision
	// x0 = rcpof(d)
	// x1 = (2*x0) - (d * x0 * x0)
	// got precision at 7th digit after '.', which is as precise as FP divide (float type)
	v2d x1 = (x0 + x0) - (d * x0 * x0);
	
	// 2nd newton-raphson iteration
	// 14th digit after '.', which is more precise than game output
	// but not as precise as FP divide (double)
	x0 = x1;
	x1 = (x0 + x0) - (d * x0 * x0);

	// 3rd iteration has *very small* speed improvement than using FP divide (double type)
	// precision same as FP divide (double)
	//x0 = x1;
	//x1 = (x0 + x0) - (d * x0 * x0);
	return x1;

#else

	// FP divide here
	v2d r = {d1, d2};
	v2d v1 = {1.0, 1.0};
	return v1 / r; 

#endif
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
//	vectorized eval_A. Use reciprocal & Newton-Raphson instead of FP divide.
//	Input:	i, j
//	Return:	1/[i, j]	1/[i +1, j]
//-------------------------------------------------------------------------------
__attribute__((const, nothrow)) 
inline
v2d eval_Ai(int i, int j)
{
	int d1 = (i 		+ j) * (i 		+ j +1) / 2 + i 		+ 1;
	int d2 = (i +1		+ j) * (i +1	+ j +1) / 2 + (i +1)	+ 1;

#ifdef USE_FAST_DIV

	v4f df	= {d1, d2};
	v4f rdf	= __builtin_ia32_rcpps(df);

	v2d d	= {d1, d2};
	v2d x0	= __builtin_ia32_cvtps2pd(rdf);

	// 1st iteration
	v2d x1	= (x0 + x0) - (d * x0 * x0);

	// 2nd iteration
	x0 = x1;
	x1 = (x0 + x0) - (d * x0 * x0);

	return x1;

#else

	// FP divide
	v2d r = {d1, d2};
	v2d v1 = {1.0, 1.0};
	return v1 / r; 

#endif
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
__attribute__((nothrow)) 
void eval_A_times_u (const double u[], int inRange, double Au[], int outRange1, int outRange2)
{
	const int N2 = inRange/2;
	const v2d* pU = (const v2d*)u;
	v2du sum;

	for (int i =outRange1; i < outRange2; i++)
	{
		sum.xmm ^= sum.xmm; // sum =0

		int j;
		for (j =0; j < N2; j++) 
			sum.xmm += eval_Aj( i, j *2 ) * pU[ j ];

		Au[i] = sum.mem[0] + sum.mem[1];

		// odd size array, *should be never*
		for (j = j*2; __builtin_expect(j < inRange, 0); j++) 
			Au[i] += eval_A( i, j ) * u[j];
	}
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
__attribute__((nothrow)) 
void eval_At_times_u(const double u[], int inRange, double Au[], int outRange1, int outRange2)
{
	const int N2 = inRange/2;
	const v2d* pU = (const v2d*)u;
	v2du sum;

	for (int i =outRange1; i < outRange2; i++)
	{
		sum.xmm ^= sum.xmm; // sum =0

		int j;
		for (j =0; j < N2; j++) 
			sum.xmm += eval_Ai( j *2, i ) * pU[ j ];

		Au[i] = sum.mem[0] + sum.mem[1];

		// odd size array, *should be never*
		for (j = j*2; __builtin_expect(j < inRange, 0); j++) 
			Au[i] += eval_A(j,i) * u[j];
	}
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
// This function is executed by N threads.
// Each thread will fill its chunk only [range1 .. range2) 
//-------------------------------------------------------------------------------
void eval_AtA_times_u(const double u[], double AtAu[], double v[], int inRange, int range1, int range2)
{
	eval_A_times_u( u, inRange, v, range1, range2 ); 

	// synchronize at completion of array "v" 
	#pragma omp barrier

	eval_At_times_u( v, inRange, AtAu, range1, range2 ); 

	// syn on array AtAu
	#pragma omp barrier
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
void spectral_game(double u[], double v[], int N, double &vBv, double &vv)
{
	__attribute__((aligned(16))) double tmp[N];

	const int N2	= N/2;
	v2d* pU		= (v2d*)u;
	v2d* pV		= (v2d*)v;

	v2d one		= {1.0, 1.0};
	v2d sumB	= {0, 0};
	v2d sumv		= {0, 0};

	// sharing var "i" for odd size array
	int i;

	#pragma omp parallel default(shared) num_threads(NUM_THREADS)
	{
		// multi thread filling
		#pragma omp for		\
			schedule(static)	\
			lastprivate(i)
		for (i = 0; i < N2; i++)
			pU[ i ] = one;

		#pragma omp master
		{
			// odd size array
			for (i = i *2; __builtin_expect(i < N, 0); i++)
				u[ i ] = 1.0;
		}

		// this block will be executed by NUM_THREADS
		// variable declared in this block is private for each thread
		{
			int threadcount	= omp_get_num_threads();
			int threadid		= omp_get_thread_num();

			int chunk	= N / threadcount;

			// calculate each thread's working range [range1 .. range2)
			int myRange1 = threadid * chunk;
			int myRange2 = ( threadid < (threadcount -1) ) ? (threadid +1) * chunk : N;

			//cout << "tid " << threadid << " num " << threadcount << " r1 " << myRange1 << " r2 " << myRange2 << endl;

			for (int ite =0; ite < 10; ite++) 
			{
				eval_AtA_times_u(u, v, tmp, N, myRange1, myRange2);
				eval_AtA_times_u(v, u, tmp, N, myRange1, myRange2);
			}
		}

		// multi thread adding
		#pragma omp for		\
			schedule(static)	\
			lastprivate(i)		\
			reduction( + : sumB, sumv )
		for (i =0; i < N2; i++) 
		{
			sumB += pU[ i ] * pV[ i ];
			sumv += pV[ i ] * pV[ i ];
		}
	} // end parallel region

	// final sum
	v2du sb = {sumB};
	v2du sv = {sumv};

	vBv	= sb.mem[0] + sb.mem[1];
	vv	= sv.mem[0] + sv.mem[1];

	// odd size array
	for (i = i *2; __builtin_expect(i < N, 0); i++)
	{
		vBv	+= u[i] * v[i]; 
		vv	+= v[i] * v[i]; 
	}
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	int N = ((argc == 2) ? atoi(argv[1]) : 2000);
	
	__attribute__((aligned(16))) double u[N];
	__attribute__((aligned(16))) double v[N];

	// main calculation
	double vBv, vv;
	spectral_game(u, v, N, vBv, vv);

	cout << setprecision(10) << sqrt( vBv / vv ) << endl;

	return 0;
}
//-------------------------------------------------------------------------------

