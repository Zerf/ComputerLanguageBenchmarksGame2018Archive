// The Great Computer Language Shootout
// http://shootout.alioth.debian.org/
//
// Compile: g++ -O3 -msse2 -ffast-math -o spectralnorm spectralnorm.cpp
//
// Original C contributed by Sebastien Loisel
// Conversion to C++ by Jon Harrop
// Vectorizing & tweaking by The Anh Tran

//-------------------------------------------------------------------------------
#include <cmath>
#include <vector>
#include <iostream>
#include <iomanip>

// comment the following define to use standard FP divide instruction
#define USE_FAST_DIV

using namespace std;
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
typedef float	v4f	__attribute__((vector_size(16)));
typedef double	v2d	__attribute__((vector_size(16)));

union v2du
{
	v2d		xmm;
	double	mem[2];
};
//-------------------------------------------------------------------------------



//-------------------------------------------------------------------------------
// original eval_A, for odd size array. Should be *never* called
__attribute__((const, nothrow)) 
inline
double eval_A(int i, int j) 
{
	int d = (i + j) * (i + j +1) / 2 + i + 1;
	return 1.0 / d; 
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
//	vectorized eval_A. Use reciprocal & Newton-Raphson instead of FP divide.
//	Input:	scalar type	i, j
//	Return:	vector type	1/[i, j]	1/[i, j+1]
//-------------------------------------------------------------------------------
__attribute__((const, nothrow)) 
inline
v2d eval_Aj(int i, int j)
{
	int d1 = (i + j		) * (i + j 		+1) / 2 + i + 1;
	int d2 = (i + j +1	) * (i + j +1	+1) / 2 + i + 1;

#ifdef USE_FAST_DIV

	// df = d1 d2 0 0	(float)
	v4f df	= {d1, d2};

	// reciprocal packed single precision (SP)
	// precision is 12 bit ~3rd digit after '.'
	// rdf = 1/d1 1/d2 Inf Inf	(float)
	v4f rdf	= __builtin_ia32_rcpps(df);

	// d		= d1 d2	(double)
	v2d d	= {d1, d2};

	// x0		= 1/d1 1/d2	(double)
	v2d x0	= __builtin_ia32_cvtps2pd(rdf); // convert packed SP to DP

	// 1st newton-raphson iteration, to improve precision
	// x0 = rcpof(d)
	// x1 = (2*x0) - (d * x0 * x0)
	// got precision at 7th digit after '.', which is as precise as FP divide (float type)
	v2d x1 = (x0 + x0) - (d * x0 * x0);
	
	// 2nd newton-raphson iteration
	// 14th digit after '.', which is more precise than game output
	// but not as precise as FP divide (double)
	x0 = x1;
	x1 = (x0 + x0) - (d * x0 * x0);

	// 3rd iteration has *very small* speed improvement than using FP divide (double type)
	// precision same as FP divide (double)
	//x0 = x1;
	//x1 = (x0 + x0) - (d * x0 * x0);
	return x1;

#else

	// FP divide here
	v2d r = {d1, d2};
	v2d v1 = {1.0, 1.0};
	return v1 / r; 

#endif
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
//	vectorized eval_A. Use reciprocal & Newton-Raphson instead of FP divide.
//	Input:	i, j
//	Return:	1/[i, j]	1/[i +1, j]
//-------------------------------------------------------------------------------
__attribute__((const, nothrow)) 
inline
v2d eval_Ai(int i, int j)
{
	int d1 = (i 		+ j) * (i 		+ j +1) / 2 + i 		+ 1;
	int d2 = (i +1		+ j) * (i +1	+ j +1) / 2 + (i +1)	+ 1;

#ifdef USE_FAST_DIV

	v4f df	= {d1, d2};
	v4f rdf	= __builtin_ia32_rcpps(df);

	v2d d	= {d1, d2};
	v2d x0	= __builtin_ia32_cvtps2pd(rdf);

	// 1st iteration
	v2d x1	= (x0 + x0) - (d * x0 * x0);

	// 2nd iteration
	x0 = x1;
	x1 = (x0 + x0) - (d * x0 * x0);

	return x1;

#else

	// FP divide
	v2d r = {d1, d2};
	v2d v1 = {1.0, 1.0};
	return v1 / r; 

#endif
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
__attribute__((nothrow)) 
void eval_A_times_u (const double u[], int inRange, double Au[])
{
	const int N2 = inRange/2;
	const v2d* pU = (const v2d*)u;
	v2du sum;

	for (int i =0; i < inRange; i++)
	{
		sum.xmm ^= sum.xmm; // sum =0

		int j;
		for (j =0; j < N2; j++) 
			sum.xmm += eval_Aj( i, j *2 ) * pU[ j ];

		Au[i] = sum.mem[0] + sum.mem[1];

		// odd size array, *should be never*
		for (j = j*2; __builtin_expect(j < inRange, 0); j++) 
			Au[i] += eval_A( i, j ) * u[j];
	}
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
__attribute__((nothrow)) 
void eval_At_times_u(const double u[], int inRange, double Au[])
{
	const int N2 = inRange/2;
	const v2d* pU = (const v2d*)u;
	v2du sum;

	for (int i =0; i < inRange; i++)
	{
		sum.xmm ^= sum.xmm; // sum =0

		int j;
		for (j =0; j < N2; j++) 
			sum.xmm += eval_Ai( j *2, i ) * pU[ j ];

		Au[i] = sum.mem[0] + sum.mem[1];

		// odd size array, *should be never*
		for (j = j*2; __builtin_expect(j < inRange, 0); j++) 
			Au[i] += eval_A(j,i) * u[j];
	}
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
void eval_AtA_times_u(const double u[], double AtAu[], double v[], int inRange)
{ 
	eval_A_times_u( u, inRange, v ); 
	eval_At_times_u( v, inRange, AtAu ); 
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
void spectral_game(double u[], double v[], int N, double &vBv, double &vv)
{
	__attribute__((aligned(16))) double tmp[N];

	const int N2 = N/2;
	v2d* pU = (v2d*)u;
	v2d* pV = (v2d*)v;

	// fill 1.0 to u[N]
	v2d fill_1 = {1.0, 1.0};
	int i;
	for (i = 0; i < N2; i++)
		pU[ i ] = fill_1;

	// odd size array
	for (i = i *2; __builtin_expect(i < N, 0); i++)
		u[ i ] = 1.0;


	for (i =0; i < 10; i++) 
	{
		eval_AtA_times_u(u, v, tmp, N);
		eval_AtA_times_u(v, u, tmp, N);
	}


	v2du sumB, sumv;
	sumB.xmm ^= sumB.xmm;	// sumB = 0
	sumv.xmm = sumB.xmm;	// sumv = 0

	for (i =0; i < N2; i++) 
	{
		sumB.xmm += pU[ i ] * pV[ i ];
		sumv.xmm += pV[ i ] * pV[ i ];
	}

	vBv	= sumB.mem[0] + sumB.mem[1];
	vv	= sumv.mem[0] + sumv.mem[1];

	// odd size array
	for (i = i *2; __builtin_expect(i < N, 0); i++)
	{
		vBv += u[i] * v[i]; 
		vv += v[i] * v[i]; 
	}
}
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	int N = ((argc == 2) ? atoi(argv[1]) : 2000);
	
	__attribute__((aligned(16))) double u[N];
	__attribute__((aligned(16))) double v[N];

	// main calculation
	double vBv, vv;
	spectral_game(u, v, N, vBv, vv);

	cout << setprecision(10) << sqrt( vBv / vv ) << endl;

	return 0;
}
//-------------------------------------------------------------------------------

