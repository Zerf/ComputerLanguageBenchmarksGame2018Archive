// The Great Computer Language Shootout
// http://shootout.alioth.debian.org/
//
// Compile: g++ -O3 -fopenmp -msse2 -ffast-math -mtune=native -o spectralnorm spectralnorm.cpp
//
// Original C contributed by Sebastien Loisel
// Conversion to C++ by Jon Harrop
// OpenMP parallelize by The Anh Tran

//-------------------------------------------------------------------------------
#include <cmath>
#include <vector>
#include <iostream>
#include <iomanip>

// Use OpenMP. Supported from GCC >= v4.2.xx
// need "-fopenmp" flag when compile
#include <omp.h>

// how many thread???
#define NUM_THREADS 4

using namespace std;
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
__attribute__((const, nothrow)) 
inline
double eval_A(int i, int j) 
{
	int d = (i + j) * (i + j +1) / 2 + i + 1;
	return 1.0 / d; 
}
//-------------------------------------------------------------------------------



//-------------------------------------------------------------------------------
__attribute__((nothrow)) 
inline
void eval_A_times_u (const double u[], int inRange, double Au[], int outRange1, int outRange2)
{
	for (int i =outRange1; i < outRange2; i++)
	{
		Au[i] = 0.0;
		for (int j =0; j < inRange; j++) 
			Au[i] += eval_A( i, j ) * u[j];
	}
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
__attribute__((nothrow)) 
inline
void eval_At_times_u(const double u[], int inRange, double Au[], int outRange1, int outRange2)
{
	for (int i =outRange1; i < outRange2; i++)
	{
		Au[i] = 0.0;
		for (int j =0; j < inRange; j++) 
			Au[i] += eval_A(j,i) * u[j];
	}
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
// This function is executed by N threads.
// Each thread will fill its chunk only [range1 .. range2) 
//-------------------------------------------------------------------------------
__attribute__((nothrow)) 
inline
void eval_AtA_times_u(const double u[], double AtAu[], double v[], int inRange, int range1, int range2)
{
	eval_A_times_u( u, inRange, v, range1, range2 ); 

	// synchronize at completion of array "v" before going to fill AtAu. 
	// Because each element of AtAu reading all elements of "v"
	#pragma omp barrier

	eval_At_times_u( v, inRange, AtAu, range1, range2 ); 

	// syn on array AtAu
	#pragma omp barrier
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
void spectral_game(double u[], double v[], int N, double &vBv, double &vv)
{
	__attribute__((aligned(16))) double tmp[N];

	double sumB	= 0.0;
	double sumv	= 0.0;

	#pragma omp parallel default(shared) num_threads(NUM_THREADS)
	{
		// multi thread filling
		#pragma omp for		\
			schedule(guided)
		for (int i = 0; i < N; i++)
			u[ i ] = 1.0;

		// this block will be executed by NUM_THREADS
		// variable declared in this block is private for each thread
		{
			int threadcount	= omp_get_num_threads();
			int threadid		= omp_get_thread_num();

			int chunk	= N / threadcount;

			// calculate each thread's working range [range1 .. range2)
			int myRange1 = threadid * chunk;
			int myRange2 = ( threadid < (threadcount -1) ) ? (threadid +1) * chunk : N;

			//cout << "tid " << threadid << " num " << threadcount << " r1 " << myRange1 << " r2 " << myRange2 << endl;

			for (int ite =0; ite < 10; ite++) 
			{
				eval_AtA_times_u(u, v, tmp, N, myRange1, myRange2);
				eval_AtA_times_u(v, u, tmp, N, myRange1, myRange2);
			}
		}

		// multi thread adding
		#pragma omp for		\
			schedule(guided)	\
			reduction( + : sumB, sumv )
		for (int i =0; i < N; i++) 
		{
			sumB += u[ i ] * v[ i ];
			sumv += v[ i ] * v[ i ];
		}
	} // end parallel region

	vBv	= sumB;
	vv	= sumv;
}
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	int N = ((argc == 2) ? atoi(argv[1]) : 2000);
	
	__attribute__((aligned(16))) double u[N];
	__attribute__((aligned(16))) double v[N];

	// main calculation
	double vBv, vv;
	spectral_game(u, v, N, vBv, vv);

	cout << setprecision(10) << sqrt( vBv / vv ) << endl;

	return 0;
}
//-------------------------------------------------------------------------------

