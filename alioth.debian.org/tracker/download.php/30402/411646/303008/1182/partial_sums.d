/*
** The Great Computer Language Shootout
** http://shootout.alioth.debian.org/
** contributed by Isaac Freeman 
** adapted from Mike Palle's gcc version
**
** compile with:
**   dmd -O (-inline) -w -profile
*/

import std.string;
import std.math;

int main( char[][] argv )
{
  int k, n = cast(int)atoi(argv[1]);
  double sum = 0.0; // temp storage; derefencing and array lookups would slow things down.
  double[9] sums; // just keep numbers for now, it's faster to print them all later

  for (k = 0; k <= n; k++) {  /* pow(2.0/3.0, kd) inlined */
    double x = 1.0, q = 2.0/3.0;
    int j = k;
    for (;;) { if (j & 1) x *= q; if ((j >>= 1) == 0) break; q = q*q; }
    sum += x;
  }
  sums[0] = sum;

  sum = 0.0;
  for (k = 1 ; k <= n; k++) sum += 1/sqrt(cast(double)k);  /* aka pow(kd, -0.5) */
  sums[1] = sum;

  sum = 0.0;
  for (k = 1; k <= n; k++) sum += 1.0/(cast(double)k*(cast(double)k+1.0));
  sums[2] = sum;

  sum = 0.0;
  for (k = 1; k <= n; k++) {
    double sk = sin(cast(double)k);
    sum += 1.0/(cast(double)k*cast(double)k*cast(double)k*sk*sk);
  }
  sums[3] = sum;

  sum = 0.0;
  for (k = 1; k <= n; k++) {
    double ck = cos(cast(double)k);
    sum += 1.0/((cast(double)k*cast(double)k)*cast(double)k*ck*ck);
  }
  sums[4] = sum;

  sum = 0.0;
  for (k = 1; k <= n; k++) sum += 1.0/cast(double)k;
  sums[5] = sum;

  sum = 0.0;
  for (k = 1; k <= n; k++) sum += 1.0/(cast(double)k*cast(double)k);
  sums[6] = sum;

  sum = 0.0;
  for (k = 1; k <= n-1; k += 2) sum += 1.0/cast(double)k;
  for (k = 2; k <= n; k += 2) sum -= 1.0/cast(double)k;
  sums[7] = sum;

  sum = 0.0;
  for (k = 1; k <= 2*n-1; k += 4) sum += 1.0/cast(double)k;
  for (k = 3; k <= 2*n; k += 4) sum -= 1.0/cast(double)k;
  sums[8] = sum;


  printf("%.9f\t(2/3)^k\n", sums[0]);
  printf("%.9f\tk^-0.5\n", sums[1]);
  printf("%.9f\t1/k(k+1)\n", sums[2]);
  printf("%.9f\tFlint Hills\n", sums[3]);
  printf("%.9f\tCookson Hills\n", sums[4]);
  printf("%.9f\tHarmonic\n", sums[5]);
  printf("%.9f\tRiemann Zeta\n", sums[6]);
  printf("%.9f\tAlternating Harmonic\n", sums[7]);
  printf("%.9f\tGregory\n", sums[8]);

  return 0;
}


