"* The Computer Language Shootout
    http://shootout.alioth.debian.org/
    contributed by Carlo Teixeira *"!

Object subclass: #SumFile
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Shootout'!

SumFile class
	instanceVariableNames: ''!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!


!SumFile class methodsFor: 'benchmark scripts'!

sumcol
	| sum |
	sum := 0.
	Tests stdinSpecial 
		linesDo: [:line | sum := sum + (Integer readFrom: line readStream radix: 10)].
	(Tests stdout)
		print: sum;
		cr.
	^''! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

