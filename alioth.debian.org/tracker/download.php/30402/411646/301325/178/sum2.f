\ sumcol.gforth
\ Submitted by Ian Osgood

: sumcol ( -- n ) 0
  begin  0. pad dup 80 stdin read-line throw
  while  over c@ '- =
         if   1 /string >number 2drop d>s -
         else           >number 2drop d>s +
         then
  repeat 2drop 2drop ;

sumcol 1 u.r cr bye
