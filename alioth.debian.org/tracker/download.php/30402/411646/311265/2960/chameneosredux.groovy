/* The Computer Language Benchmarks Game
   http://shootout.alioth.debian.org/

   contributed by Jochen Hinrichsen
*/

class Pair {
	def colour
    def sameId
}

class MeetingPlace {

     def meetingsLeft
     def firstColour = null
     def firstId = 0
     def current

     public Pair meet(id, c) {
         def newPair
         synchronized (this) {
             if (meetingsLeft == 0) {
                 return null
             } else {
                 if (firstColour == null) {
                     firstColour = c
                     firstId = id
                     current = new Future<Pair>()
                 } else {
                     def newColour = c.map(firstColour)
                     current.setItem(new Pair(sameId: (id == firstId), colour: newColour))
                     firstColour = null
                     meetingsLeft--
                 }
                 newPair = current
             }
         }
         newPair.getItem()
     }
}

class Future<T> {

     private volatile T t

     public T getItem() {
         while (t == null) {
             Thread.yield()
         }
         return t
     }

     // no synchronization necessary as assignment is atomic
     public void setItem(T t) {
         this.t = t
     }
 }

enum Colour {
	
    blue, red, yellow

    /*
     * Groovy 1.5.6, 1.5.7 and 1.6 beta2 have a problem with static initializers, all values are set to null. As a workaround, the complimentary colors
     * are set in main().
     */
	static def mapping

    def map(c2) {
    	mapping[ordinal()][c2.ordinal()]
    }
}

class Creature implements Runnable {

    def place
    def count = 0
    def sameCount = 0
    def colour
    def id = System.identityHashCode(this)

    public void run() {
        while (true) {
            def p = place.meet(id, colour)
            if (p == null) break
            colour = p.colour
            if (p.sameId) {
                sameCount++
            }
            count++
        }
    }

    public String toString() {
        return String.valueOf(count) + new Number(n: sameCount)
    }
}

class Number {
	
	def n
	
    def DIGITS = [ ' zero', ' one', ' two', ' three', ' four', ' five', ' six', ' seven', ' eight', ' nine' ]

    String toString() {
        def s = ''
        String.valueOf(n).each() {
            def i = it as int 
            // Compensate for different groovy versions: 1.5.6 produces 49, 1.5.7 produces 1
            i -= '0' as int
            s += DIGITS[i]
        }
        s
    }
}

public class chameneosredux {

	static def DEFAULT_N = 600

    def n

    def run(Colour...colours) {
        def place = new MeetingPlace(meetingsLeft: n)
        def creatures = []
        colours.eachWithIndex() { c, i ->
            print " ${c}"
            creatures[i] = new Creature(colour: c, place: place)
        }
        println()
        def threads = []
        colours.eachWithIndex() { c, i ->
        	def t = new Thread(creatures[i])
        	threads << t
        	t.start()
        }
        threads.each() {
            try {
                it.join();
            } catch (InterruptedException e) {
            	// NOP
            }
        }

        def total = 0;
        creatures.each() {
            println(it)
            total += it.count
        }
        println(new Number(n: total))
        println()
    }

    def printColours() {
        [[ Colour.blue, Colour.blue]
        , [ Colour.blue, Colour.red ]
        , [ Colour.blue, Colour.yellow ]
        , [ Colour.red, Colour.blue ]
        , [ Colour.red, Colour.red ]
        , [ Colour.red, Colour.yellow ]
        , [ Colour.yellow, Colour.blue ]
        , [ Colour.yellow, Colour.red ]
        , [ Colour.yellow, Colour.yellow ]].each() {
        	println "${it[0]} + ${it[1]} -> ${it[0].map(it[1])}"
        }
    }

    public static void main(String[] args) {
        try {
        	def start = System.currentTimeMillis()

        	Colour.mapping = [
                                [ Colour.blue,   Colour.yellow, Colour.red    ]
                              , [ Colour.yellow, Colour.red,    Colour.blue   ]
                              , [ Colour.red,    Colour.blue,   Colour.yellow ]
            ]

        	def arg0 = DEFAULT_N
        	try {
        		arg0 = Integer.parseInt(args[0])
        	} catch (Exception xc) {
        		// NOP
        	}
            def cr = new chameneosredux(n: arg0)
            cr.printColours()
            println()
            cr.run(Colour.blue, Colour.red, Colour.yellow)
            cr.run(Colour.blue, Colour.red, Colour.yellow, Colour.red, Colour.yellow,
                   Colour.blue, Colour.red, Colour.yellow, Colour.red, Colour.blue)
            def stop = System.currentTimeMillis()
            println "Runtime: ${stop -start} ms"
            System.exit(0)
        } catch (Exception xc) {
            System.err.println("Exception occured: " + xc)
            xc.printStackTrace()
            System.exit(1)
        }
    }
}
