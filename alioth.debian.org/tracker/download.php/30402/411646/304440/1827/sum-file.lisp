(declaim (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0) (space 0)))
(let ((j 0))
  (handler-case
    (do ((i (parse-integer (read-line)) (parse-integer (read-line))))
        (())
      (incf j i))
    (end-of-file (x) (princ j))))
