#!/usr/bin/python
# http://shootout.alioth.debian.org/

# submitted by Sokolov Yura
# changed from Ian Osgood's version

import sys


def gen_freq(seq, length):
	n,t = len(seq)+1-length, {}

	for f in xrange(length):
		for i in xrange(f,n,length):
			s = seq[i:i+length]
			t[s] = t.setdefault(s,0)+1

	return n,t


def sort_seq(seq, length):
	n,t = gen_freq(seq, length)
	l = zip(t.values(), t.keys())
	l.sort(lambda (v1,k1),(v2,k2): v1==v2 and cmp(k1,k2) or cmp(v2,v1))
	print '\n'.join(["%s %.3f" % (s,100.0*count/n) for count,s in l])
	print


def find_seq(seq, s):
	n,t = gen_freq(seq, len(s))
	print "%d\t%s" % (s in t and t[s] or 0, s)


seq = []
add_line = seq.append
for line in sys.stdin:
	if line.startswith(">THREE"):
		break
for line in sys.stdin:
	if line[0] in ">;":
		break
	add_line(line[:-1])

seq = ''.join(seq).upper()

sort_seq(seq, 1)
sort_seq(seq, 2)
find_seq(seq, "GGT")
find_seq(seq, "GGTA")
find_seq(seq, "GGTATT")
find_seq(seq, "GGTATTTTAATT")
find_seq(seq, "GGTATTTTAATTTATAGT")

