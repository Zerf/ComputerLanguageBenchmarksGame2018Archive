program pfannkuchen;

type
    TIntegerArray = Array[0..99] of longint;

function fannkuch(n : longint): longint;
var
   perm, perm1, count: TIntegerArray;
   print30, m, r, j, i, k, temp, mostFlips, flips, perm0: longint;
begin
   print30 := 0;
   mostFlips := 0;
   m := n - 1;
   for i := 0 to m do
       perm1[i] := i;
   r := n;
   repeat
      if print30 < 30 then
      begin
         for i := 0 to m do
            write(perm1[i] + 1);
         writeln;
         inc(print30);
      end;
      while r <> 1 do
      begin
         count[r-1] := r;
         dec(r);
      end;
      if not ((perm1[0]=0) or (perm1[m]=m)) then
      begin
         for i := 0 to m do
            perm[i] := perm1[i];
         flips := 0;
         k := perm[0];
         repeat
           i:=1;
	   j:=k-1;
           while i<j do
             begin
               temp := perm[i];
               perm[i] := perm[j];
               inc(i);
               perm[j]:= temp;
               dec(j);
             end;
           inc(flips);
           j:=perm[k];
           perm[k]:=k;
           k:=j;
         until k = 0;
         if flips > mostFlips then
            mostFlips := flips;
      end;
      while true do
      begin
         if r = n then
         begin
            fannkuch := mostFlips;
            exit;
         end
         else
         begin
            perm0 := perm1[0];
            i := 0;
            while i < r do
            begin
               k := i + 1;
               perm1[i] := perm1[k];
               i := k;
            end;
            perm1[r] := perm0;
            dec(count[r]);
            if count[r] > 0 then
              break;
            inc(r);
         end;
      end;
   until false;
end;

var
   n, Code, answer : integer;

begin
   if paramCount() = 1
      then Val (ParamStr (1),n,Code)
   else n := 7;
   answer := fannkuch(n);
   writeln('Pfannkuchen(',n,') = ', answer);
end.


