-- Original by Jeff Newbern
-- Uses random generation code derived from Simon Marlow and Einar Karttunen's
-- "random" test entry.

-- Note: This code has not been optimized *at all*.  It is written to be clear
-- and to follow standard Haskell idioms as much as possible (but we have to match
-- the stateful PRNG idiom in the test definition, which is oriented toward an
-- imperative language).  Performance is decent with ghc -O2, but if it can be
-- improved without sacrificing the clarity of the code, by all means go for it!

import System(getArgs)

type Base = Char
type Sequence = [Base]

alu :: Sequence  -- predefined sequence
alu = "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG" ++
      "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA" ++
      "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT" ++
      "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA" ++
      "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG" ++
      "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC" ++
      "AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA"

type BaseFrequency = (Base,Double)

iub :: [BaseFrequency]
iub = [ ('a', 0.27), ('c', 0.12), ('g', 0.12), ('t', 0.27),
        ('B', 0.02), ('D', 0.02), ('H', 0.02), ('K', 0.02), ('M', 0.02), ('N', 0.02),
        ('R', 0.02), ('S', 0.02), ('V', 0.02), ('W', 0.02), ('Y', 0.02) ]

homosapiens :: [BaseFrequency]
homosapiens = [ ('a', 0.3029549426680), ('c', 0.1979883004921),
                ('g', 0.1975473066391), ('t', 0.3015094502008) ]

-- select a base whose interval covers the given double
chooseBase :: [BaseFrequency] -> Double -> Base
chooseBase [(b,_)]    _ = b
chooseBase ((b,f):xs) p | p < f     = b
                        | otherwise = chooseBase xs (p-f)

-- write a sequence in Fasta format
writeFasta :: String -> String -> Sequence -> IO ()
writeFasta label title sequence =
  do putStrLn $ ">" ++ (label ++ (" " ++ title))
     writeWrapped 60 sequence
  where writeWrapped _   []  = do return ()
        writeWrapped len str = do let (s1,s2) = splitAt len str
                                  putStrLn s1
                                  writeWrapped len s2

-- generate an infinite sequence of random doubles using the
-- prng from the "random" test
probs :: Int -> [Double]
probs seed = tail $ map normalize (iterate nextSeed seed)
  where nextSeed s = (s * ia + ic) `mod` im
        im = 139968
        ia = 3877
        ic = 29573
        normalize n = (fromIntegral n) * (1.0 / fromIntegral im)

main = do args <- getArgs
          let n = if (null args) then 1000 else read (head args)
          writeFasta "ONE" "Homo sapiens alu" (take (2*n) (cycle alu))
          let (seq1,seq2) = splitAt (3*n) (probs 42) -- we have to match the imperative version
          let seq2' = take (5*n) seq2                -- instead of using the Haskell idiom
          writeFasta "TWO" "IUB ambiguity codes" (map (chooseBase iub) seq1)
          writeFasta "THREE" "Homo sapiens frequency" (map (chooseBase homosapiens) seq2')
