// binarytrees.fs
//
// The Great Computer Language Shootout
// http://shootout.alioth.debian.org/
//
// Contributed by Robert Pickering
// Based on ocaml version by Troestler Christophe & Isaac Gouy
#light
type 'a tree = Empty | Node of 'a tree * 'a * 'a tree

let rec make i d =
  if d = 0 then Node(Empty, i, Empty)
  else 
    let i2 = 2 * i 
    let d = d - 1 
    Node(make (i2 - 1) d, i, make i2 d)

let rec check x = match x with | Empty -> 0 | Node(l, i, r) -> i + check l - check r

let min_depth = 4
let max_depth =  
    let n = try int_of_string Sys.argv.(1) with _ -> 10
    max (min_depth + 2) n
let stretch_depth = max_depth + 1


let rec loop_depths d =
  if d <= max_depth then
    let niter = 1 <<< (max_depth - d + min_depth) 
    let mutable c = 0 
    for i = 1 to niter do c <- c + check(make i d) + check(make (-i) d)
    Printf.printf "%i\t trees of depth %i\t check: %i\n" (2 * niter) d c
    loop_depths (d + 2)

let main() =
  let c = check (make 0 stretch_depth)
  Printf.printf "stretch tree of depth %i\t check: %i\n" stretch_depth c
  let long_lived_tree = make 0 max_depth
  loop_depths min_depth
  Printf.printf "long lived tree of depth %i\t check: %i\n"
    max_depth (check long_lived_tree)

main()