// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org/
//
// Contributed by The Anh Tran

#include <omp.h>
#include <memory.h>
#include <sched.h>
#include <sys/mman.h>

#include <iostream>
#include <vector>

#include <boost/shared_array.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <boost/format.hpp>

using namespace boost::xpressive;


/*
	Regex DNA benchmark focuses on regex engine performance.
	It has 4 steps:
		1.	read from input
		2.	strip header and \n. This step is not parallel-able, b/c there hasn't
			any struture information in read buffer to divide task among threads.
		3.	Regex_search on entire buffer => store data as flat char array.
		4.	Regex_replace on entire buffer => store each data line in a big enough struct
			to avoid resizing 
*/

// This struct encapsules a line of data
// data line read from input has length = 60
// But we reserve 256 bytes to avoid resizing when regex_replace is called
struct DataLine
{
	static const int MAX_LEN = 256;

	char data[MAX_LEN];
	size_t data_len;
};

// This class stores markers of [begin - end) of each DNA segment
// Input data has 3 segments
typedef std::vector< std::pair<size_t, size_t> > Segment_Markers;


// read all redirected data from stdin, then strip header line & '\n' char
void ReadInput_StripHeader(	size_t &file_size,	size_t &strip_size,
				 boost::shared_array<DataLine> &lines_array, size_t &line_count, 
				 boost::shared_array<char> &flat_array, Segment_Markers &seg_marker)
{
	// get input size
	file_size = ftell(stdin);
	fseek(stdin, 0, SEEK_END);
	file_size = ftell(stdin) - file_size;
	fseek(stdin, 0, SEEK_SET);

	// map input file to process space
	char* p_input_buffer = (char*)mmap(0, file_size, PROT_READ, MAP_PRIVATE|MAP_POPULATE, fileno(stdin), 0);

	// Alloc flat array storage space
	flat_array = boost::shared_array<char>(new char[file_size]);
	char* p_flat_array = flat_array.get();

	// Predict ndataline
	size_t nlines = file_size / 60 + 8;
	// Alloc dataline storage space
	lines_array = boost::shared_array<DataLine>(new DataLine[nlines]);
	DataLine* p_lines_array = lines_array.get();


	// Rule: Strip pattern: ">[^\n]*\n | \n"
	cregex strip_ptn( (as_xpr('>') >> *(~_n) >> _n) | (s1 = _n) );
	cmatch match_result;


	const char* psrc = p_input_buffer;
	const char* pend = p_input_buffer + file_size;

	size_t segment_begin =0, segment_end =0;

	strip_size = 0;
	line_count = 0;

	while ( boost::xpressive::regex_search((const char*)psrc, (const char*)pend, match_result, strip_ptn) )
	{
		if (match_result[1].matched)	// normal data line
		{
			size_t len = match_result[0].first - psrc;
			
			// copy to DataLine
			memcpy(p_lines_array[line_count].data, psrc, len);
			p_lines_array[line_count].data_len = len;
			line_count++;
			
			// copy to simple - flat array
			memcpy(p_flat_array + segment_end, psrc, len);
			segment_end += len;
			
			strip_size += len;
		}
		else		// header data line
		{
			if (segment_end > 0)
			{
				seg_marker.push_back (std::pair<size_t, size_t>(segment_begin, segment_end));
				segment_begin = segment_end;
			}
		}

		psrc = match_result[0].second;
	}

	if (segment_begin < segment_end)
		seg_marker.push_back (std::pair<size_t, size_t>(segment_begin, segment_end));

	munmap(p_input_buffer, file_size);
}


void CountPatternsMatch(boost::shared_array<char> const flat_array, 
						Segment_Markers const &seg_marker,
						std::string &result)
{
	static char const* patterns[] = 
	{
		("agggtaaa|tttaccct"),
		("[cgt]gggtaaa|tttaccc[acg]"),
		("a[act]ggtaaa|tttacc[agt]t"),
		("ag[act]gtaaa|tttac[agt]ct"),
		("agg[act]taaa|ttta[agt]cct"),
		("aggg[acg]aaa|ttt[cgt]ccct"),
		("agggt[cgt]aa|tt[acg]accct"),
		("agggta[cgt]a|t[acg]taccct"),
		("agggtaa[cgt]|[acg]ttaccct")
	};
	static const size_t npatttern = sizeof(patterns) / sizeof(patterns[0]);
	
	// element X in this array, counts how many matched so far, of pattern X
	static size_t matched_count[npatttern] = {0};
	// element X in this array, stores dna segments, which is processed with pattern X
	static size_t segment_processed[npatttern] = {0};

	for (size_t ipt = 0; ipt < npatttern; ++ipt)
	{
		cregex const &search_ptn(cregex::compile(patterns[ipt], regex_constants::nosubs|regex_constants::optimize));

		size_t job = 0;
		size_t seg_size = seg_marker.size ();
		
		// Parallel search, work is divided by dna segments
		while ((job = __sync_fetch_and_add(&segment_processed[ipt], 1)) < seg_size)
		{
			char const *pbegin	= flat_array.get() + seg_marker[job].first;
			char const *pend	= flat_array.get() + seg_marker[job].second;

			cregex_iterator ite(pbegin, pend, search_ptn), ite_end;
			// while (ite != ite_end) ++count;
			size_t count = std::distance (ite, ite_end);
			
			#pragma omp atomic
			matched_count[ipt] += count;
		}
	}

	// we want the last thread, reaching this code block, to print result
	static size_t thread_passed = 0;
	if (__sync_add_and_fetch(&thread_passed, 1) == omp_get_num_threads())
	{
		boost::format fmt("%1% %2%\n");
		for (size_t i = 0; i < npatttern; ++i)
		{
			fmt % patterns[i] % matched_count[i];
			result += fmt.str();
			fmt.clear();
		}
		thread_passed = 0;
	}
}


void Replace_Patterns(boost::shared_array<DataLine> const lines_array, size_t line_count, size_t &replace_len)
{
	// static variables here are shared among N threads
	static const cregex search_reg[] = 
	{	
		as_xpr('B'),	as_xpr('D'),	as_xpr('H'),	as_xpr('K'),	
		as_xpr('M'),	as_xpr('N'),	as_xpr('R'),	as_xpr('S'),	
		as_xpr('V'),	as_xpr('W'),	as_xpr('Y')
	};
	static const char* replace_ptn[] =
	{
		"(c|g|t)",	"(a|g|t)",		"(a|c|t)",	"(g|t)",	
		"(a|c)",	"(a|c|g|t)",	"(a|g)",	"(c|t)",	
		"(a|c|g)",	"(a|t)",		"(c|t)"
	};

	static const size_t n_search_ptn = sizeof(replace_ptn)/sizeof(replace_ptn[0]);
	static const size_t replace_ptn_len[] = 
	{
		7,	7,	7,	5,	
		5,	9,	5,	5,	
		7,	5,	5
	};

	// element X in this array, stores datalines, which is processed with pattern X
	// Ex: line_processed[2] = 3 means there are 3 data lines that has been processed with pattern 'H'
	static size_t line_processed[n_search_ptn] = {0};

	// for each replace pattern
	for (size_t ptn_idx = 0; ptn_idx < n_search_ptn; ++ptn_idx)
	{
		cregex const &search_ptn (search_reg[ptn_idx]);
		cmatch mt;
		char tmpbuf[DataLine::MAX_LEN];

		// Fetch line hasn't been processed
		// This is parallel replace, work is divided by lines
		size_t job;
		while ((job = __sync_fetch_and_add(&line_processed[ptn_idx], 1)) <  line_count)
		{
			DataLine &dataline = lines_array[job];
			size_t deslen = 0;

			const char* psrc = dataline.data;
			const char* psrc_end = dataline.data + dataline.data_len;

			// regex_replace can be used here instead. 
			// But input data has about 1/3 lines which aren't matched => no need to be copied out.
			// => manually search, replace is prefered 
			bool changed = false;
			while ( regex_search(psrc, psrc_end, mt, search_ptn) )
			{
				// copy [prefix - first char matched)
				size_t prefix = mt[0].first - psrc;
				memcpy(tmpbuf + deslen, psrc, prefix);
				deslen += prefix;

				// copy [replace_pattern]
				memcpy(tmpbuf + deslen, replace_ptn[ptn_idx], replace_ptn_len[ptn_idx]);
				deslen += replace_ptn_len[ptn_idx];

				// set pointer to next search position
				psrc = mt[0].second;
				changed = true;
			}

			if (changed)
			{
				// copy [last char matched - end line)
				size_t suffix = psrc_end - psrc;
				memcpy(tmpbuf + deslen, psrc, suffix);
				deslen += suffix;

				// replace existing data line
				memcpy(dataline.data, tmpbuf, deslen);
				dataline.data_len = deslen;
			}

			// if this is the last replace pattern, update replaced sequence length
			if ( ptn_idx == (n_search_ptn-1) )
			{
				#pragma omp atomic
				replace_len += dataline.data_len;
			}
		}
	}
}

// Detect single - multi thread benchmark
static
int GetThreadCount()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	for (int i = 0; i < 16; i++)
	{
		if (CPU_ISSET(i, &cs))
			count++;
	}
	return count;
}

int main()
{
	size_t initial_length = 0;
	size_t striped_length = 0;
	size_t replace_length = 0;
	
	boost::shared_array<DataLine> data_as_lines;
	size_t line_count = 0;
	
	boost::shared_array<char> data_as_flat_array;
	Segment_Markers seg_markers; 
	
	ReadInput_StripHeader (initial_length, striped_length, data_as_lines, line_count, data_as_flat_array, seg_markers);

	// search dna result
	std::string match_result;

	// spawn N threads
	#pragma omp parallel default(shared) num_threads(GetThreadCount())
	{
		CountPatternsMatch(data_as_flat_array, seg_markers, match_result);
		Replace_Patterns (data_as_lines, line_count, replace_length);
	}

	std::cout	<< match_result		<< std::endl
				<< initial_length	<< std::endl
				<< striped_length	<< std::endl
				<< replace_length	<< std::endl;

}


