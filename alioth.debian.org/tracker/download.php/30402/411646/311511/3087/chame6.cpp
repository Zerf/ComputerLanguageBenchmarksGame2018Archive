/* The Computer Language Benchmarks Game
http://shootout.alioth.debian.org/

Based on C contribution by Alex Burlyga
Further tweaking by The Anh Tran
*/


#include <cstdlib>
#include <cstdio>
#include <pthread.h>
#include <boost/lexical_cast.hpp>

//#define   MP_LTYPE      pthread_mutex_t
#define   MP_LTYPE      pthread_spinlock_t

//#define CR_LTYPE      pthread_mutex_t
//#define CR_LTYPE      pthread_spinlock_t
#define CR_LTYPE      void


template <typename T1 = void>
class CLock
{
public:
	CLock()		{   }
	~CLock()	{   }

	void Wait()	{	}
	void Signal()	{   }
	bool TryLock()	{	return true;	}

	bool Lock()		{	return true;	}
	void Unlock()	{   }
};


template <>
class CLock<pthread_spinlock_t>
{
private:
	pthread_spinlock_t   mlock;

public:
	CLock()		{   pthread_spin_init(&mlock, PTHREAD_PROCESS_PRIVATE);   }
	~CLock()	{   pthread_spin_destroy(&mlock);      }

	void Wait()		{   Lock();   }
	void Signal()	{   Unlock();   }

	void Unlock()	{   pthread_spin_unlock(&mlock);   }
	bool Lock()
	{
		while (pthread_spin_trylock(&mlock) != 0)
			sched_yield();
		return true;
	}
};


enum Color
{
	blue	= 0,
	red		= 1,
	yellow	= 2,
	Invalid	= 3
};

const char*	ColorName[]	= {"blue", "red", "yellow"};
const int	STACK_SIZE	= 16*1024;

static
Color ColorCompliment(Color c1, Color c2)
{
	switch (c1)
	{
	case blue:
		switch (c2)
		{
		case blue:
			return blue;
		case red:
			return yellow;
		case yellow:
			return red;
		default:
			goto errlb;
		}
	case red:
		switch (c2)
		{
		case blue:
			return yellow;
		case red:
			return red;
		case yellow:
			return blue;
		default:
			goto errlb;
		}
	case yellow:
		switch (c2)
		{
		case blue:
			return red;
		case red:
			return blue;
		case yellow:
			return yellow;
		default:
			goto errlb;
		}
	default:
		break;
	}

errlb:
	printf("Invalid color\n");
	exit( 1 );
}

static
char* SpellNumber(int n, char* outbuf)
{
	const char* NUMBERS[] =
	{
		"zero", "one", "two", "three", "four", "five",
		"six", "seven", "eight", "nine"
	};

	char tmp[64];
	int ichar = sprintf(tmp, "%d", n);

	int ochar = 0;
	for (int i = 0; i < ichar; i++)
		ochar += sprintf( outbuf + ochar, " %s", NUMBERS[ tmp[i] - '0' ] );

	return outbuf;
}


class Creature;
class MeetingPlace
{
protected:
	CLock<MP_LTYPE>	mp_lock;

	int			meetings_left;
	Creature*	waiting_creature;

public:
	MeetingPlace(int N)
	{
		meetings_left = N;
		waiting_creature = 0;
	}

	bool Meet( Creature* cr );
};


class Creature
{
protected:
	pthread_t		ht;
	pthread_attr_t	stack_att;
	CLock<CR_LTYPE>	cr_lock;

	MeetingPlace*	place;
	int		count;
	int		samecount;

	Color	color;
	int		id;
	bool	met;

public:
	Creature()
	{
		count	= samecount = 0;
		id		= 0;
		met		= false;

		pthread_attr_init( &stack_att );
		pthread_attr_setstacksize( &stack_att, STACK_SIZE );
	}

	void Start( MeetingPlace* mp, Color c )
	{
		place	= mp;
		color	= c;

		pthread_create( &ht, &stack_att, &Creature::ThreadRun, (void*)this );
	}

	static 
	void* ThreadRun(void* param)
	{
		((Creature*)param)->Run();
		return 0;
	}

	void Run()
	{
		id = pthread_self();
		cr_lock.Lock();

		while (true)
		{
			met = false;

			if ( place->Meet( this ) )
			{
				count++;
				while (met == false)
					sched_yield();
			}
			else
				break;
		}

		cr_lock.Unlock();
	}

	void Meet(Creature* other)
	{
		if (__builtin_expect(id == other->id, 0))
		{
			samecount++;
			other->samecount++;
		}

		Color newcolor	= ColorCompliment( color, other->color );
		other->color	= color	= newcolor;
		other->met		= met	= true;

		other->cr_lock.Signal();
	}

	void GetMeetingTimes(int &total, int &same) const
	{
		total	= count;
		same	= samecount;
	}

	void Wait() const
	{   pthread_join( ht, 0 );   }
};


bool MeetingPlace::Meet( Creature* currrent_creature )
{
	/*
	This function need atomic accesses to 2 variables:
		meetings_left
		waiting_creature
	*/
	mp_lock.Lock();

	if ( meetings_left > 0 )
	{
		if ( waiting_creature == 0 )  // first arrive
		{
			waiting_creature = currrent_creature;
			mp_lock.Unlock();
		}
		else // second arrive
		{
			// store pointer to first creature
			Creature* first_creature = waiting_creature;
			waiting_creature = 0;
			meetings_left--;

			// release MeetingPlace's lock ASAP
			// for other creatures to gain lock
			mp_lock.Unlock();

			// 2 creatures meeting each other
			currrent_creature->Meet(first_creature);
		}

		return true;
	}

	mp_lock.Unlock();
	return false;
}

template <int ncolor>
static
void RunGame(int n, Color const (&color)[ncolor])
{
	MeetingPlace	place(n);
	Creature		cr[ncolor];

	// print initial color of each creature
	for (int i = 0; i < ncolor; i++)
	{
		printf( "%s ", ColorName[ color[i] ] );
		cr[i].Start( &place, color[i] );
	}

	printf("\n");

	// wait for them to meet
	for (int i = 0; i < ncolor; i++)
		cr[i].Wait();

	int total = 0;
	char str[256];

	// print meeting times of each creature
	for (int i = 0; i < ncolor; i++)
	{
		int count, same;
		cr[i].GetMeetingTimes(count, same);

		printf("%u%s\n", count, SpellNumber(same, str) );

		total += count;
	}

	// print total meeting times
	printf("%s\n\n", SpellNumber(total, str) );
}

static
void PrintColors()
{
	for (int c1 = blue; c1 <= yellow; c1++)
	{
		for (int c2 = blue; c2 <= yellow; c2++)
		{
			printf ( "%s + %s -> %s\n",
				ColorName[c1],
				ColorName[c2],
				ColorName[ ColorCompliment( (Color)c1, (Color)c2 ) ]      );
		}
	}
	printf("\n");
}


int main(int argc, char** argv)
{
	sched_param priority;
	priority.sched_priority = 1;
	if (sched_setscheduler(0, SCHED_RR, &priority) != 0)
	{
		// meeting times will largely vary
	}
	
	int n = (argc >= 2) ? boost::lexical_cast<int>(argv[1]) : 600;

	PrintColors();

	const Color r1[] = {   blue, red, yellow   };
	const Color r2[] = {   blue, red, yellow,
		red, yellow, blue,
		red, yellow, red, blue   };

	RunGame( n, r1 );
	RunGame( n, r2 );
}


