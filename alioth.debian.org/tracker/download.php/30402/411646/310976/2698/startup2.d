/* The Computer Language Benchmarks Game
   http://shootout.alioth.debian.org/
   Base on C language contribution by Joe Tucek 2008-03-31
   Port to D by: dualamd  

	Compile command is a little bit different:
	Existing command: /dmd/bin/dmd ./startup.d -O -release -inline
	New command: /dmd/bin/dmd ./startup.d -O -release -inline -L"--entry=hellostart"
*/

module startup_c_like;

private const char[] str = "Hello world\n";

private const int _nr_write = 4;
private const int _nr_exit = 1;

private const int stdout_handle	= 1;

extern (C) 
{
	void syscall(int nr, ...);
	void hellostart()
	{
		syscall( _nr_write, stdout_handle, str.ptr, str.length );
		syscall( _nr_exit,  0 );
	}
}

void main()	
{	
	// nothing here will be executed
	// because we asked linker to set ELF entry point to hellostart()
}

