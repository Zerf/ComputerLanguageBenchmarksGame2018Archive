/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   Contributed by Josh Goldfoot
   to compile, use gcc -O3
*/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

typedef struct hte_struct
{
  unsigned long hash;
  char *sequence;
  long value;
} hashtableentry;

typedef struct ht_struct
{
  hashtableentry *hashes;
  int hashsize;
  long tablesize;
} hashtable;

unsigned long
djb2hash (char *str)
{
  unsigned long hash = 5381;
  int c;

  while ((c = *str++))
    hash = ((hash << 5) + hash) + c;
  return hash;
  /* credit: http://www.cs.yorku.ca/~oz/hash.html */
}

hashtable *
construct_hashtable (int new_hashsize, long buflen)
{
  hashtable *ret;
  long maxsize1, maxsize2;

  ret = malloc (sizeof (hashtable));
  if (!ret)
    return NULL;
  ret->hashsize = new_hashsize;
  ret->tablesize = 0;
  maxsize1 = buflen - new_hashsize + 1;
  maxsize2 = 4;
  while (--new_hashsize > 0 && maxsize2 < maxsize1)
    maxsize2 = maxsize2 * 4;
  if (maxsize2 > maxsize1)
    maxsize2 = maxsize1;
  ret->hashes = malloc (sizeof (hashtableentry) * maxsize2);
  if (!ret->hashes)
    return NULL;
  return ret;
}

char
find_entry (hashtable * ht, char *searchFor, long *index)
{
  long left, right, mid;
  unsigned long searchHash, thisHash;

  searchHash = djb2hash (searchFor);
  *index = 0;
  left = 0;
  if (ht == NULL)
    return 0;
  right = ht->tablesize - 1;
  if (right == -1)
    return 0;
  while (left <= right)
    {
      mid = (left + right) / 2;
      thisHash = ht->hashes[mid].hash;
      if (searchHash == thisHash)
	{
	  *index = mid;
	  return 1;
	}
      if (searchHash < thisHash)
	right = mid - 1;
      else
	left = mid + 1;
    }
  *index = mid;
  return 0;
}

long
get_value (hashtable * ht, char *searchHash)
{
  long index;

  if (find_entry (ht, searchHash, &index))
    {
      return ht->hashes[index].value;
    }
  return 0;
}

void
add_or_inc (hashtable * ht, char *searchHash)
{
  long mid;

  if (find_entry (ht, searchHash, &mid))
    {
      (ht->hashes[mid]).value++;
    }
  else
    {
      while (mid < ht->tablesize
	     && (djb2hash (searchHash) > ht->hashes[mid].hash))
	mid++;
      ht->tablesize++;
      if (ht->tablesize > 1)
	memmove (&(ht->hashes[mid + 1]), &(ht->hashes[mid]),
		 (ht->tablesize - 1 - mid) * sizeof (hashtableentry));
      ht->hashes[mid].value = 1;
      ht->hashes[mid].sequence = malloc (ht->hashsize + 1);
      if (ht->hashes[mid].sequence == NULL)
	return;
      strcpy (ht->hashes[mid].sequence, searchHash);
      ht->hashes[mid].hash = djb2hash (searchHash);
    }
}

void
destruct_hashtable (hashtable * ht)
{
  long i;
  for (i = 0; i < ht->tablesize; i++)
    {
      if (ht->hashes[i].sequence)
	free (ht->hashes[i].sequence);
    }
  free (ht->hashes);
  free (ht);
}

hashtable *
generate_frequencies (int fl, char *buffer, long buflen)
{
  hashtable *ht;
  char *reader;
  long i;
  char nulled;

  ht = construct_hashtable (fl, buflen);
  if (!ht)
    return NULL;
  for (i = 0; i < buflen - fl; i++)
    {
      reader = &(buffer[i]);
      nulled = reader[fl];
      reader[fl] = 0x00;
      add_or_inc (ht, reader);
      reader[fl] = nulled;
    }
  return ht;
}

void
write_frequencies (int fl, char *buffer, long buflen)
{
  hashtable *ht;
  long total, i, j;
  hashtableentry tmp;

  ht = generate_frequencies (fl, buffer, buflen);
  total = 0;
  for (i = 0; i < ht->tablesize; i++)
    total = total + ht->hashes[i].value;
  for (i = 0; i < ht->tablesize - 1; i++)
    for (j = i + 1; j < ht->tablesize; j++)
      if (ht->hashes[i].value < ht->hashes[j].value)
	{
	  memcpy (&tmp, &(ht->hashes[i]), sizeof (hashtableentry));
	  memcpy (&(ht->hashes[i]), &(ht->hashes[j]),
		  sizeof (hashtableentry));
	  memcpy (&(ht->hashes[j]), &tmp, sizeof (hashtableentry));
	}
  for (i = 0; i < ht->tablesize; i++)
    printf ("%s %.3f\n", ht->hashes[i].sequence,
	    100 * (float) ht->hashes[i].value / total);
  printf ("\n");
  destruct_hashtable (ht);
}


typedef struct node_struct
{
  unsigned long hash;
  long value;
  struct node_struct *less;
  struct node_struct *more;
} node;

node *
tfind (node * head, char *searchFor, int *found)
{
  unsigned long hash;
  node *last;

  hash = djb2hash (searchFor);
  last = head;
  while (head != NULL)
    {
      if (head->hash == hash)
	{
	  *found = 1;
	  return head;
	}
      else
	{
	  last = head;
	  if (head->hash > hash)
	    head = head->less;
	  else
	    head = head->more;
	}
    }
  *found = 0;
  return last;
}

node *
tnewnode (char *sequence)
{
  node *nn;

  nn = malloc (sizeof (node));
  if (!nn)
    return nn;
  nn->hash = djb2hash (sequence);
  nn->value = 1;
  nn->less = NULL;
  nn->more = NULL;
  return nn;
}

void
tadd_or_inc (node * head, char *searchFor)
{
  int found;
  node *where, *nn;

  where = tfind (head, searchFor, &found);
  if (found)
    where->value++;
  else
    {
      nn = tnewnode (searchFor);
      if (nn->hash < where->hash)
	where->less = nn;
      else
	where->more = nn;
    }
}

long
tget_value (node * head, char *searchFor)
{
  int found;
  node *where;

  where = tfind (head, searchFor, &found);
  if (found)
    return where->value;
  else
    return 0;
}

void
tdestruct (node * head)
{
  if (head->less)
    tdestruct (head->less);
  if (head->more)
    tdestruct (head->more);
  free (head);
}

void
write_count (char *searchFor, char *buffer, long buflen)
{
  int fl;
  char *reader;
  long i;
  node *head;
  char nulled;

  fl = strlen (searchFor);
  head = NULL;
  for (i = 0; i < buflen - fl; i++)
    {
      reader = &(buffer[i]);
      nulled = reader[fl];
      reader[fl] = 0x00;
      if (head == NULL)
	head = tnewnode (reader);
      else
	tadd_or_inc (head, reader);
      reader[fl] = nulled;
    }
  printf ("%d\t%s\n", tget_value (head, searchFor), searchFor);
  tdestruct (head);
}


int
main ()
{
  char flag, c;
  char *line, *buffer, *tmp;
  int i, linelen;
  long buflen, seqlen;

  line = malloc (256);
  buffer = NULL;
  flag = 0;
  buflen = 10240;
  buffer = malloc (buflen + 1);
  seqlen = 0;

  while (fgets (line, 255, stdin))
    {
      linelen = strlen (line);
      if (linelen)
	{
	  if (!flag)
	    {
	      flag = !!strstr (line, ">THREE");
	      continue;
	    }
	  else
	    {
	      c = line[0];
	      if (c == '>')
		break;
	      else if (c != ';')
		{
		  seqlen = seqlen + linelen - 1;
		  if (seqlen >= buflen)
		    {
		      buflen = buflen + 10240;
		      tmp = realloc (buffer, buflen + 1);
		      if (tmp == NULL)
			return -1;
		      buffer = tmp;
		    }
		  strncat (buffer, line, linelen - 1);
		}
	    }
	}
    }

  for (i = 0; i < seqlen; i++)
    buffer[i] = toupper (buffer[i]);
  write_frequencies (1, buffer, seqlen);
  write_frequencies (2, buffer, seqlen);
  write_count ("GGT", buffer, seqlen);
  write_count ("GGTA", buffer, seqlen);
  write_count ("GGTATT", buffer, seqlen);
  write_count ("GGTATTTTAATT", buffer, seqlen);
  write_count ("GGTATTTTAATTTATAGT", buffer, seqlen);
  free (buffer);
  free (line);
  return 0;
}
