#!/usr/bin/perl
# $Id: wc-perl.code,v 1.6 2005/04/06 14:50:28 bfulgham Exp $
# http://www.bagley.org/~doug/shootout/

# this program is modified from:
#   http://cm.bell-labs.com/cm/cs/who/bwk/interps/pap.html
# Timing Trials, or, the Trials of Timing: Experiments with Scripting
# and User-Interface Languages</a> by Brian W. Kernighan and
# Christopher J. Van Wyk.

use strict;
my($nl, $nw, $nc);
while (read(STDIN, $_, 4095)) {
    $_ .= <STDIN>;
    $nc += length;
    $nw += scalar split;
    $nl += tr/\n/\n/;
}
print "$nl $nw $nc\n";
