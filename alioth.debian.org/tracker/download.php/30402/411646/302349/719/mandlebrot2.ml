(*								-*tuareg-*-
 * The Great Computer Language Shootout
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Christophe TROESTLER
 * Enhanced by Christian Szegedy <szegedy@t-online.de>
 *             Yaron Minsky <yminsky-guest>
 * Modified by Brian Hurt <bhurt@spnz.org>
 *)

let niter = 50
let limit = 2.

let limit2 = limit *. limit

type complex = { mutable r: float; mutable i: float }

let add_bit0 c =
  let rec loop i zr zi c =
	if i >= niter then
      1
	else if (zr *. zr +. zi *. zi) > limit2 then
      0
	else loop (i+1) (zr *. zr -. zi *. zi +. c.r) (2. *. zr *. zi +. c.i) c
  in
  loop 0 0. 0. c

let () =
  let w = int_of_string(Array.get Sys.argv 1) in
  let h = w in
  let fw = float w
  and fh = float h in
  Printf.printf "P4\n%i %i\n" w h;
  let rec yloop c y =
    if y >= h then
      ()
    else
      let rec xloop c x byte b =
        if x >= w then
          if b > 1 then
            output_byte stdout (byte lsl (9 - b))
          else
            ()
        else 
          let () = c.r <- 2. *. float x /. fw -. 1.5 in
          let byte = (byte lsl 1) lor (add_bit0 c) in
          if (b < 8) then
            xloop c (x+1) byte (b+1)
          else 
            let () = output_byte stdout byte in
            xloop c (x+1) 0 1
      in
      c.i <- 2. *. float y /. fh -. 1.;
      xloop c 0 0 1;
      yloop c (y+1)
  in
  yloop {r = 0.; i = 0.} 0

