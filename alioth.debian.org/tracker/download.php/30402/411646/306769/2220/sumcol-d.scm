#!/home/dsilva/bin/mzscheme -uqxA 
;;; Sumcol
;;; http://shootout.alioth.debian.org/
;;;
;;; Contributed by Eli Barzilay
;;; Modified by Daniel

(module sumcol-d mzscheme
  (let loop ([acc 0])
    (let ([n (read-line)])
      (if (eof-object? n)
	  (printf "~a\n" acc)
	  (loop (+ acc (string->number n)))))))
