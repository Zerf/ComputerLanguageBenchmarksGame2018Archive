
module Main where

import System.IO

rdbl :: String -> Double
rdbl = read

main = do
	bytes <- getContents
	print (sum (map read (lines bytes)))