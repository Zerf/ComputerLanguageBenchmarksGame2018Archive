{-# OPTIONS -O2 #-}
{-
	The Computer Language Shootout
   	http://shootout.alioth.debian.org/
   	contributed by Bertram Felgenhauer
-}

import Data.Word
import Data.Array
import Data.Bits
import System

pieces = [
    [(0,0),(0,1),(0,2),(0,3),(1,3)],
    [(0,0),(0,2),(0,3),(1,0),(1,1)],
    [(0,0),(0,1),(0,2),(1,2),(2,1)],
    [(0,0),(0,1),(0,2),(1,1),(2,1)],
    [(0,0),(0,2),(1,0),(1,1),(2,1)],
    [(0,0),(0,1),(0,2),(1,1),(1,2)],
    [(0,0),(0,1),(1,1),(1,2),(2,1)],
    [(0,0),(0,1),(0,2),(1,0),(1,2)],
    [(0,0),(0,1),(0,2),(1,2),(1,3)],
    [(0,0),(0,1),(0,2),(0,3),(1,2)]]

transform p =
    [ map (\(y,x) -> (y-dy,x-dx)) p2
    | p' <- take 6 (iterate (map rotate) p),
      p2 <- take 2 (iterate (map mirror) p'),
      let (dy,dx) = minimum p2] where
    rotate (y,x) = (x+y,-y)
    mirror (y,x) = (-y,x+y)

pieceMasks = listArray (0,9) (map (pieceMasks' . transform) pieces) where
    pieceMasks' ps = listArray (0,9) [pieceMasks2 ps (y,x) | y <- [0,1], x <- [0..4]]
    pieceMasks2 ps pos = [m | p <- ps, m <- mask 0 pos p]
    mask m (dy,dx) [] = [m]
    mask m (dy,dx) ((y,x):ps) =
        let x' = x+dx + (y+dy)`div`2
        in  if x' < 0 || x' > 4 then []
            else mask (m .|. (1 `shiftL` (5*(dy+y)+x'))) (dy,dx) ps

search _ 50 _ ps = [ps]
search m i p ps | (m .&. (1 `shiftL` i)) > 0 = search m (i+1) p ps
search m i p ps = let (q,r) = i `quotRem` 10 in
    [ ps'
    | p' <- [0..9], p .&. (1 `shiftL` p') == 0,
      mask <- pieceMasks ! p' ! r,
      let mask' = mask `shiftL` (10*q),
      m .&. mask' == 0,
      ps' <- search (m .|. mask') (i+1) (p .|. (1 `shiftL` p')) ((p',mask') : ps)]

display ps =
    [ [ i | x <- [0..4], (i,m) <- ps, (1 `shiftL` (y*5+x)) .&. m /= 0]
    | y <- [0..9]]

main = do
    n <- readIO . head =<< getArgs
    let sols = map display $ take n $ search ((-1::Word64) `shiftL` 50) 0 (0::Word) []
    putStrLn (show (length sols) ++ " solutions found")
    mapM (putStr . unlines . ("":) . (zipWith (++) (cycle [""," "]))
         . map ((++ " ") . unwords . map show)) [minimum sols,maximum sols]
