;;; The Computer Language Shootout
;;; http://shootout.alioth.debian.org/
;;;
;;; contributed by Greg Buchholz 

(defun fib (n)
  (if (< n 2) 1 (+ (fib (1- n)) (fib (- n 2)))))

(defun ack (x y)
  (if (zerop x)
      (1+ y)
      (if (zerop y)
          (ack (1- x) 1)
          (ack (1- x) (ack x (1- y))))))

(defun tak (x y z)
  (if (< y x)
      (tak (tak (1- x) y z) 
           (tak (1- y) z x)
           (tak (1- z) x y))
      z))
      
(defun main ()
  (let* ((args #+sbcl sb-ext:*posix-argv*
               #+cmu extensions:*command-line-strings*
               #+gcl si::*command-args*)
         (n (1- (parse-integer (car (last args))))))
    (format t "Ack(3,~A): ~A~%" (1+ n) (ack 3 (1+ n)))
    (format t "Fib(~,1F): ~,1F~%" (+ 28.0d0 n) (fib (+ 28.0d0 n)))
    (format t "Tak(~A,~A,~A): ~A~%" (* 3 n) (* 2 n) n (tak (* 3 n)
                                                           (* 2 n)
                                                           n))
    (format t "Fib(~A): ~A~%" 3 (fib 3))
    (format t "Tak(3.0,2.0,1.0): ~,1F~%" (tak 3.0d0 2.0d0 1.0d0))))
