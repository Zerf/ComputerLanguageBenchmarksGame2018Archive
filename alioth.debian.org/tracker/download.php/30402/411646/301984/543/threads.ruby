#!/usr/bin/ruby
#
# The Computer Language Shootout
# http://shootout.alioth.debian.org
#
# Contributed by Jesse Millikan

N = ARGV[0].to_i

links = Array.new(N - 1)

prev = Thread.main

for i in 0..(N - 1) do
 links[N - i - 1] = Thread.new(prev) { |prev_link|
  Thread.current["msg"] = -1
  Thread.stop

  # And then, once it is resume

  while Thread.current["msg"] == -1; Thread.pass; end

  prev_link["msg"] = Thread.current["msg"] + 1 }

 prev = links[N - i - 1]
end

links[0]["msg"] = 0

for i in 0..(N - 1); links[i].run; end

links[N - 1].join

puts "#{Thread.current['msg']}"

