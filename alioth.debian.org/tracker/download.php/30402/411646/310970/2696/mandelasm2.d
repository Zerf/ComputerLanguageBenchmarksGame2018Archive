//-----------------------------------------------------------------------------
/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   Contributed by Dave Fladebo
   compile: dmd -O -inline -release mandelbrot2.d
   
   Asm modification by dualamd. Which again copy&paste from:
   Paolo Bonzini, Paul Kitchin, Greg Buchholz
*/
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
import std.stdio;
import std.string;
import std.conv;
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void main(char[][] args)
{
	uint n = 200;

	try
	{
		if (args.length == 2)
			n = cast(uint)(toInt(args[1]));
	}
	catch (Exception) 	{	}

	writefln("P4\n%d %d",n,n);

	reverse2N[0] = reverse2N[1] = 2.0 / (cast(double)n);
	Calc1(n);
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void Calc1(uint n)
{
	uint bit_num = 0; // use dword instead of byte to avoid byte <-> int conversion
	uint byte_acc = 0; // and give better memory alignment access

	for (uint y = 0; y < n; y++)
	{
		for (uint x = 0; x < n; x += 2)
		{
			// a little tricky in calling, to avoid swapping result bits
			uint c2 = Calc_DoItMyself( x +1, x, y, y );
			byte_acc = (byte_acc << 2) | c2;
	
			bit_num += 2;
			if (bit_num == 8)
			{
				putc( byte_acc, stdout );
				bit_num = byte_acc = 0;
			}
		}
		// move this condition out of the 2nd loop. 
		// Copied from C ver by Paolo Bonzini
		if (byte_acc != 0)
		{
			byte_acc <<= (8 - (n % 8));
			putc( byte_acc, stdout );
			bit_num = byte_acc = 0;
		}
	}
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
align (16) double[2] 		reverse2N;

align (16) const int		iter	= 50;
align (16) const double[2]	limit	= [(2.0 * 2.0), (2.0 * 2.0)];

align (16) const double[2]	c15 = [1.5, 1.5];
align (16) const double[2]	c10 = [1.0, 1.0];
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
extern (C) // C linkage AND _cdecl calling convention
{
	align (16)
	uint Calc_DoItMyself(uint x1, uint x2, uint y1, uint y2)
	{
		asm
		{
			naked;	// tell DMD that me want to do it myself
			
			// Load const value from mem
			movapd		XMM7,	reverse2N; 	// x7 = 2/n 2/n
			movapd		XMM6,	limit;		// x6 = 2.0 + 2.0 = 4.0 4.0
			movapd		XMM5,	c10;		// x5 = 1.0 1.0
			movapd		XMM4,	c15;		// x4 = 1.5 1.5
			
			// calculate C value, real part
			cvtdq2pd	XMM3,	[ESP +4]; 	// x3 = a1 a2 (real part)
			mulpd		XMM3,	XMM7;		// x3 = x3*x7 = a*2/n
			subpd		XMM3,	XMM4;		// x3 = x3 - x5 = (2a/n -1.5)
			movapd		XMM4,	XMM3;		// move real part to x4 to free up register

			// calculate C value, img part
			cvtdq2pd	XMM3,	[ESP +12]; 	// x3 = b1 b2 (img part)
			mulpd		XMM3,	XMM7;		// x3 = x3*x7 = b*2/n
			subpd		XMM3,	XMM5;		// x3 = x3 - x5 = (2b/n -1.0)
			movapd		XMM5,	XMM3;		// move img part to x5 to free up register
			
			// init Z = 0 + 0i
			pxor		XMM3,	XMM3;	// x3 = Z.img = 0.0 0.0
			movapd		XMM2,	XMM3;	// x2 = Z.real = 0.0 0.0
			
			mov			EDX,	iter;	// loop count
			mov			EAX,	3;		// result mask is [true true]

			// registers holding:
			// x0 x1 x7 free
			// x2 + x3 = Z = real + img
			// x4 + x5 = C = real + img
			// x6 = limit
		not_done_yet:
			// calculate (a*a + b*b) = len(Z)
			movapd		XMM0,	XMM2;			// x0 = a1 a2 = Z.real
			mulpd		XMM0,	XMM2;			// x0 = x0 * x3 = a1*a1 a2*a2
			
			movapd		XMM1,	XMM3;			// x1 = x3 = b1 b2 = Z.img
			mulpd		XMM1,	XMM3;			// x1 = x1*x3 = b1*b1 b2*b2
			
			movapd		XMM7,	XMM0;			// x7 = a1*a1 a2*a2
			addpd		XMM7,	XMM1;			// x7 = [a1*a1 + b1*b1] [a2*a2 + b2*b2]
			subpd		XMM7,	XMM6;			// x7 = x7 - x6 = [len(Z1) - limit] [len(Z2) - limit]
	
			// checking loop condition (len(z1) <= limit) && (len(z2) <= limit)
			movmskpd	ECX,	XMM7;			// ecx = sign bit of subtraction result
			test		ECX,	ECX;			// ecx = 0 means (len(Z1) > limit) && (len(Z2) > limit)
			jz			done;					// len(Z) > limit : return false
			
			and			EAX,	ECX;			// mask out known false value
			
			// checking loop cond i < 50
			test		EDX,	EDX;
			jz			done;
			
			// registers holding:
			// x0 = a1*a1 a2*a2
			// x1 = b1*b1 b2*b2
			// x2 = Z.real = a1 a1
			// x3 = Z.img = b1 b2
			// x4 = C.real = a3 a4
			// x5 = C.img = b3 b4
			// x6 = limit len
			// x7 = free

			// calculate Z = Z*Z + C
			// here is Z*Z = (aa-bb) + (ab + ab)i
			// calculate (aa - bb) of real part
			subpd		XMM0,	XMM1;			// x0 = [a1*a1 - b1*b1] [a2*a2 - b2*b2]
			
			// calculate (ab + ab) of img part
			mulpd		XMM3,	XMM2;			// x3 = b1*a1 b2*a2
			movapd		XMM1,	XMM3;			// x1 = a1*b1 a2*b2
			addpd		XMM3,	XMM1;			// x3 = 2*a1b1 2*b2b2 = Z*Z.img

			movapd		XMM2,	XMM0;			// x2 = x0 = Z*Z.real
			
			// calculate Z += C
			addpd		XMM3,	XMM5;	// Z.img += C.img
			addpd		XMM2,	XMM4; 	// Z.real += C.real
			
			sub			DL,		1;
			jmp			not_done_yet;
		
		done:
			// ecx contains sign bit of two subtraction:
			// bit 0 = sign [len(z2) - limit]. Bit 0 = 1 mean len(z2) <= limit
			// bit 1 = sign [len(z1) - limit]. Bit 1 = 1 mean len(z1) <= limit
			and		EAX,	ECX;	// transfer sign bit to result.
			// (result & 1) != 0 means len(Z2) <= limit. Z2 at (x+1,y) 
			// (result & 2) != 0 means len(Z1) <= limit. Z1 at (x,y) 
			ret;	// _cdecl convention
		}
	}
}
//-----------------------------------------------------------------------------
