/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   Contributed by Dave Fladebo
   compile: dmd -O -inline -release mandelbrot2.d
   
   Modification by dualamd
*/

import std.stdio;
import std.string;
import std.conv;

void main(char[][] args)
{
	char bit_num = 0, byte_acc = 0;
	int n = 200;

	try
	{
		n = toInt(args[1]);
	}
	catch (Exception) 	{	}

	writefln("P4\n%d %d",n,n);
	//readln();

	reverseN[0] = reverseN[1] = 2.0 / (cast(double)n);

	for (int y = 0; y < n; y++)
	{
		for (int x = 0; x < n; x++)
		{
			char c2 = Calc_DoItMyself( x,y );
			byte_acc = (byte_acc << 1) | c2;

			bit_num++;
			if (bit_num == 8)
			{
				putc( byte_acc, stdout );
				bit_num = byte_acc = 0;
			}
			else if (x == n -1)
			{
				byte_acc <<= (8 - (n % 8));
				putc( byte_acc, stdout );
				bit_num = byte_acc = 0;
			}
		}
	}
}

align (16) const int	iter	= 50;
align (16) const double	lim		= 2.0 * 2.0;

align (16) double[2] 		reverseN;
align (16) const double[2]	cinit = [-1.5, -1.0];

extern (C) // C linkage AND _cdecl calling convention
{
align (16)
char Calc_DoItMyself(int x, int y)
{
	asm
	{
		naked;	// tell DMD that me want to do it myself
		
		// Load const value from mem
		movapd		XMM7,	reverseN; 	// x7 = 2/n + 2/ni
		movapd		XMM6,	cinit;		// x6 = -1.5 + -1i
		movsd		XMM5,	lim;		// x5 = 2.0 + 2.0 = 4.0
		
		// calculate C value
		cvtdq2pd	XMM4,	[ESP +4]; 	// x4 = x +yi
		mulpd		XMM4,	XMM7;		// x4 = x4*x7 = (x + yi) * (2/n + 2/ni) = 2x/n + 2y/ni
		addpd		XMM4,	XMM6;		// x4 = x4 + x6 = (2x/n -1.5  +  2y/ni -1i)
		
		pxor		XMM3,	XMM3;	// x3 = z = 0+0i
		mov			ECX,	iter;	// loop count
		xor			EAX,	EAX;	// return value

	not_done_yet:
		// calculate (a*a + b*b)
		movapd		XMM0,	XMM3;			// x0 = x3 = z = a + b
		mulpd		XMM0,	XMM3;			// x0 = x0 * x3 = z*z = aa + bb
		movapd		XMM1,	XMM0;			// x1 = x0 = aa + bb
		shufpd		XMM0,	XMM1,	0x01;	// x0 = bb + bb
		addsd		XMM0,	XMM1;			// x0 = aa + bb <-> norm(Z);
		comisd		XMM0,	XMM5;			// norm(Z) cmp limit
		ja			ret_false;				// norm(Z) > limit : return false

		test		ECX,	ECX;	// norm(Z) <= limit
		jz			ret_true;		// return true
		
		// x0 = scratch
		// x1 = aa + bb; 
		// x2 = scratch
		// x3 = a + b
		// x4 = C
		// x5 = lim
		// x6 = -1.5 + -1
		// x7 = 2/n + 2/n
		// calculate (aa - bb) of real part
		movapd		XMM0,	XMM1;			// x0 = aa + bb
		shufpd		XMM0,	XMM1, 0x01;		// x0 = bb + bb
		subsd		XMM1,	XMM0;			// x1 = (aa - bb) + bb
		
		// calculate (ab + ab) of img part
		movapd		XMM2,	XMM3;			// x2 = a + b
		shufpd		XMM2,	XMM3, 0x01;		// x2 = b + b
		mulsd		XMM2,	XMM3;			// x2 = (ab) + b
		addsd		XMM2,	XMM2;			// x2 = (2ab) + b
		
		// reassemble Z = Z*Z
		shufpd		XMM1,	XMM2, 0x00; 	// x1 = (aa-bb) + (ab + ab)i = Z*Z
		
		movapd		XMM3,	XMM1;
		addpd		XMM3,	XMM4;	// x3 = Z*Z + C
		
		sub			ECX,	1;
		jmp			not_done_yet;
	
	ret_true:
		mov			AL,		1;
	ret_false:
		ret;	// _cdecl convention
	}
}
}
