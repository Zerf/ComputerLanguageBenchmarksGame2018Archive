//-----------------------------------------------------------------------------
/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   Contributed by Dave Fladebo
   compile: dmd -O -inline -release mandelbrot2.d
   
   Asm modification by dualamd. Which again copy&paste from:
   Paolo Bonzini, Paul Kitchin, Greg Buchholz
*/
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
import std.stdio;
import std.string;
import std.conv;
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void main(char[][] args)
{
	uint n = 200;

	try
	{
		if (args.length == 2)
			n = cast(uint)(toInt(args[1]));
	}
	catch (Exception) 	{	}

	writefln("P4\n%d %d",n,n);
	Calc1(n);
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
void Calc1(uint n)
{
	uint bit_num = 0; // use dword instead of byte to avoid byte <-> int conversion
	uint byte_acc = 0; // and give better memory alignment access

	// this value is used to calc C.real = x*2/n -1.5
	reverse2N[0] = reverse2N[1] = 2.0 / (cast(double)n);
	
	// C.img = y*2/n -1.0
	Cimg[0] = Cimg[1] = -1.0;
	
	for (uint y = 0; y < n; y++)
	{
		for (uint x = 0; x < n; x += 2)
		{
			// a little tricky in calling, to avoid swapping result bits
			uint cc = Calc_DoItMyself(x +1, x);

			byte_acc = (byte_acc << 2) | cc;
	
			bit_num += 2;
			if (bit_num == 8)
			{
				putc( byte_acc, stdout );
				bit_num = byte_acc = 0;
			}

		}
		// move this condition out of the 2nd loop. 
		// Copied from C ver by Paolo Bonzini
		if (byte_acc != 0)
		{
			byte_acc <<= (8 - (n % 8));
			putc( byte_acc, stdout );
			bit_num = byte_acc = 0;
		}

		// C.img = y*2/n -1 <=> C.img += 2/n
		double im = Cimg[1] + reverse2N[1];
		Cimg[0] = Cimg[1] = im;
	}
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
align (16) const uint		iter	= (50 -1);
align (16) const double[2]	limit	= [(2.0 * 2.0), (2.0 * 2.0)];

align (16) double[2]	reverse2N;
align (16) double[2]	Cimg;

// can't replace heavy C.real = x*2/n -1.5 by C.real += 2/n
// due to precision lost :(
//align (16) double[2]	Creal;

// const value to calc C.real
align (16) const double[2]	c15		= [1.5, 1.5];

// const value to calc C.img
//align (16) const double[2]	c10 = [1.0, 1.0];

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
extern (C) // C linkage AND _cdecl calling convention
{
	/* Stack layout:
		esp +8	a1	= x
		esp +4	a2	= x+1
		esp +0	return address
	*/
	align (16)
	uint Calc_DoItMyself(uint, uint)
	{
		asm
		{
			naked;	// tell DMD that me want to do it myself
			
			// Load const value from mem
			movapd		XMM6,	limit;

			// load pre-calculated C.img value
			movapd		XMM5,	Cimg;
			
			// calculate C.real value
			movapd		XMM7,	reverse2N;
			cvtdq2pd	XMM3,	[ESP +4]; 	// x3 = a1 a2 (real part)
			movapd		XMM4,	c15;		// x4 = 1.5 1.5
			mulpd		XMM3,	XMM7;		// x3 = x3*x7 = a*2/n
			subpd		XMM3,	XMM4;		// x3 = x3 - x5 = (2a/n -1.5)
			movapd		XMM4,	XMM3;		// move real part to x4 to free up register

			// z = 0 + 0i
			//pxor		XMM3,	XMM3;
			//pxor		XMM2,	XMM2;
			// assign Z = C rightaway, avoiding 0.0 * 0.0 multiply -> loop 49 times
			movapd		XMM3,	XMM5;
			movapd		XMM2,	XMM4;
			
			mov			EDX,	iter;	// loop count
			mov			EAX,	3;		// result mask is [true true]

			// registers holding:
			// x0 x1 x7 free
			// x2 + x3 = Z = real + img
			// x4 + x5 = C = real + img
			// x6 = limit len
		not_done_yet:
			// calculate (a*a + b*b) = len(Z)
			movapd		XMM0,	XMM2;			// x0 = a1 a2 = Z.real
			mulpd		XMM2,	XMM2;			// x2 = x2 * x2 = a1*a1 a2*a2
			
			movapd		XMM1,	XMM3;			// x1 = x3 = b1 b2 = Z.img
			mulpd		XMM3,	XMM3;			// x3 = x3*x3 = b1*b1 b2*b2

			movapd		XMM7,	XMM2;			// x7 = a1*a1 a2*a2
			addpd		XMM7,	XMM3;			// x7 = [a1*a1 + b1*b1] [a2*a2 + b2*b2]
			subpd		XMM7,	XMM6;			// x7 = x7 - x6 = [len(Z1) - limit] [len(Z2) - limit]
	
			// checking loop condition (len(z1) <= limit) && (len(z2) <= limit)
			movmskpd	ECX,	XMM7;			// ecx = sign bit of subtraction result
			test		ECX,	ECX;			// ecx = 0 means (len(Z1) > limit) && (len(Z2) > limit)
			jz			done_false;					// len(Z) > limit : return false
			
			and			EAX,	ECX;			// mask out known false value

			// checking loop cond i < 50
			test		EDX,	EDX;
			jz			done_true;
			
			// registers holding:
			// x0 = Z.real = a1 a1
			// x1 = Z.img = b1 b2
			// x2 = a1*a1 a2*a2
			// x3 = b1*b1 b2*b2
			// x4 = C.real = a3 a4
			// x5 = C.img = b3 b4
			// x6 = limit len
			// x7 = free

			// calculate Z = Z*Z + C
			// here is Z*Z = (aa-bb) + (ab + ab)i
			// calculate (aa - bb) of real part
			subpd		XMM2,	XMM3;			// x2 = [a1*a1 - b1*b1] [a2*a2 - b2*b2]
			
			// calculate (ab + ab) of img part
			mulpd		XMM1,	XMM0;			// x1 = b1*a1 b2*a2
			movapd		XMM3,	XMM1;			// x3 = a1*b1 a2*b2
			addpd		XMM3,	XMM1;			// x3 = 2*a1b1 2*b2b2 = Z*Z.img
			
			// calculate Z += C
			addpd		XMM2,	XMM4; 	// Z.real += C.real
			addpd		XMM3,	XMM5;	// Z.img += C.img
			
			sub			DL,		1;
			jmp			not_done_yet;
		
		done_false:
			// ecx contains sign bit of two subtraction:
			// bit 0 = sign [len(z2) - limit]. Bit 0 = 1 mean len(z2) <= limit
			// bit 1 = sign [len(z1) - limit]. Bit 1 = 1 mean len(z1) <= limit
			and		EAX,	ECX;	// transfer sign bit to result.
			// (result & 1) != 0 means len(Z2) <= limit. Z2 at (x+1,y) 
			// (result & 2) != 0 means len(Z1) <= limit. Z1 at (x,y) 
		done_true:
			ret;	// _cdecl convention
		}
	}
}
//-----------------------------------------------------------------------------
