{ The Computer Language Shootout
  http://shootout.alioth.debian.org

  contributed by Ales Katona
  modified by Vincent Snijders
  modified by Steve Fisher
}



{$inline on}
{$FPUTYPE SSE2}
{$I-}

var
  n, x, y: word;
  bits, bit: byte;
  c_x, c_y: double;

procedure calc_point; inline;
const
  limit: double = 4.0;
var
  i: word;
  z_r, z_i, c_r, c_i, t_i, t_r: double;
begin
  c_r := c_x;
  c_i := c_y;
  z_r := 0;  z_i := 0;  t_r := 0;  t_i := 0;
  for i := 1 to 50 do
  begin
    z_i := z_r*z_i*2 + c_i;
    z_r := t_r - t_i + c_r;
    t_i := z_i * z_i;
    t_r := z_r * z_r;
    if t_r + t_i >= limit then
    begin
      bits := bits xor bit;
      break
    end
  end;
end;

{$FPUTYPE X87}

begin
  val(ParamStr(1), n);

  writeln('P4');
  writeln(n,' ',n);
  for y := 0 to n-1 do
  begin
    c_y := y * 2 / n - 1;
    bits := 255;
    bit := 128;
    for x := 0 to n-1 do
    begin
      c_x := x * 2 / n - 1.5;

      calc_point;

      if bit > 1 then
        bit := bit shr 1
      else
      begin
        write(char(bits));
        bits := 255;
        bit := 128;
      end;
    end;
    if bit < 128 then write(char(bits xor ((bit shl 1)-1)));
  end;

end.
