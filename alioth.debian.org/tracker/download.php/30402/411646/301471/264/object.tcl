#!/usr/bin/tclsh
# $Id: objinst-tcl-2.code,v 1.1 2005/04/12 15:04:56 bfulgham Exp $
# http://shootout.alioth.debian.org/
#
# Contributed by Hemang Lavana

package require XOTcl

::xotcl::Class Toggle
Toggle instproc init {start_state} {
    ::xotcl::my instvar state
    set state $start_state
}
Toggle instproc value {} {
    ::xotcl::my instvar state
    return [expr {$state ? true : false}]
}
Toggle instproc activate {} {
    ::xotcl::my instvar state
    set state [expr {!$state}]
    return [::xotcl::self]
}

::xotcl::Class NthToggle -superclass Toggle
NthToggle instproc init {start_state max_counter} {
    ::xotcl::next $start_state
    ::xotcl::my instvar counter count_max
    set counter 0
    set count_max $max_counter
}
NthToggle instproc activate {} {
    ::xotcl::my instvar state counter count_max
    incr counter 1
    if {$counter >= $count_max} {
        set state [expr {!$state}]
        set counter 0
    }
    return [::xotcl::self]
}

proc main {n} {
    Toggle toggle TRUE
    for {set i 0} {$i<5} {incr i} {
        toggle activate
        puts [toggle value]
    }
    puts ""

    for {set i 0} {$i<$n} {incr i} {Toggle toggle$i FALSE}

    NthToggle ntoggle TRUE 3
    for {set i 0} {$i<8} {incr i} {
        ntoggle activate
        puts [ntoggle value]
    }

    for {set i 0} {$i<$n} {incr i} {NthToggle ntoggle$i TRUE 3}
}
main [lindex $argv 0]

