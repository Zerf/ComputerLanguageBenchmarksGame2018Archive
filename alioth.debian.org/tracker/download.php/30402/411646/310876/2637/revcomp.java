/*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 * by Bob Carpenter
 * modified by Amir K aka Razzi (reads 82 bytes per read)
 */

import java.io.*;

public class revcomp {

    static final int INITIAL_BUF_LEN = 82; /// READ 82 BYTES...
    static final byte FIRST_CHAR_IN_RECORD = (byte) '>';
    static final byte NEWLINE_CHAR = (byte) '\n';


    static byte[] buf = new byte[INITIAL_BUF_LEN];

    static int bufLen = INITIAL_BUF_LEN;
    
    public static void main(final String[] args)
    throws IOException {

        int start = 0;
        buf[0] = FIRST_CHAR_IN_RECORD;
        System.in.read();
        int next = 1;

        while (true) {

            int numBytesRead = System.in.read(buf,next,bufLen-next);
            if (numBytesRead == -1) break;
            int pos = next;
            next += numBytesRead;
            while (pos < next) {
                if (buf[pos] == FIRST_CHAR_IN_RECORD) {
                    writeReversed(buf,start,pos-1);
                    start = pos;
                }
                ++pos;
            }
            if (next == bufLen) {
                if (start > 0) {
                    for (int i = start; i < bufLen; ++i)
                        buf[i-start] = buf[i];
                    next -= start;
                    start = 0;
                } else {
                    byte[] oldBuf = buf;
                    buf = new byte[bufLen = oldBuf.length * 2];
                     for (int i = 0; i < next; ++i)
                        buf[i] = oldBuf[i];
                }
            }
        }
        if (start != next-1)
            writeReversed(buf,start,next-1);
    }

    static void writeReversed(final byte[] buf,
                              final int start,
                              final int end)
    throws IOException {

        int pos = start + 1;
        while (buf[pos++] != NEWLINE_CHAR) ;
        System.out.write(buf,start,pos-start);
        reverseComplement(buf,pos,end);
        System.out.write(buf,pos,end-pos+1);
    }

    static void reverseComplement(final byte[] buf, int start, int end) {
        while (start < end) {
            if (buf[start] == NEWLINE_CHAR) ++start;
            if (buf[end] == NEWLINE_CHAR) --end;
            if (start >= end)
                return;
            byte temp = buf[start];
            buf[start++] = cmp[buf[end]];
            buf[end--] = cmp[temp];
        }
    }

    static final byte[] cmp = new byte[128];
    static {
        for (int i = 0; i < 128; ++i) cmp[i] = (byte) i;
        cmp['t'] = cmp['T'] = 'A';
        cmp['a'] = cmp['A'] = 'T';
        cmp['g'] = cmp['G'] = 'C';
        cmp['c'] = cmp['C'] = 'G';
        cmp['v'] = cmp['V'] = 'B';
        cmp['h'] = cmp['H'] = 'D';
        cmp['r'] = cmp['R'] = 'Y';
        cmp['m'] = cmp['M'] = 'K';
        cmp['y'] = cmp['Y'] = 'R';
        cmp['k'] = cmp['K'] = 'M';
        cmp['b'] = cmp['B'] = 'V';
        cmp['d'] = cmp['D'] = 'H';
        cmp['u'] = cmp['U'] = 'A';
    }
}