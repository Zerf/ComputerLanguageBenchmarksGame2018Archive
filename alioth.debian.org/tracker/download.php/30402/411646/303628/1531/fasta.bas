
type aminoacids
   c as byte
   p as double
end type

const IUB_LEN = 15
dim iub(14) as aminoacids
iub(0).c = 97 : iub(0).p = 0.27
iub(1).c = 99 : iub(1).p = 0.12
iub(2).c = 103 : iub(2).p = 0.12
iub(3).c = 116 : iub(3).p = 0.27
iub(4).c = 66 : iub(4).p = 0.02
iub(5).c = 68 : iub(5).p = 0.02
iub(6).c = 72 : iub(6).p = 0.02
iub(7).c = 75 : iub(7).p = 0.02
iub(8).c = 77 : iub(8).p = 0.02
iub(9).c = 78 : iub(9).p = 0.02
iub(10).c = 82 : iub(10).p = 0.02
iub(11).c = 83 : iub(11).p = 0.02
iub(12).c = 86 : iub(12).p = 0.02
iub(13).c = 87 : iub(13).p = 0.02
iub(14).c = 89 : iub(14).p = 0.02

const HOMOSAPIENS_LEN = 4
dim homosapiens(3) as aminoacids
homosapiens(0).c = 97 : homosapiens(0).p = 0.3029549426680
homosapiens(1).c = 99 : homosapiens(1).p = 0.1979883004921
homosapiens(2).c = 103 : homosapiens(2).p = 0.1975473066391
homosapiens(3).c = 116 : homosapiens(3).p = 0.3015094502008

dim alu as string
alu = "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG"
alu += "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA"
alu += "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT"
alu += "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA"
alu += "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG"
alu += "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC"
alu += "AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA"

function gen_random(max as double) as double
   const IM = 139968
   const IA =  3877
   const IC =  29573
   const IIM=1/im
   static last as long = 42
   last = (last * IA + IC) mod IM
   return max * last * IIM
end function

sub makeCumulative(genelist() as aminoacids, count as integer)
   dim cp as double
   for i = 0 to count-1
      cp += genelist(i).p
      genelist(i).p = cp
   next i
end sub

function selectRandom(genelist() as aminoacids, count as integer) as byte
   dim r as double
   r = gen_random(1)
   if r < genelist(0).p then return genelist(0).c
   lo = 0
   hi = count - 1
   while hi > lo + 1
      i = (hi + lo) \ 2
      if r < genelist(i).p then hi = i else lo = i
   wend
   return genelist(hi).c
end function

sub makeRandomFasta(id as string, desc as string, genelist() as aminoacids, count as integer, n as long)
   print ">";id;" ";desc
   'spos = 1
   x = 0
   'slen = len(s)
   dim tline as zstring *61
   dim i as long
   tline=space(60)
   for i = 1 to n
      tline[x]= selectRandom(genelist(), count)
      x += 1
      if x > 59 then
         print tline
         x = 0
      end if
      'spos += 1
      'if spos > 59 then spos = 1
   next i
   tline[x]=0  
   if x>0 then print tline
    
end sub

sub makeRepeatFasta(id as string, desc as string, s as string, todo as integer)
   print ">";id;" ";desc
   spos = 1
   x = 0
   slen = len(s)
   s = s + left(s, 61)
   while todo > 0
      if todo > 60 then printlen = 60 else printlen = todo
      print mid(s, spos, printlen)
      spos += printlen
      todo -= printlen
      if spos > slen then spos -= slen
   wend
end sub

n = val(command$)
if n < 1 then n = 1000
makeCumulative(iub(), IUB_LEN)
makeCumulative(homosapiens(), HOMOSAPIENS_LEN)
makeRepeatFasta("ONE", "Homo sapiens alu", alu, n*2)
makeRandomFasta("TWO", "IUB ambiguity codes", iub(), IUB_LEN, n*3)
makeRandomFasta("THREE", "Homo sapiens frequency", homosapiens(), HOMOSAPIENS_LEN, n*5)