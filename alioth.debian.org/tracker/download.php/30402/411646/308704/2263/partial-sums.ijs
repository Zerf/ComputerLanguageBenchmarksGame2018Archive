#!/Users/aks/j602/bin/jconsole
NB. built from project: ~Projects/partial-sums
NB. partial-sums
NB.
NB. The Computer Language Benchmarks Game
NB.   http://shootout.alioth.debian.org/
NB.
NB.   contributed by Alan K. Stebbens <aks@stebbens.org>
NB.   modified by
NB.
NB.
NB. This script implements the partial-sums benchmark,
NB. as defined at:
NB.  http://shootout.alioth.debian.org/gp4/benchmark.php?test=partialsums&lang=all
NB. 
NB.
NB. The "correct" output file for this benchmark test is defined
NB. at http://shootout.alioth.debian.org/gp4/iofile.php?test=partialsums&lang=all&file=output
NB.
NB. I've included the correct output here, for easy reference:
NB.
NB. 3.000000000		(2/3)^k
NB. 314.770573775	k^-0.5
NB. 0.999960002		1/k(k+1)
NB. 30.314520404	Flint Hills 		1/( k^3 * sin(k)^2 )
NB. 42.994899946	Cookson Hills		1/( k^3 * cos(k)^2 )
NB. 10.703866769	Harmonic		1/k
NB. 1.644894068		Riemann Zeta		1/(k^2)
NB. 0.693127181		Alternating Harmonic	-1^(k+1) / k
NB. 0.785388163		Gregory			-1^(k+1) / ( 2k - 1 )


echo=: 0 0&$ @ (1!:2&2)

time =: 6!:2

n     =: >((''&-:@>@{.) { ])25000;~".>{.2}.ARGV	NB. get any argument and default it
cache =: >((''&-:@>@{.) { ])    1;~".>{.3}.ARGV	NB. get cache flag or its default

fmt  =: 0j9&":				NB. 9 digits precision sum
sin  =: 1&o.
cos  =: 2&o.

NB. CACHE makesums NUM -- flag for caching

makesums=: 4 : 0
 k =: i.y
 sum1 =: +/(2%3)^k			NB. (2/3)^k
 if. x do.		NB. argument == caching flag
  k1     =. >:k				NB. k+1
  k1sq   =. *:k1			NB. k^2
  k1cube =. k1^3			NB. k^3
  m1pow  =. _1^>:k1			NB. -1^(k+1)

  sum2 =: +/k1^_0.5			NB. k^(-0.5)
  sum3 =: +/%k1sq + k1			NB. 1/(k * (k + 1))
  sum4 =: +/%k1cube * (sin k1)^2	NB. 1/((k^3)*(sin(k)^2))
  sum5 =: +/%k1cube * (cos k1)^2	NB. 1/((k^3)*(cos(k)^2))
  sum6 =: +/%k1				NB. 1/k
  sum7 =: +/%k1sq			NB. 1/(k^2)
  sum8 =: +/m1pow%k1			NB. -1^(k+1) / k
  sum9 =: +/m1pow%<:+:k1		NB. -1^(k+1) / (2k - 1)
 else.
  sum2 =: +/_0.5^~>:k			NB. k^(-0.5)
  sum3 =: +/%([ * >:)>:k		NB. 1/(k * (k + 1))
  sum4 =: +/%((]^&3)*(*:@sin))>:k 	NB. 1/((k^3)*(sin(k)^2))
  sum5 =: +/%((]^&3)*(*:@cos))>:k	NB. 1/((k^3)*(cos(k)^2))
  sum6 =: +/%>:k			NB. 1/k
  sum7 =: +/%*:>:k			NB. 1/(k^2)
  sum8 =: +/((_1&^@>:)%])>:k		NB. -1^(k+1) / k
  sum9 =: +/((_1&^@>:)%(<:@+:))>:k	NB. -1^(k+1) / (2k - 1)
 end.
)

NB.       sumfor N
NB. CACHE sumfor N

sumfor =: 3 : 0
 1 sumfor n
 :
 x makesums y
 echo (fmt sum1),'	(2/3)^k'
 echo (fmt sum2),'	k^-0.5'
 echo (fmt sum3),'	1/k(k+1)'
 echo (fmt sum4),'	Flint Hills'
 echo (fmt sum5),'	Cookson Hills'
 echo (fmt sum6),'	Harmonic'
 echo (fmt sum7),'	Riemann Zeta'
 echo (fmt sum8),'	Alternating Harmonic'
 echo (fmt sum9),'	Gregory'
 
)

cache sumfor n				NB. caching version
exit ''

