# The Computer Language Shootout Benchmarks
# http://shootout.alioth.debian.org/
#
# contributed by Dani Nanz
# based on previous versions with input by
# Isaac Gouy, Mark Baker, S. Fermigier, Josh Hoyt

from sys import stdin

print sum(int(line) for line in stdin)
