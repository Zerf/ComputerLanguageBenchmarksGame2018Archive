--
-- nseive_bits
-- (c) Simon Marlow 2005
--
import Data.Array.IO
import System
import IO
import Monad
import Data.Bits
import Text.Printf

main = do
  as <- getArgs
  case as of
    [m] -> do let n = read m :: Int
	      test n
	      when (n >= 1) $ test (n-1)
	      when (n >= 2) $ test (n-2)
    _   -> do hPutStrLn stderr "usage: nsieve-bits M"
	      exitWith (ExitFailure 1)

test :: Int -> IO ()
test n = do
  let m = (1 `shiftL` n) * 10000
  arr <- newArray (0,m) False :: IO (IOUArray Int Bool)
  let for i count
	| count `seq` False = undefined	-- strictness hack
	| i > m = return count
	| otherwise = do
		x <- readArray arr i
		if x
		  then for (i+1) count
		  else let for' j | j > m = for (i+1) (count+1)
			    	  | otherwise = do
			     		writeArray arr j True
			     		for' (j + i)
		       in for' (i*2)
  r <- for 2 0
  printf "Primes up to %8d %8d\n" (m::Int) (r::Int)
