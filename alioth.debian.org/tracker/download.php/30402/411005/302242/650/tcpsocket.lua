-- The Great Computer Language Shootout
-- http://shootout.alioth.debian.org/
-- contributed by Mike Pall

-- Note: LuaSocket's methods always send/receive full buffers in sync mode.
-- Don't port this blindly to another language using plain socket calls.
local socket = require "socket"

local HOST, PORT, REQUEST_SIZE = "127.0.0.1", 11000, 64
local reply_mult, reply_size = { 2, 7, 1 }, { 64, 4096, 409600 }

local function client(n)
  local request, replies = string.rep(" ", REQUEST_SIZE), 0
  local sk = assert(socket.connect(HOST, PORT))
  for k,mult in ipairs(reply_mult) do
    local size = reply_size[k]
    for i=1,mult*n do
      sk:send(request)
      sk:receive(size)
      replies = replies + 1
    end
    sk:send(request)
  end
  io.write("replies: ", replies, "\tbytes: ", sk:getstats(), "\n")
  sk:close()
end

local function server(n)
  local ls = assert(socket.bind(HOST, PORT)) -- Already sets SO_REUSEADDR.
  local sk = ls:accept()
  ls:close()
  for k,mult in ipairs(reply_mult) do
    local reply = string.rep(" ", reply_size[k])
    for i=1,mult*n do
      if not sk:receive(REQUEST_SIZE) then break end
      sk:send(reply)
    end
    if not sk:receive(REQUEST_SIZE) then break end
  end
  sk:close()
end

local N = tonumber(arg and arg[1]) or 100
if N > 0 then client(N) else server(-N) end
