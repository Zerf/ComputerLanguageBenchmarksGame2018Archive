import Data.Array as Array
import Data.IntSet as Set
import Data.List as List
import System

main = do                 
    n <- getArgs >>= return . read . head
    putStrLn (printMatrix n (firstSolution n))

firstSolution n =
   head (bestFirst (initialNode n) (msPriorityFunc n mn) (msSuccessornodes n mn))
   where mn = magicNumber n
    
printMatrix n grid = do
    unlines [ show [grid ! (x,y) | x <- [1..n] ] | y <- [1..n]]
    
initialNode n = Array.listArray ((1,1),(n,n)) (repeat 0)

magicNumber :: Int -> Int
magicNumber n = n * (1 + n * n) `div` 2

gridRow grid n x y = [grid ! (xx, y) | xx <- [1..n]]
gridCol grid n x y = [grid ! (x, yy) | yy <- [1..n]]
diag1 grid n = [grid ! (i, i) | i <- [1..n]]
diag2 grid n = [grid ! (i, n - i + 1) | i <- [1..n]]
    
count n xs = length (List.filter ((==) n) xs)

possibleMoves n mn grid (x,y) =
    [ i | i <- [1..highest], not (i `Set.member` usednumbers) ]
    where
        usednumbers = Set.fromList (Array.elems grid)
        theRow = gridRow grid n x y
        theCol = gridCol grid n x y
        rowSum = sum theRow
        colSum = sum theCol
        highest = minimum [n * n, mn - rowSum, mn - colSum]

findFewestMoves n mn grid =
    if openMap /= []
        then (movelist, length movelist, mx, my)
        else ([], 0, 0, 0)
    where
        openSquares = [ (x,y) | x <- [1..n], y <- [1..n], (grid ! (x,y)) == 0]
        pm = possibleMoves n mn grid
        openMap = List.map (\(x,y) -> (pm (x,y), (x,y))) openSquares
        mycompare f g = compare ((length . fst) f) ((length . fst) g)
        (movelist, (mx, my)) = minimumBy mycompare openMap

mightBeMagic n mn x y grid =
    (rowFull == rowAdds) && (colFull == colAdds) && (if x == y then d1Full == d1Adds else True) && (if x + y == n + 1 then d2Full == d2Adds else True)
    where
        theRow = gridRow grid n x y
        theCol = gridCol grid n x y
        d1 = diag1 grid n
        d2 = diag2 grid n
        isFull xs = all (\x -> x /= 0) xs
        rowFull = isFull theRow
        colFull = isFull theCol
        d1Full = isFull d1
        d2Full = isFull d2
        rowAdds = (sum theRow) == mn
        colAdds = (sum theCol) == mn
        d1Adds = (sum d1) == mn
        d2Adds = (sum d2) == mn
                                    
msSuccessornodes n mn grid =
    List.filter (mightBeMagic n mn x y) [ grid//[((x, y), i)] | i <- possibilities ]
    where
        (possibilities, _, x, y) = findFewestMoves n mn grid

msPriorityFunc n mn grid =
    numZeros
    where
        (_, possibilitiesLen, _, _) = findFewestMoves n mn grid
        numZeros = count 0 (Array.elems grid)
       
        
bestFirst :: a -> (a -> Int) -> (a -> [a]) -> [a]
bestFirst node priorityFunc successornodes =
   bestFirstIterate priorityFunc successornodes (addQ [] (priorityFunc node) node)

-- Called only by bestFirst
bestFirstIterate :: (a -> Int) -> (a -> [a]) -> [(Int, a)] -> [a]
bestFirstIterate priorityFunc successornodes [] = []
bestFirstIterate priorityFunc successornodes (frontnode:openq) = 
   if (fst frontnode) == 0
      then (snd frontnode) : (bestFirstIterate priorityFunc successornodes openq)
      else bestFirstIterate priorityFunc successornodes (addQlist priorityFunc openq (successornodes(snd(frontnode))))

-- Add node, with associated priority, to a priority queue
addQ :: [(Int, a)] -> Int -> a -> [(Int, a)]
addQ queue newdist newnode =
   List.insertBy (\aa bb -> compare (fst aa) (fst bb)) (newdist, newnode) queue

-- Add a list of nodes to a priority queue.
--   priorityFunc:  As with bestFirst above
--   queue: The priority queue to which we are adding
--   addlist: the list of nodes (without priority) to add
addQlist :: (a -> Int) -> [(Int, a)] -> [a] -> [(Int, a)]
addQlist priorityFunc queue [] = queue
addQlist priorityFunc queue (x:addlist) =
   addQlist priorityFunc (addQ queue (priorityFunc x) x) addlist

   
