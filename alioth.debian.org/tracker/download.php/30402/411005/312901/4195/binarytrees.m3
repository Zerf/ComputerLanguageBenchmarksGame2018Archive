(* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/
 
   contributed by Leonhard Holz
*)
MODULE binarytrees EXPORTS Main;

IMPORT IO, Params, Scan;

TYPE
	Node = OBJECT
			left, right : Node;
			item : INTEGER;
		METHODS
			init(left, right: Node; item : INTEGER) := Init;
			check() : INTEGER := Check;
	END;

PROCEDURE Init(self : Node; left, right : Node; item : INTEGER) =
BEGIN
	self.left := left;
	self.right := right;
	self.item := item;
END Init;

PROCEDURE Check(self : Node) : INTEGER = 
BEGIN
	IF self.left = NIL THEN
		RETURN self.item;
	ELSE
		RETURN self.item + self.left.check() - self.right.check();
	END;
END Check;

PROCEDURE buildTree(item, depth : INTEGER) : Node =
VAR
	node : Node := NEW(Node);
BEGIN
	IF depth > 0 THEN
		node.init(buildTree(2 * item - 1, depth - 1), buildTree(2 * item, depth - 1), item);
	ELSE
		node.init(NIL, NIL, item);
	END;
	RETURN node;
END buildTree;

PROCEDURE powerOfTwo(n : INTEGER) : INTEGER = 
VAR
	p : INTEGER := 1;
BEGIN
	FOR i := 1 TO n DO
		p := p * 2;
	END;
	RETURN p;
END powerOfTwo;

VAR
	longLivedTree : Node;
	minDepth : INTEGER := 4;
	maxDepth : INTEGER := 10;
	stretchDepth, check, iterations : INTEGER;

BEGIN
	IF Params.Count > 1 THEN
		TRY
			maxDepth := Scan.Int(Params.Get(1));
			IF minDepth + 2 > maxDepth THEN
				maxDepth := minDepth + 2;
			END;
		EXCEPT ELSE END;
	END;
	stretchDepth := maxDepth + 1;

	check := buildTree(0, stretchDepth).check();
	IO.Put("stretch tree of depth ");
	IO.PutInt(stretchDepth);
	IO.Put("\t check: ");
	IO.PutInt(check);
	IO.Put("\n");

	longLivedTree := buildTree(0, maxDepth);

	FOR depth := minDepth TO maxDepth BY 2 DO
		check := 0;
		iterations := powerOfTwo(maxDepth - depth + minDepth);
		FOR i := 1 TO iterations DO
			check := check + buildTree(i, depth).check();
			check := check + buildTree(-i, depth).check();
		END;
		IO.PutInt(iterations * 2);
		IO.Put("\t trees of depth ");
		IO.PutInt(depth);
		IO.Put("\t check: ");
		IO.PutInt(check);
		IO.Put("\n");
	END;

	IO.Put("long lived tree of depth ");
	IO.PutInt(maxDepth);
	IO.Put("\t check: ");
	IO.PutInt(longLivedTree.check());
	IO.Put("\n");
END binarytrees.
