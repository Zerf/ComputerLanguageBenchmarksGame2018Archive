/* doloop.c
 * 2008-03-31, Joe Tucek
 * Replace the repeat.sh script with something less CPU hungry.
 * NOTE - you must provide a specific path; there is no path search!
 *
 * I *highly* recommend using a count of above 2000; randomness in the
 * scheduling can cause up to 200ms of inaccuracy, and the fast end of the
 * spectrum is taking under 200ms to complete at 200 iterations.  N=10000 
 * would be better.
 *
 * If run as root, it will set itself to realtime scheduling, which will
 * give more consistent results on a system which is not necessarily 100%
 * idle.  BUT it could conceivably cause problems with some language
 * runtimes. As in a completely nonresponsive hardlocked system.
 * Or perhaps not.  YMMV, user assumes all risk.
 */

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char ** argv){
  int count;
  int i;
  int res;
  struct sched_param sc;
  
  if(argc<3){
    fprintf(stderr, "Need at least a count and a program to run\n");
    exit(1);
  }
  if(argc>=8){
    fprintf(stderr, "Too many arguments; we'd drop the end ones\n");
    fprintf(stderr, "So we're aborting instead\n");
    exit(2);
  }

  count = atoi(argv[1]);
  
  /*Redirect stdin, stdout, and stderr to /dev/null*/
  close(0);
  close(1);
  close(2);
  open("/dev/null", O_RDWR);
  open("/dev/null", O_RDWR);
  open("/dev/null", O_RDWR);

  /*Realtime scheduling; if effective user isn't root then this doesn't do 
    anything*/
  sc.sched_priority=2;
  sched_setscheduler(0, SCHED_RR, &sc);


  /*Different number of arguments (for Java and the like)*/
  

  if(argc==3){
    for(i=0; i<count; i++){
      res = fork();
      if(res){
	waitpid(res, 0, 0);
      }else{
	execl(argv[2], argv[2], 0);
      }
    }
  }else if(argc==4){
    for(i=0; i<count; i++){
      res = fork();
      if(res){
	waitpid(res, 0, 0);
      }else{
	execl(argv[2], argv[2], argv[3], 0);
      }
    }
  }else if(argc==5){
    for(i=0; i<count; i++){
      res = fork();
      if(res){
	waitpid(res, 0, 0);
      }else{
	execl(argv[2], argv[2], argv[3], argv[4], 0);
      }
    }
  }else if(argc==6){
    for(i=0; i<count; i++){
      res = fork();
      if(res){
	waitpid(res, 0, 0);
      }else{
	execl(argv[2], argv[2], argv[3], argv[4], argv[5], 0);
      }
    }
  }else if(argc==7){
    for(i=0; i<count; i++){
      res = fork();
      if(res){
	waitpid(res, 0, 0);
      }else{
	execl(argv[2], argv[2], argv[3], argv[4], argv[5], argv[6], 0);
      }
    }
  }else{
    /*Should be unreachable*/
    exit(3);
  }

  return 0;

}
