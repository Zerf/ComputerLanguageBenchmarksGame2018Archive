#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
double Spot,Strike,Time,Volatility,Rate,ST[4],sum=0.0;
double Z1,Z2,N1,N2,MCPrice;
double TruePrice=10.0022,Error,factor,Pi=3.1415926;
int i,j,Niterations=1000000;	

sscanf(argv[1],"%d",&Niterations);

Spot=100; Strike=105;Time=1.0;Volatility=0.25; Rate=0.05;
factor=exp((Rate-Volatility*Volatility/2)*Time);
  Z1=(double)rand()/(1.0+RAND_MAX);  

for (i=0;i<Niterations;i+=4)
{
  Z1=(double)rand()/(1.0+RAND_MAX);  
  Z2=(double)rand()/(1.0+RAND_MAX);
	N1=sqrt(-2*log(Z1))*sin(2*Pi*Z2);
	N2=sqrt(-2*log(Z1))*cos(2*Pi*Z2);
// antithetic simulation, if N1 is normal, -N1 is also normal
ST[0]=Spot*factor*exp(Volatility*N1*sqrt(Time));
ST[1]=Spot*factor*exp(-Volatility*N1*sqrt(Time));
ST[2]=Spot*factor*exp(Volatility*N2*sqrt(Time));
ST[3]=Spot*factor*exp(-Volatility*N2*sqrt(Time));

//printf("%g %g %g %g\n",ST[0],ST[1],ST[2],ST[3]);

 for (j=0;j<4;j++)  
 {  if (ST[j]>Strike)    
		sum+=(double)(ST[j]-Strike);
 }
} 
  MCPrice=(double)exp(-Rate*Time)*sum/Niterations;
  Error=fabs(MCPrice-TruePrice)/TruePrice;
  printf("MonteCarlo Price(%d) = %g Error=%g%%\n",Niterations,MCPrice,100*Error);
}

