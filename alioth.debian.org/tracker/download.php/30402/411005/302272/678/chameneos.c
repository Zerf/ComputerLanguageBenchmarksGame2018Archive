/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Lester Vecsey */

#include <windows.h>

#include <stdio.h>
#include <assert.h>

enum texture { blue, red, yellow, faded };

#define tmask 3
#define metshift 2

int compTexture(int a, int b) {
	
	if (a==b) return a;

	switch(a) {
		case blue: return b == red ? yellow : red;
		case red: return b == blue ? yellow : blue;
		case yellow: return b == blue ? red : blue;
		default: return a;
		}

	return 0;

	}

enum { empty, first, filled };

struct Server {

	HINSTANCE hInst; HWND hWnd; HANDLE **e; CRITICAL_SECTION *cs;
	DWORD num_Chameneos, threads_active; LONG state, remaining_meetings;
	int *states, meetings, aEventsMask;

	};

struct worker_clamp { struct Server *s; int talk_to, *i; };

#define least(x,h1,y,h2) ( (x) < (y) ? h1 : h2 )

int process(int *m, int *o) {

	int count = 2, *t;

	assert(m != NULL && m!=o);

	for ( ; count-- > 0; t = o, o = m, m = t) {
	
		(*m) = ( ((*o)&tmask) == faded) ? faded | ((*m) & (~tmask)) : ((*m)&(~tmask)) | compTexture((*m)&tmask, (*o)&tmask);
		if ( ((*m) & tmask) != faded ) *m = ((*m)&tmask) | ((((*m) >> metshift) + 1) << metshift);

		}

	return 0;

	}

DWORD WINAPI f_worker(LPVOID lpParam) {

	struct worker_clamp *w = (struct worker_clamp *) lpParam;
	HANDLE *phs[2], hset[2], **pp = phs, *h = hset, *e, t;
	DWORD d;
	
	assert(w!=NULL && w->i != NULL);

	for (d = 1, e = *(w->s->e); d <= 4; d<<=1, e++) if (w->talk_to&d) { *h++ = *e; *pp++ = e; }

	assert(hset[0] == *phs[0] && hset[1] == *phs[1]);

	for ( e = *(w->s->e); ((*(w->i)) & tmask) != faded && w->s->threads_active > 0; t = hset[0], hset[0] = hset[1], hset[1] = t) {

		if ( (d = WaitForMultipleObjectsEx(2, hset, FALSE, INFINITE, FALSE)) <= WAIT_OBJECT_0 + 1) {

			EnterCriticalSection(w->s->cs);
	
			if (w->s->remaining_meetings <= 0) {
				(*w->i) = ((*w->i)&(~tmask)) | faded;
				w->s->threads_active--;
				}
			else {

			process(w->i, w->s->states + (phs[d-WAIT_OBJECT_0] - e));

			if (!SetEvent(hset[d-WAIT_OBJECT_0])) w->s->threads_active = 0;

			}

			w->s->remaining_meetings--;

			LeaveCriticalSection(w->s->cs);
				
			}

		}

	return 0;

	}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) {

	HANDLE *e, *end;
	CRITICAL_SECTION cs[3], *c = cs;
	int is[] = { blue, red, yellow }, *i, *ie, bit=1;
	struct Server s = { NULL, NULL, &e, cs, 3, 0, empty, 100, is, 0, 6 };
	struct worker_clamp w_clamps[] = { {&s,6,is}, {&s,5,is+1}, {&s,3,is+2} }, *w = w_clamps;

	if (hInstance == hPrevInstance) return -1;

	if (lpCmdLine[0] >= '0') s.remaining_meetings = strtol(lpCmdLine, NULL, 10);

	if ( (e = (HANDLE *) malloc(sizeof(HANDLE) * s.num_Chameneos)) == NULL) return -1;

	for (end = e + s.num_Chameneos; e < end; e++,bit<<=1) { *e = CreateEvent(NULL,1,(s.aEventsMask&bit)>0,NULL); } e-=s.num_Chameneos;

	{ HANDLE threads[] = {NULL,NULL,NULL}, *start = threads; while ( ++s.threads_active <= s.num_Chameneos) {

		InitializeCriticalSection(c++);
		if ( (*start++ = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) f_worker, (LPVOID) w++, 0, NULL)) == NULL) return -1;

		}

	WaitForMultipleObjects(s.num_Chameneos, threads, TRUE, INFINITE); }

	for (i = is, ie = i + s.num_Chameneos; i < ie; i++) s.meetings += *i >> metshift;
	
	printf("%d\n", s.meetings);

	return nShowCmd = 0;

	}
