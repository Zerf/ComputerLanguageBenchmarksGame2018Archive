/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/
 
   from RC474.pdf document by Claude Kaiser, Jean-Fran�ois Pradat-Peyre

*/

#define NB_CHAMENEOS 3

typedef int idChameneos, colour;
enum { blue, red, yellow, faded };

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

sem_t AtMostTwo, Mutex, SemPriv; 

int FirstCall = 1, remaining_meetings; 
colour AColour, BColour; 

colour Cooperation(colour c) {

	colour otherColour;

	sem_wait(&AtMostTwo); // limits the number of partners 
	if (remaining_meetings <= 0) { sem_post(&AtMostTwo); return faded; }
	sem_wait(&Mutex);

	// user programmed mutual exclusion = setting the lock 
	if ( FirstCall ) {
		if (remaining_meetings > 0) {
			AColour = c; FirstCall = 0; 

			// the next call will be considered as a second one 
			sem_post(&Mutex); sem_wait(&SemPriv); // waiting for the lock 

			otherColour = BColour;
			remaining_meetings--;
			sem_post(&Mutex); // releases the lock since the rendez vous ends 
			sem_post(&AtMostTwo); sem_post(&AtMostTwo); // allows a new pair
			} 
			else { otherColour = faded; }
		}
		
		else { // this is the second chameneos of the pair 
			FirstCall = 1;
			BColour = c;
			otherColour = AColour;
			
			// the next call will start a new meeting 
			sem_post(&SemPriv); // passes the lock to its mate 
		}
	
	return otherColour;

	}

void initCooperation(void) {

	sem_init (&AtMostTwo, 0, 2); 
	sem_init (&Mutex, 0, 1); 
	sem_init (&SemPriv , 0, 0); 

	}

int compTexture(int a, int b) {
	
	if (a==b) return a;

	switch(a) { 
		case blue: return b == red ? yellow : red;
		case red: return b == blue ? yellow : blue;
		case yellow: return b == blue ? red : blue;
		}


	return a;

	}

struct mpack { char *args; int num_meetings; };

void chameneosCode(void *args) {

	struct mpack *m = (struct mpack *) args;
	idChameneos myId; 
	colour myColour, oldColour, otherColour;
	
	sscanf(m->args, "%d %d", &myId, &myColour);

	while (myColour != faded) {
		otherColour = Cooperation(myColour);
		if (otherColour == faded) myColour = otherColour;
		else { m->num_meetings++; myColour = compTexture(myColour, otherColour); }
		oldColour = myColour;
		}

	}

int main(int argc, char *argv[]) {

	struct mpack mset[] = { { NULL, 0 }, { NULL, 0 }, { NULL, 0 } };
	colour tabColour[NB_CHAMENEOS] = { blue, red, yellow };
	char theArgs[255][NB_CHAMENEOS];
	pthread_t tabPid[NB_CHAMENEOS];
	int i;

	remaining_meetings = argc>1 ? strtol(argv[1], NULL, 10) : 100;

	initCooperation(); 

	for (i=0; i < NB_CHAMENEOS; i++) {
		sprintf ( theArgs[ i ], "%d %d", i, tabColour[i]); mset[i].args = theArgs[i];
		pthread_create (& tabPid[ i ], NULL, (void *(*)(void*)) chameneosCode, mset + i);
		}

	for ( i=0; i < NB_CHAMENEOS; i++) 
		pthread_join (tabPid[i], NULL);

	printf("%d\n", mset[0].num_meetings + mset[1].num_meetings + mset[2].num_meetings);

	return 0;

	}
