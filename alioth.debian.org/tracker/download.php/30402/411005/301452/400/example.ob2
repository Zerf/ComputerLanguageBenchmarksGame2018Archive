(* 
   Example of split_file.bash
   
   $ bash split_file.bash example.ob2
   $ oo2c -M example.ob2
   $ ./bin/example
   
*)


(* SPLITFILE=example.ob2 *)
MODULE example;
IMPORT B := Bottle, P := PressurizedBottle, Out;

VAR
   b1, b2, b3, b4, b5, b6, b7, b8, b9, b0 : B.Bottle;
   p1, p2, p3, p4, p5, p6, p7, p8, p9, p0 : P.Bottle;

   n, i, check : LONGINT;
   

PROCEDURE BottleCheck (a1,a2,a3,a4,a5: B.Bottle; i: LONGINT): LONGINT;
VAR
   c : LONGINT;
BEGIN
   B.Cycle(a1); B.Cycle(a2); B.Cycle(a3); B.Cycle(a4); B.Cycle(a5);

   c := i MOD 2;
   RETURN B.Check(a1,c) + B.Check(a2,c) + B.Check(a3,c) +
            B.Check(a4,c) + B.Check(a5,c);
END BottleCheck;


PROCEDURE PressurizedBottleCheck (a1,a2,a3,a4,a5: P.Bottle; i: LONGINT): LONGINT;
VAR
   c : LONGINT;
BEGIN
   P.Cycle(a1); P.Cycle(a2); P.Cycle(a3); P.Cycle(a4); P.Cycle(a5);

   c := i MOD 2;
   RETURN P.Check(a1,c) + P.Check(a2,c) + P.Check(a3,c) +
            P.Check(a4,c) + P.Check(a5,c);
END PressurizedBottleCheck;


BEGIN
   n := 100;

   b1 := B.New(1); b2 := B.New(2); b3 := B.New(3); b4 := B.New(4); b5 := B.New(5);
   b6 := B.New(6); b7 := B.New(7); b8 := B.New(8); b9 := B.New(9); b0 := B.New(0);

   p1 := P.New(1); p2 := P.New(2); p3 := P.New(3); p4 := P.New(4); p5 := P.New(5);
   p6 := P.New(6); p7 := P.New(7); p8 := P.New(8); p9 := P.New(9); p0 := P.New(0);

   check := 0;
   FOR i := 1 TO n DO
      INC(check, BottleCheck(b1,b2,b3,b4,b5,i));
      INC(check, BottleCheck(b6,b7,b8,b9,b0,i));

      INC(check, PressurizedBottleCheck(p1,p2,p3,p4,p5,i));
      DEC(check, PressurizedBottleCheck(p6,p7,p8,p9,p0,i));
   END;

   Out.Int(check,1); Out.Ln;
END example.



(* SPLITFILE=src/Bottle.Mod *)
MODULE Bottle;
IMPORT Lib := BottleLibrary, EmptyState, FullState, SealedState;

TYPE
   Bottle * = Lib.Bottle;


PROCEDURE New * (i: INTEGER): Bottle;
VAR
   b: Bottle;
BEGIN
   NEW(b); Lib.Initialize(b,Lib.Empty,i); RETURN b;
END New;

PROCEDURE Dispatch (b: Bottle);
BEGIN
   CASE b.state.tag OF
        EmptyState.Tag : EmptyState.Next(b);
      | FullState.Tag : FullState.Next(b);
      | SealedState.Tag : SealedState.Next(b);
   END;
END Dispatch;

PROCEDURE Empty (b: Bottle);
BEGIN Dispatch(b); END Empty;

PROCEDURE Fill (b: Bottle);
BEGIN Dispatch(b); END Fill;

PROCEDURE Seal (b: Bottle);
BEGIN Dispatch(b); END Seal;

PROCEDURE Cycle * (b: Bottle);
BEGIN Fill(b); Seal(b); Empty(b); END Cycle;

PROCEDURE Check * (b: Bottle; c: LONGINT): LONGINT;
BEGIN RETURN b.state.tag + b.id + c; END Check;

END Bottle.



(* SPLITFILE=src/BottleLibrary.Mod *)
MODULE BottleLibrary;

TYPE
   State * = POINTER TO StateDesc;
   StateDesc = RECORD
      tag - : INTEGER;
   END;

   Bottle * = POINTER TO BottleDesc;
   BottleDesc = RECORD
      state - : State;
      id - : INTEGER;
   END;


VAR
   Empty * ,Full * ,Sealed * : State;


PROCEDURE Initialize * (VAR b: Bottle; s: State; i: INTEGER);
BEGIN b.state := s; b.id := i; END Initialize;

PROCEDURE SetState * (b: Bottle; s: State);
BEGIN b.state := s; END SetState;

PROCEDURE NewState * (tag: INTEGER): State;
VAR
   s: State;
BEGIN
   NEW(s); s.tag := tag; RETURN s;
END NewState;

END BottleLibrary.




(* SPLITFILE=src/EmptyState.Mod *)
MODULE EmptyState;
IMPORT Lib := BottleLibrary;

CONST Tag * = 1;

PROCEDURE Next * (b: Lib.Bottle);
BEGIN
   Lib.SetState(b, Lib.Full);
END Next;

BEGIN
   Lib.Empty := Lib.NewState(Tag);
END EmptyState.



(* SPLITFILE=src/FullState.Mod *)
MODULE FullState;
IMPORT Lib := BottleLibrary;

CONST Tag * = 2;

PROCEDURE Next * (b: Lib.Bottle);
BEGIN
   Lib.SetState(b, Lib.Sealed);
END Next;

BEGIN
   Lib.Full := Lib.NewState(Tag);
END FullState.



(* SPLITFILE=src/SealedState.Mod *)
MODULE SealedState;
IMPORT Lib := BottleLibrary;

CONST Tag * = 3;

PROCEDURE Next * (b: Lib.Bottle);
BEGIN
   Lib.SetState(b, Lib.Empty);
END Next;

BEGIN
   Lib.Sealed := Lib.NewState(Tag);
END SealedState.



(* SPLITFILE=src/PressurizedBottle.Mod *)
MODULE PressurizedBottle;
IMPORT Lib := PressurizedBottleLibrary, UnpressurizedEmptyState,
   UnpressurizedFullState, PressurizedUnsealedState, PressurizedSealedState;

TYPE
   Bottle * = Lib.Bottle;


PROCEDURE New * (i: INTEGER): Bottle;
VAR
   b: Bottle;
BEGIN
   NEW(b); Lib.Initialize(b,Lib.UnpressurizedEmpty,i); RETURN b;
END New;

PROCEDURE Dispatch (b: Bottle);
BEGIN
   CASE b.state.tag OF
        UnpressurizedEmptyState.Tag : UnpressurizedEmptyState.Next(b);
      | UnpressurizedFullState.Tag : UnpressurizedFullState.Next(b);
      | PressurizedUnsealedState.Tag : PressurizedUnsealedState.Next(b);
      | PressurizedSealedState.Tag : PressurizedSealedState.Next(b);
   END;
END Dispatch;

PROCEDURE Empty (b: Bottle);
BEGIN Dispatch(b); END Empty;

PROCEDURE Fill (b: Bottle);
BEGIN Dispatch(b); END Fill;

PROCEDURE Seal (b: Bottle);
BEGIN Dispatch(b); END Seal;

PROCEDURE Pressurize (b: Bottle);
BEGIN Dispatch(b); END Pressurize;

PROCEDURE Cycle * (b: Bottle);
BEGIN Fill(b); Pressurize(b); Seal(b); Empty(b); END Cycle;

PROCEDURE Check * (b: Bottle; c: LONGINT): LONGINT;
BEGIN RETURN b.state.tag + b.id + c; END Check;

END PressurizedBottle.



(* SPLITFILE=src/PressurizedBottleLibrary.Mod *)
MODULE PressurizedBottleLibrary;
IMPORT Lib := BottleLibrary;

TYPE
   State * = Lib.State;
   Bottle * = Lib.Bottle;

VAR
   UnpressurizedEmpty * ,UnpressurizedFull *
   ,PressurizedUnsealed * ,PressurizedSealed * : State;


PROCEDURE Initialize * (VAR b: Bottle; s: State; i: INTEGER);
BEGIN Lib.Initialize(b,s,i); END Initialize;

PROCEDURE SetState * (b: Bottle; s: State);
BEGIN Lib.SetState(b,s); END SetState;

PROCEDURE NewState * (tag: INTEGER): State;
BEGIN RETURN Lib.NewState(tag); END NewState;

END PressurizedBottleLibrary.



(* SPLITFILE=src/UnpressurizedEmptyState.Mod *)
MODULE UnpressurizedEmptyState;
IMPORT Lib := PressurizedBottleLibrary;

CONST Tag * = 4;

PROCEDURE Next * (b: Lib.Bottle);
BEGIN
   Lib.SetState(b, Lib.UnpressurizedFull);
END Next;

BEGIN
   Lib.UnpressurizedEmpty := Lib.NewState(Tag);
END UnpressurizedEmptyState.



(* SPLITFILE=src/UnpressurizedFullState.Mod *)
MODULE UnpressurizedFullState;
IMPORT Lib := PressurizedBottleLibrary;

CONST Tag * = 5;

PROCEDURE Next * (b: Lib.Bottle);
BEGIN
   Lib.SetState(b, Lib.PressurizedUnsealed);
END Next;

BEGIN
   Lib.UnpressurizedFull := Lib.NewState(Tag);
END UnpressurizedFullState.



(* SPLITFILE=src/PressurizedUnsealedState.Mod *)
MODULE PressurizedUnsealedState;
IMPORT Lib := PressurizedBottleLibrary;

CONST Tag * = 6;

PROCEDURE Next * (b: Lib.Bottle);
BEGIN
   Lib.SetState(b, Lib.PressurizedSealed);
END Next;

BEGIN
   Lib.PressurizedUnsealed := Lib.NewState(Tag);
END PressurizedUnsealedState.



(* SPLITFILE=src/PressurizedSealedState.Mod *)
MODULE PressurizedSealedState;
IMPORT Lib := PressurizedBottleLibrary;

CONST Tag * = 7;

PROCEDURE Next * (b: Lib.Bottle);
BEGIN
   Lib.SetState(b, Lib.UnpressurizedEmpty);
END Next;

BEGIN
   Lib.PressurizedSealed := Lib.NewState(Tag);
END PressurizedSealedState.
