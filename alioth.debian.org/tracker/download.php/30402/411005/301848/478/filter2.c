/*
   Each program should be implemented to do the same thing as this C
   program.  The purpose of this test is see how well suited the
   language is for what might be described as a typical scientific
   data analysis task where one has to deal with large numbers of
   independent samples.

   This program uses the same random number generator as other tests
   to create 3 double precision arrays, X, Y, and E, each
   with 100000 elements.  The arrays X, Y represents the (x,y)
   positions of particles in a plane, and the E array represents the
   energies of the corresponding particles.  For this test, the
   positions of the particles will be uniformly distributed in a 10x10
   box and energies uniformly distributed in the range 0-20.

   Let (X0,Y0) be the mean position of the particles and consider a
   series of N uniformly spaced circles centered upon this position.
   The circles will have radii (dR, 2*dR, ..., N*dR) where N*dR is
   equal to the distance to the furthest particle from (X0,Y0).  The
   goal is to find the index of the radial shell that contains largest
   energy density.  The energy density is defined as the ratio of the
   sum of the energies of the particles in the shell to the area of
   the shell.

   For a default value of 100 shells (N=100), the program should print:
   
      6 216 13021.894
      
   indicating that the 6th shell was the one with the highest energy
   density (13021.894) and contained 216 particles.  (Shells are
   numbered from 0).   
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define IM 139968
#define IA 3877
#define IC 29573

#define MAX_X 10.0
#define MAX_Y 10.0
#define MAX_E 20.0
#define NUM_POINTS 100000
#define PI M_PI

static double gen_random(double max) 
{
   static long last = 42;
   last = (last * IA + IC) % IM;
   return( max * last / IM );
}

static void gen_data (double *x, double *y, double *e, unsigned int n)
{
   unsigned int i;
   for (i = 0; i < n; i++)
     {
	x[i] = gen_random (MAX_X);
	y[i] = gen_random (MAX_Y);
	e[i] = gen_random (MAX_E);
     }
}

static double compute_mean (double *x, unsigned int n)
{
   double sum = 0.0;
   unsigned int i;
   
   for (i = 0; i < n; i++)
     sum += x[i];
   
   return sum/n;
}

static void compute_radii (double *x, double *y, double *r, unsigned int n)
{
   unsigned int i;
   double x_0, y_0;
   
   x_0 = compute_mean (x, n);
   y_0 = compute_mean (y, n);

   for (i = 0; i < n; i++)
     {
	r[i] = hypot (x[i] - x_0, y[i] - y_0);
     }
}

int main(int argc, char *argv[]) 
{
   int N = ((argc == 2) ? atoi(argv[1]) : 100);
   double *x, *y, *e, *r;
   double max_energy_density, rmax, dr, r0;
   unsigned int max_number;
   int i, imax;

   x = (double *) malloc (NUM_POINTS * sizeof (double));
   y = (double *) malloc (NUM_POINTS * sizeof (double));
   e = (double *) malloc (NUM_POINTS * sizeof (double));
   
   gen_data (x, y, e, NUM_POINTS);

   r = (double *) malloc (NUM_POINTS * sizeof (double));
   compute_radii (x, y, r, NUM_POINTS);
   rmax = 0;
   for (i = 0; i < NUM_POINTS; i++)
     {
	if (r[i] > rmax) 
	  rmax = r[i];
     }

   dr = rmax/N;
   r0 = 0.0;
   max_energy_density = 0.0;
   max_number = 0;
   imax = 0;
   for (i = 0; i < N; i++)
     {
	double r1 = r0 + dr;
	unsigned int num = 0;
	double area, energy_density, sum = 0.0;
	int j;

	for (j = 0; j < NUM_POINTS; j++)
	  {
	     if ((r0 <= r[j]) && (r[j] < r1))
	       {
		  sum += e[j];
		  num++;
	       }
	  }
	area = PI * dr * (r1+r0);
	energy_density = sum / area;
	if (energy_density > max_energy_density)
	  {
	     max_energy_density = energy_density;
	     imax = i;
	     max_number = num;
	  }
	r0 = r1;
     }
   
   (void) fprintf (stdout, "%d %d %7.3f\n", imax, max_number, max_energy_density);

   free (x);
   free (y);
   free (e);
   free (r);
   return 0;
}

