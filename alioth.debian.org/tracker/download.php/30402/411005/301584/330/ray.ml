(* Written by Jon Harrop, 2005
   Compile: ocamlopt -unsafe -inline 100 ray.ml -o ray *)

let delta = sqrt epsilon_float and pi = 4. *. atan 1.

type vec = {x:float; y:float; z:float}
let ( *| ) s r = {x = s *. r.x; y = s *. r.y; z = s *. r.z}
let ( +| ) a b = {x = a.x +. b.x; y = a.y +. b.y; z = a.z +. b.z}
let ( -| ) a b = {x = a.x -. b.x; y = a.y -. b.y; z = a.z -. b.z}
let dot a b = a.x *. b.x +. a.y *. b.y +. a.z *. b.z
let unitise r = (1. /. sqrt (dot r r)) *| r

type ray = { orig: vec; dir: vec }

type scene = Sphere of vec * float | Group of vec * float * scene array

let ray_sphere ray center radius =
  let v = center -| ray.orig in
  let b = dot v ray.dir in
  let disc = b *. b -. dot v v +. radius *. radius in
  if disc < 0. then infinity else
    let disc = sqrt disc in
    (fun t2 -> if t2 < 0. then infinity else
       ((fun t1 -> if t1 > 0. then t1 else t2) (b -. disc))) (b +. disc)

let intersect ray scene =
  let rec of_scene ((l, _) as first) = function
      Sphere (center, radius) ->
	let l' = ray_sphere ray center radius in
	if l' >= l then first else
	  l', unitise (ray.orig +| l' *| ray.dir -| center)
    | Group (center, radius, scenes) ->
	let l' = ray_sphere ray center radius in
	if l' >= l then first else Array.fold_left of_scene first scenes in
  of_scene (infinity, {x=0.; y=0.; z=0.}) scene

let rec ray_trace weight light ray scene =
  let lambda, n = intersect ray scene in
  if lambda = infinity then 0. else
    let g = -. dot n light in
    if g <= 0. then 0. else
      match intersect { orig = ray.orig +| lambda *| ray.dir +| delta *| n;
			dir = {x=0.; y=0.; z=0.} -| light } scene with
	l, _ when l = infinity -> g | _ -> 0.

let rec create level r (x, y, z) =
  let obj = Sphere ({x=x; y=y; z=z}, r) in
  if level = 1 then obj else
    let aux l (x', y', z') =
      create (level-1) (0.5 *. r) (x -. x', y +. y', z +. z') :: l in
    let objs = let r' = 3. *. r /. sqrt 12. in
    Array.fold_left aux [obj]
      [|-.r', r', -.r'; r', r', -.r'; -.r', r', r'; r', r', r'|] in
    Group ({x=x; y=y; z=z}, 3. *. r, Array.of_list objs)

let () =
  let level = match Sys.argv with [| _; l |] -> int_of_string l | _ -> 6 in
  let scene = create level 1. (0., -1., 0.) in
  let w, h = 512, 512 and ss = 4 in

  Printf.printf "P2\n%d %d\n255\n" w h;
  for y = h - 1 downto 0 do
    for x = 0 to w - 1 do
      let g = ref 0. in
      for dx = 0 to ss - 1 do
	for dy = 0 to ss - 1 do
	  let ray =
	    {orig = {x=0.; y=0.; z= -4.};
	     dir = unitise {x = float (x - w / 2) +. float dx /. float ss;
			    y = float (y - h / 2) +. float dy /. float ss;
			    z = float (max w h)} } in
	  g := !g +. ray_trace 1. (unitise {x= -1.; y= -3.; z=2.}) ray scene;
	done;
      done;
      Printf.printf "%d " (int_of_float (min 255. 256.*. !g /. float (ss*ss)));
    done;
    Printf.printf "\n";
  done;
