// Written by Jon Harrop, 2005
// Compile: g++ -O3 ray.cpp -o ray

#include <vector>
#include <iostream>
#include <limits>
#include <cmath>

using namespace std;

numeric_limits<double> dbl;
double delta = sqrt(dbl.epsilon()), infinity = dbl.infinity(), pi = M_PI;

struct Vec { // 3D vector
  double x, y, z;
  Vec(double x2, double y2, double z2) : x(x2), y(y2), z(z2) {}
};
Vec operator+(Vec a, Vec b) { return Vec(a.x + b.x, a.y + b.y, a.z + b.z); }
Vec operator-(Vec a, Vec b) { return Vec(a.x - b.x, a.y - b.y, a.z - b.z); }
Vec operator*(double a, Vec b) { return Vec(a * b.x, a * b.y, a * b.z); }
double dot(Vec a, Vec b) { return a.x*b.x + a.y*b.y + a.z*b.z; }
Vec unitise(Vec a) { return (1 / sqrt(dot(a, a))) * a; }

struct Ray { Vec orig, dir; Ray(Vec o, Vec d) : orig(o), dir(d) {} };

struct Scene { // Abstract base class representing a scene
  virtual void intersect(double &, Vec &, Ray) = 0;
  virtual void del() {};
};

struct Sphere : public Scene { // Derived class representing a sphere
  Vec center;
  double radius;

  Sphere(Vec c, double r) : center(c), radius(r) {}

  double ray_sphere(Ray ray) { // Intersection of a ray with a sphere
    Vec v = center - ray.orig;
    double b = dot(v, ray.dir), disc = b*b - dot(v, v) + radius * radius;
    if (disc < 0) return infinity;
    double d = sqrt(disc), t2 = b + d;
    if (t2 < 0) return infinity;
    double t1 = b - d;
    return (t1 > 0 ? t1 : t2);
  }

  void intersect(double &lambda, Vec &normal, Ray ray) {
    double l = ray_sphere(ray);
    if (l >= lambda) return;
    lambda = l;
    normal = unitise(ray.orig + l * ray.dir - center);
  }
};

struct Group : public Scene { // Derived class representing a group of scenes
  Sphere bound;
  vector<Scene *> objs;

  Group(Sphere b) : bound(b), objs(0) {}

  void del() {
    for (vector<Scene *>::iterator it=objs.begin(); it!=objs.end(); ++it)
      delete *it;
  }

  void intersect(double &lambda, Vec &normal, Ray ray) {
    double l = bound.ray_sphere(ray);
    if (l >= lambda) return;
    for (vector<Scene *>::iterator it=objs.begin(); it!=objs.end(); ++it)
      (*it)->intersect(lambda, normal, ray);
  }
};

double ray_trace(double weight, Vec light, Ray ray, Scene *scene) {
  double lambda = infinity;
  Vec normal(0, 0, 0);
  scene->intersect(lambda, normal, ray);
  if (lambda == infinity) return 0;
  Vec o = ray.orig + lambda * ray.dir + delta * normal;
  double g = -dot(normal, light), l = infinity;
  if (g <= 0) return 0.;
  scene->intersect(l, normal, Ray(o, Vec(0, 0, 0) - light));
  return (l == infinity ? g : 0);
}

Scene *create(int level, double r, double x, double y, double z) {
  Sphere *sphere = new Sphere(Vec(x, y, z), r);
  if (level == 1) return sphere;
  Group group = Group(Sphere(Vec(x, y, z), 3*r));
  group.objs.push_back(sphere);
  double rn = 3*r/sqrt(12.);
  for (int dz=-1; dz<=1; dz+=2)
    for (int dx=-1; dx<=1; dx+=2)
      group.objs.push_back(create(level-1, r/2, x - dx*rn, y + rn, z - dz*rn));
  return (new Group(group));
}

int main(int argc, char *argv[]) {
  int level = (argc==2 ? atoi(argv[1]) : 6), w = 512, h = 512, ss = 4;
  Scene *scene=create(level, 1, 0, -1, 0); // Build the scene

  cout << "P2\n" << w << " " << h << "\n256\n";
  for (int y=h-1; y>=0; --y) {
    for (int x=0; x<w; ++x) {
      double g=0;
      for (int dx=0; dx<ss; ++dx)
	for (int dy=0; dy<ss; ++dy) {
	  Vec d(x+double(dx)/ss-w/2, y+double(dy)/ss-h/2, max(w, h));
	  g += ray_trace(1, unitise(Vec(-1, -3, 2)), Ray(Vec(0, 0, -4),
							 unitise(d)), scene);
	}
      cout << int(256. * g / (ss*ss)) << " ";
    }
    cout << endl;
  }

  scene->del(); // Deallocate the scene

  return 0;
}
