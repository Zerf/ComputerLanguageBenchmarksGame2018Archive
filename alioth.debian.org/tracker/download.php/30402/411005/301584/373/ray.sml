(* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/
   contributed by Jon Harrop, 2005
   Compile: mlton ray.sml -o ray *)

fun real n = Real.fromInt n
fun for (s, e, f) = if s=e then () else (f (real s); for (s+1, e, f))

val delta = 0.00000001
val infinity = 1.0 / 0.0
val pi = 4.0 * Real.Math.atan 1.0

type vec = real * real * real
infix 2 *| fun s *| (x, y, z) = (s*x+0.0, s*y, s*z)
infix 1 +| fun (x1, y1, z1) +| (x2, y2, z2) =
	       (x1+x2+0.0, y1+y2+0.0, z1+z2+0.0)
infix 1 -| fun (x1, y1, z1) -| (x2, y2, z2) =
	       (x1-x2+0.0, y1-y2+0.0, z1-z2+0.0)
fun dot (x1, y1, z1) (x2, y2, z2) = x1*x2 + y1*y2 + z1*z2+0.0
fun unitise r = (1.0 / Real.Math.sqrt (dot r r)) *| r

type ray = { orig: vec, dir: vec }

datatype scene =
	 Sphere of vec * real
       | Group of vec * real * scene list

fun ray_sphere (orig, dir) center radius =
    let
	val v = center -| orig
	val b = dot v dir
	val disc = b * b - dot v v + radius * radius
    in
	if disc < 0.0 then infinity else
	let val disc = Real.Math.sqrt disc in
	    let val t2 = b + disc in
	    if t2 < 0.0 then infinity else
	    (fn t1 => if t1 > 0.0 then t1 else t2) (b - disc)
	    end end end

fun intersect (orig, dir) scene =
    let fun of_scene (scene, (l, n)) =
	    case scene of
		Sphere (center, radius) =>
		let val l' = ray_sphere (orig, dir) center radius in
		    if l' >= l then (l, n) else
		    (l', unitise (orig +| l' *| dir -| center))
		end
	      | Group (center, radius, scenes) =>
		let val l' = ray_sphere (orig, dir) center radius in
		    if l' >= l then (l, n) else
		    foldl of_scene (l, n) scenes
		end in
	of_scene (scene, (infinity, (0.0, 0.0, 0.0)))
    end

fun ray_trace light (orig, dir) scene =
    let val (lambda, n) = intersect (orig, dir) scene in
	if lambda >= infinity then 0.0 else
	let val g = 0.0 - dot n light in
	    if g <= 0.0 then 0.0 else
	    let
		val orig = orig +| lambda *| dir +| delta *| n
		val dir = (0.0, 0.0, 0.0) -| light
	    in
		let val (l, _) = intersect (orig, dir) scene in
		    if l >= infinity then g else 0.0
		end end end end

fun create level r (x, y, z) =
    let val obj = Sphere ((x, y, z), r) in
	if level = 1 then obj else
	let val r' = 3.0 * r / Real.Math.sqrt 12.0 in
	    let fun aux x' z' =
		    create (level-1) (0.5 * r) (x-x', y+r', z+z') in
		let val objs = [aux (~r') (~r'), aux r' (~r'),
				aux (~r') r', aux r' r', obj] in
		    Group ((x, y, z), 3.0 * r, objs)
		    end end end end

val () =
    let
	val level = 6
	val ss = 4
	val n = case CommandLine.arguments () of
		    [s] => (case Int.fromString s of
				SOME n => n | _ => 256)
		  | _ => 256
	val scene = create level 1.0 (0.0, ~1.0, 0.0)
    in
	(fn s => print ("P5\n"^s^" "^s^"\n255\n")) (Int.toString n);
	for (0, n, fn y => for (0, n, fn x =>
            let val g = ref 0.0 in
		for (0, ss, fn dx => for (0, ss, fn dy =>
	            let val n = real n
			val x = x + dx / real ss - n / 2.0
			val y = n - 1.0 - y + dy / real ss - n / 2.0 in
			let val eye = unitise (~1.0, ~3.0, 2.0)
			    val ray = ((0.0, 0.0, ~4.0),
				       unitise (x, y, n)) in
			    g := !g + ray_trace eye ray scene
			end
		    end));
		let val g = 0.5 + 255.0 * !g / real (ss*ss) in
		    print (String.str(Char.chr(Real.trunc g)))
		end
	    end))
    end
