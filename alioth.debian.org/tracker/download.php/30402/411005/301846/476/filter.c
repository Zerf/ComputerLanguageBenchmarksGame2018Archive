/*
   Each program should be implemented to do the same thing as this C
   program. The purpose of this test is see how well suited the
   langauge is for what might be described as a typical scientific
   data analysis task.

   This program uses the random number generator from some of the
   other tests create 3 double precision arrays, X, Y, and E, each
   with 10000 elements.  The arrays X, Y represents the (x,y)
   positions of particles in a plane, and the E array represents the
   energies of the corresponding particles.

   The program computes the mean position of the particles (X0,Y0) and
   using that position as the center of a circle, it computes the
   minimuum radius required to contain at least 2/3 of the particles
   (6666).  It then computes the average energy of the particles that
   fall within or on this circle.

   The program should create and initialize the arrays once at the
   beginning and then perform the indicated computation the number of
   times N as specified on the command line.  For N = 100, the correct
   output is
   
      5.03729 5.0045 10.2028 1020.28
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define IM 139968
#define IA 3877
#define IC 29573

#define MAX_X 10.0
#define MAX_Y 10.0
#define MAX_E 20.0
#define NUM_POINTS 10000
#define MIN_INSIDE (NUM_POINTS*2)/3

static double gen_random(double max) 
{
   static long last = 42;
   last = (last * IA + IC) % IM;
   return( max * last / IM );
}

static void gen_data (double *x, double *y, double *e, unsigned int n)
{
   unsigned int i;
   for (i = 0; i < n; i++)
     {
	x[i] = gen_random (MAX_X);
	y[i] = gen_random (MAX_Y);
	e[i] = gen_random (MAX_E);
     }
}

static double compute_mean (double *x, unsigned int n)
{
   double sum = 0.0;
   unsigned int i;
   
   for (i = 0; i < n; i++)
     sum += x[i];
   
   return sum/n;
}

static int double_cmp (double *a, double *b)
{
   if (*a > *b) return 1;
   if (*a == *b) return 0;
   return -1;
}

static void filter_data (double *x, double *y, double *e, unsigned int n,
			 unsigned int min_wanted,
			 double *x0p, double *y0p, double *e0p)
{
   double *r;
   double x0, y0;
   double max_r;
   double sum;
   unsigned int i, num_inside;

   x0 = *x0p = compute_mean (x, n);
   y0 = *y0p = compute_mean (y, n);

   r = (double *) malloc (NUM_POINTS*sizeof(double));
   for (i = 0; i < n; i++)
     r[i] = hypot (x[i]-x0, y[i]-y0);
   
   qsort (r, n, sizeof (double), double_cmp);
   
   max_r = r[min_wanted-1];
   sum = 0.0;
   num_inside = 0;
   for (i = 0; i < n; i++)
     {
	if (r[i] <= max_r)
	  {
	     sum += e[i];
	     num_inside++;
	  }
     }
   *e0p = sum/num_inside;

   free (r);
}

int main(int argc, char *argv[]) 
{
   int N = ((argc == 2) ? atoi(argv[1]) : 1);
   double *x, *y, *e;
   double x0, y0, e0;
   double sum_e0;

   x = (double *) malloc (NUM_POINTS * sizeof (double));
   y = (double *) malloc (NUM_POINTS * sizeof (double));
   e = (double *) malloc (NUM_POINTS * sizeof (double));
   
   gen_data (x, y, e, NUM_POINTS);
   sum_e0 = 0.0;
   while (N > 0)
     {
	N--;

	filter_data (x, y, e, NUM_POINTS, MIN_INSIDE, &x0, &y0, &e0);
	sum_e0 += e0;
     }
   
   fprintf (stdout, "%g %g %g %g\n", x0, y0, e0, sum_e0);
   free (x);
   free (y);
   free (e);
   return 0;
}

