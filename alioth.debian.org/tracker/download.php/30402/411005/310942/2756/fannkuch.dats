(*
** The Great Computer Language Shootout
** http://shootout.alioth.debian.org/
**
** contributed by Hongwei Xi (hwxi AT cs DOT bu DOT edu)
**
** compilation command:
**   atscc -fomit-frame-pointer -O3 fannkuch.dats -o fannkuch
*)

%{^

typedef ats_ptr_type iarray ;

static inline
ats_ptr_type
iarray_make (ats_int_type n) {
  return malloc(n * sizeof(ats_int_type)) ;
}

static inline
ats_void_type
iarray_free (ats_ptr_type A) { free (A) ; return ; }

static inline
ats_int_type
iarray_get (ats_ptr_type A, ats_int_type i) {
  return ((ats_int_type *)A)[i] ;
}

static inline
ats_void_type
iarray_set (ats_ptr_type A, ats_int_type i, ats_int_type f) {
  ((ats_int_type *)A)[i] = f ; return ;
}

%}

abst@ype iarray = $extype "iarray"

extern fun iarray_make (sz: int): iarray = "iarray_make"
extern fun iarray_free (A: iarray): void = "iarray_free"

// unsafe version
extern fun iarray_get (A: iarray, i: int): int = "iarray_get"
extern fun iarray_set (A: iarray, i: int, x: int): void = "iarray_set"

overload [] with iarray_get
overload [] with iarray_set

// printing an integer array
fn q (p: iarray, n: int): void = let
  var i: int = 0
in
  while (i < n) (print p[i]; i := i+1) ; print_newline ()
end

// counting permutations
fun a (r: iarray, n: int): int = let
  val x = r[n]; val () = r[n] := x + 1
in
  if x = n - 2 then a (r, n+1) else (if x = n - 1 then r[n] := 0; n)
end

fn w (p: iarray, s: iarray, n: int, m: int): int = let
  fun loop1 (i: int):<cloptr1> void =
    if i < n then (s[i] := p[i]; loop1 (i+1))

  fun loop2 (x: int, i: int, u: int):<cloptr1> void =
    if i <= u then let
      val t = s[i] and o = x - i
    in
      s[i] := s[o]; s[o] := t; loop2 (x, i+1, u)
    end

  fun b (i: int):<cloptr1> bool =
    if i = n then true else (if p[i]<>(i+1) then b(i+1) else false)

  fun y (m: int):<cloptr1> int = let
    val x = s[0] - 1
  in
    if x = 0 then m else (loop2 (x, 0, (x - 1) >> 1); y (m+1))
  end
in
  if b 0 then (loop1 (0); y m) else 0
end

(* building new permutations *)
fn x (p: iarray, n: int): void = let
  fun loop_inner (p: iarray, j: int, i: int): void =
    if j < i then (p[j] := p[j+1]; loop_inner (p, j+1, i))
  fun loop_outer (p: iarray, n: int, i: int): void =
    if i < n then let
      val t = p[0] in loop_inner (p, 0, i); p[i] := t; loop_outer (p, n, i+1)
    end
in
  loop_outer (p, n, 1)
end

fn* f (r: iarray, p: iarray, s: iarray, n: int, i: int, m: int,  z: int)
  : int = if z > 0 then
    if i <= n then begin
      q (p, n); x (p, i);
      f (r, p, s, n, a (r, 2), max (m, w (p, s, n, 0)), z-1)
    end else begin
      q (p, n); g (r, p, s, n, i, m)
    end
  else g (r, p, s, n, i, m)

and g (r: iarray, p: iarray, s: iarray, n: int, i: int, m: int): int =
  if i <= n then begin
    x (p, i); g(r, p, s, n, a (r, 2), max (m, w (p, s, n, 0)))
  end else begin
    m // return value of [g]
  end

(* ****** ****** *)

fn usage (cmd: string): void = printf ("usage: %s [integer]\n", @(cmd))

implement main (argc, argv) = let
  val () = if argc <> 2 then (usage argv.[0]; exit (1))
  val () = assert (argc = 2)
  val s = argv.[1]
  val n = int_of_string s; val n2 = n + 2
  val r = iarray_make (n+2)
  val () = loop (0) where {
    fun loop (i: int):<cloref1> void =
      if i < n2 then (r[i] := i - 1; loop (i + 1))
  }
  val p = iarray_make (n)
  val () = loop (0) where {
    fun loop (i: int):<cloref1> void =
      if i < n then (p[i] := i + 1; loop (i+1))
  }
  val s = iarray_make (n)
  val () = loop (0) where {
    fun loop (i: int):<cloref1> void =
      if i < n then (s[i] := 0; loop (i+1))
  }
  val ans = f (r, p, s, n, a (r, 2), 0, 30)
in
  printf ("Pfannkuchen(%i) = %i\n", @(n, ans))
end

(* ****** ****** *)

(* end of [fannkuch.dats] *)
