% ----------------------------------------------------------------------
% The Computer Language Shootout
% http://shootout.alioth.debian.org/
%
% Assumes execution using the following command-line usage:
%
% compile:
%
%   mmc --make startup 
%   mmc --grade hlc.gc --make startup
%
% run:
%
%	./startup
%
% Modified from Mercury by Example Book by Glendon Holst
% ----------------------------------------------------------------------

:- module startup.
:- interface.
:- import_module io.

:- pred main(io::di, io::uo) is det.

% ----------------------------------------------------------------------

:- implementation.

% ----------------------------------------------------------------------

main(!IO) :- io.write_string("hello world", !IO), io.nl(!IO).

% ------------------------------- %
