/*
 *   GenPrime
 *
 *   (c) 2006-2008 Steven Noonan.
 *   Licensed under the New BSD License.
 *
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <memory.h>
#include <time.h>

typedef unsigned long prime_t;

/* 16K of data. Should fit in most L1 caches. */
#define PREGEN 4096

#ifdef PREGEN
prime_t primeCache[PREGEN]; prime_t *primeCachePtr;
#endif

int
isPrime ( unsigned long _candidate )
{
    prime_t i, limit, next, *p, n;

    if ( _candidate == 1 ) return 0;

    /* All numbers less than 4 are prime, except '1'. */
    if ( _candidate < 4 ) return -1;

    if ( _candidate == 5 ) return -1;

    /* All other numbers divisible by 2 are not prime. */
    if ( _candidate % 2 == 0 ) return 0;

    /* All other numbers divisible by 5 are not prime. */
    if ( _candidate % 5 == 0 ) return 0;

    if ( ( _candidate + 1 ) % 6 != 0 && ( _candidate - 1 ) % 6 != 0 ) return 0;

    n = (prime_t)_candidate;

    /*
       if n is composite then it can be factored into two values,
       at least one of which is less than or equal to sqrt(n)
     */
    limit = (prime_t)sqrt ((double)_candidate);

#ifdef PREGEN
    /* Iterate through the prime cache first to check divisors. */
    for ( p = primeCache; *p != 0 && *p <= limit; p++ )
        if ( n % *p == 0 ) return 0;

    /* Why did we exit the loop? Too high a divisor? */
    if ( *p >= limit ) return 1;

    /* No cache hit, we're moving on to odd numbers. */
    if ( primeCache[PREGEN-2] )
        next = primeCache[PREGEN-2] + 2;
    else
#endif
    next = 3;

    /* Now test all other odd numbers up to sqrt(n) */
    for ( i = next; i <= limit; i += 2 ) if ( n % i == 0 ) return 0;

    return 1;
}

unsigned long
genPrime ( unsigned long _maxToFind )
{
    unsigned long count = 0, num;
    for ( num = 1; count < _maxToFind; num++ )
    {
        if ( isPrime ( num ) )
        {
            count++;
        }
    }
    
    /* Returning 'count' prevents the compiler from optimizing this function out as worthless */
    return count;
}

#ifdef PREGEN
void AddPrimeToCache ( prime_t _prime )
{
    if ( primeCachePtr - primeCache > PREGEN - 2 )
        return;
    *primeCachePtr = _prime;
    primeCachePtr++;
}

void PrecalculatePrimes ()
{
    /* Find a predetermined number of primes to use as divisors. */

    unsigned long n;
    memset ( primeCache, 0, sizeof ( primeCache ) );
    primeCachePtr = primeCache;
    for ( n = 3; primeCachePtr - primeCache <= PREGEN - 2; n += 2 )
        if ( isPrime ( n ) )
            AddPrimeToCache ( (prime_t)n );

}
#endif

int main ( int argc, char **argv )
{

	struct timeval start;
	struct timeval finish;
	unsigned long i;

#ifdef PREGEN
    printf ( "Pregenerating %d primes... ", PREGEN );
    PrecalculatePrimes ();
    printf ( "OK\n\n" );
#endif

    for ( i = 100000; i <= 500000; i += 100000 )
    {
        gettimeofday ( &start, NULL );
        genPrime ( i );
        gettimeofday ( &finish, NULL );
        double secondsElapsed = (double)( finish.tv_sec - start.tv_sec ) +
                   ( (double)finish.tv_usec - (double)start.tv_usec ) / 1000000.0;
        printf ( "Time for %9d primes: %6.3lf seconds (%lu PPS)\n", i, secondsElapsed,
                             (unsigned long)((double)i / secondsElapsed) );
    }

    return 0;
}
