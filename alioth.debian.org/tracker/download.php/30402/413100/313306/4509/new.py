# -*- coding: utf-8 -*-
   #The Computer Language Benchmarks Game
   #http://shootout.alioth.debian.org/

   #contributed by Dieter Weber (dieter@uellue.de)
   
import numpy as np
import sys
import os
import multiprocessing
import ctypes
import mmap

# commandline: python new.py width ofilename

size = int(sys.argv[1])
remin = -1.5
remax = 0.5
imin = -1.
imax = 1.

# maximum number of points calculated in parallel to avoid
# excessive memory use
# may have to be optimized for cache size etc, not tested yet...
maxchunk = 1<<22

# tweaked to match reference output
# iterations = 49

# List of iteration steps, supplied by hand as number of iterations is fixed
# optimized for speed and to avoid overflows
iteration_steps = [8]*5+[9]*1

# distance between points in real direction
restep = (remax - remin)/(size)
# distance between points in imaginary direction
imstep = (imax - imin)/(size)

# number of bits
# remainder of experiments with long long int for bit shifting,
# but no advantage, byte boundary of lines not implemented for
# other types
(bits, calctype) = (8, 'u1')
bytesize = 8

# important to reproduce reference image precisely:
(floattype, complextype) = ('float64', 'complex64')
# precalculate and reuse later
reline = np.arange(remin, remax, restep, dtype=floattype)

# padding to start new line at byte boundary following pbm format
# size in [1,8] -> 1 byte per line ; size in [9,16]-> 2 bytes per line etc.
linesize = (size - 1)//bytesize + 1

# PBM header, important to calculate buffer size
header = "P4\n{0} {0}\n".format(size)
header_offset = len(header)

bufferlen = len(header) + linesize * size

# creates ctypes array in shared memory
sharedbitmap = multiprocessing.Array(ctypes.c_char, bufferlen, lock=False)
sharedbitmap[0:len(header)] = header

# not more to avoid task switching overhead...
# scales pretty well
workers = multiprocessing.cpu_count()

# calculate line package, either divide fairly per cpu or limit
# by memory use
# size is much smaller than maxchunk, division will never give zero
maxlines = maxchunk // size
lines_per_chunk = min(size//workers,  maxlines)

# number of tasks
packages = size // lines_per_chunk

# construct task array for mapping later
tasks = []
for i in range(packages):
    tasks.append((i*lines_per_chunk, lines_per_chunk))
	
# see what lines are not covered yet, distribute them among the tasks
(last_offset, last_lines) = tasks[-1]
missing_lines = size - (last_offset+last_lines)
for i in range(missing_lines):
    (offset, lines) = tasks[-(missing_lines-i)]
    tasks[-(missing_lines-i)] = (offset + i, lines + 1)
	
	
# modifies z! call by reference!
# iterate z according to formula, set all elements
# in the set to 0
def mandeliterations(z):
    # calculate subsequently shorter lists and keep track of indices
    indexlist = []
    calcc = z.copy()
    calcz = z
    # filtering the list is expensive,
    # calculate several iterations before filtering
    for iterations in iteration_steps:
        for i in range(iterations):
                calcz **= 2
                calcz += calcc
        indices = np.nonzero(abs(calcz) < 2)
        # I guess that continuous arrays are better for fast iterating,
        # therefore create new arrays
        calcz = calcz[indices]
        calcc = calcc[indices]
        indexlist.append(indices)
    # "collapes" the index list from the bottom to get the elements 
    # remaining in the set
    mandelbrot = indexlist.pop()
    # a bit messy because of the index format that numpy uses
    for indices in reversed(indexlist[1:]):
        mandelbrot = indices[0][mandelbrot]
    mandelbrot = (indexlist[0][0][mandelbrot], indexlist[0][1][mandelbrot])
    # finally, set everything in the set = 0
    z[mandelbrot] = 0

# generate/allocate block of complex numbers for subsequent iteration
def mandelblock(line_offset, lines):
    start = line_offset*imstep-imax
    stop = (line_offset+lines)*imstep-imax
    imline = -1j*np.arange(start, stop, imstep, dtype=complextype)
    # maybe numpy.mgrid would be faster, but not tried yet...
    (re, im) = np.meshgrid(reline, imline)
    # reuse memory
    im += re
    return im

# convert data array into "compressed" bitmap to be written to binary pbm file
def pbmline(points):
    lines = len(points[:,0])
    # each point is in [0, 1] now, 8 bit unsigned int 
    bitmap = np.zeros(linesize*bits*lines, dtype=calctype)
    # this is necessary because of the "padding" bits at the end of 
    # the line to get byte boundaries
    for line in range(lines):
        offset = line*linesize*bits
        # read data from complex-typed calculation array
        bitmap[offset:offset + size] = (points[line,:] == 0.)
    # make blocks with 8 bits
    bitmap = bitmap.reshape((linesize*lines, bits))
    # shift bits, accumulate in highest bit
    for bit in range(0,bits-1):
        # fix bit order here
        bitmap[:,bit] <<= (bits-bit-1)
        bitmap[:,bits-1] += bitmap[:,bit]
    # return accumulator
    result = bitmap[:,bits-1]
    return result

# move bitmap fragments to output
def copybits(line_offset, bitmap):
    startbyte = header_offset + line_offset * linesize
    # program works with 8/8 now, less headache
    copybytes = len(bitmap)*bits//bytesize
    # didn't get ctypes.memcopy to work, 
    # this does it although it may be a bit slower
    sharedbitmap[startbyte:startbyte+len(bitmap)] = bitmap.tostring()
	
# function to be called with element of the task array, 
# suitable for mapping (no output, goes directly to shared file)
def work(tup):
    (line_offset, lines) = tup
    block = mandelblock(line_offset, lines)
    mandelbrot = mandeliterations(block)
    bitmap = pbmline(block)
    copybits(line_offset, bitmap)

pool = multiprocessing.Pool(workers)
# for debugging, just use map(work, tasks)
# global variables etc. are shared between processes! :-)
pool.map(work, tasks)

# dump it!
sys.stdout.write(sharedbitmap.raw)