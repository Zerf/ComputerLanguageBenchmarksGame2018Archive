# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
#
# contributed by Stefan Thomas

import sys

def fannkuch(liste):
	global checksum
	global mflips
	global s
	flips = 0
	fliste = liste[:]
	while fliste[0] != 1:
		x = fliste[0]
		hliste = fliste[0:x]
		hliste.reverse()
		fliste[0:x] = hliste
		flips += 1
	s += 1
	checksum += (-1 if s%2 else 1) * flips
	if flips > mflips: mflips = flips

def permute(liste,n):
	pliste = liste[:]
	for j in range(n-1):	
		if n > 1:
			permute(pliste,n-1)
		hliste = []
		hliste = pliste[0:n]
		lshift(hliste,n)
		pliste[:n] = hliste
		fannkuch(pliste)
	if n > 1:
		permute(pliste,n-1)

def lshift(p,n):
	x = p[0]
	p[0:n-1] = p[1:n]
	p[n-1] = x

mflips = 0
checksum = 0
n = int(sys.argv[1])
p = list(range(1,n+1))
s = 1
fannkuch(p)
permute(p,n)

print(checksum)
print("Pfannkuchen({0}) = {1}".format(str(n), str(mflips)))
