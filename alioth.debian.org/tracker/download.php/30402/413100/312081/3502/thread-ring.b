#
# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
#
# Venkatesh Srinivas <me@acm.jhu.edu> (2009-11-13)
#
# Limbo implementation of the thread-ring benchmark. We start a number 
# of threads and pass a token between each one a number of times. We
# print the last thread to recieve the token.
#

implement ThreadRing;

include "sys.m";
	sys : Sys;
include "draw.m";
include "arg.m";

bye : chan of int;

ThreadRing: module {
	init: fn(nil: ref Draw->Context, nil: list of string);
};

ring_thread(id: int, left : chan of int, right : chan of int)
{
	for (;;) {
	    ival : int;
	    ival = <- left;
	    if (ival == 0) {
	       sys->print("%d \n", id);
           bye <- = 1;
        }
	    right <- = ival - 1;
	}
}

init(nil: ref Draw->Context, args: list of string) 
{
	sys = load Sys Sys->PATH;
	arg := load Arg Arg->PATH;
	count : int = 50000000;
	threads : int = 503;
	bye = chan of int;
	
	arg->init(args);
	arg->setusage(arg->progname()+" [-t threads] [-n count]");
	while ((opt := arg->opt()) != 0)
		case opt {
			'n' => count = int arg->earg();
			't' => threads = int arg->earg();
			*   => arg->usage();
		}
	arg = nil;

	chans := array[threads] of chan of int;
	for (i := 0; i < threads; i++) {
	    chans[i] = chan of int;
	}

	spawn ring_thread(0, chans[threads - 1], chans[0]);
	for (i = 1; i < threads; i++) {
	    spawn ring_thread(i, chans[i - 1], chans[i]);
	}

	chans[0] <- = count;

    <- bye;
    exit;
}
