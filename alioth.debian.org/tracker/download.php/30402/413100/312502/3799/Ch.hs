{- The Computer Language Benchmarks Game
   http://shootout.alioth.debian.org/
   Written by Tom Pledger, 13 Nov 2006. modified by Don Stewart
   Updated for chameneos-redux by Spencer Janssen, 27 Nov 2007 
   Modified by Péter Diviánszky, 19 May 2010 -}

import Control.Concurrent
import Control.Monad
import Data.Char
import Data.IORef
import System.Environment
import System.IO

default(Int)

data Colour = Blue | Red | Yellow deriving (Show, Eq, Enum)

complement a b | a == b = a
complement a b = case a of
    Blue   -> case b of Red  -> Yellow; _ -> Red
    Red    -> case b of Blue -> Yellow; _ -> Blue
    Yellow -> case b of Blue -> Red;    _ -> Blue

type Chameneous = IORef Colour
data MP = Nobody !Int | Somebody !Int !Chameneous !(MVar Chameneous)

arrive mpv finish c0 = do
    ch <- newIORef c0
    waker <- newEmptyMVar
    let inc x = (fromEnum (ch == x) +)
        go !t !b = do
            w <- takeMVar mpv
            case w of
                Nobody 0 -> do
                    putMVar mpv w
                    putMVar finish (t, b)

                Nobody q -> do
                    putMVar mpv $ Somebody q ch waker
                    ch' <- takeMVar waker
                    go (t+1) $ inc ch' b

                Somebody q ch' waker' -> do
                    c  <- readIORef ch
                    c' <- readIORef ch'
                    let !c'' = complement c c'
                    writeIORef ch  c''
                    writeIORef ch' c''
                    putMVar waker' ch
                    let !q' = q-1
                    putMVar mpv $ Nobody q'
                    go (t+1) $ inc ch' b
    go 0 0

showN = unwords . map ((digits !!) . digitToInt) . show

digits = words "zero one two three four five six seven eight nine"

run n cs = do
    fs    <- replicateM (length cs) newEmptyMVar
    mpv   <- newMVar $ Nobody n
    zipWithM ((forkIO .) . arrive mpv) fs cs
    ns    <- mapM takeMVar fs

    putStrLn . map toLower . unwords . ([]:) . map show $ cs
    putStr . map toLower . unlines $ [unwords [show n, showN b] | (n, b) <- ns]
    putStrLn . (" "++) . showN . sum . map fst $ ns
    putStrLn ""

main = do
    putStrLn . map toLower . unlines $
        [unwords [show a, "+", show b, "->", show $ complement a b]
            | a <- [Blue ..], b <- [Blue ..]]

    n <- readIO . head =<< getArgs
    run n [Blue ..]
    run n [Blue, Red, Yellow, Red, Yellow, Blue, Red, Yellow, Red, Blue]


