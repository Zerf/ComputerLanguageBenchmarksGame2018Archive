(*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Christophe TROESTLER
 * Enhanced by Christian Szegedy, Yaron Minsky
 * Enhanced by Otto Bommer
 *)

let niter = 50
let limit = 2.
let limit2 = limit *. limit

let () =
  let w = try int_of_string(Array.get Sys.argv 1) with _ -> 1600 in
  let h = w in
  Printf.printf "P4\n%i %i\n" w h;
  let fw = float w /. 2. and fh = float h in
  let fhinv = 2. /. fh in 
  let cr = ref 0. and ci = ref 0. and zr = ref 0. and zi = ref 0. in
  let t1 = ref 0. and t2 = ref 0. in
  let byte = ref 0 in
  let i = ref 0 in
  for y = 0 to h - 1 do
    ci := float y *. fhinv -. 1.;
    for x = 0 to w - 1 do
      cr := float x /. fw -. 1.5;
      zr := 0.; zi := 0.;
      i := niter;
      while !i > 0 do
        t1 := !zr *. !zi;
        t2 := !t1 +. !t1 +. !ci;
        zr := !zr *. !zr -. !zi *. !zi +. !cr;
        zi := !t2;
        decr i;
        if !zr *. !zr +. !zi *. !zi > limit2 then begin i := -1 end
      done;
      if !i == 0 then byte := (!byte lsl 1) lor 0x01 
      else byte := !byte lsl 1; 
      if x mod 8 = 7 then output_byte stdout !byte;
    done;
    if w mod 8 != 0 then output_byte stdout (!byte lsl (8-w mod 8));
    byte := 0;
  done

