# The Computer Language Shootout
# http://shootout.alioth.debian.org/
#
# submitted by Todd Smith

import itertools
import collections
import sys

def count(seq, sz):
    freq = collections.defaultdict(int)
    for i in range(len(seq)+1-sz):
        freq[seq[i:i+sz]] += 1
    return freq

# read input line-by-line
input = itertools.dropwhile(lambda ln: ln[0:3] != ">TH", sys.stdin)
next(input)
seq = "".join(ln[:-1] for ln in input).upper()

for sz in (1,2):
    x = 100.0/(len(seq)+1-sz)
    print "".join("%s %.3f\n" % (s,f*x) for s,f in
                  sorted(count(seq, sz).items(), key=lambda sf:(-sf[1],sf[0])))

for p in ('GGT', 'GGTA', 'GGTATT', 'GGTATTTTAATT', 'GGTATTTTAATTTATAGT'):
    print "%d\t%s" % (count(seq, len(p))[p], p)
