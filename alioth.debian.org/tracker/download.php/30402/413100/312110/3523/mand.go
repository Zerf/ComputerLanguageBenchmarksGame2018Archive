/* The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Martin Koistinen
 * Based on mandelbrot.c contributed by Greg Buchholz and The Go Authors
 * flag.Arg hack by Isaac Gouy
 *
 * Large changes by Bill Broadley, including:
 * 1) Switching the one goroutine per line to one per CPU
 * 2) Replacing gorouting calls with channels
 * 3) Handling out of order results in the file writer.
 * 
 * Version 5
 */

package main

import (
	"bufio";
	"flag";
	"fmt";
	"os";
	"strconv";
	"runtime";
)

/* targeting a q6600 system, one cpu worker per core */
const pool = 4

const ZERO float64 = 0
const LIMIT = 2.0
const ITER = 50	// Benchmark parameter

var rows [][]byte;

func writeRow(writeChan chan int, size int,finishChan chan bool) {

	var d int;
	var row []uint8;
	
	out := bufio.NewWriter(os.Stdout);
	defer out.Flush();
	fmt.Fprintf(out, "P4\n%d %d\n", size, size);

	/* vector of bits to track which rows are completed */
	r := make([]bool, size);

	bytes := int(size / 8);
	if size%8 > 0 {
		bytes++
	}

	written := 0;
	for y := 0; y < size; y++ {
		d = <-writeChan;
		r[d] = true;
		for ; written < size && r[written] == true; written++ {
			row=rows[written];
			out.Write(row);
		}
	}
	finishChan <- true;
}

// This func is responsible for rendering a row of pixels,
// and when complete writing it out to the file.
func renderRow(w, h, bytes int, workChan,writeChan chan int, rank int) {

	var row []uint8;
	var Zr, Zi, Tr, Ti, Cr float64;
	var x,i int;

	for y := range workChan {
		// This will hold this rows pixels
		row = rows[y];

		Ci := (2*float64(y)/float64(h) - 1.0);

		for x = 0; x < w; x++ {
			Zr, Zi, Tr, Ti = ZERO, ZERO, ZERO, ZERO;
			Cr = (2*float64(x)/float64(w) - 1.5);

			for i = 0; i < ITER && Tr+Ti <= LIMIT*LIMIT; i++ {
				Zi = 2*Zr*Zi + Ci;
				Zr = Tr - Ti + Cr;
				Tr = Zr * Zr;
				Ti = Zi * Zi;
			}

			// Store the value in the array of ints
			if Tr+Ti <= LIMIT*LIMIT {
				row[x/8] |= (byte(1) << uint(7-(x%8)))
			}
		}
		// queue this row for writing
		writeChan <- y;
	}
}

func main() {
	runtime.GOMAXPROCS(4); 

	size := 16000;	// Contest settings

	// Get input, if any...
	flag.Parse();
	if flag.NArg() > 0 {
		size, _ = strconv.Atoi(flag.Arg(0))
	}
	w, h := size, size;
	bytes := int(w / 8);
	if w%8 > 0 {
		bytes++
	}
	rows=make([][]byte,size);
	for i := 0; i < size; i++ {
		rows[i] = make([]byte, bytes)
	}

	/* global buffer of work for workers, ideally never runs dry */
	workChan := make(chan int, pool*2+1);
	/* global buffer of results for output, ideally never blocks */
	writeChan := make(chan int, pool*2+1);
	finishChan := make(chan bool);
	y := 0;
	go writeRow(writeChan, size,finishChan);
	// start pool workers, and assign all work
	for y = 0; y < size; y++ {
		if y < pool {
			go renderRow(w, h, bytes, workChan, writeChan, y)
		}
		workChan <- y;
	}
	/* write for the file writer to finish */
	<- finishChan;
}
