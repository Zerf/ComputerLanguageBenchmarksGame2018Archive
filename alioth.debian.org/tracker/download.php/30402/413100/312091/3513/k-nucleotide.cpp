/* The Computer Language Benchmarks Game
   http://shootout.alioth.debian.org/

   Contributed by Andrew Moon
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>

#include <sched.h>
#include <pthread.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/hash_policy.hpp>

typedef unsigned long long u64;
typedef unsigned int u32;
typedef unsigned char u8;

using namespace std;

struct CPUs {
	CPUs() {
		cpu_set_t cs;
		CPU_ZERO( &cs );
		sched_getaffinity( 0, sizeof(cs), &cs );
		count = 0;
		for ( size_t i = 0; i < CPU_SETSIZE; i++ )
			count += CPU_ISSET( i, &cs ) ? 1 : 0;
		count = std::max( count, u32(1) );
	}

	u32 count;
} cpus;

/*
	Handles packing the DNA sequences in to a more efficient
	representation (2 bits per sequence)
	
    0000:0xx0 are the bits we want. ACGT and acgt both map to the same
    four values with this bitmask, but not in alphabetical order. Convert
    the key to a string to sort!

	Called rarely so efficiency is not important
*/

template< int N >
struct DNAPack {
	enum {
		completedwords = N / 4,
		partialbytes = N & 3,
		storagedwords = ( N + 15 ) / 16,
	};

	static void pack( const u8 *in, u8 *packed ) {
		for ( u32 i = 0, shift = 0; i < N; i++, shift += 2 ) 
			packed[shift >> 3] |= ( (unsigned )( *in++ & 6 ) >> 1 ) << (shift & 7);
	}
};

/*
	A DNA sequence Key, N sequences long (max 32)
	
	Compiler optimization will create optimized versions of hash/equals/advance 
	without the if code since N will be knowable at compile time. Shifts have to
	be put in to non-const variables because the compiler will complain about out
	of range shifts otherwise
*/

template< int N >
struct Key {
	enum { dwords = DNAPack<N>::storagedwords };

	struct Ops {
		// hash
		u32 operator() ( const Key &k ) const {
			if ( N <= 4 ) {
				return k.dword(0);
			} else if ( N <= 16 ) {
				u8 shift = N / 2;
				return k.dword(0) + ( k.dword(0) >> shift );
			} else {
				return k.dword(0) + ( k.dword(0) >> 13 ) + k.dword(1);
			}
		}

		// equals
		bool operator() ( const Key &a, const Key &b ) const {
			return ( N <= 16 ) ? a.dword(0) == b.dword(0) : a.qword() == b.qword();
		}
	};

	Key() {}
	
	Key( const char *in ) {
		for ( int i = 0; i < dwords; i++ )
			dword(i) = 0;
		DNAPack<N>::pack( (u8 *)in, (u8 *)packed );
	}

	Key &advance( char next ) {
		if ( N == 1 ) {
			dword(0) = (unsigned )( next & 6 ) >> 1;
		} else if ( ( N >= 2 ) && ( N <= 16 ) ) {
			u8 shift = ( ( N - 2 ) * 2 + 1 );
			dword(0) = (unsigned )dword(0) >> 2;
			dword(0) |= ( ( next & 0x6 ) << shift );
		} else if ( N == 18 ) {
			u8 shift = ( N * 2 ) - 3;
			qword() = ( qword() >> 2 ) | ( u64( next & 0x6 ) << shift );
		}
		return *this;
	}

	// up to 2 instances active at once
	const char *tostring() const {
		static char names[2][N+1], table[4] = { 'A', 'C', 'T', 'G' };
		static u32 on = 0;
		u64 bits = ( N <= 16 ) ? dword(0) : qword();
		on ^= 1;
		for ( int i = 0; i < N; i++, bits >>= 2 )
			names[on][i] = table[bits & 3];
		names[on][N] = 0;
		return names[on];
	}

	bool operator< ( const Key &b ) const {
		return strcmp( tostring(), b.tostring() ) < 0;
	}

	u32 &dword( size_t i ) { return packed[i]; }
	const u32 &dword( size_t i ) const { return packed[i]; }
	u64 &qword() { return *((u64 *)packed); }
	const u64 &qword() const { return *((const u64 *)packed); }
protected:
	u32 packed[dwords];
};

template< int N > 
	class KeyHash :
		public __gnu_pbds::cc_hash_table <
			Key<N>, // key
			u32, // value
			typename Key<N>::Ops, // hash 
			typename Key<N>::Ops // equality
		> {};


static const u32 lengths[] = { 18, 12, 6, 4, 3, 2, 1 }, numLengths = 7;
static const u32 lineLength = 60, longestSeq = lengths[0] - 1;


struct Block {
	enum {
		lines = 1092 * 2,
		size = lineLength * lines + 2,
	};

	Block() : count(0), actualSize(size), overlapped(false) {
		data = new char[actualSize];
	}

	Block( Block &prev ) 
		: count(longestSeq), actualSize(size + longestSeq), overlapped(true) {
		data = new char[actualSize];
		memcpy( data, prev.data + prev.count - longestSeq, longestSeq );
	}

	~Block() { delete[] data; }

	Block *read() {
		size_t line = 0;
		data[count + lineLength] = -1;		
		while ( fgets_unlocked( data + count, int(actualSize - count), stdin ) ) {
			if ( data[count] == '>' )
				break;
			// -1 trick should keep us from calling strlen
			if ( data[count + lineLength] != 0xa ) {
				count += int(strlen( data + count )) - 1;
				break;
			}
			count += lineLength;
			if ( ++line >= lines )
				return new Block( *this );
			data[count + lineLength] = -1;
		}
		return NULL;
	}

	bool untilheader( const char *match ) {
		size_t len = strlen( match );
		const u32 *in = (const u32 *)data, *want = (const u32 *)match;
		while ( fgets_unlocked( data, size, stdin ) ) 
			if ( ( *in == *want ) && ( memcmp( data, match, len ) == 0 ) )
				return true;
		return false;
	}

	bool isoverlapped() const { return overlapped; }
	int getcount() const { return count; }
	char *getdata() { return data; }

protected:
	char *data;
	int count, actualSize;
	bool overlapped;
};

struct Queue {
	Queue() : count(0), block(0), length(0), max(0xffffffff) {
		// make the list large enough so it can't resize out from under a worker
		list.reserve( 1048576 * 2047 / Block::size ); // ~2gb
	}
	
	~Queue() {
		vector<Block *>::iterator iter = list.begin(), end = list.end();
		for ( ; iter != end; ++iter )
			delete *iter;
	}

	void add( Block *block ) { 
		list.push_back( block );
		count++;
	}

	bool get( Block *&out, u32 &sequence ) {
		for ( ; length < numLengths; sched_yield() ) {
			// are there available blocks?
			if ( ( block == max ) || ( block == count ) )
				continue;
			
			// try to claim the next block
			u32 index = block, next = block + 1;
			if ( __sync_val_compare_and_swap( &block, index, next ) != index )
				continue;

			// it's ours
			out = list[index];
			sequence = lengths[length];
			
			// we can update these because other threads will stall on block == max
			if ( next == max ) {
				__sync_val_compare_and_swap( &length, length, length + 1 );
				__sync_val_compare_and_swap( &block, block, 0 );
			}
			return true;
		}

		return false;
	}

	void finish() {
		max = count;
	}

protected:
	vector<Block *> list;
	volatile u32 count, block, length, max;
} workQueue;

struct Worker {
	Worker() {}

	template< int N, class Hash >
	void process( Hash &hash, Block &block ) {
		int offset = ( block.isoverlapped() ) ? longestSeq - ( N - 1 ) : 0;
		const char *data = block.getdata() + offset;
		int left = block.getcount() - offset;
		Key<N> key( data );
		data += N;

		for ( left -= ( N - 1 ); left; left-- ) {
			hash[key]++;
			key.advance( *data++ );
		}
	}

	void run() {
		Block *block;
		u32 length;

		while ( workQueue.get( block, length ) ) {
			switch ( length ) {
				case 1: process<1>( hash1, *block ); break;
				case 2: process<2>( hash2, *block ); break;
				case 3: process<3>( hash3, *block ); break;
				case 4: process<4>( hash4, *block ); break;
				case 6: process<6>( hash6, *block ); break;
				case 12: process<12>( hash12, *block ); break;
				case 18: process<18>( hash18, *block ); break;
				default: break;
			}
		}
	}

	void join() { pthread_join( handle, 0 ); }
	void start() { pthread_create( &handle, 0, Worker::thread, this ); }
	static void *thread( void *arg ) { ((Worker *)arg)->run(); return 0; }

	pthread_t handle;

	KeyHash<18> hash18;
	KeyHash<12> hash12;
	KeyHash<6> hash6;
	KeyHash<4> hash4;
	KeyHash<3> hash3;
	KeyHash<2> hash2;
	KeyHash<1> hash1;
};

template< int N, class W > KeyHash<N> &Get( W &w );

template<> KeyHash<1> &Get( Worker &w ) { return w.hash1; }
template<> KeyHash<2> &Get( Worker &w ) { return w.hash2; }
template<> KeyHash<3> &Get( Worker &w ) { return w.hash3; }
template<> KeyHash<4> &Get( Worker &w ) { return w.hash4; }
template<> KeyHash<6> &Get( Worker &w ) { return w.hash6; }
template<> KeyHash<12> &Get( Worker &w ) { return w.hash12; }
template<> KeyHash<18> &Get( Worker &w ) { return w.hash18; }

template< int N >
void printcount( Worker *workers, const char *key ) {
	Key<N> find( key );
	u32 count = 0;
	for ( u32 i = 0; i < cpus.count; i++ )
		count += Get<N>( workers[i] )[find];
	cout << count << '\t' << find.tostring() << endl;
}

template<class T>
struct CompareFirst {
	bool operator() ( const T &a, const T &b ) { return a.first < b.first; }
};

template<class T>
struct CompareSecond {
	bool operator() ( const T &a, const T &b ) { return a.second > b.second; }
};


template< int N >
void printfreq( Worker *workers ) {
	cout.setf( ios::fixed, ios::floatfield );
	cout.precision( 3 );

	u32 count = 0;
	KeyHash<N> sum;
	for ( u32 i = 0; i < cpus.count; i++ ) {
		KeyHash<N> &hash = Get<N>( workers[i] );
		typename KeyHash<N>::iterator iter = hash.begin(), end = hash.end();
		for ( ; iter != end; ++iter ) {
			count += iter->second;
			sum[iter->first] += iter->second;
		}
	}

	typedef pair< Key<N>, u32 > sequence;
	vector<sequence> seqs( sum.begin(), sum.end() );
	stable_sort( seqs.begin(), seqs.end(), CompareFirst<sequence>() ); // by name
	stable_sort( seqs.begin(), seqs.end(), CompareSecond<sequence>() ); // by count

	typename vector<sequence>::iterator iter = seqs.begin(), end = seqs.end();
	for ( ; iter != end; ++iter )
		cout << 
			iter->first.tostring() << " " << (100.0f * iter->second / count) << endl;
	cout << endl;
}



int main( int argc, const char *argv[] ) {
	Block *block = new Block();
	if ( !block->untilheader( ">THREE" ) )
		return -1;

	Worker *workers = new Worker[cpus.count];
	for ( u32 i = 0; i < cpus.count; i++ )
		workers[i].start();

	while ( block ) {
		Block *block2 = block->read();
		if ( block2 != block )
			workQueue.add( block );
		block = block2;
	}

	workQueue.finish();
	for ( u32 i = 0; i < cpus.count; i++ )
		workers[i].join();

	printfreq<1>( workers );
	printfreq<2>( workers );

	printcount<3>( workers, "ggt" );
	printcount<4>( workers, "ggta" );
	printcount<6>( workers, "ggtatt" );
	printcount<12>( workers, "ggtattttaatt" );
	printcount<18>( workers, "ggtattttaatttatagt" );

	delete[] workers;

	return 0;
}
