/* The Computer Language Benchmarks Game
   http://shootout.alioth.debian.org/

   Originally contributed by John Nilsson
   Major performance improvement by Geoff Reedy
   GMP adaptor + further improvements by Rex Kerr
*/

object pidigits {
    import Gmp._

    case class LFT(q:I, r:I, t:I) {
      def deferTo(lft: LFT) = {
        def defer(i: I, j: I) = { if (i ne j) ~i }
        defer(q,lft.q); defer(r,lft.r); defer(t,lft.t)
        lft
      }
      def compose(iqrt: (Int,Int,Int)) = {
        val (iq,ir,it) = iqrt
        deferTo( LFT( (q*iq)! , ((q*ir) + (r*it))! , (t*it)! ) )
      }
      def next(y: Int) = {
        deferTo( LFT( (q*10)! , ((r-(t*y))*10)! , t ) )
      }
      def extractDigit = {
        val yr = (((q*3 + r) /% t)!!)
        val result = if ((yr._2 + q) < t) Some(yr._1.toInt) else None
        ~yr._1 ; ~yr._2
        result
      }
      def reduce = {
        val d = ((q gcd r) gcd t)!
        val result = LFT( (q/d)! , (r/d)! , (t/d)! )
        ~d
        deferTo( result )
      }
    }

    def pi_digits = {
      def infinite_lft = Stream from 1 map( k => (k,k*4+2,k*2+1) )
      def digits(z: LFT, lfts:Stream[(Int,Int,Int)], n:Int): Stream[(Int,Int)] = {
          val rz = if (lfts.head._1 % 2000 == 0) z.reduce else z
          rz extractDigit match {
              case Some(y) => 
                Stream.cons((n,y),digits(rz next y, lfts,n+1))
              case None    => digits(rz compose lfts.head, lfts.tail,n)
          }
      }
      digits(new LFT(I(1),I(0),I(1)),infinite_lft,1)
    }

    def by[T](s: Stream[T], n: Int): Stream[Stream[T]] =
        if(s.isEmpty) Stream.empty
        else Stream.cons(s take n, by(s drop n, n))

    def main(args: Array[String]): Unit =
        for (d <- by(pi_digits take args(0).toInt, 10))
            println("%-10s\t:%d".format(d.map(_._2.toInt).mkString(""),d.last._1))
}

class GmpUtil {
  System.loadLibrary("jpargmp")
  @native def mpz_init(): Long
  @native def mpz_clear(src: Long)
  @native def mpz_set_si(lhs: Long, a: Int)
  @native def mpz_get_si(a: Long): Int
  @native def mpz_cmp(a: Long, b: Long): Int
  @native def mpz_add(sum: Long, a: Long, b: Long)
  @native def mpz_sub(sum: Long, a: Long, b: Long)
  @native def mpz_mul_si(prod: Long, a: Long, b: Int)
  @native def mpz_divexact(quot: Long, n: Long, d: Long)
  @native def mpz_tdiv_qr(quot: Long, rem: Long, n: Long, d: Long)
  @native def mpz_gcd(ans: Long, a: Long, b: Long)
}
object Gmp {
  import collection.mutable.ArrayStack
  val gmp = new GmpUtil
  private var stack = Nil:List[I]
  private var defunct = Nil:List[I]
  class I {
    private val z = gmp.mpz_init()
    def !() = stack match {
      case i :: rest if (i eq this) =>
        stack = Nil
        defunct = rest ::: defunct
        i
      case _ => I.die
    }
    def !!() = stack match {
      case i :: j :: rest if (i eq this) =>
        stack = Nil
        defunct = rest ::: defunct
        (i,j)
      case _ => I.die
    }
    def toInt = gmp.mpz_get_si(z)
    def <(i: I) = gmp.mpz_cmp(z, i.z) < 0
    def +(i: I) = { gmp.mpz_add(I.ans.z, z, i.z); I.get }
    def -(i: I) = { gmp.mpz_sub(I.ans.z, z, i.z); I.get }
    def *(n: Int) = { gmp.mpz_mul_si(I.ans.z, z, n); I.get }
    def /(i: I) = { gmp.mpz_divexact(I.ans.z, z, i.z); I.get }
    def /%(i: I) = { val r = I.ans.z; gmp.mpz_tdiv_qr(I.ans.z, r, z, i.z); I.get }
    def gcd(i: I) = { gmp.mpz_gcd(I.ans.z, z, i.z); I.get }
    def unary_~() = { defunct ::= this }
    override def finalize() { gmp.mpz_clear(z); super.finalize }
  }
  object I {
    def apply(n:Int) = defunct match {
      case i :: rest =>
        defunct = rest
        gmp.mpz_set_si(i.z,n)
        i
      case _ =>
        val i = new I
        if (n != 0) gmp.mpz_set_si(i.z,n)
        i
    }
    def ans() = { val i = I(0); stack ::= i; i }
    def die: Nothing = throw new IndexOutOfBoundsException
    def get() = stack match { case i :: rest => i ; case _ => die }
  }  
}

