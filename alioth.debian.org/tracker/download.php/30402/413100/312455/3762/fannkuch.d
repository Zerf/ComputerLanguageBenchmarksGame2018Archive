/*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Eckehard Berns
 * Based on code by Heiner Marxen
 * and the ATS version by Hongwei Xi
 * 
 * Modified to work with D by rofl0r
 */
module fannkuch;
// static if ((void*).sizeof == 8))
// or 
// version (X86_64)
pragma (lib, "pthread");

version (D_LP64 ) {
	alias long sizeint;
	alias ulong sizeuint;
	alias ulong sizeulong; 
} else {
	alias int sizeint;
	alias uint sizeuint;
	alias uint sizeulong;
}

version = USETHREADS;

struct sched_param {
	int __sched_priority;
}

struct pthread_attr_t {
    int __detachstate;
    int __schedpolicy;
    sched_param __schedparam;
    int __inheritsched;
    int __scope;
    size_t __guardsize;
    int __stackaddr_set;
    void *__stackaddr;
    sizeulong __stacksize;
} ;

alias sizeulong pthread_t;

extern (C) {
	int printf(char* s, ...);
	int atoi ( char* str );
	void* malloc ( size_t size );
	void free ( void* ptr );
	int pthread_join(pthread_t th, void** thread_return);
	int pthread_create(pthread_t* thread, in pthread_attr_t* attr,
			void* function(void*), void* arg);
	void abort();
}

struct worker_args {
   sizeint i, n;
   version(USETHREADS) {
   pthread_t id;
   }
   worker_args *next;
};

extern(C) static void* fannkuch_worker(void* _arg)
{
   worker_args* args = cast(worker_args*)_arg;
   sizeint* perm1; sizeint* count; sizeint* perm;
   sizeint maxflips, flips, i, n, r, j, k, tmp;

   maxflips = 0;
   n = args.n;
   perm1 = cast(sizeint*) malloc(n * sizeint.sizeof);
   perm = cast(sizeint*)malloc(n * sizeint.sizeof);
   count = cast(sizeint*)malloc(n * sizeint.sizeof);
   for (i = 0; i < n; i++)
      perm1[i] = i;
   perm1[args.i] = n - 1;
   perm1[n - 1] = args.i;
   r = n;

   for (;;) {
      for (; r > 1; r--)
         count[r - 1] = r;
      if (perm1[0] != 0 && perm1[n - 1] != n - 1) {
         for (i = 0; i < n; i++)
            perm[i] = perm1[i];
         flips = 0;
         k = perm[0];
         do {
            for (i = 1, j = k - 1; i < j; i++, j--) {
               tmp = perm[i];
               perm[i] = perm[j];
               perm[j] = tmp;
            }
            flips++;
            tmp = perm[k];
            perm[k] = k;
            k = tmp;
         } while (k);
         if (maxflips < flips)
            maxflips = flips;
      }
      for (;;) {
         if (r >= n - 1) {
            free(perm1);
            free(perm);
            free(count);
            return cast(void*)maxflips;
         }

         {
            int p0 = perm1[0];
            for (i = 0; i < r; i++)
               perm1[i] = perm1[i + 1];
            perm1[i] = p0;
         }
         if (--count[r] > 0)
            break;
         r++;
      }
   }
}

static sizeint
fannkuch(sizeint n)
{
   worker_args* args; worker_args* targs;
   sizeint showmax = 30;
   sizeint* perm1; sizeint* count; 
   sizeint i, r, maxflips, flips;

   args = null;
   for (i = 0; i < n - 1; i++) {
      targs = cast(worker_args*)malloc(worker_args.sizeof);
      targs.i = i;
      targs.n = n;
      targs.next = args;
      args = targs;
version (USETHREADS) {
      if (0 != pthread_create(&args.id, cast(pthread_attr_t*)null, &fannkuch_worker, cast(void*)args))
         abort();
}
   }

   perm1 = cast(sizeint*)malloc(n * sizeint.sizeof);
   count = cast(sizeint*)malloc(n * sizeint.sizeof);

   for (i = 0; i < n; i++)
      perm1[i] = i;

   r = n;
   for (;;) {
      if (showmax) {
         for (i = 0; i < n; i++)
            printf("%d", perm1[i] + 1);
         printf("\n");
         showmax--;
      } else
         goto cleanup;

      for (; r > 1; r--)
         count[r - 1] = r;

      for (;;) {
         if (r == n)
            goto cleanup;
         {
            int p0 = perm1[0];
            for (i = 0; i < r; i++)
               perm1[i] = perm1[i + 1];
            perm1[i] = p0;
         }
         if (--count[r] > 0)
            break;

         r++;
      }
   }

    cleanup:
   free(perm1);
   free(count);
   maxflips = 0;
   while (args != null) {
version(USETHREADS) {
      if (0 != pthread_join(args.id, cast(void**)&flips))
         abort();
} else {
      flips = cast(sizeint)fannkuch_worker(args);
}
      if (maxflips < flips)
         maxflips = flips;
      targs = args;
      args = args.next;
      free(targs);
   }
   return maxflips;
}

int main(char[][] args)
{
   int n = args.length > 1 ? atoi(args[1].ptr) : 0;

   if (n < 1) {
      printf("Wrong argument.\n");
      return 1;
   }
   printf("Pfannkuchen(%d) = %d\n", n, fannkuch(n));
   return 0;
}

