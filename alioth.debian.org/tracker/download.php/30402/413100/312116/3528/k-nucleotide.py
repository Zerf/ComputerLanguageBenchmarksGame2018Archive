# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
#
# contributed by Jeroen Dirks
# added multiprocessing to submission by Ian Osgood
# in order to use all available cores.
# modified by Paul Ivanov: added the default dictionary to get rid of if
# statement in gen_freq and removed some local variables to improve
# performance.

from sys import stdin
from multiprocessing import Pool
from collections import defaultdict

def gen_freq(seq, frame, freqs):
    for ii in xrange(len(seq)+1-frame):
        freqs[seq[ii:ii+frame]] += 1
    return ii+1

def sort_seq(seq, length, freqs):
    n = gen_freq(seq, length, freqs)
    l = sorted(freqs.items(), reverse=True, key=lambda (seq,freq): (freq,seq))
    return '\n'.join("%s %.3f" % (st, 100.0*fr/n) for st,fr in l) + "\n"

def test_process(key):
    freqs = defaultdict(lambda: 0)
    if key in ["1","2"]:
        return sort_seq( seq, int(key), freqs)
    else:
        gen_freq(seq, len(key), freqs )
        return "%d\t%s" % (freqs[key], key)

def readSeq():
    for line in stdin:
        if line[0:3] == ">TH":
            break
    seq = []
    for line in stdin:
        if line[0] in ">;":
            break
        seq.append( line[:-1] )
    return  "".join(seq).upper()

if __name__ == '__main__':
    seq = readSeq()
    pool = Pool()
    result = pool.map(test_process, "1 2 GGT GGTA GGTATT GGTATTTTAATT GGTATTTTAATTTATAGT".split())
    for line in result:
        print line
