﻿﻿// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org/
//
// Modified version of Valentin Kraevskiy
// Contributed by Vassil Keremidchiev
open System.Text.RegularExpressions
open System.Threading

let regex s = Regex (s, RegexOptions.Compiled)
let input = stdin.ReadToEnd ()
let withoutComments = (regex ">.*\n").Replace (input, "")
let text = (regex "\n").Replace (withoutComments, "")

["agggtaaa|tttaccct"
 "[cgt]gggtaaa|tttaccc[acg]"
 "a[act]ggtaaa|tttacc[agt]t"
 "ag[act]gtaaa|tttac[agt]ct"
 "agg[act]taaa|ttta[agt]cct"
 "aggg[acg]aaa|ttt[cgt]ccct"
 "agggt[cgt]aa|tt[acg]accct"
 "agggta[cgt]a|t[acg]taccct"
 "agggtaa[cgt]|[acg]ttaccct"]
|> List.map (fun s -> async { return (s, ((regex s).Matches text).Count) } ) 
|> Async.Parallel |> Async.RunSynchronously 
|> Array.iter (fun (s,i) -> printf "%s %i\n" s i)

let newText =
    ["B", "(c|g|t)"
     "D", "(a|g|t)"
     "H", "(a|c|t)"
     "K", "(g|t)"
     "M", "(a|c)"
     "N", "(a|c|g|t)"
     "R", "(a|g)"
     "S", "(c|g)"
     "V", "(a|c|g)"
     "W", "(a|t)"
     "Y", "(c|t)"]
     |> List.fold (fun s (code, alt) -> (regex code).Replace (s, alt)) text

printf "\n%i\n%i\n%i\n" input.Length text.Length newText.Length