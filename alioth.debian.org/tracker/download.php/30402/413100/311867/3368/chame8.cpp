/* The Computer Language Benchmarks Game
http://shootout.alioth.debian.org/

Based on C contribution by Alex Burlyga
Based on Java contribution by Michael Barker
Contributed by The Anh Tran
*/


#include <iostream>
#include <string>

#include <cmath>
#include <memory.h>

#include <sched.h>

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <boost/bind.hpp>
#include <boost/scoped_ptr.hpp>

#include "threadpool/boost/threadpool.hpp"


using namespace std;
using namespace boost;

bool multi_core;
scoped_ptr<boost::threadpool::pool>		pool;


enum COLOR
{
	BLUE	= 0,
	RED		= 1,
	YELLOW	= 2
};

char const * const ColorName[]	= {"blue", "red", "yellow"};


COLOR 
ColorCompliment(COLOR c1, COLOR c2)
{
	switch (c1)
	{
	case BLUE:
		switch (c2)
		{
		case BLUE:		return BLUE;
		case RED:		return YELLOW;
		case YELLOW:	return RED;
		}

	case RED:
		switch (c2)
		{
		case BLUE:		return YELLOW;
		case RED:		return RED;
		case YELLOW:	return BLUE;
		}

	case YELLOW:
		switch (c2)
		{
		case BLUE:		return RED;
		case RED:		return BLUE;
		case YELLOW:	return YELLOW;
		}
	}

	cerr << "Invalid color\n";
	return BLUE;
}


string
SpellNumber(int n)
{
	static char const* const NumberStr[] = 
	{
		"zero ", "one ", "two ", "three ", "four ",
		"five ", "six ", "seven ", "eight ", "nine "
	};
	string num;
	
	while ( n >= 10 )
	{
		int m = n % 10;
		n /= 10;
		
		num = NumberStr[m] + num;
	}

	num = NumberStr[n] + num;
	return num;
}



struct Creature;
struct MeetingPlace
{
	int	volatile	_meetings_left;
	Creature*		_creature_waiting;


	MeetingPlace(int N) 
	:	_meetings_left(N), 
		_creature_waiting(0)
	{	}

	bool GotoMeet( Creature* cr );
};


struct Creature
{
	MeetingPlace *		_place;
	COLOR				_color;

	int					_count;
	int					_same_count;
	bool volatile		_met;

	int					_id;
	double				_dummy;


	Creature()
	{
		memset(this, 0, sizeof(*this));
	}

	void EnQueue() 
	{
		pool->schedule(bind(&Creature::Run, this));
	}
	
	void 
	Start( MeetingPlace * mp, COLOR color)
	{
		_place	= mp;
		_color	= color;
		_dummy = _id = reinterpret_cast<int>(this);
		
		EnQueue();
	}

	void 
	Run()
	{
		_met = false;

		if ( _place->GotoMeet( this ) )
		{
			while ((_met == false) && (_place->_meetings_left > 0))
			{
				if (multi_core)	// multi thread: spin wait
					++_dummy;
				else			// single thread: just return, the other creature will schedule for this creature
					return ;
			}
			++_count;

			EnQueue();
		}
	}

	void 
	Meet(Creature &other)
	{
		if (__builtin_expect(_id == other._id, false))
		{
			++_same_count;
			++other._same_count;
		}

		COLOR new_color	= ColorCompliment( _color, other._color );
		other._color	= _color	= new_color;
		other._met		= _met		= true;
		
		if (!multi_core)
			other.EnQueue();
	}

};

bool 
MeetingPlace::GotoMeet( Creature* cr )
{
	while (true)
	{
		// store pointer to first creature
		Creature* first = reinterpret_cast<Creature*>(__sync_val_compare_and_swap(&_creature_waiting, 0, cr));
		
		if (first == 0)	// first arrive
			return (_meetings_left > 0);
		else	// second arrive
		{
			if (__sync_bool_compare_and_swap(&_creature_waiting, first, 0))
			{
				if (__sync_fetch_and_sub(&_meetings_left, 1) > 0)
				{
					// 2 creatures meeting each other
					cr->Meet(*first);
					return true;
				}
				else
					return false;
			}
		}
	}
}


template <int ncolor>
void 
RunGame(int n, COLOR const (&color)[ncolor])
{
	MeetingPlace	place(n);
	Creature		cr[ncolor];
	
	format fmt("%1% ");
	// print initial color of each creature
	for (int i = 0; i < ncolor; ++i)
	{
		cout << (fmt % ColorName[color[i]] );
		cr[i].Start( &place, color[i] );
	}

	cout << endl;


	// wait for them to meet
	pool->wait();


	int total = 0;
	fmt = format("%1% %2%\n");
	// print meeting times of each creature
	for (int i = 0; i < ncolor; i++)
	{
		total += cr[i]._count;
		cout << (fmt % cr[i]._count % SpellNumber(cr[i]._same_count));
	}

	// print total meeting times
	fmt = format(" %1%\n\n");
	cout << (fmt % SpellNumber(total));
}

void 
PrintColors()
{
	format fmt("%1% + %2% -> %3%\n");
	
	for (int c1 = BLUE; c1 <= YELLOW; ++c1)
	{
		for (int c2 = BLUE; c2 <= YELLOW; ++c2)
		{
			cout << (fmt 
						% ColorName[c1] 
						% ColorName[c2] 
						% ColorName[ColorCompliment( (COLOR)c1, (COLOR)c2 )]  );
		}
	}

	cout << endl;
}


int 
GetThreadCount()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	for (int i = 0; i < 16; ++i)
	{
		if (CPU_ISSET(i, &cs))
			++count;
	}
	return count;
}


int 
main(int argc, char** argv)
{
	PrintColors();
	
	// Spawn N threads; ie: 1 or 4, depends on benchmark
	int nc = GetThreadCount();
	pool.reset(new boost::threadpool::pool(nc));
	multi_core = nc > 1;


	COLOR const r1[] = {   BLUE, RED, YELLOW   };
	COLOR const r2[] = {   BLUE, RED, YELLOW, RED, YELLOW, BLUE, RED, YELLOW, RED, BLUE   };
	
	int n = (argc >= 2) ? lexical_cast<int>(argv[1]) : 600;
	RunGame( n, r1 );
	RunGame( n, r2 );
	
	return 0;
}

