# The Computer Language Shootout Benchmarks
# http://shootout.alioth.debian.org/
#
# contributed by Antoine Pitrou
# modified by Dominique Wahli and Daniel Nanz

from __future__ import print_function

import sys
import multiprocessing as mp


def make_tree(i, d):
    
    if d > 0:
        i2 = i + i
        d -= 1
        return (i, make_tree(i2 - 1, d), make_tree(i2, d))
    else:
        return (i, None, None)


def check_tree(n):

    (i, l, r) = n
    if l is None:
        return i
    else:
        return i + check_tree(l) - check_tree(r)


def make_and_check(dep_iter_msg, cs=0, make=make_tree, check=check_tree):

    d, iters, msg = dep_iter_msg
    for i in range(1, iters + 1):
        cs += check(make(i, d))
        cs += check(make(-i, d))
    return msg % (i * 2, d, cs)
    

def main(n, min_d=4, make=make_tree, check=check_tree):
    
    max_d = max(min_d + 2, n)
    stretch_d = max_d + 1
    out = []
    msg = ['stretch tree of depth %d\t check: %d',
           '%d\t trees of depth %d\t check: %d',
           'long lived tree of depth %d\t check: %d']
    
    out.append(msg[0] % (stretch_d, check(make(0, stretch_d))))
    long_lived_tree = make(0, max_d)

    mmd = max_d + min_d
    dims = []
    for d in range(min_d, stretch_d, 2):
        dims.append((d, 2 ** (mmd - d ), msg[1]))
    out.extend(mp.Pool().map(make_and_check, dims, chunksize=4))
    
    out.append(msg[2] % (max_d, check(long_lived_tree)))
    return '\n'.join(out)


if __name__ == '__main__':
    print(main(int(sys.argv[1])))
