﻿/// The Computer Language Benchmarks Game
/// http://shootout.alioth.debian.org/
///
/// contributed by Cesar Mendoza
///
#light
open System
open System.Text
open System.IO
open System.Collections.Generic

let complements = Map.of_list [ ('A'B , 'T'B); ( 'a'B , 'T'B); ( 'C'B , 'G'B); ( 'c'B , 'G'B); ( 'G'B , 'C'B);
                                ('g'B , 'C'B); ( 'T'B , 'A'B); ( 't'B , 'A'B); ( 'U'B , 'A'B); ( 'u'B , 'A'B);
                                ('M'B , 'K'B); ( 'm'B , 'K'B); ( 'R'B , 'Y'B); ( 'r'B , 'Y'B); ( 'Y'B , 'R'B);
                                ('y'B , 'R'B); ( 'K'B , 'M'B); ( 'k'B , 'M'B); ( 'V'B , 'B'B); ( 'v'B , 'B'B);
                                ('H'B , 'D'B); ( 'h'B , 'D'B); ( 'D'B , 'H'B); ( 'd'B , 'H'B); ( 'B'B , 'V'B); 
                                ( 'b'B , 'V'B)]
type End   = int
type Start = int
type Fasta = {header: string; body: Start * End }

let stream = Console.OpenStandardOutput()
let buffer = Array.create 16384 0uy
let width = 60
let mutable index = 0
let inline flush () =
    stream.Write (buffer, 0, index)
    index <- 0
let inline write b =
    buffer.[index] <- b
    index <- index + 1
    if index = buffer.Length then flush ()
    
type DataArray(stream: BinaryReader) =
    let buffer = false |> Seq.unfold(fun finished -> if finished then None else let d = stream.ReadBytes(16384) in if d.Length < 16384 then Some(d,true) else Some(d,false)) |> Seq.to_array
    member x.Item
        with public get n =
            let i,j = n / 16384, n % 16384
            buffer.[i].[j]
        and public set n k =
            let i,j = n / 16384, n % 16384
            buffer.[i].[j] <- k
    member x.seq = seq {
        for arr in buffer do
            for b in arr do
                yield b
    }
    
let kk = Array.create 0 0
let writeComplement (data: DataArray) (fasta: Fasta) =
    fasta.header |> Seq.iter (byte >> write)
    let index = ref (snd fasta.body) //finish
    let finish = fst fasta.body //start
    let n = ref 1
    while !index >= finish do
        let ch = data.[!index]
        if ch <> '\n'B then
            ch |> byte |> write
            if !n % width = 0 then write '\n'B
            incr n
        decr index
    if (!n - 1) % width <> 0 then write '\n'B

let convert (data: DataArray) = 
    let comment = ref false
    let headerBuffer = ref (new StringBuilder(80))
    let index = ref 0
    let start = ref 0
    seq {
        for ch in data.seq do            
            if ch = '>'B then 
                if !index <> 0 then
                    yield {header = (!headerBuffer).ToString(); body= !start,!index - 1}
                    headerBuffer := new StringBuilder(80)
                comment := true
                (!headerBuffer).Append(char ch) |> ignore
            elif !comment then
                (!headerBuffer).Append(char ch) |> ignore
                if ch = '\n'B then
                    comment := false
                    start := !index + 1
            else
                if ch <> '\n'B then
                    match complements.TryFind(ch) with
                        | None -> ()
                        | Some cmp -> data.[!index] <- cmp
            index := !index + 1
        yield {header = (!headerBuffer).ToString(); body = !start,!index - 1 }
    }

let data = DataArray(new BinaryReader(Console.OpenStandardInput()))
data |> convert |> Seq.iter (writeComplement data)
flush()
    