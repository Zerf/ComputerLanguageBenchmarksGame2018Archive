;; The Computer Language Benchmarks Game
;;   http://shootout.alioth.debian.org/
;;
;;  contributed by Jim Kannampuzha
;;  version 0.9

(ns specnorm
 (:import (java.text DecimalFormat) (java.util Locale))
 (:use clojure.test)
 (:gen-class))

(set! *warn-on-reflection* true)


(def num-threads (.availableProcessors (Runtime/getRuntime)))
(def workers (vec (repeatedly num-threads #(agent ()))))
(def indices (atom ()))
(def chunks (atom ()))

(defn eval-A
  [ii jj] (let [i (int ii)
                j (int jj)
                div (+ (* j j)
                       (+ (* (+ i i) j)
                          (- (+ (* i i)(- (int 2) i)) (* (int 3) j))))]
            (/ (double 2) div)))

(defn mult-Av [a chunk v]
  (for [i chunk] (reduce + (map * (for [j @indices] (eval-A i j)) v))))

(defn mult-Atv [a chunk v]
  (for [i chunk] (reduce + (map * (for [j @indices] (eval-A j i)) v))))

(defn mult-AtAv [v]
  (dotimes [i num-threads] (send (nth workers i) mult-Atv (nth @chunks i) v))
  (apply await workers)
  (let [w (apply concat (map deref workers))]
    (dotimes [i num-threads] (send (nth workers i) mult-Av (nth @chunks i) w)))
  (apply await workers)
  (apply concat (map deref workers)))

(defn spec [n]
  (let [chunk-size (Math/ceil (/ n (double num-threads)))]
    (reset! indices (range 1 (inc n)))
    (reset! chunks (vec (partition-all chunk-size @indices)))
    (let [u (repeat n 1.0)
          v (nth (iterate mult-AtAv u) 19)
          uv (mult-AtAv v)
          vBv (reduce + (map * uv v))
          vv (reduce + (map * v v))]
      (java.lang.Math/sqrt (double (/ vBv vv))))))

(defn -main [& args]
  (let [n (if (first args) (Integer/parseInt (first args)) 1000)
        df (doto ^DecimalFormat (DecimalFormat/getNumberInstance Locale/US)
             (.applyPattern "#.000000000"))]
    (println (.format df (spec n))))
  (shutdown-agents))
