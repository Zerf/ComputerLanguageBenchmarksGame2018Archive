function go ()
   while true do
      coroutine.yield ()
   end
end

local n = tonumber (arg[1])

local ring = {}
for i = 1, 503 do
   ring[i] = {coroutine.create (go), i + 1}
end

ring[503][2] = 1

local index = 1
coroutine.resume (ring[index][1])
for i = 1, n do
   coroutine.resume (ring[index][1])
   index = ring[index][2]
end
print (index)
