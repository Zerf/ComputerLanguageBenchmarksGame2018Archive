/*
	The Computer Language Benchmarks Game
	http://shootout.alioth.debian.org/

	Based on C contribution of Josh Goldfoot. And modified by Cedric Bail.
	Based on bit encoding idea of C++ contribution of Andrew Moon
	Contributed by The Anh Tran
*/

#define _GNU_SOURCE
#include <omp.h>
#include <sched.h>

#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <stdint.h>

// Use hashmap lib written by Heng Li
#include "khash.h"


typedef unsigned int uint;
typedef uint64_t	ui64;

#define BIT_PER_CODE	2
#define MAX_THREADS		16
#define KNU_TEST		7


// Hastable<ui64, uint>
KHASH_MAP_INIT_INT64(knu64, uint)


void
ReadAll(char* *input, size_t *length)
{
	// get input size
	*length = ftell(stdin);
	fseek(stdin, 0, SEEK_END);
	*length = ftell(stdin) - *length;
	fseek(stdin, 0, SEEK_SET);

	*input = (char*)malloc(*length);

	// rule: read line-by-line
	char buffer[64];
	while (fgets_unlocked(buffer, sizeof(buffer), stdin))
	{
		if ((buffer[0] == '>') && (strncmp(buffer, ">THREE", 6) == 0))
			break;
	}

	size_t const LL = 60;
	buffer[LL] = 0;
	size_t byte_read = 0;

	while (fgets_unlocked(buffer, sizeof(buffer), stdin))
	{
		if (buffer[LL] == '\n')
		{
			memcpy(*input + byte_read, buffer, LL);
			byte_read += LL;
		}
		else
		{
			size_t sz = strlen(buffer);
			if (buffer[sz -1] == '\n')
				--sz;

			memcpy(*input + byte_read, buffer, sz);
			byte_read += sz;
		}

		buffer[LL] = 0;
	}

	*length = byte_read;
	(*input)[ byte_read ] = 0;
}


typedef struct
{
	size_t			frame_size;
	khash_t(knu64)	*hash_table[MAX_THREADS];

	size_t			stride_processed;
	size_t			thread_passed;
}	Result_t;

Result_t result_list[KNU_TEST] = {{1}, {2}, {3}, {4}, {6}, {12}, {18}};


ui64
CodeToBit(char c)
{
	switch (c)
	{
	case 'a':	return 0;
	case 't':	return 1;
	case 'c':	return 2;
	case 'g':	return 3;

	case 'A':	return 0;
	case 'T':	return 1;
	case 'C':	return 2;
	case 'G':	return 3;
	}

	assert(0);
	return (ui64)(-1);
}


char
BitToCode(uint bit)
{
	static
	char const tb[4] = {'A', 'T', 'C', 'G'};

	assert(bit < 4);
	return tb[bit];
}


ui64
EncodeKey(char const* data, size_t hash_length)
{
	ui64 key = 0;
	size_t index = 0;
	size_t shift = 0;

	for (; index < hash_length; ++index)
	{
		key |= (CodeToBit(data[index]) << shift);

		shift += BIT_PER_CODE;
	}

	return key;
}

char*
DecodeKey(char* buffer, size_t frame_size, ui64 key)
{
	size_t index = 0;
	ui64 extract_mask = 3;

	for (; index < frame_size; ++index)
	{
		uint extract_value = ((key & extract_mask) >> (index * BIT_PER_CODE));
		buffer[index] = BitToCode(extract_value);

		extract_mask <<= BIT_PER_CODE;
	}

	return buffer;
}

void
MergeTable(khash_t(knu64) *des, khash_t(knu64) * src)
{
	khiter_t src_ite, src_iend;

    for (src_ite = kh_begin(src), src_iend = kh_end(src); src_ite != src_iend; ++src_ite)
    {
		if (kh_exist(src, src_ite))
		{
			ui64 src_key = kh_key(src, src_ite);
			uint src_val = kh_val(src, src_ite);

			int	existed;
			khiter_t des_ite = kh_put(knu64, des, src_key, &existed);

			if (existed == 0)
				kh_val(des, des_ite) += src_val;
			else
				kh_val(des, des_ite) = src_val;
		}
	}
}

void
BuildHashTableAtOffset(char const* data, size_t data_length,
						khash_t(knu64)* ht, size_t frame_size, size_t start_offset)
{
	size_t i_begin = start_offset;
	size_t i_end = data_length - frame_size + 1;

	for(; i_begin < i_end; i_begin += frame_size)
	{
		ui64 key = EncodeKey(data + i_begin, frame_size);

		int	existed;
		khiter_t ite = kh_put(knu64, ht, key, &existed);

		if (existed == 0)
			kh_val(ht, ite) += 1;
		else
			kh_val(ht, ite) = 1;
	}
}



void
BuildHashTable(char const* data, size_t data_length, int n_element)
{
	size_t offset;
	Result_t *result = &(result_list[n_element]);

	khash_t(knu64)* local_table = kh_init(knu64);
	result->hash_table[omp_get_thread_num()] = local_table;


	while ( (offset = __sync_fetch_and_add(&result->stride_processed, 1)) < result->frame_size )
		BuildHashTableAtOffset(data, data_length, local_table, result->frame_size, offset);


	if (__sync_add_and_fetch(&result->thread_passed, 1) == omp_get_num_threads())
	{
		int i = 1;
		for (; i < omp_get_num_threads(); ++i)
		{
			MergeTable(result->hash_table[0], result->hash_table[i]);

			kh_destroy(knu64, result->hash_table[i]);
			result->hash_table[i] = 0;
		}
	}

}


typedef struct
{
	ui64	key;
	int		value;
} KVPair;


int
DescCompFunc(void const*p1, void const* p2)
{
	KVPair const* k1 = (KVPair const*)p1;
	KVPair const* k2 = (KVPair const*)p2;

	return -(k1->value - k2->value);
}

void
WriteFreq(Result_t const* result, size_t length)
{
	khash_t(knu64) const *ht = result->hash_table[0];

	uint n_elements		= kh_size(ht);
	KVPair* sorted_list	= (KVPair*)calloc(n_elements, sizeof(KVPair));

	khiter_t ite		= kh_begin(ht);
	khiter_t ite_end	= kh_end(ht);

	int		index = 0;

	for (; ite != ite_end; ++ite)
	{
		if (kh_exist(ht, ite))
		{
			sorted_list[index].key		= kh_key(ht, ite);
			sorted_list[index].value	= kh_val(ht, ite);

			++index;
		}
	}

	n_elements = index;
	qsort(sorted_list, n_elements, sizeof(KVPair), DescCompFunc);

	char buffer[64];
	float total = length - result->frame_size +1;

	for (index = 0; index < n_elements; ++index)
	{
		buffer[result->frame_size] = 0;

		printf("%s %.3f\n",
			DecodeKey(buffer, result->frame_size, sorted_list[index].key),
			sorted_list[index].value * 100.0f / total);
	}

	printf("\n");
	free(sorted_list);
}

void
WriteCount(Result_t const* result, char const* pattern)
{
	khash_t(knu64) const *ht = result->hash_table[0];

	ui64	key		= EncodeKey(pattern, result->frame_size);
	khiter_t ite	= kh_get(knu64, ht, key);
	uint 	val		= (ite != kh_end(ht)) ? kh_val(ht, ite) : 0;

	printf("%d\t%s\n", val, pattern);
}

int
GetThreadCount()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	int i = 0;
	for (; i < MAX_THREADS; ++i)
	{
		if (CPU_ISSET(i, &cs))
			++count;
	}
	return count;
}


int
main()
{
	char*	input;
	size_t	length;
	ReadAll(&input, &length);

	#pragma omp parallel num_threads(GetThreadCount()) default(shared)
	{
		int i;
		for (i = 0; i < KNU_TEST; ++i)
			BuildHashTable(input, length, i);
	}

	WriteFreq(&result_list[0], length);
	WriteFreq(&result_list[1], length);

	WriteCount(&result_list[2], "GGT");
	WriteCount(&result_list[3], "GGTA");
	WriteCount(&result_list[4], "GGTATT");
	WriteCount(&result_list[5], "GGTATTTTAATT");
	WriteCount(&result_list[6], "GGTATTTTAATTTATAGT");

	{
		free(input);

		int i;
		for (i = 0; i < KNU_TEST; ++i)
			kh_destroy(knu64, result_list[i].hash_table[0]);
	}

	return 0;
}
