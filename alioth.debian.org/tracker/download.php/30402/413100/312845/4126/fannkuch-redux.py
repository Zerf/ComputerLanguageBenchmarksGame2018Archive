#    The Computer Language Benchmarks Game
#    http://shootout.alioth.debian.org/

#    contributed by Isaac Gouy
#    converted to Java by Oleg Mazurov
#    converted to Python by Buck Golemon
#    modified by Justin Peel

def fannkuch(n):
    maxFlipsCount = 0
    permCount = False
    checksum = 0

    r = n
    perm1 = range(1,n+1)
    count = [0] * r
    ranges = [None, None] + [range(2,x+1) for x in xrange(2,n+1)]
    rxrange = xrange(3,n)
    nm = n - 1
    while True:
        if r != 1:
            count[1:r] = ranges[r]
            r = 1
        k=perm1[0]
        if k != 1:
            perm = perm1[:]
            perm[:k] = perm[k-1::-1]
            flipsCount = 1
            k=perm[0]
            while k != 1:
                perm[:k] = perm[k-1::-1]
                flipsCount += 1
                k=perm[0]
        
            maxFlipsCount = flipsCount if maxFlipsCount < flipsCount else maxFlipsCount
            checksum += -flipsCount if permCount else flipsCount


        # Use incremental change to generate another permutation
        perm1[0],perm1[1] = perm1[1],perm1[0]
        count[1] -= 1
        if not count[1]:
            perm1[0],perm1[1],perm1[2] = perm1[1],perm1[2],perm1[0]
            count[2] -= 1
            if not count[2]:
                for r in rxrange:
                    perm0 = perm1[0]
                    perm1[:r] = perm1[1:r+1]
                    perm1[r] = perm0
                    count[r] -= 1
                
                    if count[r]:
                        break
                if r == nm and count[nm] == 0:
                    print( checksum )
                    return maxFlipsCount
            else:
                r = 2
            
        permCount = not permCount
from sys import argv
n = int(argv[1])

print( "Pfannkuchen(%i) = %i" % (n, fannkuch(n)) )
