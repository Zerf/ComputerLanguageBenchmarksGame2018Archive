/* The Computer Language Benchmarks Game
   http://shootout.alioth.debian.org/

   contributed by Simon Oberhammer

   transliterated from Matthew Wilson's JavaScript code
   uses java.math.BigInteger as would anyone do on Rhino
*/

importClass(java.math.BigInteger);
function main($n) {
   var $i=1;
   var $s="";
   var $d, $g;
   var neg10 = new BigInteger(-10);
   var three = new BigInteger(3);
   var ten = new BigInteger(10);
   var g = 1;
   var digits=Array(10);
   var $z0 = new BigInteger(1);
   var $z1 = new BigInteger(0);
   var $z2 = new BigInteger(1);
   var negdigits = Array(10);
   var k = 0;
   var $k;
   var l = 2;
   var $l;
   var a;

   for(var i=0; i<10; ++i) {
      digits[i] = new BigInteger(i);
      negdigits[i] = digits[i].multiply(neg10);
   }

   do {
       while ($z0.compareTo($z2) > 0
            || ($d = $z0.multiply(three).add($z1).divide($z2).intValue()) !=
                ($z0.shiftLeft(2).add($z1).divide($z2).intValue())
       ) {
         g+=2;
         $g = new BigInteger(g);
         $z1 = $z1.multiply($g);
         $z2 = $z2.multiply($g);
         l+=4;
         $l = new BigInteger(l);
         $z1 = $z1.add($z0.multiply($l));
         $k = new BigInteger(++k);
         $z0 = $z0.multiply($k);
       };
       $z0 = $z0.multiply(ten);
       $z1 = $z1.multiply(ten);
       $z1 = $z1.add($z2.multiply(negdigits[$d]));
       $s += $d;
       if ($i % 10 == 0) {
         print($s+"\t:"+$i);
         $s="";
       };
   } while (++$i <= $n)

   if (($i = $n % 10) != 0) {
      $s += Array(11 - $i).join(' ');
   }
   if ($s.length > 0) {
      print($s+"\t:"+$n);
   };
}

main(parseInt(arguments[0],10));
