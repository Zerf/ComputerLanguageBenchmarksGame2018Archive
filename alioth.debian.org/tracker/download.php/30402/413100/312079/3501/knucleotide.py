# The Computer Language Shootout
# http://shootout.alioth.debian.org/
#
# submitted by Jeroen Dirks
# added multiprocessing to submission by Ian Osgood it order to use
# all available cores.

from sys import stdin
from multiprocessing import Pool

def gen_freq(seq, frame, frequences):
    ns = len(seq) + 1 - frame
    for ii in xrange(ns):
        nucleo = seq[ii:ii + frame]
        try:
            frequences[nucleo] += 1
        except KeyError:
            frequences[nucleo] = 1
    return ns, frequences

def sort_seq(seq, length, frequences):
    n, frequences = gen_freq(seq, length, frequences)

    l = sorted(frequences.items(), reverse=True, key=lambda (seq,freq): (freq,seq))

    return '\n'.join("%s %.3f" % (st, 100.0*fr/n) for st,fr in l) + "\n"

def test_process(key):
    frequences = {}
    if key in ["1","2"]:
        return sort_seq( sequence, int(key), frequences )
    else:
        n, t = gen_freq(sequence, len(key), frequences )
        return "%d\t%s" % (t.get(key, 0), key)

def readSequence():
    for line in stdin:
        if line[0:3] == ">TH":
            break
    seq = []
    for line in stdin:
        if line[0] in ">;":
            break
        seq.append( line[:-1] )
    return  "".join(seq).upper()

if __name__ == '__main__':
    sequence = readSequence()
    
    pool = Pool()
    result = pool.map(test_process,  "1 2 GGT GGTA GGTATT GGTATTTTAATT GGTATTTTTAATTTATAGT".split())
    for line in result:
        print line
