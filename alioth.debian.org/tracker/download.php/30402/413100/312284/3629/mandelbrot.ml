(*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Christophe TROESTLER
 * Enhanced by Christian Szegedy, Yaron Minsky
 * Enhanced by Otto Bommer
 *
 *)

let niter = 50
let limit = 2.

let limit2 = limit *. limit

type complex = { mutable r: float; mutable i: float }

let () =
  let w = try int_of_string(Array.get Sys.argv 1) with _ -> 1600 in
  let h = w in
  let fw = float w
  and fh = float h in
  Printf.printf "P4\n%i %i\n" w h;
  let c = { r=0.; i=0. } and z = { r=0.; i=0. } in
  let byte = ref 0 in
  for y = 0 to h - 1 do
    c.i <- float (y * 2) /. fh -. 1.;
    for x = 0 to w - 1 do
      c.r <- float (x * 2) /. fw -. 1.5;
      z.r <- 0.; z.i <- 0.;
      let i = ref niter in
      while !i > 0 do
        let ti1 = z.r *. z.i in
        let ti = ti1 +. ti1 +. c.i in
        z.r <- z.r *. z.r -. z.i *. z.i +. c.r;
        z.i <- ti;
        decr i;
        if z.r *. z.r +. ti *. ti > limit2 then begin byte := !byte lsl 1; i := 0 end
        else if !i <= 0 then byte := (!byte lsl 1) lor 0x01 
      done;
      if x mod 8 = 7 then output_byte stdout !byte;
    done;
    if w mod 8 != 0 then output_byte stdout (!byte lsl (8-w mod 8));
    byte := 0;
  done

