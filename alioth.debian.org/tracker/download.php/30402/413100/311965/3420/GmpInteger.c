#include "GmpInteger.h"
#include <gmp.h>

/*
 * Class:     GmpInteger
 * Method:    mpz_init
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_GmpInteger_mpz_1init
  (JNIEnv *x, jclass y)
{
  mpz_t *p = malloc( sizeof(__mpz_struct) );
  __gmpz_init( *p );

  return (jlong)p;
}

/*
 * Class:     GmpInteger
 * Method:    mpz_clear
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_GmpInteger_mpz_1clear
  (JNIEnv *x, jclass y, jlong o)
{
  mpz_t* p = (mpz_t*)o;
  __gmpz_clear( *p );
  free( p );
}

/*
 * Class:     GmpInteger
 * Method:    mpz_mul_si
 * Signature: (JJI)V
 */
JNIEXPORT void JNICALL Java_GmpInteger_mpz_1mul_1si
  (JNIEnv *x, jclass y, jlong dest, jlong src, jint val)
{
  __gmpz_mul_si( *((mpz_t*)dest), *((mpz_t*)src), (long) val);
}

/*
 * Class:     GmpInteger
 * Method:    mpz_add
 * Signature: (JJJ)V
 */
JNIEXPORT void JNICALL Java_GmpInteger_mpz_1add
  (JNIEnv *x, jclass y, jlong dest, jlong src, jlong val)
{
  __gmpz_add( *((mpz_t*)dest), *((mpz_t*)src), *((mpz_t*)val) );
}

/*
 * Class:     GmpInteger
 * Method:    mpz_tdiv_q
 * Signature: (JJJ)V
 */
JNIEXPORT void JNICALL Java_GmpInteger_mpz_1tdiv_1q
  (JNIEnv *x, jclass y, jlong dest, jlong src, jlong val)
{
  __gmpz_tdiv_q( *((mpz_t*)dest), *((mpz_t*)src), *((mpz_t*)val) );
}

/*
 * Class:     GmpInteger
 * Method:    mpz_set_si
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL Java_GmpInteger_mpz_1set_1si
  (JNIEnv *x, jclass y, jlong dest, jint val)
{
  __gmpz_set_si( *((mpz_t*)dest), (long) val);
}

/*
 * Class:     GmpInteger
 * Method:    mpz_get_si
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_GmpInteger_mpz_1get_1si
  (JNIEnv *x, jclass y, jlong dest)
{
  return (jint)__gmpz_get_si( *((mpz_t*)dest));
}
