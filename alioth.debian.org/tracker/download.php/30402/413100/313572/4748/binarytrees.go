/* The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 *
 * contributed by The Go Authors.
 * based on C program by Kevin Carson
 * flag.Arg hack by Isaac Gouy
 * modified for performance by Tylor Arndt
 */

package main

import (
	"flag"
	"fmt"
	"strconv"
)

var n uint = 0

type Node struct {
	item        int
	left, right *Node
}

func bottomUpTree(item int, depth uint) *Node {
	nodes := make([]Node, 2<<depth-1)
	root := &nodes[0]
	nodec := 0
	buildChildren(root, item, depth, nodes, &nodec)
	return root
}

func buildChildren(parent *Node, item int, depth uint, nodes []Node, nodec *int) {
	parent.item = item
	if depth > 0 {
		depth--
		(*nodec)++
		parent.left = &nodes[(*nodec)]
		buildChildren(parent.left, item<<1-1, depth, nodes, nodec)
		(*nodec)++
		parent.right = &nodes[(*nodec)]
		buildChildren(parent.right, item<<1, depth, nodes, nodec)
	}
}

func (n *Node) itemCheck() int {
	if n.left == nil {
		return n.item
	}
	return n.item + n.left.itemCheck() - n.right.itemCheck()
}

const minDepth uint = 4

func main() {
	flag.Parse()
	if flag.NArg() > 0 {
		tmp, _ := strconv.Atoi(flag.Arg(0))
		n = uint(tmp)
	}

	var maxDepth uint = n
	if minDepth+2 > n {
		maxDepth = minDepth + 2
	}
	stretchDepth := maxDepth + 1

	check := bottomUpTree(0, stretchDepth).itemCheck()
	fmt.Printf("stretch tree of depth %d\t check: %d\n", stretchDepth, check)

	longLivedTree := bottomUpTree(0, maxDepth)

	for depth := minDepth; depth <= maxDepth; depth += 2 {
		iterations := 1 << uint(maxDepth-depth+minDepth)
		check = 0

		for i := 1; i <= iterations; i++ {
			check += bottomUpTree(i, depth).itemCheck()
			check += bottomUpTree(-i, depth).itemCheck()
		}
		fmt.Printf("%d\t trees of depth %d\t check: %d\n", iterations*2, depth, check)
	}
	fmt.Printf("long lived tree of depth %d\t check: %d\n", maxDepth, longLivedTree.itemCheck())
}
