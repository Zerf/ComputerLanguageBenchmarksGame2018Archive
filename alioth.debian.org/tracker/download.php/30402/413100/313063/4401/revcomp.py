# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
#
# contributed by Alex Gaynor

import array
import ctypes
import string
import sys


TRANS_TABLE = string.maketrans(
    "ACBDGHKMNSRUTWVYacbdghkmnsrutwvy",
    "TGVHCDMKNSYAAWBRTGVHCDMKNSYAAWBR",
)

libc = ctypes.CDLL("libc.so.6")
libc.write.argyptes = [ctypes.c_int, ctypes.c_void_p, ctypes.c_size_t]

def show(seq, out, out_idx, end, start):
    line_idx = 0
    while end >= start:
        ch = seq[end]
        if ch != "\n":
            out[out_idx] = TRANS_TABLE[ord(ch)]
            out_idx += 1
            line_idx += 1
            if line_idx == 60:
                out[out_idx] = "\n"
                out_idx += 1
                line_idx = 0
        end -= 1
        del ch
    if line_idx != 0:
        out[out_idx] = "\n"
        out_idx += 1
    return out_idx

def main():
    data = sys.stdin.read()

    out = array.array("c", ["\x00"]) * len(data)
    idx = 0
    out_idx = 0
    start_idx = 0

    while idx < len(data):
        ch = data[idx]
        if ch == ">" or ch == ";":
            if idx != 0:
                out_idx = show(data, out, out_idx, idx - 1, start_idx)
            while True:
                out[out_idx] = ch
                out_idx += 1
                idx += 1
                if ch == "\n":
                    break
                ch = data[idx]
            start_idx = idx
        else:
            idx += 1
        del ch
    out_idx = show(data, out, out_idx, idx - 1, start_idx)

    n = 0
    while n < out_idx:
        n += libc.write(sys.stdout.fileno(), out.buffer_info()[0] + n, out_idx - n)


if __name__ == "__main__":
    main()
