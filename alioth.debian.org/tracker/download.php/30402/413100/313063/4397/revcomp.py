# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
#
# contributed by Alex Gaynor

import array
import string
import sys


TRANS_TABLE = array.array("c",
    string.maketrans(
        "ACBDGHKMNSRUTWVYacbdghkmnsrutwvy",
        "TGVHCDMKNSYAAWBRTGVHCDMKNSYAAWBR",
    )
)

def show(seq, end):
    out = array.array("c", ['\x00']) * int(len(seq) * 1.05)
    line_idx = 0
    idx = 0
    while end >= 0:
        ch = seq[end]
        out[idx] = TRANS_TABLE[ord(ch)]
        idx += 1
        line_idx += 1
        if line_idx == 60:
            out[idx] = "\n"
            idx += 1
            line_idx = 0
        end -= 1
    if line_idx != 0:
        out[idx] = "\n"
        idx += 1
    sys.stdout.write(buffer(out, 0, idx))

def main():
    start = sys.stdin.tell()
    sys.stdin.seek(0, 2)
    data_len = sys.stdin.tell() - start
    sys.stdin.seek(start)

    data = array.array("c", ["\x00"]) * data_len
    idx = 0
    print sys.stdin.next(),
    for line in sys.stdin:
        if line[0] == '>' or line[0] == ";":
            show(data, idx - 1)
            print line,
            idx = 0
        else:
            i = 0
            while i < len(line) - 1:
                data[idx] = line[i]
                idx += 1
                i += 1
    show(data, idx - 1)

if __name__ == "__main__":
    main()