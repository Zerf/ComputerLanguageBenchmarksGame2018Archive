-- The Great Computer Language Shootout
-- http://shootout.alioth.debian.org
--
-- Contributed by Jim Rogers
-- Fixed by Pascal Obry on 2005/03/21
-- Modified Experimental version suggested by Pascal Obry
-- Modified (using Complex) by Gautier de Montmollin

with Ada.Command_Line; use Ada.Command_Line;
with Ada.Numerics.Generic_Complex_Types;

with Interfaces;       use Interfaces;
with GNAT.IO;          use GNAT.IO;

procedure Mandelbrot is
   type Real is digits 15;
   package R_Complex is new Ada.Numerics.Generic_Complex_Types(Real);
   use R_Complex;
   Iter                   : constant := 50;
   Limit                  : constant := 4.0;
   Size                   : Positive;
   Bit_Num                : Natural    := 0;
   Byte_Acc               : Unsigned_8 := 0;
   Z, Z2, C, Zero         : Complex;
   Two_on_Size            : Real;
begin
   Size := Positive'Value (Argument (1));
   Two_on_Size:= 2.0 / Real (Size);
   Zero:= (0.0, 0.0);

   Put_Line ("P4");
   Put_Line (Argument (1) & " " & Argument (1));

   for Y in 0 .. Size - 1 loop
      for X in 0 .. Size - 1 loop
         Z := Zero;
         C := Two_on_Size * (Real (X), Real (Y)) - (1.5, 1.0);

         for I in 1 .. Iter + 1 loop
            Z2 := (Z.re ** 2, Z.im ** 2);
            Z  := (Z2.re - Z2.im, 2.0 * Z.re * Z.im) + C;
            exit when Z2.re + Z2.im > Limit;
         end loop;

         if Z2.re + Z2.im > Limit then
            Byte_Acc := Shift_Left (Byte_Acc, 1) or 16#00#;
         else
            Byte_Acc := Shift_Left (Byte_Acc, 1) or 16#01#;
         end if;

         Bit_Num := Bit_Num + 1;

         if Bit_Num = 8 then
            Put (Character'Val (Byte_Acc));
            Byte_Acc := 0;
            Bit_Num  := 0;
         elsif X = Size - 1 then
            Byte_Acc := Shift_Left (Byte_Acc, 8 - (Size mod 8));
            Put (Character'Val (Byte_Acc));
            Byte_Acc := 0;
            Bit_Num  := 0;
         end if;
      end loop;
   end loop;
end Mandelbrot;
