/* The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 * original contributed by Kenneth Jonsson
 * small optimizations by Mark Hammons
 */

import scala.actors.Futures._

class RowCalc(size: Int) {
  val maxIterations = 50
  val limitSquared = 4.0
  val bytesPerRow = (size + 7) >> 3
  val cib = new Array[Double](size + 7)
  val crb = new Array[Double](size + 7)

  @inline private def instantiateArrays(i: Int, c: Double) {
    if (i < size + 7) {
      cib(i) = i*c-1.0
      crb(i) = i*c-1.5
      instantiateArrays(i+1,c)
    } else return
  }

  instantiateArrays(0, 2.0/size)

  @inline final def square(d: Double) = d*d

  final def calcRow(rowNum: Int): Array[Byte] = {
    val rowBitmap = new Array[Byte](bytesPerRow)

    val ci = cib(rowNum)

    @inline def outerLoop(column: Int) {
      if (column < size) {
        val cr = crb(column)

        @inline def innerLoop(i:Double = ci, r: Double = cr, iterations: Int) {
          val sr = square(r)
          val si = square(i)
          if(sr + si > limitSquared)
            return
          else if(iterations >= maxIterations) {
            rowBitmap(column >> 3) = (rowBitmap(column >> 3)
                      | (0x80 >> (column & 7))).toByte
          } else
            innerLoop(2.0 * r * i + ci, sr - si + cr, iterations + 1)
        }

        innerLoop(iterations = 1)
        outerLoop(column + 1)
      } else return
    }

    outerLoop(column = 0)

    rowBitmap
  }
}


object Mandelbrot {
  val numProcessors = Runtime.getRuntime.availableProcessors

  def main(args: Array[String]) {
    val size = args(0).toInt
    val calcTo = size/2 + 1
    val rowCalc = new RowCalc(size)
    val bitmap = new Array[Array[Byte]](calcTo)

    def futureHelper(i: Int) {
      if (i < calcTo) {
        val bytes = rowCalc.calcRow(i)
        bitmap(i) = bytes
        futureHelper(i+numProcessors)
      } else return
    }

    val futures = (for (i <- 0 until numProcessors) yield
      future{futureHelper(i)}
    )

    println("P4\n" + size + " " + size)

    futures.foreach(_())
    (bitmap ++ (if(size % 2 == 0) bitmap.tail.reverse.tail
      else bitmap.tail.reverse)).foreach(row => System.out.write(row))
  }
}