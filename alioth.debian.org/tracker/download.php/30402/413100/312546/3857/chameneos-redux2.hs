{-# OPTIONS -funbox-strict-fields #-}
{-# LANGUAGE GeneralizedNewtypeDeriving, BangPatterns, CPP #-}
{- The Computer Language Benchmarks Game
   http://shootout.alioth.debian.org/
   Written by Tom Pledger, 13 Nov 2006. modified by Don Stewart
   Updated for chameneos-redux by Spencer Janssen, 27 Nov 2007
   Modified by Péter Diviánszky, 19 May 2010
   Modified by Louis Wasserman, 6 June 2010 
   
   Should be compiled with -O2 -fasm and run without any RTS options.
   (It actually runs faster if it isn't compiled with -threaded.) -}

import Control.Concurrent
import Control.Monad
import Data.Char
import Data.IORef
import System.Environment
import System.IO
import GHC.Conc
import Foreign hiding (complement)

default(Int)

-- data Colour = Blue | Red | Yellow deriving (Show, Eq, Enum)
newtype Color = C {unC :: Int} deriving (Storable)

#define Y (C 0)
#define R (C 1)
#define B (C 2)

instance Show Color where
	show Y = "yellow"
	show R = "red"
	show B = "blue"


complement :: Color -> Color -> Color
complement !a !b = case a of
    B -> case b of R -> Y; B -> B; _ -> R
    R -> case b of B -> Y; R -> R; _ -> B
    Y -> case b of B -> R; Y -> Y; _ -> B

type Chameneous = Ptr Int
data MP = Nobody !Int | Somebody !Int !Chameneous !(MVar Chameneous)

arrive :: Ptr Int -> MVar MP -> MVar (Int, Int) -> Chameneous -> IO ()
arrive !ct !mpv !finish !ch = do
--     ch <- newIORef c0
    let getCol = fmap C . peek
    let putCol c (C x) = poke c x
    waker <- newEmptyMVar
    let inc x = (fromEnum (ch == x) +)
        go !t !b = do
            w <- takeMVar mpv
            !q <- peek ct
            case w of
                Nobody 0
                 -> do
                    putMVar mpv w
                    putMVar finish (t, b)
  		Nobody q -> {-# SCC "nobody" #-} do 
                    putMVar mpv $ Somebody q ch waker
                    ch' <- takeMVar waker
                    go (t+1) $ inc ch' b

                Somebody q ch' waker' -> {-# SCC "somebody" #-} do
                    c  <- getCol ch
                    c' <- getCol ch'
                    let !c'' = complement c c'
                    putCol ch  c''
                    putCol ch' c''
                    putMVar waker' ch
--                     !q <- peek ct
                    let !q' = q-1
                    poke ct q'
                    putMVar mpv $ Nobody q'
                    go (t+1) $ inc ch' b
    go 0 0

showN = unwords . map ((digits !!) . digitToInt) . show

digits = words "zero one two three four five six seven eight nine"

run :: Int -> [Color] -> IO ()
run n cs = do
    putStrLn . map toLower . unwords . ([]:) . map show $ cs
    fs    <- replicateM (length cs) newEmptyMVar
    mpv   <- newMVar (Nobody n)
    withArrayLen (map unC cs) $ \ n cols -> alloca $ \ ctr -> do
    	poke ctr n
    	zipWithM_ ((forkOnIO 0 .) . arrive ctr mpv) fs (take n (iterate (`advancePtr` 1) cols))
    	ns    <- mapM takeMVar fs
	
    	putStr . map toLower . unlines $ [unwords [show n, showN b] | (n, b) <- ns]
    	putStrLn . (" "++) . showN . sum . map fst $ ns
	putStrLn ""

main = do
    putStrLn . map toLower . unlines $
        [unwords [show a, "+", show b, "->", show $ complement a b]
            | a <- [B, R, Y], b <- [B, R, Y]]

    n <- readIO . head =<< getArgs
    run n [B, R, Y]
    run n [B, R, Y, R, Y, B, R, Y, R, B]