<? /* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/

   contributed by Isaac Gouy
   modified by Maxim Matyukhin

   php -q pidigits.php 27
*/
if (!function_exists('gmp_add') && !dl('gmp.so')) {
    exit("No GMP\n");
}

class Transformation {
   var $q, $r, $s, $t, $k;

   function Transformation($q, $r, $s, $t){
      $this->q = $q;
      $this->r = $r;
      $this->s = $s;
      $this->t = $t;
      $this->k = 0;
   }

   function Unity(){
      return new Transformation(gmp_init(1), gmp_init(0), gmp_init(0), gmp_init(1));
   }

   function Zero(){
      return new Transformation(gmp_init(0), gmp_init(0), gmp_init(0), gmp_init(0));
   }

   function Compose($a){
      $qq = gmp_mul($this->q, $a->q);
      $qrrt = gmp_add(gmp_mul($this->q, $a->r), gmp_mul($this->r, $a->t));
      $sqts = gmp_add(gmp_mul($this->s, $a->q), gmp_mul($this->t, $a->s));
      $srtt = gmp_add(gmp_mul($this->s, $a->r), gmp_mul($this->t, $a->t));
      return new Transformation($qq, $qrrt, $sqts, $srtt);
   }

   function Extract($j){
      $qjr = gmp_add(gmp_mul($this->q, $j), $this->r);
      $sjt = gmp_add(gmp_mul($this->s, $j), $this->t);
      $d = gmp_div($qjr, $sjt);
      return gmp_intval($d);
   }

   function Next(){
      $this->k++;
      $this->q = gmp_init($this->k);
      $this->r = gmp_init(4*$this->k + 2);
      $this->s = gmp_init(0);
      $this->t = gmp_init(2*$this->k + 1);
      $o = $this;
      return $o;
   }
}

class PiDigitStream {
   var $z, $x, $inverse;

   function PiDigitStream(){
      $this->z = Transformation::Unity();
      $this->x = Transformation::Zero();
      $this->inverse = Transformation::Zero();
      $this->gmp_prepared = array(
        0 => gmp_init(0),
        1 => gmp_init(1),
        3 => gmp_init(3),
        4 => gmp_init(4),
        10=> gmp_init(10),
      );
      $this->three_gmp = gmp_init(3);
      $this->four_gmp = gmp_init(4);
   }

   function Produce($j){
      $i = $this->inverse;
      $i->q = $this->gmp_prepared[10];
      $i->r = gmp_init(-10*$j);
      $i->s = $this->gmp_prepared[0];
      $i->t = $this->gmp_prepared[1];
      return $i->Compose($this->z);
   }

   function Consume($a){
      return $this->z ->Compose($a);
   }

   function Digit(){
      return $this->z ->Extract($this->gmp_prepared[3]);
   }

   function IsSafe($j){
      return $j == ($this->z ->Extract($this->gmp_prepared[4]));
   }

   function Next(){
      $y = $this->Digit();
      if ($this->IsSafe($y)){
         $this->z = $this->Produce($y);
         return $y;
      } else {
         $this->z = $this->Consume($this->x ->Next());
         return $this->Next();
      }
   }
}

$n = $argv[1];
$i = 0;
$length = 10;
$pidigit = new PiDigitStream;

while ($n > 0){
   if ($n < $length){
      for ($j=0; $j<$n; $j++) printf("%d",$pidigit->Next());
      print str_repeat(" ", $length - $n);
      $i += $n;
   } else {
      for ($j=0; $j<$length; $j++) printf("%d",$pidigit->Next());
      $i += $length;
   }
   print "\t:$i\n";
   $n -= $length;
}
