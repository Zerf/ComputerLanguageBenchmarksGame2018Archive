with Ada.Text_IO;
with Ada.Command_Line; 

-- The Computer Language Benchmarks Game
-- http://shootout.alioth.debian.org/
-- 
-- contributed by Graham Stark (graham.stark@virtual-worlds.biz)
-- modified by [??]
-- 
-- gcc -c -gnatwu -O2 -gnatn -funroll-loops -gnatp fannkuch.adb
-- gnatmake -gnatp -O3 -fomit-frame-pointer -lpthread -funroll-loops  -f src/fannkuch.adb -o bin/fannkuch
--
-- see: http://shootout.alioth.debian.org/u32/benchmark.php?test=fannkuch&lang=all
--
procedure Fannkuch_Tasking is

   use Ada.Text_IO;
   MAX_TO_PRINT : constant := 30;
   DEBUG : constant Boolean := True;
   
   task type Fannkuch_Worker( num_elements : Positive ) is
      entry Start( worker_number : Positive );
      entry Stop( max_flips : out Positive );
   end Fannkuch_Worker;
   
   task body Fannkuch_Worker is
   
      subtype I_Range is Positive range 1 .. num_elements;
      type Int_Array is array( I_Range ) of I_Range;
      
      max_num_flips : Positive := 1;
      num_fannkuchs : Natural := 0;
      
      wno : Positive;
      
      procedure Swap( a : in out Int_Array; p1 : I_Range; p2 : I_Range ) is
         tmp : I_Range;
      begin
         tmp := a( p1 );
         a( p1 ) := a( p2 );
         a( p2 ) := tmp;
      end Swap;
      
      procedure Reverse_Array( a : in out Int_Array; end_pos : I_Range ) is
         b : Int_Array := a;
      begin
         for p in 1 .. end_pos loop
            a( p ) := b( end_pos - p + 1 );
         end loop;
      end Reverse_Array;
         
      procedure Print( a : in Int_Array ) is
      begin
         for i in a'Range loop
            Put( I_Range'Image( a(i) ));
         end loop;
         New_Line;
      end Print;

      procedure Do_One_Fannkuch( a : in out Int_Array; num_flips : out Natural ) is
      begin
         num_flips := 0; 
         loop
            exit when( a( 1 ) = 1 );
            Reverse_Array( a, a( 1 ));
            num_flips := num_flips + 1;
         end loop;
         if( num_flips > max_num_flips ) then 
            max_num_flips := num_flips; 
         end if; 
         num_fannkuchs := num_fannkuchs + 1;
         if( wno = 1 ) and ( num_fannkuchs < MAX_TO_PRINT )then
            Print( a );
         end if;
      end Do_One_Fannkuch;
      
      procedure Permute( a : in Int_Array; start_pos : I_Range ) is
         local_a : Int_Array := a;
         num_flips : Natural;
      begin
         if( start_pos = local_a'Last ) then
            Do_One_Fannkuch( local_a, num_flips );
         else
            for p in start_pos .. local_a'Last loop
               Swap( local_a, start_pos, p );
               Permute( local_a, start_pos + 1 );
            end loop;
         end if;
      end Permute;

      a : Int_Array;
   begin
      accept Start( worker_number : Positive ) do
         if( DEBUG ) then
            Put_Line( "Worker " & Positive'Image( worker_number ) & "STARTS" );
         end if;
         wno := worker_number;
      end Start;
      a( 1 ) := wno;
      for i in 2 .. a'Last loop
         if( i <= wno ) then
            a( i ) := i-1;
         else
            a( i ) := i;
         end if;
      end loop;
      Permute( a, 2 ); -- start at position 2 since each worker begins with 
                       -- an ascending digit
      accept Stop( max_flips : out Positive ) do
         max_flips := max_num_flips;    
         if( DEBUG ) then
            Put_Line( "Worker " & Positive'Image( wno ) & "Stops; Max Flips"
                      &  Positive'Image( max_num_flips ));
         end if;
      end Stop;
   end Fannkuch_Worker;
   
procedure Run( size : Positive ) is
   --
   -- one worker per digit. We permute starting with the 2nd digit
   -- (so all the 1s stop right away)
   -- 
   type Workers_Array is array( 1 .. SIZE ) of Fannkuch_Worker( 
      num_elements => size ); 
   t1 : Workers_Array;
   
   max_num_flips : Natural := 0;
   mf : Natural := 0;
begin
   for p in Workers_Array'Range loop
      t1( p ).Start( p );
   end loop;
   for p in Workers_Array'Range loop
      t1( p ).Stop( mf );
      if( mf > max_num_flips ) then 
         max_num_flips := mf; 
      end if; 
   end loop;
   Put_Line( "All Stopped; Max Flips(" & Positive'Image( size ) & " ) = " &
             Positive'Image( max_num_flips ));
end Run;

use Ada.Command_Line;
   size : Natural := 7;
begin
   
   if( Argument_Count > 0 ) then
      size := Natural'Value( Argument( 1 ));
   end if;   
   Run( size );
   
end Fannkuch_Tasking;
