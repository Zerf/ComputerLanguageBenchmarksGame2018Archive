"""
The Computer Language Benchmarks Game
http://shootout.alioth.debian.org/

contributed by Kristian Klette <klette@klette.us>

"""
from math import sqrt
from sys import argv

def calc(data):
    s = len(data)
    t = [sum([ (1.0 / ((x+o) * ((x+o) + 1) / 2 + x + 1)) * x_o for o, x_o in enumerate(data)]) for x in range(s)]
    return [sum([ (1.0 / ((i+j) * ((i+j) + 1) / 2 + j + 1)) * u_j for j, u_j in enumerate(t)]) for i in range(s)]


n = int(argv[1])
u = [1] * n

for _ in range(10):
    v = calc(u)
    u = calc(v)

vBv = vv = 0
for ue, ve in zip(u, v):
    vBv += ue * ve
    vv  += ve * ve

print("%0.9f" % (sqrt(vBv/vv)))

