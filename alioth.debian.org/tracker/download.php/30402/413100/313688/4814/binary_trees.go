/* The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 *
 * contributed by The Go Authors.
 * based on C program by Kevin Carson
 * flag.Arg hack by Isaac Gouy
 * goroutines by Atom
 * NodeBatchAlloc by Jan Pfeifer
 */
package main

import (
	"flag"
	"fmt"
	"runtime"
	"strconv"
)

const LOG2_N_CPU = 2
const N_CPU = (1 << LOG2_N_CPU)

type Node struct {
	item        int
	left, right *Node
}

// Minimizes contention on memory manager by providing
// one batch allocator per goroutine (thread).
const BATCH_SIZE = 2048

type NodeBatchAlloc struct {
	batch *[BATCH_SIZE]Node
	used  int
}

func NewNodeBatchAlloc() (me *NodeBatchAlloc) {
	me = new(NodeBatchAlloc)
	me.batchAlloc()
	return
}

func (me *NodeBatchAlloc) batchAlloc() {
	me.batch = new([BATCH_SIZE]Node)
	me.used = 0
}

func (me *NodeBatchAlloc) alloc() (node *Node) {
	if me.used >= BATCH_SIZE {
		me.batchAlloc()
	}
	node = &(*me.batch)[me.used]
	me.used += 1
	return
}

func (me *NodeBatchAlloc) bottomUpTree(item, depth int) (node *Node) {
	node = me.alloc()
	node.item = item
	if depth <= 0 {
		node.left = nil
		node.right = nil
	} else {
		node.left = me.bottomUpTree(2*item-1, depth-1)
		node.right = me.bottomUpTree(2*item, depth-1)
	}
	return
}

func (n *Node) itemCheck() int {
	if n.left == nil {
		return n.item
	}
	return n.item + n.left.itemCheck() - n.right.itemCheck()
}

// Parallel build tree.
func goBottomUpTree(item, depth, goDepth int) (node *Node) {
	node = &Node{item: item}
	if depth <= 0 {
		return
	} else if goDepth > 0 {
		leftChan := make(chan *Node)
		rightChan := make(chan *Node)
		go func() {
			leftChan <- goBottomUpTree(2*item-1, depth-1, goDepth-1)
		}()
		go func() {
			rightChan <- goBottomUpTree(2*item, depth-1, goDepth-1)
		}()
		node.left, node.right = <-leftChan, <-rightChan
	} else {
		batchAlloc := NewNodeBatchAlloc()
		node.left = batchAlloc.bottomUpTree(2*item-1, depth-1)
		node.right = batchAlloc.bottomUpTree(2*item, depth-1)
	}
	return
}

// Parallel tree item check.
func (n *Node) goItemCheck(goDepth int) int {
	if n.left == nil {
		return n.item
	} else if goDepth <= 0 {
		return n.item + n.left.itemCheck() - n.right.itemCheck()
	}
	check := make(chan int)
	go func() { check <- n.left.goItemCheck(goDepth - 1) }()
	go func() { check <- n.right.goItemCheck(goDepth - 1) }()
	return n.item + <-check + <-check
}

const minDepth = 4

func main() {
	runtime.GOMAXPROCS(N_CPU)

	n := 0
	flag.Parse()
	if flag.NArg() > 0 {
		n, _ = strconv.Atoi(flag.Arg(0))
	}

	maxDepth := n
	if minDepth+2 > n {
		maxDepth = minDepth + 2
	}
	stretchDepth := maxDepth + 1

	{
		check := goBottomUpTree(0, stretchDepth, LOG2_N_CPU).itemCheck()
		fmt.Printf("stretch tree of depth %d\t check: %d\n", stretchDepth, check)
	}

	longLivedTree := goBottomUpTree(0, maxDepth, LOG2_N_CPU)

	outputs := make(map[int]chan string)
	control := make(chan byte, N_CPU) // This 'control' also puts a cap on memory usage
	for _depth := minDepth; _depth <= maxDepth; _depth += 2 {
		outputs[_depth] = make(chan string, 1)
		go func(depth int) {
			control <- 0

			iterations := 1 << uint(maxDepth-depth+minDepth)
			check := 0

			batchNodes := NewNodeBatchAlloc()
			for i := 1; i <= iterations; i++ {
				check += batchNodes.bottomUpTree(i, depth).itemCheck()
				check += batchNodes.bottomUpTree(-i, depth).itemCheck()
			}
			outputs[depth] <- fmt.Sprintf("%d\t trees of depth %d\t check: %d\n",
				iterations*2, depth, check)

			<-control
		}(_depth)
	}
	for depth := minDepth; depth <= maxDepth; depth += 2 {
		fmt.Print(<-outputs[depth])
	}

	fmt.Printf("long lived tree of depth %d\t check: %d\n",
		maxDepth, longLivedTree.itemCheck())
}
