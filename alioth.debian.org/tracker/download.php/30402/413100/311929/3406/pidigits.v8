// The Great Computer Language Shootout
//  http://shootout.alioth.debian.org
//
//  Contributed by Matthew Wilson (see below re: BigInteger)
//    Based on the Perl #3 implementation which was
//      contributed by Robert Bradshaw
//      modified by Ruud H.G.van Tol
//      modified by Emanuele Zeppieri
//      modified to use Math:GMP by Kuang-che Wu

var compareTo, multiply, divide, addTo, add;

function main($n) {
  var $i=1, $s="", $d, two=nbv(2), four=nbv(4), neg10=nbv(-10),
    three=nbv(3), one=nbv(1), ten=nbv(10), digits=Array(10),
    $z0=nbv(1), $z1=nbv(0), $z2=nbv(1), negdigits=Array(10),
    g = 1, $g, k = 0, $k, l = 2, $l, a;
  
  for(var i=0; i<10; ++i) { digits[i] = nbv(i) }
  for(var i=0; i<10; ++i) { negdigits[i] = multiply(digits[i],neg10) }
  
  do {
    while ( compareTo($z0,$z2) > 0
         || (($d = +(a = divide(add(multiply($z0,three),$z1),$z2))),
             !(compareTo(a,divide(add($z0.shiftLeft(two),$z1),$z2))==0))
    ) {
      $z1 = multiply($z1,$g = nbv(g+=2));
      $z2 = multiply($z2,$g);
      addTo($z1, multiply($z0,$l = nbv(l+=4)), $z1);
      $z0 = multiply($z0,$k = nbv(++k));
    }
    $z0 = multiply($z0,ten);
    $z1 = multiply($z1,ten);
    addTo($z1, multiply($z2,negdigits[$d]), $z1);
    $s += $d;
    
    if ($i % 10 == 0) { print($s+"\t:"+$i); $s="" }
  } while (++$i <= $n)
  
  if (($i = $n % 10) != 0) { $s += Array(11-$i).join(' ') }
  if ($s.length > 0) { print($s+"\t:"+$n) }
}

var functions=(function(){

//   BigInteger library
// Copyright (c) 2005  Tom Wu
// All Rights Reserved.
// See "LICENSE" for details.

/* Ed. note: "LICENSE" is a BSD-style license 
    see http://www-cs-students.stanford.edu/~tjw/jsbn/
   Modified by Matthew Wilson to extend Array, so that all obj[whole number]
    lookups are numbers indexed to the Array, not hashset members.
   Then I got another large speedup from mostly removing the OO-ness.
*/

// Basic JavaScript BN library - subset useful for RSA encryption.

// Bits per digit
var dbits;

// JavaScript engine analysis
var canary = 0xdeadbeefcafe;
var j_lm = ((canary&0xffffff)==0xefcafe);

var amp=Array.prototype;

// (public) Constructor
function newBigInteger(a,b,c) {
  var This = [];
  if(a != null)
    if("number" == typeof a) This.fromNumber(a,b,c);
    else if(b == null && "string" != typeof a) This.fromString(a,256);
    else This.fromString(a,b);
  return This;
}

// am: Compute w_j += (x*this_i), propagate carries,
// c is initial carry, returns final carry.
// c < 3*dvalue, x < 2*dvalue, this_i < dvalue
// Set max digit bits to 28 since some
// browsers slow down when dealing with 32-bit numbers.
// including V8
function am(This,i,x,w,j,c,n) {
  var xl = x&0x3fff, xh = x>>14, z, l, h, m;
  if(--n >= 0) for(;;) {
    l =(((m=xh*(l=(z=This[i])&0x3fff)+xl*(h=z>>14))&0x3fff)<<14)+xl*l+w[j]+c;
    w[j++] = l&0xfffffff; ++i;
    if (--n >= 0) { c = (l>>28)+(m>>14)+xh*h }
    else { return (l>>28)+(m>>14)+xh*h }
  }
  return c;
}

dbits = 28;

var BI_FP = 52, DB = dbits, DM = ((1<<dbits)-1), DV = (1<<dbits),
  FV = Math.pow(2,BI_FP), F1 = BI_FP-dbits, F2 = 2*dbits-BI_FP;

// Digit conversions
var BI_RM = "0123456789abcdefghijklmnopqrstuvwxyz";
var BI_RC = new Array();
var rr,vv;
rr = "0".charCodeAt(0);
for(vv = 0; vv <= 9; ++vv) BI_RC[rr++] = vv;
rr = "a".charCodeAt(0);
for(vv = 10; vv < 36; ++vv) BI_RC[rr++] = vv;
rr = "A".charCodeAt(0);
for(vv = 10; vv < 36; ++vv) BI_RC[rr++] = vv;

function int2char(n) { return BI_RM.charAt(n); }
function intAt(s,i) {
  return BI_RC[s.charCodeAt(i)];
  //var c = BI_RC[s.charCodeAt(i)];
  //return (c==null)?-1:c; // dead code
}

// (protected) copy this to r
function bnpCopyTo(r) {
  for(var i = this.t-1; i >= 0; --i) r[i] = this[i];
  r.t = this.t;
  r.s = this.s;
}

// (protected) set from integer value x, -DV <= x < DV
function bnpFromInt(x) {
  this.t = 1;
  this.s = (x<0)?-1:0;
  if(x > 0) this[0] = x;
  else if(x < -1) this[0] = x+DV;
  else this.t = 0;
}

// return bigint initialized to value
function nbv(i) { var r = []; r.fromInt(i); return r; }

// (protected) clamp off excess high words
function bnpClamp() {
  var c = this.s&DM, t = this.t;
  while(t > 0 && this[t-1] == c) { --t; }
  this.t = t;
}

// (public) return string representation in given radix
function bnToString(b) {
  if(this.s < 0) return "-"+this.negate().toString(b);
  var k;
  if(b == 16) k = 4;
  else if(b == 8) k = 3;
  else if(b == 2) k = 1;
  else if(b == 32) k = 5;
  else if(b == 4) k = 2;
  else return this.toRadix(b);
  var km = (1<<k)-1, d, m = false, r = "", i = this.t;
  var p = DB-(i*DB)%k;
  if(i-- > 0) {
    if(p < DB && (d = this[i]>>p) > 0) { m = true; r = int2char(d); }
    while(i >= 0) {
      if(p < k) {
        d = (this[i]&((1<<p)-1))<<(k-p);
        d |= this[--i]>>(p+=DB-k);
      }
      else {
        d = (this[i]>>(p-=k))&km;
        if(p <= 0) { p += DB; --i; }
      }
      if(d > 0) m = true;
      if(m) r += int2char(d);
    }
  }
  return m?r:"0";
}

// (public) -this
function bnNegate() { var r = []; bnpSubTo(ZERO,this,r); return r; }

// (public) |this|
function bnAbs() { return (this.s<0)?this.negate():this; }

// (public) return + if this > a, - if this < a, 0 if equal
function bnCompareTo(o,a) {
  var r = o.s-a.s;
  if(r != 0) return r;
  var i = o.t;
  r = i-a.t;
  if(r != 0) return r;
  while(--i >= 0) if((r=o[i]-a[i]) != 0) return r;
  return 0;
}

// returns bit length of the integer x
function nbits(x) {
  var r = 1, t;
  if((t=x>>>16) != 0) { x = t; r += 16; }
  if((t=x>>8) != 0) { x = t; r += 8; }
  if((t=x>>4) != 0) { x = t; r += 4; }
  if((t=x>>2) != 0) { x = t; r += 2; }
  if((t=x>>1) != 0) { x = t; r += 1; }
  return r;
}
/*
// (public) return the number of bits in "this"
function bnBitLength() {
  if(this.t <= 0) return 0;
  return DB*(this.t-1)+nbits(this[this.t-1]^(this.s&DM));
}
*/
// (protected) r = this << n*DB
function bnpDLShift(o,n) {
  var i, t,r=Array((t=o.t)+n);
  for(i = t-1; i >= 0; --i) r[i+n] = o[i];
  for(i = n-1; i >= 0; --i) r[i] = 0;
  r.t = t+n;
  r.s = o.s;
  return r;
}

// (protected) r = this >> n*DB
function bnpDRShiftTo(o,n,r) {
  for(var i = n; i < o.t; ++i) r[i-n] = o[i];
  r.t = Math.max(o.t-n,0);
  r.s = o.s;
}

// (protected) r = this << n
function bnpLShiftTo(o,n,r) {
  var bs = n%DB, cbs = DB-bs, bm = (1<<cbs)-1,
    ds = Math.floor(n/DB), c = ((s=o.s)<<bs)&DM, i, z, t, s;
  for(i = (t=o.t)-1; i >= 0; --i) {
    r[i+ds+1] = ((z=o[i])>>cbs)|c;
    c = (z&bm)<<bs;
  }
  for(i = ds-1; i >= 0; --i) r[i] = 0;
  r[ds] = c;
  r.t = t+ds+1;
  r.s = s;
  r.clamp();
}

// (protected) r = this >> n
function bnpRShiftTo(o,n,r) {
  r.s = o.s;
  var ds = Math.floor(n/DB), t=o.t, z;
  if(ds >= t) { r.t = 0; return; }
  var bs = n%DB, cbs = DB-bs, bm = (1<<bs)-1;
  r[0] = o[ds]>>bs;
  for(var i = ds+1; i < t; ++i) {
    r[i-ds-1] |= ((z=o[i])&bm)<<cbs;
    r[i-ds] = z>>bs;
  }
  if(bs > 0) r[t-ds-1] |= (o.s&bm)<<cbs;
  r.t = t-ds;
  r.clamp();
}

// (protected) r = this - a
function bnpSubTo(o,a,r) {
  var i = 0, c = 0, t, m = Math.min(a.t,t=o.t);
  while(i < m) {
    c += o[i]-a[i];
    r[i++] = c&DM;
    c >>= DB;
  }
  if(a.t < t) {
    c -= a.s;
    while(i < t) {
      c += o[i];
      r[i++] = c&DM;
      c >>= DB;
    }
    c += o.s;
  }
  else {
    c += o.s;
    while(i < a.t) {
      c -= a[i];
      r[i++] = c&DM;
      c >>= DB;
    }
    c -= a.s;
  }
  r.s = (c<0)?-1:0;
  if(c < -1) r[i++] = DV+c;
  else if(c > 0) r[i++] = c;
  r.t = i;
  r.clamp();
}

// (protected) r = this * a, r != this,a (HAC 14.12)
// "this" should be the larger one if appropriate.
function bnpMultiplyTo(o,a,r) {
  var x = o.abs(), y = a.abs();
  var i = x.t;
  r.t = i+y.t;
  while(--i >= 0) r[i] = 0;
  for(i = 0; i < y.t; ++i) r[i+x.t] = am(x,0,y[i],r,i,0,x.t);
  r.s = 0;
  r.clamp();
  if(o.s != a.s) bnpSubTo(ZERO,r,r);
}
// (protected) divide this by m, quotient and remainder to q, r (HAC 14.20)
// r != q, this != m.  q or r may be null.
function bnpDivRemTo(o,m,q,r) {
  var pm = m.abs();
  if(pm.t <= 0) return;
  var pt = o.abs();
  if(pt.t < pm.t) {
    //if(q != null)
    q.fromInt(0);
    if(r != null) o.copyTo(r);
    return;
  }
  if(r == null) r = [];
  var y = [], ts = o.s, ms = m.s;
  var nsh = DB-nbits(pm[pm.t-1]);	// normalize modulus
  if(nsh > 0) { bnpLShiftTo(pm,nsh,y); bnpLShiftTo(pt,nsh,r); }
  else { pm.copyTo(y); pt.copyTo(r); }
  var ys = y.t;
  var y0 = y[ys-1];
  if(y0 == 0) return;
  var yt = y0*(1<<F1)+((ys>1)?y[ys-2]>>F2:0);
  var d1 = FV/yt, d2 = (1<<F1)/yt, e = 1<<F2;
  var i = r.t, j = i-ys, t = bnpDLShift(y,j);
  if(bnCompareTo(r,t) >= 0) {
    r[r.t++] = 1;
    bnpSubTo(r,t,r);
  }
  t = bnpDLShift(ONE,ys);
  bnpSubTo(t,y,y);	// "negative" y so we can replace sub with am later
  while(y.t < ys) y[y.t++] = 0;
  while(--j >= 0) {
    // Estimate quotient digit
    var qd = (r[--i]==y0)?DM:Math.floor(r[i]*d1+(r[i-1]+e)*d2);
    if((r[i]+=am(y,0,qd,r,j,0,ys)) < qd) {	// Try it out
      t = bnpDLShift(y,j);
      bnpSubTo(r,t,r);
      while(r[i] < --qd) bnpSubTo(r,t,r);
    }
  }
  if(q != null) {
    bnpDRShiftTo(r,ys,q);
    if(ts != ms) bnpSubTo(ZERO,q,q);
  }
  r.t = ys;
  r.clamp();
  if(nsh > 0) bnpRShiftTo(r,nsh,r);	// Denormalize remainder
  if(ts < 0) bnpSubTo(ZERO,r,r);
}

// protected
amp.copyTo = bnpCopyTo;
amp.fromInt = bnpFromInt;
amp.clamp = bnpClamp;
amp.toString = bnToString;
amp.negate = bnNegate;
amp.abs = bnAbs;

// "constants"
ZERO = nbv(0);
ONE = nbv(1);
// Copyright (c) 2005  Tom Wu
// All Rights Reserved.
// See "LICENSE" for details.

// Extended JavaScript BN functions, required for RSA private ops.

// (public) return value as integer
function bnIntValue() {
  if(this.s < 0) {
    if(this.t == 1) return this[0]-DV;
    else if(this.t == 0) return -1;
  }
  else if(this.t == 1) return this[0];
  else if(this.t == 0) return 0;
  // assumes 16 < DB < 32
  return ((this[1]&((1<<(32-DB))-1))<<DB)|this[0];
}

// (protected) return x s.t. r^x < DV
function bnpChunkSize(r) { return Math.floor(Math.LN2*DB/Math.log(r)); }

// (public) 0 if this == 0, 1 if this > 0
function bnSigNum() {
  if(this.s < 0) return -1;
  else if(this.t <= 0 || (this.t == 1 && this[0] <= 0)) return 0;
  else return 1;
}

// (protected) convert to radix string
function bnpToRadix(b) {
  if(b == null) b = 10;
  if(this.signum() == 0 || b < 2 || b > 36) return "0";
  var cs = this.chunkSize(b);
  var a = Math.pow(b,cs);
  var d = nbv(a), y = [], z = [], r = "";
  bnpDivRemTo(this,d,y,z);
  while(y.signum() > 0) {
    r = (a+z.intValue()).toString(b).substr(1) + r;
    bnpDivRemTo(y,d,y,z);
  }
  return z.intValue().toString(b) + r;
}

// (public) this << n
function bnShiftLeft(n) {
  var r = [];
  if(n < 0) bnpRShiftTo(this,-n,r); else bnpLShiftTo(this,n,r);
  return r;
}

// (protected) r = this + a
function bnpAddTo(o,a,r) {
  var i = 0, c = 0, m = Math.min(a.t,o.t);
  while(i < m) {
    c += o[i]+a[i];
    r[i++] = c&DM;
    c >>= DB;
  }
  if(a.t < o.t) {
    c += a.s;
    while(i < o.t) {
      c += o[i];
      r[i++] = c&DM;
      c >>= DB;
    }
    c += o.s;
  }
  else {
    c += o.s;
    while(i < a.t) {
      c += a[i];
      r[i++] = c&DM;
      c >>= DB;
    }
    c += a.s;
  }
  r.s = (c<0)?-1:0;
  if(c > 0) r[i++] = c;
  else if(c < -1) r[i++] = DV+c;
  r.t = i;
  r.clamp();
}

// (public) this + a
function bnAdd(a) { var r = []; bnpAddTo(this,a,r); return r; }

// (public) this - a
function bnSubtract(a) { var r = []; bnpSubTo(this,a,r); return r; }

// (public) this * a
function bnMultiply(a) { var r = []; bnpMultiplyTo(this,a,r); return r; }

// (public) this / a
function bnDivide(a) { var r = []; bnpDivRemTo(this,a,r,null); return r; }

// protected
amp.chunkSize = bnpChunkSize;
amp.toRadix = bnpToRadix;

// public
amp.intValue = bnIntValue;
amp.signum = bnSigNum;
amp.shiftLeft = bnShiftLeft;
amp.add = bnAdd;
amp.multiply = bnMultiply;
amp.divide = bnDivide;

return [compareTo = bnCompareTo,
multiply = function multiply(o,a) { var r = []; bnpMultiplyTo(o,a,r);return r},
divide = function divide(o,a) { var r =[]; bnpDivRemTo(o,a,r,null); return r},
addTo = bnpAddTo,
add = function add(o,a) { var r = []; bnpAddTo(o,a,r); return r; },
nbv];
})();
compareTo=functions[0];
multiply=functions[1];
divide=functions[2];
addTo=functions[3];
add=functions[4];
nbv=functions[5];
main.call(this, 1*arguments[0]*1)
