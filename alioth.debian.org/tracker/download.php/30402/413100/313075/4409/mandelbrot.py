# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
#
# contributed by Tupteq
# modified by Joseph LaFata

from array import array
import sys

cout = sys.stdout.write
if sys.version_info >= (3, 0):
    xrange = range
    cout = sys.stdout.buffer.write

def main():
    size = int(sys.argv[1])
    xr_size = xrange(size)
    xr_iter = xrange(50)
    bit = 128
    byte_acc = 0

    cout(("P4\n%d %d\n" % (size, size)).encode('ASCII'))

    result = array('B')
    size = float(size)
    for y in xr_size:
        fy = 2j * y / size - 1j
        for x in xr_size:
            z = 0j
            c = 2. * x / size - 1.5 + fy

            for i in xr_iter:
                z = z * z + c
                if (z.real * z.real + z.imag * z.imag) >= 4.0:
                    break
            else:
                byte_acc += bit

            if bit > 1:
                bit >>= 1
            else:
                result.append(byte_acc)
                bit = 128
                byte_acc = 0

        if bit != 128:
            result.append(byte_acc)
            bit = 128
            byte_acc = 0
            
    cout(result.tostring())


main()