/* The Computer Language Benchmarks Game
	http://shootout.alioth.debian.org/
	
	contributed by: Seth Hoenig
*/


import std.stdio;
import std.string;
import std.conv;

void main(string[] args)
{
	int n = 0;
	int minDepth = 4;
	if(args.length > 1)
		n = parse!(int)(args[1]);
	int maxDepth = (minDepth+2>n) ? minDepth+2 : n;
	int stretchDepth = maxDepth+1;
	int check = (TreeNode.bottomUpTree(0, stretchDepth)).itemCheck();
	writefln("stretch tree of depth %d\t check: %d", stretchDepth, check);
	
	TreeNode longLivedTree = TreeNode.bottomUpTree(0, maxDepth);
	for(int depth = minDepth; depth <= maxDepth; depth+=2)
	{
		int iters = 1 << (maxDepth - depth + minDepth);
		check = 0;
		for(int i=1; i<= iters; i++)
		{
			check += (TreeNode.bottomUpTree(i, depth)).itemCheck();
			check += (TreeNode.bottomUpTree(-i, depth)).itemCheck();
		}
		writefln("%d\t trees of depth %d\t check: %d", (iters*2), depth, check);
	}
	writefln("long lived tree of depth %d\t check: %d", maxDepth, 
													longLivedTree.itemCheck());
}

class TreeNode
{
	TreeNode left;
	TreeNode right;
	int item;
	
	this(int item)
	{
		this.item = item;
	}
	
	this(TreeNode left, TreeNode right, int item)
	{
		this.left = left;
		this.right = right;
		this.item = item;
	}
	
	static TreeNode bottomUpTree(int item, int depth)
	{
		if(depth > 0)
			return new TreeNode(
				bottomUpTree(2*item-1, depth-1),
				bottomUpTree(2*item, depth-1),
				item);
		else
			return new TreeNode(item);
	}
	
	int itemCheck() 
	{
		if(left is null)
			return item;
		else
			return item+left.itemCheck() - right.itemCheck();
	}
}
