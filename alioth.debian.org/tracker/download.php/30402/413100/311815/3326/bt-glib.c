/*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 *
 * Contributed by Eckehard Berns
 * Based on code by Kevin Carson
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <glib.h>
#include <string.h>

typedef struct node
{
   struct node *left, *right;
   long item;
} node;

#define MAX_ELM (8192 / sizeof (node))

static node *
new_node(node *left, node *right, long item, GTrashStack **mp, GPtrArray *roots)
{
   node *ret;

   ret = g_trash_stack_pop(mp);
   if (!ret)
     {
	int i;

	ret = malloc(sizeof (node) * MAX_ELM);

	g_ptr_array_add(roots, ret);
	for (i = 1; i < MAX_ELM; ++i)
	  g_trash_stack_push(mp, ret + i);
     }

   ret->left = left;
   ret->right = right;
   ret->item = item;

   return ret;
}

static long
item_check(node *tree)
{
   if (tree->left == NULL) return tree->item;
   else
     return tree->item + item_check(tree->left) - item_check(tree->right);
}

static node *
bottom_up_tree(long item, int depth, GTrashStack **mp, GPtrArray *roots)
{
   if (depth > 0)
     return new_node(bottom_up_tree(2 * item - 1, depth - 1, mp, roots),
		     bottom_up_tree(2 * item, depth - 1, mp, roots),
		     item, mp, roots);
   else
     return new_node(NULL, NULL, item, mp, roots);
}

static void
delete_tree(node *tree, GTrashStack **mp)
{
   if (tree->left != NULL)
     {
	delete_tree(tree->left, mp);
	delete_tree(tree->right, mp);
     }

   g_trash_stack_push(mp, tree);
}

struct worker_args
{
   long iter, check;
   int depth;

   struct worker_args *next;
};

struct worker_mempool
{
   GTrashStack *mp;
   GPtrArray *roots;
   pthread_t id;
};

static struct worker_args *gargs = NULL;
static struct worker_args *rargs = NULL;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

static void *
check_tree_of_depth(void *mempool)
{
   struct worker_args *args;
   struct worker_args *prev;
   struct worker_args *over;
   struct worker_mempool *wm = mempool;
   long i, iter, check, depth;
   node *tmp;

   while (gargs)
     {
	pthread_mutex_lock(&mutex);

	if (!gargs) break;

	args = gargs;
	gargs = gargs->next;

	pthread_mutex_unlock(&mutex);

	iter = args->iter;
	depth = args->depth;

	check = 0;

	for (i = 1; i <= iter; i++)
	  {
	     tmp = bottom_up_tree(i, depth, &wm->mp, wm->roots);
	     check += item_check(tmp);
	     delete_tree(tmp, &wm->mp);

	     tmp = bottom_up_tree(-i, depth, &wm->mp, wm->roots);
	     check += item_check(tmp);
	     delete_tree(tmp, &wm->mp);
	  }

	args->check = check;

	pthread_mutex_lock(&mutex);

	prev = NULL;
	over = rargs;

	while (over && over->depth < args->depth)
	  {
	     prev = over;
	     over = over->next;
	  }

	if (!prev)
	  {
	     args->next = rargs;
	     rargs = args;
	  }
	else
	  {
	     args->next = over;
	     prev->next = args;
	  }

	pthread_mutex_unlock(&mutex);
     }

   return NULL;
}

static int GetThreadCount()
{
   int count = 0;
   cpu_set_t cs;
   int i;

   CPU_ZERO(&cs);

   sched_getaffinity(0, sizeof(cs), &cs);

   for (i = 0; i < 8; i++)
     {
	if (CPU_ISSET(i, &cs))
	  count++;
     }

   return count;
}

int
main(int ac, char **av)
{
   node *stretch, *longlived;
   struct worker_args *args;
   struct worker_mempool *mps;
   node *tmp;
   int n, depth, mindepth, maxdepth, stretchdepth;
   int id_count, j;

   GTrashStack *array = NULL;
   GPtrArray *roots = g_ptr_array_new();

   n = ac > 1 ? atoi(av[1]) : 10;
   if (n < 1)
     {
	fprintf(stderr, "Wrong argument.\n");
	exit(1);
     }

   mindepth = 4;
   maxdepth = mindepth + 2 > n ? mindepth + 2 : n;
   stretchdepth = maxdepth + 1;

   stretch = bottom_up_tree(0, stretchdepth, &array, roots);
   printf("stretch tree of depth %u\t check: %li\n", stretchdepth,
	  item_check(stretch));

   delete_tree(stretch, &array);

   longlived = bottom_up_tree(0, maxdepth, &array, roots);

   for (depth = mindepth; depth <= maxdepth; depth += 2)
     {
	args = malloc(sizeof(struct worker_args));
	args->iter = 1 << (maxdepth - depth + mindepth);
	args->depth = depth;
	args->next = gargs;

	gargs = args;
     }

   id_count = GetThreadCount();
   mps = alloca(sizeof (struct worker_mempool) * id_count);
   memset(mps, 0, sizeof (struct worker_mempool) * id_count);

   while ((tmp = g_trash_stack_pop(&array)))
     g_trash_stack_push(&mps[0].mp, tmp);

   for (j = 0; j < id_count; ++j)
     {
	mps[j].roots = g_ptr_array_new();
	pthread_create(&mps[j].id, NULL, check_tree_of_depth, &mps[j]);
     }

   for (j = 0; j < id_count; ++j)
     {
	pthread_join(mps[j].id, NULL);

	g_ptr_array_foreach(mps[j].roots, free, NULL);
	g_ptr_array_free(mps[j].roots, TRUE);
     }

   while (rargs)
     {
	args = rargs;

	printf("%ld\t trees of depth %d\t check: %ld\n",
	       args->iter * 2, args->depth, args->check);

	rargs = args->next;
	free(args);
     }

   printf("long lived tree of depth %d\t check: %ld\n", maxdepth,
	  item_check(longlived));

   /* not in original C version: */
   delete_tree(longlived, &array);

   g_ptr_array_foreach(roots, free, NULL);
   g_ptr_array_free(roots, TRUE);

   return 0;
}
