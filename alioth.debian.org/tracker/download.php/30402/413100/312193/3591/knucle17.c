/*
	The Computer Language Benchmarks Game
	http://shootout.alioth.debian.org/

	Based on C contribution of Josh Goldfoot. And modified by Cedric Bail.
	Based on bit encoding idea of C++ contribution of Andrew Moon
	Contributed by The Anh Tran
*/

#define _GNU_SOURCE
#include <omp.h>
#include <sched.h>

#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <stdint.h>

//#define HT_DEBUG
#include "simple_hash.h"


#define BIT_PER_CODE 	2
#define MAX_THREADS 	16
#define HT_SIZE 		(2*1024*1024)


void 
ReadAll(char* *input, size_t *length)
{
	// get input size
	*length = ftell(stdin);
	fseek(stdin, 0, SEEK_END);
	*length = ftell(stdin) - *length;
	fseek(stdin, 0, SEEK_SET);

	*input = (char*)malloc(*length);
	
	// rule: read line-by-line
	char buffer[64];
	while (fgets_unlocked(buffer, sizeof(buffer), stdin))
	{
		if ((buffer[0] == '>') && (strncmp(buffer, ">THREE", 6) == 0))
			break;
	}

	size_t const LL = 60;
	buffer[LL] = 0;
	size_t byte_read = 0;
	
	while (fgets_unlocked(buffer, sizeof(buffer), stdin))
	{
		if (buffer[LL] == '\n')
		{
			memcpy(*input + byte_read, buffer, LL);
			byte_read += LL;
		}
		else
		{
			size_t sz = strlen(buffer);
			if (buffer[sz -1] == '\n')
				--sz;

			memcpy(*input + byte_read, buffer, sz);
			byte_read += sz;
		}

		buffer[LL] = 0;
	}

	*length = byte_read;	
	(*input)[ byte_read ] = 0;
}


typedef struct 
{
	struct ht_ht	*hash_table[MAX_THREADS];
	
	size_t			stride_processed;
	size_t			thread_passed;
	char			str_output[256];
}	Result_t;

Result_t result_list[7] = {0};



uint 
CodeToBit(uint c)
{
	switch (c)
	{
	case 'a':	return 0;
	case 't':	return 1;
	case 'c':	return 2;
	case 'g':	return 3;

	case 'A':	return 0;
	case 'T':	return 1;
	case 'C':	return 2;
	case 'G':	return 3;
	}

	assert(0);
	return 0;
}


char
BitToCode(uint bit)
{
	static
	char const tb[4] = {'A', 'T', 'C', 'G'};

	assert(bit < 4);
	return tb[bit];
}

#define EK_Imp(hash_length)	\
ui64	\
EncodeKey##hash_length(char const* data)	\
{	\
	ui64 key = 0;	\
	size_t index = 0;\
	size_t shift = 0;\
	\
	for (; index < hash_length; ++index	)	\
	{\
		key		|= (ui64)CodeToBit(data[index]) << shift;	\
		shift	+= BIT_PER_CODE;	\
	}\
	\
	return key;\
}

EK_Imp(1); EK_Imp(2); EK_Imp(3); EK_Imp(4); EK_Imp(6); EK_Imp(12); EK_Imp(18);

#define EncodeKey(len, data)	EncodeKey##len(data)


char* 
DecodeKey(char* buffer, size_t frame_size, ui64 key)
{
	size_t index = 0;
	ui64 extract_mask = 3;

	for (; index < frame_size; ++index)
	{
		uint extract_value = ((key & extract_mask) >> (index * BIT_PER_CODE));
		buffer[index] = BitToCode(extract_value);

		extract_mask <<= BIT_PER_CODE;
	}
	
	buffer[index] = 0;
	return buffer;
}

void 
MergeTable(struct ht_ht *des, struct ht_ht* src)
{
	struct ht_node* src_node;

	for (src_node = ht_first(src); src_node != 0; src_node = ht_next(src))
	{
		struct ht_node* des_node = ht_find_new(des, src_node->key);
		des_node->val += src_node->val;
	}
}


#define BHT_Imp(frame_size)\
void 	\
BuildHashTable##frame_size(char const* data, size_t data_length, Result_t *result)\
{\
	struct ht_ht* local_table = ht_create(HT_SIZE);\
	result->hash_table[omp_get_thread_num()] = local_table;\
\
	size_t offset; /* each thread pick up task here */\
	while ((offset = __sync_fetch_and_add(&result->stride_processed, 1)) < frame_size)	\
	{\
		size_t i_begin = offset;	\
		size_t i_end = data_length - frame_size + 1;	\
		\
		ui64 key = 0;	\
		for(; i_begin < i_end; i_begin += frame_size)	\
		{\
			key = EncodeKey(frame_size, data + i_begin);\
			ht_find_new(local_table, key)->val += 1;\
		}\
	}\
}

BHT_Imp(1); BHT_Imp(2); BHT_Imp(3); BHT_Imp(4); BHT_Imp(6); BHT_Imp(12); BHT_Imp(18);

#define BuildHashTable(frame_size, data, data_length, result)	\
		BuildHashTable##frame_size(data, data_length, result)	


typedef struct
{
	ui64	key;
	int		value;
} KVPair;


int
DescCompFunc(void const*p1, void const* p2)
{
	KVPair const* k1 = (KVPair const*)p1;
	KVPair const* k2 = (KVPair const*)p2;
	
	return -(k1->value - k2->value);
}



#define WF_Imp(frame_size) \
void \
WriteFreq##frame_size(char* data, size_t length, Result_t * result) \
{ \
	BuildHashTable(frame_size, data, length, result); \
\
	if (__sync_add_and_fetch(&result->thread_passed, 1) == (size_t)omp_get_num_threads()) \
	{ \
		int i = 1; \
		for (; i < omp_get_num_threads(); ++i) \
		{ \
			MergeTable(result->hash_table[0], result->hash_table[i]); \
			ht_destroy(result->hash_table[i]); \
		} \
\
		struct ht_ht* ht = result->hash_table[0]; \
		uint n_elements = ht_count(ht); \
		KVPair* sorted_list = (KVPair*)calloc(n_elements, sizeof(KVPair)); \
\
		struct ht_node * node; \
		uint index = 0; \
\
		for (node = ht_first(ht); node != 0; node = ht_next(ht), ++index) \
		{\
			sorted_list[index].key		= node->key; \
			sorted_list[index].value	= node->val; \
		} \
\
		n_elements = index;\
		qsort(sorted_list, n_elements, sizeof(KVPair), DescCompFunc);\
\
		char buffer[64];\
		int char_count = 0;\
		float total = length - frame_size +1;\
\
		for (index = 0; index < n_elements; ++index)\
		{\
			buffer[frame_size] = 0;\
\
			char_count += sprintf(result->str_output + char_count,\
				"%s %.3f\n", \
				DecodeKey(buffer, frame_size, sorted_list[index].key), \
				sorted_list[index].value * 100.0f / total);\
		}\
\
		sprintf(result->str_output + char_count, "\n");\
\
		free(sorted_list);\
		ht_destroy(result->hash_table[0]); \
	}\
}

WF_Imp(1);	WF_Imp(2);

#define WriteFreq(frame_size, data, length, result) \
WriteFreq##frame_size(data, length, result)



#define WC_Imp(frame_size) \
void \
WriteCount##frame_size(char* data, size_t length, Result_t* result, char const* pattern) \
{ \
	BuildHashTable(frame_size, data, length, result); \
\
	if (__sync_add_and_fetch(&result->thread_passed, 1) == (size_t)omp_get_num_threads()) \
	{ \
		int i = 1; \
		for (; i < omp_get_num_threads(); ++i) \
		{ \
			MergeTable(result->hash_table[0], result->hash_table[i]); \
			ht_destroy(result->hash_table[i]); \
		} \
\
		ui64 key = EncodeKey(frame_size, pattern); \
		struct ht_node const* node = ht_find(result->hash_table[0], key); \
\
		sprintf(result->str_output, "%d\t%s\n", (node != 0) ? node->val : 0, pattern); \
		ht_destroy(result->hash_table[0]); \
	}\
}

WC_Imp(3); WC_Imp(4); WC_Imp(6); WC_Imp(12); WC_Imp(18);

#define WriteCount(frame_size, data, len, rs, pt) WriteCount##frame_size(data, len, rs, pt)

int 
GetThreadCount()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	int i;
	for (i = 0; i < MAX_THREADS; ++i)
	{
		if (CPU_ISSET(i, &cs))
			++count;
	}
	return count;
}


int 
main()
{
	char*	input;
	size_t	length;
	ReadAll(&input, &length);

	#pragma omp parallel num_threads(GetThreadCount()) default(shared)
	{
		WriteCount(18,	input, length, &result_list[6], "GGTATTTTAATTTATAGT");
		WriteCount(12,	input, length, &result_list[5], "GGTATTTTAATT");
		WriteCount(6,	input, length, &result_list[4], "GGTATT");
		WriteCount(4,	input, length, &result_list[3], "GGTA");
		WriteCount(3,	input, length, &result_list[2], "GGT");
		WriteFreq(2,	input, length, &result_list[1]);
		WriteFreq(1,	input, length, &result_list[0]);
	}

	free(input);
	
	int i;
	for (i = 0; i < 7; ++i)
		printf("%s", result_list[i].str_output);

	return 0;
}

