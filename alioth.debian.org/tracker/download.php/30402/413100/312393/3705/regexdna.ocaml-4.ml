(*
 *  The Computer Language Benchmarks Game
 *  http://shootout.alioth.debian.org/
 *
 *  contributed by Jordan P.
 *
 *)


let fasta_list =
  let rec loop list =
    try 
      let line = input_line stdin in
      let line = line ^ "\n" in
      loop (line :: list)
    with End_of_file -> List.rev list
  in loop []
;;

let seq_length seq =
  List.fold_left (fun num line -> num + (String.length line)) 0 seq
;;


let comment_out flist =
  let not_comment line = 
      let first_char = (String.sub line 0 1) in
      if first_char <> ">" && first_char <> ";" then true else false
  in
  List.filter not_comment  flist
;;

let remove_linefeeds flist = 
  let clean str = Str.replace_first (Str.regexp_string "\n") "" str in
  List.map clean flist
;;

let variants = [
  "agggtaaa\\|tttaccct";    
  "[cgt]gggtaaa\\|tttaccc[acg]";
  "a[act]ggtaaa\\|tttacc[agt]t";  
  "ag[act]gtaaa\\|tttac[agt]ct";
  "agg[act]taaa\\|ttta[agt]cct";
  "aggg[acg]aaa\\|ttt[cgt]ccct";
  "agggt[cgt]aa\\|tt[acg]accct";
  "agggta[cgt]a\\|t[acg]taccct";
  "agggtaa[cgt]\\|[acg]ttaccct"
]

let find_variants flist = 
  let find_matches match_string = 
    let regex = Str.regexp match_string in
    let get_total_matches rx str =
      let rec loop num total =
        try 
          let index = Str.search_forward rx str num in
          loop (index+1) (total + 1)
        with Not_found -> total 
      in loop 0 0
    in
    let rec loop list total =
      match list with 
      [] -> (match_string, total) 
    | (hd :: []) -> 
        let matches = get_total_matches regex hd in
        loop [] (total + matches)
    | (hd1 :: hd2 :: tl) -> 
        let matches = get_total_matches regex (hd1 ^ hd2) in
        let matches = matches - (get_total_matches regex hd1) in
        loop (hd2 :: tl) (total + matches)
    in loop flist 0
  in
  List.map find_matches variants
;;

let print_variants vars = 
  let rec loop vars =
    match vars with
    [] -> ()
  | (hd :: tl) -> 
      match hd with (pat, num) -> 
        let pat = Str.global_replace (Str.regexp_string "\\") "" pat in
        Printf.printf "%s %d\n" pat num;
        loop tl
  in loop vars
;;

let iubs = [
  ("B", "(c|g|t)");
  ("D", "(a|g|t)");
  ("H", "(a|c|t)");
  ("K", "(g|t)");
  ("M", "(a|c)");
  ("N", "(a|c|g|t)");
  ("R", "(a|g)");
  ("S", "(c|g)");
  ("V", "(a|c|g)");
  ("W", "(a|t)");
  ("Y", "(c|t)");
];;

let replace_IUBs flist = 
  let replace iub_pair str =
    match iub_pair with (original, updated) ->
      let regex = Str.regexp original in
      Str.global_replace regex updated str
  in
  let replace_all str =
    let rec loop str pairs =
      match pairs with 
      [] -> str
    | (hd :: tl) ->
        let str = replace hd str in
        loop str tl
    in loop str iubs
  in
  List.map replace_all flist
;;

(* Run the program with input from stdin, output to stdout *)
let _ = 
  let flist = fasta_list in
  let seq_length1 = seq_length flist in
  let flist = comment_out flist in
  let flist = remove_linefeeds flist in
  let seq_length2 = seq_length flist in
  let variants = find_variants flist in
  print_variants variants;
  let flist = replace_IUBs flist in
  let seq_length3 = seq_length flist in

  Printf.printf "\n";
  Printf.printf "%d\n" seq_length1;
  Printf.printf "%d\n" seq_length2;
  Printf.printf "%d" seq_length3;
;;
