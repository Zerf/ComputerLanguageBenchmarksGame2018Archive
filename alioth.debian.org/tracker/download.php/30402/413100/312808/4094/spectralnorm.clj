;; The Computer Language Benchmarks Game
;; http://shootout.alioth.debian.org/
;;
;; contributed by Jesse Rosenstock
;; modified by Jim Kannampuzha

(ns spectralnorm
  (:gen-class))

(set! *warn-on-reflection* true)

(defmacro eval-a [ii jj]
  `(let [i# (int ~ii)
         j# (int ~jj)
         one# (int 1)
         n# (unchecked-add i# j#)
         n+1# (unchecked-add n# one#)]
     (/ (double 1.0)
        (unchecked-add (bit-shift-right (unchecked-multiply n# n+1#) one#)
                       (unchecked-add i# one#)))))

(defn multiply-a-v [^doubles v
                    ^doubles av]
  (dotimes [i (alength av)]
    (aset av i (double (areduce v j sum (double 0) (+ sum (* (eval-a i j) (aget v j))))))))

(defn multiply-at-v [^doubles v
                     ^doubles atv]
  (dotimes [i (alength atv)]
    (aset atv i (double (areduce v j sum (double 0) (+ sum (* (eval-a j i) (aget v j))))))))

(defn multiply-at-a-v [^doubles v
                       ^doubles tmp
                       ^doubles at-av]
  (multiply-a-v v tmp)
  (multiply-at-v tmp at-av))

(defn dot-product [^doubles u
                   ^doubles v]
  (areduce u i sum (double 0) (+ sum (* (aget u i) (aget v i)))))

(defn run-game [size]
  (let [u (double-array size 1.0)
        tmp (double-array size 0.0)
        v (double-array size 0.0)]
    (dotimes [_ 10]
      (multiply-at-a-v u tmp v)
      (multiply-at-a-v v tmp u))
    (let [vbv (dot-product u v)
          vv (dot-product v v)]
      (Math/sqrt (/ vbv vv)))))

(defn -main [& args]
  (let [n (if (empty? args)
            2500
            (Integer/valueOf ^String (first args)))]
    (println (format "%.9f" (run-game n)))))

