# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
#
# contributed by Jacob Lee, Steven Bethard, et al
# modified by 2to3, Daniele Varrazzo, and Daniel Nanz

import sys

def main(table=str.maketrans('ACBDGHKMNSRUTWVYacbdghkmnsrutwvy',
                             'TGVHCDMKNSYAAWBRTGVHCDMKNSYAAWBR')):
    
    sequences = ''.join(line for line in sys.stdin).split('>')
    
    for sequence in sequences[1 : ]:
        [header, seq] = sequence.split('\n', 1)
        seq = seq.replace('\n', '')[ : : -1].translate(table)

        print('>', header, sep='')
        for i in range(0, len(seq), 60):
            print(seq[i : i + 60])
      

main()
