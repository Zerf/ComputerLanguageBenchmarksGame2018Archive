#!/usr/bin/python -OO
"""
    The Computer Language Benchmarks Game
    http://shootout.alioth.debian.org/

    contributed by Isaac Gouy
    converted to Java by Oleg Mazurov
    converted to Python by Buck Golemon
"""

def fannkuch(n):
    maxFlipsCount = 0
    permCount = 0
    checksum = 0

    r = n
    perm1 = list(range(1,n+1))
    count = [0] * r
    perm  = [0] * r

    while True:

        while r != 1:
            count[r-1] = r
            r -= 1 

        perm = perm1[:]
        flipsCount = 0

        k=perm[0]
        while k != 1:
            temp = perm[0:k]
            temp.reverse()
            perm[0:k] = temp
            
            flipsCount += 1
            k=perm[0]
        
        maxFlipsCount = max(maxFlipsCount, flipsCount)
        if permCount & 1:
            checksum -= flipsCount
        else:
            checksum += flipsCount

        # Use incremental change to generate another permutation
        while True:
            if r == n:
                print checksum
                return maxFlipsCount
        
            perm0 = perm1[0]
            perm1[0:r] = perm1[1:r+1]
            perm1[r] = perm0
            count[r] = count[r] - 1

            if count[r] > 0:
                break
            r += 1
        permCount += 1
    
from sys import argv
n = int(argv[1])

print "Pfannkuchen(%i) = %i" % (n, fannkuch(n))

