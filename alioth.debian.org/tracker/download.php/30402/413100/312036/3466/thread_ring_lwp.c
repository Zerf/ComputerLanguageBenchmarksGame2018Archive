/*
* The Computer Language Benchmarks Game
* http://shootout.alioth.debian.org/

* based on a contribution by Premysl Hruby
* modified to use LWP instead of pthreads by Guenther Thomsen
*/

#include <stdio.h>
#include <stdlib.h>
#include <lwp/lwp.h>
#include <lwp/lock.h>
#include <string.h>
#include <limits.h>

#define THREADS (503)


// LWP2 library doesn't contain explicit mutex handling functions.
typedef volatile int mutex_t;
inline void mutex_init(mutex_t *m)
{
	*m = 0;
}

// lock mutex, yield iff mutex was already locked and retry
inline void mutex_lock(mutex_t *m)
{
	// No need for atomic operations or careful locking here; we're using
	// non-preemptive "scheduling" within a single thread of execution.
	// Simply yielding turns out to be faster than waiting here and later
	// signaling the LWProcess(es) waiting for the mutex.
	while(++*m > 1) {
		--*m;
		LWP_DispatchProcess();
	}
}

// unlock mutex, yield
inline void mutex_unlock(mutex_t *m)
{
	*m = 0;
	LWP_DispatchProcess();
}


static mutex_t mutex[THREADS];
static int data[THREADS];
#define STACK_SIZE 256
static int result;

static PROCESS thread_ids[THREADS];


static void* thread(void *num)
{
	int l = (int)num;
	int r = (l+1) % THREADS;
	int token;

	while(1) {
		mutex_lock(&mutex[l]);
		token = data[l];
		if (token) {
			data[r] = token - 1;
			mutex_unlock(&mutex[r]);
		}
		else {
			result = l+1;
			LWP_SignalProcess(&result);
		}
	}
}


int main(int argc, char **argv)
{
	int i;
	PROCESS id;
	char pname[10];

	if (argc != 2)
		exit(255);
	data[0] = atoi(argv[1]);

	LWP_Init(LWP_VERSION, LWP_NORMAL_PRIORITY, &id);

	for (i = 0; i < THREADS; i++) {
		mutex_init(&mutex[i]);
		mutex_lock(&mutex[i]);
		sprintf(pname, "Thread%3d", i);
		LWP_CreateProcess(thread, STACK_SIZE, LWP_NORMAL_PRIORITY, 
				  (void *)i, pname, &thread_ids[i]);
	}

	mutex_unlock(&mutex[0]);
	LWP_WaitProcess(&result);
	printf("%i\n", result);
	exit(0);
}
