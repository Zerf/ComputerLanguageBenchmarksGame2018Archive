// The Computer Language Benchmarks Game
// http://shootout.alioth.debian.org/
//
// Contributed by Ben Lynn.

// Deterministic. Single-threaded. Standard C library only.
//
// Cheats: Handles only ACGT bases, loosely interprets "hash table" (we
// use tries), carefully chosen memory pool size.

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Reading frames.
static char *frame[1 << 16];
// Number of frames.
static int framen;

// We use extensible hashing with a hash table of minimal size, i.e. a trie.
struct node_s {
  union {
    int count;
    struct node_s *child[4];
  };
};
typedef struct node_s *node_ptr;
typedef struct node_s node_t[1];

struct tab_s {
  int len;
  int count;
  node_t root;
};

typedef struct tab_s *tab_ptr;
typedef struct tab_s tab_t[1];

enum {
  BIG = 16384,
};

static void node_init(node_t t) {
  memset(t, 0, sizeof(t));
}

enum {
  POOLMAX = 1 << 21,
};
static struct node_s pool[POOLMAX];
static int poolcount;

static node_ptr node_new() {
  if (poolcount < POOLMAX) {
    return &pool[poolcount++];
  }
  node_ptr res = malloc(sizeof(*res));
  node_init(res);
  return res;
}

static int acgt_code[128];
static char to_acgt[] = { 'A', 'C', 'G', 'T' };

static tab_ptr update(int len) {
  // Assume we'll never use old hash tables again.
  memset(pool, 0, poolcount * sizeof(struct node_s));
  poolcount = 0;
  tab_ptr res = malloc(sizeof(*res));
  int count = 0;
  node_ptr root = res->root;
  node_init(res->root);

  // A circular buffer so we can follow pointers in parallel.
  node_ptr circ[len + 1];
  circ[len] = NULL;  // Sentinel.
  for (int i = 0; i < len; i++) circ[i] = root;
  char **fr = frame;
  char *c = *fr;
  int i;
  for(i = 0; i < len - 1; i++) {
    int k = *c++;

    int c1;
    for(c1 = 0; c1 <= i; c1++) {
      node_ptr p = circ[c1];
      if (!p->child[k]) p->child[k] = node_new();
      circ[c1] = p->child[k];
    }
  }
  node_ptr *circp = circ;

  for(;;) {
    int i = *c++;
    for(node_ptr *cp = circ; *cp; cp++) {
      // The inner loop.
      node_ptr p = *cp;
      node_ptr *pp = &p->child[i];
      if (!*pp) *pp = node_new();
      *cp = *pp;
    }
    count++;
    (*circp)->count++;
    if (-1 == *c) {
      fr++;
      if (!*fr) {
	res->len = len;
	res->count = count;
	return res;
      }
      c = *fr;
    }
    *circp = root;
    circp++;
    if (!*circp) circp = circ;
  }
}

static void tab_dump(tab_ptr tab) {
  char buf[32];
  struct entry_s {
    char *s;
    int count;
  };
  typedef struct entry_s *entry_ptr;
  entry_ptr earr[4 * 4];
  int en = 0;
  int k = 0;
  void recurse(node_ptr p) {
    int i;
    if (k == tab->len) {
      // Insertion sort will do.
      entry_ptr e = malloc(sizeof(*e));
      e->s = strndup(buf, k);
      e->count = p->count;
      for(i = 0; i < en && earr[i]->count >= p->count; i++);
      int j;
      for(j = en; j >= i; j--) {
	earr[j] = earr[j - 1];
      }
      earr[i] = e;
      en++;
      return;
    }
    for(i = 0; i < 4; i++) {
      if (p->child[i]) {
	buf[k++] = to_acgt[i];
	recurse(p->child[i]);
	k--;
      }
    }
  }
  recurse(tab->root);
  for(int i = 0; i < en; i++) {
    printf("%s %.3f\n", earr[i]->s, 100.0 * (float) earr[i]->count / tab->count);
  }
}

static void tab_lookup(tab_ptr tab, const char *s) {
  node_ptr p = tab->root;
  const char *c = s;
  while (*c) {
    p = p->child[acgt_code[*c]];
    if (!p) break;
    c++;
  }
  printf("%d\t%s\n", p ? p->count : 0, s);
}

int main() {
  for(int i = 0; i < 4; i++) {
    acgt_code[to_acgt[i]] = i;
    acgt_code[to_acgt[i] | 32] = i;
  }

  char buf[128];
  while (fgets(buf, 128, stdin)) {
    if (!strncmp(buf, ">THREE", 6)) break;
  }

  framen = 0;
  char *p = frame[framen++] = malloc(BIG);
  int i = 0;
  for (;;) {
    int c = getchar();
    if ('\n' == c) continue;
    if (EOF == c || '>' == c) {
      p[i] = -1;
      break;
    }
    p[i++] = acgt_code[c];
    if (BIG - 1 == i) {
      p[i] = -1;
      p = frame[framen++] = malloc(BIG);
      i = 0;
    }
  }
  frame[framen] = NULL;

  tab_ptr tab1 = update(1);
  tab_dump(tab1);
  putchar('\n');

  tab_ptr tab2 = update(2);
  tab_dump(tab2);
  putchar('\n');

  tab_ptr tab3 = update(3);
  tab_lookup(tab3, "GGT");

  tab_ptr tab4 = update(4);
  tab_lookup(tab4, "GGTA");

  tab_ptr tab6 = update(6);
  tab_lookup(tab6, "GGTATT");

  tab_ptr tab12 = update(12);
  tab_lookup(tab12, "GGTATTTTAATT");

  tab_ptr tab18 = update(18);
  tab_lookup(tab18, "GGTATTTTAATTTATAGT");

  return 0;
}
