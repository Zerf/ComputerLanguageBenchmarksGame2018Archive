/// The Computer Language Benchmarks Game
/// http://shootout.alioth.debian.org/
///
/// contributed by Cesar Mendoza

open System
open System.Text
open System.Text.RegularExpressions

let data = Console.In.ReadToEnd()
let initialLength  = data.Length


let buffer = 
    let sb = data.Split('\n') |> Seq.filter(fun line -> line.StartsWith(">") |> not)
                |> Seq.fold(fun (sb: StringBuilder) line -> sb.Append(line)) (new StringBuilder(initialLength))
    sb.ToString()

let codeLength = buffer.Length 

let variants = [ "agggtaaa|tttaccct";
                 "[cgt]gggtaaa|tttaccc[acg]";
                 "a[act]ggtaaa|tttacc[agt]t";
                 "ag[act]gtaaa|tttac[agt]ct";
                 "agg[act]taaa|ttta[agt]cct";
                 "aggg[acg]aaa|ttt[cgt]ccct";
                 "agggt[cgt]aa|tt[acg]accct";
                 "agggta[cgt]a|t[acg]taccct";
                 "agggtaa[cgt]|[acg]ttaccct" ]


let map_parallel f items =                                  
    seq { for i in items -> async { return (f i) } } |> Async.Parallel |> Async.RunSynchronously

let processVariant (var: string) =
    let count = var.Split('|') |> map_parallel(fun var ->
        let re = new Regex(var)
        let matches = re.Matches(buffer) in matches.Count) |> Array.sum
    (var, count)
    
let results = variants |> map_parallel processVariant

let map = Map.of_list [ ('W', "(a|t)");
            ('Y', "(c|t)");
            ('K', "(g|t)");
            ('M', "(a|c)");
            ('S', "(c|g)");
            ('R', "(a|g)");
            ('B', "(c|g|t)");
            ('D', "(a|g|t)");
            ('V', "(a|c|g)");
            ('H', "(a|c|t)");
            ('N', "(a|c|g|t)") ]

let sequence =
    seq {
        for ch in buffer do
            match map.TryFind(ch) with
                | None -> yield ch
                | Some str -> yield! str
    }
let sequenceLength = sequence |> Seq.length

results |> Array.iter(fun (var,count) -> printfn "%s %d" var count)

printfn ""

printfn "%d" initialLength

printfn "%d" codeLength
printfn "%d" sequenceLength

