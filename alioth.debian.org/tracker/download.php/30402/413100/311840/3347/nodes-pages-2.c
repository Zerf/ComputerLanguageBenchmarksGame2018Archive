/*
 * Francesco Abbate, fr4nko at ubuntuforums
 */

#include <stdlib.h>
#include <stdio.h>

const size_t	LINE_SIZE = 64;
const size_t	PAGE_SIZE = 4 * 1024;

struct node
{
  int i;
  struct node *left;
  struct node *right;
};

struct page {
  struct page *next;
  char data[1];
};

struct pool {
  struct node *current;
  struct node *end_mark;
  struct page *pages;
};

int
node_check(const struct node *n)
{
  if (n->left)
    {
      int lc = node_check (n->left);
      int rc = node_check (n->right);
      return lc + n->i - rc;
    }

  return n->i;
}

struct node *
node_get_avail (struct pool *pool)
{
  struct node *p = pool->current;

  if (p == NULL || p + 1 > pool->end_mark)
    {
      struct page *new_page = malloc (PAGE_SIZE);
      char *ptr;

      if (new_page == NULL)
	exit(1);

      new_page->next = pool->pages;
      pool->pages = new_page;

      ptr = (char *) new_page;
      pool->current  = (struct node *) (ptr + sizeof(void *));
      pool->end_mark = (struct node *) (ptr + PAGE_SIZE);
      p = pool->current;
    }

  pool->current ++;

  return p;
}

void
pool_init (struct pool *pool)
{
  pool->current = NULL;
  pool->pages = NULL;
}

void
pool_dealloc (struct pool *pool)
{
  struct page *p = pool->pages;

  while (p)
    {
      struct page *next = p->next;
      free (p);
      p = next;
    }
}

struct node *
make (int i, int depth, struct pool *pool)
{
  struct node *curr = node_get_avail (pool);

  curr->i = i;

  if (depth > 0)
    {
      struct node *left  = make (2*i-1, depth - 1, pool);
      struct node *right = make (2*i  , depth - 1, pool);
	
      curr->left  = left;
      curr->right = right;
    }
  else
    {
      curr->left  = NULL;
      curr->right = NULL;
    }

  return curr;
}

int
main(int argc, char *argv[])
{
  struct pool long_lived_pool[1];
  int min_depth = 4;
  int req_depth = (argc == 2 ? atoi(argv[1]) : 10);
  int max_depth = (req_depth > min_depth + 2 ? req_depth : min_depth + 2);
  int stretch_depth = max_depth+1;

  /* Alloc then dealloc stretchdepth tree */
  {
    struct pool store[1];
    struct node *curr;

    pool_init (store);
    curr = make (0, stretch_depth, store);
    printf ("stretch tree of depth %i\t check: %i\n", stretch_depth, 
	    node_check (curr));
    pool_dealloc (store);
  }

  pool_init (long_lived_pool);

  {
    struct node *long_lived_tree = make(0, max_depth, long_lived_pool);

    /* buffer to store output of each thread */
    char *outputstr = (char*) malloc(LINE_SIZE * (max_depth +1) * sizeof(char));
    int d;

#pragma omp parallel for
    for (d = min_depth; d <= max_depth; d += 2)
      {
        int iterations = 1 << (max_depth - d + min_depth);
        int c = 0, i;

        for (i = 1; i <= iterations; ++i)
	  {
	    struct pool store[1];
	    struct node *a, *b;

	    pool_init (store);
	    a = make ( i, d, store);
	    b = make (-i, d, store);
            c += node_check (a) + node_check (b);
	    pool_dealloc (store);
        }
	
	/* each thread write to separate location */
	sprintf(outputstr + LINE_SIZE * d, "%d\t trees of depth %d\t check: %d\n", (2 * iterations), d, c);
      }

    /* print all results */
    for (d = min_depth; d <= max_depth; d += 2)
      printf("%s", outputstr + (d * LINE_SIZE) );
    free(outputstr);

    printf ("long lived tree of depth %i\t check: %i\n", max_depth, 
	    node_check (long_lived_tree));

    return 0;
  }
}
