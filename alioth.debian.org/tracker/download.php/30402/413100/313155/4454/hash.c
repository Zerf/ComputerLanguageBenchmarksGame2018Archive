/* The Computer Language Benchmarks Game
   http://shootout.alioth.debian.org/

   contributed by Lauri Viitanen
*/
#include "hash.h"

#include <malloc.h>

#define HASH_INITIAL_SIZE	16	/** # of keys in initial hash map. */
#define HASH_GROWTH_RATE	4	/** Power of 2 rate for hash wrowth. */

#if 0
	/* http://www.concentric.net/~ttwang/tech/inthash.htm */
	#define _hash_hash(k_mer, len, res) do { \
		uint32_t hash = (k_mer); \
		hash = ~hash + (hash << 18); \
		hash ^= hash >> 31; \
		hash *= 21; \
		hash ^= hash >> 11; \
		hash += hash << 6; \
		hash ^= hash >> 22; \
		(res) = (uint32_t)(hash); \
	} while (0)
#else
	#define _hash_hash(k_mer, len, res) do { \
		const uint8_t sh = (len) >> 1; \
		(res) = ((k_mer) + ((k_mer) >> sh)); \
	} while (0)
#endif

/* Linear search is great - 99.9 % of data is found within 6 comparisons! */
#define _hash_find(this, k_mer, res) do { \
	const struct helem* data = (this)->data; \
	const uint32_t mask = (this)->table_size - 1; \
	uint32_t _p = 0, _q = 0; \
	\
	_hash_hash((k_mer), (this)->k_mer_sz, _p); \
	_q = _p &= mask; \
	\
	while (data[_q].key != (k_mer) && data[_q].count && \
	       ++_q < this->table_size); \
	\
	if (_q < this->table_size) \
		(res) = _q; \
	else \
	{ \
		_q = 0; \
		while (data[_q].key != (k_mer) && data[_q].count && \
		       ++_q < _p); \
		(res) = _q; \
	} \
} while (0)

void hash_add(struct hash* this, uint64_t k_mer)
{
	uint32_t p;
	k_mer &= this->k_mer_mask;

	if (this->index_is_key)
	{
		++this->count[k_mer];
		return;
	}

	if (4*this->fill_rate >= 3*this->table_size) /* max. 75% fill rate. */
	{
		/* Re-hashing consumes approx. 1/14 of total running time. */
		struct helem* old_data = this->data;
		const uint32_t old_size = this->table_size;
		uint32_t i, j;

		this->table_size *= HASH_GROWTH_RATE;

		if (this->k_mer_mask < this->table_size)
		{
			this->index_is_key = 1;
			this->table_size = this->k_mer_mask + 1;
			this->count = (uint32_t*)calloc(this->table_size,
				sizeof(uint32_t));

			for (i = 0, j = this->fill_rate; j && i < old_size;
			     ++i)
			{
				if (old_data[i].count)
					this->count[old_data[i].key] =
						old_data[i].count;
			}

			++this->count[k_mer];
			free(old_data);
			return;
		}

		this->data = (struct helem*)calloc(this->table_size,
			sizeof(struct helem));

		for (i = 0, j = this->fill_rate; j && i < old_size; ++i)
			if (old_data[i].count)
			{
				_hash_find(this, old_data[i].key, p);
				this->data[p].key = old_data[i].key;
				this->data[p].count = old_data[i].count;
				--j;
			}

		free(old_data);
	}

	_hash_find(this, k_mer, p);
	this->fill_rate += !this->data[p].count;
	this->data[p].key = k_mer;
	++this->data[p].count;
}

uint32_t hash_count(struct hash* this, uint64_t k_mer)
{
	uint32_t p;

	if (this->index_is_key)
		return this->count[k_mer];

	_hash_find(this, k_mer, p);
	return this->data[p].count;
}

void hash_init(struct hash* this, uint32_t k_mer_sz)
{
	this->k_mer_mask = (1ULL << (k_mer_sz << 1)) - 1;
	this->k_mer_sz = k_mer_sz;

	if (this->k_mer_mask < HASH_INITIAL_SIZE)
	{
		this->table_size = 1 + this->k_mer_mask;
		this->count = calloc(this->table_size, sizeof(uint32_t));
		this->index_is_key = 1;
	}
	else
	{
		this->table_size = HASH_INITIAL_SIZE;
		this->data = calloc(this->table_size, sizeof(struct helem));
		this->index_is_key = 0;
	}

	this->fill_rate = 0;
}

