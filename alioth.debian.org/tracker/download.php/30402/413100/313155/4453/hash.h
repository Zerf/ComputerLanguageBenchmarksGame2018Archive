/* The Computer Language Benchmarks Game
   http://shootout.alioth.debian.org/

   contributed by Lauri Viitanen
*/
#ifndef HASH_H
#define HASH_H

#include <stdint.h>

struct helem
{
	uint64_t key;
	uint32_t count;
};

struct hash
{
	uint64_t k_mer_mask; /** Bitmask for k-mer bit presentations. */

	struct helem* data; /** Hash table in memory. */
	uint32_t* count; /** Contains only key's hit counter. */

	uint32_t table_size; /** Size of table in elements. */
	uint32_t k_mer_sz;

	uint32_t fill_rate; /** # of occupied table elements. */
	uint32_t index_is_key;
};

extern void hash_add(struct hash* this, uint64_t k_mer);
extern uint32_t hash_count(struct hash* this, uint64_t k_mer);
extern void hash_init(struct hash* this, uint32_t k_mer_sz);
extern void hash_shutdown(struct hash* this);

#endif

