/* The Computer Language Benchmarks Game
   http://shootout.alioth.debian.org/

   contributed by Lauri Viitanen
*/
/*
   Input reading and output printing & sorting copied from gcc entry #7.
   Idea for threads and hashing function taken from gpp entry #6.
*/

#include "hash.h"

#include <malloc.h>
#include <pthread.h>
#include <stdint.h>
#define __USE_ISOC99
#include <stdio.h>
#include <stdlib.h> /* qsort() */
#include <string.h> /* strlen() */
#include <unistd.h>
#include <sys/types.h> /* fstat() */
#include <sys/stat.h> /* fstat() */

struct thread_params
{
	uint64_t k_mer;
	struct hash* h;
	const char* data;
	uint32_t size;
};

extern char *fgets_unlocked (char *__restrict __s, int __n,
                             FILE *__restrict __stream);
extern int fputs_unlocked (__const char *__restrict __s,
                           FILE *__restrict __stream);

static const char k_mer1_str[4][2] = { "A", "C", "T", "G" };
static const char k_mer2_str[16][4] = {
	"AA\0", "AC\0", "AT\0", "AG\0",
	"CA\0", "CC\0", "CT\0", "CG\0",
	"TA\0", "TC\0", "TT\0", "TG\0",
	"GA\0", "GC\0", "GT\0", "GG\0" };

int cmp(const void *l, const void *r)
{
    const struct helem *lh = l, *rh = r;

    if (lh->key != rh->key)
        return rh->count - lh->count;
    else
        return  lh->key < rh->key ? -1 : lh->key > rh->key ? 1 : 0;
}

void* thread_job(void* p)
{
	const struct thread_params* const pa = (struct thread_params*)p;
	uint64_t k_mer = pa->k_mer;
	uint32_t k, i;

	for (i = 0; i < pa->size; ++i)
		if ((k = pa->data[i]) != '\n')
		{
			k_mer = k_mer << 2 | (k >> 1 & 3);
			hash_add(pa->h, k_mer);
		}

	pthread_exit(NULL);
}

static void count_k_mers(const char* data, uint32_t size)
{
	#define MAX_LENGTH	18
	#define BUF_SZ    	512

	struct hash h1, h2, h3, h4, h6, h12, h18;
	struct thread_params th_prm;
	struct helem he[16];
	uint64_t k_mer;
	pthread_t tid;
	float f;
	uint32_t i, k, s;
	char buf[BUF_SZ];

	if (size < MAX_LENGTH)
		return;

	#define HI(n) do { hash_init(&h##n, n); } while (0)
	#define K() do { k_mer = k_mer << 2 | (data[i++] >> 1 & 3); } while(0)
	#define HA(n) do { hash_add((&h##n), k_mer); } while (0)

	HI(1); HI(2); HI(3); HI(4); HI(6); HI(12); HI(18);
 
	i = 0;
	k_mer = 0;
	K(); HA(1);
	K(); HA(1); HA(2);
	K(); HA(1); HA(2); HA(3);
	K(); HA(1); HA(2); HA(3); HA(4);
	K(); HA(1); HA(2); HA(3); HA(4);
	K(); HA(1); HA(2); HA(3); HA(4); HA(6);
	K(); HA(1); HA(2); HA(3); HA(4); HA(6);
	K(); HA(1); HA(2); HA(3); HA(4); HA(6);
	K(); HA(1); HA(2); HA(3); HA(4); HA(6);
	K(); HA(1); HA(2); HA(3); HA(4); HA(6);
	K(); HA(1); HA(2); HA(3); HA(4); HA(6);
	K(); HA(1); HA(2); HA(3); HA(4); HA(6); HA(12);
	K(); HA(1); HA(2); HA(3); HA(4); HA(6); HA(12);
	K(); HA(1); HA(2); HA(3); HA(4); HA(6); HA(12);
	K(); HA(1); HA(2); HA(3); HA(4); HA(6); HA(12);
	K(); HA(1); HA(2); HA(3); HA(4); HA(6); HA(12);
	K(); HA(1); HA(2); HA(3); HA(4); HA(6); HA(12);
	K(); HA(1); HA(2); HA(3); HA(4); HA(6); HA(12); HA(18);

	th_prm.k_mer = k_mer;
	th_prm.h = &h18;
	th_prm.data = &data[i];
	th_prm.size = size - i;
	pthread_create(&tid, NULL, thread_job, (void*)&th_prm);

	for (; i < size; ++i)
		if ((k = data[i]) != '\n')
		{
			k_mer = k_mer << 2 | (k >> 1 & 3);
			HA(1); HA(2); HA(3); HA(4); HA(6); HA(12);
		}

	#undef HA
	#undef K
	#undef HI

	for (s = 0, i = 0; i < 4; ++i)
	{
		he[i].key = i;
		s += he[i].count = h1.count[i];
	}

	f = 1.0f/(float)s;
	qsort(he, 4, sizeof(struct helem), cmp);

	k = 0;
	for (i = 0; i < 4; ++i)
		k += snprintf(buf + k, BUF_SZ - k, "%s %.3f\n",
		              k_mer1_str[he[i].key],
		              he[i].count*100.0f*f);

	for (s = 0, i = 0; i < 16; ++i)
	{
		he[i].key = i;
		s += he[i].count = h2.count[i];
	}

	f = 1.0f/(float)s;
	qsort(he, 16, sizeof(struct helem), cmp);

	for (i = 0; i < 16; ++i)
		k += snprintf(buf + k, BUF_SZ - k, "\n%s %.3f",
		              k_mer2_str[he[i].key],
		              he[i].count*100.0f*f);

	pthread_join(tid, NULL);

	snprintf(buf + k, BUF_SZ - k, "\n\n%u\tGGT\n%u\tGGTA\n%u\t"
	         "GGTATT\n%u\tGGTATTTTAATT\n%u\tGGTATTTTAATTTATAGT\n",
	         hash_count(&h3,  0x3eULL     /*0b111110*/),
	         hash_count(&h4,  0xf8ULL     /*0b11111000*/),
	         hash_count(&h6,  0xf8aULL    /*0b111110001010*/),
	         hash_count(&h12, 0xf8aa0aULL /*0b111110001010101000001010*/),
	         hash_count(&h18, 0xf8aa0a88eULL
		                  /*0b111110001010101000001010100010001110*/));

	fputs_unlocked(buf, stdout);

	#undef BUF_SZ
	#undef MAX_LENGTH
}

static char* read_stdin(uint32_t* size)
{
	const char* str = ">THREE";

	struct stat st;
	char* result;
	uint32_t read, len;

	fstat(STDIN_FILENO, &st);
	result = malloc(st.st_size); /* Allocate memory for entire file. */

	do
	{
		fgets_unlocked(result, st.st_size, stdin);
	} while (*result != *str || memcmp(result, str, 6) != 0);

	read = 0;
	while (fgets_unlocked(result + read, st.st_size - read, stdin))
	{
		len = strlen(result + read);
		if (len == 0 || result[read] == '>')
			break;

		read += len;
		read -= (result[read - 1] == '\n');
	}

	*size = read;
	return realloc(result, read);
}

int main(void)
{
	char* data;
	uint32_t size;

	data = read_stdin(&size);
	count_k_mers(data, size);
	free(data);
	return 0;
}

