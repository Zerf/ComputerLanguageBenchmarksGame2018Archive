(* The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 *
 * contributed by Troestler Christophe
 * modified by Mauricio Fernandez
 * optimized by Fabrice Le Fessant
 *)

let tab = Array.create 256 0
let _ =
  tab.(Char.code 'A') <- 0;
  tab.(Char.code 'a') <- 0;
  tab.(Char.code 'T') <- 1;
  tab.(Char.code 't') <- 1;
  tab.(Char.code 'C') <- 2;
  tab.(Char.code 'c') <- 2;
  tab.(Char.code 'g') <- 3;
  tab.(Char.code 'G') <- 3

let uppercase line =
  let len = String.length line in
    for i = 0 to len- 1 do
      let c =  line.[i] in
	line.[i] <- Char.unsafe_chr tab.(Char.code c)
    done

    (* Extract DNA sequence "THREE" from stdin *)
    let dna =
      let is_not_three s = String.length s < 6 || String.sub s 0 6 <> ">THREE" in
	while is_not_three(input_line stdin) do () done;
	let buf = Buffer.create 130000000 in
	  (* Skip possible comment *)
	  (try
	     while true do
	       let line = input_line stdin in
		 if line.[0] <> ';' then begin
		   uppercase line;
		   Buffer.add_string buf line;
		   raise Exit
		 end
	     done with _ -> ());
	  (* Read the DNA sequence *)
	  (try while true do
	     let line = input_line stdin in
	       if line.[0] = '>' then raise End_of_file;
	       uppercase line;
	       Buffer.add_string buf line
	   done with End_of_file -> ());
	  Buffer.contents buf


    module K31 = struct
      type t = int
      let equal k1 k2 = k1 = k2
      let hash n = n
    end

    module K32 = struct
      type t = int * int
      let equal (a1,a2) (b1,b2) = a1 = b1 && a2 = b2
      let hash (a1, _) = a1
    end

    type entry = {
      mutable count : int;
    }

    let c = 0x40000 - 1
    module H31 = Hashtbl.Make(K31)
    module H32 = Hashtbl.Make(K32)
    let h31 = H31.create c
    let h32 = H32.create c

    let rec pack_word n k h =
      let b = Char.code dna.[n] in
      let h = h * 4 + b in
	if k > 1 then
	  pack_word (n+1) (k-1) h
	else h

    let pack31 k n =
      pack_word n k 0

    let pack32 k n =
      let h1 = pack_word n 31 0 in
      let h2 = pack_word (n+31) (k-31) 0 in
	(h1, h2)

    let rec pack_word_in dna n k h =
      let b = dna.[n] in
      let b = tab.(Char.code b) in
      let h = h * 4 + b in
	if k > 1 then
	  pack_word_in dna (n+1) (k-1) h
	else h

    let pack_key31 seq =
      let k = String.length seq in
	pack_word_in seq 0 k 0

    let pack_key32 seq =
      let k = String.length seq in
      let h1 = pack_word_in seq 0 31 0 in
      let h2 = pack_word_in seq 31 (k-31) 0 in
	(h1, h2)

    let char = [| 'A'; 'T'; 'C'; 'G' |]

    let rec unpack h s pos k =
      let pos = pos - 1 in
	s.[pos] <- char.(h land 3);
	if k > 1 then
	  unpack (h lsr 2) s pos (k-1)

    let unpack31 k h1 =
      let s = String.create k in
	unpack h1 s k k;
	s

    let unpack32 k (h1, h2) =
      let s = String.create k in
	unpack h1 s 31 31;
	unpack h2 s k (k-31);
	s

    let count31 k =
      for i = 0 to String.length dna - k - 1 do
	let packed = pack31 k i in
	  try
	    let key = H31.find h31 packed in
	      key.count <- key.count + 1
	  with Not_found ->
	    H31.add h31 packed { count = 1 }
      done

    let count32 k =
      for i = 0 to String.length dna - k - 1 do
	let packed = pack32 k i in
	  try
	    let key = H32.find h32 packed in
	      key.count <- key.count + 1
	  with Not_found ->
	    H32.add h32 packed { count = 1 }
      done

    let count k =
      if k < 32 then count31 k else count32 k

    let compare_freq ((k1:string),(f1:float)) (k2, f2) =
      if f1 > f2 then -1 else if f1 < f2 then 1 else String.compare k1 k2

    let write_frequencies31 k =
      count31 k;
      let tot = float(H31.fold (fun _ n t -> n.count + t) h31 0) in
      let frq =
	H31.fold (fun h n l ->
		  (unpack31 k h, 100. *. float n.count /. tot) :: l) h31 [] in
      let frq = List.sort compare_freq frq in
	String.concat ""
	  (List.map (fun (k,f) -> Printf.sprintf "%s %.3f\n" k f) frq)

    let write_frequencies32 k =
      count32 k;
      let tot = float(H32.fold (fun _ n t -> n.count + t) h32 0) in
      let frq =
	H32.fold (fun h n l ->
		  (unpack32 k h, 100. *. float n.count /. tot) :: l) h32 [] in
      let frq = List.sort compare_freq frq in
	String.concat ""
	  (List.map (fun (k,f) -> Printf.sprintf "%s %.3f\n" k f) frq)

    let write_count31 k seq =
	count31 k;
	Printf.sprintf "%d\t%s" (try (H31.find h31 (pack_key31 seq)).count with Not_found -> 0) seq

    let write_count32 k seq =
	count32 k;
	Printf.sprintf "%d\t%s" (try (H32.find h32 (pack_key32 seq)).count with Not_found -> 0) seq

    let write_frequencies k =
	if k < 32 then write_frequencies31 k
	else write_frequencies32 k

    let write_count seq =
      let k = String.length seq in
	if k < 32 then write_count31 k seq
	else write_count32 k seq

    type t = Size of int | Dna of string

let invoke (f : t -> string) x : unit -> string =
  let input, output = Unix.pipe() in
  match Unix.fork() with
  | -1 -> Unix.close input; Unix.close output; (let v = f x in fun () -> v)
  | 0 ->
      Unix.close input;
      let output = Unix.out_channel_of_descr output in
        Marshal.to_channel output (f x) [];
        close_out output;
        exit 0
  | pid ->
      Unix.close output;
      let input = Unix.in_channel_of_descr input in fun () ->
        let v = Marshal.from_channel input in
        ignore (Unix.waitpid [] pid);
        close_in input;
	v

let parallelize f l =
  let list = List.map (invoke f) (List.rev l) in
  List.iter (fun g -> print_endline (g ())) (List.rev list)

let () =
  parallelize
    (fun i ->
       match i with
	   Size i ->
             write_frequencies i
	 | Dna k ->
             write_count k
    ) [Size 1;
       Size 2;
       Dna "GGT";
       Dna "GGTA";
       Dna "GGTATT";
       Dna "GGTATTTTAATT";
       Dna "GGTATTTTAATTTATAGT"]
