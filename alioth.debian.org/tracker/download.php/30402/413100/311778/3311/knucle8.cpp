// The Computer Language Shootout
// http://shootout.alioth.debian.org/

// Contributed by The Anh Tran

#include <omp.h>
#include <sched.h>

#include <algorithm>
#include <vector>
#include <iostream>

//#include <ext/hash_map>
//#include <boost/unordered_map.hpp>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/hash_policy.hpp>

#include <boost/algorithm/string/case_conv.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/format.hpp>
#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

typedef unsigned int	uint;


// Hash_table key type, with key's length = reading_frame_size
template <int size>
struct hash_key
{
	uint	hash_value;
	char	key[size +1];

	hash_key()
	{
		memset(this, 0, sizeof(*this));
	}

	hash_key(char const * str)
	{
		ReHash (str);
	}

	void ReHash(char const *str)
	{
		// naive hashing algorithm.
		hash_value = 0;

		for (int i = 0; i < size; ++i)
		{
			key[i] = str[i];
			hash_value = (hash_value * 5) + str[i];
		}
	}

	// Hash functor Hash<HKey_Type>
	uint operator() (const hash_key &k) const
	{
		return k.hash_value;
	}

	// Comparison functor equal_to<HKey_Type>(Left, Right)
	bool operator() (const hash_key &k1, const hash_key &k2) const
	{
		if (k1.hash_value == k2.hash_value)
		{
			for (int i = 0; i < size; ++i)
				if ( __builtin_expect((k1.key[i] != k2.key[i]), false) )
					return false;	// collision

			return true;
		}

		return false;
	}
};

// Game's rule: function to update hashtable
template <int hash_len, typename Input_Type, typename HTable_Type>
void calculate_frequency(Input_Type const &input, HTable_Type& hash_table)
{
	typedef typename Input_Type::const_pointer	Ite_Type;

	Ite_Type	ite_beg		= &(input[0]);
	int			char_total	= static_cast<int>(input.size() - hash_len +1);
	Ite_Type	ite_end		= ite_beg + char_total;

	typename HTable_Type::key_type key;

	int threadcount	= omp_get_num_threads ();
	if (threadcount > 1)
	{
		int chunk = std::max(64, std::min(16*1024, char_total / threadcount / 64));

		static int char_done = 0;
		int index;

		// Fetch task. Each thread hashes a block, which block size = chunk
		while ( (index = __sync_fetch_and_add(&char_done, chunk)) < char_total )
		{
			Ite_Type ite	= ite_beg + index;
			Ite_Type end	= std::min(ite + chunk, ite_end);

			for (; ite < end; ++ite)
			{
				key.ReHash(ite);
				++(hash_table[key]);
			}
		}
	}
	else
	{
		for (; ite_beg < ite_end; ++ite_beg)
		{
			key.ReHash(ite_beg);
			++(hash_table[key]);
		}
	}
}


// Build a hash_table, count all key with hash_len = 1, 2
// write the code and percentage frequency
template <int hash_len, typename Input_Type>
void write_frequencies(Input_Type const &input, std::string &output)
{
	typedef hash_key<hash_len>			HKey_Type;
	typedef std::pair<HKey_Type, uint>	HValue_Type;

	//typedef __gnu_cxx::hash_map <
	//typedef boost::unordered_map <
	typedef __gnu_pbds::cc_hash_table	<
											HKey_Type,	// key type
											uint,		// map type
											HKey_Type,	// hash functor
											HKey_Type	// equal_to functor
										> 	HTable_Type;

	typedef std::vector<HValue_Type> 	List_Type;

	static HTable_Type hash_table;	// private for each function<hash_len>()
	HTable_Type local_table;		// private for each thread

	// parallel hashing. Each thread updates its own hash_table.
	calculate_frequency<hash_len>(input, local_table);

	// merge thread local results to main hash_table
	#pragma omp critical
	{
		foreach (typename HTable_Type::value_type const & e, local_table)
			hash_table[e.first] += e.second;
	}

	// we want the last thread, reaching this code block, to process result
	static int thread_passed = 0;
	if (__sync_add_and_fetch(&thread_passed, 1) == omp_get_num_threads())
	{
		// Copy results from hash_table to list
		List_Type order_table(hash_table.begin(), hash_table.end());

		// Sort with descending frequency
		using namespace boost::lambda;
		std::sort(	order_table.begin(), order_table.end(),
			( !(bind(&HValue_Type::second, _1) < bind(&HValue_Type::second, _2)) )	);

		float const total_char = static_cast<float>(input.size() - hash_len +1);
		boost::format fmt("%|1$s| %|2$0.3f|\n");

		foreach(typename List_Type::value_type &e, order_table)
		{
			e.first.key[hash_len] = 0; // ensure proper null terminated
			boost::to_upper(e.first.key);

			float percent = static_cast<float>(e.second) * 100.0f / total_char;
			fmt % e.first.key % percent;

			output += fmt.str();
		}

		output += "\n";
		thread_passed = 0;
	}
}

// Build a hash_table, count all key with hash_len = 3, 4, 6, 12, 18
// Then print a specific sequence's count
template <int hash_len, typename Input_Type>
void write_frequencies(Input_Type const &input, std::string &output, char const *specific)
{
	typedef hash_key<hash_len>		HKey_Type;
	typedef __gnu_pbds::cc_hash_table	<
											HKey_Type,	// key type
											uint,		// map type
											HKey_Type,	// hash functor
											HKey_Type	// equal_to functor
										> 	HTable_Type;

	HTable_Type local_table;	// private for each thread
	calculate_frequency<hash_len>(input, local_table);	// parallel hash

	// Build hash key for searching
	HKey_Type printkey(specific);

	// count how many matched for specific sequence
	static uint total_matched = 0;
	uint matched = local_table[printkey]; // parallel look up

	#pragma omp atomic
	total_matched += matched;

	// The last thread, reaching this code block, will print result
	static int thread_passed = 0;
	if (__sync_add_and_fetch(&thread_passed, 1) == omp_get_num_threads())
	{
		printkey.key[hash_len] = 0; // null terminated
		boost::to_upper(printkey.key);

		boost::format fmt("%1%\t%2%\n");
		fmt % total_matched % printkey.key;
		output = fmt.str ();

		thread_passed = 0;
	}
}

int GetThreadCount()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	for (int i = 0; i < 16; ++i)
	{
		if (CPU_ISSET(i, &cs))
			++count;
	}
	return count;
}

int main()
{
	typedef std::vector<char> Input_Type;
	Input_Type input;
	input.reserve(256*1024*1024); // 256MB

	char buffer[128];

	// rule: read line-by-line
	while (fgets(buffer, sizeof(buffer), stdin))
	{
		if(strncmp(buffer, ">THREE", 6) == 0)
			break;
	}

	std::back_insert_iterator<Input_Type> back_ite ( input );
	while (fgets(buffer, sizeof(buffer), stdin))
	{
		size_t sz = strlen(buffer);
		if (buffer[sz -1] == '\n')
			--sz;

		std::copy(buffer, buffer + sz, back_ite);
	}

	std::string output[7];
	#pragma omp parallel num_threads(GetThreadCount()) default(shared)
	{
		write_frequencies<18>( input, output[6], "ggtattttaatttatagt" );
		write_frequencies<12>( input, output[5], "ggtattttaatt" );
		write_frequencies< 6>( input, output[4], "ggtatt" );
		write_frequencies< 4>( input, output[3], "ggta" );
		write_frequencies< 3>( input, output[2], "ggt" );
		write_frequencies< 2>( input, output[1] );
		write_frequencies< 1>( input, output[0] );
	}

	std::for_each(&(output[0]), &(output[7]), std::cout << boost::lambda::_1);
}

