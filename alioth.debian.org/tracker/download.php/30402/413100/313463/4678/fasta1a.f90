! The Great Computer Language Shootout
! http://shootout.alioth.debian.org/
!
! fasta implementation - translated from the lua program
! contributed by Simon Geard, 18/1/05
! modified by Andrei Jirnyi
!
! Building info.
! ==============
!
! Linux  - using the Intel Fortran90 compiler:
!
!          ifort -fast -O3 fasta.f90
! or equivalently 
!          ifort -xHost -no-prec-div -static -O3 fasta.f90
! 
!
! Run
! ===
!          fasta 1000

program fasta
  use iso_fortran_env

 implicit none
  integer num
  character(len=8) argv
  integer, parameter :: IM = 139968, IA = 3877, IC = 29573
  integer, parameter :: LW=60
  character(len=*), parameter :: alu = &
'GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG' // &
'GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA' // &
'CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT' // &
'ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA' // &
'GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG' // &
'AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC' // &
'AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA'
  character(len=1), parameter :: EOL = achar(10)

  type pair
     character(len=1),dimension(:),allocatable :: c
     real(8),dimension(:),allocatable :: p
  end type pair
  
  type(pair) :: homosapiens
  type(pair) :: iub


  ! the allocate() statements are unnecessary if we compile with
  !  -assume realloc_lhs (ifort default is not standard F2003)
  allocate(homosapiens%c(4))
  allocate(homosapiens%p(3))
  homosapiens%c = ['a','c','g','t']
  homosapiens%p = [0.3029549426680d0, 0.1979883004921d0, &
                   0.1975473066391d0]

  allocate(iub%c(15))
  allocate(iub%p(14))
  iub%c = ['a','c','g','t','B','D','H','K','M','N',&
         & 'R','S','V','W','Y']
  iub%p = [.27,.12,.12,.27,(.02,num=1,10)]

  call makeCumulative(iub)
  call makeCumulative(homosapiens)

  call getarg(1,argv)
  read(argv,*) num
 
  close(output_unit)
  open(unit=output_unit, access='stream',form='unformatted')

  call makeRepeatFasta('ONE','Homo sapiens alu',alu,num*2)

  call makeRandomFasta('TWO','IUB ambiguity codes',iub,num*3)

  call makeRandomFasta('THREE','Homo sapiens frequency',homosapiens,num*5)

     
contains

  real(8) function getRandom ()
    integer, save :: last = 42
    last = mod(last * IA + IC, IM)
    getRandom = dble(last)/IM
  end function getRandom

  subroutine makeCumulative(a)
     type(pair), intent(inout) :: a
     integer i
     do i=2, size(a%p)
        a%p(i) = a%p(i) + a%p(i-1)
     end do
  end subroutine makeCumulative
  
  character(len=1) function selectRandom(a)
    type(pair), intent(in) :: a
    selectRandom = a%c(1+count(getRandom() >= a%p))
  end function selectRandom
  
  subroutine makeRandomFasta(id,desc,a,n)
     character(len=*), intent(in) :: id
     character(len=*), intent(in) :: desc
     type(pair), intent(inout) :: a
     integer, intent(in) :: n
     character(len=LW) :: title
     integer :: i,j,l
     character(len=1), allocatable,dimension(:) :: buff
     
     allocate(buff(n+n/10))

     write(title,'(4a)') '>',id,' ',desc
     write(output_unit) trim(title),EOL
     i = 0
     j = 0
     l = 0
     do
        i = i+1
        j = j+1
        l = l+1
        buff(i) = selectRandom(a)
        if(l==n) exit
        if(j == LW) then
           i = i+1
           j = 0
           buff(i) = EOL
        end if
     end do
     i = i+1
     buff(i) = EOL
     write(output_unit) buff(1:i)
     flush(output_unit)
     deallocate(buff)
  end subroutine makeRandomFasta

  subroutine makeRepeatFasta(id,desc,s,n)
     character(len=*), intent(in) :: id
     character(len=*), intent(in) :: desc
     character(len=*), intent(in) :: s
     integer, intent(in) :: n
     integer :: i, j, k, l, kn
     integer, parameter :: length = 60
     character(len=1), allocatable, dimension(:) :: buff
     character(len=LW) :: title
     intrinsic len

     allocate(buff(n+n/10))

     write(title,'(4a)') '>',id,' ',desc
     write(output_unit) trim(title),EOL
     k = 1; kn = len(s)
     
     i = 0 ! in output buffer
     j = 0 ! in output line
     k = 0 ! in repeat seq
     l = 0 ! generated count
     do
        i = i+1
        j = j+1
        k = k+1
        l = l+1
        if(k>kn) k=1
        buff(i) = s(k:k)
        if(l == n) exit
        if(j == LW) then
           i = i+1
           j = 0
           buff(i) = EOL
        end if
     end do
     i = i+1
     buff(i) = EOL
     write(output_unit) buff(1:i)
     flush(output_unit)
     deallocate(buff)
  end subroutine makeRepeatFasta

end program fasta
