﻿(*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 * F# Version contributed by Alex Peake
 * Single Threaded Version - v4
 *)


#light
open System

let fannkuch N =
    let perm = [| 1..N |]
    let count = Array.create N 0
    let flipper = Array.create N 0
   
    
    for e in perm do printf "%d" e
    do printfn "" 

    let rec repeat maxFlips print =
        let rec loop j =
            if (count.[j - 1] <> 0) then Some (perm)
            else if j = N then None
            else
                let tmp = perm.[0]
                for k in 0..(j - 1) do
                    perm.[k] <- perm.[k+1]
                perm.[j] <- tmp

                count.[j] <- (count.[j] + 1) % (j + 1)
                loop (j + 1)
        
        match (loop 1) with
        | Some (perm) ->
            for i in 0..N-1 do
                flipper.[i] <- perm.[i]
            
            let mutable numberFlips = 0
            while (flipper.[0] <> 1) do
                let f0 = (flipper.[0] - 1)
                for i in 0..f0 / 2  do
                    let temp = flipper.[i]
                    flipper.[i] <- flipper.[f0 - i]
                    flipper.[f0 - i] <- temp

                numberFlips <- numberFlips + 1
            
            if (print > 1) then
                for e in perm do printf "%d" e
                do printfn ""
            repeat (max numberFlips maxFlips) (print - 1)
        | None ->
            maxFlips
    
    repeat 0 30


[<EntryPoint>]
let main(args) =
    let x = if args.Length > 0 then int args.[0] else 7
    printfn "Pfannkuchen(%d) = %d" x (fannkuch x)
    0
