import Data.List
import System
import Control.Applicative

flop lst@(h:_) = reverse f ++ s where (f, s) = splitAt h lst

flopS (1:_) = 0
flopS lst = 1 + flopS (flop lst)

rotate n (h:t) = f ++ h:s where (f, s) = splitAt (n-1) t

checksum (i, p@(h:_))
	| mod i 2 == 0 = flopS p
	| True = - (flopS p)
	
permut n = foldr perm [[1..n]] [2..n] where
	perm x lst = concat [take x $ iterate (rotate x) l | l <- lst]
	
main = do
	n <- (read.head) <$> getArgs
	let chksm = sum $ map checksum $ zip [0..] (permut n)
	let mflops = maximum $ map flopS (permut n)
	putStrLn $ (show chksm) ++ "\nPfannkuchen(" ++ (show n) ++ ") = " ++ (show $ mflops)