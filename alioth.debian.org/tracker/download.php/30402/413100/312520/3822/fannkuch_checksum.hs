module Main where
	import Prelude hiding (flip)
	import System

	flip (2:x1:t) = x1:2:t
	flip (3:x1:x2:t) = x2:x1:3:t
	flip (4:x1:x2:x3:t) = x3:x2:x1:4:t
	flip (5:x1:x2:x3:x4:t) = x4:x3:x2:x1:5:t
	
	flip lst@(h:t) = flip' h ([],lst) where
		flip' 0 (l, r) = l ++ r
		flip' n (l, (h2:t2)) = flip' (n-1) (h2:l, t2)

	runFlip lst = runFlip' 0 lst where
		runFlip' n (1:_) = n
		runFlip' n lst = runFlip' (n+1) $ flip lst

	rotate n lst@(h:t) = rotate' n t h where
		rotate' 1 lst e = e:lst
		rotate' n (h:t) e = h:(rotate' (n-1) t e)
	
	skip :: [Int] -> Bool
	skip p@(h':_) = h' == 1 || h == l where
		(h:_) = reverse p
		l = length p

	permut n = filter (\(_,p) -> not.skip $ p) $ zip (concat.repeat $ [(+),(-)]) (foldr perm' [[1..n]] [2..n]) where
		perm' x lst = concat [take x $ iterate (rotate x) l | l <- lst]
	
	main = 
		do	[f] <- getArgs
			let n = read f
			let checksum = sum $ map (\(f, flps) -> f 0 flps) (zip (fst $ unzipped n) (map runFlip (snd $ unzipped n)))
			let maxflips = maximum (map runFlip $ (snd $ unzipped n))
			putStrLn $ show checksum
			putStrLn $ "Pfannkuchen(" ++ f ++ ") = " ++ (show maxflips)
		where
			unzipped n = unzip $ permut n
