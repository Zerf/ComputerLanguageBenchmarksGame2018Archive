module Main where
	import Prelude hiding (flip)
	import System

	flip lst@(n:_) = f s where 
		(f, s) = flip' n lst
		flip' 0 lst = (id, lst)
		flip' n (h:t) = (f . (h:), s) where (f, s) = (flip' (n-1) t)

	doFlip lst = doFlip' 0 lst where
		doFlip' n (1:_) = n
		doFlip' n lst = doFlip' (n+1) $ flip lst
	
	rotate n lst@(h:t) = rotate' n t h where
		rotate' 1 lst e = e:lst
		rotate' n (h:t) e = h:(rotate' (n-1) t e)
	
	permutations n = foldr perm' [[1..n]] [2..n] where
		perm' x lst = concat [take x $ iterate (rotate x) l | l <- lst]

	main = 
		do	[n] <- getArgs	
			let perm = permutations (read n)
			putStr $ foldr (\lst s -> (foldr (\e s' -> (show e) ++ s') ['\n'] lst) ++ s) "" (take 30 perm)
			putStrLn $ "Pfannkuchen(" ++ n ++ ") = " ++ (show $ maximum $ map doFlip perm)
