;;   The Computer Language Benchmarks Game
;;   http://shootout.alioth.debian.org/
;;;
;;; By Jon Smith
;;; Tranlated from OCaml implementation by ?, who translated it from
;;; the Scala implementation by Otto Bommer.
;;; 
;;; This is a single core implementation.
;;; I am sure that this program can be improved upon quite a lot.
;;; Most likely it will involve knowing how sbcl does its optimizations.
;;; As you can see, I simply used fixnums everywhere. There may be a better choice.
;;;
;;; To compile
;;; sbcl --load fannkuch.lisp --eval "(save-lisp-and-die \"fannkuch.core\" :purify t :toplevel (lambda () (main) (quit)))"
;;; To run
;;; sbcl --noinform --core fannkuch.core %A

(declaim (optimize (speed 3) (safety 0) (space 0) (debug 0)))

(defun flip (n array)
  (declare (type fixnum n)
	   (type (simple-array fixnum (16)) array))
  (loop for i from 0 to (ash n -1)
	do (let ((temp (aref array i))
		 (k (- n i)))
	     (setf (aref array i) (aref array k))
	     (setf (aref array k) temp))))

(defun fcount (c array)
  (declare (type fixnum c)
	   (type (simple-array fixnum (16)) array)
	    (inline flip))
  (let ((z (aref array 0)))
    (if (not (= z 0))
	(progn
	  (flip z array)
	  (fcount (1+ c) array))
      c)))

(defun rotate (n array)
  (declare (type fixnum n)
	   (type (simple-array fixnum (16)) array))
  (let ((temp (aref array 0))
	(m (- n 1)))
    (loop for i from 1 to m do
	  (setf (aref array (- i 1)) (aref array i)))
    (setf (aref array m) temp)))

(defun iter-perms (n f)
  (declare (type fixnum n))
  (let ((perm (make-array n :element-type 'fixnum))
	(copy (make-array n :element-type 'fixnum))
	(num 0))
    (declare (type fixnum num))
    (loop for i from 0 to (- n 1) do (setf (aref perm i) i))
    (labels ((do-iter (f ht)
		      (declare (type fixnum ht)
			       (type (function (fixnum (simple-array fixnum (16))) fixnum) f)
			       (inline rotate))

		      (if (= ht 1)
			  (progn
			    (loop for i from 0 to (- n 1) do
				  (setf (aref copy i) (aref perm i)))
			    (funcall f num copy)
			    (incf num))
			(loop for i from 1 to ht do
			      (progn (do-iter f (- ht 1))
				     (rotate ht perm))))))

      (do-iter f n))))

(defun fannkuch (n)
  (declare (type fixnum n))
  (let ((csum 0)
	(fmax 0))
    (declare (type fixnum fmax)
	     (type fixnum csum))
    (iter-perms n (lambda (num array)
		    (declare (type fixnum num)
			     (inline fcount))
		    (let ((c (fcount 0 array)))
		      (declare (type fixnum c))
		      (setf csum (+ csum  (if (evenp num) c (- c))))
		      (when (> c fmax)
			(setf fmax c)))))
    (format t "~s~%Pfannkuchen(~s) = ~s~%" csum n fmax)))


(defun main ()  
  (let* ((args (cdr sb-ext:*posix-argv*))
         (n (parse-integer (car args))))
    (fannkuch n)))