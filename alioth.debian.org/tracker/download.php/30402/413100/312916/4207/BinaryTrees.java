/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/
 
   contributed by Jarkko Miettinen
   modified by Leonhard Holz
*/

public class BinaryTrees {

	private final static int minDepth = 4;
	private final static int threadCount = 4;
	private final static TreeGenerator[] threads = new TreeGenerator[threadCount];
	
	public static void main(String[] args)
	{
		int n = 0;
		if (args.length > 0) n = Integer.parseInt(args[0]);
		
		int maxDepth = (minDepth + 2 > n) ? minDepth + 2 : n;
		int stretchDepth = maxDepth + 1;
		
		int check = (TreeNode.bottomUpTree(0, stretchDepth)).itemCheck();
		System.out.println("stretch tree of depth " + stretchDepth + "\t check: " + check);
		
		TreeNode longLivedTree = TreeNode.bottomUpTree(0, maxDepth);
		
		for (int depth = minDepth; depth <= maxDepth; depth+=2 ) {

			check = 0;
			int iterations = 1 << (maxDepth - depth + minDepth);
			int length = iterations / threadCount;

			for (int i = 0; i < threadCount; i++) {
				threads[i] = new TreeGenerator(i * length, (i + 1) * length, depth);
				threads[i].start();
			}
			for (int i = 0; i < threadCount; i++) try {
				threads[i].join();
				check += threads[i].getCheckResult();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.out.println((iterations*2) + "\t trees of depth " + depth + "\t check: " + check);
		}	
		
		System.out.println("long lived tree of depth " + maxDepth + "\t check: "+ longLivedTree.itemCheck());
	}
	
	private static class TreeGenerator extends Thread
	{
		private int start;
		private int end;
		private int depth;
		private int result = 0;

		private TreeGenerator(int start, int end, int depth)
		{
			this.end = end;
			this.start = start;
			this.depth = depth;
		}
		
		private int getCheckResult()
		{
			return result;
		}
		
		public void run()
		{
			for (int i = start; i < end; i++) {
				result += TreeNode.bottomUpTree(i, depth).itemCheck() + TreeNode.bottomUpTree(-i, depth).itemCheck();
			}
		}
	}
	
	private static class TreeNode
	{
		private int item;
		private TreeNode left, right;
		
		private TreeNode(int item)
		{
			this.item = item;
		}
		
		private TreeNode(TreeNode left, TreeNode right, int item)
		{
			this.left = left;
			this.right = right;
			this.item = item;
		}
		
		private static TreeNode bottomUpTree(int item, int depth)
		{
			if (depth > 0) {
				return new TreeNode(bottomUpTree(2*item-1, depth-1), bottomUpTree(2*item, depth-1), item);
			} else {
				return new TreeNode(item);
			}
		}
		
		private int itemCheck()
		{
			if (left == null) {
				return item;
			} else {
				return item + left.itemCheck() - right.itemCheck();
			}
		}
	}
}
