#!/usr/bin/env perl

# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
#
# Contributed by David Eccles (gringer)

use strict;
use warnings;

local $/ = ">";
while(<>){
	chomp;
	my $lineSepPos = index($_, "\n");
	my $header = substr($_,0,$lineSepPos);
	if($header){
		print(">", $header, "\n");
		my $sequence = reverse(substr($_,$lineSepPos));
		$sequence =~ tr{ACGTUMRWSYKVHDBNacgtumrwsykvhdbn\n}
                       {TGCAAKYWSRMBDHVNTGCAAKYWSRMBDHVN}d;
		for(my $pos = 0; $pos < length($sequence);$pos += 60){
			print(substr($sequence, $pos, 60),"\n");
		}
	}
}
