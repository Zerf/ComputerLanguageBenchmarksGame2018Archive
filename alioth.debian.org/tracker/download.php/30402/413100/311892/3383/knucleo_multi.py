# The Computer Language Shootout
# http://shootout.alioth.debian.org/
#
# submitted by Ian Osgood
# modified by Sokolov Yura
# modified by bearophile
# modified by xfm for parallelization


from sys import stdin
from collections import defaultdict
from multiprocessing import Pool

def gen_freq(frame) :
    global sequence
    frequences = defaultdict(int)
    for ii in range(len(sequence)-frame+1) :
        frequences[sequence[ii:ii+frame]] += 1
    return frequences
    

def sort_seq(length):
    frequences = gen_freq(length)
    n= sum(frequences.values())

    l = sorted(list(frequences.items()), reverse=True, key=lambda seq_freq: (seq_freq[1],seq_freq[0]))

    return '\n'.join("%s %.3f" % (st, 100.0*fr/n) for st,fr in l)
    

def find_seq(s):
    t = gen_freq(len(s))
    return (s,t.get(s,0))

def prepare(file=stdin) :
    for line in file:
        if line[0:3] == ">TH":
            break
        
    seq = []
    for line in file:
        if line[0] in ">;":
            break
        seq.append( line[:-1] )
    return "".join(seq).upper()


def main():
    global sequence
    sequence = prepare()
    p=Pool()
    
    res = p.map(sort_seq,(1,2))
    for s in res : print (s+'\n')
    
    res= p.map(find_seq,"GGT GGTA GGTATT GGTATTTTAATT GGTATTTTAATTTATAGT".split())    
    print ("\n".join("{1:d}\t{0}".format(*s) for s in res))
            
if __name__=='__main__' :
    main()
