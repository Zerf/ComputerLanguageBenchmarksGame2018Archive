/* The Computer Language Benchmarks Game
   http://shootout.alioth.debian.org/

   contributed by Alex Burlyga
*/
#define _GNU_SOURCE
#include <inttypes.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef struct {
    void     *stack;
    jmp_buf  context;
    void     (*thread_func)(void);
    uint32_t running;
} thread_t;

#define NUMBER_OF_THREADS 503

uint32_t token = 0;
uint32_t token_count = 1000;

volatile static thread_t thread_list[NUMBER_OF_THREADS];

volatile static sig_atomic_t sigusr1_handler_called = 0;

int32_t current_thread = -1;
int32_t thread_count = 0;
int32_t in_thread = 0;
jmp_buf main_thread;

void init_threads();
void thread_yield();
void thread_join_all();
int  thread_create(void (*func)(void));

void thread_function(void) {
    printf("thread_function[%d], %d of %d\n", current_thread, token, token_count);
    while (token < token_count) {
        printf("thread_function[%d]: incrementing token\n", current_thread);
        token++;
        printf("thread_function[%d]: yeilding\n", current_thread);
        thread_yield();
        printf("thread_function[%d]: return from yielding, %d of %d\n", current_thread, token, token_count);
    }

    if (token == token_count) {
        printf("%d\n", current_thread + 1);
        token++;
    }
    printf("thread_function[%d]: exiting\n", current_thread);
}

int
main(int argc, char **argv) {

    if (argc > 1) {
        token_count = strtoll(argv[1], NULL, 0);
    }
    printf("main: token_count = %d\n", token_count);

    printf("main: initing threads\n");
    init_threads();

    for (int i = 0; i < NUMBER_OF_THREADS; i++) {
        if (thread_create(&thread_function)) {
            printf("Failed to create thread %d\n", i);
            return 1;
        }
    }

    thread_join_all();

    return 0;
}

void
init_threads() {
    for (int i = 0; i < NUMBER_OF_THREADS; i++) {
        thread_list[i].stack = NULL;
        thread_list[i].thread_func = NULL;
        memset(&thread_list[i].context, 0, sizeof(jmp_buf));
    }
}

void
sigusr1_handler() {
    printf("sigusr1_handler[%d]: thread_count=%d\n", current_thread, thread_count);
    if (setjmp(thread_list[thread_count].context)) {
        printf("sigusr1_handler[%d]: entering thread_func\n", current_thread);
        thread_list[current_thread].thread_func();
        thread_list[current_thread].running = 0;
        printf("sigusr1_handler[%d]: finished thread_func\n", current_thread);
        longjmp(main_thread, 1);
    }

    sigusr1_handler_called = 1;
    printf("sigusr1_handler[%d]: return, thread_count=%d\n", current_thread, thread_count);
    return;
}

int
thread_create(void (*func)(void)) {

    struct sigaction thread_handler, current_handler;
    stack_t thread_stack, current_stack;

    sigset_t sigs;
    sigset_t old_sigs;

    if (thread_count == NUMBER_OF_THREADS) {
        return 1;
    }

    thread_stack.ss_flags = 0;
    thread_stack.ss_size = SIGSTKSZ;
    thread_stack.ss_sp = malloc(SIGSTKSZ);
    if (thread_stack.ss_sp == NULL) {
        return 2;
    }

    if (sigaltstack(&thread_stack, &current_stack)) {
        return 3;
    }

    sigemptyset(&sigs);
    sigaddset(&sigs, SIGUSR1);
    sigprocmask(SIG_BLOCK, &sigs, &old_sigs);

    thread_handler.sa_handler = &sigusr1_handler;
    thread_handler.sa_flags = SA_ONSTACK;
    sigemptyset(&thread_handler.sa_mask);

    if (sigaction(SIGUSR1, &thread_handler, &current_handler)) {
        return 4;
    }

    sigusr1_handler_called = 0;
    kill(getpid(), SIGUSR1);
    sigfillset(&sigs);
    sigdelset(&sigs, SIGUSR1);
    while (!sigusr1_handler_called) {
        sigsuspend(&sigs);
    }

    sigaltstack(&current_stack, NULL);
    sigaction(SIGUSR1, &current_handler, NULL);
    sigprocmask(SIG_SETMASK, &old_sigs, NULL);

    thread_list[thread_count].running = 1;
    thread_list[thread_count].thread_func = func;
    thread_list[thread_count].stack = thread_stack.ss_sp;

    thread_count++;
    return 0;
}

void
thread_yield() {

    printf("thread_yield[%d]: start\n", in_thread ? current_thread : -1);
    if (in_thread) {
        printf("thread_yield[%d]: in_thread, storing context\n", current_thread);
        if (!setjmp(thread_list[current_thread].context)) {
            printf("thread_yield[%d]: jumping back to main_thread\n", current_thread);
            longjmp(main_thread, 1);
        }
    } else {
        printf("thread_yield[-1]: yielding on main thread\n");
        if (thread_count == 0) {
            printf("thread_yield[-1]: thread_count is 0\n");
            return;
        }

        printf("thread_yield[-1]: setjmp(main_thread)\n");
        if (setjmp(main_thread)) {
            printf("thread_yield[-1]: return to main_thread\n");
            in_thread = 0;
            if (!thread_list[current_thread].running) {
                printf("thread_yield[-1]: done running a thread %d, freeing\n", current_thread);
                free(thread_list[current_thread].stack);
                thread_count--;
                if (current_thread != thread_count) {
                    printf("thread_yield[-1]: compacting thread list: curr=%d, cnt=%d\n", current_thread, thread_count);
                    thread_list[current_thread] = thread_list[thread_count];
                }
                thread_list[thread_count].stack = NULL;
                thread_list[thread_count].thread_func = NULL;
                thread_list[thread_count].running = 0;
            } else {
                printf("thread_yield[-1]: thread %d still running, going back to main for dispatch\n", current_thread);
            }
        } else {
            printf("thread_yield[-1]: about to jump to a thread %d\n", (current_thread + 1) % thread_count);
            current_thread = (current_thread + 1) % thread_count;
            in_thread = 1;
            longjmp(thread_list[current_thread].context, 1);
        }
    }
}

void
thread_join_all()
{
    int remaining_thread_count = in_thread ? 1 : 0;
    while (thread_count > remaining_thread_count) {
        thread_yield();
    }
}
