/* The Computer Language Benchmarks Game
   http://shootout.alioth.debian.org/

   contributed by Alex Burlyga
*/
#define _GNU_SOURCE
#include <inttypes.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    void     *stack;
    jmp_buf  context;
    void     (*thread_func)(void);
    uint32_t running;
} thread_t;

#define NUMBER_OF_THREADS 503

uint32_t token = 0;
uint32_t token_count = 1000;

thread_t thread_list[NUMBER_OF_THREADS];

int32_t current_thread = -1;
int32_t thread_count = 0;
int32_t in_thread = 0;
jmp_buf main_thread;

void init_threads();
void thread_yield();
void thread_join_all();
int  thread_create(void (*func)(void));

void thread_function(void) {
    while (token < token_count) {
        token++;
        thread_yield();
    }

    if (token == token_count) {
        printf("%d\n", current_thread + 1);
        token++;
    }
}

int
main(int argc, char **argv) {

    if (argc > 1) {
        token_count = strtoll(argv[1], NULL, 0);
    }
    init_threads();

    for (int i = 0; i < NUMBER_OF_THREADS; i++) {
        if (thread_create(&thread_function)) {
            printf("Failed to create thread %d\n", i);
            return 1;
        }
    }

    thread_join_all();

    return 0;
}

void
init_threads() {
    for (int i = 0; i < NUMBER_OF_THREADS; i++) {
        thread_list[i].stack = NULL;
        thread_list[i].thread_func = NULL;
        memset(&thread_list[i].context, 0, sizeof(jmp_buf));
    }
}

void
sigusr1_handler() {
    if (setjmp(thread_list[thread_count].context)) {
        thread_list[current_thread].thread_func();
        thread_list[current_thread].running = 0;
        longjmp(main_thread, 1);
    }

    return;
}

int
thread_create(void (*func)(void)) {

    struct sigaction thread_handler, current_handler;
    stack_t thread_stack, current_stack;

    if (thread_count == NUMBER_OF_THREADS) {
        return 1;
    }

    thread_stack.ss_flags = 0;
    thread_stack.ss_size = SIGSTKSZ;
    thread_stack.ss_sp = malloc(SIGSTKSZ);
    if (thread_stack.ss_sp == NULL) {
        return 2;
    }

    if (sigaltstack(&thread_stack, &current_stack)) {
        return 3;
    }

    thread_handler.sa_handler = &sigusr1_handler;
    thread_handler.sa_flags = SA_ONSTACK;
    sigemptyset(&thread_handler.sa_mask);

    if (sigaction(SIGUSR1, &thread_handler, &current_handler)) {
        return 4;
    }

    if (raise(SIGUSR1)) {
        return 5;
    }

    sigaltstack(&current_stack, NULL);
    sigaction(SIGUSR1, &current_handler, NULL);

    thread_list[thread_count].running = 1;
    thread_list[thread_count].thread_func = func;
    thread_list[thread_count].stack = thread_stack.ss_sp;

    thread_count++;
    return 0;
}

void
thread_yield() {

    if (in_thread) {
        if (!setjmp(thread_list[current_thread].context)) {
            longjmp(main_thread, 1);
        }
    } else {
        if (thread_count == 0) {
            return;
        }

        if (setjmp(main_thread)) {
            in_thread = 0;
            if (!thread_list[current_thread].running) {
                free(thread_list[current_thread].stack);
                thread_count--;
                if (current_thread != thread_count) {
                    thread_list[current_thread] = thread_list[thread_count];
                }
                thread_list[thread_count].stack = NULL;
                thread_list[thread_count].thread_func = NULL;
                thread_list[thread_count].running = 0;
            }
        } else {
            current_thread = (current_thread + 1) % NUMBER_OF_THREADS;
            current_thread = (current_thread + 1) % thread_count;
            in_thread = 1;
            longjmp(thread_list[current_thread].context, 1);
        }
    }
}

void
thread_join_all()
{
    int remaining_thread_count = in_thread ? 1 : 0;
    while (thread_count > remaining_thread_count) {
        thread_yield();
    }
}
