
-- The Computer Language Benchmarks Game
-- http://shootout.alioth.debian.org
--
-- contributed by Pascal Obry on 2005/04/07
-- modified by Gautier de Montmollin
-- modified by Georg Bauhaus, Jonathan Parker (July 2011)

with Ada.Command_Line;
with GNAT.Float_Control;

procedure Fasta.Run is

   Homosapiens : constant Nucleotide_Set(0..3) :=
    (('a', 0.3029549426680), ('c', 0.1979883004921),
     ('g', 0.1975473066391), ('t', 0.3015094502008));

   IUB : constant Nucleotide_Set(0..14) :=
    (('a', 0.27), ('c', 0.12), ('g', 0.12), ('t', 0.27),
     ('B', 0.02), ('D', 0.02), ('H', 0.02), ('K', 0.02),
     ('M', 0.02), ('N', 0.02), ('R', 0.02), ('S', 0.02),
     ('V', 0.02), ('W', 0.02), ('Y', 0.02));

   ALU : constant String :=
     "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG" &
     "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA" &
     "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT" &
     "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA" &
     "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG" &
     "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC" &
     "AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA";

   N : constant Positive := Positive'Value (Ada.Command_Line.Argument (1));

   Runner : Environment;
begin
   GNAT.Float_Control.Reset;

   Make_Repeat_Fasta (">ONE Homo sapiens alu", ALU, N*2);
   Make_Random_Fasta (">TWO IUB ambiguity codes", IUB, N*3);
   Make_Random_Fasta (">THREE Homo sapiens frequency", Homosapiens, N*5);

end Fasta.Run;


with Ada.Finalization;

package Fasta is

   type Real is digits 15;

   type Nucleotide is record
      C : Character;
      P : Real;
   end record;

   Max_No_of_Nucleotides : constant := 15;

   subtype Nucleo_id_Type is Integer range 0 .. Max_No_of_Nucleotides-1;

   type Nucleotide_Set is array (Nucleo_id_Type range <>) of Nucleotide;

   procedure Make_Random_Fasta
     (Title       : in String; 
      Nucleotides : in Nucleotide_Set; 
      N           : in Positive);

   procedure Make_Repeat_Fasta 
     (Title : in String;
      S     : in String; 
      N     : in Positive);

private

   type Environment is new Ada.Finalization.Limited_Controlled
     with null record;

   overriding
   procedure Initialize (Active : in out Environment);

   overriding
   procedure Finalize (Active : in out Environment);

end Fasta;


with LCG_Random;
with Line_IO;

package body Fasta is

   package Real_Random_Nums is new LCG_Random (Real);
   use Real_Random_Nums;

   procedure Initialize (Active : in out Environment) is
   begin
      Line_IO.Open_Stdout;
   end Initialize;

   procedure Finalize (Active : in out Environment) is
   begin
      Line_IO.Close_Stdout;
   end Finalize;


   Line_Length : constant := 60;
   End_of_Line : String renames Line_IO.End_of_Line;

   subtype Line_End_Positions is Positive
      range Line_Length + 1 .. Line_Length + End_of_Line'Length;

   Line_Buffer : String (1 .. Line_Length + End_of_Line'Length);

   Nucleo_Cumulative : array (Nucleo_id_Type) of Nucleotide;

   procedure Make_Random_Fasta
     (Title       : in String; 
      Nucleotides : in Nucleotide_Set; 
      N           : in Positive)
   is
      function Random_Nucleotide return Character is
         r : constant Real := Gen_Random (1.0);
         Result : Character := 'J'; -- 'J' is output only in error.
      begin
         Choose_Random: 
         for i in Nucleo_Cumulative'Range loop
            if Nucleo_Cumulative(i).P > r then
               Result := Nucleo_Cumulative(i).C;
               exit Choose_Random;
            end if;
         end loop Choose_Random;
         return Result;
      end Random_Nucleotide;

      Sum  : Real;
      Remaining_Chars  : constant Natural := N mod Line_Length;
      No_of_Full_Lines : constant Natural := N  /  Line_Length;
   begin
      Line_IO.Print (Title & End_of_Line);

      Nucleo_Cumulative := (others => ('J', 2.0));
      for k in Nucleotides'Range loop
         Nucleo_Cumulative(k).C := Nucleotides(k).C;
      end loop;

      Sum := 0.0;
      for k in Nucleotides'Range loop
         Sum := Sum + Nucleotides(k).P;
         Nucleo_Cumulative(k).P := Sum;
      end loop;

      Line_Buffer(Line_End_Positions) := End_of_Line;

      for k in 1 .. No_of_Full_Lines loop
         for i in 1 .. Line_Length loop
            Line_Buffer(i) := Random_Nucleotide;
         end loop;
         Line_IO.Print (Line_Buffer);
      end loop;

      if Remaining_Chars > 0 then
         for i in 1 .. Remaining_Chars loop
            Line_Buffer(i) := Random_Nucleotide;
         end loop;
         Line_IO.Print (Line_Buffer(1 .. Remaining_Chars) & End_of_Line);
      end if;

   end Make_Random_Fasta;

   procedure Make_Repeat_Fasta 
     (Title : in String;
      S     : in String; 
      N     : in Positive) 
   is
      Todo  : Integer  := N;
      K     : Positive := S'First;
      S_App : constant String := S & S(S'First .. S'First + Line_Length);
   begin
      Line_IO.Print (Title & End_of_Line);

      while Todo > 0 loop
         Line_IO.Print (S_App (K .. K + Integer'Min(Todo, Line_Length) - 1));
         Line_IO.Print (End_of_Line);
         K := K + Line_Length;
         if K > S'Last then -- we are on the extra characters
            K := K - S'Length;
         end if;
         Todo := Todo - Line_Length;
      end loop;

   end Make_Repeat_Fasta;

end Fasta;

package Line_IO is

   procedure Print (Item : String);

   procedure Close_Stdout;

   procedure Open_Stdout;

   End_of_Line : constant String := (1 => ASCII.LF);

   pragma Inline (Print);

end Line_IO;


with Ada.Streams.Stream_IO;
with Unchecked_Conversion;

package body Line_IO is

   use Ada.Streams;

   Stdout : Stream_IO.File_Type;

   procedure Print (Item : String) is
      subtype Index is Stream_Element_Offset range
         Stream_Element_Offset(Item'First) .. Stream_Element_Offset(Item'Last);
      subtype XString is String (Item'Range);
      subtype XBytes is Stream_Element_Array (Index);
      function To_Bytes is new Unchecked_Conversion
        (Source => XString,
         Target => XBytes);
   begin
      Stream_IO.Write (File => Stdout, Item => To_Bytes (Item));
   end Print;

   procedure Close_Stdout is
   begin
      Stream_IO.Close (Stdout);
   end Close_Stdout;

   procedure Open_Stdout is
   begin
      Stream_IO.Open 
        (File => Stdout,
         Mode => Stream_IO.Out_File,
         Name => "/dev/stdout");
   end Open_Stdout;

end Line_IO;


generic

   type Real is digits <>;

package LCG_Random is

   function Gen_Random (Max : in Real) return Real;
   -- Linear congruential random number generator. 
   -- Period = 139_968, with output in [0.0, 1.0) if Max = 1.0.

end LCG_Random;

package body LCG_Random is

   pragma Assert (Real'Digits > 5);

   type Random_State is mod 2**32;

   State : Random_State := 42;

   type Signed is range
      -2**(Random_State'Size-1) .. 2**(Random_State'Size-1) - 1;

   function Gen_Random (Max : in Real) return Real is
      IM : constant := 139_968;
      IA : constant :=   3_877;
      IC : constant :=  29_573;
   begin
      State := (State * IA + IC) mod IM;
      return (Max * Real (Signed (State))) * (1.0 / Real (IM));
   end Gen_Random;

end LCG_Random;
