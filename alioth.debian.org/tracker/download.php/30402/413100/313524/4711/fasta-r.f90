! The Computer Language Benchmarks Game
! http://shootout.alioth.debian.org/
!
! Submitted by Andrei Jirnyi
! This is a modification the "fasta #2" contribution by Jason Blevins
!   (adapted from the C version by Petr Prokhorenkov)
!   (AJ: minor changes in I/O and the "repeat" routine.)
! Compilation:
!   ifort -fast -opt-prefetch=1 fasta-r.f90

module line_by_line
  interface
     function puts(str) bind(C, name='puts')
       use, intrinsic :: ISO_C_BINDING
       integer(kind=c_int) :: puts
       character(kind=c_char), dimension(*) :: str
     end function puts
  end interface
end module line_by_line

program fasta
  use line_by_line

  implicit none
  integer, parameter :: IM = 139968
  integer, parameter :: IA = 3877
  integer, parameter :: IC = 29573

  integer, parameter :: LINE_LEN = 60
  integer, parameter :: LOOKUP_SIZE = 4096
  real, parameter :: LOOKUP_SCALE = real(LOOKUP_SIZE - 1)

  type :: random_t
     integer :: state = 42
  end type random_t

  type :: amino_acid_t
     character(len=1) :: sym
     real :: prob
     real :: cprob_lookup = 0.d0
  end type amino_acid_t

  type(amino_acid_t), dimension(15) :: amino_acid = (/ &
       amino_acid_t('a', 0.27), &
       amino_acid_t('c', 0.12), &
       amino_acid_t('g', 0.12), &
       amino_acid_t('t', 0.27), &
       amino_acid_t('B', 0.02), &
       amino_acid_t('D', 0.02), &
       amino_acid_t('H', 0.02), &
       amino_acid_t('K', 0.02), &
       amino_acid_t('M', 0.02), &
       amino_acid_t('N', 0.02), &
       amino_acid_t('R', 0.02), &
       amino_acid_t('S', 0.02), &
       amino_acid_t('V', 0.02), &
       amino_acid_t('W', 0.02), &
       amino_acid_t('Y', 0.02)  &
       /)

  type(amino_acid_t), dimension(4) :: homo_sapiens = (/ &
       amino_acid_t('a', 0.3029549426680), &
       amino_acid_t('c', 0.1979883004921), &
       amino_acid_t('g', 0.1975473066391), &
       amino_acid_t('t', 0.3015094502008)  &
       /)

  character(len=*), parameter :: alu = &
       "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTG" // &
       "GGAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGA" // &
       "GACCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAA" // &
       "AATACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAAT" // &
       "CCCAGCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAAC" // &
       "CCGGGAGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTG" // &
       "CACTCCAGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA"

  character(len=60) :: arg
  integer :: n
  type(random_t) :: rand

  if (command_argument_count() > 0) then
     call get_command_argument(1, arg)
     read(arg, *) n
  else
     n = 512
  end if

  call makeRepeatFasta('ONE','Homo sapiens alu',alu,n*2)
  call randomize(amino_acid, ">TWO IUB ambiguity codes", n*3, rand)
  call randomize(homo_sapiens, ">THREE Homo sapiens frequency", n*5, rand)

contains

  ! Special version with result rescaled to LOOKUP_SCALE.
  function random_next_lookup(random)
    type(random_t), intent(inout) :: random
    real :: random_next_lookup
    random%state = mod(random%state*IA + IC, IM)
    random_next_lookup = random%state * (LOOKUP_SCALE / IM)
  end function random_next_lookup

  subroutine makeRepeatFasta(id,desc,s,n)
     character(len=*), intent(in) :: id
     character(len=*), intent(in) :: desc
     character(len=*), intent(in) :: s
     integer, intent(in) :: n
     integer :: j, k, l, kn, istat
     integer, parameter :: length = 60
     character(len=LINE_LEN) :: title
     character(len=1) :: line(LINE_LEN+1)
     equivalence(title, line)
     intrinsic len

     write(title,'(4a)') '>',id,' ',desc
     line(len(trim(title))+1) = achar(0)
     istat = puts(line)

     k = 1; kn = len(s)
     
     j = 0 ! in output line
     k = 0 ! in repeat seq
     l = 0 ! generated count
     do
        j = j+1
        k = k+1
        l = l+1
        if(k>kn) k=1
        line(j) = s(k:k)
        if(l == n) then
           line(j+1) = achar(0)
           istat = puts(line)
           exit
        end if
        if(j == LINE_LEN) then
           j = 0
           line(LINE_LEN+1) = achar(0)
           istat = puts(line)
        end if
     end do

  end subroutine makeRepeatFasta


  subroutine fill_lookup(lookup, amino_acid)
    integer, dimension(:), intent(out) :: lookup
    type(amino_acid_t), dimension(:), intent(inout) :: amino_acid
    real :: p
    integer :: i, j

    p = 0.d0

    do i = 1, size(amino_acid)
       p = p + amino_acid(i)%prob
       amino_acid(i)%cprob_lookup = p*LOOKUP_SCALE
    end do

    ! Prevent rounding error.
    amino_acid(size(amino_acid))%cprob_lookup = LOOKUP_SIZE - 1.d0

    j = 1
    do i = 1, LOOKUP_SIZE
       do while (amino_acid(j)%cprob_lookup < i - 1)
          j = j + 1
       end do
       lookup(i) = j
    end do
    
  end subroutine fill_lookup

  subroutine randomize(amino_acid, title, n, rand)
    type(amino_acid_t), dimension(:), intent(inout) :: amino_acid
    character(len=*), intent(in) :: title
    integer, intent(in) :: n
    type(random_t), intent(inout) :: rand
    integer, dimension(LOOKUP_SIZE) :: lookup
    character(len=LINE_LEN) :: line_buffer
    integer :: i, j, u, istat
    real :: r

    character(len=1) :: line(LINE_LEN+1)

    call fill_lookup(lookup, amino_acid)

    print '(a)', title

    j = 1
    do i = 1, n
       r = random_next_lookup(rand)
       u = lookup(int(r)+1)
       do while (amino_acid(u)%cprob_lookup < r)
          u = u + 1
       end do

       line(j) = amino_acid(u)%sym

       if (j == LINE_LEN) then
           line(j+1) = achar(0)
           istat = puts(line)
          j = 1
       else
          j = j + 1
       end if
    end do
    if (j > 1) then
       line(j) = achar(0)
       istat = puts(line)
    end if
  end subroutine randomize

end program fasta
