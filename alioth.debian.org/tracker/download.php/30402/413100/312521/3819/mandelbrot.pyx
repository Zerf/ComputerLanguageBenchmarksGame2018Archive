# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
#
# contributed by Tupteq
# modified by Simon Descarpentries
# modified by Ivan Baldin
# Cython version by Yannick Versley

import sys
from array import array
#from multiprocessing import Pool

cdef int size
cdef double step

def do_row(double ci):
    cdef double zr, zi, cr
    cdef double zr2, zi2
    cdef int i, x, byte_acc, offset

    result = array('B')
    for x from 7<=x<size by 8:
        byte_acc = 0
        for 0<=offset<8:
            zr=zi=zr2=zi2=0.0
            cr = step*(x-offset)-1.5
            for i from 0<=i<50:
                zi = 2.0*zr*zi + ci
                zr = zr2-zi2 + cr
                zr2 = zr*zr
                zi2 = zi*zi
                if zr2+zi2 >= 4.0:
                    break
            else:
                byte_acc += 1 << offset

        result.append(byte_acc)

    if x != size + 7:
        result.append(byte_acc)

    return result.tostring()

def main(out):
    global step
    out.write('P4\n%d %d\n' % (size, size))

    step = 2.0 / size
    for row in map(do_row, [-1.0+<double>y*step for y in xrange(size)]):
        out.write(row)

if __name__ == '__main__':
    size = int(sys.argv[1])
    main(sys.stdout)
