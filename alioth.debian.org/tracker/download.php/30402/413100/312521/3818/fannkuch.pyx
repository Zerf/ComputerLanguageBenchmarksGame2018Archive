# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
#
# contributed by Sokolov Yura
# modified by Tupteq
cimport numpy
import numpy

def fannkuch(int n):
    cdef numpy.ndarray[int,ndim=1,mode="c"] count
    cdef numpy.ndarray[int,ndim=1,mode="c"] perm
    cdef numpy.ndarray[int,ndim=1,mode="c"] perm1
    cdef numpy.ndarray[int,ndim=1,mode="c"] perm2
    cdef int m,r,tmp,i,k
    cdef int flips_count,max_flips,check
    count = numpy.array(range(1, n+1))
    max_flips = 0
    m = n-1
    r = n
    check = 0
    perm1 = numpy.array(range(n))
    perm = perm1.copy()
    perm2=numpy.zeros(n,numpy.int32)

    while 1:
        if check < 30:
            print "".join([str(i+1) for i in perm1])
            check += 1

        while r != 1:
            count[r-1] = r
            r -= 1

        if perm1[0] != 0 and perm1[m] != m:
            for i from 0<=i<n:
                perm[i]=perm1[i]
            flips_count = 0
            k = perm[0]
            while k:
                for i from 0<=i<=k/2:
                    tmp=perm[k-i]
                    perm[k-i]=perm[i]
                    perm[i]=tmp
                flips_count += 1
                k = perm[0]

            if flips_count > max_flips:
                max_flips = flips_count

        while r != n:
            tmp=perm1[0]
            for i from 0<=i<r:
                perm1[i]=perm1[i+1]
            perm1[r]=tmp
            count[r] -= 1
            if count[r] > 0:
                break
            r += 1
        else:
            return max_flips

def main():
    from sys import argv
    n = int(argv and argv[1] or 1)
    print "Pfannkuchen(%d) = %d\n" % (n, fannkuch(n)),

if __name__=="__main__":
    main()
