# The Computer Language Benchmarks Game
# http://shootout.alioth.debian.org/
# Contributed by Sebastien Loisel
# Fixed by Isaac Gouy
# Sped up by Josh Goldfoot
# Dirtily sped up by Simon Descarpentries
# Cython version by Yannick Versley

cimport numpy
import numpy

from math            import sqrt
from sys             import argv

cdef inline double eval_A(int i, int j):
    return 1.0 / ((i + j) * (i + j + 1) / 2.0 + i + 1.0)

cdef numpy.ndarray eval_A_times_u(numpy.ndarray u):
    cdef int i
    cdef numpy.ndarray[double] result=numpy.zeros(u.shape[0])
    for i from 0<=i<u.shape[0]:
        result[i]=part_A_times_u(i,u.shape[0],<double *>u.data)
    return result

cdef numpy.ndarray eval_At_times_u(numpy.ndarray u):
    cdef int i
    cdef numpy.ndarray[double] result=numpy.zeros(u.shape[0])
    for i from 0<=i<u.shape[0]:
        result[i]=part_At_times_u(i,u.shape[0],<double *>u.data)
    return result

cdef numpy.ndarray eval_AtA_times_u(u):
    return eval_At_times_u(eval_A_times_u(u))

cdef double part_A_times_u(int i, int n, double *u):
    cdef double partial_sum=0.0
    cdef int j
    for j from 0<=j<n:
        partial_sum += eval_A(i, j)*u[j]
    return partial_sum

cdef double part_At_times_u(int i, int n, double *u):
    cdef double partial_sum=0.0
    cdef int j
    for j from 0<=j<n:
        partial_sum += eval_A (j, i) * u[j]
    return partial_sum

def main():
    cdef numpy.ndarray[double] u
    #cdef numpy.ndarray[double] v
    cdef int n
    cdef int i
    cdef double ud, ve, vBv, vv

    n = int(argv[1])
    u = numpy.ones(n)

    for dummy in xrange (10):
        v = eval_AtA_times_u (u)
        u = eval_AtA_times_u (v)

    vBv = vv = 0

    for i from 0<=i<n:
        ue=u[i]
        ve=v[i]
        vBv += ue*ve
        vv  += ve*ve

    print "%0.9f" % (sqrt(vBv/vv))

if __name__ == '__main__':
    main()
