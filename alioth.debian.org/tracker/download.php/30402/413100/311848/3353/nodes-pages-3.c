/*
 * Francesco Abbate, fr4nko at ubuntuforums
 */

#include <stdlib.h>
#include <stdio.h>

const size_t	LINE_SIZE = 64;
const size_t	PAGE_SIZE = 8 * 4 * 1024;

struct node
{
  int i;
  struct node *left;
  struct node *right;
};

struct page {
  struct page *next;
  char data[1];
};

struct pool {
  struct node *current;
  struct node *end_mark;
  struct page *pages;
};

int
node_check(const struct node *n)
{
  int acc = n->i;

  if (n->left)
    {
      const struct node *l = n->left, *r = n->right;

      acc += l->i;
      if (l->left)
	acc += node_check (l->left) - node_check (l->right);

      acc -= r->i;
      if (r->left)
	acc -= node_check (r->left) - node_check (r->right);
    }

  return acc;
}

void
pool_get_new_page (struct pool *pool)
{
  struct page *new_page = malloc (PAGE_SIZE);
  char *ptr;

  if (new_page == NULL)
    exit(1);

  new_page->next = pool->pages;
  pool->pages = new_page;

  ptr = (char *) new_page;
  pool->current  = (struct node *) (ptr + sizeof(void *));
  pool->end_mark = (struct node *) (ptr + PAGE_SIZE);
}

struct node *
node_get_avail (struct pool *pool)
{
  struct node *p = pool->current;

  if (p + 1 > pool->end_mark)
    {
      pool_get_new_page (pool);
      p = pool->current;
    }

  pool->current ++;

  return p;
}

struct node *
node_get_avail_2 (struct pool *pool)
{
  struct node *p = pool->current;

  if (p + 2 > pool->end_mark)
    {
      pool_get_new_page (pool);
      p = pool->current;
    }

  pool->current += 2;

  return p;
}

void
pool_init (struct pool *pool)
{
  pool->pages = NULL;
  pool_get_new_page (pool);
}

void
pool_dealloc (struct pool *pool)
{
  struct page *p = pool->pages;

  while (p)
    {
      struct page *next = p->next;
      free (p);
      p = next;
    }
}

struct node *
make (int i, int depth, struct pool *pool)
{
  struct node *curr = node_get_avail (pool);

  curr->i = i;

  if (depth <= 0)
    {
      curr->left  = NULL;
      curr->right = NULL;
    }
  else if (depth == 1)
    {
      struct node *sub;

      sub = node_get_avail_2 (pool);
      sub->i = 2*i - 1;
      sub->left = sub->right = NULL;
      curr->left = sub;

      sub ++;
      sub->i = 2*i;
      sub->left = sub->right = NULL;
      curr->right = sub;
    }
  else 
    {
      curr->left  = make (2*i-1, depth - 1, pool);
      curr->right = make (2*i  , depth - 1, pool);
    }

  return curr;
}

int
main(int argc, char *argv[])
{
  struct pool long_lived_pool[1];
  int min_depth = 4;
  int req_depth = (argc == 2 ? atoi(argv[1]) : 10);
  int max_depth = (req_depth > min_depth + 2 ? req_depth : min_depth + 2);
  int stretch_depth = max_depth+1;

  /* Alloc then dealloc stretchdepth tree */
  {
    struct pool store[1];
    struct node *curr;

    pool_init (store);
    curr = make (0, stretch_depth, store);
    printf ("stretch tree of depth %i\t check: %i\n", stretch_depth, 
	    node_check (curr));
    pool_dealloc (store);
  }

  pool_init (long_lived_pool);

  {
    struct node *long_lived_tree = make(0, max_depth, long_lived_pool);

    /* buffer to store output of each thread */
    char *outputstr = (char*) malloc(LINE_SIZE * (max_depth +1) * sizeof(char));
    int d;

#pragma omp parallel for
    for (d = min_depth; d <= max_depth; d += 2)
      {
        int iterations = 1 << (max_depth - d + min_depth);
        int c = 0, i;

        for (i = 1; i <= iterations; ++i)
	  {
	    struct pool store[1];
	    struct node *a, *b;

	    pool_init (store);
	    a = make ( i, d, store);
	    b = make (-i, d, store);
            c += node_check (a) + node_check (b);
	    pool_dealloc (store);
        }
	
	/* each thread write to separate location */
	sprintf(outputstr + LINE_SIZE * d, "%d\t trees of depth %d\t check: %d\n", (2 * iterations), d, c);
      }

    /* print all results */
    for (d = min_depth; d <= max_depth; d += 2)
      printf("%s", outputstr + (d * LINE_SIZE) );
    free(outputstr);

    printf ("long lived tree of depth %i\t check: %i\n", max_depth, 
	    node_check (long_lived_tree));

    /* no need to free long_lived_pool since the program ends */

    return 0;
  }
}
