/* The Computer Language Benchmarks Game
http://shootout.alioth.debian.org/
Contributed by Dmitry Vyukov
chameneos-redux

Kernel thread is created for each chameneous.
Atomic compare-and-swap primitive is used 
for meeting place state manipulation.
*/

#define _GNU_SOURCE
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>
#include <pthread.h>
#include <sched.h>

#define CL_SIZE 64

void* cache_aligned_malloc(size_t sz)
{
    char*                       mem;
    char*                       res;
    void**                      pos;

    mem = (char*)malloc(sz + 2 * CL_SIZE);
    if (mem == 0)
        exit(1);
    res = (char*)((uintptr_t)(mem + CL_SIZE) & ~(CL_SIZE - 1));
    pos = (void**)(res - sizeof(void*));
    pos[0] = mem;
    return res;
}

void cache_aligned_free(void* res)
{
    void*                       mem;
    void**                      pos;

    assert(((uintptr_t)res & (CL_SIZE - 1)) == 0);
    pos = (void**)((char*)res - sizeof(void*));
    mem = pos[0];
    free(mem);
}

enum color_t
{
    color_blue,
    color_red,
    color_yellow,
};

char const* color_names[] = {"blue", "red", "yellow"};

enum color_t color_complement(enum color_t c1, enum color_t c2)
{
   switch (c1)
   {
   case color_blue:
      switch (c2)
      {
      case color_blue:      return color_blue;
      case color_red:       return color_yellow;
      case color_yellow:    return color_red;
      }
   case color_red:
      switch (c2)
      {
      case color_blue:      return color_yellow;
      case color_red:       return color_red;
      case color_yellow:    return color_blue;
      }
   case color_yellow:
      switch (c2)
      {
      case color_blue:      return color_red;
      case color_red:       return color_blue;
      case color_yellow:    return color_yellow;
      }
   }
   assert(0);
   return 0;
}

void print_colors()
{
    enum color_t                c1;
    enum color_t                c2;
    enum color_t                c3;

    for (c1 = color_blue; c1 <= color_yellow; c1 += 1)
    {
        for (c2 = color_blue; c2 <= color_yellow; c2 += 1)
        {
            c3 = color_complement(c1, c2);
            printf("%s + %s -> %s\n",
                color_names[c1], color_names[c2], color_names[c3]);
        }
    }
    printf("\n");
}

char const* spell_number(size_t n)
{
    static char                 buf [128];
    static char const*          numbers [] = {
        " zero", " one", " two",   " three", " four",
        " five", " six", " seven", " eight", " nine"};

    size_t                      tokens [32];
    size_t                      token_count;
    char const*                 tok;
    char*                       pos;

    token_count = 0;
    do
    {
        tokens[token_count] = n % 10;
        token_count += 1;
        n /= 10;
    }
    while (n);

    pos = buf;
    while (token_count)
    {
        token_count -= 1;
        tok = numbers[tokens[token_count]];
        while (tok[0])
            pos++[0] = tok++[0];
    }
    pos[0] = 0;
    return buf;
}

struct meeting_place_t
{
    uintptr_t volatile          state;
};

#define CHAMENEOS_IDX_MASK      0xFF
#define MEET_COUNT_SHIFT        8

struct chameneos_t
{
    enum color_t                color;
    size_t                      meet_count;
    size_t                      meet_same_count;
    int volatile                meeting_completed;
    struct meeting_place_t*     place;
    struct chameneos_t**        chameneoses;
    size_t                      id;
    int                         is_smp;
    pthread_t                   thread;
    pthread_attr_t              thread_attr;
};

void* chameneos_func(void* ctx)
{
    struct chameneos_t*         chameneos;
    struct chameneos_t**        chameneoses;
    struct chameneos_t*         peer;
    size_t                      my_id;
    size_t                      is_same;
    size_t                      spin_count;
    uintptr_t volatile*         state_p;
    uintptr_t                   state;
    uintptr_t                   peer_idx;
    uintptr_t                   xchg;
    uintptr_t                   prev;
    enum color_t                new_color;
    int                         is_smp;

    chameneos = (struct chameneos_t*)ctx;
    chameneoses = chameneos->chameneoses;
    state_p = &chameneos->place->state;
    my_id = chameneos->id;
    is_smp = chameneos->is_smp;

    state = state_p[0];
    for (;;)
    {
        peer_idx = state & CHAMENEOS_IDX_MASK;
        if (peer_idx)
            xchg = state - peer_idx - (1 << MEET_COUNT_SHIFT);
        else if (state)
            xchg = state | my_id;
        else
            break;
        prev = __sync_val_compare_and_swap(state_p, state, xchg);
        if (prev == state)
        {
            if (peer_idx)
            {
                is_same = (peer_idx == my_id);
                peer = chameneoses[peer_idx - 1];
                new_color = color_complement(chameneos->color, peer->color);
                peer->color = new_color;
                peer->meet_count += 1;
                peer->meet_same_count += is_same;
                peer->meeting_completed = 1;
                chameneos->color = new_color;
                chameneos->meet_count += 1;
                chameneos->meet_same_count += is_same;
            }
            else
            {
                if (is_smp)
                {
                    spin_count = 20000;
                    while (chameneos->meeting_completed == 0)
                    {
                        if (spin_count)
                            spin_count -= 1;
                        else
                            sched_yield();
                    }
                }
                else
                {
                    while (chameneos->meeting_completed == 0)
                    {
                        sched_yield();
                    }
                }
                chameneos->meeting_completed = 0;
                state = state_p[0];
            }
        }
        else
        {
            state = prev;

        }
    }
    return 0;
}

int get_affinity(cpu_set_t* affinity)
{
    cpu_set_t                   active_cpus;
    size_t                      cpu_count;
    size_t            i;
    
    CPU_ZERO(&active_cpus);
    sched_getaffinity(0, sizeof(active_cpus), &active_cpus);
    cpu_count = 0;
    CPU_ZERO(affinity);
    for (i = 0; i != CPU_SETSIZE; i += 1)
    {
        if (CPU_ISSET(i, &active_cpus))
        {
            cpu_count += 1;
            CPU_SET(i, affinity);
            if (cpu_count > 1)
                break;
        }
    }
    return cpu_count > 1;
}

void init_chameneos(struct chameneos_t** chameneoses, struct meeting_place_t* place,
    size_t i, enum color_t color, int is_smp, cpu_set_t* affinity)
{
    chameneoses[i] = (struct chameneos_t*)
        cache_aligned_malloc(sizeof(struct chameneos_t));
    chameneoses[i]->place = place;
    chameneoses[i]->chameneoses = chameneoses;
    chameneoses[i]->id = i + 1;
    chameneoses[i]->is_smp = is_smp;
    chameneoses[i]->meet_count = 0;
    chameneoses[i]->meet_same_count = 0;
    chameneoses[i]->color = color;
    chameneoses[i]->meeting_completed = 0;
    if (pthread_attr_init(&chameneoses[i]->thread_attr))
        exit(1);
    if (pthread_create(&chameneoses[i]->thread, &chameneoses[i]->thread_attr,
        chameneos_func, chameneoses[i]))
        exit(1);
    if (is_smp)
        pthread_setaffinity_np(chameneoses[i]->thread,
        sizeof(cpu_set_t), affinity);
}

void run(enum color_t* initial_colors,
         size_t chameneos_count, size_t meet_count)
{
    struct meeting_place_t*     place;
    struct chameneos_t**        chameneoses;
    cpu_set_t                   affinity;
    int                         is_smp;
    size_t                      total_meet_count;
    size_t                      i;

    for (i = 0; i != chameneos_count; i += 1)
        printf(" %s", color_names[initial_colors[i]]);
    printf("\n");

    is_smp = get_affinity(&affinity);

    place = (struct meeting_place_t*)
        cache_aligned_malloc(sizeof(struct meeting_place_t));
    place->state = meet_count << MEET_COUNT_SHIFT;
    chameneoses = (struct chameneos_t**)
        cache_aligned_malloc(chameneos_count * sizeof(struct chameneos_t*));
    for (i = 0; i != chameneos_count; i += 1)
    {
        init_chameneos(chameneoses, place, i,
            initial_colors[i], is_smp, &affinity);
    }

    for (i = 0; i != chameneos_count; i += 1)
    {
        pthread_join(chameneoses[i]->thread, 0);
        pthread_attr_destroy(&chameneoses[i]->thread_attr);
    }

    total_meet_count = 0;
    for (i = 0; i != chameneos_count; i += 1)
    {
        total_meet_count += chameneoses[i]->meet_count;
        printf("%u%s\n", chameneoses[i]->meet_count,
            spell_number(chameneoses[i]->meet_same_count));
        cache_aligned_free(chameneoses[i]);
    }
    printf("%s\n\n", spell_number(total_meet_count));

    cache_aligned_free(chameneoses);
    cache_aligned_free(place);
}

int main(int argc, char** argv)
{
    enum color_t                initial_colors1 [] = 
        {color_blue, color_red, color_yellow};

    enum color_t                initial_colors2 [] = 
        {color_blue, color_red, color_yellow, color_red, color_yellow,
        color_blue, color_red, color_yellow, color_red, color_blue};

    size_t                      chameneos_count;
    size_t                      meet_count;
    
    meet_count = 600;
    if (argc > 1)
        meet_count = atoi(argv[1]);

    print_colors();

    chameneos_count = sizeof(initial_colors1)/sizeof(initial_colors1[0]);
    run(initial_colors1, chameneos_count, meet_count);

    chameneos_count = sizeof(initial_colors2)/sizeof(initial_colors2[0]);
    run(initial_colors2, chameneos_count, meet_count);

    return 0;
}


