! The Computer Language Benchmarks Game
! http://shootout.alioth.debian.org/
!
! Original submission by Andrei Jirnyi
!  thanks to Gilbert Brietzke for the previous Fortran code
!  and to other submitters for their work

program fannkuch
  use iso_fortran_env
  implicit none

  integer :: NP, maxfk, cksum = 0, current = 0
  integer(8) ::NQ
  integer(1), allocatable :: blk(:,:)
  logical ::saveblk = .true.
  character(len=2) :: arg
  character(len=20) :: out
  
  call get_command_argument(1,arg)
  read(arg,'(I2)') NP
  
  NQ = factorial(NP)/NP+NP
  allocate(blk(NP,NQ))
  
  call fillblock(NP)
  
  write(out,'(i15)') cksum-2
  write(*,'(a)') trim(adjustl(out))
  write(*,'(3a,i3)') 'Pfannkuchen(',trim(adjustl(arg)),') =',maxfk

contains

  function factorial(n)
    integer n, factorial, i
    factorial = 1
    do i=1,n
       factorial = factorial*i
    end do
  end function factorial
  
  subroutine fillblock(NP)
    integer :: NP
    integer(8) :: bsize
    integer :: i,j,k
    integer(1), dimension(NP) :: base

    base = [(i,i=1,NP)]
    blk(:,1) = base;

    k = 2;
    bsize = 1;
    maxfk = 0;
    do i=2,NP       ! rot count
       current = i
       if(i==NP) saveblk = .false.
       bsize = bsize*(i-1)
       do j=1,i-1   ! blk count
          base(1:i) = cshift(base(1:i),1)
          ! k is out of bounds for i==NP, it's OK since saveblk=.f.
          call procblk(blk(1,k),blk(1,1),base,bsize)
          k = k+bsize
       end do
       base(1:i) = cshift(base(1:i),1)
    end do
  end subroutine fillblock

  subroutine procblk(blk,mult,base,bsize)
    integer(8) :: bsize,i
    integer(1) :: blk(NP*bsize),mult(NP*bsize),base(NP)
    integer(1) :: line(NP), t, t1
    integer :: j, ii, iii

    if(saveblk) blk=base(mult)
    
    !$omp  parallel do default(shared) &
    !$omp& private(i,j,t,line,t1,ii,iii) &
    !$omp& reduction(max: maxfk) reduction(+: cksum) &
    !$omp& schedule(guided) if(current>9)
    do iii=0,bsize-1
       i = iii*NP
       line = base(mult(i+1:i+NP))
       j = 0
       t = line(1)
       do while(t /= 1)  ! flip till line(1)==1
          do ii=1,ishft(t,-1)
             t1 = line(ii)
             line(ii) = line(t+1-ii)
             line(t+1-ii) = t1
          end do
          t = line(1)
          j = j+1
       end do
       maxfk = max(maxfk, j)
       cksum = cksum+j*(1-ishft(mod(iii,2),1))
    end do
    !$omp end parallel do

  end subroutine procblk
  
end program fannkuch

