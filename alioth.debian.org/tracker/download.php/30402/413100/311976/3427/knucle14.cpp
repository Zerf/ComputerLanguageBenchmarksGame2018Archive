// The Computer Language Shootout
// http://shootout.alioth.debian.org/

// Copy many tricks from Java & C++ entry, contributed by James McIlree
// Contributed by The Anh Tran


#include <omp.h>
#include <sched.h>

#include <algorithm>
#include <vector>
#include <iostream>
#include <sstream>

#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/hash_policy.hpp>

#include <boost/algorithm/string/case_conv.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/format.hpp>
#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH


typedef unsigned int	uint;

int const MAX_CORE = 16;
uint const SEED = 183; //183 193 405 <= zero collision for hashing algorithm


// Hash_table key type, with key's length = reading_frame_size
template <int frm_sz>
struct Key_T
{
	uint32_t	hash_value;
	char		key[frm_sz +1];

	Key_T() 				{	memset(this, 0, sizeof(*this));		}
	Key_T(char const * str)	{	ReHash (str);	}

	void 
	ReHash(char const *str)
	{
		// naive hashing algorithm.
		hash_value = 0;

		for (int i = 0; i < frm_sz; ++i)
		{
			key[i] = str[i];
			hash_value = (hash_value * SEED) + str[i];
		}
		key[frm_sz] = 0;
	}


	// Hash functor Hash<HKey_T>
	uint 
	operator() (const Key_T &k) const	{	return k.hash_value;	}


	// Comparison functor equal_to<HKey_T>(Left, Right)
	bool 
	operator() (const Key_T &k1, const Key_T &k2) const
	{
		assert(false);
		return false;
	}
};


// Specialized comparison functor, with each frame size
template<>
bool 
Key_T<1>::operator() (Key_T const &key1, Key_T const &key2) const 
{
    return key1.key[0] == key2.key[0];
}

template<>
bool 
Key_T<2>::operator() (Key_T const &key1, Key_T const &key2) const 
{
    return *((uint16_t*)key1.key) == *((uint16_t*)key2.key);
}

template<>
bool 
Key_T<3>::operator() (Key_T const &key1, Key_T const &key2) const 
{
    return *((uint32_t*)key1.key) == *((uint32_t*)key2.key);
}

template<>
bool 
Key_T<4>::operator() (Key_T const &key1, Key_T const &key2) const 
{
    return *((uint32_t*)key1.key) == *((uint32_t*)key2.key);
}

template<>
bool 
Key_T<6>::operator() (Key_T const &key1, Key_T const &key2) const 
{
    uint32_t* k31 = (uint32_t*)key1.key;
    uint32_t* k32 = (uint32_t*)key2.key;

    uint16_t* k11 = (uint16_t*)key1.key;
    uint16_t* k12 = (uint16_t*)key2.key;

    return (	k31[0] == k32[0] && k11[2] == k12[2]	);
}

template<>
bool 
Key_T<12>::operator() (Key_T const &key1, Key_T const &key2) const 
{
    uint64_t* k61 = (uint64_t*)key1.key;
    uint64_t* k62 = (uint64_t*)key2.key;

    uint32_t* k31 = (uint32_t*)key1.key;
    uint32_t* k32 = (uint32_t*)key2.key;

    return (k61[0] == k62[0] && k31[2] == k32[2]);
}

template<>
bool 
Key_T<18>::operator() (Key_T const &key1, Key_T const &key2) const 
{
    uint64_t* k61 = (uint64_t*)key1.key;
    uint64_t* k62 = (uint64_t*)key2.key;

    uint16_t* k11 = (uint16_t*)key1.key;
    uint16_t* k12 = (uint16_t*)key2.key;

    return (	k61[0] == k62[0] 
    		&& 	k61[1] == k62[1] 
    		&& 	k11[8] == k12[8]	);
}


// Game's rule: function to update hashtable. Build hashtable from zero element.
template <int hash_len, bool MT, typename Input_T, typename HTable_T>
void 
calculate_frequency(Input_T const &input, HTable_T& hash_table)
{
	hash_table.clear();
	int	const total_length = static_cast<int>(input.size() - hash_len +1);

	typedef typename Input_T::const_pointer	Ite_T;
	Ite_T const	ite_beg	= &(input[0]);
	Ite_T const	ite_end	= &(input[0]) + total_length;

	typename HTable_T::key_type key;

	if (MT) // multi thread
	{
		static int char_done[hash_len] = {0};
		int const chunk_sz = std::max(512, std::min(1024*1024, total_length / omp_get_num_threads() / 128));
		int ichunk;

		for(int offset = 0; offset < hash_len; ++offset)
		{
			// Fetch task. Each thread hashes a block, which block size = chunk
			while ( (ichunk = __sync_fetch_and_add(char_done + offset, chunk_sz)) < total_length )
			{
				Ite_T ite	= ite_beg + ichunk + offset;
				Ite_T end	= std::min(ite_beg + ichunk + chunk_sz, ite_end);
			
				for (; ite < end; ite += hash_len)
				{
					key.ReHash(ite);
					++(hash_table[key]);
				}
			}
		}
	}
	else // single thread
	{
		for(int offset = 0; offset < hash_len; ++offset)
		{
			for (Ite_T index = ite_beg + offset; index < ite_end; index += hash_len)
			{
				key.ReHash(index);
				++(hash_table[key]);
			}
		}
	}
}


// Build a hash_table, count all key with hash_len = 1, 2
// write the code and percentage frequency
template <int hash_len, typename Input_T>
void 
write_frequencies(Input_T const &input, std::ostream &output)
{
	typedef Key_T<hash_len>			HKey_T;
	typedef __gnu_pbds::cc_hash_table	<
											HKey_T,		// key type
											uint32_t,	// map type
											HKey_T,		// hash functor
											HKey_T		// equal_to functor
										> 	HTable_T;


	static HTable_T hash_table[MAX_CORE];

	// parallel hashing. Each thread updates its own hash_table.
	if (omp_get_num_threads() > 1)
		calculate_frequency<hash_len, true>(input, hash_table[omp_get_thread_num()]);
	else	// single thread hashing
		calculate_frequency<hash_len, false>(input, hash_table[omp_get_thread_num()]);


	// only the last thread, reaching this code block, to process result
	static int thread_passed = 0;
	if (__sync_add_and_fetch(&thread_passed, 1) == omp_get_num_threads())
	{
		// merge thread local results to main hash_table
		HTable_T &merge_table (hash_table[0]);

		for (int i = 1; i < omp_get_num_threads(); ++i)
		{
			foreach (typename HTable_T::value_type const & e, hash_table[i])
				merge_table[e.first] += e.second;
		}
		
	
		typedef std::pair<HKey_T, uint>	HValue_T;
		typedef std::vector<HValue_T> 	List_T;

		// Copy results from hash_table to list
		List_T order_table(merge_table.begin(), merge_table.end());

		{
			// Sort with descending frequency
			using namespace boost::lambda;
			std::sort(	order_table.begin(), order_table.end(),
				( !(bind(&HValue_T::second, _1) < bind(&HValue_T::second, _2)) )	);
		}

		float const total_char = static_cast<float>(input.size() - hash_len +1);
		boost::format fmt("%|1$s| %|2$0.3f|\n");

		foreach(typename List_T::value_type &e, order_table)
		{
			e.first.key[hash_len] = 0; // ensure proper null terminated
			boost::to_upper(e.first.key);

			float percent = static_cast<float>(e.second) * 100.0f / total_char;
			fmt % e.first.key % percent;

			output << fmt;
		}

		output << std::endl;
		thread_passed = 0;
	}
}


// Build a hash_table, count all key with hash_len = 3, 4, 6, 12, 18
// Then print a specific sequence's count
template <int hash_len, typename Input_T>
void 
write_frequencies(Input_T const &input, std::ostream &output, char const *specific)
{
	typedef Key_T<hash_len>		HKey_T;
	typedef __gnu_pbds::cc_hash_table	<
											HKey_T,		// key type
											uint32_t,	// map type
											HKey_T,		// hash functor
											HKey_T		// equal_to functor
										> 	HTable_T;

	HTable_T local_table;	// private for each thread
	if (omp_get_num_threads() > 1)
		calculate_frequency<hash_len, true>(input, local_table);	// parallel hash
	else
		calculate_frequency<hash_len, false>(input, local_table);	// parallel hash

	// Build hash key for searching
	HKey_T printkey(specific);

	// count how many matched for specific sequence
	static uint total_matched = 0;
	
	// parallel look up
	#pragma omp atomic
	total_matched += local_table[printkey];

	// The last thread, reaching this code block, will print result
	static int thread_passed = 0;
	if (__sync_add_and_fetch(&thread_passed, 1) == omp_get_num_threads())
	{
		printkey.key[hash_len] = 0; // null terminated
		boost::to_upper(printkey.key);

		boost::format fmt("%1%\t%2%\n");
		fmt % total_matched % printkey.key;
		output << fmt;

		thread_passed = 0;
		total_matched = 0;
	}
}

int 
GetThreadCount()
{
	cpu_set_t cs;
	CPU_ZERO(&cs);
	sched_getaffinity(0, sizeof(cs), &cs);

	int count = 0;
	for (int i = 0; i < MAX_CORE; ++i)
	{
		if (CPU_ISSET(i, &cs))
			++count;
	}
	return count;
}

int 
main()
{
	typedef std::vector<char> Input_T;
	Input_T input;
	input.reserve(256*1024*1024); // 256MB

	char buffer[64];

	// rule: read line-by-line
	while (fgets(buffer, sizeof(buffer), stdin))
	{
		if(strncmp(buffer, ">THREE", 6) == 0)
			break;
	}

	std::back_insert_iterator<Input_T> b_it (input);
	while (fgets(buffer, sizeof(buffer), stdin))
	{
		size_t sz = strlen(buffer);
		if (buffer[sz -1] == '\n')
			--sz;

		std::copy(buffer, buffer + sz, b_it);
	}

	std::ostringstream output[7];
	#pragma omp parallel num_threads(GetThreadCount()) default(shared)
	{
		write_frequencies<18>( input, output[6], "ggtattttaatttatagt" );
		write_frequencies<12>( input, output[5], "ggtattttaatt" );
		write_frequencies< 6>( input, output[4], "ggtatt" );
		write_frequencies< 4>( input, output[3], "ggta" );
		write_frequencies< 3>( input, output[2], "ggt" );
		write_frequencies< 2>( input, output[1] );
		write_frequencies< 1>( input, output[0] );
	}

	foreach(std::ostringstream const& s, output)
		std::cout << s.str();
		
	return 0;
}



