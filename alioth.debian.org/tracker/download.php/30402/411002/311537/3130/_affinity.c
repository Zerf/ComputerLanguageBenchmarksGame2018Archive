/* Enfold Enterprise Server */
/* Copyright(C), 2004-5, Enfold Systems, LLC - ALL RIGHTS RESERVED */

/* Enfold Systems, LLC */
/* 4617 Montrose Blvd., Suite C215 */
/* Houston, Texas 77006 USA */
/* p. +1 713.942.2377 | f. +1 832.201.8856 */
/* www.enfoldsystems.com */
/* info@enfoldsystems.com */

/* Inspired by:  */
/* http://www.linuxjournal.com/article/6799 */

#include "Python.h"
#include <sched.h>

PyDoc_STRVAR(_affinity__doc__, "Linux Processor Affinity\n");

static PyObject *
get_process_affinity_mask(PyObject *self, PyObject *args)
{
  cpu_set_t cs;
  pid_t pid;

  if (!PyArg_ParseTuple(args, "i:get_process_affinity_mask", &pid))
    return NULL;

  if (sched_getaffinity(pid, sizeof(cpu_set_t), &cs) < 0) {
    PyErr_SetFromErrno(PyExc_ValueError);
    return NULL;
  }

  return Py_BuildValue("l", *(unsigned long *)&cs);
}

static PyObject *
set_process_affinity_mask(PyObject *self, PyObject *args)
{
  cpu_set_t cs;
  pid_t pid;
  unsigned long omask, nmask;

  if (!PyArg_ParseTuple(args, "il:set_process_affinity_mask",
			&pid, &nmask))
    return NULL;

  if (sched_getaffinity(pid, sizeof(cpu_set_t), &cs) < 0) {
    PyErr_SetFromErrno(PyExc_ValueError);
    return NULL;
  }
  omask = *(unsigned long *)&cs;
  *(unsigned long *)&cs = nmask;

  if (sched_setaffinity(pid, sizeof(cpu_set_t), &cs)) {
    PyErr_SetFromErrno(PyExc_ValueError);
    return NULL;
  }

  return Py_BuildValue("l", omask);
}

static PyMethodDef methods[] = {
  {"get_process_affinity_mask", get_process_affinity_mask, METH_VARARGS,
    "get_process_affinity_mask(pid) ->\n\
Get the process affinity mask of 'pid'.\n\n\
You can get the affinity mask of any process running\n\
in the system, even if you are not the process owner."},
  {"set_process_affinity_mask", set_process_affinity_mask, METH_VARARGS,
    "set_process_affinity_mask(pid, affinity_mask) ->\n\
Set the process affinity mask of 'pid' to 'affinity_mask'\n\
and return the previous affinity mask.\n\n\
If the PID is set to zero, the PID of the current task is used.\n\n\
Note: you must be 'root' or the owner of 'pid' in\n\
order to be able to call this."},
  {NULL, NULL},
};

PyMODINIT_FUNC
init_affinity(void)
{
  Py_InitModule3("_affinity", methods, _affinity__doc__);
}

