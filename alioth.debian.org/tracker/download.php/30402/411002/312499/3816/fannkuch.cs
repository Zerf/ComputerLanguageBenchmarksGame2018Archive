using System;

class Fannkuch
{
   public static int[] fannkuch(int n) {
      int[] perm = new int[n];
      int[] perm1 = new int[n];
      int[] count = new int[n];
      int[] maxPerm = new int[n];
      int maxFlipsCount = 0;
      int permCount = 0;
      int checksum = 0;

      for(int i=0; i<n; i++) perm1[i] = i;
      int r = n;
      int m = n-1;

      while (true) {

         while (r != 1){ count[r-1] = r; r--; }
         if (perm1[0]!=0 && perm1[m]!=m){
            for(int i=0; i<n; i++) perm[i] = perm1[i];
            int flipsCount = 0;
            int k;

            while ( !((k=perm[0]) == 0) ) {
               int k2 = (k+1) >> 1;
               for(int i=0; i<k2; i++) {
                  int temp = perm[i]; perm[i] = perm[k-i]; perm[k-i] = temp;
               }
               flipsCount++;
            }

            if (flipsCount > maxFlipsCount) {
               maxFlipsCount = flipsCount;
               for(int i=0; i<n; i++) maxPerm[i] = perm1[i];
            }
            
            checksum += permCount%2 == 0 ? flipsCount : -flipsCount;            
         }

         // Use incremental change to generate another permutation
         while (true) {        
            if (r == n) {
               int[] result = {maxFlipsCount,checksum};
               return result;
            }
            int perm0 = perm1[0];
            int i = 0;
            while (i < r) {
               int j = i + 1;
               perm1[i] = perm1[j];
               i = j;
            }
            perm1[r] = perm0;

            count[r] = count[r] - 1;
            if (count[r] > 0) break;
            r++;
         }

         permCount++;
         if (permCount == 1048576){ permCount = 0; }

      }
   }

   static void Main(string[] args){
      int n = 7;
      if (args.Length > 0) n = Int32.Parse(args[0]);
      int[] pf = fannkuch(n);

      Console.WriteLine("checksum = {0}", pf[1]);
      Console.WriteLine("Pfannkuchen({0}) = {1}", n, pf[0]);
   }
}
