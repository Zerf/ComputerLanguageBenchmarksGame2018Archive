(* The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 *
 * contributed by Olivia Xiaoni Lai
 *)

(* Random number generator *)
let im = 139968
and ia = 3877
and ic = 29573
and lookupsize = 4*1024;;
let lookupscale = (float_of_int lookupsize) -. 1.0;;
let last = ref 42 and im_f = float im;;
let gen_random () =
  let n = (!last * ia + ic) mod im in
    last := n;
    (float n) *. (lookupscale /. im_f);;

let getFirst (a, b) = a;;
let getSecond (a, b) = b;;

let compute_cumulative probs =
  let p = ref 0.0 in
  let compute (ch, p1) = p := !p +. p1; (ch, ((!p) *. lookupscale)) in
   Array.map compute probs;;

let generate_lookup probs_cumu = 
  let lookup = Array.make lookupsize ('a', 0.0) in
      for i = 0 to lookupsize-1 do
        let j = ref 0 in
          while (getSecond (Array.get probs_cumu !j)) < (float_of_int i) do
            j := !j+1;
          done;
          Array.set lookup i (Array.get probs_cumu !j);
      done;
      lookup;;
    
let rand_char lookuptable =
  let r = gen_random () in
  let ai = ref (int_of_float r) in
    while ((getSecond (Array.get lookuptable !ai)) < r) do incr ai done;
    getFirst (Array.get lookuptable (!ai));;

let width = 60;;
let make_random_fasta id desc table n =
  Printf.printf ">%s %s\n" id desc;
  let lookuptable = generate_lookup table in
  let line = String.make (width+1) '\n' in
  for i = 1 to n / width do
    for j = 0 to width - 1 do line.[j] <- rand_char lookuptable done;
    print_string line;
  done;
  let w = n mod width in
  if w > 0 then 
    for j = 1 to w do print_char(rand_char lookuptable); done;
    print_char '\n';;
  
let rec write s i0 l w =
  let len = l - i0 in
  if w <= len then (output stdout s i0 w; print_char '\n'; i0 + w)
  else (output stdout s i0 len; write s 0 l (w - len))

let make_repeat_fasta id desc src n =
  Printf.printf ">%s %s\n" id desc;
  let l = String.length src
  and i0 = ref 0 in
  for i = 1 to n / width do
    i0 := write src !i0 l width;
  done;
  let w = n mod width in
  if w > 0 then ignore(write src !i0 l w);;


let alu = "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG\
GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA\
CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT\
ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA\
GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG\
AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC\
AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA";;

let iub = [| ('a', 0.27);  ('c', 0.12);  ('g', 0.12);  ('t', 0.27);
	     ('B', 0.02);  ('D', 0.02);  ('H', 0.02);  ('K', 0.02);
	     ('M', 0.02);  ('N', 0.02);  ('R', 0.02);  ('S', 0.02);
	     ('V', 0.02);  ('W', 0.02);  ('Y', 0.02);  |];;

let homosapiens = [| ('a', 0.3029549426680);    ('c', 0.1979883004921);
		     ('g', 0.1975473066391);    ('t', 0.3015094502008);  |];;

let iub_cumu = compute_cumulative iub;;
let homosapiens_cumu = compute_cumulative homosapiens;;

let () =
  let n = try int_of_string(Array.get Sys.argv 1) with _ -> 1000 in
  make_repeat_fasta "ONE" "Homo sapiens alu" alu (n*2);
  make_random_fasta "TWO" "IUB ambiguity codes" iub_cumu (n*3);
  make_random_fasta "THREE" "Homo sapiens frequency" homosapiens_cumu (n*5)
