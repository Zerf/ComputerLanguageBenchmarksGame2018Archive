#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>
#include <unordered_map>
#include <vector>
#include <future>
#include <unistd.h>

using namespace std;

// we don't need ordering as we will sort them at the end
typedef unordered_map<string, unsigned int> FreqMap;

FreqMap calculate_freqs(const string & input, unsigned int step) {
    int SPREAD = sysconf(_SC_NPROCESSORS_ONLN);
    int RESERVE = 4096*4;

    // work function. We spread the work over x steps
    auto work = [&](unsigned int start, int spread) {
        FreqMap freqs;
        freqs.reserve(RESERVE);

        for (unsigned int i = start, i_end = input.size()+1-step; i<i_end; i+=spread){
           freqs[input.substr(i, step)]+=1;
        }
        return move(freqs);
    };

    // start SPREAD threads to perform the actual work
    vector<future<FreqMap>> ft;
    for (int i=0;i<SPREAD;++i) {
        ft.push_back(async(launch::async, work, i, SPREAD));
    }

    // summarize the partial sums
    FreqMap freqs = ft[0].get();
    for (int i=1;i<SPREAD;++i){
         for (auto & j: ft[i].get()){
             freqs[j.first]+=j.second;
          }
    }

    return move(freqs);
}

void printMap(unsigned int sum, FreqMap && freqs) {
    vector<pair<string, unsigned int>> counts;
    
    // copy everything to the vector
    for (auto &i: freqs) counts.emplace_back(i.first, i.second);

    // sort by count - descending
    sort(counts.begin(), counts.end(), 
            [](const pair<string, unsigned int> &a, const pair<string, unsigned int> &b){
            return a.second > b.second;
            });

    // print 
    for (auto &i: counts)  cout << i.first << ' '  << (sum ? double(100 * i.second) / sum : 0.0) << endl;
    std::cout << '\n';
}

inline
void frequencies(const string & input, unsigned int step) {
    printMap(input.size() + 1 - step, calculate_freqs(input, step));
}

inline
void counts(const string & input, const string & fragment) {
    auto step = fragment.size();
    auto freqs = calculate_freqs(input, step);

    cout << freqs[fragment] << '\t' << fragment << endl;
}

int main(){
    string buff;
    string input;

    while (getline(cin, buff) && buff.find(">THREE") != 0);
    while (getline(cin, buff) && buff[0] != '>') {
        input.append(buff);
    }
    transform(input.begin(),input.end(),input.begin(),::toupper);

    cout << setprecision(3) << setiosflags(ios::fixed);   

    frequencies(input, 1);
    frequencies(input, 2);
    counts(input, "GGT");
    counts(input, "GGTA");
    counts(input, "GGTATT");
    counts(input, "GGTATTTTAATT");
    counts(input, "GGTATTTTAATTTATAGT");
}

