%{
    #include <string>
    #include <iostream>
    #include <vector>
    #include <algorithm>

	int pat_count[]={0,0,0,0,0,0,0,0,0};
    char const * const pat1[]={
        "agggtaaa|tttaccct",
        "[cgt]gggtaaa|tttaccc[acg]",
        "a[act]ggtaaa|tttacc[agt]t",
        "ag[act]gtaaa|tttac[agt]ct",
        "agg[act]taaa|ttta[agt]cct",
        "aggg[acg]aaa|ttt[cgt]ccct",
        "agggt[cgt]aa|tt[acg]accct",
        "agggta[cgt]a|t[acg]taccct",
        "agggtaa[cgt]|[acg]ttaccct"
    }, *rep[]={
        "<4>",
        "<3>",
        "<2>",
        "|",
        "-"
    };
    std::vector<char> outbuf[2]; //alternating input/output for replace
	std::vector<char> &ob(outbuf[0]);
%}
%option reentrant stack nodebug noyywrap nodefault never-interactive nounistd

%x X0 X1 X2 X3 X4 X5 X6 X7 X8 ROUND0 ROUND1 ROUND2 ROUND3 ROUND4

%%
<X0>agggtaaa|tttaccct					pat_count[0]++;
<X1>[cgt]gggtaaa|tttaccc[acg]			pat_count[1]++;
<X2>a[act]ggtaaa|tttacc[agt]t			pat_count[2]++;
<X3>ag[act]gtaaa|tttac[agt]ct			pat_count[3]++;
<X4>agg[act]taaa|ttta[agt]cct			pat_count[4]++;
<X5>aggg[acg]aaa|ttt[cgt]ccct			pat_count[5]++;
<X6>agggt[cgt]aa|tt[acg]accct			pat_count[6]++;
<X7>agggta[cgt]a|t[acg]taccct			pat_count[7]++;
<X8>agggtaa[cgt]|[acg]ttaccct			pat_count[8]++;
	/*<X0,X1,X2,X3,X4,X5,X6,X7,X8>[^acgt]+	//eat up all nonmatching characters
	  however this rule slows down the scanner tremendously */
<ROUND0>tHa[Nt]							ob.insert(ob.begin()+ob.size(),rep[0],rep[0]+3);
<ROUND1>aND|caN|Ha[DS]|WaS				ob.insert(ob.begin()+ob.size(),rep[1],rep[1]+3);
<ROUND2>a[NSt]|BY						ob.insert(ob.begin()+ob.size(),rep[2],rep[2]+3);
<ROUND3>\<[^>]*\>						ob.emplace_back(rep[3][0]);
<ROUND4>\|[^|][^|]*\|					ob.emplace_back(rep[4][0]);
<ROUND0,ROUND1,ROUND2,ROUND3,ROUND4>.	ob.emplace_back(yytext[0]);
<*>.|\n 	
%%

using namespace std;

int main() {
	//find out how big input file is
    cin.seekg(0, ios::end);
    int read_size = cin.tellg();
    cin.seekg(0, ios::beg);
	//declare variables and allocate space for them
    string s; //for input
    s.resize(read_size);
    outbuf[0].reserve(read_size);
    outbuf[1].reserve(read_size);
    cin.read(&s[0], s.size());
    size_t pos;
	//get rid of comments
    while ((pos=s.find('>'))!=string::npos) {
        s.erase(s.begin()+pos,s.begin()+s.find('\n',pos)+1);
    }
    //	and newlines. Doing that in place is too slow
    int spos=0;
    auto it=ob.begin();
    while ((pos=s.find('\n',spos))!=string::npos) {
        //it=copy(s.begin()+spos,s.begin()+pos,it);
        ob.insert(it,s.begin()+spos,s.begin()+pos);
		it+=(pos-spos);
        spos=pos+1;
    }
	ob.insert(it,s.begin()+spos,s.end()); //just in case input doesn't end in \n
    int stripsize=ob.size();
	//search for the 9 pattern in parallel
	#pragma omp parallel for schedule(dynamic) 
	for (int x=X0;x<=X8;++x) {
		yyscan_t yyscanner;
		yylex_init ( &yyscanner );
	    yyset_in(fmemopen(ob.data(),stripsize,"r"),yyscanner);
		yy_push_state(x,yyscanner);
		yylex(yyscanner);
   		fclose(yyget_in(yyscanner));
		yylex_destroy ( yyscanner );
	}
	for (int x=X0;x<=X8;++x) {
		cout<<pat1[x-X0]<<" "<<pat_count[x-X0]<<endl;
	}
	
	// do the replacements using the scanner one by one
	for (int r=ROUND0;r<=ROUND4;++r) {
		//make outbuf[1] the "infile"
		outbuf[0].swap(outbuf[1]);
		yyscan_t scanner;
		yylex_init ( &scanner );
		yyset_in(fmemopen(outbuf[1].data(),outbuf[1].size(),"r"),scanner);
		yy_push_state(r,scanner);
		yylex(scanner);
   		fclose(yyget_in(scanner));
		yylex_destroy ( scanner );
		outbuf[1].clear();
	}

	//output the sizes
	cout<<endl<<read_size<<endl<<stripsize<<endl<<outbuf[0].size()<<endl;
	return 0;
}
