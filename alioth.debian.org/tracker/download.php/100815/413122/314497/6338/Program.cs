﻿/* The Computer Language Benchmarks Game

 * http://benchmarksgame.alioth.debian.org/

 * 

 * contributed by Stefan Krause

 * slightly modified by Chad Whipkey

 * parallelized by Colin D Bennett 2008-10-04

 * reduce synchronization cost by The Anh Tran

 * optimizations and refactoring by Enotus 2010-11-11

 * optimization by John Stalcup 2012-2-19
 
 * ported to C# & optimized by kasthack 

 */

using System;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

public class Mandelbrot {
    static byte[][] _output;
    static int _yCt;
    static double[] _crb;
    static double[] _cib;
    static int GetByte( int x, int y ) {
        var res = 0;
        var cibY = _cib[ y ];

        for ( var i = 0; i < 8; i += 2 ) {
            var crbXI = _crb[ x + i ];
            var crbXip = _crb[ x + i + 1 ];

            var zr1 = crbXI;
            var zi1 = cibY;

            var zr2 = crbXip;
            var zi2 = cibY;

            var b = 0;
            var j = 49;

            double nZr1;
            double nZi1;
            double nZr2;
            double nZi2;

            do {
                nZr1 = zr1 * zr1 - zi1 * zi1 + crbXI;
                nZi1 = zr1 * zi1 * 2D + cibY;
                zr1 = nZr1;
                zi1 = nZi1;

                nZr2 = zr2 * zr2 - zi2 * zi2 + crbXip;
                nZi2 = zr2 * zi2 * 2D + cibY;
                zr2 = nZr2;
                zi2 = nZi2;

                if ( zr1 * zr1 + zi1 * zi1 > 4D ) {
                    b |= 2;
                    if ( b == 3 )
                        break;
                }
                if ( zr2 * zr2 + zi2 * zi2 <= 4D ) continue;
                b |= 1;
                if ( b == 3 )
                    break;
            }
            while ( --j > 0 );
            res = ( res << 2 ) + b;
        }
        return res ^ -1;
    }
    static void PutLine( int y, byte[] line ) {
        for ( var xb = 0; xb < line.Length; xb++ )
            line[ xb ] = (byte) GetByte( xb * 8, y );
    }

    public static void Main( string[] args ) {
        var n=16000;
        if (args.Length>=1) n=int.Parse(args[0]);
        
        //init
        _crb=new double[n+7];
        _cib=new double[n+7];
        var invN=2.0/n;
        for (var i = 0; i < n; i++) {
            _cib[i]=i*invN-1.0;
            _crb[i]=i*invN-1.5;
        }
        _output=new byte[n][];
        var cnt = ( n + 7 ) >> 3;
        for(var i=0;i<n;i++)
            _output[i]= new byte[cnt];
        
        //compute
        var pool = new Thread[ 2 * Environment.ProcessorCount ];
        for (var i=0;i<pool.Length;i++)
            pool[i]=new Thread(Compute);
        foreach (var t in pool)
            t.Start();
        foreach (var thread in pool)
            thread.Join();

        //print
        var encoding = Encoding.ASCII;
        using (var ns = Console.OpenStandardOutput()){
            using(var stream = ns) {
                var bytes = encoding.GetBytes("P4\n" + n + " " + n + "\n");
                stream.Write(bytes, 0, bytes.Length);
                for(var i=0;i<n;i++)
                    stream.Write(_output[i],0, _output[i].Length);
                stream.Flush();
            }
        }
    }
    private static void Compute() {
        int y;
        while ( ( y = Interlocked.Increment( ref _yCt ) ) < _output.Length )
            PutLine( y, _output[ y ] );
    }
}
