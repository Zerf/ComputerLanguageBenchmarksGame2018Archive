/* The Computer Language Benchmarks Game
 * http://benchmarksgame.alioth.debian.org/
 *
 * contributed by chaishushan{AT}gmail.com.
 * based on Go program by The Go Authors.
 * based on C program by Kevin Carson
 * flag.Arg hack by Isaac Gouy
 *
 * 2013-04
 * modified by Jamil Djadala to use goroutines
 *
 */

package main

import (
	"flag"
	"fmt"
	"runtime"
	"strconv"
	"sync"
)

var minDepth = 4
var n = 0

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU() * 2)

	flag.Parse()
	if flag.NArg() > 0 {
		n, _ = strconv.Atoi(flag.Arg(0))
	}

	maxDepth := n
	if minDepth+2 > n {
		maxDepth = minDepth + 2
	}
	stretchDepth := maxDepth + 1

	stretchArena := NewArena(stretchDepth)
	check_l := bottomUpTree(0, stretchDepth, stretchArena).ItemCheck()
	fmt.Printf("stretch tree of depth %d\t check: %d\n", stretchDepth, check_l)

	longLivedArena := NewArena(maxDepth)
	longLivedTree := bottomUpTree(0, maxDepth, longLivedArena)

	result_trees := make([]int, maxDepth+1)
	result_check := make([]int, maxDepth+1)

	var wg sync.WaitGroup
	for depth_l := minDepth; depth_l <= maxDepth; depth_l += 2 {
		wg.Add(1)
		go func(depth int) {
			arena := NewArena(depth)

			iterations := 1 << uint(maxDepth-depth+minDepth)
			check := 0

			for i := 1; i <= iterations; i++ {
				arena.Clear()
				check += bottomUpTree(i, depth, arena).ItemCheck()
				arena.Clear()
				check += bottomUpTree(-i, depth, arena).ItemCheck()
			}
			result_trees[depth] = iterations * 2
			result_check[depth] = check

			wg.Done()
		}(depth_l)
	}
	wg.Wait()

	for depth := minDepth; depth <= maxDepth; depth += 2 {
		fmt.Printf("%d\t trees of depth %d\t check: %d\n",
			result_trees[depth], depth, result_check[depth],
		)
	}
	fmt.Printf("long lived tree of depth %d\t check: %d\n",
		maxDepth, longLivedTree.ItemCheck(),
	)
}

func bottomUpTree(item, depth int, arena *Arena) *Node {
	if depth <= 0 {
		return arena.Alloc(item, nil, nil)
	}
	return arena.Alloc(item,
		bottomUpTree(2*item-1, depth-1, arena),
		bottomUpTree(2*item, depth-1, arena),
	)
}

type Node struct {
	item        int
	left, right *Node
}

func (self *Node) ItemCheck() int {
	if self.left == nil {
		return self.item
	}
	return self.item + self.left.ItemCheck() - self.right.ItemCheck()
}

type Arena struct {
	brk   int
	idx   int
	store []Node
}

func NewArena(depth int) *Arena {
	self := &Arena{}
	self.brk = 1 << uint(depth+1)
	self.idx = -1
	self.store = make([]Node, self.brk)
	return self
}

func (self *Arena) Alloc(i int, l, r *Node) *Node {
	self.idx++
	p := &(self.store[self.idx])
	p.item = i
	p.left = l
	p.right = r
	return p
}

func (self *Arena) Clear() {
	self.idx = -1
}

