# The Computer Language Benchmarks Game
# http://benchmarksgame.alioth.debian.org
#
#  contributed by Karl von Laudermann
#  modified by Jeremy Echols
#  modified by Detlef Reichl
#  modified by Joseph LaFata
#  modified by Peter Zotov
#  modified by Aaron Tavistock

size = ARGV.shift.to_f

puts "P4\n#{size.to_i} #{size.to_i}"

byte_acc = 0
bit_num = 0

def get_bit_or(cr, ci)
  zrzr = 0.0
  zizi = 0.0
  zrzi = 0.0

  count = 50
  while count > 0
    
    zr = zrzr - zizi + cr
    zi = 2.0 * zrzi + ci

    # preserve recalculation
    zrzr = zr*zr
    zizi = zi*zi
    zrzi = zr*zi

    return 0b0 if zrzr + zizi > 4.0
      
    count -= 1
  end

  0b1
end

y = 0.0
while y < size
  ci = (2.0 * y / size ) - 1.0

  x = 0.0
  while x < size
    cr = (2.0 * x / size) - 1.5

    byte_acc = (byte_acc << 1) | get_bit_or(cr, ci)
    bit_num += 1

    # Code is very similar for these cases, but using separate blocks
    # ensures we skip the shifting when it's unnecessary, which is most cases.
    if (bit_num == 8)
      print byte_acc.chr
      byte_acc = 0
      bit_num = 0
    elsif (x == size - 1)
      byte_acc <<= (8 - bit_num)
      print byte_acc.chr
      byte_acc = 0
      bit_num = 0
    end
    x += 1.0
  end
  y += 1.0
end
