import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;

public class knucleotide {

    public static void main(final String[] args) throws Exception {
        // read full fullSequence
        final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        scanPastHeader(in, ">THREE");
        fullSequence = readRestIntoByteArray(in);

        // create tasks
        final List<Callable<TIntLongHashMap2>> nucleotideMaps = buildCountTasks(fullSequence);

        // execute tasks
        final ExecutorService executor = new ForkJoinPool();
        final List<Future<TIntLongHashMap2>> futures = executor.invokeAll(nucleotideMaps);
        executor.shutdown();

        // print frequencies
        final StringBuilder out = new StringBuilder();
        TIntLongHashMap2 mapi = futures.get(0).get();
        printFrequencies(out, fullSequence, mapi);
        printFrequencies(out, fullSequence, sumMaps(futures.get(1).get(), futures.get(2).get()));

        // print counts
        final String[] nucleotideFrequenciesToPrint = { "ggt", "ggta", "ggtatt", "ggtattttaatt", "ggtattttaatttatagt" };
        for (final String nucleotideSequence : nucleotideFrequenciesToPrint) {
            printCount(out, futures, nucleotideSequence);
        }

        System.out.print(out.toString());
    }

    private static List<Callable<TIntLongHashMap2>> buildCountTasks(final byte[] fullSequence) {
        final int[] nucleotideLengths = { 1, 2, 3, 4, 6, 12, 18 };
        final List<Callable<TIntLongHashMap2>> nucleotideMaps = new LinkedList<>();
        for (final int nucleotideLength : nucleotideLengths) {
            // for (int i = 0; i < nucleotideLength; i++) {
            for (int i = nucleotideLength - 1; i >= 0; i--) {
                final int offset = i;
                nucleotideMaps.add(new Callable<TIntLongHashMap2>() {
                    @Override
                    public TIntLongHashMap2 call() throws Exception {
                        return countSequences(fullSequence, nucleotideLength, offset);
                    }
                });
            }
        }
        return nucleotideMaps;
    }

    private static byte[] readRestIntoByteArray(final BufferedReader in) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();

        final byte bytes[] = new byte[256];
        String line;
        while ((line = in.readLine()) != null) {
            for (int i = 0; i < line.length(); i++)
                bytes[i] = (byte) line.charAt(i);
            baos.write(bytes, 0, line.length());
        }

        return baos.toByteArray();
    }

    private static void scanPastHeader(final BufferedReader in, final String header) throws IOException {
        String line;
        while ((line = in.readLine()) != null) {
            if (line.startsWith(header))
                break;
        }
    }

    private static TIntLongHashMap2 countSequences(final byte[] fullSequence, final int nucleotideLength,
            final int offset) {
        final TIntLongHashMap2 nucleotideMap = new TIntLongHashMap2(7000);
        for (int i = offset; i < fullSequence.length - nucleotideLength; i += nucleotideLength) {
            final int hash = calculateHash(fullSequence, i, nucleotideLength);
            long data = nucleotideMap.get(hash);
            if (data == 0) {
                nucleotideMap.put(hash, NucleotideH.getDataFor(fullSequence, i, nucleotideLength));
            } else {
                nucleotideMap.put(hash, NucleotideH.incFor(data));
            }
        }
        return nucleotideMap;
    }

    private static TIntLongHashMap2 sumMaps(final TIntLongHashMap2... nucleotideMaps){
    final TIntLongHashMap2 totalNucleotideMap = new TIntLongHashMap2();
    for (TIntLongHashMap2 nucleotideMap : nucleotideMaps) {
            for (int key : nucleotideMap.keys()) {
                long data = totalNucleotideMap.get(key);
                if (data != 0) {
                    totalNucleotideMap.put(key, NucleotideH.setCountFor(nucleotideMap.get(key),
                            NucleotideH.getCountFor(data) + NucleotideH.getCountFor(nucleotideMap.get(key))));
                } else {
                    totalNucleotideMap.put(key, nucleotideMap.get(key));
                }
            }
        }
        return totalNucleotideMap;
    }

    private static int calculateHash(final byte[] bytes, final int offset, final int length) {
        int hash = 17;
        for (int i = offset; i < offset + length; i++) {
            hash = hash * 23 + bytes[i];
        }
        return hash;
    }

    private static void printCount(final StringBuilder out, final List<Future<TIntLongHashMap2>> nucleotideMapFutures,
            final String nucleotideSequence) throws Exception {
        final int hash = calculateHash(nucleotideSequence.getBytes(), 0, nucleotideSequence.length());
        int count = 0;
        for (Future<TIntLongHashMap2> future : nucleotideMapFutures) {
            long data = future.get().get(hash);
            if (data != 0)
                count += NucleotideH.getCountFor(future.get().get(hash));
        }
        out.append(count).append("\t").append(nucleotideSequence.toUpperCase()).append("\n");
    }

    private static void printFrequencies(final StringBuilder out, final byte[] fullSequence,
            TIntLongHashMap2 nucleotideMap) {

        long[] arr = nucleotideMap.values();
        Nucleotide[] arr2 = new Nucleotide[nucleotideMap.size()];
        int iPos = 0;
        for (long data : arr) {
            arr2[iPos++] = new Nucleotide(fullSequence, NucleotideH.getOffsetFor(data), NucleotideH.getLengthFor(data),
                    NucleotideH.getCountFor(data));
        }
        Arrays.sort(arr2);
        for (final Nucleotide nuc : arr2) {
            final String sequenceString = nuc.toString();
            final double factor = 100d / (fullSequence.length - nuc.getLength() + 1);
            final double frequency = (nuc.getCount() * factor);
            out.append(sequenceString);
            out.append(" ");
            out.append(String.format("%.3f", frequency));
            out.append("\n");
        }

    }

    static byte[] fullSequence;

    private static class NucleotideH {
        static long getDataFor(final byte[] fullSequence, final int offset, final int length) {
            long data = (((long) offset) << 32) | ((length) << 24) | 1;
            return data;
        }

        static int getCountFor(long data) {
            return ((int) data & 0x00FFFFFF);
        }

        static long setCountFor(long data, int count) {
            data = data & 0xFFFFFFFF_FF000000L | count;
            return data;
        }

        static long incFor(long data) {
            return setCountFor(data, (int) getCountFor(data) + 1);
        }

        static byte getLengthFor(long data) {
            return (byte) (((int) data) >> 24);
        }

        static int getOffsetFor(long data) {
            return (int) (data >> 32);
        }

        static String toString(long data) {
            // System.out.println(getOffset()+"_"+getLength()+ "_"+getCount());
            return new String(fullSequence, getOffsetFor(data), getLengthFor(data)).toUpperCase();
        }

    }

    private static class Nucleotide implements Comparable<Nucleotide> {
        // private final byte[] fullSequence;
        // private final int offset;
        // private final byte length;
        // private int count = 1;
        private long data;

        private Nucleotide(final byte[] fullSequence, final int offset, final byte length, final int count) {
            this(fullSequence, offset, length);
            setCount(count);
        }

        private Nucleotide(final byte[] fullSequence, final int offset, final byte length) {
            // this.fullSequence = fullSequence;
            // this.offset = offset;
            // this.length = (byte)length;

            // this.data = (length<<24) & offset;
            // 231.988 instances
            data = (((long) offset) << 32) | ((length) << 24) | 1;

        }

        public final int compareTo(final Nucleotide other) {
            if (other.getCount() != getCount()) {
                // For primary sort by count
                return other.getCount() - getCount();
            } else {
                // For secondary sort by bytes
                return compareBytes(this, other);
            }
        }

        private int compareBytes(final Nucleotide a, final Nucleotide b) {
            final int suma = sumBytes(a);
            final int sumb = sumBytes(b);
            return suma < sumb ? -1 : suma == sumb ? 0 : 1;
        }

        private byte getLength() {
            return (byte) (((int) data) >> 24);
        }

        private int getCount() {
            return ((int) data & 0x00FFFFFF);
        }

        private void setCount(int count) {
            data = data & 0xFFFFFFFF_FF000000L | count;
        }

        private int getOffset() {
            return (int) (data >> 32);
        }

        private int sumBytes(final Nucleotide nucleotide) {
            int sum = 0;
            byte length = getLength();
            for (int i = 0; i < length; i++) {
                // sum += nucleotide.fullSequence[nucleotide.offset + i] * (nucleotide.length - i);
                sum += fullSequence[getOffset() + i] * (getLength() - i);
            }
            return sum;
        }

        @Override
        public String toString() {
            // System.out.println(getOffset()+"_"+getLength()+ "_"+getCount());
            return new String(fullSequence, getOffset(), getLength()).toUpperCase();
        }

        public void inc() {
            setCount(getCount() + 1);
        }
    }

}

final class TIntLongHashMap2 extends TIntLongHash2 {
    static final long serialVersionUID = 1L;

    /** the values of the map */
    protected transient long[] _values;

    /**
     * Creates a new <code>TIntLongHashMap</code> instance with the default capacity and load factor.
     */
    public TIntLongHashMap2() {
        super();
    }

    /**
     * Creates a new <code>TIntLongHashMap</code> instance with a prime capacity equal to or greater than
     * <tt>initialCapacity</tt> and with the default load factor.
     *
     * @param initialCapacity
     *            an <code>int</code> value
     */
    public TIntLongHashMap2(int initialCapacity) {
        super(initialCapacity);
    }

    /**
     * initializes the hashtable to a prime capacity which is at least <tt>initialCapacity + 1</tt>.
     *
     * @param initialCapacity
     *            an <code>int</code> value
     * @return the actual capacity chosen
     */
    protected int setUp(int initialCapacity) {
        int capacity;

        capacity = super.setUp(initialCapacity);
        _values = new long[capacity];
        return capacity;
    }

    /**
     * rehashes the map to the new capacity.
     *
     * @param newCapacity
     *            an <code>int</code> value
     */
    /** {@inheritDoc} */
    protected void rehash(int newCapacity) {
        int oldCapacity = _set.length;

        int oldKeys[] = _set;
        long oldVals[] = _values;
        byte oldStates[] = _states;

        _set = new int[newCapacity];
        _values = new long[newCapacity];
        _states = new byte[newCapacity];

        for (int i = oldCapacity; i-- > 0;) {
            if (oldStates[i] == FULL) {
                int o = oldKeys[i];
                int index = insertKey(o);
                _values[index] = oldVals[i];
            }
        }
    }

    /** {@inheritDoc} */
    public long put(int key, long value) {
        int index = insertKey(key);
        return doPut(key, value, index);
    }

    private long doPut(int key, long value, int index) {
        long previous = no_entry_value;
        boolean isNewMapping = true;
        if (index < 0) {
            index = -index - 1;
            previous = _values[index];
            isNewMapping = false;
        }
        _values[index] = value;

        if (isNewMapping) {
            postInsertHook(consumeFreeSlot);
        }

        return previous;
    }

    /** {@inheritDoc} */
    public long get(int key) {
        int index = index(key);
        return index < 0 ? no_entry_value : _values[index];
    }

    /** {@inheritDoc} */
    public long remove(int key) {
        long prev = no_entry_value;
        int index = index(key);
        if (index >= 0) {
            prev = _values[index];
            removeAt(index); // clear key,state; adjust size
        }
        return prev;
    }

    /** {@inheritDoc} */
    public int[] keys() {
        int[] keys = new int[size()];
        int[] k = _set;
        byte[] states = _states;

        for (int i = k.length, j = 0; i-- > 0;) {
            if (states[i] == FULL) {
                keys[j++] = k[i];
            }
        }
        return keys;
    }

    /** {@inheritDoc} */
    public long[] values() {
        long[] vals = new long[size()];
        long[] v = _values;
        byte[] states = _states;

        for (int i = v.length, j = 0; i-- > 0;) {
            if (states[i] == FULL) {
                vals[j++] = v[i];
            }
        }
        return vals;
    }

    /** {@inheritDoc} */
    public boolean containsValue(long val) {
        byte[] states = _states;
        long[] vals = _values;

        for (int i = vals.length; i-- > 0;) {
            if (states[i] == FULL && val == vals[i]) {
                return true;
            }
        }
        return false;
    }

    /** {@inheritDoc} */
    public boolean containsKey(int key) {
        return contains(key);
    }

} // TIntLongHashMap

abstract class TIntLongHash2 extends TPrimitiveHash2 {
    static final long serialVersionUID = 1L;

    /** the set of ints */
    public transient int[] _set;

    /**
     * key that represents null
     *
     * NOTE: should not be modified after the Hash is created, but is not final because of Externalization
     *
     */
    protected int no_entry_key;

    /**
     * value that represents null
     *
     * NOTE: should not be modified after the Hash is created, but is not final because of Externalization
     *
     */
    protected long no_entry_value;

    protected boolean consumeFreeSlot;

    /**
     * Creates a new <code>T#E#Hash</code> instance with the default capacity and load factor.
     */
    public TIntLongHash2() {
        super();
        no_entry_key = (int) 0;
        no_entry_value = (long) 0;
    }

    /**
     * Creates a new <code>T#E#Hash</code> instance whose capacity is the next highest prime above
     * <tt>initialCapacity + 1</tt> unless that value is already prime.
     *
     * @param initialCapacity
     *            an <code>int</code> value
     */
    public TIntLongHash2(int initialCapacity) {
        super(initialCapacity);
        no_entry_key = (int) 0;
        no_entry_value = (long) 0;
    }

    /**
     * initializes the hashtable to a prime capacity which is at least <tt>initialCapacity + 1</tt>.
     *
     * @param initialCapacity
     *            an <code>int</code> value
     * @return the actual capacity chosen
     */
    protected int setUp(int initialCapacity) {
        int capacity;

        capacity = super.setUp(initialCapacity);
        _set = new int[capacity];
        return capacity;
    }

    /**
     * Searches the set for <tt>val</tt>
     *
     * @param val
     *            an <code>int</code> value
     * @return a <code>boolean</code> value
     */
    public boolean contains(int val) {
        return index(val) >= 0;
    }

    /**
     * Releases the element currently stored at <tt>index</tt>.
     *
     * @param index
     *            an <code>int</code> value
     */
    protected void removeAt(int index) {
        _set[index] = no_entry_key;
        super.removeAt(index);
    }

    /**
     * Locates the index of <tt>val</tt>.
     *
     * @param key
     *            an <code>int</code> value
     * @return the index of <tt>val</tt> or -1 if it isn't in the set.
     */
    protected int index(int key) {
        int hash, probe, index, length;

        final byte[] states = _states;
        final int[] set = _set;
        length = states.length;
        hash = HashFunctions2.hash(key) & 0x7fffffff;
        index = hash % length;
        byte state = states[index];

        if (state == FREE)
            return -1;

        if (state == FULL && set[index] == key)
            return index;

        return indexRehashed(key, index, hash, state);
    }

    int indexRehashed(int key, int index, int hash, byte state) {
        // see Knuth, p. 529
        int length = _set.length;
        int probe = 1 + (hash % (length - 2));
        final int loopIndex = index;

        do {
            index -= probe;
            if (index < 0) {
                index += length;
            }
            state = _states[index];
            //
            if (state == FREE)
                return -1;

            //
            if (key == _set[index] && state != REMOVED)
                return index;
        } while (index != loopIndex);

        return -1;
    }

    /**
     * Locates the index at which <tt>val</tt> can be inserted. if there is already a value equal()ing <tt>val</tt> in
     * the set, returns that value as a negative integer.
     *
     * @param key
     *            an <code>int</code> value
     * @return an <code>int</code> value
     */
    protected int insertKey(int val) {
        int hash, index;

        hash = HashFunctions2.hash(val) & 0x7fffffff;
        index = hash % _states.length;
        byte state = _states[index];

        consumeFreeSlot = false;

        if (state == FREE) {
            consumeFreeSlot = true;
            insertKeyAt(index, val);

            return index; // empty, all done
        }

        if (state == FULL && _set[index] == val) {
            return -index - 1; // already stored
        }

        // already FULL or REMOVED, must probe
        return insertKeyRehash(val, index, hash, state);
    }

    int insertKeyRehash(int val, int index, int hash, byte state) {
        // compute the double hash
        final int length = _set.length;
        int probe = 1 + (hash % (length - 2));
        final int loopIndex = index;
        int firstRemoved = -1;

        /**
         * Look until FREE slot or we start to loop
         */
        do {
            // Identify first removed slot
            if (state == REMOVED && firstRemoved == -1)
                firstRemoved = index;

            index -= probe;
            if (index < 0) {
                index += length;
            }
            state = _states[index];

            // A FREE slot stops the search
            if (state == FREE) {
                if (firstRemoved != -1) {
                    insertKeyAt(firstRemoved, val);
                    return firstRemoved;
                } else {
                    consumeFreeSlot = true;
                    insertKeyAt(index, val);
                    return index;
                }
            }

            if (state == FULL && _set[index] == val) {
                return -index - 1;
            }

            // Detect loop
        } while (index != loopIndex);

        // We inspected all reachable slots and did not find a FREE one
        // If we found a REMOVED slot we return the first one found
        if (firstRemoved != -1) {
            insertKeyAt(firstRemoved, val);
            return firstRemoved;
        }

        // Can a resizing strategy be found that resizes the set?
        throw new IllegalStateException("No free or removed slots available. Key set full?!!");
    }

    void insertKeyAt(int index, int val) {
        _set[index] = val; // insert value
        _states[index] = FULL;
    }

    protected int XinsertKey(int key) {
        int hash, probe, index, length;

        final byte[] states = _states;
        final int[] set = _set;
        length = states.length;
        hash = HashFunctions2.hash(key) & 0x7fffffff;
        index = hash % length;
        byte state = states[index];

        consumeFreeSlot = false;

        if (state == FREE) {
            consumeFreeSlot = true;
            set[index] = key;
            states[index] = FULL;

            return index; // empty, all done
        } else if (state == FULL && set[index] == key) {
            return -index - 1; // already stored
        } else { // already FULL or REMOVED, must probe
            // compute the double hash
            probe = 1 + (hash % (length - 2));

            // if the slot we landed on is FULL (but not removed), probe
            // until we find an empty slot, a REMOVED slot, or an element
            // equal to the one we are trying to insert.
            // finding an empty slot means that the value is not present
            // and that we should use that slot as the insertion point;
            // finding a REMOVED slot means that we need to keep searching,
            // however we want to remember the offset of that REMOVED slot
            // so we can reuse it in case a "new" insertion (i.e. not an update)
            // is possible.
            // finding a matching value means that we've found that our desired
            // key is already in the table

            if (state != REMOVED) {
                // starting at the natural offset, probe until we find an
                // offset that isn't full.
                do {
                    index -= probe;
                    if (index < 0) {
                        index += length;
                    }
                    state = states[index];
                } while (state == FULL && set[index] != key);
            }

            // if the index we found was removed: continue probing until we
            // locate a free location or an element which equal()s the
            // one we have.
            if (state == REMOVED) {
                int firstRemoved = index;
                while (state != FREE && (state == REMOVED || set[index] != key)) {
                    index -= probe;
                    if (index < 0) {
                        index += length;
                    }
                    state = states[index];
                }

                if (state == FULL) {
                    return -index - 1;
                } else {
                    set[index] = key;
                    states[index] = FULL;

                    return firstRemoved;
                }
            }
            // if it's full, the key is already stored
            if (state == FULL) {
                return -index - 1;
            } else {
                consumeFreeSlot = true;
                set[index] = key;
                states[index] = FULL;

                return index;
            }
        }
    }

} // TIntLongHash

abstract class TPrimitiveHash2 extends THash2 {
    @SuppressWarnings({ "UnusedDeclaration" })
    static final long serialVersionUID = 1L;

    /**
     * flags indicating whether each position in the hash is FREE, FULL, or REMOVED
     */
    public transient byte[] _states;

    /* constants used for state flags */

    /** flag indicating that a slot in the hashtable is available */
    public static final byte FREE = 0;

    /** flag indicating that a slot in the hashtable is occupied */
    public static final byte FULL = 1;

    /**
     * flag indicating that the value of a slot in the hashtable was deleted
     */
    public static final byte REMOVED = 2;

    /**
     * Creates a new <code>THash</code> instance with the default capacity and load factor.
     */
    public TPrimitiveHash2() {
        super();
    }

    /**
     * Creates a new <code>TPrimitiveHash</code> instance with a prime capacity at or near the specified capacity and
     * with the default load factor.
     *
     * @param initialCapacity
     *            an <code>int</code> value
     */
    public TPrimitiveHash2(int initialCapacity) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR);
    }

    /**
     * Creates a new <code>TPrimitiveHash</code> instance with a prime capacity at or near the minimum needed to hold
     * <tt>initialCapacity<tt> elements with load factor
     * <tt>loadFactor</tt> without triggering a rehash.
     *
     * @param initialCapacity
     *            an <code>int</code> value
     * @param loadFactor
     *            a <code>float</code> value
     */
    public TPrimitiveHash2(int initialCapacity, float loadFactor) {
        super();
        initialCapacity = Math.max(1, initialCapacity);
        _loadFactor = loadFactor;
        setUp(HashFunctions2.fastCeil(initialCapacity / loadFactor));
    }

    /**
     * Returns the capacity of the hash table. This is the true physical capacity, without adjusting for the load
     * factor.
     *
     * @return the physical capacity of the hash table.
     */
    public int capacity() {
        return _states.length;
    }

    /**
     * Delete the record at <tt>index</tt>.
     *
     * @param index
     *            an <code>int</code> value
     */
    protected void removeAt(int index) {
        _states[index] = REMOVED;
        super.removeAt(index);
    }

    /**
     * initializes the hashtable to a prime capacity which is at least <tt>initialCapacity + 1</tt>.
     *
     * @param initialCapacity
     *            an <code>int</code> value
     * @return the actual capacity chosen
     */
    protected int setUp(int initialCapacity) {
        int capacity;

        capacity = super.setUp(initialCapacity);
        _states = new byte[capacity];
        return capacity;
    }
} // TPrimitiveHash

abstract class THash2 {
    @SuppressWarnings({ "UnusedDeclaration" })
    static final long serialVersionUID = -1792948471915530295L;

    /** the load above which rehashing occurs. */
    protected static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     * the default initial capacity for the hash table. This is one less than a prime value because one is added to it
     * when searching for a prime capacity to account for the free slot required by open addressing. Thus, the real
     * default capacity is 11.
     */
    protected static final int DEFAULT_CAPACITY = 10;

    /** the current number of occupied slots in the hash. */
    protected transient int _size;

    /** the current number of free slots in the hash. */
    protected transient int _free;

    /**
     * Determines how full the internal table can become before rehashing is required. This must be a value in the
     * range: 0.0 < loadFactor < 1.0. The default value is 0.5, which is about as large as you can get in open
     * addressing without hurting performance. Cf. Knuth, Volume 3., Chapter 6.
     */
    protected float _loadFactor;

    /**
     * The maximum number of elements allowed without allocating more space.
     */
    protected int _maxSize;

    /** The number of removes that should be performed before an auto-compaction occurs. */
    protected int _autoCompactRemovesRemaining;

    /**
     * The auto-compaction factor for the table.
     *
     * @see #setAutoCompactionFactor
     */
    protected float _autoCompactionFactor;

    /**
     * @see #tempDisableAutoCompaction
     */
    protected transient boolean _autoCompactTemporaryDisable = false;

    /**
     * Creates a new <code>THash</code> instance with the default capacity and load factor.
     */
    public THash2() {
        this(DEFAULT_CAPACITY, DEFAULT_LOAD_FACTOR);
    }

    /**
     * Creates a new <code>THash</code> instance with a prime capacity at or near the specified capacity and with the
     * default load factor.
     *
     * @param initialCapacity
     *            an <code>int</code> value
     */
    public THash2(int initialCapacity) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR);
    }

    /**
     * Creates a new <code>THash</code> instance with a prime capacity at or near the minimum needed to hold
     * <tt>initialCapacity</tt> elements with load factor <tt>loadFactor</tt> without triggering a rehash.
     *
     * @param initialCapacity
     *            an <code>int</code> value
     * @param loadFactor
     *            a <code>float</code> value
     */
    public THash2(int initialCapacity, float loadFactor) {
        super();
        _loadFactor = loadFactor;

        // Through testing, the load factor (especially the default load factor) has been
        // found to be a pretty good starting auto-compaction factor.
        _autoCompactionFactor = loadFactor;

        setUp(HashFunctions2.fastCeil(initialCapacity / loadFactor));
    }

    /**
     * Tells whether this set is currently holding any elements.
     *
     * @return a <code>boolean</code> value
     */
    public boolean isEmpty() {
        return 0 == _size;
    }

    /**
     * Returns the number of distinct elements in this collection.
     *
     * @return an <code>int</code> value
     */
    public int size() {
        return _size;
    }

    /**
     * @return the current physical capacity of the hash table.
     */
    abstract public int capacity();

    /**
     * Ensure that this hashtable has sufficient capacity to hold
     * <tt>desiredCapacity<tt> <b>additional</b> elements without
     * requiring a rehash.  This is a tuning method you can call
     * before doing a large insert.
     *
     * @param desiredCapacity an <code>int</code> value
     */
    public void ensureCapacity(int desiredCapacity) {
        if (desiredCapacity > (_maxSize - size())) {
            rehash(PrimeFinder2.nextPrime(
                    Math.max(size() + 1, HashFunctions2.fastCeil((desiredCapacity + size()) / _loadFactor) + 1)));
            computeMaxSize(capacity());
        }
    }

    /**
     * Compresses the hashtable to the minimum prime size (as defined by PrimeFinder) that will hold all of the elements
     * currently in the table. If you have done a lot of <tt>remove</tt> operations and plan to do a lot of queries or
     * insertions or iteration, it is a good idea to invoke this method. Doing so will accomplish two things:
     * <p/>
     * <ol>
     * <li>You'll free memory allocated to the table but no longer needed because of the remove()s.</li>
     * <p/>
     * <li>You'll get better query/insert/iterator performance because there won't be any <tt>REMOVED</tt> slots to skip
     * over when probing for indices in the table.</li>
     * </ol>
     */
    public void compact() {
        // need at least one free spot for open addressing
        rehash(PrimeFinder2.nextPrime(Math.max(_size + 1, HashFunctions2.fastCeil(size() / _loadFactor) + 1)));
        computeMaxSize(capacity());

        // If auto-compaction is enabled, re-determine the compaction interval
        if (_autoCompactionFactor != 0) {
            computeNextAutoCompactionAmount(size());
        }
    }

    /**
     * The auto-compaction factor controls whether and when a table performs a {@link #compact} automatically after a
     * certain number of remove operations. If the value is non-zero, the number of removes that need to occur for
     * auto-compaction is the size of table at the time of the previous compaction (or the initial capacity) multiplied
     * by this factor.
     * <p/>
     * Setting this value to zero will disable auto-compaction.
     *
     * @param factor
     *            a <tt>float</tt> that indicates the auto-compaction factor
     */
    public void setAutoCompactionFactor(float factor) {
        if (factor < 0) {
            throw new IllegalArgumentException("Factor must be >= 0: " + factor);
        }

        _autoCompactionFactor = factor;
    }

    /**
     * @see #setAutoCompactionFactor
     *
     * @return a <<tt>float</tt> that represents the auto-compaction factor.
     */
    public float getAutoCompactionFactor() {
        return _autoCompactionFactor;
    }

    /**
     * This simply calls {@link #compact compact}. It is included for symmetry with other collection classes. Note that
     * the name of this method is somewhat misleading (which is why we prefer <tt>compact</tt>) as the load factor may
     * require capacity above and beyond the size of this collection.
     *
     * @see #compact
     */
    public final void trimToSize() {
        compact();
    }

    /**
     * Delete the record at <tt>index</tt>. Reduces the size of the collection by one.
     *
     * @param index
     *            an <code>int</code> value
     */
    protected void removeAt(int index) {
        _size--;

        // If auto-compaction is enabled, see if we need to compact
        if (_autoCompactionFactor != 0) {
            _autoCompactRemovesRemaining--;

            if (!_autoCompactTemporaryDisable && _autoCompactRemovesRemaining <= 0) {
                // Do the compact
                // NOTE: this will cause the next compaction interval to be calculated
                compact();
            }
        }
    }

    /** Empties the collection. */
    public void clear() {
        _size = 0;
        _free = capacity();
    }

    /**
     * initializes the hashtable to a prime capacity which is at least <tt>initialCapacity + 1</tt>.
     *
     * @param initialCapacity
     *            an <code>int</code> value
     * @return the actual capacity chosen
     */
    protected int setUp(int initialCapacity) {
        int capacity;

        capacity = PrimeFinder2.nextPrime(initialCapacity);
        computeMaxSize(capacity);
        computeNextAutoCompactionAmount(initialCapacity);

        return capacity;
    }

    /**
     * Rehashes the set.
     *
     * @param newCapacity
     *            an <code>int</code> value
     */
    protected abstract void rehash(int newCapacity);

    /**
     * Temporarily disables auto-compaction. MUST be followed by calling {@link #reenableAutoCompaction}.
     */
    public void tempDisableAutoCompaction() {
        _autoCompactTemporaryDisable = true;
    }

    /**
     * Re-enable auto-compaction after it was disabled via {@link #tempDisableAutoCompaction()}.
     *
     * @param check_for_compaction
     *            True if compaction should be performed if needed before returning. If false, no compaction will be
     *            performed.
     */
    public void reenableAutoCompaction(boolean check_for_compaction) {
        _autoCompactTemporaryDisable = false;

        if (check_for_compaction && _autoCompactRemovesRemaining <= 0 && _autoCompactionFactor != 0) {

            // Do the compact
            // NOTE: this will cause the next compaction interval to be calculated
            compact();
        }
    }

    /**
     * Computes the values of maxSize. There will always be at least one free slot required.
     *
     * @param capacity
     *            an <code>int</code> value
     */
    protected void computeMaxSize(int capacity) {
        // need at least one free slot for open addressing
        _maxSize = Math.min(capacity - 1, (int) (capacity * _loadFactor));
        _free = capacity - _size; // reset the free element count
    }

    /**
     * Computes the number of removes that need to happen before the next auto-compaction will occur.
     *
     * @param size
     *            an <tt>int</tt> that sets the auto-compaction limit.
     */
    protected void computeNextAutoCompactionAmount(int size) {
        if (_autoCompactionFactor != 0) {
            // NOTE: doing the round ourselves has been found to be faster than using
            // Math.round.
            _autoCompactRemovesRemaining = (int) ((size * _autoCompactionFactor) + 0.5f);
        }
    }

    /**
     * After an insert, this hook is called to adjust the size/free values of the set and to perform rehashing if
     * necessary.
     *
     * @param usedFreeSlot
     *            the slot
     */
    protected final void postInsertHook(boolean usedFreeSlot) {
        if (usedFreeSlot) {
            _free--;
        }

        // rehash whenever we exhaust the available space in the table
        if (++_size > _maxSize || _free == 0) {
            // choose a new capacity suited to the new state of the table
            // if we've grown beyond our maximum size, double capacity;
            // if we've exhausted the free spots, rehash to the same capacity,
            // which will free up any stale removed slots for reuse.
            int newCapacity = _size > _maxSize ? PrimeFinder2.nextPrime(capacity() << 1) : capacity();
            rehash(newCapacity);
            computeMaxSize(capacity());
        }
    }

    protected int calculateGrownCapacity() {
        return capacity() << 1;
    }

}// THash

final class HashFunctions2 {
    /**
     * Returns a hashcode for the specified value.
     *
     * @return a hash code value for the specified value.
     */
    public static int hash(double value) {
        assert!Double.isNaN(value) : "Values of NaN are not supported.";

        long bits = Double.doubleToLongBits(value);
        return (int) (bits ^ (bits >>> 32));
        // return (int) Double.doubleToLongBits(value*663608941.737);
        // this avoids excessive hashCollisions in the case values are
        // of the form (1.0, 2.0, 3.0, ...)
    }

    /**
     * Returns a hashcode for the specified value.
     *
     * @return a hash code value for the specified value.
     */
    public static int hash(float value) {
        assert!Float.isNaN(value) : "Values of NaN are not supported.";

        return Float.floatToIntBits(value * 663608941.737f);
        // this avoids excessive hashCollisions in the case values are
        // of the form (1.0, 2.0, 3.0, ...)
    }

    /**
     * Returns a hashcode for the specified value.
     *
     * @return a hash code value for the specified value.
     */
    public static int hash(int value) {
        return value;
    }

    /**
     * Returns a hashcode for the specified value.
     *
     * @return a hash code value for the specified value.
     */
    public static int hash(long value) {
        return ((int) (value ^ (value >>> 32)));
    }

    /**
     * Returns a hashcode for the specified object.
     *
     * @return a hash code value for the specified object.
     */
    public static int hash(Object object) {
        return object == null ? 0 : object.hashCode();
    }

    /**
     * In profiling, it has been found to be faster to have our own local implementation of "ceil" rather than to call
     * to {@link Math#ceil(double)}.
     */
    public static int fastCeil(float v) {
        int possible_result = (int) v;
        if (v - possible_result > 0)
            possible_result++;
        return possible_result;
    }
}

final class PrimeFinder2 {
    /**
     * The largest prime this class can generate; currently equal to <tt>Integer.MAX_VALUE</tt>.
     */
    public static final int largestPrime = Integer.MAX_VALUE; // yes, it is prime.

    /**
     * The prime number list consists of 11 chunks.
     *
     * Each chunk contains prime numbers.
     *
     * A chunk starts with a prime P1. The next element is a prime P2. P2 is the smallest prime for which holds: P2 >=
     * 2*P1.
     *
     * The next element is P3, for which the same holds with respect to P2, and so on.
     *
     * Chunks are chosen such that for any desired capacity >= 1000 the list includes a prime number <= desired capacity
     * * 1.11.
     *
     * Therefore, primes can be retrieved which are quite close to any desired capacity, which in turn avoids wasting
     * memory.
     *
     * For example, the list includes 1039,1117,1201,1277,1361,1439,1523,1597,1759,1907,2081.
     *
     * So if you need a prime >= 1040, you will find a prime <= 1040*1.11=1154.
     *
     * Chunks are chosen such that they are optimized for a hashtable growthfactor of 2.0;
     *
     * If your hashtable has such a growthfactor then, after initially "rounding to a prime" upon hashtable
     * construction, it will later expand to prime capacities such that there exist no better primes.
     *
     * In total these are about 32*10=320 numbers -> 1 KB of static memory needed.
     *
     * If you are stingy, then delete every second or fourth chunk.
     */

    private static final int[] primeCapacities = {
            // chunk #0
            largestPrime,

            // chunk #1
            5, 11, 23, 47, 97, 197, 397, 797, 1597, 3203, 6421, 12853, 25717, 51437, 102877, 205759, 411527, 823117,
            1646237, 3292489, 6584983, 13169977, 26339969, 52679969, 105359939, 210719881, 421439783, 842879579,
            1685759167,

            // chunk #2
            433, 877, 1759, 3527, 7057, 14143, 28289, 56591, 113189, 226379, 452759, 905551, 1811107, 3622219, 7244441,
            14488931, 28977863, 57955739, 115911563, 231823147, 463646329, 927292699, 1854585413,

            // chunk #3
            953, 1907, 3821, 7643, 15287, 30577, 61169, 122347, 244703, 489407, 978821, 1957651, 3915341, 7830701,
            15661423, 31322867, 62645741, 125291483, 250582987, 501165979, 1002331963, 2004663929,

            // chunk #4
            1039, 2081, 4177, 8363, 16729, 33461, 66923, 133853, 267713, 535481, 1070981, 2141977, 4283963, 8567929,
            17135863, 34271747, 68543509, 137087021, 274174111, 548348231, 1096696463,

            // chunk #5
            31, 67, 137, 277, 557, 1117, 2237, 4481, 8963, 17929, 35863, 71741, 143483, 286973, 573953, 1147921,
            2295859, 4591721, 9183457, 18366923, 36733847, 73467739, 146935499, 293871013, 587742049, 1175484103,

            // chunk #6
            599, 1201, 2411, 4831, 9677, 19373, 38747, 77509, 155027, 310081, 620171, 1240361, 2480729, 4961459,
            9922933, 19845871, 39691759, 79383533, 158767069, 317534141, 635068283, 1270136683,

            // chunk #7
            311, 631, 1277, 2557, 5119, 10243, 20507, 41017, 82037, 164089, 328213, 656429, 1312867, 2625761, 5251529,
            10503061, 21006137, 42012281, 84024581, 168049163, 336098327, 672196673, 1344393353,

            // chunk #8
            3, 7, 17, 37, 79, 163, 331, 673, 1361, 2729, 5471, 10949, 21911, 43853, 87719, 175447, 350899, 701819,
            1403641, 2807303, 5614657, 11229331, 22458671, 44917381, 89834777, 179669557, 359339171, 718678369,
            1437356741,

            // chunk #9
            43, 89, 179, 359, 719, 1439, 2879, 5779, 11579, 23159, 46327, 92657, 185323, 370661, 741337, 1482707,
            2965421, 5930887, 11861791, 23723597, 47447201, 94894427, 189788857, 379577741, 759155483, 1518310967,

            // chunk #10
            379, 761, 1523, 3049, 6101, 12203, 24407, 48817, 97649, 195311, 390647, 781301, 1562611, 3125257, 6250537,
            12501169, 25002389, 50004791, 100009607, 200019221, 400038451, 800076929, 1600153859 };

    static { // initializer
        // The above prime numbers are formatted for human readability.
        // To find numbers fast, we sort them once and for all.

        Arrays.sort(primeCapacities);
    }

    /**
     * Returns a prime number which is <code>&gt;= desiredCapacity</code> and very close to <code>desiredCapacity</code>
     * (within 11% if <code>desiredCapacity &gt;= 1000</code>).
     *
     * @param desiredCapacity
     *            the capacity desired by the user.
     * @return the capacity which should be used for a hashtable.
     */
    public static final int nextPrime(int desiredCapacity) {
        int i = Arrays.binarySearch(primeCapacities, desiredCapacity);
        if (i < 0) {
            // desired capacity not found, choose next prime greater
            // than desired capacity
            i = -i - 1; // remember the semantics of binarySearch...
        }
        return primeCapacities[i];
    }
}
