/* The Computer Language Benchmarks Game
   http://benchmarksgame.alioth.debian.org/

   contributed by Trey White
   based on Java version by Mark C Lewis
   iterative refinement of rsqrt inspired by Branimir Maksimovic
*/
   

#include <cassert>
#include <cstdio>
#include <immintrin.h>
#include <string>


static constexpr double DT = 0.01;
static constexpr int N = 5;
static constexpr int NxN = 10;

static constexpr double PI = 3.141592653589793;
static constexpr double SOLAR_MASS = 4*PI*PI;
static constexpr double DAYS_PER_YEAR = 365.24;

static constexpr double DIV_SOLAR_MASS = 1.0/SOLAR_MASS;


struct DD 
// Wrapper class for SSE 128-bit packed-double type
{
	__m128d m;
	
	inline DD(): m(_mm_setzero_pd()) {}
	inline DD(const __m128d m): m(m) {}
	inline DD(const DD &that): m(that.m) {}
	inline DD(const double d0) : m(_mm_set1_pd(d0)) {}
	inline DD(const double d0, const double d1): m(_mm_set_pd(d1,d0)) {}
	
	inline DD operator+(const DD that) const
	{ return DD(_mm_add_pd(m,that.m)); }
	
	inline DD operator-(const DD that) const
	{ return DD(_mm_sub_pd(m,that.m)); }
	
	inline DD operator*(const DD that) const
	{ return DD(_mm_mul_pd(m,that.m)); }
	
	inline void operator+=(const DD that)
	{ m = _mm_add_pd(m,that.m); }
	
	inline void operator-=(const DD that)
	{ m = _mm_sub_pd(m,that.m); }
	
	inline void operator*=(const DD that)
	{ m = _mm_mul_pd(m,that.m); }
	
	inline void drsqrt()
	{ m = _mm_div_pd(_mm_set_pd1(1.0),_mm_sqrt_pd(m)); }
	
	void print() const
	{
		const double *const ds = reinterpret_cast<const double *>(&(m));
		printf("%.16f %.16f ",ds[0],ds[1]);
	}

	inline void setLo(const double d)
	{
		const __m128d b0 = _mm_set_sd(d);
		m = _mm_shuffle_pd(b0,m,2);
	}
};

inline DD hi0(const DD dd)
{ return DD(_mm_shuffle_pd(dd.m,_mm_setzero_pd(),1)); }

inline DD hiHi(const DD dd)
{ return DD(_mm_shuffle_pd(dd.m,dd.m,3)); }

inline DD hiLo(const DD aa, const DD bb)
{ return DD(_mm_shuffle_pd(aa.m,bb.m,1)); }

inline DD lo0(const DD dd)
{ return DD(_mm_shuffle_pd(dd.m,_mm_setzero_pd(),0)); }

inline DD loHi(const DD aa, const DD bb)
{ return DD(_mm_shuffle_pd(aa.m,bb.m,2)); }

inline DD loLo(const DD dd)
{ return DD(_mm_shuffle_pd(dd.m,dd.m,0)); }

inline DD nHiLo(const DD dd)
{
	const __m128d swap = _mm_shuffle_pd(dd.m,dd.m,1);
	return DD(_mm_addsub_pd(_mm_setzero_pd(),swap));
}

inline DD refine(const DD xx, const DD dd)
{ return xx*(DD(1.5)-DD(0.5)*xx*xx*dd); }

inline void rsqrt(DD &dd)
{
	const __m128 aa00 = _mm_cvtpd_ps(dd.m);
	const __m128 ones = _mm_set1_ps(1.0f);
	const __m128 aa11 = _mm_movelh_ps(aa00,ones);
	const __m128 rr11 = _mm_rsqrt_ps(aa11);
	const DD rr(_mm_cvtps_pd(rr11));
	dd = refine(rr,dd);
}

inline void rsqrt(DD &aa, DD &bb)
{
	const __m128 aa00 = _mm_cvtpd_ps(aa.m);
	const __m128 bb00 = _mm_cvtpd_ps(bb.m);
	const __m128 aabb = _mm_movelh_ps(aa00,bb00);
	const __m128 rrss = _mm_rsqrt_ps(aabb);
	const DD rr(_mm_cvtps_pd(rrss));
	const DD ss(_mm_cvtps_pd(_mm_movehl_ps(rrss,rrss)));
	aa = refine(rr,aa);
	bb = refine(ss,bb);
}

inline double sum(const DD dd)
{ return _mm_cvtsd_f64(_mm_hadd_pd(dd.m,dd.m)); }class NxNDoubles;


struct NDoubles 
// Unrolled 5-double vectors for N bodies
{
	DD dd0, dd1, dd2;
	
	inline NDoubles(const double d0, const double d1, const double d2, const double d3, const double d4):
		dd0(d0,d1),
		dd1(d2,d3),
		dd2(d4)
	{}
	
	inline NDoubles(const NDoubles &that) = default;
	
	inline NDoubles(const NDoubles &x, const NDoubles &y, const NDoubles &z)
	{
		dd0 = x.dd0*x.dd0+y.dd0*y.dd0+z.dd0*z.dd0;
		dd1 = x.dd1*x.dd1+y.dd1*y.dd1+z.dd1*z.dd1;
		dd2 = x.dd2*x.dd2+y.dd2*y.dd2+z.dd2*z.dd2;
	}
	
	inline void operator+=(const NDoubles &that)
	{
		dd0 += that.dd0;
		dd1 += that.dd1;
		dd2 += that.dd2;
	}
	
	inline void operator*=(const NDoubles &that)
	{
		dd0 *= that.dd0;
		dd1 *= that.dd1;
		dd2 *= that.dd2;
	}
	
	inline void operator*=(const double x)
	{
		const DD xx(x);
		dd0 *= xx;
		dd1 *= xx;
		dd2 *= xx;
	}
	
	void print() const
	{
		dd0.print();
		dd1.print();
		dd2.print();
		printf("\n");
	}
		
	inline void set0(const double d0)
	{ dd0.setLo(d0); }
	
	inline double sum() const 
	{ return ::sum(dd0+dd1+lo0(dd2)); }
	
	inline void advance(const NxNDoubles &nxn, const NDoubles &m);
};


struct NxNDoubles 
// Unrolled 10-double vectors for nested loops
// for (int i = 0; i < N; ++i) for (int j = i+1; j < N; ++j)
{
	DD dd0, dd1, dd2, dd3, dd4, dd5;
	
	inline NxNDoubles(const NDoubles &n):
		dd0(loLo(n.dd0)),
		dd1(dd0),
		dd2(hiHi(n.dd0)),
		dd3(hiLo(n.dd0,n.dd1)),
		dd4(n.dd1)
	{}
	
	inline NxNDoubles(const NxNDoubles &x, const NxNDoubles &y, const NxNDoubles &z)
	{
		dd0 = x.dd0*x.dd0+y.dd0*y.dd0+z.dd0*z.dd0; 
		dd1 = x.dd1*x.dd1+y.dd1*y.dd1+z.dd1*z.dd1; 
		dd2 = x.dd2*x.dd2+y.dd2*y.dd2+z.dd2*z.dd2; 
		dd3 = x.dd3*x.dd3+y.dd3*y.dd3+z.dd3*z.dd3; 
		dd4 = x.dd4*x.dd4+y.dd4*y.dd4+z.dd4*z.dd4;
	}
	
	inline void operator*=(const NxNDoubles &that)
	{
		dd0 *= that.dd0;
		dd1 *= that.dd1;
		dd2 *= that.dd2;
		dd3 *= that.dd3;
		dd4 *= that.dd4;
	}
	
	inline void operator*=(const double x)
	{
		const DD xx(x);
		dd0 *= xx;
		dd1 *= xx;
		dd2 *= xx;
		dd3 *= xx;
		dd4 *= xx;
	}
	
	inline void cross(const NDoubles &n)
	{
		dd0 *= hiLo(n.dd0,n.dd1);
		dd1 *= hiLo(n.dd1,n.dd2);
		dd2 *= n.dd1;
		dd3 *= loHi(n.dd2,n.dd1);
		dd4 *= loLo(n.dd2);
	}
	
	inline void diff(const NDoubles &n)
	{
		dd0 -= hiLo(n.dd0,n.dd1);
		dd1 -= hiLo(n.dd1,n.dd2);
		dd2 -= n.dd1;
		dd3 -= loHi(n.dd2,n.dd1);
		dd4 -= loLo(n.dd2);
	}
	
	inline void drsqrt()
	{
		dd0.drsqrt();
		dd1.drsqrt();
		dd2.drsqrt();
		dd3.drsqrt();
		dd4.drsqrt();
	}

	void print() const
	{
		dd0.print();
		dd1.print();
		dd2.print();
		dd3.print();
		dd4.print();
		printf("\n");
	}
	
	inline void rsqrt()
	{
		::rsqrt(dd0,dd1);
		::rsqrt(dd2,dd3);
		::rsqrt(dd4);
	}
		
	inline double sum() const
	{ return ::sum(dd0+dd1+dd2+dd3+dd4); }	
};

inline void NDoubles::advance(const NxNDoubles &nxn, const NDoubles &n)
{
	dd0 += nHiLo(n.dd0)*loLo(nxn.dd0)
		-loLo(n.dd1)*hiLo(nxn.dd0,nxn.dd2)
		-hiHi(n.dd1)*loHi(nxn.dd1,nxn.dd2)
		-loLo(n.dd2)*hiLo(nxn.dd1,nxn.dd3);
		
	dd1 += loLo(n.dd0)*hiLo(nxn.dd0,nxn.dd1)
		+hiHi(n.dd0)*nxn.dd2
		+nHiLo(n.dd1)*hiHi(nxn.dd3)
		-loLo(n.dd2)*nxn.dd4;
		
	dd2 += n.dd0*hi0(nxn.dd1)
		+hi0(n.dd0)*nxn.dd3
		+lo0(n.dd1)*nxn.dd4
		+hi0(n.dd1)*hi0(nxn.dd4);
}


struct Bodies {

	NDoubles x, y, z, vx, vy, vz, mass;
	
	void offsetMomentum(const double px, const double py, const double pz)
	{
		vx.set0(-px*DIV_SOLAR_MASS);
		vy.set0(-py*DIV_SOLAR_MASS);
		vz.set0(-pz*DIV_SOLAR_MASS);
	}
	
	Bodies():
	
		x(0.0,4.84143144246472090e+00,
			8.34336671824457987e+00,
			1.28943695621391310e+01,
			1.53796971148509165e+01),
			
		y(0.0,-1.16032004402742839e+00,
			4.12479856412430479e+00,
			-1.51111514016986312e+01,
			-2.59193146099879641e+01),
			
		z(0.0,-1.03622044471123109e-01,
			-4.03523417114321381e-01,
			-2.23307578892655734e-01,
			1.79258772950371181e-01),
			
		vx(0.0,1.66007664274403694e-03*DAYS_PER_YEAR,
			-2.76742510726862411e-03*DAYS_PER_YEAR,
			2.96460137564761618e-03*DAYS_PER_YEAR,
			2.68067772490389322e-03*DAYS_PER_YEAR),
			
		vy(0.0,7.69901118419740425e-03*DAYS_PER_YEAR,
			4.99852801234917238e-03*DAYS_PER_YEAR,
			2.37847173959480950e-03*DAYS_PER_YEAR,
			1.62824170038242295e-03*DAYS_PER_YEAR),
			
		vz(0.0,
			-6.90460016972063023e-05*DAYS_PER_YEAR,
			2.30417297573763929e-05*DAYS_PER_YEAR,
			-2.96589568540237556e-05*DAYS_PER_YEAR,
			-9.51592254519715870e-05*DAYS_PER_YEAR),
			
		mass(SOLAR_MASS,
			9.54791938424326609e-04*SOLAR_MASS,
			2.85885980666130812e-04*SOLAR_MASS,
			4.36624404335156298e-05*SOLAR_MASS,
			5.15138902046611451e-05*SOLAR_MASS)
	{}
};


struct NBodySystem {

	Bodies bodies;

	NBodySystem()
	{
		NDoubles px(bodies.vx);
		NDoubles py(bodies.vy);
		NDoubles pz(bodies.vz);
		px *= bodies.mass;
		py *= bodies.mass;
		pz *= bodies.mass;
		bodies.offsetMomentum(px.sum(),py.sum(),pz.sum());
	}
	
	inline void advance(const double dt)
	{
		NxNDoubles dx(bodies.x);
		NxNDoubles dy(bodies.y);
		NxNDoubles dz(bodies.z);
		
		dx.diff(bodies.x);
		dy.diff(bodies.y);
		dz.diff(bodies.z);
				
		NxNDoubles d(dx,dy,dz);
		d.rsqrt();
		
		NxNDoubles mag(d);
		mag *= d;
		mag *= d;
		mag *= dt;
		
		dx *= mag;
		dy *= mag;
		dz *= mag;
				
		bodies.vx.advance(dx,bodies.mass);
		bodies.vy.advance(dy,bodies.mass);
		bodies.vz.advance(dz,bodies.mass);
		
		NDoubles vx(bodies.vx);
		NDoubles vy(bodies.vy);
		NDoubles vz(bodies.vz);
		
		vx *= dt;
		vy *= dt;
		vz *= dt;

		bodies.x += vx;
		bodies.y += vy;
		bodies.z += vz;
	}
	
	inline double energy() const
	{
		NDoubles v(bodies.vx,bodies.vy,bodies.vz);
		v *= bodies.mass;
		double e = 0.5*v.sum();
		
		NxNDoubles dx(bodies.x);
		NxNDoubles dy(bodies.y);
		NxNDoubles dz(bodies.z);
		
		dx.diff(bodies.x);
		dy.diff(bodies.y);
		dz.diff(bodies.z);
		
		NxNDoubles d(dx,dy,dz);
		d.drsqrt();
		
		NxNDoubles m(bodies.mass);
		m.cross(bodies.mass);
		m *= d;
		e -= m.sum();
		
		return e;
	}
};


int main(const int argc, const char *const *const argv)
{
	assert(2 == argc);
	const int n = std::stoi(argv[1]);
	assert(0 <= n);
	NBodySystem system;
	printf("%.9f\n",system.energy());
	for (int i = 0; i < n; ++i) system.advance(DT);
	printf("%.9f\n",system.energy());
	return 0;
}
