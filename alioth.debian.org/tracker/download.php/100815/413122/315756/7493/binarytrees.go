/* The Computer Language Benchmarks Game
 * http://benchmarksgame.alioth.debian.org/
 *
 * based on Go program by The Go Authors.
 * based on C program by Kevin Carson
 * flag.Arg hack by Isaac Gouy
 * modified by Jamil Djadala to use goroutines
 * modified by Chai Shushan
 * *reset*
 * modified by Tomasz Kłak to manually manage memory
 *
 */

package main

import (
	"flag"
	"fmt"
	"runtime/debug"
	"strconv"
	"sync"
)

var minDepth = 4
var n = 0

func NewNode() *Node {
	return new(Node)
}

type pool struct {
	objects []*Node
}

func NewPool() *pool {
	return new(pool)
}

func (p *pool) Get() *Node {
	if l := len(p.objects); l > 0 {
		r := p.objects[l-1]
		p.objects = p.objects[:l-1]
		return r
	}
	return NewNode()
}

func (p *pool) Put(n *Node) {
	p.objects = append(p.objects, n)
}

func main() {
	flag.Parse()
	if flag.NArg() > 0 {
		n, _ = strconv.Atoi(flag.Arg(0))
	}

	maxDepth := n
	if minDepth+2 > n {
		maxDepth = minDepth + 2
	}
	stretchDepth := maxDepth + 1

	debug.SetGCPercent(-1)

	globalPool := NewPool()

	check_l := bottomUpTree(stretchDepth, globalPool).ItemCheck(globalPool)
	fmt.Printf("stretch tree of depth %d\t check: %d\n", stretchDepth, check_l)

	longLivedTreeItemCheck := bottomUpTree(maxDepth, globalPool).ItemCheck(globalPool)

	result_trees := make([]int, maxDepth+1)
	result_check := make([]int, maxDepth+1)

	var wg sync.WaitGroup
	for depth_l := minDepth; depth_l <= maxDepth; depth_l += 2 {
		wg.Add(1)
		go func(depth int) {
			iterations := 1 << uint(maxDepth-depth+minDepth)
			check := 0
			localPool := NewPool()

			for i := 1; i <= iterations; i++ {
				check += bottomUpTree(depth, localPool).ItemCheck(localPool)
			}
			result_trees[depth] = iterations
			result_check[depth] = check

			wg.Done()
		}(depth_l)
	}
	wg.Wait()

	for depth := minDepth; depth <= maxDepth; depth += 2 {
		fmt.Printf("%d\t trees of depth %d\t check: %d\n",
			result_trees[depth], depth, result_check[depth],
		)
	}
	fmt.Printf("long lived tree of depth %d\t check: %d\n",
		maxDepth, longLivedTreeItemCheck,
	)
}

func bottomUpTree(depth int, nodePool *pool) *Node {
	n := nodePool.Get()
	if depth <= 0 {
		n.left, n.right = nil, nil
		return n
	}
	n.left = bottomUpTree(depth-1, nodePool)
	n.right = bottomUpTree(depth-1, nodePool)
	return n
}

type Node struct {
	left, right *Node
}

func (self *Node) ItemCheck(pool *pool) int {
	if self.left == nil {
		pool.Put(self)
		return 1
	}
	ret := 1 + self.left.ItemCheck(pool) + self.right.ItemCheck(pool)
	pool.Put(self)
	return ret
}
