/* The Computer Language Benchmarks Game
 * http://benchmarksgame.alioth.debian.org/
 *
 * contributed by The Go Authors.
 * Based on C program by Joern Inge Vestgaarden
 * and Jorge Peixoto de Morais Neto.
 * flag.Arg hack by Isaac Gouy.
 * concurrent generation by INADA Naoki.
 */

package main

import (
	"bufio"
	"flag"
	"math/rand"
	"os"
	"runtime"
	"strconv"
)

var out *bufio.Writer

const WIDTH = 60 // Fold lines after WIDTH bytes

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

type AminoAcid struct {
	p float64
	c byte
}

func AccumulateProbabilities(genelist []AminoAcid) {
	for i := 1; i < len(genelist); i++ {
		genelist[i].p += genelist[i-1].p
	}
}

// RepeatFasta prints the characters of the byte slice s. When it
// reaches the end of the slice, it goes back to the beginning.
// It stops after generating count characters.
// After each WIDTH characters it prints a newline.
// It assumes that WIDTH <= len(s) + 1.
func RepeatFasta(s []byte, count int) {
	pos := 0
	s2 := make([]byte, len(s)+WIDTH)
	copy(s2, s)
	copy(s2[len(s):], s)
	for count > 0 {
		line := min(WIDTH, count)
		out.Write(s2[pos : pos+line])
		out.WriteByte('\n')
		pos += line
		if pos >= len(s) {
			pos -= len(s)
		}
		count -= line
	}
}

type RandomFastaTask struct {
	genelist []AminoAcid
	count    int
	output   []byte
	done     chan int
	rs       *rand.Rand
}

// Each element of genelist is a struct with a character and
// a floating point number p between 0 and 1.
// RandomFasta generates a random float r and
// finds the first element such that p >= r.
// This is a weighted random selection.
// RandomFasta then prints the character of the array element.
// This sequence is repeated count times.
// Between each WIDTH consecutive characters, the function prints a newline.
func RandomFasta(genelist []AminoAcid, rs *rand.Rand, count int, buf []byte) int {
	pos := 0
	for count > 0 {
		line := min(WIDTH, count)
		count -= line
		for i := 0; i < line; i++ {
			r := rs.Float64()
			for _, v := range genelist {
				if v.p >= r {
					buf[pos] = v.c
					break
				}
			}
			pos++
		}
		buf[pos] = '\n'
		pos++
	}
	return pos
}

func RandomFastaWorker(task *RandomFastaTask) {
	task.done <- RandomFasta(task.genelist, task.rs, task.count, task.output)
}

func ConcurrentRandomFasta(genelist []AminoAcid, count int) {
	block := 600000
	n := runtime.NumCPU() + 2
	m := min(n, count/block)

	tasks := make([]RandomFastaTask, m)
	running := 0
	for i := 0; i < m; i++ {
		tasks[i].genelist = genelist
		tasks[i].done = make(chan int, 1)
		tasks[i].output = make([]byte, block+block/WIDTH)
		tasks[i].rs = rand.New(rand.NewSource(rand.Int63()))

		tasks[i].count = min(block, count)
		go RandomFastaWorker(&tasks[i])
		running++
		count -= block
	}

	i := 0
	for running > 0 {
		size := <-tasks[i].done
		out.Write(tasks[i].output[:size])
		if count > 0 {
			tasks[i].count = min(block, count)
			go RandomFastaWorker(&tasks[i])
			count -= block
		} else {
			running--
		}
		i++
		if i >= m {
			i = 0
		}
	}
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	out = bufio.NewWriter(os.Stdout)
	defer out.Flush()

	var n = 0

	flag.Parse()
	if flag.NArg() > 0 {
		n, _ = strconv.Atoi(flag.Arg(0))
	}

	iub := []AminoAcid{
		AminoAcid{0.27, 'a'},
		AminoAcid{0.12, 'c'},
		AminoAcid{0.12, 'g'},
		AminoAcid{0.27, 't'},
		AminoAcid{0.02, 'B'},
		AminoAcid{0.02, 'D'},
		AminoAcid{0.02, 'H'},
		AminoAcid{0.02, 'K'},
		AminoAcid{0.02, 'M'},
		AminoAcid{0.02, 'N'},
		AminoAcid{0.02, 'R'},
		AminoAcid{0.02, 'S'},
		AminoAcid{0.02, 'V'},
		AminoAcid{0.02, 'W'},
		AminoAcid{0.02, 'Y'},
	}

	homosapiens := []AminoAcid{
		AminoAcid{0.3029549426680, 'a'},
		AminoAcid{0.1979883004921, 'c'},
		AminoAcid{0.1975473066391, 'g'},
		AminoAcid{0.3015094502008, 't'},
	}

	AccumulateProbabilities(iub)
	AccumulateProbabilities(homosapiens)

	alu := []byte(
		"GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG" +
			"GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA" +
			"CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT" +
			"ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA" +
			"GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG" +
			"AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC" +
			"AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA")

	out.WriteString(">ONE Homo sapiens alu\n")
	RepeatFasta(alu, 2*n)
	out.WriteString(">TWO IUB ambiguity codes\n")
	ConcurrentRandomFasta(iub, 3*n)
	out.WriteString(">THREE Homo sapiens frequency\n")
	ConcurrentRandomFasta(homosapiens, 5*n)
}
