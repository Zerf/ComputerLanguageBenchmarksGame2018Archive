﻿// The Computer Language Benchmarks Game
// http://benchmarksgame.alioth.debian.org/
//
// Contributed by Kevin Miller
//
// compile with following gcc flags
//  -pipe -Wall -Ofast -fno-finite-math-only -march=native -mfpmath=sse -msse2 -fopenmp


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <emmintrin.h>


inline long numDigits(long n)
{
	long len = 0;
	while(n)
	{
		n=n/10;
		len++;
	}
	return len;
}

int main(int argc, char ** argv)
{
	// get width/height from arguments

	long wid_ht = 16000;
	if (argc >= 2)
	{
		wid_ht = atoi(argv[1]);
	}
	wid_ht = (wid_ht+7) & ~7;


	// allocate memory for header and pixels

	long headerLength = numDigits(wid_ht)*2+5;
	long dataLength = headerLength + wid_ht*(wid_ht>>3);
	unsigned char * const data = malloc(dataLength);
	unsigned char * const pixels = data + headerLength;


	// generate the bitmap header

	sprintf((char *)data, "P4\n%ld %ld\n", wid_ht, wid_ht);


	// calculate initial values, store in r0, i0

	__m128d r0[wid_ht/2];
	double i0[wid_ht];

	for(long xy=0; xy<wid_ht; xy+=2)
	{
		r0[xy>>1] = 2.0 / wid_ht * (__m128d){xy,  xy+1} - 1.5;
		i0[xy]    = 2.0 / wid_ht *  xy    - 1.0;
		i0[xy+1]  = 2.0 / wid_ht * (xy+1) - 1.0;
	}


	// generate the bitmap

	#pragma omp parallel for schedule(guided)
	for(long y=0; y<wid_ht; y++)
	{
		long rowstart = y*wid_ht/8;
		const double init_i = i0[y];
		for(long x=0; x<wid_ht; x+=8)
		{
			__m128d r[4], i[4], sum[4];
			__m128d *init_r = &(r0[x>>1]);
			for(long pair=0; pair<4; pair++)
			{
				r[pair]=init_r[pair];
				i[pair]=(__m128d){init_i,init_i};
			}

			long pix8 = 0xff;

			for (long iteration = 0; iteration < 50; iteration++)
			{
				for(long pair=0; pair<4; pair++)
				{
					__m128d r2 = r[pair] * r[pair];
					__m128d i2 = i[pair] * i[pair];
					__m128d ri = r[pair] * i[pair];

					sum[pair] = r2 + i2;

					r[pair]=r2 - i2 + init_r[pair];
					i[pair]=ri + ri + init_i;
				}

				if (!(sum[0][0] <= 4.0 ||
				sum[0][1] <= 4.0 ||
				sum[1][0] <= 4.0 ||
				sum[1][1] <= 4.0 ||
				sum[2][0] <= 4.0 ||
				sum[2][1] <= 4.0 ||
				sum[3][0] <= 4.0 ||
				sum[3][1] <= 4.0))
				{
					pix8 = 0x00;
					break;
				}
			}

			if (pix8)
			{
				if(!(sum[0][0] <= 4.0)) pix8 &= 0x7f;
				if(!(sum[0][1] <= 4.0)) pix8 &= 0xbf;
				if(!(sum[1][0] <= 4.0)) pix8 &= 0xdf;
				if(!(sum[1][1] <= 4.0)) pix8 &= 0xef;
				if(!(sum[2][0] <= 4.0)) pix8 &= 0xf7;
				if(!(sum[2][1] <= 4.0)) pix8 &= 0xfb;
				if(!(sum[3][0] <= 4.0)) pix8 &= 0xfd;
				if(!(sum[3][1] <= 4.0)) pix8 &= 0xfe;
			}
			pixels[rowstart + x/8]= (unsigned char)pix8;
		}
	}


	// write the data

	long ret = ret = write(STDOUT_FILENO, data, dataLength);


	free(data);

	return 0;
}

