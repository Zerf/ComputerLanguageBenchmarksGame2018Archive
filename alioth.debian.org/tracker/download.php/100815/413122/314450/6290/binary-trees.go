/* The Computer Language Benchmarks Game
 * http://benchmarksgame.alioth.debian.org/
 *
 * contributed by The Go Authors.
 * based on C program by Kevin Carson
 * flag.Arg hack by Isaac Gouy
 * slice based binary tree by Jakob Borg
 */

package main

import (
	"flag"
	"fmt"
	"strconv"
)

var n = 0

type Node struct {
	value       int
	hasChildren bool
}

type Tree []Node

func bottomUpTree(item, depth int) Tree {
	t := make(Tree, 1<<uint(depth+1))
	t.setNodes(0, item, depth)
	return t
}

func (t Tree) setNodes(idx, item, depth int) {
	t[idx].value = item
	if depth > 0 {
		t[idx].hasChildren = true
		t.setNodes(2*idx+1, 2*item-1, depth-1)
		t.setNodes(2*idx+2, 2*item, depth-1)
	}
}

func (t Tree) itemCheck() int {
	return t.itemCheckIdx(0)
}

func (t Tree) itemCheckIdx(idx int) int {
	if !t[idx].hasChildren {
		return t[idx].value
	}
	return t[idx].value + t.itemCheckIdx(2*idx+1) - t.itemCheckIdx(2*idx+2)
}

const minDepth = 4

func main() {
	flag.Parse()
	if flag.NArg() > 0 {
		n, _ = strconv.Atoi(flag.Arg(0))
	}

	maxDepth := n
	if minDepth+2 > n {
		maxDepth = minDepth + 2
	}
	stretchDepth := maxDepth + 1

	check := bottomUpTree(0, stretchDepth).itemCheck()
	fmt.Printf("stretch tree of depth %d\t check: %d\n", stretchDepth, check)

	longLivedTree := bottomUpTree(0, maxDepth)

	for depth := minDepth; depth <= maxDepth; depth += 2 {
		iterations := 1 << uint(maxDepth-depth+minDepth)
		check = 0

		for i := 1; i <= iterations; i++ {
			check += bottomUpTree(i, depth).itemCheck()
			check += bottomUpTree(-i, depth).itemCheck()
		}
		fmt.Printf("%d\t trees of depth %d\t check: %d\n", iterations*2, depth, check)
	}
	fmt.Printf("long lived tree of depth %d\t check: %d\n", maxDepth, longLivedTree.itemCheck())
}
