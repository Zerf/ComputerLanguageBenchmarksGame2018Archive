/* The Computer Language Benchmarks Game
 * http://benchmarksgame.alioth.debian.org/
 *
 * Contributed by Martin Koistinen
 * Based on mandelbrot.c contributed by Greg Buchholz and The Go Authors
 * flag.Arg hack by Isaac Gouy
 *
 * Large changes by Bill Broadley, including:
 * 1) Switching the one goroutine per line to one per CPU
 * 2) Replacing gorouting calls with channels
 * 3) Handling out of order results in the file writer.
 */

package main

import (
   "bufio"
   "flag"
   "fmt"
   "os"
   "strconv"
   "runtime"
   "sync"
)

/* targeting a q6600 system, one cpu worker per core */
const pool = 4

const ZERO float64 = 0
const LIMIT = 2.0
const ITER = 50   // Benchmark parameter
const SIZE = 16000

var rows []byte
var bytesPerRow int

// This func is responsible for rendering a row of pixels,
// and when complete writing it out to the file.

func renderRow(w, h, y0, maxiter int, wg * sync.WaitGroup) {

   var Zr, Zi, Tr, Ti, Cr float64
   var x,i int

   for y := y0; y < h; y += pool {

      offset := bytesPerRow * y
      Ci := (float64(y << 1)/float64(h) - 1.0)

      for x = 0; x < w; x++ {
         Zr, Zi, Tr, Ti = ZERO, ZERO, ZERO, ZERO
         Cr = (float64(x << 1)/float64(w) - 1.5)

         for i = 0; i < maxiter && Tr + Ti <= LIMIT*LIMIT; i++ {
            Zr, Zi = Tr - Ti + Cr, 2*Zr*Zi + Ci
            Tr = Zr*Zr
            Ti = Zi*Zi
         }

         // Store the value in the array of ints
         if Tr + Ti <= LIMIT*LIMIT {
            rows[offset + (x >> 3)] |= (byte(1) << uint(7-(x & int(7))))
         }
      }
   }
   //Signal finish
   wg.Done()
}

func main() {
   runtime.GOMAXPROCS(pool)

   size := SIZE   // Contest settings
   maxiter := ITER

   // Get input, if any...
   flag.Parse()
   if flag.NArg() > 0 {
      size, _ = strconv.Atoi(flag.Arg(0))
   }
   w, h := size, size
   bytesPerRow =  w / 8

   out := bufio.NewWriter(os.Stdout)
   defer out.Flush()
   fmt.Fprintf(out, "P4\n%d %d\n", w, h)

   rows = make([]byte, bytesPerRow*h)

   /* Wait group for finish */
   wg := new( sync.WaitGroup )
   // start pool workers, and assign all work
   for y := 0; y < pool; y++ {
   		wg.Add(1)
    	go renderRow(w, h, y, maxiter, wg)
   }

   /* wait for the file workers to finish, then write */
   wg.Wait()
   out.Write(rows)
}
