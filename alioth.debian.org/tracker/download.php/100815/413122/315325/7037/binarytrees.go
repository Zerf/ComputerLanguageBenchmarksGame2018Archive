package main

import (
    "flag"
    "fmt"
    "strconv"
    "runtime"
    "sync"
)

var n = 0

type Node struct {
    item   int
    left, right   *Node
}

func  bottomUpTree(item, depth int,pool *Pool) *Node {
    if depth <= 0 {
        return pool.GetNode(item,nil,nil)
    }
    return pool.GetNode(item,bottomUpTree(2 * item-1,depth-1,pool),bottomUpTree(2*item,depth-1,pool))
}

func (n *Node) itemCheck() int {
    if n.left == nil {
        return n.item
    }
    return n.item + n.left.itemCheck() - n.right.itemCheck()
}

const minDepth = 4


type Pool struct {
    Size,Index int64
    Base []Node
}

func newPool(base int64) *Pool {
    return &Pool {
        Size : base,
        Index : 0,
        Base : make([]Node, base),
    }
}

func (p *Pool) Clear() {
    p.Index = 0
}



func (p *Pool) GetNode(item int,left,right *Node) *Node {
    if p.Index == p.Size{
        p.Size *= 2
        p.Base = make([]Node ,p.Size)
        p.Index = 0
    }
    n := &p.Base[p.Index]
    p.Index += 1
    n.item = item
    n.left = left
    n.right = right
    return n
}

func main() {
    runtime.GOMAXPROCS( runtime.NumCPU() + 2)

    flag.Parse()
    if flag.NArg() > 0 { n,_ = strconv.Atoi( flag.Arg(0) ) }

    maxDepth := n
    if minDepth + 2 > n {
        maxDepth = minDepth + 2
    }
    stretchDepth := maxDepth + 1

    pool := newPool(1024)
    check_l := bottomUpTree(0, stretchDepth,pool).itemCheck()
    fmt.Printf("stretch tree of depth %d\t check: %d\n", stretchDepth, check_l)

    pool.Clear()
    longLivedTree := bottomUpTree(0, maxDepth,pool)

    var wg sync.WaitGroup
    result := make( []string, maxDepth+1)

    for depth_l := minDepth; depth_l <= maxDepth; depth_l+=2 {
        wg.Add(1)
        go func( depth int, check int, r *string) {
            defer wg.Done()
            iterations := 1 << uint(maxDepth - depth + minDepth)
            check = 0
            pool := newPool(1024)
            for i := 1; i <= iterations; i++ {
                check += bottomUpTree(i,depth,pool).itemCheck()
                pool.Clear()
                check += bottomUpTree(-i,depth,pool).itemCheck()
                pool.Clear()
            }
            *r = fmt.Sprintf("%d\t trees of depth %d\t check: %d", iterations*2, depth, check)
        }( depth_l, check_l, &result[depth_l])
    }
    wg.Wait()
    for depth := minDepth; depth <= maxDepth; depth+=2 {
        fmt.Println( result[depth])
    }
    fmt.Printf("long lived tree of depth %d\t check: %d\n", maxDepth, longLivedTree.itemCheck())

}
