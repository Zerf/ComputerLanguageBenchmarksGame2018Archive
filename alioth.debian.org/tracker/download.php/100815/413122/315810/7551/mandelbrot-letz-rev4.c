/* The Computer Language Benchmarks Game
 http://benchmarksgame.alioth.debian.org/
 contributed by Dominic Letz rev4

 Compile with: gcc -O3 -march=native -fopenmp main.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define _LOOP(F, S) {F} S {F} S {F} S {F}
#define VEC_SIZE 2
#define LOOP_SIZE 4
#define UNROLL(F) {int X=0; _LOOP(F, ;X++;)}
typedef double Vec __attribute__ ((vector_size(16)));
typedef long long VecSi __attribute__ ((vector_size(16)));

// Calculate mandelbrot set for four vectors each 2 doubles into one byte
uint8_t mand(Vec* cr, Vec ci) {
  Vec Zr[LOOP_SIZE] = {0};
  Vec Zi[LOOP_SIZE] = {0};
  Vec Cr[LOOP_SIZE] = {cr[0], cr[1], cr[2], cr[3]};
    
  for (int i = 0; i < 10; i++) {
    Vec Sum[LOOP_SIZE];
    for (int j = 0; j < 5; j++) {
      UNROLL(   
          Vec Ti = Zi[X] * Zi[X];
          Vec Tr = Zr[X] * Zr[X];
          Sum[X] = Tr + Ti;
          Zi[X] = 2 * Zr[X] * Zi[X] + ci;
          Zr[X] = Tr - Ti + Cr[X];                      
      )
    }
    UNROLL(
      VecSi ret = Sum[X] <= 4.0;
      if (ret[0] + ret[1]) continue;
    )
    return 0;
  }

  uint8_t byte = 0;
  UNROLL(
    VecSi ret = Zi[X] * Zi[X] + Zr[X] * Zr[X] <= 4.0;
    byte |= ((ret[0] & 0x80) | (ret[1] & 0x40)) >> (X * VEC_SIZE);
  )
  return byte;
}

uint32_t mand32(Vec* cr, Vec ci) {
  uint32_t ret = 0;
  UNROLL(
    ret |= mand(cr + X*LOOP_SIZE, ci) << X*8;
  )
  return ret;
}

// Parse command line arguments
int main(int argc, char* argv[])
{
  int n = (argc > 1) ? atoi(argv[1]) : 200;
  int N = (n + 31) & ~31;
  double inv = 2.0 / ((double)n);
  Vec xvals[N/VEC_SIZE];
  Vec yvals[n];
  uint32_t *rows = malloc(n*N/8);

  // Preparation
  for (int i = 0; i < N; i++) {
    xvals[i/VEC_SIZE][i%VEC_SIZE] = ((double)i) * inv - 1.5;
    for (int j = 0; j < VEC_SIZE; j++)
      yvals[i][j] = ((double)i) * inv - 1.0;
  }

  // Actual Calculation
  #pragma omp parallel for schedule(guided)
  for (int y = 0; y < n; y++)
    for (int x = 0; x < N/32; x++)
      rows[y*N/32+x] = mand32(xvals + x*16, yvals[y]);
  
  // Write Output
  fprintf(stdout, "P4\n%u %u\n", n, n);
  fwrite(&rows[0], n*N/8, 1, stdout);
  free(rows);
  return 0;
}
