/* The Computer Language Benchmarks Game
 * http://benchmarksgame.alioth.debian.org/
 *
 * based on Go program by The Go Authors.
 * based on C program by Kevin Carson
 * flag.Arg hack by Isaac Gouy
 * modified by Jamil Djadala to use goroutines
 * modified by Chai Shushan
 * modified by Arkadiusz Rychlinski to implement own allocator
 */

package main

import (
	"flag"
	"fmt"
	"runtime"
	"strconv"
	"sync"
)

var minDepth = 4
var n = 0

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU() * 2)

	flag.Parse()
	if flag.NArg() > 0 {
		n, _ = strconv.Atoi(flag.Arg(0))
	}

	maxDepth := n
	if minDepth+2 > n {
		maxDepth = minDepth + 2
	}
	stretchDepth := maxDepth + 1

	mal := createAllocator()

	check_l := bottomUpTree(mal, 0, stretchDepth).ItemCheck()
	fmt.Printf("stretch tree of depth %d\t check: %d\n", stretchDepth, check_l)
	longLivedTree := bottomUpTree(mal, 0, maxDepth)

	result_trees := make([]int, maxDepth+1)
	result_check := make([]int, maxDepth+1)

	var wg sync.WaitGroup
	for depth_l := minDepth; depth_l <= maxDepth; depth_l += 2 {
		wg.Add(1)
		go func(depth int) {

			iterations := 1 << uint(maxDepth-depth+minDepth)
			check := 0

			al := createAllocator()
			for i := 1; i <= iterations; i++ {
				check += bottomUpTree(al, i, depth).ItemCheck()
				check += bottomUpTree(al, -i, depth).ItemCheck()
				al.Reset()
			}
			result_trees[depth] = iterations * 2
			result_check[depth] = check

			wg.Done()
		}(depth_l)
	}
	wg.Wait()

	for depth := minDepth; depth <= maxDepth; depth += 2 {
		fmt.Printf("%d\t trees of depth %d\t check: %d\n",
			result_trees[depth], depth, result_check[depth],
		)
	}
	fmt.Printf("long lived tree of depth %d\t check: %d\n",
		maxDepth, longLivedTree.ItemCheck(),
	)

}

func bottomUpTree(al *allocator, item, depth int) *Node {
	if depth <= 0 {
		return al.newNode(item, nil, nil)
	}
	return al.newNode(item,
		bottomUpTree(al, 2*item-1, depth-1),
		bottomUpTree(al, 2*item, depth-1))
}

const segmentLen = 10000000

type segment [segmentLen]Node
type allocator struct {
	stack          []*segment
	stackTop       int
	currentSegment int
}

func createAllocator() *allocator {
	al := &allocator{make([]*segment, 0, 256), 0, 0}
	al.stack = append(al.stack, new(segment))
	return al
}

func (al *allocator) newNode(item int, left, right *Node) *Node {
	if al.stackTop == segmentLen {
		al.stackTop = 0
		al.currentSegment++
		if al.currentSegment == len(al.stack) {
			al.stack = append(al.stack, new(segment))
		}
	}

	node := &(al.stack[al.currentSegment][al.stackTop])
	al.stackTop++

	node.item = item
	node.left = left
	node.right = right

	return node
}

func (al *allocator) Reset() {
	al.currentSegment = 0
	al.stackTop = 0
}

type Node struct {
	item        int
	left, right *Node
}

func (self *Node) ItemCheck() int {
	if self.left == nil {
		return self.item
	}
	return self.item + self.left.ItemCheck() - self.right.ItemCheck()
}
