// The Computer Language Benchmarks Game
// http://benchmarksgame.alioth.debian.org/
//
// Contributed by Ainar Garipov
// Based on the Java program #5 by Heikki Salokanto
//
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int min_depth = 4;
int max_depth = 8;

char **result_strings;

typedef struct tree_node {
	struct tree_node *left;
	struct tree_node *right;
	int64_t item;
} tree_node;

void *calc_trees(void *pdepth);
int64_t check_tree(tree_node * tree);
tree_node *create_tree(int64_t item, int depth);

int main(int argc, char **argv)
{
	if (argc < 2) {
		puts("please specify the maximum depth");
		return 1;
	}
	max_depth = atoi(argv[1]);
	if (min_depth + 2 > max_depth) {
		max_depth = min_depth + 2;
	}
	int stretch_depth = max_depth + 1;

	tree_node *stretch_tree = create_tree(0, stretch_depth);
	int64_t stretch_check = check_tree(stretch_tree);
	printf("stretch tree of depth %d\t check: %ld\n", stretch_depth,
	       stretch_check);
	free(stretch_tree);

	tree_node *long_lived_tree = create_tree(0, max_depth);

	// We could've used smaller buffers since the step is 2 but it's easier
	// that way.
	result_strings = calloc(max_depth, sizeof(char *));
	pthread_t *threads = calloc(max_depth, sizeof(pthread_t));
	int n_threads = 0;
	for (uintptr_t depth = min_depth; depth <= max_depth; depth += 2) {
		int res =
		    pthread_create(&threads[n_threads++], NULL, calc_trees,
				   (void *)depth);
		if (res != 0) {
			printf("failed to create thread: %d\n", res);
			return 1;
		}
	}

	for (int i = 0; i < n_threads; i++) {
		int res = pthread_join(threads[i], NULL);
		if (res != 0) {
			printf("failed to join thread: %d\n", res);
			return 1;
		}
	}
	free(threads);

	for (int depth = min_depth; depth <= max_depth; depth += 2) {
		puts(result_strings[depth]);
		free(result_strings[depth]);
	}
	free(result_strings);

	int64_t long_lived_check = check_tree(long_lived_tree);
	printf("long lived tree of depth %d\t check: %ld\n", max_depth,
	       long_lived_check);
	free(long_lived_tree);

	return 0;
}

void *calc_trees(void *pdepth)
{
	intptr_t depth = (intptr_t) pdepth;
	int iterations = 1 << (max_depth - depth + min_depth);
	int check = 0;

	for (int64_t i = 1; i <= iterations; i++) {
		tree_node *tree = create_tree(i, depth);
		check += check_tree(tree);
		free(tree);

		tree = create_tree(-i, depth);
		check += check_tree(tree);
		free(tree);
	}
	result_strings[depth] = calloc(80, sizeof(char));
	sprintf(result_strings[depth], "%d\t trees of depth %d\t check: %d",
		iterations * 2, (int)depth, check);

	return NULL;
}

tree_node *create_tree(int64_t item, int depth)
{
	uint64_t target = 1ul << (depth - 1);
	int64_t head = 0;
	int64_t nodes = 0;

	tree_node *queue = calloc(target * 2, sizeof(tree_node));

	queue[head].item = item;

	while (nodes <= target) {
		item *= 2;
		tree_node *n = &queue[head++];

		n->left = &queue[++nodes];
		n->right = &queue[++nodes];
		n->left->item = item - 1;
		n->right->item = item;
	}

	return queue;
}

int64_t check_tree(tree_node * tree)
{
	if (tree->left == NULL) {
		return tree->item;
	}
	return tree->item + check_tree(tree->left) - check_tree(tree->right);
}
