/**
 * Copyright by <a href="http://crazyadam.net"><em><i>Joseph J.C. Tang</i></em></a> <br/>
 * Email: <a href="mailto:jinntrance@gmail.com">jinntrance@gmail.com</a>
 * @author joseph
 * @since 6/24/13 2:39 PM
 * @version 1.0
 */
import math._
object PiDigits {
  def main(args: Array[String]) {
    def pi(digitNum:Int)={
      val times=loopTimes(digitNum)
      def calPi(i:Int):BigDecimal=if (i>times) 2.5  else 2+i*calPi(i+1)/(2*i+1)
      calPi(1)
    }
    def loopTimes(n:Int):Int={
      val pres=1.0/pow(10,n)
      def t(initial:BigDecimal=2,i:Int=0):Int={
        val current=(i+1)*1.0/(2*i+3)*initial
        if(current<pres) i else t(current,i+1)
      }
      t()
    }
    def print10(s:String,offset:Int=0){
      val (s1,remain)=s.splitAt(10)
      println(s"$s1\t:${offset+s1.length}")
      if(!remain.isEmpty) print10(remain,10+offset)
    }
    val n=if(args.isEmpty) 27 else args(0).toInt
    print10(pi(n).toString().filterNot(_=='.').substring(0,n))
  }
}
