! The Computer Language Benchmarks Game
! http://benchmarksgame.alioth.debian.org/
!
!   based on nbody.java by Mark C. Williams
!   inverse real sqrt inspired by Yuankun Shi
!   contributed by Trey White

program n_body
    implicit none
    
    integer,parameter :: m = 3
    integer,parameter :: n = 5
    integer,parameter :: n_even = n+mod(n,2)
    integer,parameter :: nm1 = n-1
    integer,parameter :: nn = n*nm1/2
    integer,parameter :: nn2 = n*nm1
    
    real(8),parameter :: pi = 3.141592653589793d0
    real(8),parameter :: solar_mass = 4d0*pi*pi
    real(8),parameter :: days_per_year = 365.24d0
    
    real(8),parameter :: dt = 0.01d0
    
    integer :: k_max,k
    character(32) :: argv
    
    integer,dimension(nn) :: is,js
    integer,dimension(n_even,nm1) :: ks

    real(8),dimension(n_even) :: mass
    real(8),dimension(n_even,m) :: r,v
    real(8),dimension(nn) :: d,d2,mi,mj
    real(8),dimension(nn,m) :: dr
    real(8),dimension(nn2,m) :: dv
    

    ! begin
    
    call getarg(1,argv)
    read (argv,*) k_max
    
    call new_nbody_system()
    write(*,'(f19.16)') energy()
    do k = 1,k_max
        call advance(dt)
    end do
    write(*,'(f19.16)') energy()    

contains


    subroutine advance(dt)
        implicit none
        integer :: i
        real(8),intent(in) :: dt
        
        ! begin
        
        dr = r(is,:)-r(js,:)
        d2 = sum(dr*dr,2)
        d = 1e0/sqrt(real(d2))
        d = d*(1.5d0-0.5d0*d2*d*d)
        d = dt*d*d*d
        do i = 1,m
            dr(:,i) = dr(:,i)*d
            dv(:nn,i) = dr(:,i)*mi
            dv(nn+1:,i) = dr(:,i)*mj
        end do
        do i = 1,nm1
            v = v+dv(ks(:,i),:)
        end do
        r = r+v*dt
    end subroutine

   
    function energy()
        implicit none
        real(8) :: energy

        ! begin
        
        energy = 0.5d0*sum(mass*sum(v*v,2))
        dr = r(is,:)-r(js,:)
        d2 = sum(dr*dr,2)
        d2 = 1d0/sqrt(d2)
        energy = energy+sum(mi*mj*d2)
        
    end function
    
    
    subroutine new_nbody_system
        implicit none
        
        integer :: i,j,k
        real(8),parameter :: div_solar_mass = 1d0/solar_mass
        real(8) :: p
        
        ! begin
        
        k = 1
        do i = 1,n
            do j = i+1,n
                is(k) = i
                js(k) = j
                k = k+1
            end do
        end do

        k = nn+1
        do i = 1,nm1
            do j = 1,n-i
                ks(i,j) = k
                k = k+1
            end do
        end do
        k = 1
        do j = nm1,1,-1
            do i = n-j+1,n
                ks(i,j) = k
                k = k+1
            end do
        end do
        ks(n_even,:) = ks(n_even-1,:)
        
        r(:,1) = (/ 0d0, &
            4.84143144246472090d+00, &
            8.34336671824457987d+00, &
            1.28943695621391310d+01, &
            1.53796971148509165d+01, &
            0d0 /)
        r(:,2) = (/ 0d0, &
            -1.16032004402742839d+00, &
            4.12479856412430479d+00, &
            -1.51111514016986312d+01, &
            -2.59193146099879641d+01, &
            0d0 /)
        r(:,3) = (/ 0d0, &
            -1.03622044471123109d-01, &
            -4.03523417114321381d-01, &
            -2.23307578892655734d-01, &
            1.79258772950371181d-01, &
            0d0 /)
        
        v(:,1) = (/ 0d0, &
            1.66007664274403694d-03*days_per_year, &
            -2.76742510726862411d-03*days_per_year, &
            2.96460137564761618d-03*days_per_year, &
            2.68067772490389322d-03*days_per_year, &
            0d0 /)
        v(:,2) = (/ 0d0, &
            7.69901118419740425d-03*days_per_year, &
            4.99852801234917238d-03*days_per_year, &
            2.37847173959480950d-03*days_per_year, &
            1.62824170038242295d-03*days_per_year, &
            0d0 /)
        v(:,3) = (/ 0d0, &
            -6.90460016972063023d-05*days_per_year, &
            2.30417297573763929d-05*days_per_year, &
            -2.96589568540237556d-05*days_per_year, &
            -9.51592254519715870d-05*days_per_year, &
            0d0 /)
            
        mass = (/ solar_mass, &
            9.54791938424326609d-04*solar_mass, &
            2.85885980666130812d-04*solar_mass, &
            4.36624404335156298d-05*solar_mass, &
            5.15138902046611451d-05*solar_mass, &
            0d0 /)
        
        do i = 1,m
            p = sum(v(:,i)*mass)
            v(1,i) = -p*div_solar_mass
        end do
        
        mi = mass(is)
        mj = -mass(js)
        
    end subroutine
    
    
end program
