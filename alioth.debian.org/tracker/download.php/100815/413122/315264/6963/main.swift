#!/usr/bin/env swift

/* The Computer Language Benchmarks Game
http://benchmarksgame.alioth.debian.org/
contributed by David Turnbull
*/

import CoreFoundation

class BinaryTreeStore {

    // Swift arrays have automatic memory management that perform
    // much better than allocating individual nodes.
    private var storage = Array<Node>()

    // A node is identified by its storage position.
    // A negative NodeID marks the end of a branch.
    typealias NodeID = Int

    // Typical binary tree structure. Payload is an Int.
    private struct Node {
        let left:NodeID
        let right:NodeID
        let value:Int
    }

    // Deletes every node in this store while retaining its storage capacticy.
    func removeAllNodes() {
        storage.removeAll(keepCapacity: true)
    }

    // Builds a tree as defined by the benchmark rules. Returns its root.
    func createTree(value value:Int, depth:Int) -> NodeID {
        if depth > 0 {
            storage.append(
                Node(
                    left: createTree(value: 2*value-1, depth: depth-1),
                    right:createTree(value: 2*value, depth: depth-1),
                    value: value
                )
            )
        } else {
            storage.append(Node(left: -1, right: -1, value: value))
        }
        return storage.count - 1
    }

    // Reduce the values of a tree as defined by the benchmark rules.
    func reduceTree(rootNode id:NodeID) -> Int {
        if storage[id].left < 0 {
            return 0
        } else {
            return storage[id].value +
                reduceTree(rootNode: storage[id].left) -
                reduceTree(rootNode: storage[id].right)
        }
    }

}


func main(depth maxDepth:Int) {

    let minDepth = 4

    let longLivedTreeStore = BinaryTreeStore()

    ({
        let stretchDepth = maxDepth + 1
        let stretchTreeRoot = longLivedTreeStore.createTree(value: 0, depth: stretchDepth)
        let chk = longLivedTreeStore.reduceTree(rootNode: stretchTreeRoot)
        print("stretch tree of depth \(stretchDepth)\t check: \(chk)")

        longLivedTreeStore.removeAllNodes()
    }())

    let longLivedTreeRoot = longLivedTreeStore.createTree(value: 0, depth: maxDepth)

    for depth in minDepth.stride(through: maxDepth, by: 2) {
        let store = BinaryTreeStore()
        let iterations = 1 << (maxDepth - depth + minDepth)
        var check = 0
        for i in 1...iterations {
            let tree1 = store.createTree(value: i, depth: depth)
            let tree2 = store.createTree(value: -i, depth: depth)
            check += store.reduceTree(rootNode: tree1)
            check += store.reduceTree(rootNode: tree2)
            store.removeAllNodes()
        }
        print("\(iterations*2)\t trees of depth \(depth)\t check: \(check)")
    }

    let chk = longLivedTreeStore.reduceTree(rootNode: longLivedTreeRoot)
    print("long lived tree of depth \(maxDepth)\t check: \(chk)")
}


main(depth: Int(Process.arguments[1])!)
