/* The Computer Language Benchmarks Game
 * http://benchmarksgame.alioth.debian.org/
 *
 * contributed by Daryl Griffith 
 */

import java.io.Closeable;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class knucleotide {

    static final HashTable MAP = new HashTable();
    static final int[] SEQUENCES1 = {1, 2};
    static final int[] SEQUENCES2 = {3, 4, 6, 12, 18};
    static final String[] SPICIFIC_SEQUENCES = new String[]{"GGT", "GGTA", "GGTATT", "GGTATTTTAATT", "GGTATTTTAATTTATAGT"};
    static final int LINE_LENGTH = 60;
    static final int EOF = -1;
    static byte[] nucleotides;

    public static void main(String[] args) {
        {
            byte[] temp = new byte[LINE_LENGTH];
            byte[] buffer = new byte[125_000_000];
            byte[] species = ">TH".getBytes();
            int n;
            int i;

            try (LineInputStream in = new LineInputStream(System.in)) {
outer:
                for (;;) {
                    n = in.readLine(temp);
                    if (n == EOF) {
                        return;
                    }
                    if (n != LINE_LENGTH) {
                        for (i = 0; i < species.length; i++) {
                            if (temp[i] != species[i]) {
                                continue outer;
                            }
                        }
                        break;
                    }
                }
                i = 0;
                for (;;) {
                    n = in.readLine(temp);
                    if (n == EOF) {
                        break;
                    }
                    for (int j = 0; j < n; i++, j++) {
                        buffer[i] = translate(temp[j]);
                    }
                }
                if (i == buffer.length) {
                    nucleotides = buffer;
                } else {
                    nucleotides = new byte[i];
                    System.arraycopy(buffer, 0, nucleotides, 0, i);
                }
            } catch (IOException e) {

            }
        }
        countSequences(SEQUENCES1);
        {
            List<Entry> sequence1 = new ArrayList<>(4);
            List<Entry> sequence2 = new ArrayList<>(16);

            for (Entry entry : MAP) {
                switch (Long.numberOfLeadingZeros(entry.key)) {
                    case 61:
                        sequence1.add(entry);
                        break;
                    case 59:
                        sequence2.add(entry);
                }
            }
            printSequence(sequence1);
            printSequence(sequence2);
        }
        countSequences(SEQUENCES2);
        {
            Entry key = new Entry();

            for (String sequence : SPICIFIC_SEQUENCES) {
                key.setKey(sequence);
                System.out.print(MAP.get(key).count);
                System.out.print('\t');
                System.out.println(sequence);
            }
        }
    }
        
    static byte translate(byte b) {
        return (byte) ((b >> 1) & 3);
    }

    static void countSequences(int[] sequences) {
        for (int sequence : sequences) {
            updateHashtable(sequence);
        }
    }

    static void updateHashtable(int sequence) {
        int sequenceTop = nucleotides.length - sequence + 1;
        Entry key = new Entry();
        Entry value;
        
        for (int i = 0; i < sequenceTop; i++) {
            key.setKey(i, sequence);
            value = MAP.get(key);
            if (value != null) {
                value.count++;
                continue;
            }
            MAP.put(key);
            key = new Entry();
        }
    }

    static void printSequence(List<Entry> sequence) {
        int sum = 0;

        Collections.sort(sequence, new Comparator<Entry>() {

            @Override
            public int compare(Entry entry1, Entry entry2) {
                if (entry2.count != entry1.count) {
                    return entry2.count - entry1.count;
                }
                return entry1.toString().compareTo(entry2.toString());
            }
        });
        for (Entry entry : sequence) {
            sum += entry.count;
        }
        for (Entry entry : sequence) {
            System.out.format("%s %.3f\n", entry, entry.count * 100f / sum);
        }
        System.out.println();
    }

    static class LineInputStream implements Closeable {

        private static final int LF = 10;
        private final ByteBuffer buffer = ByteBuffer.allocate(8192);
        private final InputStream in;

        public LineInputStream(InputStream in) {
            this.in = in;
            buffer.limit(buffer.position());
        }

        public int readLine(byte[] b) throws IOException {
            for (int end = buffer.position(); end < buffer.limit(); end++) {
                if (buffer.get(end) == LF) {
                    if (end - buffer.position() == LINE_LENGTH) {
                        buffer.get(b);
                        buffer.position(buffer.position() + 1);
                        return LINE_LENGTH;
                    } else {
                        int size = end - buffer.position();

                        buffer.get(b, 0, size);
                        buffer.position(buffer.position() + 1);
                        return size;
                    }
                }
            }
            buffer.compact();
            int n = in.read(buffer.array(), buffer.position(), buffer.remaining());

            if (n == EOF) {
                buffer.flip();
                if (buffer.hasRemaining()) {
                    int size = buffer.remaining();

                    buffer.get(b, 0, size);
                    return size;
                } else {
                    return EOF;
                }
            } else {
                buffer.position(buffer.position() + n);
                buffer.flip();
            }
            for (int end = buffer.position(); end < buffer.limit(); end++) {
                if (buffer.get(end) == LF) {
                    if (end - buffer.position() == LINE_LENGTH) {
                        buffer.get(b);
                        buffer.position(buffer.position() + 1);
                        return LINE_LENGTH;
                    } else {
                        int size = end - buffer.position();

                        buffer.get(b, 0, size);
                        buffer.position(buffer.position() + 1);
                        return size;
                    }
                }
            }
            return EOF;
        }

        @Override
        public void close() throws IOException {
            in.close();
        }
    }

    static class Entry {
        
        long key;
        int count = 1;
        Entry next;

        void setKey(int offset, int length) {
            key = 1;
            for (int i = offset + length - 1; i >= offset; i--) {
                key = (key << 2) | nucleotides[i];
            }
        }

        void setKey(String species) {
            key = 1;
            for (int i = species.length() - 1; i >= 0; i--) {
                key = (key << 2) | translate((byte) species.charAt(i));
            }
        }

        public int hash() {
            return (int) (key ^ (key >>> 18));
        }

        @Override
        public String toString() {
            char[] name = new char[(63 - Long.numberOfLeadingZeros(key)) / 2];
            long temp = key;

            for (int i = 0; temp > 1; temp >>= 2, i++) {
                name[i] = (char) (((temp & 3) << 1) | 'A');
                if (name[i] == 'E') {
                    name[i] = 'T';
                }
            }
            return new String(name);
        }
    }
    
    static class HashTable implements Iterable<Entry> {
        
        private static final int LOAD_FACTOR = 0xc;
        private Entry[] table = new Entry[1 << 4];
         int mask = table.length - 1; 
        private int size = 0;

        public Entry get(Entry key) {
            Entry entry = table[key.hash() & mask];

            while (entry != null && entry.key != key.key) {
                entry = entry.next;
            }
            if (entry == null) {
                return null;
            }
            return entry;
        }

        public void put(Entry entry) {
            if (((size << 4) / table.length) > LOAD_FACTOR) {
                resize();
            }
            putImpl(entry);
        }

        private void resize() {
            Entry[] oldTable = table;
            Entry e;

            table = new Entry[table.length << 1];
            mask = table.length - 1;
            size = 0;
            for (Entry entry: oldTable) {
                while (entry != null) {
                    e = entry.next;
                    entry.next = null;
                    putImpl(entry);
                    entry = e;
                }
            }
        }

        private void putImpl(Entry entry) {
            Entry e = table[entry.hash() & mask];
            Entry d;

            size++;
            if (e == null) {
                table[entry.hash() & mask] = entry;
                return;
            }
            do {
                d = e;
                e = e.next;
            } while (e != null);
            d.next = entry;
        }

        @Override
        public Iterator<Entry> iterator() {
            return new Iterator<Entry>() {

                final Entry[] t = table;
                int i = -1;

                @Override
                public boolean hasNext() {
                    for (i++; i < t.length; i++) {
                        if (t[i] != null) {
                            return true;
                        }
                    }
                    return false;
                }

                @Override
                public Entry next() {
                    return t[i];
                }

                @Override
                public void remove() {
                }
                
            };
        }
    }
}
