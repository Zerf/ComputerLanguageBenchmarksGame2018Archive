// The Computer Language Benchmarks Game
// http://benchmarksgame.alioth.debian.org/
//
// Contributed by Alex A Skinner
// Based on the C program by Jeremy Zerfas
// Based on the C++ program from Jon Harrop, Alex Mizrahi, and Bruno Coutinho.

package main

import (
	"fmt"
	"os"
	"runtime"
	"strconv"
	"sync"
)

type nodePool struct {
	nodes []*node
	index int
}

func newNodePool() *nodePool {
	return &nodePool{[]*node{}, 0}
}

func (np *nodePool) get() *node {
	if np.index >= len(np.nodes) {
		np.nodes = append(np.nodes, &node{})
	}
	if np.nodes[np.index] == nil {
		np.nodes[np.index] = &node{}
	}
	np.index++
	return np.nodes[np.index-1]
}

func (np *nodePool) put(n *node) {
	if np.index > 0 {
		np.nodes[np.index-1] = n
		np.index--
	}
}

func (np *nodePool) clear(n *node) {
	if n == nil {
		return
	}
	np.clear(n.left)
	np.clear(n.right)
	np.put(n)
}

type node struct {
	value       int
	left, right *node
}

func (n *node) compute() int {
	if n.left != nil {
		return n.left.compute() - n.right.compute() + n.value
	}
	return n.value
}

func createTree(value int, depth int, pool *nodePool) *node {
	n := pool.get()
	if depth > 0 {
		n.left = createTree(2*value-1, depth-1, pool)
		n.right = createTree(2*value, depth-1, pool)
	} else {
		n.left, n.right = nil, nil
	}
	n.value = value
	return n
}

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("%s <depth>\n", os.Args[0])
		return
	}
	runtime.GOMAXPROCS(runtime.NumCPU() * 2)
	minDepth := 4
	treeDepth, _ := strconv.Atoi(os.Args[1])
	if treeDepth < minDepth+2 {
		treeDepth = minDepth + 2
	}
	p := newNodePool()
	stretchTree := createTree(0, treeDepth+1, p)
	fmt.Printf("stretch tree of depth %d\t check: %d\n", treeDepth+1, stretchTree.compute())
	longLived := createTree(0, treeDepth, p)
	toPrint := make([]string, treeDepth+1)
	var wg sync.WaitGroup
	for current := minDepth; current <= treeDepth; current += 2 {
		wg.Add(1)
		go func(depth int) {
			p := newNodePool()
			total := 0
			iterations := 1 << uint(treeDepth-depth+minDepth)
			for i := 1; i <= iterations; i++ {
				tree1 := createTree(i, depth, p)
				tree2 := createTree(-i, depth, p)
				total += tree1.compute() + tree2.compute()
				p.clear(tree1)
				p.clear(tree2)
			}
			toPrint[depth] = fmt.Sprintf("%d\t trees of depth %d\t check %d\n", 2*iterations, depth, total)
			wg.Done()
		}(current)
	}
	wg.Wait()
	for current := minDepth; current <= treeDepth; current += 2 {
		fmt.Print(toPrint[current])
	}
	fmt.Printf("long lived tree of depth %d\t check: %d\n", treeDepth, longLived.compute())
}
