/* The Computer Language Benchmarks Game
   http://benchmarksgame.alioth.debian.org/
 * 
 * regex-dna program contributed by Isaac Gouy 
 * converted from regex-dna program
 * Parallel stuff, Josh Goldfoot
 *
*/

using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

class regexredux
{
    static void Main(string[] args)
    {

        // read FASTA sequence
        string sequence = Console.In.ReadToEnd();
        int initialLength = sequence.Length;

        // remove FASTA sequence descriptions and new-lines
        sequence = Regex.Replace(sequence, ">.*\n|\n", "");
        int codeLength = sequence.Length;

        var three = Task.Run(() => {
            // regex substitution
            IUB[] codes = {
                  new IUB("tHa[Nt]", "<4>")
                 ,new IUB("aND|caN|Ha[DS]|WaS", "<3>")
                 ,new IUB("a[NSt]|BY", "<2>")
                 ,new IUB("<[^>]*>", "|")
                 ,new IUB("[^|][^|]*", "")
            };
            string s2 = sequence + "";
            foreach (IUB iub in codes)
                s2 = Regex.Replace(s2, iub.code, iub.alternatives);
            return s2.Length;
        });

        // regex match
        string[] variants = {
         "agggtaaa|tttaccct"
         ,"[cgt]gggtaaa|tttaccc[acg]"
         ,"a[act]ggtaaa|tttacc[agt]t"
         ,"ag[act]gtaaa|tttac[agt]ct"
         ,"agg[act]taaa|ttta[agt]cct"
         ,"aggg[acg]aaa|ttt[cgt]ccct"
         ,"agggt[cgt]aa|tt[acg]accct"
         ,"agggta[cgt]a|t[acg]taccct"
         ,"agggtaa[cgt]|[acg]ttaccct"
        };

        var results = variants.AsParallel().AsOrdered().Select(v => {
            int count = 0;
            for (Match m = Regex.Match(sequence, v); m.Success; m = m.NextMatch()) count++;
            return string.Format("{0} {1}", v, count);
        });

        foreach (var line in results)
            Console.WriteLine(line); 
        
        Console.WriteLine("\n{0}\n{1}\n{2}",
           initialLength, codeLength, three.Result);
    }


    struct IUB
    {
        public string code;
        public string alternatives;

        public IUB(string code, string alternatives)
        {
            this.code = code;
            this.alternatives = alternatives;
        }
    }
}
