﻿/* The Computer Language Benchmarks Game
   http://benchmarksgame.alioth.debian.org/
 *
 * submitted by Sergey Prytkov
 * shifting arithmetic borrowed from Anthony Lloyd's submission
 * 
 */

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace knucleotide
{
    class Program
    {
        private static readonly (int, Dictionary<long, Wrapper>)[] dics = new[]
        {
            (1, new Dictionary<long, Wrapper>()),
            (2, new Dictionary<long, Wrapper>()),
            (3, new Dictionary<long, Wrapper>()),
            (4, new Dictionary<long, Wrapper>()),
            (6, new Dictionary<long, Wrapper>()),
            (12, new Dictionary<long, Wrapper>()),
            (18, new Dictionary<long, Wrapper>()),
        };
        private const byte a = (byte) 'a';
        private const byte A = (byte) 'A';
        private const byte c = (byte) 'c';
        private const byte C = (byte) 'C';
        private const byte g = (byte) 'g';
        private const byte G = (byte) 'G';
        private const byte T = (byte) 'T';
        private const byte t = (byte) 't';
        private const byte newLine = (byte) '\n';

        private static readonly List<byte[]> memory = new List<byte[]>();
        private static int start = 0;
        private static int end = -1;
        private static readonly VtComparer vt = new VtComparer();

        private static int work=-1;
        private const int WORK_COUNT=6;

        private static void Worker()
        {
            while (true)
            {
                var currentWork = Interlocked.Increment(ref work);
                if (currentWork > WORK_COUNT) return;
                var (f, d) = dics[currentWork];
                CountFrequency(f, d);
                if (currentWork == WORK_COUNT) return;
            }
        }

        private static void RunWorkers()
        {
            var worker = new ThreadStart(Worker);
            var workers = new Thread[Environment.ProcessorCount];
            for (int i = 0; i < workers.Length; i++)
            {
                workers[i] = new Thread(worker);
                workers[i].Start();
            }
            for (int i = 0; i < workers.Length; i++)
            {
                workers[i].Join();
            }
        }

        static void Main(string[] args)
        {
            ReadInput();
            RunWorkers();
            var f1 = WriteFrequencies(dics[0].Item2, 1);
            Console.WriteLine(f1);
            var f2 = WriteFrequencies(dics[1].Item2, 2);
            Console.WriteLine(f2);
            Console.WriteLine(WriteCount(dics[2].Item2, "GGT"));
            Console.WriteLine(WriteCount(dics[3].Item2, "GGTA"));
            Console.WriteLine(WriteCount(dics[4].Item2, "GGTATT"));
            Console.WriteLine(WriteCount(dics[5].Item2, "GGTATTTTAATT"));
            Console.WriteLine(WriteCount(dics[6].Item2, "GGTATTTTAATTTATAGT"));
        }

        private class VtComparer : IComparer<(int, long)>
        {
            public int Compare((int, long) x, (int, long) y)
            {
                var a = y.Item1 - x.Item1;
                if (a == 0) return (int) (x.Item2 - y.Item2);
                return a;
            }
        }

        private static string WriteFrequencies(Dictionary<long, Wrapper> freq, int fragmentLength)
        {
            var sb = new StringBuilder();
            var res = new (int, long)[freq.Count];
            var total = 0.0d;
            var i = 0;
            foreach (var kv in freq)
            {
                total += kv.Value.V;
                res[i++] = (kv.Value.V, kv.Key);
            }
            double percent = 100.0 / total;
            Array.Sort(res, vt);

            foreach (var vk in res)
            {
                var key = vk.Item2;
                if (fragmentLength == 2)
                {
                    var b = ToChar((int) (key & 0x3));
                    key >>= 2;
                    var a = ToChar((int) (key & 0x3));
                    sb.Append(a);
                    sb.Append(b);
                }
                else
                {
                    sb.Append(ToChar((int) (key & 0x3)));
                }
                sb.Append(' ');
                sb.AppendLine((vk.Item1 * percent).ToString("F3"));
            }
            return sb.ToString();
        }

        private static string WriteCount(Dictionary<long, Wrapper> dictionary, string fragment)
        {
            long key = 0;
            for (int i = 0; i < fragment.Length; ++i)
                key = (key << 2) | ToNum((byte) fragment[i]);
            Wrapper w;
            var n = dictionary.TryGetValue(key, out w) ? w.V : 0;
            return string.Concat(n.ToString(), "\t", fragment);
        }

        private class Wrapper
        {
            public int V;

            public Wrapper(int v)
            {
                V = v;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static byte ToNum(byte b)
        {
            switch (b)
            {
                case a:
                case A:
                    return 0;
                case c:
                case C:
                    return 1;
                case g:
                case G:
                    return 2;
                default:
                    return 3;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static char ToChar(int i)
        {
            switch (i)
            {
                case 0:
                    return 'A';
                case 1:
                    return 'C';
                case 2:
                    return 'G';
                default:
                    return 'T';
            }
        }

        private static void CountFrequency(int fragmentLength, Dictionary<long, Wrapper> dictionary)
        {
            long rollingKey = 0;
            long mask = 0;
            int cursor;
            for (cursor = 0; cursor < fragmentLength - 1; cursor++)
            {
                rollingKey <<= 2;
                rollingKey |= ToNum(memory[0][start + cursor]);
                mask = (mask << 2) + 3;
            }
            mask = (mask << 2) + 3;
            Wrapper w;
            byte cursorByte;
            for (int i = 0; i < memory.Count; i++)
            {
                var buffer = memory[i];
                for (int j = i == 0 ? start + 1 : 0; j < buffer.Length; j++)
                {
                    if (i == memory.Count - 1 && j == end - 1) break;
                    if ((cursorByte = buffer[j]) == newLine) continue;
                    rollingKey = ((rollingKey << 2) & mask) | ToNum(cursorByte);
                    if (dictionary.TryGetValue(rollingKey, out w))
                        w.V++;
                    else
                        dictionary.Add(rollingKey, new Wrapper(1));
                }
            }
        }

        private static void ReadInput()
        {
            var bufferSize = 32 * 1024 * 8;
            var buffer = new byte[bufferSize];
            var stdin = Console.OpenStandardInput();
            var gti = -1;
            var gt = (byte) '>';
            var T = (byte) 'T';
            var H = (byte) 'H';
            var R = (byte) 'R';
            var E = (byte) 'E';
            int readen;
            bool headerFound = false;
            byte[] helpMe = null;
            int z = 0;
            // I need refactor this mess
            while ((readen = stdin.Read(buffer, 0, bufferSize)) > 0)
            {
                // looking for '>'
                while (!headerFound && (gti = Array.IndexOf(buffer, gt, gti + 1, readen - (gti + 1))) != -1)
                {
                    // if '>THREE' fits buffer with no overlap (very likely)
                    if (gti < readen - 6)
                    {
                        if (buffer[gti + 1] == T &&
                            buffer[gti + 2] == H &&
                            buffer[gti + 3] == R &&
                            buffer[gti + 4] == E &&
                            buffer[gti + 5] == E)
                        {
                            headerFound = true;
                            break;
                        }
                    }
                    else
                    {
                        helpMe = new byte[5];
                        Buffer.BlockCopy(buffer, gti, helpMe, 0, readen - gti);
                        stdin.Read(helpMe, readen - gti, helpMe.Length - (readen - gti));
                        if (helpMe[0] == T &&
                            helpMe[1] == H &&
                            helpMe[2] == R &&
                            helpMe[3] == E &&
                            helpMe[4] == E)
                        {
                            headerFound = true;
                            break;
                        }
                    }
                }
                if (headerFound)
                {
                    // if new line after header also in this buff (very likely)
                    if ((start = Array.IndexOf(buffer, newLine, gti + 6, readen - (gti + 6))) != -1)
                    {
                        memory.Add(buffer);
                        end = Array.IndexOf(buffer, gt, start, readen - start);
                        if (readen < bufferSize)
                        {
                            end = readen;
                        }
                        else
                        {
                            buffer = new byte[bufferSize];
                            goto second_loop;
                        }
                    }
                }
            }
            second_loop:
            while (end == -1 && ((readen = stdin.Read(buffer, 0, bufferSize)) > 0))
            {
                end = Array.IndexOf(buffer, gt, start, readen - start);
                memory.Add(buffer);
                buffer = new byte[bufferSize];
            }
            if (end == -1) end = readen;
            stdin.Dispose();
            GC.Collect();
        }
    }
}