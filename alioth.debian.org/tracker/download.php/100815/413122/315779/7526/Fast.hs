import Control.Monad (when)
import Data.Foldable (for_)
import Data.Monoid ((<>))
import Foreign
    ( IntPtr, Ptr, advancePtr, callocArray, free, intPtrToPtr
    , nullPtr, peek, peekElemOff, pokeElemOff, ptrToIntPtr
    )
import System.Environment (getArgs)

-- Main function that takes in n as an argument
main :: IO ()
main = do
    -- Read n from arguments
    n <- read . head <$> getArgs

    -- Create, check and free stretch tree
    let stretchN = n + 1
    stretchT <- make stretchN
    stretchC <- check stretchT
    free stretchT
    putStrLn $ makeOutput "stretch tree" stretchN stretchC

    -- Create long lived tree
    longT <- make n

    -- Check appropriate number of trees of each size
    for_ [4, 6 .. n] $ \d -> do
        let c = 16 * 2 ^ (n - d)
        sumC <- sumT d c
        putStrLn $ makeOutput (show c <> "\t trees") d sumC

    -- Check and free long lived tree
    longC <- check longT
    free longT
    putStrLn $ makeOutput "long lived tree" n longC

-- Convert message start string depth and node count to output string
makeOutput :: String -> Int -> Int -> String
makeOutput p n c = p <> " of depth " <> show n <> "\t check: " <> show c

-- Size 2 array of pointers
-- First value is either pointer to left child or nullPtr
-- Second value is either pointer to right child or nullPtr
type Tree = Ptr IntPtr

-- Create, check and allocate trees of specified depth the specified number of times
sumT :: Int -> Int -> IO Int
sumT d = go 0
  where
    go s 0 = pure s
    go s c = do
        -- Make, check and free tree of correct depth
        t <- make d
        tc <- check t
        free t
        let s' = s + tc
        -- Recurse until count is 0
        s' `seq` go s' (c - 1)

-- Recursive function that follows child pointers until a null pointer is reached
-- And sums up the number of nodes encountered
check :: Tree -> IO Int
check t = if t == nullPtr then pure 0 else do
    -- Recurse on left child
    l <- intPtrToPtr <$> peek t
    lc <- check l
    -- Recurse on right child
    r <- intPtrToPtr <$> peekElemOff t 1
    rc <- check r
    -- Force and sum up child sums plus 1
    lc `seq` rc `seq` pure $ 1 + lc + rc

-- Allocate space for a tree of specified depth
-- Then create such a tree
make :: Int -> IO Tree
make d = do
    let len = 2 * 2 ^ d - 1
    -- Allocate space
    t <- callocArray $ 2 * len
    -- Recurse through all nodes setting child pointers
    let go n = when (n >= 0) $ do
            pokeElemOff t n . ptrToIntPtr . advancePtr t $ 2 * n + 2
            go $ n - 1
    go $ len - 2
    pure t
