<?php 
/* The Computer Language Benchmarks Game
	http://benchmarksgame.alioth.debian.org/
	binarytree by Ludovic Urbain
*/

function tree(&$n,$i,&$depth){
	$n[2]=$i;
	if(!$depth){
		return $n;
	}
	$i2=$i+$i;
	$depth--;
	tree($n[0],$i2-1,$depth);
	tree($n[1],$i2,$depth);
	$depth++;
	return $n;
}

function check(&$n) {
	$r=$n[2];
	if($n[0][0] === null){
		$r+=check($n[0]);
	}else{
		$r+=$n[0][2];
	}
	if($n[1][0] === null){
		$r-=check($n[1]);
	}else{
		$r-=$n[1][2];
	}
	return $r;
}

$min_depth = 4;
if($argc == 2){
	$max_depth=$argv[1];
}else{
	$max_depth=$min_depth+2;
}
$stretch_depth=$max_depth+1;
$stree=array();
printf("stretch tree of depth %d\t check: %d\n",$stretch_depth, check(tree($stree,0,$stretch_depth)));
$bigtree=array();
tree($bigtree,0,$max_depth);
$runs = 1 << ($max_depth);


$tok = ftok(__FILE__, chr(time() & 255));
$queue = msg_get_queue($tok);

$parent = TRUE;
for ($i = 0; $i < 4; ++$i) {
	$pid = pcntl_fork();
	if ($pid === -1) {
		die('could not fork');
	} else if ($pid) {
		continue;
	}
	$parent = FALSE;
	break;
}

$tmptree=array();
$return=array();
while($min_depth <= $max_depth){
	$check = 0;
	for($i=0;$i<$runs/4;$i++){
		$check+=check(tree($tmptree,$i,$min_depth));
		$check+=check(tree($tmptree,-$i,$min_depth));
	}
	$return[$min_depth]=array($runs>>1,$check);
	$min_depth+=2;
	$runs>>=2;
}

if (!msg_send($queue, 2, $return, TRUE, FALSE, $errno)) {
	var_dump("$errno");
	var_dump(msg_stat_queue($queue));
}

if (!$parent) {
	exit(0);
}

$result2=array();
for($i=0;$i<4;$i++){
	msg_receive($queue, 2, $msgtype, 256, $result, TRUE);
	foreach($result as $depth => $res){
		$result2[$depth][0]+=$res[0];
		$result2[$depth][1]-=$res[1];
	}
	pcntl_wait($s);
}
foreach($result2 as $depth => $res){
	printf("%d\t trees of depth %d\t check: %d\n", $result2[$depth][0], $depth, -$result2[$depth][1]);
}
printf("long lived tree of depth %d\t check: %d\n",
$max_depth, check($bigtree));// this is slow, it should be multithreaded. some day who knows