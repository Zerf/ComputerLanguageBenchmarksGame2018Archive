import Glibc

let c2i: [Character:Int] = [ "A": 0, "C": 1, "T": 2, "G": 3 ]

func readInput(_ inStream: UnsafeMutablePointer<FILE>) -> [Int8] {
    var seq = [Int8]()
    var buf = UnsafeMutablePointer<Int8>.allocate(capacity: 100)
    defer {
        buf.deallocate(capacity: 100)
    }
    let pattern = ">THREE Homo sapiens frequency"
    let pat = pattern.withCString({ s in s })
    let patLen = pattern.utf8CString.count - 1
    while memcmp(buf, pat, patLen) != 0  {
        buf = fgets(buf, 100, inStream)
    }
    while fgets(buf, 100, inStream) != nil {
        seq.append(contentsOf: UnsafeMutableBufferPointer(start: buf, count: 60))
    }
    return seq
}

// Read sequence
let ins = stdin
var sequence = readInput(ins!)

