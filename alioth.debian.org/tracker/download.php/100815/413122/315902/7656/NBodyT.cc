/* The Computer Language Benchmarks Game
   http://benchmarksgame.alioth.debian.org/

   contributed by Trey White
   based on Java version by Mark C Lewis
   iterative refinement of rsqrt inspired by Branimir Maksimovic
*/
   
#include <cassert>
#include <cstdio>
#include <immintrin.h>
#include <string>

#pragma GCC diagnostic ignored "-Wattributes"
#define INLINE __attribute__((always_inline))

static constexpr double DT = 0.01;
static constexpr int N = 5;
static constexpr int NxN = N*(N-1)/2;
static constexpr int Nx2N = N*(N-1);

static constexpr double PI = 3.141592653589793;
static constexpr double SOLAR_MASS = 4*PI*PI;
static constexpr double DAYS_PER_YEAR = 365.24;

static constexpr double DIV_SOLAR_MASS = 1.0/SOLAR_MASS;


INLINE __m128d refine(const __m128d xx, const __m128d dd)
{ return _mm_mul_pd(xx,_mm_sub_pd(_mm_set1_pd(1.5),_mm_mul_pd(_mm_set1_pd(0.5),_mm_mul_pd(xx,_mm_mul_pd(xx,dd))))); }

template <int I, int J> 
INLINE constexpr int shuffle()
{ return (I%2) ? ((J%2) ? 3 : 1) : ((J%2) ? 2 : 0); }

template <int N>
struct Doubles
{
	__m128d m;
	Doubles<N-2> v;
	
	INLINE Doubles<N>() {}
	
	template <typename... Params>
	INLINE Doubles<N>(const double d0, const double d1, const Params... ps):
		m(_mm_set_pd(d1,d0)), v(ps...) {}
	
	INLINE Doubles<N>(const Doubles<N> &that): m(that.m), v(that.v) {}
	
	INLINE Doubles<N>(const __m128d m, const Doubles<N-2> &v): m(m), v(v) {}
	
	INLINE Doubles<N> operator-() const
	{ return Doubles<N>(_mm_sub_pd(_mm_setzero_pd(),m),-v); }
	
	INLINE Doubles<N> operator-(const Doubles<N> &that) const
	{ return Doubles<N>(_mm_sub_pd(m,that.m),v-that.v); }
	
	INLINE Doubles<N> operator+(const Doubles<N> &that) const
	{ return Doubles<N>(_mm_add_pd(m,that.m),v+that.v); }
	
	INLINE Doubles<N> operator*(const Doubles<N> &that) const
	{ return Doubles<N>(_mm_mul_pd(m,that.m),v*that.v); }
	
	INLINE Doubles<N> operator*(const __m128d dd) const
	{ return Doubles<N>(_mm_mul_pd(m,dd),v*dd); }
	
	INLINE void operator+=(const Doubles<N> &that)
	{ m = _mm_add_pd(m,that.m); v += that.v; }
	
	INLINE void operator-=(const Doubles<N> &that)
	{ m = _mm_sub_pd(m,that.m); v -= that.v; }
	
	INLINE void operator*=(const Doubles<N> &that)
	{ m = _mm_mul_pd(m,that.m); v *= that.v; }

	template <int I, int J>
	INLINE __m128d asum() const
	{ return fuse<I,J>(); }
	
	template <int I, int J, int K, int... Is>
	INLINE __m128d asum() const
	{ return _mm_add_pd(fuse<I,J>(),asum<K,Is...>()); }
	
	template <int M>
	void copyI(const Doubles<M> &that);
	
	template <int M>
	void copyJ(const Doubles<M> &that);
	
	template <int I>
	INLINE __m128d at() const
	{ return (I < 2) ? m : v.at<I-2>(); }

	template <int I, int J>
	INLINE __m128d fuse() const
	{ return _mm_shuffle_pd(at<I>(),at<J>(),shuffle<I,J>()); }

	template <int M, int I, int J, int... Is>
	INLINE void copy(const Doubles<M> &that)
	{ m = that.fuse<I,J>(); v.copy<M,Is...>(that); }
	
	INLINE void drsqrt()
	{ m = _mm_div_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(m)); v.drsqrt(); }
	
	INLINE __m128d dsum() const { return _mm_add_pd(m,v.dsum()); }

	INLINE void rsqrt()
	{
		const __m128 aa00 = _mm_cvtpd_ps(m);
		const __m128 bb00 = _mm_cvtpd_ps(v.m);
		const __m128 aabb = _mm_movelh_ps(aa00,bb00);
		const __m128 rrss = _mm_rsqrt_ps(aabb);
		const __m128d rr = _mm_cvtps_pd(rrss);
		const __m128d ss = _mm_cvtps_pd(_mm_movehl_ps(rrss,rrss));
		m = refine(rr,m);
		v.m = refine(ss,v.m);
		v.v.rsqrt();
	}
	
	INLINE void set0(const double d) { m[0] = d; }
	
	INLINE Doubles<N> subFrom(const __m128d dd) const
	{ return Doubles<N>(_mm_sub_pd(dd,m),v.subFrom(dd)); }
	
	INLINE double sum() const 
	{ const __m128d s = dsum(); return s[0]+s[1]; }
};

template <>
struct Doubles<2>
{
	__m128d m;
	
	INLINE Doubles<2>(): m(_mm_setzero_pd()) {}

	INLINE Doubles<2>(const __m128d m): m(m) {}
	
	INLINE Doubles<2>(const Doubles<2> &that): m(that.m) {}

	INLINE Doubles<2> operator+(const Doubles<2> &that) const
	{ return Doubles<2>(_mm_add_pd(m,that.m)); }
	
	INLINE Doubles<2> operator-(const Doubles<2> &that) const
	{ return Doubles<2>(_mm_sub_pd(m,that.m)); }
	
	INLINE Doubles<2> operator*(const Doubles<2> &that) const
	{ return Doubles<2>(_mm_mul_pd(m,that.m)); }
	
	INLINE void operator*=(const Doubles<2> &that) { m = _mm_mul_pd(m,that.m); }

	template <int I>
	INLINE __m128d at() const { return m; }

	template <int M, int I, int J>
	INLINE void copy(const Doubles<M> &that) { m = that.template fuse<I,J>(); }
	
	INLINE void drsqrt()
	{ m = _mm_div_pd(_mm_set1_pd(1.0),_mm_sqrt_pd(m)); }
	
	INLINE __m128d dsum() const { return m; }
	
	INLINE void rsqrt()
	{
        const __m128 aa00 = _mm_cvtpd_ps(m);
        const __m128 ones = _mm_set1_ps(1.0f);
        const __m128 aa11 = _mm_movelh_ps(aa00,ones);
        const __m128 rr11 = _mm_rsqrt_ps(aa11);
        const __m128d rr = _mm_cvtps_pd(rr11);
        m = refine(rr,m);
	}
	
	INLINE Doubles<2> subFrom(const __m128d dd) const
	{ return Doubles<2>(_mm_sub_pd(dd,m)); }
};

template <>
struct Doubles<1>
{
	__m128d m;
	
	INLINE Doubles<1>(): m(_mm_setzero_pd()) {}
	
	INLINE Doubles<1>(const double d): m(_mm_set1_pd(d)) {}
	
	INLINE Doubles<1>(const __m128d m) : m(m) {}
	
	INLINE Doubles<1>(const Doubles<1> &that): m(that.m) {}
	
	INLINE Doubles<1> operator-() const
	{ return Doubles<1>(_mm_sub_pd(_mm_setzero_pd(),m)); }
	
	INLINE Doubles<1> operator+(const Doubles<1> &that) const
	{ return Doubles<1>(_mm_add_pd(m,that.m)); }
	
	INLINE Doubles<1> operator*(const Doubles<1> &that) const
	{ return Doubles<1>(_mm_mul_pd(m,that.m)); }
	
	INLINE void operator+=(const Doubles<1> &that) 
	{ m = _mm_add_pd(m,that.m); }

	INLINE void operator*=(const Doubles<1> &that) 
	{ m = _mm_mul_pd(m,that.m); }

	template <int I>
	INLINE __m128d at() const { return m; }
	
	INLINE __m128d dsum() const 
	{ return _mm_shuffle_pd(m,_mm_setzero_pd(),0); }	
};

template <int M>
INLINE Doubles<M> operator-(const double d, const Doubles<M> &v)
{ const __m128d dd = _mm_set1_pd(d); return v.subFrom(dd); }

template <int M>
INLINE Doubles<M> operator*(const double d, const Doubles<M> &v)
{ const __m128d dd = _mm_set1_pd(d); return v*dd; }

template <int N, int M>
__m128d sum(const Doubles<M> &v);

template <int N, int M>
INLINE void accum(Doubles<N> &a, const Doubles<M> &b)
{ a.m += sum<N,M>(b); accum(a.v,b); }

template <int N>
INLINE void cross(Doubles<2*N> &a, const Doubles<N> &b, const Doubles<N> &c, const Doubles<N> &d)
{
	a.m = _mm_mul_pd(b.m,c.m);
	a.v.m = _mm_mul_pd(b.m,d.m);
	cross(a.v.v,b.v,c.v,d.v);
}

template <>
INLINE void cross<2>(Doubles<4> &a, const Doubles<2> &b, const Doubles<2> &c, const Doubles<2> &d)
{ a.m = _mm_mul_pd(b.m,c.m); a.v.m = _mm_mul_pd(b.m,d.m); }

template <> template <>
INLINE void Doubles<NxN>::copyI<N>(const Doubles<N> &that)
{ copy<N,0,0,0,0,1,1,1,2,2,3>(that); }

template <> template <>
INLINE void Doubles<NxN>::copyJ<N>(const Doubles<N> &that)
{ copy<N,1,2,3,4,2,3,4,3,4,4>(that); }

template <>
INLINE __m128d sum<N,Nx2N>(const Doubles<Nx2N> &v)
{ return v.asum<0,2,1,8,4,9,5,12>(); }

template <>
INLINE __m128d sum<3,Nx2N>(const Doubles<Nx2N> &v)
{ return v.asum<3,6,10,11,13,15,16,17>(); }

template <>
INLINE __m128d sum<1,Nx2N>(const Doubles<Nx2N> &v)
{
	const __m128d s = v.asum<7,14,18,19>();
	return _mm_set1_pd(s[0]+s[1]);
}


struct Bodies {

	Doubles<N> x, y, z, vx, vy, vz, mass;
	Doubles<NxN> mi, mj;
	
	void offsetMomentum(const double px, const double py, const double pz)
	{
		vx.set0(-px*DIV_SOLAR_MASS);
		vy.set0(-py*DIV_SOLAR_MASS);
		vz.set0(-pz*DIV_SOLAR_MASS);
	}
	
	Bodies():
	
		x(0.0,
			4.84143144246472090e+00,
			8.34336671824457987e+00,
			1.28943695621391310e+01,
			1.53796971148509165e+01),
			
		y(0.0,
			-1.16032004402742839e+00,
			4.12479856412430479e+00,
			-1.51111514016986312e+01,
			-2.59193146099879641e+01),
			
		z(0.0,
			-1.03622044471123109e-01,
			-4.03523417114321381e-01,
			-2.23307578892655734e-01,
			1.79258772950371181e-01),
			
		vx(0.0,
			1.66007664274403694e-03*DAYS_PER_YEAR,
			-2.76742510726862411e-03*DAYS_PER_YEAR,
			2.96460137564761618e-03*DAYS_PER_YEAR,
			2.68067772490389322e-03*DAYS_PER_YEAR),
			
		vy(0.0,
			7.69901118419740425e-03*DAYS_PER_YEAR,
			4.99852801234917238e-03*DAYS_PER_YEAR,
			2.37847173959480950e-03*DAYS_PER_YEAR,
			1.62824170038242295e-03*DAYS_PER_YEAR),
			
		vz(0.0,
			-6.90460016972063023e-05*DAYS_PER_YEAR,
			2.30417297573763929e-05*DAYS_PER_YEAR,
			-2.96589568540237556e-05*DAYS_PER_YEAR,
			-9.51592254519715870e-05*DAYS_PER_YEAR),
			
		mass(SOLAR_MASS,
			9.54791938424326609e-04*SOLAR_MASS,
			2.85885980666130812e-04*SOLAR_MASS,
			4.36624404335156298e-05*SOLAR_MASS,
			5.15138902046611451e-05*SOLAR_MASS)
	{
		mi.copyI(mass);
		mj.copyJ(-mass);
	}
};


struct NBodySystem {

	Bodies bodies;

	NBodySystem()
	{
		Doubles<N> px(bodies.vx);
		Doubles<N> py(bodies.vy);
		Doubles<N> pz(bodies.vz);
		px *= bodies.mass;
		py *= bodies.mass;
		pz *= bodies.mass;
		bodies.offsetMomentum(px.sum(),py.sum(),pz.sum());
	}
	
	INLINE void advance(const double dt)
	{
		Doubles<NxN> xi, yi, zi, xj, yj, zj;
		
		xi.copyI(bodies.x);
		yi.copyI(bodies.y);
		zi.copyI(bodies.z);
		
		xj.copyJ(bodies.x);
		yj.copyJ(bodies.y);
		zj.copyJ(bodies.z);
		
		Doubles<NxN> dx = xi-xj;
		Doubles<NxN> dy = yi-yj;
		Doubles<NxN> dz = zi-zj;
		Doubles<NxN> d2 = dx*dx+dy*dy+dz*dz;
		
		Doubles<NxN> d(d2);
		d.rsqrt();
		d *= 1.5-0.5*d2*d*d;
		d *= dt*d*d;
		
		dx *= d;
		dy *= d;
		dz *= d;
		
		Doubles<Nx2N> vxij, vyij, vzij;
		cross(vxij,dx,bodies.mj,bodies.mi);
		cross(vyij,dy,bodies.mj,bodies.mi);
		cross(vzij,dz,bodies.mj,bodies.mi);

		accum(bodies.vx,vxij);
		accum(bodies.vy,vyij);
		accum(bodies.vz,vzij);
		
		bodies.x += dt*bodies.vx;
		bodies.y += dt*bodies.vy;
		bodies.z += dt*bodies.vz;
	}
	
	double energy() const
	{
		Doubles<N> v;
		v = (bodies.vx*bodies.vx+bodies.vy*bodies.vy+bodies.vz*bodies.vz)*bodies.mass;
		double e = 0.5*v.sum();
		
		Doubles<NxN> xi, yi, zi, xj, yj, zj;
		
		xi.copyI(bodies.x);
		yi.copyI(bodies.y);
		zi.copyI(bodies.z);
		
		xj.copyJ(bodies.x);
		yj.copyJ(bodies.y);
		zj.copyJ(bodies.z);
		
		Doubles<NxN> dx = xi-xj;
		Doubles<NxN> dy = yi-yj;
		Doubles<NxN> dz = zi-zj;
		Doubles<NxN> d = dx*dx+dy*dy+dz*dz;
		
		d.drsqrt();
		
		Doubles<NxN> m(bodies.mi*bodies.mj*d);
		e += m.sum();
		
		return e;
	}
};


int main(const int argc, const char *const *const argv)
{
	assert(2 == argc);
	const int n = std::stoi(argv[1]);
	assert(0 <= n);
	NBodySystem system;
	printf("%.16f\n",system.energy());
	for (int i = 0; i < n; ++i) system.advance(DT);
	printf("%.16f\n",system.energy());
	return 0;
}
