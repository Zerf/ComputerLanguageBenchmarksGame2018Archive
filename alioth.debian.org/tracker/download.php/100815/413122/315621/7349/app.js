/// <reference path="scripts/typings/node/node.d.ts" />
/* The Computer Language Benchmarks Game
   http://benchmarksgame.alioth.debian.org/

   contributed by Josh Goldfoot
*/
"use strict";
var readline_1 = require("readline");
var RefNum = (function () {
    function RefNum(n) {
        this.num = n;
    }
    return RefNum;
}());
function frequency(sequence, length) {
    var freq = new Map();
    var n = sequence.length - length + 1;
    var sub = "";
    var m;
    for (var i = 0; i < n; i++) {
        sub = sequence.substr(i, length);
        m = freq.get(sub);
        if (m === undefined) {
            freq.set(sub, new RefNum(1));
        }
        else {
            m.num += 1;
        }
    }
    return freq;
}
function sort(sequence, length) {
    var freq = frequency(sequence, length);
    var keys = new Array();
    for (var _i = 0, _a = freq.keys(); _i < _a.length; _i++) {
        var k = _a[_i];
        keys.push(k);
    }
    keys.sort(function (a, b) { return (freq.get(b).num - freq.get(a).num); });
    var n = sequence.length - length + 1;
    keys.forEach(function (key) {
        var count = (freq.get(key).num * 100 / n).toFixed(3);
        console.log(key + " " + count);
    });
    console.log("");
}
function find(haystack, needle) {
    var freq = frequency(haystack, needle.length);
    var m = freq.get(needle);
    var num = m ? m.num : 0;
    console.log(num + "\t" + needle);
}
function main() {
    var sequence = "";
    var reading = false;
    readline_1.createInterface({ input: process.stdin, output: process.stdout })
        .on('line', function (line) {
        if (reading) {
            if (line[0] !== '>')
                sequence += line.toUpperCase();
        }
        else
            reading = line.substr(0, 6) === '>THREE';
    }).on('close', function () {
        sort(sequence, 1);
        sort(sequence, 2);
        find(sequence, 'GGT');
        find(sequence, 'GGTA');
        find(sequence, 'GGTATT');
        find(sequence, 'GGTATTTTAATT');
        find(sequence, 'GGTATTTTAATTTATAGT');
    });
}
main();
//# sourceMappingURL=app.js.map