#![crate_type = "lib"]

// This function generates Double-Precision Floating-Point instructions (divpd & addpd)

pub fn div_and_add(top0 : f64, top1 : f64, bot0 : f64, bot1 : f64, sum : &mut [f64; 2]){	
	sum[0] += top0/bot0;
	sum[1] += top1/bot1;
}
