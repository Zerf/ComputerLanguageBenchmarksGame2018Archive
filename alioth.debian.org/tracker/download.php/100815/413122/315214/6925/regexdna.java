import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.regex.Pattern;

public final class regexdna {
	private final static int _1M = 1024 * 1024;

	public static void main(String... args) throws Exception {
		ByteBuffer input = readInput();
		ByteBuffer normalizedInput = normalize(input);

		countMatches(normalizedInput);
		int replacedLength = countReplacedLength(normalizedInput);

		System.out.println();
		System.out.println(input.position());
		System.out.println(normalizedInput.position());
		System.out.println(replacedLength);
	}

	private static void countMatches(ByteBuffer input) throws Exception {
		MatchCounter[] threads = new MatchCounter[] {
				new MatchCounter("agggtaaa|tttaccct", 0x6167676774616161L, 0x7474746163636374L, input),
				new MatchCounter("[cgt]gggtaaa|tttaccc[acg]", 0x67676774616161L, 0x7474746163636300L, input),
				new MatchCounter("a[act]ggtaaa|tttacc[agt]t", 0x6100676774616161L, 0x7474746163630074L, input),
				new MatchCounter("ag[act]gtaaa|tttac[agt]ct", 0x6167006774616161L, 0x7474746163006374L, input),
				new MatchCounter("agg[act]taaa|ttta[agt]cct", 0x6167670074616161L, 0x7474746100636374L, input),
				new MatchCounter("aggg[acg]aaa|ttt[cgt]ccct", 0x6167676700616161L, 0x7474740063636374L, input),
				new MatchCounter("agggt[cgt]aa|tt[acg]accct", 0x6167676774006161L, 0x7474006163636374L, input),
				new MatchCounter("agggta[cgt]a|t[acg]taccct", 0x6167676774610061L, 0x7400746163636374L, input),
				new MatchCounter("agggtaa[cgt]|[acg]ttaccct", 0x6167676774616100L, 0x74746163636374L, input), };

		for (MatchCounter matchCounter : threads) {
			matchCounter.start();
		}

		for (MatchCounter matchCounter : threads) {
			matchCounter.join();
			System.out.print(matchCounter.regex);
			System.out.print(' ');
			System.out.println(matchCounter.matchCount);
		}
	}

	private static int countReplacedLength(ByteBuffer input) {
		byte[] lengths = new byte[256];
		Arrays.fill(lengths, (byte)1);
		lengths['W'] = (byte) "(a|t)".length();
		lengths['Y'] = (byte) "(c|t)".length();
		lengths['K'] = (byte) "(g|t)".length();
		lengths['M'] = (byte) "(a|c)".length();
		lengths['S'] = (byte) "(c|g)".length();
		lengths['R'] = (byte) "(a|g)".length();
		lengths['B'] = (byte) "(c|g|t)".length();
		lengths['D'] = (byte) "(a|g|t)".length();
		lengths['V'] = (byte) "(a|c|g)".length();
		lengths['H'] = (byte) "(a|c|t)".length();
		lengths['N'] = (byte) "(a|c|g|t)".length();

		int result = 0;
		int limit = input.position();
		for (int i = 0; i < limit; ++i) {
			result += lengths[input.get(i) & 0xFF];
		}
		return result;
	}

	private static ByteBuffer normalize(ByteBuffer input) {
		ByteBuffer result = ByteBuffer.allocateDirect(input.position());

		boolean nameLine = false;
		int limit = input.position();
		for (int i = 0; i < limit; ++i) {
			byte c = input.get(i);
			if (c == '>') {
				nameLine = true;
				continue;
			} else if (c == '\n') {
				nameLine = false;
				continue;
			} else if (nameLine) {
				continue;
			} else {
				result.put(c);
			}
		}

		return result;
	}

	private static ByteBuffer readInput() throws Exception {
		ByteBuffer result = ByteBuffer.allocateDirect(50 * _1M);

		@SuppressWarnings("resource")
		FileInputStream in = new FileInputStream(FileDescriptor.in);

		FileChannel cin = in.getChannel();

		while (cin.read(result) >= 0) {
			// loop
		}

		return result;
	}

	private static final class MatchCounter extends Thread {
		private final String regex;
		private final long mask1, mask2;
		private final ByteBuffer input;
		private int matchCount;

		private MatchCounter(String regex, long mask1, long mask2, ByteBuffer input) {
			this.regex = regex;
			this.mask1 = mask1;
			this.mask2 = mask2;
			this.input = input;
		}

		@Override
		public void run() {
			Pattern p = Pattern.compile(regex);
			LongCharSequence cs = new LongCharSequence();

			int limit = input.position() - 7;
			for (int i = 0; i < limit; ++i) {
				long chars = input.getLong(i);
				if ((chars & mask1) != mask1 & (chars & mask2) != mask2) {
					continue;
				}

				cs.value = chars;
				if (p.matcher(cs).matches()) {
					matchCount++;
				}
			}
		}
	}

	private final static class LongCharSequence implements CharSequence {
		private long value;

		@Override
		public int length() {
			return 8;
		}

		@Override
		public char charAt(int index) {
			int shift = (7 - index) << 3;
			long shifted = (value & (0xFFL << shift)) >> shift;
			return (char) (shifted & 0xFFL);
		}

		@Override
		public CharSequence subSequence(int start, int end) {
			throw new UnsupportedOperationException();
		}
	}
}
