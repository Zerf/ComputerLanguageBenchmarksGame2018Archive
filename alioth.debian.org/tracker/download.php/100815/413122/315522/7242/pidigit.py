# The Computer Language Benchmarks Game
# http://benchmarksgame.alioth.debian.org/
#
# contributed by Simon Descarpentries - 2016-10-05
# inspired by Node.js version from Zach Kelling and Roman Pletnev

import sys


def calculatePi(N):
    i = 0
    k = 0
    k1 = 1
    ns = 0
    a = 0
    d = 1
    m = 0
    n = 1
    t = 0
    u = 0

    while True:
        k += 1
        k1 += 2
        t = n << 1
        n = n * k
        a = (a + t) * k1
        d = d * k1

        if a - n >= 0:
            m = n * 3 + a
            t = m // d
            u = m % d + n

            if d - u > 0:
                ns = ns * 10 + t
                i += 1
                last = i >= N

                if i % 10 == 0 and not last:
                    print(str(ns).zfill(10).ljust(10) + '\t:' + str(i))
                    ns = 0
                if last:
                    print(str(ns).zfill(N % 10).ljust(10) + '\t:' + str(i))
                    break

                a = (a - d * t) * 10
                n = n * 10


if __name__ == '__main__':
    calculatePi(int(sys.argv[1]))
