/* The Computer Language Benchmarks Game

   http://benchmarksgame.alioth.debian.org/



   contributed by Mark C. Lewis

   modified slightly by Chad Whipkey
  
   converted from Java#2 by Razvan Petrescu

*/

import std.stdio;
import std.conv;
import std.math;

immutable double PI = 3.141592653589793;
immutable double SOLAR_MASS = 4 * PI * PI;
immutable double DAYS_PER_YEAR = 365.24;

void main(string[] args) {
	uint n = parse!uint(args[1]);
	
	NBodySystem system = new NBodySystem();
	writefln("%.9f", system.energy());
	for (uint i = 0; i < n; i++)
		system.advance(0.01);
	writefln("%.9f", system.energy());
}

struct Body {
	double x, y, z, vx, vy, vz, mass;
	
	static Body jupiter = Body(
			4.84143144246472090e+00,
		   -1.16032004402742839e+00,
		   -1.03622044471123109e-01,
			1.66007664274403694e-03 * DAYS_PER_YEAR,
			7.69901118419740425e-03 * DAYS_PER_YEAR,
		   -6.90460016972063023e-05 * DAYS_PER_YEAR,
			9.54791938424326609e-04 * SOLAR_MASS
		);
	
	static Body saturn = Body(
			8.34336671824457987e+00,
			4.12479856412430479e+00,
		   -4.03523417114321381e-01,
		   -2.76742510726862411e-03 * DAYS_PER_YEAR,
			4.99852801234917238e-03 * DAYS_PER_YEAR,
			2.30417297573763929e-05 * DAYS_PER_YEAR,
			2.85885980666130812e-04 * SOLAR_MASS
		);
	
	static Body uranus = Body(
			1.28943695621391310e+01,
		   -1.51111514016986312e+01,
		   -2.23307578892655734e-01,
			2.96460137564761618e-03 * DAYS_PER_YEAR,
			2.37847173959480950e-03 * DAYS_PER_YEAR,
		   -2.96589568540237556e-05 * DAYS_PER_YEAR,
			4.36624404335156298e-05 * SOLAR_MASS
		);
	
	static Body neptune = Body(
			1.53796971148509165e+01,
		   -2.59193146099879641e+01,
			1.79258772950371181e-01,
			2.68067772490389322e-03 * DAYS_PER_YEAR,
			1.62824170038242295e-03 * DAYS_PER_YEAR,
		   -9.51592254519715870e-05 * DAYS_PER_YEAR,
			5.15138902046611451e-05 * SOLAR_MASS
		);
	
	static Body sun = Body(0, 0, 0, 0, 0, 0, SOLAR_MASS);
	
	Body offsetMomentum(double px, double py, double pz) {
		vx = -px / SOLAR_MASS;
		vy = -py / SOLAR_MASS;
		vz = -pz / SOLAR_MASS;
		return this;
	}
}

class NBodySystem {
	
	private Body[] bodies;
	
	public this() {
		
		bodies = new Body[5];
		bodies[0] = Body.sun;
		bodies[1] = Body.jupiter;
		bodies[2] = Body.saturn;
		bodies[3] = Body.uranus;
		bodies[4] = Body.neptune;		
		
		double px = 0.0;
		double py = 0.0;
		double pz = 0.0;
		for (int i=0; i < bodies.length; ++i) {
			px += bodies[i].vx * bodies[i].mass;
			py += bodies[i].vy * bodies[i].mass;
			pz += bodies[i].vz * bodies[i].mass;
		}
		bodies[0].offsetMomentum(px, py, pz);
	}
			
	void advance(double dt) {
		foreach (i, ref bi; bodies) {
			foreach (j, ref bj; bodies[i+1 .. $]) {			
				double dx = bi.x - bj.x;
				double dy = bi.y - bj.y;
				double dz = bi.z - bj.z;
				double dSquared = dx * dx + dy * dy + dz * dz;
				double distance = sqrt(dSquared);
				double mag = dt / (distance * dSquared);
				
				bi.vx -= dx * bj.mass * mag;
				bi.vy -= dy * bj.mass * mag;
				bi.vz -= dz * bj.mass * mag;
				
				bj.vx += dx * bi.mass * mag;
				bj.vy += dy * bi.mass * mag;
				bj.vz += dz * bi.mass * mag;
			}
		}
		
		foreach (ref bi; bodies) {
			bi.x += dt * bi.vx;
			bi.y += dt * bi.vy;
			bi.z += dt * bi.vz;
		}

/*		
		foreach (i, bi; bodies) {
			writefln("%d: (%.9f, %.9f, %.9f) - V(%.9f, %.9f, %.9f)", i, 
				bi.x, bi.y, bi.z, bi.vx, bi.vy, bi.vz);
		}
*/		
	}
	
	double energy() {
		double dx, dy, dz, distance;
		double e = 0;
		
		foreach (i, bi; bodies) {
			e += 0.5 * bi.mass * (bi.vx * bi.vx
							+ bi.vy * bi.vy
							+ bi.vz * bi.vz);
				
			foreach (j, bj; bodies[i+1 .. $]) {
				dx = bi.x - bj.x;
				dy = bi.y - bj.y;
				dz = bi.z - bj.z;
				
				distance = sqrt(dx*dx + dy*dy + dz*dz);
				e -= (bi.mass * bj.mass) / distance;
			}
		}		
		return e;
	}	
}
