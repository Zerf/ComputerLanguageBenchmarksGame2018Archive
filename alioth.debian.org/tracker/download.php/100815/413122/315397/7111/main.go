package main

import (
	"flag"
	"fmt"
	"strconv"
	"sync"
	"sync/atomic"
	"unsafe"
)

var n = 0

const MaxSize = 1 << 24

type Node struct {
	item        int
	left, right *Node
}

type NodeArena struct {
	buffers [][]byte
	ptr     uintptr
	endptr  uintptr
	size    uint
	mutex   sync.Mutex
}

const nodesize = unsafe.Sizeof(Node{})

func NewArena() *NodeArena {
	buf := make([]byte, MaxSize, MaxSize)
	return &NodeArena{
		buffers: [][]byte{buf},
		ptr:     uintptr(unsafe.Pointer(&buf[0])),
		endptr:  uintptr(unsafe.Pointer(&buf[MaxSize-1])),
	}
}

func (na *NodeArena) AllocNode() *Node {
	var ptr uintptr
	for {
		ptr = na.ptr

		if ptr+nodesize > na.endptr {
			na.mutex.Lock()
			if ptr+nodesize > na.endptr {
				buf := make([]byte, MaxSize, MaxSize)
				na.buffers = append(na.buffers, buf)
				na.ptr = uintptr(unsafe.Pointer(&buf[0]))
				na.endptr = uintptr(unsafe.Pointer(&buf[MaxSize-1]))
			}
			ptr = na.ptr
			na.mutex.Unlock()
		}
		if atomic.CompareAndSwapUintptr(&na.ptr, ptr, ptr+nodesize) {
			break
		}
	}
	return (*Node)(unsafe.Pointer(ptr))
}

func (na *NodeArena) Deallocate() {
	na.ptr = uintptr(unsafe.Pointer(&na.buffers[0][0]))
	na.endptr = uintptr(unsafe.Pointer(&na.buffers[0][MaxSize-1]))
	na.buffers = na.buffers[:1]
}

func bottomUpTree(item, depth int, arena *NodeArena) *Node {
	node := arena.AllocNode()
	node.item = item

	if depth <= 0 {
		return node
	}

	node.left = bottomUpTree(2*item-1, depth-1, arena)
	node.right = bottomUpTree(2*item, depth-1, arena)
	return node
}

func (n *Node) itemCheck() int {
	if n.left == nil {
		return n.item
	}
	return n.item + n.left.itemCheck() - n.right.itemCheck()
}

const minDepth = 4

func main() {
	flag.Parse()
	if flag.NArg() > 0 {
		n, _ = strconv.Atoi(flag.Arg(0))
	}

	maxDepth := n
	if minDepth+2 > n {
		maxDepth = minDepth + 2
	}
	stretchDepth := maxDepth + 1

	check_l := bottomUpTree(0, stretchDepth, NewArena()).itemCheck()
	fmt.Printf("stretch tree of depth %d\t check: %d\n", stretchDepth, check_l)

	longLivedTree := bottomUpTree(0, maxDepth, NewArena())

	var wg sync.WaitGroup
	result := make([]string, maxDepth+1)

	for depth_l := minDepth; depth_l <= maxDepth; depth_l += 2 {
		wg.Add(1)
		go func(depth int, check int, r *string) {
			defer wg.Done()
			iterations := 1 << uint(maxDepth-depth+minDepth)
			check = 0

			arena := NewArena()
			for i := 1; i <= iterations; i++ {
				shortLivedTree1 := bottomUpTree(i, depth, arena)
				check += shortLivedTree1.itemCheck()
				shortLivedTree2 := bottomUpTree(-i, depth, arena)
				check += shortLivedTree2.itemCheck()
				arena.Deallocate()
			}
			*r = fmt.Sprintf("%d\t trees of depth %d\t check: %d", iterations*2, depth, check)
		}(depth_l, check_l, &result[depth_l])
	}
	wg.Wait()
	for depth := minDepth; depth <= maxDepth; depth += 2 {
		fmt.Println(result[depth])
	}
	fmt.Printf("long lived tree of depth %d\t check: %d\n", maxDepth, longLivedTree.itemCheck())
}
