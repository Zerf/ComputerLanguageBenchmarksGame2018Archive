import sys
from decimal import Decimal


def pidigits(n):
    q = t = 1
    r = k = counter = len_s = 0
    s = ''
    while counter < n:
        k += 1
        tmp = 2*k + 1
        r = tmp * (2*q + r)
        t *= tmp
        q *= k
        if q > r:
            continue
        digit = int(Decimal(q*3 + r) / t)
        if digit != int(Decimal(q*4 + r) / t):
            continue
        counter += 1
        s += repr(digit)
        len_s += 1
        if len_s == 10 or counter == n:
            yield s
            s = ''
            len_s = 0
        q *= 10
        r = 10 * (r - digit*t)


if __name__ == '__main__':
    n = int(sys.argv[1])
    d = 0
    for i, digits in enumerate(pidigits(n)):
        l = len(digits)
        d += l
        print('%s%s\t:%s' % (digits, ' '*(10-l), d))
