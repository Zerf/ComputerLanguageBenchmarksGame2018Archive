/* The Computer Language Benchmarks Game
   http://benchmarksgame.alioth.debian.org/

   Contributed by Jesse Millikan
   Modified by Matt Baker
   Modified by Joe Farro (ArrayBuffer and TypedArrays)
*/


var packedStr;


function intToChar(num, len) {
    var res = '';
    while (len > 0) {
        switch (num & 3) {
            case 0: res = 'A' + res; break;
            case 1: res = 'C' + res; break;
            case 2: res = 'G' + res; break;
            case 3: res = 'T' + res; break;
        }
        num = num >>> 2;
        len--;
    }
    return res;
}


function charToInt(char) {
    switch (char) {
        case 'A': return 0;
        case 'a': return 0;
        case 'C': return 1;
        case 'c': return 1;
        case 'G': return 2;
        case 'g': return 2;
        case 'T': return 3;
        case 't': return 3;
    }
}


// Converts the "test" strings. This expects 8 bytes to be enough.
function strToBytes(s) {

    var len = s.length,
        cur32 = new Uint32Array(new ArrayBuffer(8)),
        i = 0;

    if (len > 16) {
        while (i < len) {
            cur32[0] = cur32[0] << 2 | cur32[1] >>> 30;
            cur32[1] = cur32[1] << 2 | charToInt(s[i]);
            i++;
        }
    } else {
        while (i < len) {
            cur32[0] = cur32[0] << 2 | charToInt(s[i]);
            i++;
        }
    }
    return cur32;
}


function frequency(len) {

    var freq = {},
        mask,
        max,
        cur32 = new Uint32Array(new ArrayBuffer(8), 0, 2),
        i,
        idx32,
        bucket;

    // if we're matching strings longer than 16 chars (each char takes 2 bits)
    // then need to save the string in 2 uint32
    if (len > 16) {
        mask = Math.pow(2, (len - 16) * 2) - 1;
        idx32 = 1;
        max = 16;
    } else {
        mask = Math.pow(2, len * 2) - 1;
        idx32 = 0;
        max = len;
    }

    // max is countdown and number of bits to shift so have the char in the
    // correct spot in uint32
    max = 2 * max - 2;
    i = 0;
    while (max + 1 > 0) {
        cur32[idx32] |= packedStr[i] << max;
        i++;
        max -= 2;
    }
    // if it's longer than 16 chars, then fill out the rest by shifting from
    // the frist uint32 into the second uint32 and cont to fill the first
    if (len > 16) {
        while (i < len) {
            cur32[0] = cur32[0] << 2 | cur32[1] >>> 30;
            cur32[1] = cur32[1] << 2 | packedStr[i];
            i++;
        }
        // update counter for the first string
        // when working with more than 16 chars store in 2-levels, one
        // for each uint32
        bucket = freq[cur32[0]] = {};
        bucket[cur32[1]] = 1;
    } else {
        // update counter for the first string (only needs one uint32)
        freq[cur32[0]] = 1;
    }

    max = packedStr.length;
    i = len;
    if (len > 16) {
        while (i < max) {
            cur32[0] = (cur32[0] << 2 & mask) | cur32[1] >>> 30;
            cur32[1] = cur32[1] << 2 | packedStr[i];
            bucket = freq[cur32[0]] = freq[cur32[0]] || {};
            bucket[cur32[1]] = (bucket[cur32[1]] || 0) + 1;
            i++;
        }
    } else {
        while (i < max) {
            // shift everything over, mask it, add the new char to the end, etc
            cur32[0] = cur32[0] << 2 & mask | packedStr[i];
            freq[cur32[0]] = (freq[cur32[0]] || 0) + 1;
            i++;
        }
    }
    return freq;
}


function sort(length) {

    var f = frequency(length),
        keys = Object.keys(f),
        n = 100 / (packedStr.length - length + 1),
        i;

    keys.sort(function(a, b){ return f[b] - f[a]; });

    for (i in keys) {
        print(intToChar(keys[i], length), (f[keys[i]] * n).toFixed(3));
    }
    print();
}


function find(s) {

    var bf = frequency(s.length),
        sbytes = strToBytes(s),
        count;

    if (s.length > 16) {
        count = bf[sbytes[0]][sbytes[1]];
    } else {
        count = bf[sbytes[0]];
    }
    print(count + '\t' + s);
}


function readInput() {

  var lines = [],
      len = 0,
      l,
      buf,
      pos,
      i,
      c;

    while (readline().substr(0, 3) !== '>TH') {
    }
    
    while ((l = readline()) && l[0] !== '>') {
        lines.push(l);
        len += l.length;
    }

    buf = new ArrayBuffer(len);
    packedStr = new Uint8Array(buf);

    pos = 0;
    for (i in lines) {
        l = lines[i];
        i = 0;
        while (i < l.length) {
            // convert each character to a 2-bit representation, save that
            // instead of the string
            c = charToInt(l[i]);
            packedStr[pos] = c;
            i++;
            pos++;
        }
    }
}


readInput();

sort(1);
sort(2);

find("GGT");
find("GGTA");
find("GGTATT");
find("GGTATTTTAATT");
find("GGTATTTTAATTTATAGT");
