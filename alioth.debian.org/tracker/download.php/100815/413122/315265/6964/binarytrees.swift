import CoreFoundation

struct TreeNode {

    let item:Int
    let left,right:UnsafeMutablePointer<TreeNode>
    
    func check() -> Int {
        if left != nil {
           return item + left.memory.check() - right.memory.check()
        } else {
           return item
        }
    }

}

class Pool<T> {

    private var mem:UnsafeMutablePointer<T>
    let first:UnsafeMutablePointer<T>
    
    init(_ size:Int) {
        mem = UnsafeMutablePointer<T>.alloc(size)
        first = mem
    }

    func next()->UnsafeMutablePointer<T> {
        let m = mem
        mem = mem.successor()
        return m
    }

    func reset() {
        mem = first
    }

}


func bottomUpTree(item:Int, _ depth: Int, _ node:UnsafeMutablePointer<TreeNode>, _ pool:Pool<TreeNode>) {
    
    if depth > 0 {
        let i = item*2
        let d = depth-1
        let l = pool.next()
        let r = pool.next()
        node.memory = TreeNode(item:item, left:l, right:r)
        bottomUpTree(i-1, d, l, pool)
        bottomUpTree(i, d, r, pool)
    } else {
        node.memory = TreeNode(item:item, left:nil, right:nil)
    }
    
}

let n: Int = Int(Process.arguments[1])!
let minDepth = 4
let maxDepth = n
let stretchDepth = n + 1

let size = (1 << (maxDepth + minDepth))/4
let longPool = Pool<TreeNode>(size)
let shortPool = Pool<TreeNode>(size)

bottomUpTree(0,stretchDepth, shortPool.next(), shortPool)
let check = shortPool.first.memory.check()
print("stretch tree of depth \(stretchDepth)\t check: \(check)")

bottomUpTree(0,maxDepth,longPool.next(),longPool)
var depth = minDepth
while depth <= maxDepth {
   let iterations = 1 << (maxDepth - depth + minDepth)
   var check = 0
   for i in 0..<iterations {
      
      shortPool.reset()
      bottomUpTree(i,depth,shortPool.next(),shortPool)
      check += shortPool.first.memory.check();

      shortPool.reset()
      bottomUpTree(-i,depth,shortPool.next(),shortPool)
      check += shortPool.first.memory.check();

   
   }
   print("\(iterations*2)\t trees of depth \(depth)\t check: \(check)")
   depth += 2
}

print("long lived tree of depth \(maxDepth)\t check: \(longPool.first.memory.check())")

