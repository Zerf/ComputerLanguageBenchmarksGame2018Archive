/* The Computer Language Benchmarks Game
   http://benchmarksgame.alioth.debian.org/

   converted to C++ from D by Rafal Rusin
   modified by Vaclav Haisman
   modified by The Anh to compile with g++ 4.3.2
   modified by Branimir Maksimovic
   modified by Kim Walisch
   modified by Tavis Bohne
   modified by Tomas Dzetkulic

   compiles with gcc fasta.cpp -std=c++11 -O2
*/

#include <array>
#include <iostream>
#include <numeric>

#include <string.h>

constexpr int MAXLINE = 60;
constexpr int NUM_LINES = 256;

constexpr int alu_len = 287;
constexpr char alu[alu_len + 1] =
  "GGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGG"
  "GAGGCCGAGGCGGGCGGATCACCTGAGGTCAGGAGTTCGAGA"
  "CCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAAT"
  "ACAAAAATTAGCCGGGCGTGGTGGCGCGCGCCTGTAATCCCA"
  "GCTACTCGGGAGGCTGAGGCAGGAGAATCGCTTGAACCCGGG"
  "AGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCC"
  "AGCCTGGGCGACAGAGCGAGACTCCGTCTCAAAAA";

std::array<float, 15> iub = {{ 
  0.27f, 0.12f, 0.12f, 0.27f, 0.02f,
  0.02f, 0.02f, 0.02f, 0.02f, 0.02f,
  0.02f, 0.02f, 0.02f, 0.02f, 0.02f }};

const char iub_c[] = "acgtBDHKMNRSVWY";  

std::array<float, 4> homosapiens = {{ 
  0.3029549426680f, 0.1979883004921f,
  0.1975473066391f, 0.3015094502008f }};

const char homosapiens_c[] = "acgt";

class alu_repeat_functor {
public:
  alu_repeat_functor() : i(0) {
    for (int i = 0; i < sizeof(alu2); ++i) {
      alu2[i] = alu[i % alu_len];
    }
  }
  inline void GetLine(char* dest) {
    memcpy(dest, alu2 + i, MAXLINE);
    i = (i + MAXLINE) % alu_len;
  }
  inline void Get(char* dest, int num) {
    memcpy(dest, alu2 + i, num);
    i = (i + num) % alu_len;
  }
private:
  int i;
  char alu2[sizeof(alu) + MAXLINE];
};

class Random {
public:
  Random() : last(42) {};
  static constexpr int IM = 139968, IA = 3877, IC = 29573;
  inline float Get(float max = 1.0f) {
    last = (last * IA + IC) % IM;
    return max * last * (1.0f / IM);    
  }
private:
  int last;
} random_;

template<int size>
inline int binary_search(const float* const iub, float p) {
  constexpr int mid = (size - 1) / 2;
  return (p <= iub[mid]) ? binary_search<mid + 1>(iub, p)
      : mid + 1 + binary_search<size - mid - 1>(iub + mid + 1, p);
}

template<>
inline int binary_search<1>(const float* const iub, float p) {
  return 0;
}

template<typename array_type>
class random_functor_type {
public:
  random_functor_type(const array_type& iub, const char* iub_c)
      : iub_(iub) , iub_c_(iub_c) {}
  inline void Get(char* dest, int num) {
    for (int i = 0; i < num; ++i) dest[i] = GetInternal();
  }
  inline void GetLine(char* dest) {
    for (int i = 0; i < MAXLINE; ++i) dest[i] = GetInternal();
  }
private:
  inline int GetInternal() {
    return iub_c_[binary_search<std::tuple_size<array_type>::value>(
        iub_.data(), random_.Get(1.0f))];
  }
  const array_type iub_;
  const char* iub_c_;
};

template<typename array_type>
random_functor_type<array_type> make_random_functor_type(
      const array_type& x, const char* y) {
  return random_functor_type<array_type>(x, y);
}

template<typename array_type>
void make_cumulative(array_type& x) {
  std::partial_sum(x.begin(), x.end(), x.begin(),
                   [] (float l, float r) -> float { r += l; return r; });
}

template <typename F>
void make(const char* const desc, int const n, F functor) {
  std::cout << '>' << desc << '\n';
  char line[(MAXLINE + 1) * NUM_LINES];
  for (int i = 0; i < NUM_LINES; ++i) {
    line[MAXLINE + (MAXLINE + 1) * i] = '\n'; 
  }
  int i = 0;
  for (; i + MAXLINE * NUM_LINES <= n; i += MAXLINE * NUM_LINES) {
    for (int j = 0; j < NUM_LINES; ++j) {
      functor.GetLine(line + (MAXLINE + 1) * j);
    }
    std::cout.write(line, (MAXLINE + 1) * NUM_LINES);
  }
  for (; i + MAXLINE <= n; i += MAXLINE) {
    functor.GetLine(line);
    std::cout.write(line, MAXLINE + 1);
  }
  if (n - i) {
    functor.Get(line, n - i);
    line[n - i]='\n';
    std::cout.write(line, n - i + 1);
  }
}

int main(int argc, char *argv[]) {
  int n;
  if (argc < 2 || (n = std::atoi(argv[1])) <= 0) {
    std::cerr << "usage: " << argv[0] << " length\n";
    return 1;
  }

  std::cout.sync_with_stdio(false);
  make_cumulative(iub);
  make_cumulative(homosapiens);

  make("ONE Homo sapiens alu", n * 2, alu_repeat_functor()); 
  make("TWO IUB ambiguity codes", n * 3, make_random_functor_type(iub, iub_c));
  make("THREE Homo sapiens frequency", n * 5, 
       make_random_functor_type(homosapiens, homosapiens_c));
  return 0;
}
