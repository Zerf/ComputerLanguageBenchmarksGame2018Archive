/* The Computer Language Benchmarks Game
 * http://benchmarksgame.alioth.debian.org/
 *
 * contributed by Matthew McMullan
 * based on C source by Ledrug Katz
 *
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <pthread.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <cstring>
#include <algorithm>
#include <xmmintrin.h>
#include <tmmintrin.h>

/* this depends highly on the platform.  It might be faster to use
	 char type on 32-bit systems; it might be faster to use unsigned. */

typedef char elem;

elem s[16] __attribute__ ((aligned (16)));
int max_n;
int odd = 0;
int checksum = 0;
// naieve method of rotation using basic sisd instructions for sanity's sake
inline void rotate_sisd(int n) {
	elem c;
	register int i;
	c = s[0];
	for (i = 1; i <= n; i++) s[i-1] = s[i];
	s[n] = c;
}
// flip and rotation masks needed to use SSE for rotations and flipping
// the number of these remains constant for all sizes
__m128i flip_masks[16];
__m128i rotate_masks[16];
__m128i MM_ITRUE;
// populate the data in the masks. could be hard coded. will never change.
void popmasks() {
	char mask[16];
	for (unsigned i = 0; i<16; ++i) {
		for (unsigned j = 0; j<16; ++j) mask[j] = j;
		// this is actually slower than a for loop for small arrays
		std::reverse(mask,mask+i+1);
		flip_masks[i] = _mm_loadu_si128((__m128i*)mask);
		
		for (unsigned j = 0; j<16; ++j) s[j] = j;
		rotate_sisd(i);
		rotate_masks[i] = _mm_load_si128((__m128i*)s);
	}
	char truth[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	MM_ITRUE = _mm_loadu_si128((__m128i*)truth);
}
inline void rotate(int n) {
	// use SSE to rotate the values
	// n could get as high as the max for the range,
	//   but only 16 constants will ever be needed
	_mm_store_si128((__m128i*)s,
		_mm_shuffle_epi8(_mm_load_si128((__m128i*)s),rotate_masks[n]));
}

// a place to put the backlog of permutations
#define chunksize 60
struct Chunk {
	__m128i perms[chunksize] __attribute__ ((aligned (64)));
	char tmp[16] __attribute__ ((aligned (16)));
	char tmp2[16] __attribute__ ((aligned (16)));
	int perm_max;
	elem starts[chunksize];
	short odds[chunksize];
	bool free, full;
};
Chunk chunks[6];
// for flipping

int qp(int perm_max, __m128i *perms, char *tmp, char *tmp2,
		elem *starts, short *odds, int &maxflips) {
	// process the queue
	int k;
	int checksum = 0;
	// do 2 at a time when possible to take advantage of pipelining
	// see the next loop for implementation logic
	for (k=0; k<perm_max-1; k+=2) {
		__m128i perm1 = perms[k];
		__m128i perm2 = perms[k+1];
		
		int f1 = 0, f2 = 0;
		int toterm1 = starts[k], toterm2 = starts[k+1];
		while (toterm1 && toterm2) {
			perm1 = _mm_shuffle_epi8(perm1,flip_masks[toterm1]);
			perm2 = _mm_shuffle_epi8(perm2,flip_masks[toterm2]);
			_mm_storel_epi64((__m128i*)tmp,perm1);
			_mm_storel_epi64((__m128i*)tmp2,perm2);
			toterm1 = tmp[0];
			toterm2 = tmp2[0];
			++f1; ++f2;
		}
		while (toterm1) {
			perm1 = _mm_shuffle_epi8(perm1,flip_masks[toterm1]);
			_mm_storel_epi64((__m128i*)tmp,perm1);
			toterm1 = tmp[0];
			++f1;
		}
		while (toterm2) {
			perm2 = _mm_shuffle_epi8(perm2,flip_masks[toterm2]);
			_mm_storel_epi64((__m128i*)tmp2,perm2);
			toterm2 = tmp2[0];
			++f2;
		}
		
		if (f1 > maxflips) maxflips = f1;
		if (f2 > maxflips) maxflips = f2;
		checksum += odds[k] ? -f1 : f1;
		checksum += odds[k+1] ? -f2 : f2;
	}
	// finish up one at a time
	for (;k<perm_max;++k) {
		// get the data out of the structure
		// the whole array is packed into an sse integer type
		// we could use more fairly easily if we wanted to
		__m128i perm = perms[k];
		int f = 0, toterm = starts[k];
		while (toterm) {
			// hardware support for reversing arbitrary subsequences
			perm = _mm_shuffle_epi8(perm,flip_masks[toterm]);
			// check the first number. this is ~1/3 of the execution time
			_mm_storel_epi64((__m128i*)tmp,perm);
			toterm = tmp[0];
			++f;
		}
		
		if (f > maxflips) maxflips = f;
		checksum += odds[k] ? -f : f;
	}
	return checksum;
}

int maxflips = 0;
void tk(int n) {
	int i = 0;
	elem c[16] = {0};
	int perm_max = 0;
	Chunk *active = 0;
	int scanline = 5;
	while (i < n) {
		active = 0;
		while (!active) {
			scanline = (scanline+1)%6;
			if (chunks[scanline].free && !chunks[scanline].full) {
				active = &(chunks[scanline]);
				chunks[scanline].free = false;
			}
		}
		/* Tompkin-Paige iterative perm generation */
		// fill the queue up to 60
		while (i<n && perm_max<chunksize) {
			rotate(i);
			if (c[i] >= i) {
				c[i++] = 0;
				continue;
			}

			c[i]++;
			i = 1;
			odd = ~odd;
			if (*s) {
				if (s[(int)s[0]]) {
					active->perms[perm_max] = _mm_load_si128((__m128i*)s);
					active->starts[perm_max] = *s;
					active->odds[perm_max] = odd;
					perm_max++;
				} else {
					if (maxflips==0) maxflips = 1;
					checksum += odd ? -1 : 1;
				}
			}
		}
		active->perm_max = perm_max;
		chunks[scanline].full = true;
		perm_max = 0;
	}
}
bool terminate = false;
struct csmn {
	int checksum, maxflips;
};
csmn retvals[3];
void *compute(void* ID) {
	int id = *((int*)ID);
	retvals[id].checksum = 0;
	retvals[id].maxflips = 0;
	Chunk *active = &(chunks[id]);
	while (1) {
		while (!active->full) {
			if (chunks[id].full) {
				active = &(chunks[id]);
			} else if (chunks[id+3].full) {
				active = &(chunks[id+3]);
			} else if (terminate) {
				pthread_exit(&(retvals[id]));
				return &(retvals[id]);
			}
		}
		retvals[id].checksum+= qp(active->perm_max, active->perms, active->tmp,
			active->tmp2, active->starts, active->odds, retvals[id].maxflips);
		active->full = false;
		active->free = true;
	}
}
int main(int argc, char **v) {
	int i;
	popmasks();
	if (argc < 2) {
		fprintf(stderr, "usage: %s number\n", v[0]);
		exit(1);
	}

	max_n = atoi(v[1]);
	if (max_n < 3 || max_n > 15) {
		fprintf(stderr, "range: must be 3 <= n <= 12\n");
		exit(1);
	}
	for (i=0; i<6; ++i) {
		chunks[i].free = true;
		chunks[i].full = false;
	}
	for (i = 0; i < max_n; i++) s[i] = i;
	
	int args[3] = {0,1,2};
	pthread_t threads[3];
	pthread_attr_t attributes;
	/* Initialize attributes. */
	pthread_attr_init(&attributes);
	pthread_attr_setdetachstate(&attributes, PTHREAD_CREATE_DETACHED);
	pthread_attr_setstacksize(&attributes, PTHREAD_STACK_MIN);
	for (int i=0; i<3; ++i) {
		pthread_create(threads + i, &attributes, compute, (void*)(args+i));
	}
	tk(max_n);
	terminate = true;
	for (int i=0; i<3; ++i) {
		csmn *retval;
		pthread_join(threads[i], (void**)(&retval));
		retval = retvals+i;
		checksum += retval->checksum;
		if (retval->maxflips > maxflips) {
			maxflips = retval->maxflips;
		}
	}
	
	printf("%d\nPfannkuchen(%d) = %d\n", checksum, max_n, maxflips);

	return 0;
}
