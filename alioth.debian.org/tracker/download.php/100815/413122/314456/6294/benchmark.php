<?php 
/* The Computer Language Benchmarks Game
	http://benchmarksgame.alioth.debian.org/
	written by Ludovic Urbain
	translated and evolved from the very crappy OO version
*/

function but($i, &$depth){
	$n = array();
	$n[2] = $i;
	if(!$depth){
		return $n;
	}
	$i2 = $i + $i;
	$depth--;
	$n[0] = but($i2-1,$depth);
	$n[1] = but($i2,$depth);
	$depth++;
	return $n;
}

function check(&$n) {
	$r=$n[2];
	if($n[0][0] === null){
		$r+=check($n[0]);
	}else{
		$r+=$n[0][2];
	}
	if($n[1][0] === null){
		$r-=check($n[1]);
	}else{
		$r-=$n[1][2];
	}
	return $r;
}

$min_depth = 4;

$n = ($argc == 2) ? $argv[1] : 1;
$max_depth = max($min_depth + 2, $n);
$stretch_depth=$max_depth + 1;
printf("stretch tree of depth %d\t check: %d\n", $stretch_depth, check(but(0, $stretch_depth)));
$iterations = 1 << ($max_depth);
while($min_depth <= $max_depth){
	$check = 0;
	for($i = 0; $i < $iterations; $i++){
		$check += check(but($i, $min_depth));
		$check += check(but(-$i, $min_depth));
	}
	printf("%d\t trees of depth %d\t check: %d\n", $iterations<<1, $min_depth, $check);
	$min_depth += 2;
	$iterations >>= 2;
}


printf("long lived tree of depth %d\t check: %d\n",
$max_depth, check(but(0, $max_depth)));