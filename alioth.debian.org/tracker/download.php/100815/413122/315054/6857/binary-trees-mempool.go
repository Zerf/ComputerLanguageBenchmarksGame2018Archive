// The Computer Language Benchmarks Game
// http://benchmarksgame.alioth.debian.org/
//
// Contributed by Alex A Skinner
// Based on the C program by Jeremy Zerfas
// Based on the C++ program from Jon Harrop, Alex Mizrahi, and Bruno Coutinho.

package main

import (
	"fmt"
	"github.com/axaxs/mempools"
	"os"
	"runtime"
	"strconv"
	"sync"
)

var newNodePointer = func() mempools.Pointer { return mempools.Pointer(&node{}) }

type node struct {
	value       int
	left, right *node
}

func (n *node) compute() int {
	if n.left != nil {
		return n.left.compute() - n.right.compute() + n.value
	}
	return n.value
}

func createTree(value int, depth int, mp *mempools.PointerPool) *node {
	n := (*node)(mp.Alloc())
	if depth > 0 {
		n.left = createTree(2*value-1, depth-1, mp)
		n.right = createTree(2*value, depth-1, mp)
	} else {
		n.left, n.right = nil, nil
	}
	n.value = value
	return n
}

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("%s <depth>\n", os.Args[0])
		return
	}
	numprox := runtime.NumCPU() * 2
	runtime.GOMAXPROCS(numprox)
	minDepth := 4
	treeDepth, _ := strconv.Atoi(os.Args[1])
	if treeDepth < minDepth+2 {
		treeDepth = minDepth + 2
	}
	st := &mempools.PointerPool{New: newNodePointer}
	stretchTree := createTree(0, treeDepth+1, st)
	fmt.Printf("stretch tree of depth %d\t check: %d\n", treeDepth+1, stretchTree.compute())
	st.Reset()
	longLived := createTree(0, treeDepth, st)
	toPrint := make([]string, treeDepth+1)
	var wg sync.WaitGroup
	for current := minDepth; current <= treeDepth; current += 2 {
		wg.Add(1)
		go func(depth int) {
			total := 0
			iterations := 1 << uint(treeDepth-depth+minDepth)
			st2 := &mempools.PointerPool{New: newNodePointer}
			for i := 1; i <= iterations; i++ {
				total += createTree(i, depth, st2).compute()
				st2.Reset()
				total += createTree(-i, depth, st2).compute()
				st2.Reset()
			}
			toPrint[depth] = fmt.Sprintf("%d\t trees of depth %d\t check: %d\n", 2*iterations, depth, total)
			wg.Done()
		}(current)
	}
	wg.Wait()
	for current := minDepth; current <= treeDepth; current += 2 {
		fmt.Print(toPrint[current])
	}
	fmt.Printf("long lived tree of depth %d\t check: %d\n", treeDepth, longLived.compute())
}
