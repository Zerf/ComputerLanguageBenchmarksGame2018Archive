# The Computer Language Benchmarks Game
# http://benchmarksgame.alioth.debian.org
#
# Contributed by Aaron Tavistock

require 'thread'

class ThreadPool

  def initialize
    @work = Queue.new
    @pool = Array.new(number_of_workers) do |i|
      Thread.new do
        Thread.current[:id] = i
        catch(:exit) do
          while(true) do
            work, args = @work.pop
            work.call(*args)
          end
        end
      end
    end
  end

  def schedule(*args, &block)
    @work << [block, args]
  end

  def shutdown
    @pool.size.times do
      schedule { throw :exit }
    end
    @pool.each do |t|
      t.join
    end
  end

  def number_of_workers
    cpu_count = if File.readable?('/proc/cpuinfo') # Linux
      %x(/proc/cpuinfo | grep -c processor).chomp.to_i
    elsif File.executable?('/usr/sbin/sysctl')  #OS/X
      %x(/usr/sbin/sysctl -n hw.ncpu).chomp.to_i
    else
      1
    end
    [(cpu_count * 2.0).to_i, 2].max
  end

end

class Mandel

  def self.render(size)
    m = Mandel.new(size)
    m.process
    print m.results
  end

  def initialize(size)
    @size = size.to_i
    two_over_size = 2.0 / @size.to_f

    @y_data = Array.new(@size) do |y|
      (two_over_size * y.to_f) - 1.0
    end

    @x_data_groups = Array.new(@size) do |x|
      (two_over_size * x.to_f) - 1.5
    end.each_slice(8).to_a

    @output = Array.new(@size)
  end

  def process
    pool = ThreadPool.new  # Using a thread pool to orchestrate forked children
    @y_data.each_with_index do |y_data, idx|
      pool.schedule do
        @output[idx] = fork_worker do
          render_row(y_data)
        end
      end
    end
    pool.shutdown
  end

  def fork_worker
    read, write = IO.pipe
    Process.fork do
      read.close
      write.print( yield )
    end
    Process.wait
    write.close
    read.read
  end

  def results
    "P4\n#{@size} #{@size}\n#{@output.join}"
  end

  private

  def render_row(y_data)
    @x_data_groups.map do |x_data_group|
      render_byte(y_data, x_data_group)
    end.join
  end

  def render_byte(y_data, x_data_group)
    byte_acc = 0
    x_data_group.each do |x_data|
      bit_or = get_bit_or(x_data, y_data)
      byte_acc = (byte_acc << 1) | bit_or
    end
    byte_acc <<= (8 - x_data_group.size)
    byte_acc.chr
  end

  def get_bit_or(cr, ci)
    zrzr = 0.0
    zizi = 0.0
    zrzi = 0.0

    count = 50
    while count > 0

      zr = zrzr - zizi + cr
      zi = 2.0 * zrzi + ci

      zrzr = zr * zr
      zizi = zi * zi
      zrzi = zr * zi

      return 0b0 if zrzr + zizi > 4.0

      count -= 1
    end

    0b1
  end

end

Mandel.render(ARGV.shift)
