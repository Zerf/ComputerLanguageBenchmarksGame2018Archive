# The Computer Language Benchmarks Game
# http://benchmarksgame.alioth.debian.org
#
# contributed by Aaron Tavistock

def frequency(keys)
  counts = Hash.new
  keys.each do |key|
    last_index = 0
    count = 0
    while last_index = @seq.index(key, last_index+1)
      count += 1
    end
    counts[key] = count
  end
  counts
end

def percentage(keys)
  render_percentage(frequency(keys))
end

def count(keys)
  frequency(keys).map do |key, value|
    "#{value.to_s}\t#{key.upcase}"
  end
end

def singles_percentage(keys)
  counts = Hash.new
  keys.each do |key|
    counts[key] = @seq.count(key)
  end
  render_percentage(counts)
end

def render_percentage(list)
  list.sort_by { |a| a[1] }.reverse.map do |key, value|
    "%s %.3f" % [ key.upcase, ( (value*100).to_f / @seq.size) ]
  end
end

_, @seq = STDIN.read.scan(/(\n>THREE[^\n]*\n)([^>]*)\n/).flatten
@seq.gsub!(/\s/, '')

singles = %w(a t c g)
doubles = %w(aa at ac ag ta tt tc tg ca ct cc cg ga gt gc gg)
chains  = %w(ggt ggta ggtatt ggtattttaatt ggtattttaatttatagt)

print "#{singles_percentage(singles).join("\n")}\n\n"
print "#{percentage(doubles).join("\n")}\n\n"
print "#{count(chains).join("\n")}\n"
