package main

import (
   "bufio"
   "flag"
   "fmt"
   "math/big"
   "os"
   "strconv"
)

const perLine = 10

var (
   tmp = new(big.Int)
   tmp2 = new(big.Int)
   acc = big.NewInt(0)
   den = big.NewInt(1)
   num = big.NewInt(1)
   ten = big.NewInt(10)
   three = big.NewInt(3)
)

func nextTerm(k uint64) {
   tmp.Lsh(num, 1)
   acc.Add(acc, tmp)

   tmp.SetUint64(k)
   num.Mul(num, tmp)

   tmp.SetUint64(k * 2 + 1)
   acc.Mul(acc, tmp)
   den.Mul(den, tmp)
}

func extractDigit() *big.Int {
   if num.Cmp(acc) > 0 {
      return nil
   }

   // Compute (numer * 3 + accum) / denom
   tmp.Mul(num, three)
   tmp.Add(tmp, acc)
   tmp.DivMod(tmp, den, tmp2)

   // Now, if (numer * 4 + accum) % denom...
   tmp2.Add(tmp2, num)

   // ... is normalized, then the two divisions have the same result.
   if tmp2.Cmp(den) >= 0 {
      return nil
   }
   return tmp
}

func eliminateDigit(d uint64) {
   tmp.SetUint64(d)
   tmp.Mul(tmp, den)
   acc.Sub(acc, tmp)
   acc.Mul(acc, ten)
   num.Mul(num, ten)
}

func main() {
   flag.Parse()
   arg := flag.Arg(0)
   n, _ := strconv.ParseUint(arg, 10, 64)

   w := bufio.NewWriterSize(os.Stdout, 1700)
   defer w.Flush()
   var line [perLine]byte

   var i, k uint64
   for i < n {
      k++
      nextTerm(k)

      x := extractDigit()
      if x == nil {
         continue
      }
      d := x.Uint64()
      eliminateDigit(d)

      line[i%perLine] = byte('0' + d)
      i++
      if i % perLine == 0 {
         w.Write(line[:])
         fmt.Fprintf(w, "\t:%d\n", i)
      }
   }

   if r := int(n) % perLine; r != 0 {
      for i := range line {
         if i >= r {
            line[i] = ' '
         }
      }
      w.Write(line[:])
      fmt.Fprintf(w, "\t:%d\n", i)
   }
}
