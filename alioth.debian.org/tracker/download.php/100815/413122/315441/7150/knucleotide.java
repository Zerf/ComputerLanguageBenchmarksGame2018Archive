/* The Computer Language Benchmarks Game
 http://benchmarksgame.alioth.debian.org/
 
 contributed by James McIlree
 modified by Tagir Valeev
 */

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class knucleotide {
    static final byte[] codes = { -1, 0, -1, 1, 3, -1, -1, 2 };
    static final char[] nucleotides = { 'A', 'C', 'G', 'T' };
    
    static ArrayList<Callable<LongToIntHashMap>> createFragmentTasks(
            final byte[] sequence, int[] fragmentLengths) {
        ArrayList<Callable<LongToIntHashMap>> tasks = new ArrayList<>();
        for (int fragmentLength : fragmentLengths) {
            for (int index = 0; index < fragmentLength; index++) {
                final int offset = index;
                tasks.add(() -> createFragmentMap(sequence, offset, fragmentLength));
            }
        }
        return tasks;
    }

    static LongToIntHashMap createFragmentMap(byte[] sequence, int offset,
            int fragmentLength) {
        LongToIntHashMap map = new LongToIntHashMap(fragmentLength);
        int lastIndex = sequence.length - fragmentLength + 1;
        for (int index = offset; index < lastIndex; index += fragmentLength) {
            map.add(getKey(sequence, index, fragmentLength), 1);
        }

        return map;
    }

    static LongToIntHashMap sumTwoMaps(LongToIntHashMap map1,
            LongToIntHashMap map2) {
        map2.forEach((key, value) -> map1.add(key, value));
        return map1;
    }
    
    static String writeFrequencies(float totalCount, LongToIntHashMap frequencies) {
        List<Entry<String, Integer>> freq = new ArrayList<>(frequencies.size());
        frequencies.forEach((key, cnt) -> {
            freq.add(new AbstractMap.SimpleEntry<>(keyToString(key, frequencies.keySize), cnt));
        });
        freq.sort(Map.Entry.comparingByValue(Comparator.reverseOrder()));
        StringBuilder sb = new StringBuilder();
        for (Entry<String, Integer> entry : freq) {
            sb.append(String.format(Locale.ENGLISH, "%s %.3f\n", entry.getKey().toUpperCase(),
                    entry.getValue() * 100.0f / totalCount));
        }
        return sb.append('\n').toString();
    }

    static String writeCount(List<Future<LongToIntHashMap>> futures,
            String nucleotideFragment) throws Exception {
        byte[] key = nucleotideFragment.getBytes(StandardCharsets.ISO_8859_1);
        toCodes(key);
        long k = getKey(key, 0, nucleotideFragment.length());
        int count = 0;
        for (Future<LongToIntHashMap> future : futures) {
            LongToIntHashMap f = future.get();
            if(f.keySize == nucleotideFragment.length()) {
                count += f.get(k);
            }
        }

        return count + "\t" + nucleotideFragment.toUpperCase() + '\n';
    }

    /**
     * Convert long key to the nucleotides string
     */
    static String keyToString(long key, int length) {
        char[] res = new char[length];
        for(int i=0; i<length; i++) {
            res[length-i-1] = nucleotides[(int)(key & 0x3)];
            key >>= 2;
        }
        return new String(res);
    }

    /**
     * Get the long key for given byte array of codes
     * at given offset and length (length must be less than 32)
     */
    static long getKey(byte[] arr, int offset, int length) {
        long key = 0;
        for(int i=offset; i<offset+length; i++) {
            key = key * 4 + arr[i];
        }
        return key;
    }

    /**
     * Convert given byte array containing acgtACGT to codes
     * (0 = A, 1 = C, 2 = G, 3 = T)
     */
    static void toCodes(byte[] sequence) {
        for(int i=0; i<sequence.length; i++) {
            sequence[i] = codes[sequence[i] & 0x7];
        }
    }

    public static void main(String[] args) throws Exception {
        String line;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while ((line = in.readLine()) != null) {
            if (line.startsWith(">THREE"))
                break;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte bytes[] = new byte[100];
        while ((line = in.readLine()) != null) {
            if (line.length() > bytes.length)
                bytes = new byte[line.length()];

            int i;
            for (i = 0; i < line.length(); i++)
                bytes[i] = (byte) line.charAt(i);
            baos.write(bytes, 0, i);
        }

        byte[] sequence = baos.toByteArray();
        toCodes(sequence);

        ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime()
                .availableProcessors());
        int[] fragmentLengths = { 1, 2, 3, 4, 6, 12, 18 };
        List<Future<LongToIntHashMap>> futures = pool.invokeAll(createFragmentTasks(
                sequence, fragmentLengths));
        pool.shutdown();

        StringBuilder sb = new StringBuilder();

        sb.append(writeFrequencies(sequence.length, futures.get(0).get()));
        sb.append(writeFrequencies(sequence.length - 1,
                sumTwoMaps(futures.get(1).get(), futures.get(2).get())));

        String[] nucleotideFragments = { "ggt", "ggta", "ggtatt", "ggtattttaatt",
                "ggtattttaatttatagt" };
        for (String nucleotideFragment : nucleotideFragments) {
            sb.append(writeCount(futures, nucleotideFragment));
        }

        System.out.print(sb.toString());
    }

    /**
     * Specialized hash map with long keys and int values
     * Value = 0 is considered as absent value 
     */
    public static class LongToIntHashMap {
        // Default size: 16
        private static final int DEFAULT_BITS = 4;

        private long[] keys;
        private int[] values;
        int size;
        int bits;
        int keySize;

        /**
         * @param keySize some additional int value 
         * (stored in the field and ignored by map)
         */
        public LongToIntHashMap(int keySize) {
            this.keySize = keySize;
            this.bits = DEFAULT_BITS;
            this.keys = new long[1 << DEFAULT_BITS];
            this.values = new int[1 << DEFAULT_BITS];
        }

        private static int hash(long key) {
            return (int) (key ^ (key >>> 7));
        }

        private boolean equals(int bucket, long key) {
            return keys[bucket] == key;
        }
        
        public int size() {
            return size;
        }

        public int get(long key) {
            int mask = (1 << bits) - 1;
            int hash = hash(key);
            int bucket = hash & mask;
            while (true) {
                int value = values[bucket];
                if (value == 0)
                    return 0;
                if (equals(bucket, key))
                    return value;
                bucket = (bucket + 1) & mask;
            }
        }

        public int add(long key, int value) {
            int mask = (1 << bits) - 1;
            int hash = hash(key);
            int bucket = hash & mask;
            while (true) {
                if (values[bucket] == 0) {
                    keys[bucket] = key;
                    values[bucket] = value;
                    if (++size * 4 > (3 << bits))
                        rehash();
                    return value;
                }
                if (equals(bucket, key))
                    return values[bucket] += value;
                bucket = (bucket + 1) & mask;
            }
        }

        public void forEach(LongIntConsumer consumer) {
            int buckets = 1 << bits;
            for (int bucket = 0; bucket < buckets; bucket++) {
                int value = values[bucket];
                if (value != 0) {
                    consumer.accept(keys[bucket], value);
                }
            }
        }

        private void rehash() {
            long[] oldKeys = keys;
            int[] oldValues = values;
            int oldBuckets = 1 << bits;
            bits++;
            this.keys = new long[1 << bits];
            this.values = new int[1 << bits];
            int mask = (1 << bits) - 1;
            for (int oldBucket = 0; oldBucket < oldBuckets; oldBucket++) {
                int value = oldValues[oldBucket];
                if (value != 0) {
                    long key = oldKeys[oldBucket];
                    int hash = hash(key);
                    int bucket = hash & mask;
                    while (true) {
                        if (values[bucket] == 0) {
                            keys[bucket] = key;
                            values[bucket] = value;
                            break;
                        }
                        bucket = (bucket + 1) & mask;
                    }
                }
            }
        }
    }

    @FunctionalInterface
    interface LongIntConsumer {
        void accept(long l, int i);
    }
}