# The Computer Language Benchmarks Game
# http://benchmarksgame.alioth.debian.org
#
# contributed by Aaron Tavistock

def forking_counter(key)
  reader, writer = IO.pipe

  pid = Process.fork do
    begin
      reader.close
      results = count_string_occurances(key)
      Marshal.dump(results, writer) if results
    ensure
      writer.close
    end
  end

  writer.close
  begin
    results = Marshal.load(reader)
  ensure
    reader.close
  end
  Process.waitpid(pid)

  results
end

def frequency(keys)
  threads = []
  counts = {}
  keys.each do |key|
    threads << Thread.new do
      if RUBY_PLATFORM == 'java'
        counts[key] = count_string_occurances(key)
      else
        counts[key] = forking_counter(key)
      end
    end
  end
  threads.each(&:join)
  counts
end

def count_string_occurances(key)
  if key.size == 1
    @seq.count(key)
  else
    last_index = 0
    count = 0
    while last_index = @seq.index(key, last_index+1)
      count += 1
    end
    count
  end
end

def percentage(keys)
  render_percentage(frequency(keys))
end

def count(keys)
  frequency(keys).sort_by { |a| a[0].size }.map do |key, value|
    "#{value.to_s}\t#{key.upcase}"
  end
end

def render_percentage(list)
  seq_size = @seq.size.to_f
  list.sort_by { |a| -a[1] }.map do |key, value|
    "%s %.3f" % [ key.upcase, ( (value*100).to_f / seq_size) ]
  end
end

_, @seq = STDIN.read.scan(/(\n>THREE[^\n]*\n)([^>]*)\n/).flatten
@seq.gsub!(/\s/, '')

singles = %w(a t c g)
doubles = %w(aa at ac ag ta tt tc tg ca ct cc cg ga gt gc gg)
chains  = %w(ggt ggta ggtatt ggtattttaatt ggtattttaatttatagt)

print "#{percentage(singles).join("\n")}\n\n"
print "#{percentage(doubles).join("\n")}\n\n"
print "#{count(chains).join("\n")}\n"
