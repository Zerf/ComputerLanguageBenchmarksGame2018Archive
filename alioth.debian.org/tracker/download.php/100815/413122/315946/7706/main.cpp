// The Computer Language Benchmarks Game
// http://benchmarksgame.alioth.debian.org/
//
// Contributed by Sylvester Saguban
// taken some inspirations from C++ G++ #3 from Branimir Maksimovic
//
// * This implementation utilizes C++ facilities for meta-programming
//   to generate highly optimized code.
// * You can choose from 3 hash table implementations so you
//   can compare them to each other.

#include <iostream>
#include <iomanip>
#include <cstdint>
#include <string>
#include <algorithm>
#include <map>
#include <future>
#include <type_traits>
#include <cstring>
#include <vector>
#include <cassert>

// You can set the hash table implementation below so they can be compared
// to each other.

// std::unordered_map is general purpose although slower implementation.
//#define USE_STD_HTABLE

// __gnu_pbgs::cc_hash_table is "a collision-chaining hash-based
// associative container", it is a GCC extension, so is not
// portable, it is however relatively faster than std::unordered_map.
#define USE_GNU_CC_HTABLE

// __gnu_pbgs::gp_hash_table is "a general-probing hash-based
// associative container", it is another GCC extension. It is
// again faster than std::unordered_map but is a bit slower than
// __gnu_pbgs::cc_hash_table.
//#define USE_GNU_GP_HTABLE

#if defined(USE_STD_HTABLE)
    #include <unordered_map>
    #define HASH_TABLE std::unordered_map
#elif defined(USE_GNU_CC_HTABLE)
    #include <ext/pb_ds/assoc_container.hpp>
    #define HASH_TABLE __gnu_pbds::cc_hash_table
#elif defined(USE_GNU_GP_HTABLE)
    #include <ext/pb_ds/assoc_container.hpp>
    #define HASH_TABLE __gnu_pbds::gp_hash_table
#endif

struct Cfg {
    static constexpr unsigned thread_count = 4;
    static constexpr unsigned to_char[4] = {'A', 'C', 'T', 'G'};
    static inline unsigned char to_num[128];
    using Data = std::vector<unsigned char>;

    Cfg() {
        to_num['A'] = to_num['a'] = 0;
        to_num['C'] = to_num['c'] = 1;
        to_num['T'] = to_num['t'] = 2;
        to_num['G'] = to_num['g'] = 3;
    }
} const cfg;

template <size_t size>
struct Key
{
    using Data = typename std::conditional<size<=16, uint32_t, uint64_t>::type;
    struct Hash {
        Data operator()(const Key& t)const{ return t._data; }
    };

    Key(Data data) : _data(data){
    }

    Key(const std::string& str) {
        _data = 0;
        for(unsigned char i : str)
            (_data <<= 2) |= cfg.to_num[i];
    }

    template<size_t... index>
    void UpdateKey(auto data, std::index_sequence<index...>){
        (((_data <<= 2) |= data[index]), ...);
        _data &= _mask;
    }

    operator std::string() const {
        std::string tmp;
        Data data = _data;
        for(size_t i = 0; i != size; ++i, data >>= 2)
            tmp += cfg.to_char[data & 3ull];
        std::reverse(tmp.begin(), tmp.end());
        return std::move(tmp);
    }

    bool operator== (const Key& in) const {
        return _data == in._data;
    }
private:
    static constexpr Data _mask = ~(Data(-1) << (2 * size));
    Data _data;
};

template <size_t size, typename K = Key<size> >
using HashTable = HASH_TABLE<K, unsigned, typename K::Hash>;

template <size_t size>
void Calculate(const Cfg::Data& input, size_t begin, HashTable<size>& table)
{
    Key<size> key(0);
    key.UpdateKey(input.data() + begin, std::make_index_sequence<size>{});
    ++table[key];

    auto itr_begin = input.data() + begin + cfg.thread_count;
    auto itr_end = input.data() + input.size() + 1 - size;
    for(;itr_begin < itr_end; itr_begin += cfg.thread_count) {
        key.UpdateKey(itr_begin, std::make_index_sequence<cfg.thread_count>{});
        ++table[key];
    }
}

template <size_t size>
auto CalculateInThreads(const Cfg::Data& input)
{
    using HT = HashTable<size>;
    HT hash_tables[cfg.thread_count];
    std::thread threads[cfg.thread_count];

    auto invoke = [&](unsigned begin) {
        Calculate<size>(input, begin, hash_tables[begin]);
    };

    for(unsigned i = 0; i < cfg.thread_count; ++i)
        threads[i] = std::thread(invoke, i);

    for(auto& i : threads)
        i.join();

    auto& frequencies = hash_tables[0];
    for(unsigned i = 1 ; i < cfg.thread_count; ++i)
        for(auto& j : hash_tables[i])
            frequencies[j.first] += j.second;
    return std::move(frequencies);
}

template <unsigned size>
void WriteFrequencies(const Cfg::Data& input)
{
    auto&& frequencies = CalculateInThreads<size>(input);
    std::map<unsigned, std::string, std::greater<unsigned>> freq;
    for(const auto& i: frequencies)
        freq.insert({i.second, i.first});

    const unsigned sum = input.size() + 1 - size;
    for(const auto& i : freq)
        std::cout << i.second << ' ' << (sum ? double(100 * i.first) / sum : 0.0) << '\n';
    std::cout << '\n';
}

template <unsigned size>
void WriteCount( const Cfg::Data& input, const std::string& text ) {
    assert(size == text.size());
    auto&& frequencies = CalculateInThreads<size>(input);
    std::cout << frequencies[Key<size>(text)] << '\t' << text << '\n';
}

int main()
{
    Cfg::Data data;
    std::array<char, 256> buf;
    while(fgets(buf.data(), buf.size(), stdin) && memcmp(">THREE", buf.data(), 6));
    while(fgets(buf.data(), buf.size(), stdin) && buf.front() != '>') {
        if(buf.front() != ';'){
            auto i = std::find(buf.begin(), buf.end(), '\n');
            data.insert(data.end(), buf.begin(), i);
        }
    }
    std::transform(data.begin(), data.end(), data.begin(), [](auto c){
        return cfg.to_num[c];
    });

    std::cout << std::setprecision(3) << std::setiosflags(std::ios::fixed);

    WriteFrequencies<1>(data);
    WriteFrequencies<2>(data);
    WriteCount<3>(data, "GGT");
    WriteCount<4>(data, "GGTA");
    WriteCount<6>(data, "GGTATT");
    WriteCount<12>(data, "GGTATTTTAATT");
    WriteCount<18>(data, "GGTATTTTAATTTATAGT");
}
