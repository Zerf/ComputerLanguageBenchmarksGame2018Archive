# The Computer Language Benchmarks Game
# http://benchmarksgame.alioth.debian.org

# contributed by Aaron Tavistock

# Leverage GMP like all the other languages 
require 'gmp'

N = (ARGV[0] || 100).to_i

# Constants to reduce object instantiation and casting
ZERO = GMP::Z(0)
ONE = GMP::Z(1)
TWO = GMP::Z(2)
THREE = GMP::Z(3)
TEN = GMP::Z(10)

# Allocate these expensive objects once
@display_chunk = GMP::Z(0)
@k = GMP::Z(0)
@a = GMP::Z(0)
@t = GMP::Z(0)
@u = GMP::Z(0)
@k1 = GMP::Z(1)
@n = GMP::Z(1)
@d = GMP::Z(1)
@tmp = GMP::Z(0)

def next_chunk
  GMP::Z.mul(@tmp, @d, @t)
  @a.sub!(@tmp)
  GMP::Z.mul(@a, @a, TEN)
  GMP::Z.mul(@n, @n, TEN)
end

def produce_chunk
  @k.add!(ONE)
  GMP::Z.mul(@t, @n, TWO)
  GMP::Z.mul(@n, @n, @k)

  @a.add!(@t)
  @k1.add!(TWO)
  GMP::Z.mul(@a, @a, @k1)
  GMP::Z.mul(@d, @d, @k1)
  
  if @a >= @n
    GMP::Z.mul(@tmp, @n, THREE)
    @tmp.add!(@a)
    @t = @tmp.fdiv(@d)
    @u = @tmp.fmod(@d)
    @u.add!(@n)
    if @d > @u
      GMP::Z.mul(@display_chunk, @display_chunk, TEN)
      @display_chunk.add!(@t)
      return true
    end
  end
  
  false
end  

def reset_display_chunk
  GMP::Z.mul(@display_chunk, ZERO, ZERO)
end

count = 0
while(count < N) do
  if produce_chunk
    count += 1
    if count % 10 == 0
      STDOUT.write "%010d\t:%d\n" % [@display_chunk.to_i, count]
      reset_display_chunk
    end 
    next_chunk
  end
end

if @display_chunk.to_i > 0
  STDOUT.write "%s\t:%d\n" % [@display_chunk.to_s.ljust(10), count]
end
