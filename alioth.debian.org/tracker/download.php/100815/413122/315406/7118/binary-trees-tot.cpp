// The Computer Language Benchmarks Game
// http://benchmarksgame.alioth.debian.org/
//
// contributed by Teemu Torma

#include <iostream>
#include <sstream>
#include <memory>
#include <utility>
#include <cassert>
#include <vector>
#include <thread>

// Memory pool of small items, allocated in chunks and released back at the
// destruction. 
template<typename T>
struct pool
{
  // Maximum chunk size.
  constexpr static int max_chunk = (1 << 26) / sizeof (T) - 8;

  pool () noexcept
    : chunk (max_chunk)
  {};
  pool (int chunk) noexcept
    : chunk (chunk < max_chunk ? chunk : max_chunk)
  {};

  ~pool () noexcept
  {
    for (slot *s = blocks; s; )
      {
	slot *next = s->next;
	::operator delete (static_cast<void *> (s));
	s = next;
      }
  }

  T *allocate ()
  {
    if (next && next < slot_of (blocks, chunk))
      return reinterpret_cast<T *> (next ++);
    else
      return allocate_more ();
  }

private:
  // Slot values.
  union slot
  {
    T item;
    slot *next;
  };

  // How many to allocate in one go.
  std::size_t chunk;

  // All allocated blocks.
  slot *blocks = 0;
  // Next available in the blocks.  Initialize to the condition that it is
  // not valid.
  slot *next = 0;

  // The header at the beginning of the block to hold pointers to blocks.
  constexpr static std::ptrdiff_t header
    = sizeof (slot *) > alignof (slot) ? sizeof (slot *) : alignof (slot);

  // Allocate more blocks.
  T *allocate_more ()
  {
    // Allocate a new blob and add to the current blocks.
    // We assume that operator new gives address aligned for a pointer.
    slot *block = reinterpret_cast<slot *>
      (::operator new (header + sizeof (slot) * chunk));

    block->next = blocks;
    blocks = block;
    next = slot_of (block, 1);
    return reinterpret_cast<T *> (slot_of (block, 0));
  }

  // Calculate the last slot value.
  constexpr static slot *
  slot_of (void *mem, std::size_t n) noexcept
  { return reinterpret_cast<slot *> (reinterpret_cast<std::ptrdiff_t> (mem)
				     + header) + n; }
};

// Binary tree node.
struct node
{
  node (int value, int depth, pool<node> &mem)
    : value (value)
  {
    if (depth)
      {
	left = new (mem.allocate ()) node (2 * value - 1, depth - 1, mem);
        right = new (mem.allocate ()) node (2 * value, depth - 1, mem);
      }
  } 

  int checksum () const
  {
    return ((left && right)
	    ? value + left->checksum () - right->checksum ()
	    : value);
  }

  int value = 0;
  node *left = 0;
  node *right = 0;
};

// Root of the tree, holding the memory pool.
struct tree : pool<node>, node
{
  tree (int value, int depth)
    : pool<node> (1 << depth), node (value, depth, *this)
  {}
};

int main (int argc, const char *argv[])
{
  int min_depth = 4;
  int max_depth = std::max (min_depth + 2,
			    (argc >= 2 ? atoi (argv[1]) : 10));
  int stretch_depth = max_depth + 1;

  // Alloc and dealloc stretchdepth tree
  {
    tree c (0, stretch_depth);
    std::cout << "stretch tree of depth " << stretch_depth << "\t "
	      << "checksum: " << c.checksum () << "\n";
  }

  // Long living tree.
  tree t (0, max_depth);

  // For messages from threads.
  std::vector<std::string> outputs (max_depth + 1, std::string {});

  // Worker threads, one per depth.
  std::vector<std::thread> workers;

  auto worker = [&] (int d)
    {
      int iterations = 1 << (max_depth - d + min_depth);
      int c = 0;

      for (int i = 1; i <= iterations; ++i)
	{
	  tree a (i, d);
	  tree b (- i, d);
	  c += a.checksum () + b.checksum ();
	}

      std::ostringstream msg;
      msg << 2 * iterations << "\t trees of depth " << d
          << "\t checksum: " << c << "\n";

      outputs[d] = msg.str ();
    };

  for (int d = min_depth; d <= max_depth; d += 2)
    workers.emplace_back (std::thread (worker, d));

  for (auto &w : workers) { w.join (); }
  for (auto &d : outputs) { if (d.size ()) std::cout << d; }

  std::cout << "long lived tree of depth " << max_depth << "\t "
	    << "checksum: " << t.checksum () << "\n";

  return 0;
}
