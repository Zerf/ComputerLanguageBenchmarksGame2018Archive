#include <cassert>
#include <future>
#include <iostream>
#include <memory>
#include <vector>

using namespace std;

class Tree;

class Node {
    /*
     * This is a node of binary tree, it has a value and two pointers to his
     * childrens.
     *
     * The node's lifetime is managed by a Tree, you cannot neither create nor
     * destruct a Node.
     */
    public:
        int checksum() const {
            if (left) {
                return left->checksum() + value - right->checksum();
            }
            return value;
        }

    private:
        friend class Tree;

        Node(int v) : Node{v, nullptr, nullptr} {}
        Node(int v, Node* l, Node* r) : value{v}, left{l}, right{r} {}
        int value;

        /*
         * Nacked pointers are ok here, this class should only use them, not
         * manage them.
         *
         * These are nullptr if the node is a leaf.
         */
        Node* left;
        Node* right;
};

class Tree {
    /*
     * This is a binary tree of a fixed depth.
     *
     * The raison d'etre of this class is to minimize the memory allocations
     * and, given that, the size of the tree is known at the construction time
     * it's possible to accomplish the goal  with just one allocation.
     *
     * The recipe ingredients are:
     *
     *  1. A buffer big enough to hold all the nodes
     *      -> this->buffer
     *  2. The placement new to construct the nodes inside the buffer
     *  3. The lifetime of a node is managed by its tree
     *      -> Node private constructor
     *
     */
    public:
        /*
         * node_buffer is a POD big enought to hold a Node with the correct
         * align
         */
        using node_buffer = aligned_storage<sizeof(Node), alignof(Node)>::type;

    public:
        Tree(int start, uint8_t depth)
            : nodes{Tree::elements(depth)},
              owner{Tree::allocate(depth)},
              buffer{owner.get()} {
            build_nodes(start, depth);
        }

        Tree(int start, uint8_t depth, node_buffer* buffer)
            : nodes{Tree::elements(depth)},
              owner{nullptr},
              buffer{buffer} {
            build_nodes(start, depth);
        }

        ~Tree() noexcept {
            for (size_t ix=0; ix<nodes; ix++) {
                auto node = this->node(ix);
                node->~Node();
            }
        }

        Node* root() const {
            return this->node(0);
        }

        Node* node(size_t index) const {
            return reinterpret_cast<Node*>(buffer + index);
        }

        static unique_ptr<node_buffer[]> allocate(uint8_t depth) {
            auto nodes = Tree::elements(depth);
            return make_unique<node_buffer[]>(nodes);
        }

    private:
        size_t nodes;
        unique_ptr<node_buffer[]> owner;
        node_buffer* buffer;

    private:
        static size_t elements(uint8_t depth) {
            size_t elements = 0;
            for (int ix=0; ix<=depth; ix++) {
                int loop = 1 << ix;
                elements += loop;
            }
            return elements;
        }

        void build_nodes(int value, uint8_t depth) {
            /*
             * The buffer is allocated, it can contains up to `nodes` elements,
             * now is the time to build them.
             *
             * given value=0 and depth=2 the resultin tree is:
             * (x:y means node with index x and value y)
             *
             *           0:0
             *           /\
             *          /  \
             *         /    \
             *        /      \
             *      1:-1      2:0
             *      /\        /\
             *     /  \      /  \
             *    /    \    /    \
             *   3:-3 4:-2 5:-1  6:0
             *
             */
            int node_index = 0;
            for (int loop=0; loop <= depth; loop++) {
                int row = 1 << loop;
                for (int row_index=0; row_index < row; row_index++) {
                    /*
                     * We can build the tree with an iterative algorithm.
                     *
                     * At every iteration `node_index` is the index of the node
                     * that is ready to be constructed.
                     *
                     * `next_left` and `next_right` are the pointers to the
                     * children of the node that will be constructed in a
                     * following iteration.
                     *
                     * `node_value` is the value of the node and and it is
                     * calculated using the value of the parent node.
                     *
                     */
                    Node* next_left = nullptr;
                    Node* next_right = nullptr;
                    if (loop < depth) {
                        next_left = this->node(node_index * 2 + 1);
                        next_right = this->node(node_index * 2 + 2);
                    }

                    int node_value;
                    if (node_index == 0) {
                        node_value = value;
                    }
                    else {
                        auto parent_index = div(node_index, 2);
                        if (parent_index.rem == 1) {
                            // this is a left node
                            Node* parent = this->node(parent_index.quot);
                            node_value = parent->value * 2 - 1;
                        }
                        else {
                            // this is a right node
                            Node* parent = this->node(parent_index.quot - 1);
                            node_value = parent->value * 2;
                        }
                    }
                    new(buffer+node_index) Node{node_value, next_left, next_right};
                    node_index++;
                }
            }
        }
};

int main(int argc, char *argv[]) {
    uint8_t const min_depth = 4;
    uint8_t max_depth = max(min_depth+2, (argc == 2 ? atoi(argv[1]) : 10));
    uint8_t stretch_depth = max_depth + 1;

    assert(stretch_depth <= 25);

    {
        Tree t{0, stretch_depth};
        cout << "stretch tree of depth " << static_cast<int>(stretch_depth) << "\t "
             << "check: " << t.root()->checksum() << "\n";
    }

    Tree long_lived{0, max_depth};

    struct stats {
        int iterations;
        int depth;
        int checksum;
    };

    vector<future<stats>> output(max_depth+1);
    auto work = [&output, &max_depth, &min_depth](uint8_t depth) -> stats {
        int iterations = 1 << (max_depth - depth + min_depth);
        int checksum = 0;
        auto buffer1 = Tree::allocate(depth);
        auto buffer2 = Tree::allocate(depth);
        for (int i=1; i<=iterations; ++i) {
            Tree a{i, depth, buffer1.get()};
            Tree b{-i, depth, buffer2.get()};
            checksum += a.root()->checksum() + b.root()->checksum();
        }
        return {2*iterations, depth, checksum};
    };

    for (uint8_t depth=min_depth; depth<=max_depth; depth+=2) {
        output[depth] = async(launch::async, work, depth);
    }

    for (uint8_t depth=min_depth; depth<=max_depth; depth+=2) {
        auto const& stat = output[depth].get();
        cout << stat.iterations << "\t trees of depth " << stat.depth << "\t "
             << "check: " << stat.checksum << "\n";
    }

    cout << "long lived tree of depth " << static_cast<int>(max_depth) << "\t "
         << "check: " << (long_lived.root()->checksum()) << "\n";
    return 0;
}
