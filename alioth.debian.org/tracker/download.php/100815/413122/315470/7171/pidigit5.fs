(* The Computer Language Benchmarks Game
 * http://benchmarksgame.alioth.debian.org/
 * Contributed by Udo Zallmann
 * Modified after the Scala version to use
 * only one bigint division instead of two
 * when checking the next digit's validity.
 *)

module Pidigit5

type lft = LFT of bigint * bigint * bigint

type stream<'a> =
   Stream of 'a * (unit -> stream<'a>)

let streamCalc (next : 'b -> 'c)
               (safe : 'b -> 'c -> bool)
               (prod : 'b -> 'c -> 'b)
               (cons : 'b -> 'a -> 'b)
               z s =
   let rec streamCalcInt
         z (Stream (x, xs) as s) =
      let y = next z
      if safe z y
      then Stream (y, fun () ->
         streamCalcInt (prod z y) s)
      else streamCalcInt (cons z x)
                         (xs ())
   streamCalcInt z s

let comp (LFT (q, r, t)) (LFT (u, v, x)) =
   LFT (q * u, q * v + r * x, t * x)

let extr (LFT (q, r, t)) =
   let y, m =
      bigint.DivRem (3I * q + r, t)
   if m + q < t then Some y else None

let safe _ n = Option.isSome n

let prod z n =
   let m = defaultArg n 0I
   comp (LFT (10I, -10I * m, 1I)) z

let rec lfts k =
   Stream (let m = 2I * k + 1I
           LFT (k, 2I * m, m),
           fun () -> lfts (k + 1I))

let printLine l t a =
   let inline chr x = char (48 + x)
   let b = Core.string (Array.map chr a)
   let s = (b.Substring (0, l)).
            PadRight a.Length
   printfn "%s\t:%d" s t

let printLinesOf n tot s =
   let r = Array.zeroCreate n
   (1, s)
   ||> Seq.fold (fun t x ->
         let i = t % n
         let u = if i = 0 then n else i
         r.[u - 1] <- x
         if t = tot then printLine u t r
         elif i = 0 then printLine n t r
         t + 1)
   |> ignore

[<EntryPoint>]
let main args =
   let n = try int args.[0] with _ -> 27
   streamCalc extr safe prod comp
              (LFT (1I, 0I, 1I)) (lfts 1I)
   |> Seq.unfold (fun (Stream (x, xs)) ->
                  Some (x, xs ()))
   |> Seq.map (Option.get >> int)
   |> Seq.take n
   |> printLinesOf 10 n
   0
