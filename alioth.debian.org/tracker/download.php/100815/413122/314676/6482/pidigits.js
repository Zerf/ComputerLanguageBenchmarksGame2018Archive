/*
  The Computer Language Benchmarks Game
  http://benchmarksgame.alioth.debian.org/
  contributed by 10iii
  refer to Tom Wu's jsbn library, BSD license.
 */
var bi = (
  function () {
    var dbits = 28;
    var BI_DB = dbits;
    var BI_DM = ((1 << dbits) - 1);
    var BI_DV = (1 << dbits);
    var BI_FP = 52;
    var BI_FV = Math.pow(2, BI_FP);
    var BI_F1 = BI_FP - dbits;
    var BI_F2 = 2 * dbits - BI_FP;
    var BI_RM = "0123456789abcdefghijklmnopqrstuvwxyz";
    var BI_RC = new Array();

    var rr, vv;
    rr = "0".charCodeAt(0);
    for (vv = 0; vv <= 9; ++vv) {
      BI_RC[rr++] = vv;
    }
    rr = "a".charCodeAt(0);
    for (vv = 10; vv < 36; ++vv) {
      BI_RC[rr++] = vv;
    }
    rr = "A".charCodeAt(0);
    for (vv = 10; vv < 36; ++vv) {
      BI_RC[rr++] = vv;
    }

    // (public) Constructor
    function BigInteger(val, maxdigits) {
      if ('number' == typeof maxdigits && maxdigits > 0) {
        this.buf = new Array(maxdigits);
      } else {
        this.buf = new Array();
      }
      this.t = 0;
      this.s = 0;
      if ('number' == typeof val) {
        this.fromInt(val);
      }
    }

    function nbi(max_digits) {
      return new BigInteger(null, max_digits);
    }

    function am3(i, x, w, j, c, n) {
      var this_buf = this.buf;
      var w_buf = w.buf;

      var xl = x & 0x3fff,
        xh = x >> 14;
      while (--n >= 0) {
        var l = this_buf[i] & 0x3fff;
        var h = this_buf[i++] >> 14;
        var m = xh * l + h * xl;
        l = xl * l + ((m & 0x3fff) << 14) + w_buf[j] + c;
        c = (l >> 28) + (m >> 14) + xh * h;
        w_buf[j++] = l & 0xfffffff;
      }
      return c;
    }

    function int2char(n) {
      return BI_RM.charAt(n);
    }

    function intAt(s, i) {
      var c = BI_RC[s.charCodeAt(i)];
      return (c == null) ? -1 : c;
    }

    // (protected) copy this to r
    function bnpCopyTo(r) {
      var this_buf = this.buf;
      var r_buf = r.buf;

      for (var i = this.t - 1; i >= 0; --i) r_buf[i] = this_buf[i];
      r.t = this.t;
      r.s = this.s;
    }

    // (protected) set from integer value x, -DV <= x < DV
    function bnpFromInt(x) {
      var this_buf = this.buf;
      this.t = 1;
      this.s = (x < 0) ? -1 : 1;
      if (x > 0) this_buf[0] = x;
      else if (x < 0) this_buf[0] = -x;
      else {
        this.t = 0;
        this.s = 0;
      }
    }
    // return bigint initialized to value
    function nbv(i) {
      var r = nbi();
      r.fromInt(i);
      return r;
    }

    // return a new BigInteger object
    // bi(int) 
    bi = function (a) {
      return new BigInteger(a, null);
    }

    // (protected) clamp off excess high words
    function bnpClamp() {
      var this_buf = this.buf;
      while (this.t > 0 && this_buf[this.t - 1] == 0)--this.t;
      if (this.t === 0) {
        this.s = 0;
      }
      this_buf.length = this.t;
    }

    // (public) -this
    function bnNegate() {
      this.s = this.s * -1;
      return this;
    }

    function bnNegateCopy() {
      var r = this.clone();
      r.s = r.s * -1;
      return r;
    }

    // (public) |this|
    function bnAbs() {
      return (this.s < 0) ? this.negateCopy() : this;
    }

    // (public) return + if |this| > |a|, - if |this| < |a|, 0 if equal
    function bnAbsCompareTo(a) {
      var this_buf = this.buf;
      var a_buf = a.buf;
      var t = this.t - 1;

      if (this.t > a.t) {
        return 1;
      } else if (a.t > this.t) {
        return -1;
      } else {
        while (t >= 0 && this_buf[t] === a_buf[t]) {
          t--;
        }
        if (t < 0) {
          return 0;
        } else {
          return this_buf[t] - a_buf[t];
        }
      }
    }


    // (public) return + if this > a, - if this < a, 0 if equal
    function bnCompareTo(a) {
      var this_buf = this.buf;
      var a_buf = a.buf;
      var r = this.s - a.s;
      if (r > 0) { //1:0,1:-1,0:-1
        return 1;
      } else if (r < 0) { //-1:0,-1:1, 0:1
        return -1;
      } else { // 1:1,0:0,-1:-1
        return this.s === 0 ? 0 : this.s * this.absCompareTo(a);
      }
    }

    // returns bit length of the integer x
    function nbits(x) {
      var r = 1,
        t;
      if ((t = x >>> 16) != 0) {
        x = t;
        r += 16;
      }
      if ((t = x >> 8) != 0) {
        x = t;
        r += 8;
      }
      if ((t = x >> 4) != 0) {
        x = t;
        r += 4;
      }
      if ((t = x >> 2) != 0) {
        x = t;
        r += 2;
      }
      if ((t = x >> 1) != 0) {
        x = t;
        r += 1;
      }
      return r;
    }

    // (public) return the number of bits in "this"
    function bnBitLength() {
      var this_buf = this.buf;
      if (this.t <= 0) return 0;
      return BI_DB * (this.t - 1) + nbits(this_buf[this.t - 1] ^ (0 & BI_DM));
    }


    // (protected) r = this << n*DB
    function bnpDLShiftTo(n, r) {
      var this_buf = this.buf;
      var r_buf = r.buf;
      var i;
      for (i = this.t - 1; i >= 0; --i) r_buf[i + n] = this_buf[i];
      for (i = n - 1; i >= 0; --i) r_buf[i] = 0;
      r.t = this.t + n;
      r.s = this.s;
    }

    // (protected) r = this >> n*DB
    function bnpDRShiftTo(n, r) {
      var this_buf = this.buf;
      var r_buf = r.buf;
      for (var i = n; i < this.t; ++i) r_buf[i - n] = this_buf[i];
      r.t = Math.max(this.t - n, 0);
      r.s = this.s;
    }

    // (protected) r = this << n
    function bnpLShiftTo(n, r) {
      var this_buf = this.buf;
      var r_buf = r.buf;
      var bs = n % BI_DB;
      var cbs = BI_DB - bs;
      var bm = (1 << cbs) - 1;
      var ds = Math.floor(n / BI_DB),
        c = (0 << bs) & BI_DM,
        i;
      for (i = this.t - 1; i >= 0; --i) {
        r_buf[i + ds + 1] = (this_buf[i] >> cbs) | c;
        c = (this_buf[i] & bm) << bs;
      }
      for (i = ds - 1; i >= 0; --i) r_buf[i] = 0;
      r_buf[ds] = c;
      r.t = this.t + ds + 1;
      r.s = this.s;
      r.clamp();
    }

    // (protected) r = this >> n
    function bnpRShiftTo(n, r) {
      var this_buf = this.buf;
      var r_buf = r.buf;
      r.s = this.s;
      var ds = Math.floor(n / BI_DB);
      if (ds >= this.t) {
        r.t = 0;
        return;
      }
      var bs = n % BI_DB;
      var cbs = BI_DB - bs;
      var bm = (1 << bs) - 1;
      r_buf[0] = this_buf[ds] >> bs;
      for (var i = ds + 1; i < this.t; ++i) {
        r_buf[i - ds - 1] |= (this_buf[i] & bm) << cbs;
        r_buf[i - ds] = this_buf[i] >> bs;
      }
      if (bs > 0) r_buf[this.t - ds - 1] |= (0 & bm) << cbs;
      r.t = this.t - ds;
      r.clamp();
    }

    function bnp_add_same_single(a, b, r) { //a.t >= b.t
      var a_buf = a.buf;
      var b_buf = b.buf;
      var r_buf = r.buf;
      r.s = a.s;
      var i = 0;
      var c = 0;
      var m = b.t;
      while (i < m) {
        c += b_buf[i] + a_buf[i];
        r_buf[i++] = c & BI_DM;
        c >>= BI_DB;
      }
      while (i < a.t) {
        c += a_buf[i];
        r_buf[i++] = c & BI_DM;
        c >>= BI_DB;
      }
      if (c > 0) r_buf[i++] = c;
      r.t = i;
      r.clamp();
      return r;
    }

    function bnp_add_diff_single(a, b, r) { // abs(a) > abs(b)
      var a_buf = a.buf;
      var b_buf = b.buf;
      var r_buf = r.buf;
      r.s = a.s;
      var i = 0;
      var c = 0;
      var m = b.t;
      while (i < m) {
        c += a_buf[i] - b_buf[i];
        r_buf[i++] = c & BI_DM;
        c >>= BI_DB;
      }
      while (i < a.t) {
        c += a_buf[i];
        r_buf[i++] = c & BI_DM;
        c >>= BI_DB;
      }
      if (c > 0) r_buf[i++] = c;
      r.t = i;
      r.clamp();
      return r;
    }

    function bnpAddTo(a, r) {
      var ts = this.s * a.s;
      if (ts === 0) {
        return (this.s === 0 ? a : this).copyTo(r);
      } else if (ts > 0) {
        return this.t >= a.t ? bnp_add_same_single(this, a, r) : bnp_add_same_single(a, this, r);
      } else { // ts < 0
        var cmp = this.absCompareTo(a);
        if (cmp === 0) {
          r.s = 0;
          r.t = 0;
          return r;
        } else if (cmp > 0) {
          return bnp_add_diff_single(this, a, r);
        } else { // cmp < 0
          return bnp_add_diff_single(a, this, r);
        }
      }
    }

    // (protected) r = this - a
    function bnpSubTo(a, r) {
      this.addTo(a.negateCopy(), r);
    }

    // (protected) r = this * a, r != this,a (HAC 14.12)
    // "this" should be the larger one if appropriate.
    function bnpMultiplyTo(a, r) {
      var ts = this.s * a.s;
      if (ts === 0) {
        r.t = 0;
        r.s = 0;
        return r;
      }
      var this_buf = this.buf;
      var r_buf = r.buf;
      var x = this,
        y = a;
      var y_buf = y.buf;

      var i = x.t;
      r.t = i + y.t;
      while (--i >= 0) r_buf[i] = 0;
      for (i = 0; i < y.t; ++i) r_buf[i + x.t] = x.am(0, y_buf[i], r, i, 0, x.t);
      r.s = ts;
      r.clamp();
    }

    // (protected) divide this by m, quotient and remainder to q, r (HAC 14.20)
    // r != q, this != m.  q or r may be null.
    function bnpDivRemTo(m, q, r) {
      if (this.s === 0) {
        q.s = 0;
        q.t = 0;
        return;
      }
      var pm = m;
      if (pm.t <= 0) return;
      var pt = this;
      if (pt.t < pm.t) {
        if (q != null) q.fromInt(0);
        if (r != null) this.copyTo(r);
        return;
      }
      if (r == null) r = nbi(this.t + 2);
      var y = nbi(this.t + 2),
        ts = this.s,
        ms = m.s;
      var pm_buf = pm.buf;
      var nsh = BI_DB - nbits(pm_buf[pm.t - 1]); // normalize modulus
      if (nsh > 0) {
        pm.lShiftTo(nsh, y);
        pt.lShiftTo(nsh, r);
      } else {
        pm.copyTo(y);
        pt.copyTo(r);
      }
      var ys = y.t;

      var y_buf = y.buf;
      var y0 = y_buf[ys - 1];
      if (y0 == 0) return;
      var yt = y0 * (1 << BI_F1) + ((ys > 1) ? y_buf[ys - 2] >> BI_F2 : 0);
      var d1 = BI_FV / yt,
        d2 = (1 << BI_F1) / yt,
        e = 1 << BI_F2;
      var i = r.t,
        j = i - ys,
        t = (q == null) ? nbi(this.t + 2) : q;
      y.dlShiftTo(j, t);

      var r_buf = r.buf;
      if (r.compareTo(t) >= 0) {
        r_buf[r.t++] = 1;
        r.subTo(t, r);
      }
      BigInteger.ONE.dlShiftTo(ys, t);
      t.subTo(y, y); // "negative" y so we can replace sub with am later
      while (y.t < ys) y_buf[y.t++] = 0;
      while (--j >= 0) {
        // Estimate quotient digit
        var qd = (r_buf[--i] == y0) ? BI_DM : Math.floor(r_buf[i] * d1 + (r_buf[i - 1] + e) * d2);
        if ((r_buf[i] += y.am(0, qd, r, j, 0, ys)) < qd) { // Try it out
          y.dlShiftTo(j, t);
          r.subTo(t, r);
          while (r_buf[i] < --qd) r.subTo(t, r);
        }
      }
      if (q != null) {
        r.drShiftTo(ys, q);
        q.s = ts * ms > 0 ? 1 : -1;
      }
      q.clamp();
    }

    // (public) return value as integer
    function bnIntValue() {
      var this_buf = this.buf;
      var r = 0;
      if (this.s === 0) return 0;
      if (this.t == 1) {
        r = this_buf[0];
      } else {
        r = ((this_buf[1] & ((1 << (32 - BI_DB)) - 1)) << BI_DB) | this_buf[0];
      }
      return this.s * r;
    }

    // (public) this << n
    function bnShiftLeft(n) {
      var r = nbi(this.t + n + 2);
      if (n < 0) this.rShiftTo(-n, r);
      else this.lShiftTo(n, r);
      return r;
    }

    // (public) return string representation in given radix
    function bnToString(b) {
      var this_buf = this.buf;
      var k;
      if (b == 16) k = 4;
      else if (b == 8) k = 3;
      else if (b == 2) k = 1;
      else if (b == 32) k = 5;
      else if (b == 4) k = 2;
      else return this.toRadix(b);
      var km = (1 << k) - 1,
        d, m = false,
        r = "",
        i = this.t;
      var p = BI_DB - (i * BI_DB) % k;
      if (i-- > 0) {
        if (p < BI_DB && (d = this_buf[i] >> p) > 0) {
          m = true;
          r = int2char(d);
        }
        while (i >= 0) {
          if (p < k) {
            d = (this_buf[i] & ((1 << p) - 1)) << (k - p);
            d |= this_buf[--i] >> (p += BI_DB - k);
          } else {
            d = (this_buf[i] >> (p -= k)) & km;
            if (p <= 0) {
              p += BI_DB;
              --i;
            }
          }
          if (d > 0) m = true;
          if (m) r += int2char(d);
        }
      }
      return m ? (this.s < 0 ? "-" + r : r) : "0";
    }


    BigInteger.prototype.am = am3;
    // protected
    BigInteger.prototype.copyTo = bnpCopyTo;
    BigInteger.prototype.fromInt = bnpFromInt;
    BigInteger.prototype.clamp = bnpClamp;
    BigInteger.prototype.dlShiftTo = bnpDLShiftTo;
    BigInteger.prototype.drShiftTo = bnpDRShiftTo;
    BigInteger.prototype.lShiftTo = bnpLShiftTo;
    BigInteger.prototype.rShiftTo = bnpRShiftTo;
    BigInteger.prototype.subTo = bnpSubTo;
    BigInteger.prototype.addTo = bnpAddTo;
    BigInteger.prototype.multiplyTo = bnpMultiplyTo;
    BigInteger.prototype.divRemTo = bnpDivRemTo;
    BigInteger.prototype.bnShiftLeft = bnShiftLeft;
    BigInteger.prototype.bnIntValue = bnIntValue;

    // public
    BigInteger.prototype.toString = bnToString;
    BigInteger.prototype.negate = bnNegate;
    BigInteger.prototype.negateCopy = bnNegateCopy;
    BigInteger.prototype.abs = bnAbs;
    BigInteger.prototype.absCompareTo = bnAbsCompareTo;
    BigInteger.prototype.compareTo = bnCompareTo;
    BigInteger.prototype.bitLength = bnBitLength;

    // "constants"
    BigInteger.ZERO = nbv(0);
    BigInteger.ONE = nbv(1);


    BigInteger.prototype.clone = function () {
      var r = nbi(this.t + 2);
      this.copyTo(r);
      return r
    };
    BigInteger.prototype.multiply = function (a) {
      var r = nbi(this.t + a.t + 2);
      this.multiplyTo(a, r);
      return r
    };
    BigInteger.prototype.divide = function (a) {
      var r = nbi(this.t + 2);
      this.divRemTo(a, r, null);
      return r
    };
    BigInteger.prototype.add = function (a) {
      var r = nbi(Math.max(this.t, a.t) + 2);
      this.addTo(a, r);
      return r
    };
    BigInteger.prototype.sub = function (a) {
      var r = nbi(Math.max(this.t, a.t) + 2);
      this.subTo(a, r);
      return r
    };
    return function (a) {
      return new BigInteger(a, null);
    };
  }
)();

(function (n) {
  var
    $three = bi(3),
    $ten = bi(10),
    $z0 = bi(1),
    $z1 = bi(0),
    $z2 = bi(1),
    $negdigits = [],
    $g,
    d, i, j, s, g, k, l,
    end_of_var;

  for (var j = 0; j < 10; j += 1) {
    $negdigits[j] = bi(-10 * j);
  }
  i = 1;
  s = '';
  g = 1;
  k = 0;
  l = 2;
  do {
    while ($z0.compareTo($z2) > 0 ||
      ((d = $z0.multiply($three).add($z1).divide($z2).bnIntValue()) !=
        $z0.bnShiftLeft(2).add($z1).divide($z2).bnIntValue())) {
      $z1 = $z1.multiply($g = bi(g += 2));
      $z2 = $z2.multiply($g);
      $z1 = $z1.add($z0.multiply(bi(l += 4)));
      $z0 = $z0.multiply(bi(k += 1));
    }
    $z0 = $z0.multiply($ten);
    $z1 = $z1.multiply($ten);
    $z1 = $z1.add($z2.multiply($negdigits[d]));
    s += d;
    if (i % 10 == 0) {
      print(s + '\t:' + i);
      s = '';
    }
  } while ((i += 1) <= n);
  if ((i = n % 10) != 0) {
    s += Array(11 - i).join(' ');
  }
  if (s.length > 0) {
    print(s + '\t:' + n);
  }
}(1 * arguments[0]));