# The Computer Language Benchmarks Game
# http://benchmarksgame.alioth.debian.org
#
# contributed by Aaron Tavistock
# forking by Christian Stone

def forked_frequency(seq, keys)
    counts=Hash.new(0)
    forks=[]
    keys.each do |key|
      counts[key]=IO.pipe
      forks<<Process.fork do
        count=0
        last_index = 0
        while last_index = seq.index(key, last_index+1)
          count+=1
        end
        counts[key][1].write count
        exit!(0)
      end
    end
    Process.waitall
    counts.each do |k,v|
      v[1].close
      counts[k]=v[0].read.to_i
      counts.delete(k) if counts[k]==0
    end
    counts
end

def percentage(seq, keys)
    forked_frequency(seq, keys).sort { |a,b| b[1] <=> a[1] }.map do |key, value|
      "%s %.3f" % [ key.upcase, ( (value*100.0) / seq.size) ]
    end
end

def count(seq, keys)
    forked_frequency(seq, keys).map do |key, value|
      "#{value}\t#{key.upcase}"
    end
end

seq = STDIN.read.scan(/\n>THREE[^\n]*\n([^>]*)\n/)[0][0]
seq.force_encoding('ASCII-8BIT')
seq.gsub!(/\s/, '')

singles =  %w(a t c g)

doubles = singles.map { |a| singles.map { |b| "#{a}#{b}" }}.flatten
chains  = %w(ggt ggta ggtatt ggtattttaatt ggtattttaatttatagt)

print "#{percentage(seq, singles).join("\n")}\n\n"
print "#{percentage(seq, doubles).join("\n")}\n\n"
print "#{count(seq, chains).join("\n")}\n"