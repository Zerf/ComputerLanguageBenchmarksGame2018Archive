﻿open System


type Node =
    | Branch of Node * Node * int
    | Leaf of int

let rec GenNode depth item =
    if depth = 0
    then Leaf(item)
    else
        let i = item * 2
        let d = depth - 1
        Branch( (GenNode d (i - 1)), (GenNode d i), item )

let inline GenTree depth = GenNode (depth - 1)

let rec GetCheckSum = function
    | Leaf i          -> i
    | Branch(l, r, i) -> i + (GetCheckSum l) - (GetCheckSum r)


[<EntryPoint>]
let main argv =

    let depth = if argv.Length > 0
                then Int32.Parse(argv.[0])
                else 20

    let min_depth = 4
    let max_depth = Math.Max(min_depth + 2, depth)

    let stretch_depth = max_depth + 1

    GenTree stretch_depth 0
    |> GetCheckSum
    |> printfn "stretch tree of depth %i\t check: %i" stretch_depth

    let long_lived_tree = GenTree max_depth 0

    for cur_depth in min_depth ..2 ..max_depth do
        let iterations = 1 <<< (max_depth - cur_depth + min_depth)
        let mutable check = 0
        for i in 1 ..iterations do
            check <- check
                     + GetCheckSum (GenTree cur_depth i)
                     + GetCheckSum (GenTree cur_depth -i)

        printfn "%i\t trees of depth %i\t check: %i" (iterations * 2) cur_depth check

    long_lived_tree
    |> GetCheckSum
    |> printfn "long lived tree of depth %i\t check: %i" max_depth
    0