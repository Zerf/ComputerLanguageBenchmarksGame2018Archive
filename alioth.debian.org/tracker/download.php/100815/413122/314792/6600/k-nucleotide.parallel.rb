# The Computer Language Benchmarks Game
# http://benchmarksgame.alioth.debian.org
#
# contributed by Aaron Tavistock
# optimised && parallelised by Scott Leggett

module MiniParallel
    class Worker
        def initialize(read, write)
            @read, @write = read, write
        end

        def close_pipes
            @read.close
            @write.close
        end

        def work(index)
            Marshal.dump(index, @write)
            Marshal.load(@read)
        end
    end

    def self.map(array, &block)
        work_in_processes(
            array,
            [array.size, core_count].min,
            &block
        )
    end

    def self.core_count
        @@core_count ||= IO.read("/proc/cpuinfo").scan(/^processor/).size
    end

    private

    def self.work_in_processes(array, count, &block)
        index = -1
        results, threads = [], []

        workers = create_workers(array, count, &block)

        workers.each do |worker|
            threads << Thread.new do
              loop do
                Thread.exclusive{ index += 1 }
                break if index >= array.size
                results[index] = worker.work(index)
              end
              worker.close_pipes
            end
        end

        threads.each(&:join)
        Process.waitall

        results
    end

    def self.create_workers(array, count, &block)
        workers = []
        count.times do
            workers << create_worker(array, workers, &block)
        end
        workers
    end

    def self.create_worker(array, started_workers, &block)
        child_read, parent_write = IO.pipe
        parent_read, child_write = IO.pipe

        Process.fork do
            started_workers.each(&:close_pipes)

            parent_write.close
            parent_read.close

            process_incoming_jobs(child_read, child_write, array, &block)

            child_read.close
            child_write.close
        end

        child_read.close
        child_write.close

        Worker.new(parent_read, parent_write)
    end

    def self.process_incoming_jobs(read, write, array, &block)
        until read.eof?
            index = Marshal.load(read)
            Marshal.dump(block.call(array[index]), write)
        end
    end
end

def frequency(seq, keys)
    MiniParallel.map(keys) do |key|
        if key.size == 1
            [seq.count(key), key.upcase]
        else
            index = count = 0
            while index = seq.index(key, index+1)
                count += 1
            end
            [count, key.upcase]
        end
    end
end

def percentage(seq, keys)
    frequency(seq, keys).sort!.reverse!.map! do |value, key|
        "%s %.3f" % [ key, ((value*100).to_f / seq.size) ]
    end
end

def count(seq, keys)
    frequency(seq, keys).map! do |value, key|
        [value, key].join("\t")
    end
end

seq = STDIN.map do |line|
    line.chomp! if line.include?('>TH') .. false
end.compact[1..-1].join

singles = ['a', 't', 'c', 'g']
doubles = ['aa', 'at', 'ac', 'ag', 'ta', 'tt', 'tc', 'tg', 'ca', 'ct', 'cc',
           'cg', 'ga', 'gt', 'gc', 'gg']
chains  = ['ggt', 'ggta', 'ggtatt', 'ggtattttaatt', 'ggtattttaatttatagt']

puts percentage(seq, singles).join("\n"), "\n"
puts percentage(seq, doubles).join("\n"), "\n"
puts count(seq, chains).join("\n")
