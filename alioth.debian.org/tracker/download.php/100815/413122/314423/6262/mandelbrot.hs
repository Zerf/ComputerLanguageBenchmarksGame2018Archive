{-
  The Computer Language Benchmarks Game
  http://benchmarksgame.alioth.debian.org/
  
  contributed by Branimir Maksimovic
  converted from c++ version by Elam Kolenovic
-}
 
import Foreign.Ptr
import Foreign.Marshal.Alloc
import Foreign.Storable
import Data.Bits
import Data.Word
import System.Environment
import System.IO
import Text.Printf
import Control.Concurrent

type Byte = Word8

main = do
  n <- getArgs >>= readIO.head
  let width = n :: Int
      height = n :: Int 
      max_x = (width + 7) `quot` 8 :: Int
      max_iterations = 50 :: Int
      limit = 2.0 :: Double
      limit_sq = limit * limit :: Double
      buffsz = height*max_x :: Int
      cr0sz = 8 * max_x :: Int

  buff <- new buffsz :: IO(Ptr Byte)
  cr0 <- new cr0sz :: IO(Ptr Double)

  let initcr0 x = do
          let go k | k < 8 = do
                      let xk = 8 * x + k
                      put cr0 xk ((2.0 * (fromIntegral xk)) / (fromIntegral width) - 1.5)
                      go (k+1)
                   | otherwise = return ()   
          go 0

  spawn initcr0 max_x

  let produce y = do
        line <- advance buff (y*max_x)
        cr <- new 8 :: IO(Ptr Double)
        ci <- new 8 :: IO(Ptr Double)
        let ci0 = 2.0 * (fromIntegral y) / (fromIntegral height) - 1.0 :: Double
        anyescape <- malloc :: IO(Ptr Byte)
        poke anyescape 0xff
        bits <- malloc :: IO(Ptr Byte)
        bit_k <- malloc :: IO(Ptr Byte)
        let xloop x 
             | x < max_x = do
                cr0_x <- advance cr0 (8*x)
                let initcrs k 
                     | k < 8 = do
                        v <- get cr0_x k
                        put cr k v
                        put ci k ci0
                        initcrs (k+1)
                     | otherwise = return ()
                    noany = do
                      poke bits 0xff
                      let goi i = do
                            bitsv <- peek bits 
                            if (toBool bitsv) && i < max_iterations 
                              then do
                                poke bit_k 0x80
                                let gok k = do
                                      if k < 8
                                        then do
                                          bitsv <- peek bits
                                          bit_kv <- peek bit_k
                                          if toBool (bitsv .&. bit_kv)
                                            then do
                                              cr_k <- get cr k
                                              ci_k <- get ci k
                                              cr0_xk <- get cr0_x k 
                                              let cr_k_sq = cr_k * cr_k
                                                  ci_k_sq = ci_k * ci_k
                                              put cr k (cr_k_sq - ci_k_sq + cr0_xk)
                                              put ci k (2.0 * cr_k * ci_k + ci0)
                                              if cr_k_sq + ci_k_sq > limit_sq
                                                then poke bits (bitsv `xor` bit_kv)
                                                else return ()
                                            else return ()   
                                          poke bit_k (bit_kv `shiftR` 1)
                                          gok (k+1)
                                        else return ()
                                gok 0      
                                goi (i+1)
                              else 
                                return ()   
                      goi 0
                    any = do
                      poke bits 0xff
                      let goi i = do
                            bitsv <- peek bits
                            if (toBool bitsv) && i < max_iterations-1
                              then do
                                let gok k = do
                                      if k < 8
                                        then do
                                          cr_k <- get cr k
                                          ci_k <- get ci k
                                          cr0_xk <- get cr0_x k 
                                          let cr_k_sq = cr_k * cr_k
                                              ci_k_sq = ci_k * ci_k
                                          put cr k (cr_k_sq - ci_k_sq + cr0_xk)
                                          put ci k (2.0 * cr_k * ci_k + ci0)
                                          gok (k+1)
                                        else return()  
                                gok 0
                                goi (i+1)
                              else return()
                      goi 0
                      poke bit_k 0x80
                      let gok k = do
                            if k < 8
                              then do
                                cr_k <- get cr k
                                ci_k <- get ci k
                                let sum = cr_k * cr_k + ci_k * ci_k
                                bitsv <- peek bits
                                bit_kv <- peek bit_k
                                if isNaN sum || sum > limit_sq
                                  then poke bits (bitsv `xor` bit_kv)
                                  else return ()
                                poke bit_k (bit_kv `shiftR` 1)
                                gok (k+1)
                              else return()
                      gok 0
                initcrs 0
                anyv <- peek anyescape
                if not (toBool anyv)
                  then noany
                  else any
                bitsv <- peek bits
                put line x bitsv
                poke anyescape bitsv
                xloop (x+1)
             | otherwise = return ()
        xloop 0
           
  spawn produce height
    
  printf "P4\n%u %u\n" width height
  hPutBuf stdout buff buffsz

toBool i = if i == 0 then False else True

new :: Storable a => Int -> IO (Ptr a)
new = new' undefined
    where new' :: Storable a => a -> Int -> IO (Ptr a)
          new' result num = mallocBytes (num * sizeOf result)

put b i v = pokeElemOff b i v
get b i = peekElemOff b i

advance :: Storable a => Ptr a -> Int -> IO (Ptr a)
advance = advance' undefined
    where advance' :: Storable a => a -> Ptr a -> Int -> IO (Ptr a)
          advance' result b i =
            return $ b `plusPtr` (i*sizeOf result)

spawn a n = do
    let qt = n`quot`8
        step = if qt == 0 then 1 else qt
        go i vars | i < n = do
                    let step' = if n-i < step then n-i else step
                        limit = i+step'
                    var <- newEmptyMVar
                    let loop i | i < limit = do
                                    a i
                                    loop (i+1)
                               | otherwise = putMVar var True
                    forkIO (loop i)
                    go limit (var:vars)
                  | otherwise = return vars
    vars <- go 0 []
    mapM_ takeMVar vars
    