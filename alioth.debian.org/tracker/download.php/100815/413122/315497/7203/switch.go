package main

import (
	"fmt"
	"runtime"
	"time"
)

func procs(q chan<- bool) {
	for i := 1; i <= 5; i++ {
		n := 1
		if i&1 != 0 {
			n = 4
		}
		previous := runtime.GOMAXPROCS(n)
		fmt.Printf("runtime maxprocs changed from %d to %d\n", n, previous)
		time.Sleep(time.Second * 3)
	}
	q <- true
	close(q)
}

func do(ch chan uint64) {
	ch <- 1
	for {
		x := <-ch
		for i := uint64(1); i < 100000; i++ {
			x *= i
		}
		ch <- x
	}
}

func main() {
	q := make(chan bool)
	go procs(q)
	ch := make(chan uint64, 4)
	go do(ch)
	go do(ch)
	go do(ch)
	go do(ch)
	<-q
}
