/* The Computer Language Benchmarks Game
 * http://benchmarksgame.alioth.debian.org/
 *
 * contributed by Jason Alan Palmer
 *
 * specialized with github.com/glycerine/offheap,
 *  an offheap hashtable in Go by Jason E. Aten;
 *  which in turn was inspired by
 *  http://preshing.com/20130107/this-hash-table-is-faster-than-a-judy-array/
 *  and https://github.com/preshing/CompareIntegerMaps
 *
 *  The java knucleotide entry uses a specialized hashtable
 *  library, so this is fair game. The rules
 *  for knucleotide specifically allow library
 *  and 3rd party hash tables.
 *
 *  Speedup: is about 25% faster for this job
 *  than using the builtin map; on my laptop.
 */

package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime/pprof"
	"sort"
	"strings"
	"syscall"
	"time"
	"unsafe"

	"github.com/glycerine/gommap"
)

var threeSeq = []byte(">THREE")

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	by, err := ReadSequenceFromFileWithLabel(os.Stdin, threeSeq)
	if err != nil {
		panic(err)
	}
	dna := by.Bytes()

	c3 := make(chan string)
	c4 := make(chan string)
	c6 := make(chan string)
	c12 := make(chan string)
	c18 := make(chan string)
	go func() { c3 <- seqReport(dna, "GGT") }()
	go func() { c4 <- seqReport(dna, "GGTA") }()
	go func() { c6 <- seqReport(dna, "GGTATT") }()
	go func() { c12 <- seqReport(dna, "GGTATTTTAATT") }()
	go func() { c18 <- seqReport(dna, "GGTATTTTAATTTATAGT") }()
	fmt.Println(freqReport(dna, 1))
	fmt.Println(freqReport(dna, 2))
	fmt.Println(<-c3)
	fmt.Println(<-c4)
	fmt.Println(<-c6)
	fmt.Println(<-c12)
	fmt.Println(<-c18)

}

var toNum = strings.NewReplacer(
	"A", string(0),
	"C", string(1),
	"G", string(2),
	"T", string(3),
	"a", string(0),
	"c", string(1),
	"g", string(2),
	"t", string(3),
)
var toChar = strings.NewReplacer(
	string(0), "A",
	string(1), "C",
	string(2), "G",
	string(3), "T",
)

func read() []byte {
	now := time.Now()
	var buf bytes.Buffer
	scanner := bufio.NewScanner(os.Stdin)
	for !strings.HasPrefix(scanner.Text(), ">THREE") {
		scanner.Scan()
	}
	for scanner.Scan() {
		buf.WriteString(toNum.Replace(scanner.Text()))
	}
	fmt.Printf("elap: %v\n", time.Since(now))
	return buf.Bytes()
}

type sequence struct {
	nucs  string
	count int
}
type sequenceSlice []sequence

func (ss sequenceSlice) Len() int {
	return len(ss)
}
func (ss sequenceSlice) Swap(i, j int) {
	ss[i], ss[j] = ss[j], ss[i]
}
func (ss sequenceSlice) Less(i, j int) bool {
	if ss[i].count == ss[j].count {
		return ss[i].nucs > ss[j].nucs
	}
	return ss[i].count > ss[j].count
}

func freqReport(dna []byte, length int) string {
	var buf bytes.Buffer
	var sortedSeqs sequenceSlice
	counts := count(dna, length)
	for it := counts.NewIterator(); it.Cur != nil; it.Next() {
		sortedSeqs = append(
			sortedSeqs,
			sequence{toChar.Replace(string(decompress(it.Cur.UnHashedKey, length))), int(it.Cur.Value)},
		)
	}
	sort.Sort(sortedSeqs)
	for _, seq := range sortedSeqs {
		buf.WriteString(fmt.Sprintf(
			"%v %.3f\n", seq.nucs,
			100.0*float32(seq.count)/float32(len(dna)-length+1)),
		)
	}
	return buf.String()
}

func seqReport(dna []byte, seq string) string {
	counts := count(dna, len(seq))
	seq_count, _ := counts.Lookup(compress([]byte(toNum.Replace(seq))))
	return fmt.Sprintf("%v\t%v", seq_count, seq)
}

func count(dna []byte, length int) *CountTable {
	counts := NewCountTable()
	for index := 0; index < len(dna)-length+1; index++ {
		key := compress(dna[index : index+length])
		counts.Increment(key)
	}
	return counts
}

func compress(seq []byte) uint32 {
	var num uint32
	for _, char := range seq {
		num = (num << 2) | uint32(char)
	}
	return num
}

func decompress(num uint32, length int) []byte {
	seq := make([]byte, length)
	for i := 0; i < length; i++ {
		seq[length-i-1] = byte(num & 3)
		num = num >> 2
	}
	return seq
}

const bufsz = 1 * 1024 * 1024

// ReadSequenceFromFileWithLabel uses bufio.ReadBytes().
// This is faster than a scanner.
func ReadSequenceFromFileWithLabel(fn *os.File, label []byte) (*bytes.Buffer, error) {
	// input
	bufIn := bufio.NewReaderSize(fn, bufsz)
	arr := make([]byte, 0, bufsz)
	buf := bytes.NewBuffer(arr)

	// ouput
	outarr := make([]byte, 0, bufsz)
	out := bytes.NewBuffer(outarr)

	lineNum := int64(1)
	found := false
	for {
		lastLine, err := bufIn.ReadBytes('\n')
		if err != nil && err != io.EOF {
			return out, err
		}

		if err == io.EOF && len(lastLine) == 0 {
			break
		}

		if lastLine[0] == '>' {
			if found {
				// done with the target sequence
				return out, err
			}
			if bytes.HasPrefix(lastLine, label) {
				found = true
			}
		} else {
			if found {
				out.WriteString(toNum.Replace(string(lastLine[:len(lastLine)-1])))
			}
		}

		buf.Reset()
		lineNum += 1

		if err == io.EOF {
			break
		}
	}

	return out, nil
}

// from offheap.go

// offheap Copyright (C) 2015 by Jason E. Aten, Ph.D.
// MIT licensed.

// metadata serialization size can never grow bigger
// than MetadataHeaderMaxBytes (currently one 4K page), without impacting
// backwards compatibility. We reserve this many bytes
// at the beginning of the memory mapped file for the metadata.
const MetadataHeaderMaxBytes = 4096

// HashTable represents the off-heap hash table.
// Create a new one with NewHashTable(), then use
// Lookup(), Insert(), and DeleteKey() on it.
// HashTable is meant to be customized by the
// user, to reflect your choice of key and value
// types. See StringHashTable and ByteKeyHashTable
// for examples of this specialization process.
type HashTable struct {
	MagicNumber   int     // distinguish between reading from empty file versus an on-disk HashTable.
	Cells         uintptr `msg:"-"`
	CellSz        uint32
	ArraySize     uint32
	Population    uint32
	ZeroUsed      bool
	ZeroCell      Cell
	OffheapHeader []byte     `msg:"-"`
	OffheapCells  []byte     `msg:"-"`
	Mmm           MmapMalloc `msg:"-"`
}

// CountTable is the analog of map[uint32]int32
type CountTable HashTable

// Create a new hash table, able to hold initialSize count of keys.
func NewHashTable(initialSize uint32) *HashTable {
	return NewHashFileBacked(initialSize, "")
}

func NewHashFileBacked(initialSize uint32, filepath string) *HashTable {

	t := HashTable{
		MagicNumber: 3030675468910466832,
		CellSz:      uint32(unsafe.Sizeof(Cell{})),
	}

	// off-heap and off-gc version
	t.ArraySize = initialSize
	t.Mmm = *Malloc(int64(t.ArraySize*t.CellSz)+MetadataHeaderMaxBytes, filepath)
	t.OffheapHeader = t.Mmm.Mem
	t.OffheapCells = t.Mmm.Mem[MetadataHeaderMaxBytes:]
	t.Cells = (uintptr)(unsafe.Pointer(&t.OffheapCells[0]))

	// old: off-gc but still on-heap version
	//	t.ArraySize = initialSize
	//	t.Offheap = make([]byte, t.ArraySize*t.CellSz)
	//	t.Cells = (uintptr)(unsafe.Pointer(&t.Offheap[0]))

	// on-heap version:
	//Cells:     make([]Cell, initialSize),

	return &t
}

// Key_t is the basic type for keys. Users of the library will
// probably redefine this.
type Key_t uint32

// Val_t is the basic type for values stored in the cells in the table.
// Users of the library will probably redefine this to be a different
// size at the very least.
type Val_t int64

// Cell is the basic payload struct, stored inline in the HashTable. The
// cell is returned by the fundamental Lookup() function. The member
// Value is where the value that corresponds to the key (in ByteKey)
// is stored. Both the key (in ByteKey) and the value (in Value) are
// stored inline inside the hashtable, so that all storage for
// the hashtable is in the same offheap segment. The uint32 key given
// to fundamental Insert() method is stored in UnHashedKey. The hashed value of the
// UnHashedKey is not stored in the Cell, but rather computed as needed
// by the basic Insert() and Lookup() methods.
//
// ByteKey is redundant for our CountTable.
type Cell struct {
	UnHashedKey uint32
	Value       uint32 // customize this to hold your value's data type entirely here.
}

// ZeroValue sets the cell's value to all zeros.
func (cell *Cell) ZeroValue() {
	cell.Value = 0
}

// CellAt: fetch the cell at a given index. E.g. t.CellAt(pos) replaces t.Cells[pos]
func (t *HashTable) CellAt(pos uint32) *Cell {

	// off heap version
	return (*Cell)(unsafe.Pointer(uintptr(t.Cells) + uintptr(pos*t.CellSz)))

	// on heap version, back when t.Cells was []Cell
	//return &(t.Cells[pos])
}

// CellAt: fetch the cell at a given index. E.g. t.CellAt(pos) replaces t.Cells[pos]
func (t *CountTable) CellAt(pos uint32) *Cell {

	// off heap version
	return (*Cell)(unsafe.Pointer(uintptr(t.Cells) + uintptr(pos*t.CellSz)))

	// on heap version, back when t.Cells was []Cell
	//return &(t.Cells[pos])
}

// DestroyHashTable frees the memory-mapping, returning the
// memory containing the hash table and its cells to the OS.
// By default the save-to-file-on-disk functionality in malloc.go is
// not used, but that can be easily activated. See malloc.go.
// Deferencing any cells/pointers into the hash table after
// destruction will result in crashing your process, almost surely.
func (t *HashTable) DestroyHashTable() {
	t.Mmm.Free()
}

// Lookup a cell based on a uint32 key value. Returns nil if key not found.
func (t *HashTable) Lookup(key uint32) *Cell {

	var cell *Cell

	if key == 0 {
		if t.ZeroUsed {
			return &t.ZeroCell
		}
		return nil

	} else {

		h := integerHash32(key) % t.ArraySize

		for {
			cell = t.CellAt(h)
			if cell.UnHashedKey == key {
				return cell
			}
			if cell.UnHashedKey == 0 {
				return nil
			}
			h++
			if h == t.ArraySize {
				h = 0
			}
		}
	}
}

// Insert a key and get back the Cell for that key, so
// as to enable assignment of Value within that Cell, for
// the specified key. The 2nd return value is false if
// key already existed (and thus required no addition); if
// the key already existed you can inspect the existing
// value in the *Cell returned.
func (t *HashTable) Insert(key uint32) (*Cell, bool) {

	var cell *Cell

	if key != 0 {

		for {
			h := integerHash32(key) % t.ArraySize

			for {
				cell = t.CellAt(h)
				if cell.UnHashedKey == key {
					// already exists
					return cell, false
				}
				if cell.UnHashedKey == 0 {
					if (t.Population+1)*4 >= t.ArraySize*3 {
						t.Repopulate(t.ArraySize * 2)
						// resized, so start all over
						break
					}
					t.Population++
					cell.UnHashedKey = key
					return cell, true
				}

				h++
				if h == t.ArraySize {
					h = 0
				}

			}
		}
	} else {

		wasNew := false
		if !t.ZeroUsed {
			wasNew = true
			t.ZeroUsed = true
			t.Population++
			if t.Population*4 >= t.ArraySize*3 {

				t.Repopulate(t.ArraySize * 2)
			}
		}
		return &t.ZeroCell, wasNew
	}

}

// DeleteCell deletes the cell pointed to by cell.
func (t *HashTable) DeleteCell(cell *Cell) {

	if cell == &t.ZeroCell {
		// Delete zero cell
		if !t.ZeroUsed {
			panic("deleting zero element when not used")
		}
		t.ZeroUsed = false
		cell.ZeroValue()
		t.Population--
		return

	} else {

		pos := uint32((uintptr(unsafe.Pointer(cell)) - uintptr(unsafe.Pointer(t.Cells))) / uintptr(unsafe.Sizeof(Cell{})))

		// Delete from regular Cells
		if pos < 0 || pos >= t.ArraySize {
			panic(fmt.Sprintf("cell out of bounds: pos %v was < 0 or >= t.ArraySize == %v", pos, t.ArraySize))
		}

		if t.CellAt(pos).UnHashedKey == 0 {
			panic("zero UnHashedKey in non-zero Cell!")
		}

		// Remove this cell by shuffling neighboring Cells so there are no gaps in anyone's probe chain
		nei := pos + 1
		if nei >= t.ArraySize {
			nei = 0
		}
		var neighbor *Cell
		var circular_offset_ideal_pos int64
		var circular_offset_ideal_nei int64
		var cellPos *Cell

		for {
			neighbor = t.CellAt(nei)

			if neighbor.UnHashedKey == 0 {
				// There's nobody to swap with. Go ahead and clear this cell, then return
				cellPos = t.CellAt(pos)
				cellPos.UnHashedKey = 0
				cellPos.ZeroValue()
				t.Population--
				return
			}

			ideal := integerHash32(neighbor.UnHashedKey) % t.ArraySize

			if pos >= ideal {
				circular_offset_ideal_pos = int64(pos) - int64(ideal)
			} else {
				// pos < ideal, so pos - ideal is negative, wrap-around has happened.
				circular_offset_ideal_pos = int64(t.ArraySize) - int64(ideal) + int64(pos)
			}

			if nei >= ideal {
				circular_offset_ideal_nei = int64(nei) - int64(ideal)
			} else {
				// nei < ideal, so nei - ideal is negative, wrap-around has happened.
				circular_offset_ideal_nei = int64(t.ArraySize) - int64(ideal) + int64(nei)
			}

			if circular_offset_ideal_pos < circular_offset_ideal_nei {
				// Swap with neighbor, then make neighbor the new cell to remove.
				*t.CellAt(pos) = *neighbor
				pos = nei
			}

			nei++
			if nei >= t.ArraySize {
				nei = 0
			}
		}
	}

}

// Clear does not resize the table, but zeroes-out all entries.
func (t *HashTable) Clear() {
	// (Does not resize the array)
	// Clear regular Cells

	for i := range t.OffheapCells {
		t.OffheapCells[i] = 0
	}
	t.Population = 0

	// Clear zero cell
	t.ZeroUsed = false
	t.ZeroCell.ZeroValue()
}

// Compact will compress the hashtable so that it is at most
// 75% full.
func (t *HashTable) Compact() {
	t.Repopulate(uint32(upper_power_of_two(uint64((t.Population*4 + 3)) / 3)))
}

// DeleteKey will delete the contents of the cell associated with key.
func (t *HashTable) DeleteKey(key uint32) {
	value := t.Lookup(key)
	if value != nil {
		t.DeleteCell(value)
	}
}

// Repopulate expands the hashtable to the desiredSize count of cells.
func (t *HashTable) Repopulate(desiredSize uint32) {

	if desiredSize&(desiredSize-1) != 0 {
		panic("desired size must be a power of 2")
	}
	if t.Population*4 > desiredSize*3 {
		panic("must have t.Population * 4  <= desiredSize * 3")
	}

	// Allocate new table
	s := NewHashTable(desiredSize)

	s.ZeroUsed = t.ZeroUsed
	if t.ZeroUsed {
		s.ZeroCell = t.ZeroCell
		s.Population++
	}

	// Iterate through old table t, copy into new table s.
	var c *Cell

	for i := uint32(0); i < t.ArraySize; i++ {
		c = t.CellAt(i)
		if c.UnHashedKey != 0 {
			// Insert this element into new table
			cell, ok := s.Insert(c.UnHashedKey)
			if !ok {
				panic(fmt.Sprintf("key '%v' already exists in fresh table s: should be impossible", c.UnHashedKey))
			}
			*cell = *c
		}
	}

	t.DestroyHashTable()

	*t = *s
}

// Incrmenet adds one to the count in the Cell
// identified by key.
func (t *HashTable) Increment(key uint32) {

	var cell *Cell

	if key != 0 {

		for {
			h := integerHash32(key) % t.ArraySize

			for {
				cell = t.CellAt(h)
				if cell.UnHashedKey == key {
					// already exists
					cell.Value++
					return
				}
				if cell.UnHashedKey == 0 {
					if (t.Population+1)*4 >= t.ArraySize*3 {
						t.Repopulate(t.ArraySize * 2)
						// resized, so start all over
						break
					}
					t.Population++
					cell.UnHashedKey = key
					cell.Value++
					return
				}

				h++
				if h == t.ArraySize {
					h = 0
				}

			}
		}
	} else {

		if !t.ZeroUsed {
			t.ZeroUsed = true
			t.Population++
			if t.Population*4 >= t.ArraySize*3 {

				t.Repopulate(t.ArraySize * 2)
			}
		}
		t.ZeroCell.Value++
		return
	}

}

/*
Iterator

sample use: given a HashTable h, enumerate h's contents with:

    for it := offheap.NewIterator(h); it.Cur != nil; it.Next() {
      found = append(found, it.Cur.UnHashedKey)
    }
*/
type Iterator struct {
	Tab *CountTable
	Pos int64
	Cur *Cell // will be set to nil when done with iteration.
}

// NewIterator creates a new iterator for CountTable tab.
func (tab *CountTable) NewIterator() *Iterator {
	it := &Iterator{
		Tab: tab,
		Cur: &tab.ZeroCell,
		Pos: -1, // means we are at the ZeroCell to start with
	}

	if it.Tab.Population == 0 {
		it.Cur = nil
		it.Pos = -2
		return it
	}

	if !it.Tab.ZeroUsed {
		it.Next()
	}

	return it
}

// Done checks to see if we have already iterated through all cells
// in the table. Equivalent to checking it.Cur == nil.
func (it *Iterator) Done() bool {
	if it.Cur == nil {
		return true
	}
	return false
}

// Next advances the iterator so that it.Cur points to the next
// filled cell in the table, and returns that cell. Returns nil
// once there are no more cells to be visited.
func (it *Iterator) Next() *Cell {

	// Already finished?
	if it.Cur == nil {
		return nil
	}

	// Iterate through the regular Cells
	it.Pos++
	for uint32(it.Pos) != it.Tab.ArraySize {
		it.Cur = it.Tab.CellAt(uint32(it.Pos))
		if it.Cur.UnHashedKey != 0 {
			return it.Cur
		}
		it.Pos++
	}

	// Finished
	it.Cur = nil
	it.Pos = -2
	return nil
}

// Dump provides a diagnostic dump of the full CountTable contents.
func (t *CountTable) Dump() {
	for i := uint32(0); i < t.ArraySize; i++ {
		cell := t.CellAt(i)
		fmt.Printf("dump cell %d: \n cell.UnHashedKey: '%v'\n cell.Value: '%v'\n ===============", i, cell.UnHashedKey, cell.Value)
	}
}

func upper_power_of_two32(v uint32) uint32 {
	v--
	v |= v >> 1
	v |= v >> 2
	v |= v >> 4
	v |= v >> 8
	v |= v >> 16
	v++
	return v
}

func upper_power_of_two(v uint64) uint64 {
	v--
	v |= v >> 1
	v |= v >> 2
	v |= v >> 4
	v |= v >> 8
	v |= v >> 16
	v |= v >> 32
	v++
	return v
}

// from code.google.com/p/smhasher/wiki/MurmurHash3
func integerHash32(h uint32) uint32 {
	h ^= h >> 16
	h *= 0x85ebca6b
	h ^= h >> 13
	h *= 0xc2b2ae35
	h ^= h >> 16
	return h
}

// from code.google.com/p/smhasher/wiki/MurmurHash3
func integerHash(k uint64) uint64 {
	k ^= k >> 33
	k *= 0xff51afd7ed558ccd
	k ^= k >> 33
	k *= 0xc4ceb9fe1a85ec53
	k ^= k >> 33
	return k
}

// specialized from github.com/glycerine/offheap, an offheap hashtable.

// NewCountTable produces a new CountTable, one specialized for
// handling uint32 keys and counts. Resembles map[uint32]uint32.
func NewCountTable() *CountTable {
	var initialSize uint32 = 524288
	return (*CountTable)(NewHashTable(initialSize))
}

// InsertIntKey is the insert function for uint32 keys.
func (t *CountTable) InsertKey(key uint32, value uint32) bool {
	cell, ok := (*HashTable)(t).Insert(key)
	//cell.ByteKey = key
	cell.Value = value
	return ok
}

// Increment adds one to the Value of the Cell identified by key.
func (t *CountTable) Increment(key uint32) {
	(*HashTable)(t).Increment(key)
}

func minimum(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// Lookup is the lookup function for uint32 keys.
func (t *CountTable) Lookup(key uint32) (uint32, bool) {
	cell := (*HashTable)(t).Lookup(key)
	if cell == nil {
		return 0, false
	}
	return cell.Value, true
}

// Delete removes an entry with key.
func (t *CountTable) Delete(key uint32) bool {
	cell := (*HashTable)(t).Lookup(key)
	if cell == nil {
		return false
	}

	return true
}

// from malloc.go

// The MmapMalloc struct represents either an anonymous, private
// region of memory (if path was "", or a memory mapped file if
// path was supplied to Malloc() at creation.
//
// Malloc() creates and returns an MmapMalloc struct, which can then
// be later Free()-ed. Malloc() calls request memory directly
// from the kernel via mmap(). Memory can optionally be backed
// by a file for simplicity/efficiency of saving to disk.
//
// For use when the Go GC overhead is too large, and you need to move
// the hash table off-heap.
//
type MmapMalloc struct {
	Path         string
	File         *os.File
	Fd           int
	FileBytesLen int64
	BytesAlloc   int64
	MMap         gommap.MMap // equiv to Mem, just avoids casts everywhere.
	Mem          []byte      // equiv to Mmap
}

// TruncateTo enlarges or shortens the file backing the
// memory map to be size newSize bytes. It only impacts
// the file underlying the mapping, not
// the mapping itself at this point.
func (mm *MmapMalloc) TruncateTo(newSize int64) {
	if mm.File == nil {
		panic("cannot call TruncateTo() on a non-file backed MmapMalloc.")
	}
	err := syscall.Ftruncate(int(mm.File.Fd()), newSize)
	if err != nil {
		panic(err)
	}
}

// Free eleases the memory allocation back to the OS by removing
// the (possibly anonymous and private) memroy mapped file that
// was backing it. Warning: any pointers still remaining will crash
// the program if dereferenced.
func (mm *MmapMalloc) Free() {
	if mm.File != nil {
		mm.File.Close()
	}
	err := mm.MMap.UnsafeUnmap()
	if err != nil {
		panic(err)
	}
}

// Malloc() creates a new memory region that is provided directly
// by OS via the mmap() call, and is thus not scanned by the Go
// garbage collector.
//
// If path is not empty then we memory map to the given path.
// Otherwise it is just like a call to malloc(): an anonymous memory allocation,
// outside the realm of the Go Garbage Collector.
// If numBytes is -1, then we take the size from the path file's size. Otherwise
// the file is expanded or truncated to be numBytes in size. If numBytes is -1
// then a path must be provided; otherwise we have no way of knowing the size
// to allocate, and the function will panic.
//
// The returned value's .Mem member holds a []byte pointing to the returned memory (as does .MMap, for use in other gommap calls).
//
func Malloc(numBytes int64, path string) *MmapMalloc {

	mm := MmapMalloc{
		Path: path,
	}

	flags := syscall.MAP_SHARED
	if path == "" {
		flags = syscall.MAP_ANON | syscall.MAP_PRIVATE
		mm.Fd = -1

		if numBytes < 0 {
			panic("numBytes was negative but path was also empty: don't know how much to allocate!")
		}

	} else {

		if dirExists(mm.Path) {
			panic(fmt.Sprintf("path '%s' already exists as a directory, so cannot be used as a memory mapped file.", mm.Path))
		}

		if !fileExists(mm.Path) {
			file, err := os.Create(mm.Path)
			if err != nil {
				panic(err)
			}
			mm.File = file
		} else {
			file, err := os.OpenFile(mm.Path, os.O_RDWR, 0777)
			if err != nil {
				panic(err)
			}
			mm.File = file
		}
		mm.Fd = int(mm.File.Fd())
	}

	sz := numBytes
	if path != "" {
		// file-backed memory
		if numBytes < 0 {

			var stat syscall.Stat_t
			if err := syscall.Fstat(mm.Fd, &stat); err != nil {
				panic(err)
			}
			sz = stat.Size

		} else {
			// set to the size requested
			err := syscall.Ftruncate(mm.Fd, numBytes)
			if err != nil {
				panic(err)
			}
		}
		mm.FileBytesLen = sz
	}
	// INVAR: sz holds non-negative length of memory/file.

	mm.BytesAlloc = sz

	prot := syscall.PROT_READ | syscall.PROT_WRITE

	var mmap []byte
	var err error
	if mm.Fd == -1 {

		flags = syscall.MAP_ANON | syscall.MAP_PRIVATE
		mmap, err = syscall.Mmap(-1, 0, int(sz), prot, flags)

	} else {

		flags = syscall.MAP_SHARED
		mmap, err = syscall.Mmap(mm.Fd, 0, int(sz), prot, flags)
	}
	if err != nil {
		panic(err)
	}

	// duplicate member to avoid casts all over the place.
	mm.MMap = mmap
	mm.Mem = mmap

	return &mm
}

// BlockUntilSync() returns only once the file is synced to disk.
func (mm *MmapMalloc) BlockUntilSync() {
	mm.MMap.Sync(gommap.MS_SYNC)
}

// BackgroundSync() schedules a sync to disk, but may return before it is done.
// Without a call to either BackgroundSync() or BlockUntilSync(), there
// is no guarantee that file has ever been written to disk at any point before
// the munmap() call that happens during Free(). See the man pages msync(2)
// and mmap(2) for details.
func (mm *MmapMalloc) BackgrounSync() {
	mm.MMap.Sync(gommap.MS_ASYNC)
}

func fileExists(name string) bool {
	fi, err := os.Stat(name)
	if err != nil {
		return false
	}
	if fi.IsDir() {
		return false
	}
	return true
}

func dirExists(name string) bool {
	fi, err := os.Stat(name)
	if err != nil {
		return false
	}
	if fi.IsDir() {
		return true
	}
	return false
}
