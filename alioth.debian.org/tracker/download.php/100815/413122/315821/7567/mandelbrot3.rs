// The Computer Language Benchmarks Game
// http://benchmarksgame.alioth.debian.org/
//
// Contributed by Kevin Miller
// Converted from C to Rust by Tung Duong
// Some improvements by leonardo maffi
//
// Compile with:
// rustc -C opt-level=3 -C target-cpu=native -C lto -C panic=abort

extern crate rayon;
#[macro_use] extern crate arrayref;

use std::ops::{Add, Sub, Mul};
use std::io::Write;
use rayon::prelude::*;

#[derive(Clone, Copy, Default)]
struct Pair(f64, f64);

const N_PAIRS: usize = 4;
type Pairs = [Pair; N_PAIRS];

impl Add for Pair {
    type Output = Self;
    fn add(self, rhs: Self) -> Self {
        Pair(self.0 + rhs.0, self.1 + rhs.1)
    }
}

impl Sub for Pair {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self {
        Pair(self.0 - rhs.0, self.1 - rhs.1)
    }
}

impl Mul for Pair {
    type Output = Self;
    fn mul(self, rhs: Self) -> Self {
        Pair(self.0 * rhs.0, self.1 * rhs.1)
    }
}

#[inline(always)]
fn vec_nle(v: &Pairs, f: f64) -> bool {
    v[0].0 > f && v[0].1 > f &&
    v[1].0 > f && v[1].1 > f &&
    v[2].0 > f && v[2].1 > f &&
    v[3].0 > f && v[3].1 > f
}

#[inline(always)]
fn clr_pixels_nle(v: &Pairs, f: f64, pix8: &mut u8){
    if !(v[0].0 <= f) { *pix8 &= 0b_0111_1111; }
    if !(v[0].1 <= f) { *pix8 &= 0b_1011_1111; }
    if !(v[1].0 <= f) { *pix8 &= 0b_1101_1111; }
    if !(v[1].1 <= f) { *pix8 &= 0b_1110_1111; }
    if !(v[2].0 <= f) { *pix8 &= 0b_1111_0111; }
    if !(v[2].1 <= f) { *pix8 &= 0b_1111_1011; }
    if !(v[3].0 <= f) { *pix8 &= 0b_1111_1101; }
    if !(v[3].1 <= f) { *pix8 &= 0b_1111_1110; }
}

#[inline(always)]
fn calc_sum(r: &mut Pairs, i: &mut Pairs, sum: &mut Pairs,
            init_r: &Pairs, init_i: Pair) {
    for j in 0 .. N_PAIRS {
        let r2 = r[j] * r[j];
        let i2 = i[j] * i[j];
        let ri = r[j] * i[j];
        sum[j] = r2 + i2;
        r[j] = r2 - i2 + init_r[j];
        i[j] = ri + ri + init_i;
    }
}

#[inline(always)]
fn mand8(init_r: &Pairs, init_i: Pair) -> u8 {
    let mut r: Pairs = Default::default();
    r.copy_from_slice(&init_r[0 .. N_PAIRS]);

    let mut i: Pairs = [init_i; N_PAIRS];
    let mut pix8: u8 = 0xff;
    let mut sum: Pairs = Default::default();

    for _ in 0 .. 6 {
        for _ in 0 .. 8 {
            calc_sum(&mut r, &mut i, &mut sum, &init_r, init_i);
        }

        if vec_nle(&sum, 4.0) {
            pix8 = 0x00;
            break;
        }
    }
    if pix8 != 0 {
        calc_sum(&mut r, &mut i, &mut sum, &init_r, init_i);
        calc_sum(&mut r, &mut i, &mut sum, &init_r, init_i);
        clr_pixels_nle(&sum, 4.0, &mut pix8);
    }

    pix8
}

fn mand64(init_r: &[Pair; 32], init_i: Pair, out: &mut [u8; 8]) {
    for (i, oi) in out.iter_mut().enumerate() {
        *oi = mand8(array_ref!(init_r, i * N_PAIRS, N_PAIRS), init_i);
    }
}

fn main() {
    let mut width = std::env::args()
                    .nth(1)
                    .unwrap_or("16_000".into())
                    .parse()
                    .unwrap_or(16_000);
    width = (width + 7) & !7;

    println!("P4\n{} {}", width, width);
    let len = width / 2;
    let mut r0 = Vec::<Pair>::with_capacity(len);
    let mut i0 = vec![0.0; width];

    for i in 0 .. len {
        let x1 = (2 * i) as f64;
        let x2 = (2 * i + 1) as f64;
        let k = 2.0 / (width as f64);
        r0.push(Pair(k * x1, k * x2) - Pair(1.5, 1.5));
        i0[2 * i] = k * x1 - 1.0;
        i0[2 * i + 1] = k * x2 - 1.0;
    }

    let rows: Vec<_> = if width % 64 == 0 {
        // Process 64 pixels (8 bytes) at a time.
        (0 .. width).into_par_iter().map(|y| {
            let mut row = vec![0u8; width / 8];
            let init_i = Pair(i0[y], i0[y]);

            for x in 0 .. width / 64 {
                mand64(array_ref!(r0, 32 * x, 32), init_i, array_mut_ref!(row, 8 * x, 8));
            }
            row
        }).collect()
    } else {
        // Process 8 pixels (1 byte) at a time.
        (0 .. width).into_par_iter().map(|y| {
            let mut row = vec![0u8; width / 8];
            let init_i = Pair(i0[y], i0[y]);

            for x in 0 .. width / 8 {
                row[x] = mand8(array_ref!(r0, x * 4, 4), init_i);
            }
            row
        }).collect()
    };

    let stdout_unlocked = std::io::stdout();
    let mut stdout = stdout_unlocked.lock();
    for row in rows {
        stdout.write_all(&row).unwrap();
    }
    stdout.flush().unwrap();
}