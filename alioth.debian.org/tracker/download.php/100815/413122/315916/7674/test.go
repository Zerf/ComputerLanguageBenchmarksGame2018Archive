package main

import (
	"bufio"
	"bytes"
	"io/ioutil"
	"os"
	"regexp"
	"runtime"
	"strconv"
)

var vs = [9]string{
	"agggtaaa|tttaccct",
	"[cgt]gggtaaa|tttaccc[acg]",
	"a[act]ggtaaa|tttacc[agt]t",
	"ag[act]gtaaa|tttac[agt]ct",
	"agg[act]taaa|ttta[agt]cct",
	"aggg[acg]aaa|ttt[cgt]ccct",
	"agggt[cgt]aa|tt[acg]accct",
	"agggta[cgt]a|t[acg]taccct",
	"agggtaa[cgt]|[acg]ttaccct",
}

var subs = [5]struct {
	p string
	r []byte
}{
	{"tHa[Nt]", []byte("<4>")},
	{"aND|caN|Ha[DS]|WaS", []byte("<3>")},
	{"a[NSt]|BY", []byte("<2>")},
	{"<[^>]*>", []byte("|")},
	{"\\|[^|][^|]*\\|", []byte("-")},
}

const (
	Empty   = " "
	NewLine = "\n"
	RegExp  = "(>[^\n]+)?\n"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	bts, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		panic("can't read input: %s\n")
	}
	ilen := len(bts)

	r := regexp.MustCompile(RegExp)
	bts = r.ReplaceAll(bts, nil)
	clen := len(bts)

	buf := bufio.NewWriter(os.Stdout)
	ch := make(chan []byte)
	for i := 0; i < len(vs); i++ {
		go func(i int) {
			r := regexp.MustCompile(vs[i])
			m := r.FindAll(bts, -1)

			buf := bytes.NewBuffer(nil)
			buf.WriteString(vs[i])
			buf.WriteString(Empty)
			buf.WriteString(strconv.Itoa(len(m)))
			buf.WriteString(NewLine)
			ch <- buf.Bytes()
		}(i)
	}

	cnt := 0
	for s := range ch {
		cnt++
		buf.Write(s)
		if cnt == len(vs) {
			break
		}
	}

	for _, sub := range subs {
		r := regexp.MustCompile(sub.p)
		bts = r.ReplaceAll(bts, sub.r)
	}

	buf.WriteString(NewLine)
	buf.WriteString(strconv.Itoa(ilen))
	buf.WriteString(NewLine)
	buf.WriteString(strconv.Itoa(clen))
	buf.WriteString(NewLine)
	buf.WriteString(strconv.Itoa(len(bts)))
	buf.WriteString(NewLine)

	buf.Flush()
}
