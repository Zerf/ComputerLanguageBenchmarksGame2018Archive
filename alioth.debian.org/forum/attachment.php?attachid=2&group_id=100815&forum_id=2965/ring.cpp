/*
 * The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 * Thread-ring implementation
 *
 * Copyright (C) 2009  Pauli Nieminen
 * All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 *
 *  * Neither the name of "The Computer Language Benchmarks Game" nor the name
 *    of "The Computer Language Shootout Benchmarks" nor the names of its 
 *    contributors may be used to endorse or promote products derived from this 
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Threading uses GNU pth userspace threading library (libpth20 libpth-dev)
 * This implementation is using non-preemptive N:1 threading model.
 * Ring is created using single Mvar per thread. 
 * Every thread waits that someone passes token to thier Mvar.
 * When token is received the thread checks for value and passes it to next thread.
 * class thread_manager handles scheduling of active threads.
 *
 * compile: g++ -pipe --static <this file> -lpth -O3 -fomit-frame-pointer -o <output file>
 * run: ./<output file> <N>
 **/


// thread.hpp
#include <pth.h>

const size_t MIN_STACK_SIZE = 16384;

const size_t NUM_THREADS = 503;
char stack[NUM_THREADS][MIN_STACK_SIZE];

template<class T>
void start_func(void *param)
{
	T* func = reinterpret_cast<T*>(param);
	(*func)();
}


class thread
{
	public:
		thread();
		~thread();

		template <class F>
			explicit thread(F &f) :
				next_(this), 
				previous_(this),
				self_()
		{
			create_context();
			make_context(f);
			resume();
		}
		void activate();

		static thread& get_current();

		void create_context();

		// execution control
		static void exit();
		static void yield();
		void suspend();
		void resume();

		const pth_uctx_t& get_ctx() const
		{
			return self_;
		}

		/* list functionality to queue threads */
		bool empty() const;
		void add_to_end(thread& t);
		void remove_thread();
		void rotate();
		thread& first_thread() const;
	private:
		static size_t stack_counter;
		template <class F>
			void make_context(F &f)
		{
			if (!pth_uctx_make(self_, stack[stack_counter++], MIN_STACK_SIZE, NULL, 
					start_func<F>,reinterpret_cast<void*>(&f), NULL))
				throw "MAKE\n";
		}


		thread *next_;
		thread *previous_;

		//pth context
		pth_uctx_t self_;
};

size_t thread::stack_counter = 0;


// thread.cpp

class thread_manager {
	thread queue_;
	thread *current_;
	public:

	thread_manager() : queue_(), current_(&queue_)
	{
		queue_.create_context();
	}

	thread& get_current() const
	{
		return *current_;
	}

	void ctx_switch()
	{
		if (queue_.empty())
		{
			thread::exit();
		}
		thread* temp = current_;
		current_ = &queue_.first_thread();
		queue_.rotate();
		pth_uctx_switch(temp->get_ctx(), current_->get_ctx());
	}

	void exit_to_main() const
	{
		pth_uctx_switch(current_->get_ctx(), queue_.get_ctx());
	}

	void activate(thread& t)
	{
		queue_.add_to_end(t);
	}

} scheduler;

thread::thread() : 
	next_(this), 
	previous_(this),
	self_()
{
}

thread::~thread()
{
	pth_uctx_destroy(self_);
}

void thread::create_context()
{
	if (!pth_uctx_create(&self_))
		throw "CREATE\n";
}

bool thread::empty() const
{
	return next_ == this && previous_ == this;
}

void thread::add_to_end(thread& t)
{
	t.remove_thread();
	thread* temp = previous_;
	previous_ = &t;
	t.next_ = this;
	t.previous_ = temp;
	temp->next_ = &t;
}

void thread::remove_thread()
{
	next_->previous_ = previous_;
	previous_->next_ = next_;
	previous_ = this;
	next_ = this;
}

void thread::rotate()
{
	if (next_ == previous_)
		return;
	// move first entry to last
	previous_->next_ = next_;
	next_->next_->previous_ = this;
	next_->previous_ = previous_;
	thread * temp = next_->next_;
	next_->next_ = this;
	previous_ = next_;
	next_ = temp;
}

thread& thread::first_thread() const
{
	return *next_;
}

void thread::suspend()
{
	scheduler.ctx_switch();
}

void thread::resume()
{
	scheduler.activate(*this);
}

void thread::yield()
{
	scheduler.ctx_switch();
}

void thread::exit()
{
	scheduler.exit_to_main();
}

thread& thread::get_current()
{
	return scheduler.get_current();
}

// Mvar.hpp

#include <cassert>

template<class T>
class Mvar {
	bool has_value;
	T value_;

	Mvar(const Mvar&);
	Mvar operator=(const Mvar&);

	thread queue;

public:
	Mvar() : has_value(false), value_() {}

	void put(const T& val)
	{
		if (has_value)
		{
			queue.add_to_end(thread::get_current());
			// we return execution when Mvar is empty
			thread::get_current().suspend();
			assert(!has_value);
		}

		value_ = val;
		has_value = true;

		if (!queue.empty())
		{
			thread& activate = queue.first_thread();
			// implicity remove from Mvar queue
			activate.resume();
		}
	}


	void get(T& val)
	{
		if (!has_value)
		{
			queue.add_to_end(thread::get_current());
			thread::get_current().suspend();
			assert(has_value);
		}

		val = value_;
		has_value = false;

		if (!queue.empty())
		{
			thread& activate = queue.first_thread();
			// implicity remove from Mvar queue
			activate.resume();
		}
	}
};

// ring.cpp

#include <iostream>
#include <cstdlib>


typedef Mvar<int> messanger;

class ring_thread {
	messanger &listen_;
	messanger &send_;
	size_t id_;
	public:
		ring_thread(size_t id, messanger &listen, messanger &send) :
			id_(id),
			listen_(listen),
			send_(send)
		{
		}
		
		void operator()(void)
		{
			int value;
			while(true)
			{
				listen_.get(value);
				if (value > 0)
				{
					send_.put(value - 1);
				} else {
					std::cout << id_ << "\n";
					thread::exit();
				}
			}
		}
};

messanger msg[NUM_THREADS];


class ring_thread_creator {
	static size_t i;
	ring_thread r_;
	thread t_;
	size_t get_send(const size_t& i) const
	{
		return i;
	}
	size_t get_listen(const size_t& i) const
	{
		if (i > 0)
			return i - 1;
		return NUM_THREADS - 1;
	}
	public:
	ring_thread_creator() : 
		r_(i+1, msg[get_listen(i)], msg[get_send(i)]),
		t_(r_)
	{
		++i;
	}
};

size_t ring_thread_creator::i = 0;

int main(int argc, char** args)
{
	size_t start_value = 1000;
	size_t i;

	if (argc >= 2)
		start_value = std::atoi(args[1]);

	ring_thread_creator threads[NUM_THREADS];

	msg[NUM_THREADS - 1].put(start_value);

	// Main thread default to suspend state!
	// So we continue execution only after thread::exit() call
	thread::yield();
	return 0;
}
