/* The Computer Language Benchmarks Game
 * http://shootout.alioth.debian.org/
 *
 * contributed by Francesco Abbate
 */


#include <stdlib.h>
#include <stdio.h>

typedef off_t off64_t;
#include <apr_pools.h>

const size_t	LINE_SIZE = 64;

struct node
{
  int i;
  struct node *left;
  struct node *right;
};

int
node_check(const struct node *n)
{
  int acc = n->i;

  if (n->left)
    {
      const struct node *l = n->left, *r = n->right;

      acc += l->i;
      if (l->left)
	acc += node_check (l->left) - node_check (l->right);

      acc -= r->i;
      if (r->left)
	acc -= node_check (r->left) - node_check (r->right);
    }

  return acc;
}

struct node *
node_get_avail (apr_pool_t *pool)
{
  return apr_palloc (pool, sizeof(struct node));
}

struct node *
make (int i, int depth, apr_pool_t *pool)
{
  struct node *curr = node_get_avail (pool);

  curr->i = i;

  if (depth <= 0)
    {
      curr->left  = NULL;
      curr->right = NULL;
    }
  else if (depth == 1)
    {
      struct node *sub;

      sub = node_get_avail (pool);
      sub->i = 2*i - 1;
      sub->left = sub->right = NULL;
      curr->left = sub;

      sub = node_get_avail (pool);
      sub->i = 2*i;
      sub->left = sub->right = NULL;
      curr->right = sub;
    }
  else 
    {
      curr->left  = make (2*i-1, depth - 1, pool);
      curr->right = make (2*i  , depth - 1, pool);
    }

  return curr;
}

int
main(int argc, char *argv[])
{
  apr_pool_t *long_lived_pool;
  int min_depth = 4;
  int req_depth = (argc == 2 ? atoi(argv[1]) : 10);
  int max_depth = (req_depth > min_depth + 2 ? req_depth : min_depth + 2);
  int stretch_depth = max_depth+1;

  apr_initialize();

  /* Alloc then dealloc stretchdepth tree */
  {
    apr_pool_t *store;
    struct node *curr;

    apr_pool_create (&store, NULL);
    curr = make (0, stretch_depth, store);
    printf ("stretch tree of depth %i\t check: %i\n", stretch_depth, 
	    node_check (curr));
    apr_pool_destroy (store);
  }

  apr_pool_create (&long_lived_pool, NULL);

  {
    struct node *long_lived_tree = make(0, max_depth, long_lived_pool);

    /* buffer to store output of each thread */
    char *outputstr = (char*) malloc(LINE_SIZE * (max_depth +1) * sizeof(char));
    int d;

#pragma omp parallel for
    for (d = min_depth; d <= max_depth; d += 2)
      {
        int iterations = 1 << (max_depth - d + min_depth);
	apr_pool_t *store;
        int c = 0, i;

	apr_pool_create (&store, NULL);

        for (i = 1; i <= iterations; ++i)
	  {
	    struct node *a, *b;

	    a = make ( i, d, store);
	    b = make (-i, d, store);
            c += node_check (a) + node_check (b);
	    apr_pool_clear (store);
        }
	apr_pool_destroy (store);
	
	/* each thread write to separate location */
	sprintf(outputstr + LINE_SIZE * d, "%d\t trees of depth %d\t check: %d\n", (2 * iterations), d, c);
      }

    /* print all results */
    for (d = min_depth; d <= max_depth; d += 2)
      printf("%s", outputstr + (d * LINE_SIZE) );
    free(outputstr);

    printf ("long lived tree of depth %i\t check: %i\n", max_depth, 
	    node_check (long_lived_tree));

    return 0;
  }
}
