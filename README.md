About the Computer Language Benchmarks Game 2018 Archive
========================================================
This unofficial project was created to archive the Computer Language Benchmarks Game repositories, forums, and trackers
which were originally located at https://alioth.debian.org/projects/benchmarksgame/ and
https://alioth.debian.org/projects/shootout/ prior to the deprecation of the Debian Alioth server (see
https://wiki.debian.org/Alioth#Deprecation_of_Alioth ) that they were hosted on. This archive was created in April 2018.


Items That Were Archived
========================
- Repositories
    - The MAIN branch from the Computer Language Benchmarks Game repository can be viewed at
      https://gitlab.com/Zerf/ComputerLanguageBenchmarksGame2018Archive/tree/MAIN .
    - The MAIN and shootout branches from the older repository which dates back to The Great
      Computer Language Shootout days can be viewed at
      https://gitlab.com/Zerf/ComputerLanguageBenchmarksGame2018Archive/tree/shootout-MAIN and
      https://gitlab.com/Zerf/ComputerLanguageBenchmarksGame2018Archive/tree/shootout-shootout
      respectively.
- Forum pages and attachments
    - The forum pages from the Computer Language Benchmarks Game can be viewed at
      https://zerf.gitlab.io/ComputerLanguageBenchmarksGame2018Archive/alioth.debian.org/forum/forum.php%3Fforum_id=2965&max_rows=1000.html
      .
    - The older forum pages that date back to The Great Computer Language Shootout days can be
      viewed at
      https://zerf.gitlab.io/ComputerLanguageBenchmarksGame2018Archive/alioth.debian.org/forum/forum.php%3Fforum_id=999&max_rows=1000.html
      .
- Tracker pages and attachments
    - The tracker pages from the Computer Language Benchmarks Game can be viewed at
      https://zerf.gitlab.io/ComputerLanguageBenchmarksGame2018Archive/alioth.debian.org/tracker/index.php%3Fgroup_id=100815&atid=413122.html
      .
    - The older Bugs, Feature Requests, Archive One, and Archive Two tracker pages that date back to
      The Great Computer Language Shootout days can be viewed at
      https://zerf.gitlab.io/ComputerLanguageBenchmarksGame2018Archive/alioth.debian.org/tracker/index.php%3Fgroup_id=30402&atid=411002.html
      ,
      https://zerf.gitlab.io/ComputerLanguageBenchmarksGame2018Archive/alioth.debian.org/tracker/index.php%3Fgroup_id=30402&atid=411005.html
      ,
      https://zerf.gitlab.io/ComputerLanguageBenchmarksGame2018Archive/alioth.debian.org/tracker/index.php%3Fgroup_id=30402&atid=411646.html
      , and
      https://zerf.gitlab.io/ComputerLanguageBenchmarksGame2018Archive/alioth.debian.org/tracker/index.php%3Fgroup_id=30402&atid=413100.html
      respectively.


Questions?
==========
If you have any questions, please contact me at https://JeremyZerfas.com/Contact.jsp .
